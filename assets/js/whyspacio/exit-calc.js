$(document).ready(function() {
  calc();
  $('.calc').keyup(calc);
  function calc() {
    var lpm = 0.327 * 2.5 * 6.2 * $("#totalAgents").val()
    var lpy = lpm * 12
    $(".lpm").html($.number(lpm, 0, '.', ','));
    $(".lpy").html($.number(lpy, 0, '.', ','));
    $("#lpy").val(lpy);

    var cr = 0.01 * $("#convRate").val()

    var spy = 0.66 * cr * lpy
    $(".spy").html($.number(spy, 0, '.', ','));
    $("#spy").val(spy);

    var cpy = spy * $("#avgCommissionTx").val()
    $(".cpy").html($.number(cpy, 0, '.', ','));
    $("#cpy").val(cpy);

    var na = $("#totalAgents").val()
    $(".na").html($.number(na, 0, '.', ','));

    var rf = cpy * ($("#avgCommissionBrok").val() * 0.01)
    $(".rf").html($.number(rf, 0, '.', ','));
  }
  $('.calc').number(true, 0);
});
