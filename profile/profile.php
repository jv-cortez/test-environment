<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	$_key = $_GET["k"];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <title>Spacio Pro| Profile</title>

   		<link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/main2.css">
    <link rel="shortcut icon" type="image/png" href="http://www.spaciopro.com/favicon.png"/>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="/dashboard/js/jquery.xml2json.js"></script>
	<link rel="stylesheet" href="/assets/css/sp-template.css">

  </head>

<body style="margin:0;padding:0;">

	<div style="width:100%;height:100vh;display:flex;flex-direction:column;justify-content:center;align-items:center;color:white;font-size:30px;text-align:center;background:#2e2e2e;">
		UNDER CONSTRUCTION
	</div>
  <!--
	<div style="padding: 0 !important;border-bottom:1px solid rgba(255,255,255,0.5);position:absolute;left:0;top:0;z-index:99999;width:100%;height:60px;background:#262626;">
    	<div style="margin:0 auto;" id="top-menu-bar">
        		<a href="http://spac.io" style="float: left;height: 50px;padding: 15px 15px;font-size: 18px;line-height: 25px;">
                            <img src="/assets/img/nav-logo-white.png" height="25px" style="position:relative;top:3px;">
                	</a>




    	</div>
    </div>


    <div class="rs-container" style="-webkit-overflow-scrolling:touch;overflow-x:hidden;">
		<div class="full" style="width:800px;margin-left:auto;margin-right:auto;padding-top:85px;box-sizing:border-box;height:100%;display:none;">
        	<div style="float:left;width:350px;margin-right:25px;height:500px;">
				<div class="rs-profile-content">
					<div class="profile-image-container" style="margin:0px auto;height:100px;width:100px;border-radius:60px!important;border:2px solid white;overflow:hidden;display:inline-block;">

					</div>
					<div style="display:inline-block;font-size:20px;line-height:1.1;text-align:left;margin-left:10px;vertical-align:top;height:100px;">
						<div style="display:table;height:100px;width:270px;position:absolute;">
							<div style="display:table-cell;vertical-align:middle">
								<span class="profile-fname"></span> <span class="profile-lname"></span><br/>
								<span class="profile-title" style="font-size:18px;"></span>
                                <span class="profile-brokerage" style="font-size:18px;width:200px"></span>
							</div>
						</div>
					</div>
				</div>
                <div class="rs-contact-content">
					<div class="contact-label champagne">Email </div><div style="display:inline-block;" class="profile-email"></div><br/>
                    <div class="contact-label champagne">Phone </div><div style="display:inline-block;" class="profile-phone"></div><br/>
                    <div class="contact-label champagne">Mobile </div><div style="display:inline-block;" class="profile-mobile"></div><br/>
                    <div class="contact-label champagne">Website </div><div style="display:inline-block;" class="profile-website"></div><br/>
                    <span class="addr"><div class="contact-label champagne">Address </div><div style="display:inline-block;vertical-align:top;" class="profile-addr"></div><br/></span>
				</div>
            </div>
          	<div style="float:left;height:100%;" class="has-rs">
            	<div class="rs-testimonials-title">
					<span class="champagne">RATINGS</span>
					<span style="font-size:14px;">Powered By <img src="http://spaciopro.com/dashboard/img/rs_logo_full.png" height="12" style="position:relative;top:-4px;"/> </span>
				</div>

				<div class="rs-rating-content">
					<div class="rs-rating">
					<span style="font-size:24px;" class="champagne rs-satisfaction">
					</span><br/>
					Overall<br/>Satisfaction
					</div>
					<div class="rs-rating">
					<span style="font-size:24px;" class="champagne rs-recommendation">
					</span><br/>
					Recommendation<br/>Rating
					</div>
					<div class="rs-rating">
					<span style="font-size:24px;" class="champagne rs-performance">
					</span><br/>
					Performance<br/>Rating
					</div>
				</div>

				<div class="rs-testimonials-title">
					<span class="champagne">TESTIMONIALS</span>

				</div>

				<div class="rs-testimonials-content" style="position:relative;">
					<div class="rs-testimonials-scroll">
					</div>

				</div>
                <div style="text-align:center;height:30px;margin-top:15px;">
                	<img src="http://spaciopro.com/dashboard/img/arrow_down_white.png" height="30" />
                </div>
        	</div>

            <div style="float:left;height:100%;display:none;" class="no-rs" >
            	<div style="width:350px;border-top:1px solid white;border-bottom:2px solid white;padding-top:20px;padding-bottom:20px;color:white;">
                	<span class="champagne" style="font-size:20px;">Display your ratings and testimonials here.</span> Powered by <a href="http://realsatisfied.com" target="_blank"><img src="http://spaciopro.com/dashboard/img/rs_logo_full.png" height="12" style="position:relative;top:-4px;"/></a>. <Br/><BR/>
                    Simply apply your RealSatisified Vanity Key in your profile and your reviews will automatically populate here, learn how: <a href="http://spaciopro.com/realsatisfied" target="_blank"><u>spaciopro.com/realsatisfied</u></a> <BR/><Br/>

To see an example from a fellow Spacio Pro customer, visit: <a href="http://spaciopro.com/profile/petersample" target="_blank"><u>http://spaciopro.com/profile/petersample</u></a><BR/><Br/>

RealSatisfied is a customer satisfaction feedback and brand management platform for the real estate industry that generates high quality testimonials, and helps agents deliver a more consistent customer experience. <BR/><Br/>
	<span style="color:white">Don't have RealSatisified? Get your office setup today by visiting <a href="http://realsatisfied.com" target="_blank"><u>RealSatisfied.com</u></a> or by contacting help@realsatisfied.com.</span>
                </div>

        	</div>
        </div>


        <div class="mobile" style="width:300px;margin-left:auto;margin-right:auto;padding-top:85px;box-sizing:border-box;height:100%;display:none;">
				<div class="rs-profile-content" style="width:300px">
					<div class="profile-image-container" style="margin:0px auto;height:100px;width:100px;border-radius:60px;border:2px solid white;overflow:hidden;display:inline-block;">
							<img class="profile-image" src="" height="100">
					</div>
					<div style="display:inline-block;font-size:20px;line-height:1.1;text-align:left;margin-left:10px;vertical-align:top;height:100px;">
						<div style="display:table;height:100px;width:350px;position:absolute;">
							<div style="display:table-cell;vertical-align:middle">
								<span class="profile-fname"></span> <span class="profile-lname"></span><br/>
								<span class="profile-title" style="font-size:18px;"></span>
                                <span class="profile-brokerage" style="font-size:18px;"></span>
							</div>
						</div>
					</div>
				</div>
                <div class="rs-contact-content"  style="width:300px">
					<div class="contact-label champagne">Email </div><div style="display:inline-block;" class="profile-email"></div><br/>
                    <div class="contact-label champagne">Phone </div><div style="display:inline-block;" class="profile-phone"></div><br/>
                    <div class="contact-label champagne">Mobile </div><div style="display:inline-block;" class="profile-mobile"></div><br/>
                    <div class="contact-label champagne">Website </div><div style="display:inline-block;" class="profile-website"></div><br/>
                    <span class="addr"><div class="contact-label champagne">Address </div><div style="display:inline-block;vertical-align:top;" class="profile-addr"></div><br/></span>
				</div>
            	<div class="rs-testimonials-title has-rs"  style="width:300px">
					<span class="champagne">RATINGS</span>
					<span style="font-size:14px;">Powered By <a href="http://realsatisfied.com" target="_blank"><img src="http://spaciopro.com/dashboard/img/rs_logo_full.png" height="12" style="position:relative;top:-4px;"/></a> </span>
				</div>

				<div class="rs-rating-content has-rs"  style="width:300px">
					<div class="rs-rating" style="font-size:12px;">
					<span style="font-size:24px;" class="champagne rs-satisfaction">
					</span><br/>
					Overall<br/>Satisfaction
					</div>
					<div class="rs-rating" style="font-size:12px;">
					<span style="font-size:24px;" class="champagne rs-recommendation">
					</span><br/>
					Recommendation<br/>Rating
					</div>
					<div class="rs-rating" style="font-size:12px;">
					<span style="font-size:24px;" class="champagne rs-performance">
					</span><br/>
					Performance<br/>Rating
					</div>
				</div>

				<div class="rs-testimonials-title has-rs"  style="width:300px">
					<span class="champagne">TESTIMONIALS</span>

				</div>

				<div class="rs-testimonials-content rs-testimonials-noscroll has-rs"  style="width:300px">

				</div>

                <div class="no-rs" style="width:100%;border-bottom:2px solid white;padding-top:20px;padding-bottom:20px;display:none;color:white;">
                	<span class="champagne" style="font-size:20px;">Display your ratings and testimonials here.</span> Powered by <a href="http://realsatisfied.com" target="_blank"><img src="http://spaciopro.com/dashboard/img/rs_logo_full.png" height="12" style="position:relative;top:-4px;"/></a>. <Br/><BR/>
                    Simply apply your RealSatisified Vanity Key in your profile and your reviews will automatically populate here, learn how: <a href="http://spaciopro.com/realsatisfied" target="_blank"><u>spaciopro.com/realsatisfied</u></a> <BR/><Br/>

To see an example from a fellow Spacio Pro customer, visit: <a href="http://spaciopro.com/profile/petersample" target="_blank"><u>http://spaciopro.com/profile/petersample</u></a><BR/><Br/>

RealSatisfied is a customer satisfaction feedback and brand management platform for the real estate industry that generates high quality testimonials, and helps agents deliver a more consistent customer experience. <BR/><Br/>
	<span style="color:white">Don't have RealSatisified? Get your office setup today by visiting <a href="http://realsatisfied.com" target="_blank"><u>RealSatisfied.com</u></a> or by contacting help@realsatisfied.com.</span>
                </div>
        </div>
	</div>

-->







<script type="text/javascript">

	var ukey = "<?php echo $_key ?>";
	var rsFeed = {};
	var user = {};

	$(document).ready(function(){
		resizeWindow();
		getProfile(function(){

			$(".profile-fname").html(user["fname"]);
			$(".profile-lname").html(user["lname"]);

			if(user["title"] != "") {
				$(".profile-title").html(user["title"]+"<br/>");
			}
			if(user["brokerage"] != "") {
				$(".profile-brokerage").html(user["brokerage"]+"<br/>");
			}

			$(".profile-email").html(user["email"]);
			$(".profile-phone").html(user["phone"]);
			$(".profile-mobile").html(user["mobile"]);
			if (user["website"] != "" && user["website"] != "N/A") {
				$(".profile-website").html("<a href='"+user["website"]+"' target='_blank'>"+user["website"]+"</a>");
			}

			var temp_addr = "";
			if (user["addr1"] != "" && user["addr1"] != "N/A") {
				temp_addr = user["addr1"];
				if (user["addr2"] != "" && user["addr2"] != "N/A") {
					temp_addr = temp_addr+"<br/>"+user["addr2"];
				}
				$(".profile-addr").html(temp_addr);
			} else {
				$(".addr").hide();
			}

			var d = new Date();

			$(".profile-image-container").css("background-image","url("+user['pphoto']+"?"+d.getTime()+")");

			if (user["rsID"] != "" && user["rsID"] != "N/A") {
				getRS(function(){
					if("overall_satisfaction" in rsFeed["channel"]) {
						$(".rs-satisfaction").html(rsFeed["channel"]["overall_satisfaction"]+"%");
					} else {
						$(".rs-satisfaction").html("--");
					}

					if("recommendation_rating" in rsFeed["channel"]) {
						$(".rs-recommendation").html(rsFeed["channel"]["recommendation_rating"]+"%");
					} else {
						$(".rs-recommendation").html("--");
					}

					if("performance_rating" in rsFeed["channel"]) {
						$(".rs-performance").html(rsFeed["channel"]["performance_rating"]+"%");
					} else {
							$(".rs-performance").html("--");
					}

					$(".rs-testimonials-scroll").html("");
					if ($.isArray(rsFeed["channel"]["item"])) {
						$.each(rsFeed["channel"]["item"], function(index, value){
							$(".rs-testimonials-scroll").append("<div class='rs-testimonial-desc'>&#147;"+value["description"]+"&#148;</div>");
							$(".rs-testimonials-scroll").append("<div class='rs-testimonial-title'>- "+value["title"]+"</div>");

							$(".rs-testimonials-noscroll").append("<div class='rs-testimonial-desc'>&#147;"+value["description"]+"&#148;</div>");
							$(".rs-testimonials-noscroll").append("<div class='rs-testimonial-title'>- "+value["title"]+"</div>");

						});
					} else {
						$(".rs-testimonials-scroll").append("<div class='rs-testimonial-desc'>&#147;"+rsFeed["channel"]["item"]["description"]+"&#148;</div>");
						$(".rs-testimonials-scroll").append("<div class='rs-testimonial-title'>- "+rsFeed["channel"]["item"]["title"]+"</div>");

							$(".rs-testimonials-noscroll").append("<div class='rs-testimonial-desc'>&#147;"+rsFeed["channel"]["item"]["description"]+"&#148;</div>");
							$(".rs-testimonials-noscroll").append("<div class='rs-testimonial-title'>- "+rsFeed["channel"]["item"]["title"]+"</div>");
					}
				})
			} else {
				$(".has-rs").hide();
				$(".no-rs").show();
			}
		});
	})

	$(document).resize(function(){
		resizeWindow();

	})

	$(window).resize(function(){
		resizeWindow();

	})

	function resizeWindow() {
		if( typeof( window.innerWidth ) == 'number' ) {
			var winWidth = window.innerWidth;
			var winHeight = window.innerHeight;
		}


		$("body").css("height",winHeight+"px");

		if (winWidth < 800) {
			$(".mobile").show();
			$(".full").hide();

		} else {
			$(".mobile").hide();
			$(".full").show();
			$(".rs-testimonials-content").css({"height":winHeight-350+"px"});
		}




	}

	function getProfile(completionHandler) {
			var _query = {};

			_query["fn"] = "getProfile";
			_query["ukey"] = ukey;


			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: "/oh/webservice/1.0.10/webservice.php",
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						user["fname"] = data.fname;
						user["lname"] = data.lname;
						user["title"] = data.title;
						user["email"] = data.email;
						user["phone"] = data.phone;
						user["mobile"] = data.mobile;
						user["website"] = data.website;
						user["brokerage"] = data.brokerage;
						user["addr1"] = data.addr1;
						user["addr2"] = data.addr2;
						user["pphoto"] = data.pphoto;
						user["rsID"] = data.rsID;

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
						forceLogout();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getManager error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
	}

	function getRS(completionHandler) {
		if (user["rsID"] != "" && user["rsID"] != "N/A") {
			console.log("getting RS");
				$.get('http://rss.realsatisfied.com/rss/agent/'+user["rsID"],
					function(xml){
						rsFeed = $.xml2json(xml);
						console.log(rsFeed);
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					});
		}
	}

	function randomString(_length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

		var randomstring = '';
		for (var i=0; i<_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}

		return randomstring;
	}
	/*
	var agent = navigator.userAgent;
	var isIphone = ((agent.indexOf('iPhone') != -1) || (agent.indexOf('iPod') != -1)) ;
	if (isIphone) {
		$("#iphone-msg").html("Returning you to the app.");
		setTimeout(function(){window.location.href = 'SpacioiOS://'; }, 2500);

	}
	*/
	function showAlert(title, message, button1, button2, buttonfn1, buttonfn2) {
	$(".alert-overlay").show();
	$(".alert-title").html(title);
	$(".alert-message").html(message);


	if (typeof buttonfn1 === "undefined" || buttonfn1 === null) {
		buttonfn1 = function() {
			$(".alert-overlay").hide();
		}
	}

	if (typeof buttonfn2 === "undefined" || buttonfn2 === null) {
		buttonfn2 = function() {
			$(".alert-overlay").hide();
		}
	}


	if (typeof button1 === "undefined" || button1 === null) {

		button1 = "Ok";
		$(".alert-button-1").html(button1);
		$(".alert-button-1").css("width","100%");
		$(".alert-button-2").hide();


		buttonfn1 = function() {
			$(".alert-overlay").hide();
		}
		$(".alert-button-1").on("click", buttonfn1);

  	} else if (typeof button2 === "undefined" || button2 === null) {

		$(".alert-button-1").html(button1);
		$(".alert-button-1").css("width","100%");
		$(".alert-button-2").hide();
		$(".alert-button-1").on("click", buttonfn1);
		$(".alert-button-2").on("click", buttonfn2);

	} else {
		$(".alert-button-1").html(button1);
		$(".alert-button-2").html(button2);
		$(".alert-button-1").off();
		$(".alert-button-2").off();
		$(".alert-button-1").on("click", buttonfn1);
		$(".alert-button-2").on("click", buttonfn2);
	}

}
</script>

<style>
/***************************** ALERT BOX ****************************/


@media (min-width: 700px) {
	#signup-box {
		width:700px;
		padding:20px;
		text-align:left;
	}

	#pw, #lname {
		margin-left:30px;
	}

	#top-menu-bar {
		width:1170px;
	}

}

@media (max-width: 699px) {
	#signup-box {
		width:90%;
		text-align:center;
		padding:20px 0px;

	}

	#signup-box input {
		display:block;
		margin:5px auto;
		width:90%;
		text-align:center;

	}

	#top-menu-bar {
		width:90%;
	}

}

.alert-box {
	width:400px;
	min-height:100px;
	background:white;
	vertical-align:middle;
	margin-left:auto;
	margin-right:auto;
}

.alert-title {
	color:#DD8000;
	font-size:18px;
	font-family:proxima_novasemibold;
}

.alert-message {
	color: rgb(106,98,90);
	font-size:16px;
	margin-top:10px;
}

.alert-content {
	width:400px;
	min-height:100px;

	padding:20px;
	box-sizing:border-box;
}

alert-buttons {
	width:500px;
	height:40px;
	text-align:center;

}

.alert-button-1 {
	width:200px;
	height:40px;
	line-height:40px;
	cursor:pointer;
	color:white;
	float:left;
	background:#DD8000;
	text-align:center;
}

.alert-button-2 {
	width:200px;
	height:40px;
	line-height:40px;
	cursor:pointer;
	color:white;
	float:right;
	background:rgb(239,164,79);
	text-align:center;
}

.alert-overlay {
	position:absolute;
	top:0px;
	right:0px;
	width:100%;
	height:100%;
	background:rgba(0,0,0,0.5);
	z-index:99999;
	display:none;
}

input[type=password],input[type=number] {
	width:290px;
	margin-top:10px;
	border:1px solid #e1e1e1;
	padding:7px 5px 7px 5px;
	font-size:15px;
	color:#6a625a;
}

select {
	width:290px;
	height:30px;
	border:1px solid #e1e1e1;
	background:white;
	margin-top:10px;


}


.rs-container {
	position:absolute;
	top:0px;
	left:0px;
	height:100%;
	width:100%;
	overflow:auto;
	-webkit-transition:all 0.5s ease-in;
	background:rgba(0,0,0,0.85);
	z-index:9999;
}

.rs-button {
	display:none;
	width:160px;
	padding-left:40px;
	padding-right:10px;
	height:30px;
	line-height:31px;
	font-size:16px;
	color:white;
	border:1px solid white;
	margin-top:10px;
	background-image:url(../img/rs_logo_small.png);
	background-size:30px 30px;
	background-repeat:no-repeat;
	background-position:left top;
}


.rs-profile-content {
	width:350px;
	height:146px;
	margin-left:auto;
	margin-right:auto;
	border-bottom:1px solid white;
	color:white;

}

.rs-contact-content {
	width:350px;
	margin-left:auto;
	margin-right:auto;
	color:white;
	border-bottom:2px solid white;
	padding-top:20px;
	padding-bottom:20px;

}

.rs-rating-content {
	width:350px;
	height:90px;
	margin-top:15px;
	margin-left:auto;
	margin-right:auto;
	border-bottom:2px solid white;
	color:white;

}

.rs-rating {
	display:inline-block;
	margin-right:15px;
	height:100%;
	color:white;
	font-size:14px;
}

.rs-testimonials-title {
	width:350px;
	height:40px;
	line-height:40px;
	border-bottom:1px solid white;
	font-size:20px;
	margin-left:auto;
	margin-right:auto;
	color:white;

}

.rs-testimonials-content {
	margin-top:15px;
	width:350px;
	overflow:hidden;
	margin-left:auto;
	margin-right:auto;
	color:white;

}

.rs-testimonials-scroll {
	width:350px;
	height:inherit;
	padding-right:45px;
	overflow-y:scroll;
	overflow-x:hidden;
	-webkit-overflow-scrolling:touch;

}

.rs-testimonial-desc {
	font-size:16px;
	color:white;
	margin-top:15px;
}

.rs-testimonial-title {
	font-size:16px;
	color:#eee;
	margin-top:10px;
	font-weight:bold;
	padding-bottom:10px;
	border-bottom:1px solid rgba(255,255,255,0.35);
}

.champagne {
	color:#9A6B52;
}

.contact-label {
	display:inline-block;width:80px;
}

.profile-image-container, .profile-image-container2  {
	background-repeat:no-repeat;background-size:cover;background-position:top center;
}
</style>

</body>
</html>
