<?php

error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Content-type: application/json');

require_once('system_queries.php');
require_once('manager_queries.php');
require_once('property_queries.php');
require_once('registrant_queries.php');
require_once('brokerage_queries.php');
require_once('form_queries.php');
require_once('admin_queries.php');
require_once('shared_queries.php');
require_once('mls_queries.php');
require_once('onboarding_emails.php');
require_once('system_emails.php');



$data = isset( $_POST['json'] ) ? $_POST['json'] : 0;

$fn = $data["fn"];

switch($fn) {
	case "unsubscribeEmail":
		$_email =(string)$data["email"];
		$_managerID =(string)$data["uid"];

		$return = Man_Query::unsubscribeEmail($_email, $_managerID);

		echo json_encode($return);
		break;
	case "addCurOHObj":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_ohObj = (array)$data["ohObj"];

		$return = Prop_Query::addCurOHObj($_managerID,$_vtoken,$_ohObj);

		echo json_encode($return);
		break;
	case "checkSystemStatus":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Sys_Query::checkSystemStatus($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getStandardQuestions":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getStandardQuestions($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getCustomQuestions":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getCustomQuestions($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "addCustomQuestion":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_questionObj = (array)$data["questionObj"];
		$return = Prop_Query::addCustomQuestion($_managerID,$_vtoken,$_questionObj);

		echo json_encode($return);
		break;
	case "deleteCustomQuestion":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_questionID = (string)$data["questionID"];
		$return = Prop_Query::deleteCustomQuestion($_managerID,$_vtoken,$_questionID);

		echo json_encode($return);
		break;
	case "requestDemo":
		$name = (string)$data["name"];
		$company = (string)$data["company"];
		$phone = (string)$data["phone"];
		$email = (string)$data["email"];
		$product = (string)$data["product"];
		$message = (string)$data["message"];

		$return = Adm_Query::requestDemo($name,$company,$phone,$email,$product,$message);

		echo json_encode($return);
		break;
	case "contactSales":
		$name = (string)$data["name"];
		$company = (string)$data["company"];
		$phone = (string)$data["phone"];
		$email = (string)$data["email"];
		$product = (string)$data["product"];
		$message = (string)$data["message"];

		$return = Adm_Query::contactSales($name,$company,$phone,$email,$product,$message);

		echo json_encode($return);
		break;

	case "getBrand":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getBrand($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "applyCorpCode":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_code = (string)$data["corpcode"];

		$return = Man_Query::applyCorpCode($_managerID,$_vtoken,$_code);

		echo json_encode($return);
		break;
	case "applyPromoCode":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_promo = (string)$data["promo"];

		$return = Man_Query::applyPromoCode($_managerID,$_vtoken,$_promo);

		echo json_encode($return);
		break;
	case "applyMLS":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_mls = (string)$data["mls"];
		$_mlsID = (string)$data["mlsID"];

		$return = Man_Query::applyMLS($_managerID,$_vtoken,$_mls,$_mlsID);

		echo json_encode($return);
		break;
	case "editOLR":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_olrEnabled = (string)$data["olrEnabled"];

		$return = Man_Query::editOLR($_managerID,$_vtoken,$_olrEnabled);

		echo json_encode($return);
		break;
	case "editMoxi":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_moxiEnabled = (string)$data["moxiEnabled"];

		$return = Man_Query::editMoxi($_managerID,$_vtoken,$_moxiEnabled);

		echo json_encode($return);
		break;
	case "editREW":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_rewEnabled = (string)$data["rewEnabled"];

		$return = Man_Query::editREW($_managerID,$_vtoken,$_rewEnabled);

		echo json_encode($return);
		break;
	case "editTribus":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_tribusEnabled = (string)$data["tribusEnabled"];

		$return = Man_Query::editTribus($_managerID,$_vtoken,$_tribusEnabled);

		echo json_encode($return);
		break;
	case "editReliance":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_relianceEnabled = (string)$data["relianceEnabled"];

		$return = Man_Query::editReliance($_managerID,$_vtoken,$_relianceEnabled);

		echo json_encode($return);
		break;
	case "editSendToRepresented":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_integration = (string)$data["integration"];
		$_sendToRepresented = (string)$data["sendToRepresented"];

		$return = Man_Query::editSendToRepresented($_managerID,$_vtoken,$_integration,$_sendToRepresented);

		echo json_encode($return);
		break;

	case "applyRenthopID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_renthopID = (string)$data["renthopID"];

		$return = Man_Query::applyRenthopID($_managerID,$_vtoken,$_renthopID);

		echo json_encode($return);
		break;
	case "applyMailchimpID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_mailchimp_key = (string)$data["mailchimp_key"];
		$_mailchimp_username = $data["mailchimp_username"];

		$return = Man_Query::applyMailchimpID($_managerID,$_vtoken,$_mailchimp_key,$_mailchimp_username);

		echo json_encode($return);
		break;
	case "applyRealScoutID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_realscoutKey = (string)$data["realscoutKey"];
		$_realscoutEmail = (string)$data["realscoutEmail"];

		$return = Man_Query::applyRealScoutID($_managerID,$_vtoken,$_realscoutKey,$_realscoutEmail);

		echo json_encode($return);
		break;
	case "generateCloudCMAReport":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_pid = (string)$data["pid"];
		$_cloudcmaMLS = (string)$data["cloudcmaMLS"];

		$return = Prop_Query::generateCloudCMAReport($_managerID,$_vtoken,$_pid,$_cloudcmaMLS);

		echo json_encode($return);
		break;
	case "clearCloudCMAReport":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_pid = (string)$data["pid"];

		$return = Prop_Query::clearCloudCMAReport($_managerID,$_vtoken,$_pid);

		echo json_encode($return);
		break;
	case "applyCloudCMAID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_cloudcmaID = (string)$data["cloudcmaID"];

		$return = Man_Query::applyCloudCMAID($_managerID,$_vtoken,$_cloudcmaID);

		echo json_encode($return);
		break;
	case "applyTTID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_ttID = (string)$data["ttID"];
		$_ttAutoClose = (string)$data["ttAutoClose"];

		$return = Man_Query::applyTTID($_managerID,$_vtoken,$_ttID,$_ttAutoClose);

		echo json_encode($return);
		break;
	case "applyRSID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_rsID = (string)$data["rsID"];
		$_rsAutoClose = (string)$data["rsAutoClose"];

		$return = Man_Query::applyRSID($_managerID,$_vtoken,$_rsID,$_rsAutoClose);

		echo json_encode($return);
		break;
	case "applyMSID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_marketsnapID = (string)$data["marketsnapID"];

		$return = Man_Query::applyMSID($_managerID,$_vtoken,$_marketsnapID);

		echo json_encode($return);
		break;
	case "applyTPID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_topproducerID = (string)$data["topproducerID"];

		$return = Man_Query::applyTPID($_managerID,$_vtoken,$_topproducerID);

		echo json_encode($return);
		break;
	case "applyRealtyJugglerID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_realtyjugglerID = (string)$data["realtyjugglerID"];

		$return = Man_Query::applyRealtyJugglerID($_managerID,$_vtoken,$_realtyjugglerID);

		echo json_encode($return);
		break;
	case "applyFollowUpBossID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_followupbossID = (string)$data["followupbossID"];

		$return = Man_Query::applyFollowUpBossID($_managerID,$_vtoken,$_followupbossID);

		echo json_encode($return);
		break;
	case "applyWiseAgentID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_wiseagentID = (string)$data["wiseagentID"];

		$return = Man_Query::applyWiseAgentID($_managerID,$_vtoken,$_wiseagentID);

		echo json_encode($return);
		break;
	case "applyBTID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_boomtownID = (string)$data["boomtownID"];

		$return = Man_Query::applyBTID($_managerID,$_vtoken,$_boomtownID);

		echo json_encode($return);
		break;
	case "applyContactuallyID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_contactuallyID = (string)$data["contactuallyID"];
		$_contactuallyBucket = (string)$data["contactuallyBucket"];

		$return = Man_Query::applyContactuallyID($_managerID,$_vtoken,$_contactuallyID,$_contactuallyBucket);

		echo json_encode($return);
		break;
	case "getContactuallyBuckets":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getContactuallyBuckets($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "saveContactuallyBucket":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_bucketID =(string)$data["bucketID"];

		$return = Man_Query::saveContactuallyBucket($_managerID,$_vtoken,$_bucketID);

		echo json_encode($return);
		break;
	case "editTopProducer":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_topproducerID = (string)$data["topproducerID"];
		$_marketsnapID = (string)$data["marketsnapID"];

		$return = Man_Query::editTopProducer($_managerID,$_vtoken,$_topproducerID,$_marketsnapID);

		echo json_encode($return);
		break;
	case "getTestimonialTree":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getTestimonialTree($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "testEmail":
		$return = Reg_Query::testEmail();

		echo json_encode($return);
		break;
	case "registerManager":
		$_email =strtolower((string)$data["email"]);
		$_phone =(string)$data["phone"];
		$_website =(string)$data["website"];
		$_fname =(string)$data["fname"];
		$_lname =(string)$data["lname"];
		$_brokerage=(string)$data["brokerage"];
		$_pw=(string)$data["pw"];
		$_salt=(string)$data["salt"];

		$return = Man_Query::registerManager($_fname, $_lname, $_email, $_phone,$_website, $_brokerage, $_pw, $_salt);

		echo json_encode($return);
		break;
	case "checkUsersBank":
		$_email =strtolower((string)$data["email"]);
		$_pw=(string)$data["password"];
		$_salt=(string)$data["salt"];

		$return = Man_Query::checkUsersBank($_email, $_salt, $_pw);

		echo json_encode($return);
		break;
	case "registerManagerTrial":
		$_email =strtolower((string)$data["email"]);
		$_phone =(string)$data["phone"];
		$_mobile =(string)$data["mobile"];
		$_website =(string)$data["website"];
		$_fname =(string)$data["fname"];
		$_lname =(string)$data["lname"];
		$_brokerage=(string)$data["brokerage"];
		$_pw=(string)$data["pw"];
		$_salt=(string)$data["salt"];
		$_promo=(string)$data["promo"];
		$_pphoto = array_key_exists("pphoto", $data) ? (bool)$data["photo"] : "";
		$_confirmed = array_key_exists("confirmed", $data) ? (bool)$data["confirmed"] : false;
		$_referred = array_key_exists("referred", $data) ? $data["referred"] : "";

		$return = Man_Query::registerManagerTrial($_fname, $_lname, $_email, $_phone,$_mobile,$_website, $_pphoto,$_brokerage, $_pw, $_salt,$_promo,$_confirmed,$_referred);

		echo json_encode($return);
		break;
	case "registerManagerRS":
		$_email =strtolower((string)$data["email"]);
		$_phone =(string)$data["phone"];
		$_mobile =(string)$data["mobile"];
		$_website =(string)$data["website"];
		$_fname =(string)$data["fname"];
		$_lname =(string)$data["lname"];
		$_title =(string)$data["title"];
		$_pphoto =(string)$data["pphoto"];
		$_brokerage=(string)$data["brokerage"];
		$_addr1=(string)$data["addr1"];
		$_pw=(string)$data["pw"];
		$_salt=(string)$data["salt"];
		$_rsID=(string)$data["rsID"];
		$_promocode=(string)$data["promocode"];

		$return = Man_Query::registerManagerRS($_fname, $_lname, $_email, $_phone, $_mobile, $_website, $_brokerage, $_addr1, $_pw, $_salt, $_pphoto, $_title, $_rsID, $_promocode );

		echo json_encode($return);
		break;

	case "registerManagerBasic":
		$_email =(string)$data["email"];
		$_phone =(string)$data["phone"];
		$_website =(string)$data["website"];
		$_fname =(string)$data["fname"];
		$_lname =(string)$data["lname"];
		$_brokerage=(string)$data["brokerage"];
		$_addr1=(string)$data["addr1"];
		$_addr2=(string)$data["addr2"];
		$_country=(string)$data["country"];
		$_stripetoken=(string)$data["stripeToken"];
		$_pw=(string)$data["pw"];
		$_salt=(string)$data["salt"];

		$return = Man_Query::registerManagerBasic($_fname, $_lname, $_email, $_phone,$_website, $_brokerage, $_addr1, $_addr2, $_pw, $_salt, $_stripetoken);

		echo json_encode($return);
		break;
	case "upgradeAccount":
		$_email =(string)$data["email"];
		$_promo =(string)$data["promo"];
		$_stripetoken=(string)$data["stripeToken"];
		$_plan=(string)$data["plan"];

		$return = Man_Query::upgradeAccount($_email, $_promo, $_stripetoken, $_plan);

		echo json_encode($return);
		break;
	case "upgradeAccountStage":
		$_email =(string)$data["email"];
		$_promo =(string)$data["promo"];
		$_stripetoken=(string)$data["stripeToken"];

		$return = Man_Query::upgradeAccountStage($_email, $_promo, $_stripetoken);

		echo json_encode($return);
		break;
	case "getSalt":
		$_email =trim(strtolower((string)$data["email"]));
		$return = Man_Query::getSaltByEmail($_email);
		echo json_encode($return);
		break;
	case "getBrokerageSalt":
		$_email =strtolower((string)$data["email"]);
		$return = Brok_Query::getBrokerageSaltByEmail($_email);
		echo json_encode($return);
		break;
	case "loginManager":
		$_email =trim(strtolower((string)$data["email"]));
		$_pw=(string)$data["password"];

		$return = Man_Query::loginManager($_email,$_pw);

		echo json_encode($return);
		break;
	case "loginBrokerageManager":
		$_email =strtolower((string)$data["email"]);
		$_pw=(string)$data["password"];

		$return = Brok_Query::loginBrokerageManager($_email,$_pw);

		echo json_encode($return);
		break;
	case "logoutManager":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::logoutManager($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "checkTrial":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::checkTrial($_managerID, $_vtoken);

		echo json_encode($return);
		break;
	case "downgradeAccount":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::downgradeAccount($_managerID, $_vtoken);

		echo json_encode($return);
		break;
	case "getProfile":
		$_ukey =(string)$data["ukey"];

		$return = Man_Query::getProfile($_ukey);

		echo json_encode($return);
		break;
	case "getManagerPaymentInfo":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getManagerPaymentInfo($_managerID,$_vtoken);
		echo json_encode($return);
		break;
	case "getMLSList":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Prop_Query::getMLSList($_managerID,$_vtoken);
		echo json_encode($return);
		break;
	case "saveTimezone":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_timezoneOffset = (int)$data["timezoneOffset"];

		$return = Man_Query::saveTimezone($_managerID,$_vtoken,$_timezoneOffset);
		//$return = array("status" => 1);
		echo json_encode($return);
		break;
	case "getManager":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getManager($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getHostedCount":

		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Prop_Query::getHostedCount($_managerID,$_vtoken);
		//$return = array("status" => 1);
		echo json_encode($return);
		break;
	case "updateHostedCount":

		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_pid =(string)$data["pid"];
		$_dateHosted =$data["dateHosted"];
		$_dayHosted =(string)$data["dayHosted"];
		$_offset =$data["offset"];

		$return = Prop_Query::updateHostedCount($_managerID,$_vtoken, $_pid, $_dateHosted, $_dayHosted,$_offset);

		echo json_encode($return);
		break;
	case "updateLastActive":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::updateLastActive($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "changePassword":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_old_pass =(string)$data["old_pass"];
		$_pass =(string)$data["pass"];

		$return = Man_Query::changePassword($_managerID,$_vtoken,$_old_pass,$_pass);

		echo json_encode($return);
		break;
	case "checkEmailConfirmed":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::checkEmailConfirmed($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "confirmEmail":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::confirmEmail($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "resendConfirmationEmail":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::resendConfirmationEmail($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "forgotPass":
		$_email =(string)$data["email"];
		$return = Man_Query::forgotPass($_email);

		echo json_encode($return);
		break;

	case "resetPass":
		$_managerID =(string)$data["managerID"];
		$_rtoken =(string)$data["rtoken"];
		$_pw =(string)$data["pw"];
		$_salt =(string)$data["salt"];

		$return = Man_Query::resetPass($_managerID,$_rtoken,$_pw,$_salt);

		echo json_encode($return);
		break;
	case "getAccountType":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];


		$return = Man_Query::getAccountType($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getProfile":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getProfile($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getProfiles":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Man_Query::getProfile($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getFC":
		$_rid =(string)$data["rid"];
		$return = Reg_Query::getFC($_rid);
		echo json_encode($return);
		break;
	case "editProfile":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_email =(string)$data["email"];
		$_phone =(string)$data["phone"];
		$_website =(string)$data["website"];
		$_fname =(string)$data["fname"];
		$_lname =(string)$data["lname"];
		$_title =array_key_exists("title", $data) ? (string)$data["title"] : "";
		$_mobile =array_key_exists("mobile", $data) ? (string)$data["mobile"] : "";
		$_addr1 =array_key_exists("addr1", $data) ? (string)$data["addr1"] : "";
		$_addr2 =array_key_exists("addr2", $data) ? (string)$data["addr2"] : "";
		$_pphoto =array_key_exists("pphoto", $data) ? (string)$data["pphoto"] : "";
		$_brokerage =(string)$data["brokerage"];
		$_office =(string)$data["office"];

		$return = Man_Query::editProfile($_managerID,$_vtoken,$_fname, $_lname, $_title,$_email, $_phone,$_mobile, $_website,$_brokerage,$_office, $_addr1, $_addr2, $_pphoto);

		echo json_encode($return);
		break;
	case "resetEmail":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_email =(string)$data["email"];

		$return = Man_Query::resetEmail($_managerID,$_vtoken,$_email);

		echo json_encode($return);
		break;
	case "broadcastMessage":
		$_vtoken =(string)$data["vtoken"];
		$_managerID =(string)$data["managerID"];
		$_pid=(string)$data["pid"];
		$_message = (string)$data["message"];
		$_emails = (array)$data["emails"];
		$_subject = (string)$data["subject"];
		$_bcc = (string)$data["bcc"];
		$_test = (string)$data["test"];

		$return = Reg_Query::broadcastMessage($_managerID,$_vtoken,$_pid,$_message,$_emails,$_subject,$_bcc,$_test);
		echo json_encode($return);
		break;
	case "getMLSListing":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_mls = array_key_exists("mls", $data) ? (string)$data["mls"] : "N/A";
		$_mlsnum = array_key_exists("mlsnum", $data) ? (string)$data["mlsnum"] : "N/A";

		$return = Prop_Query::getMLSListing($_managerID,$_vtoken,$_mls, $_mlsnum);
		echo json_encode($return);
		break;
	case "addProperty":

		$_url =(string)$data["url"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_title =(string)$data["title"];
		$_addr1 =(string)$data["addr1"];
		$_addr2=(string)$data["addr2"];
		$_image = (string)$data["image"];
		$_price = (string)$data["price"];
		$_beds = (float)$data["beds"];
		$_baths = (float)$data["baths"];
		$_dimens = (float)$data["dimens"];
		$_desc = (string)$data["desc"];
		$_questionsObj = array_key_exists("questionsObj", $data) ? $data["questionsObj"] : array();
		$_currency = array_key_exists("currency", $data) ? (string)$data["currency"] : "N/A";
		$_measurement = array_key_exists("measurement", $data) ? (string)$data["measurement"] : "N/A";
		$_cnEnabled = array_key_exists("cnEnabled", $data) ? (string)$data["cnEnabled"] : "NO";
		$_anon = array_key_exists("anon", $data) ? (string)$data["anon"] : "NO";
		$_autoEmail = array_key_exists("autoEmail", $data) ? (string)$data["autoEmail"] : "NO";
		$_mls = array_key_exists("mls", $data) ? (string)$data["mls"] : "N/A";
		$_mlsnum = array_key_exists("mlsnum", $data) ? (string)$data["mlsnum"] : "N/A";
		$_tlc = array_key_exists("tlc", $data) ? (string)$data["tlc"] : "N/A";


		$return = Prop_Query::addProperty($_managerID,$_vtoken,$_url,$_title,$_addr1,$_addr2,$_image,$_price,$_beds,$_baths,$_dimens,$_desc,$_currency,$_measurement, $_questionsObj,$_cnEnabled, $_anon,$_autoEmail, $_mls, $_mlsnum, $_tlc);
		echo json_encode($return);
		break;
	case "addPropertySudo":

		$_url =(string)$data["url"];
		$_email = strtolower((string)$data["email"]);
		$_title =(string)$data["title"];
		$_addr1 =(string)$data["addr1"];
		$_addr2=(string)$data["addr2"];
		$_image = (string)$data["image"];
		$_price = (string)$data["price"];
		$_beds = (float)$data["beds"];
		$_baths = (float)$data["baths"];
		$_dimens = (float)$data["dimens"];
		$_desc = (string)$data["desc"];
		//$_note = array_key_exists("note", $data) ? (string)$data["note"] : "";
		//$_tags = array_key_exists("tags", $data) ? $data["tags"] : array();
		$_currency = array_key_exists("currency", $data) ? (string)$data["currency"] : "N/A";
		$_measurement = array_key_exists("measurement", $data) ? (string)$data["measurement"] : "N/A";

		$return = Prop_Query::addPropertySudo($_email,$_url,$_title,$_addr1,$_addr2,$_image,$_price,$_beds,$_baths,$_dimens,$_desc,$_currency,$_measurement);
		echo json_encode($return);
		break;
	case "saveForm":
		$_pid =(string)$data["pid"];
		$_questionsObj =$data["questionsObj"];
		$_anon = (string)$data["anon"];
		$_autoEmail = (string)$data["autoEmail"];
		$_autoBCC = (string)$data["autoBCC"];
		$_mobile = array_key_exists("mobile", $data) ? (string)$data["mobile"] : "YES";
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_cnEnabled = array_key_exists("cnEnabled", $data) ? (string)$data["cnEnabled"] : "NO";
		$_brokersEnabled = array_key_exists("brokersEnabled", $data) ? (string)$data["brokersEnabled"] : "NO";
		$_emailMandatory = array_key_exists("emailMandatory", $data) ? (string)$data["emailMandatory"] : "NO";
		$_phoneMandatory = array_key_exists("phoneMandatory", $data) ? (string)$data["phoneMandatory"] : "NO";

		//$return = array("status" => 1);
		$return = Prop_Query::saveForm($_managerID,$_vtoken,$_pid,$_questionsObj,$_anon,$_mobile,$_autoEmail,$_autoBCC,$_cnEnabled,$_brokersEnabled,$_emailMandatory,$_phoneMandatory);

		echo json_encode($return);
		break;
	case "editProperty":
		$_pid =(string)$data["pid"];
		$_url =(string)$data["url"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_title =(string)$data["title"];
		$_addr1 =(string)$data["addr1"];
		$_addr2=(string)$data["addr2"];
		$_image = (string)$data["image"];
		$_price = (string)$data["price"];
		$_beds = (float)$data["beds"];
		$_baths = (float)$data["baths"];
		$_dimens = (float)$data["dimens"];
		$_desc = (string)$data["desc"];
		$_currency = (string)$data["currency"];
		$_questionsObj =(array)$data["questionsObj"];
		$_measurement = (string)$data["measurement"];
		$_cnEnabled = (string)$data["cnEnabled"];
		$_anon = (string)$data["anon"];
		$_mobile = array_key_exists("mobile", $data) ? (string)$data["mobile"] : "YES";
		$_disabled = array_key_exists("disabled", $data) ? (string)$data["disabled"] : "NO";
		$_brokersEnabled = array_key_exists("brokersEnabled", $data) ? (string)$data["brokersEnabled"] : "NO";
		$_autoEmail = (string)$data["autoEmail"];

		$return = Prop_Query::editProperty($_managerID,$_vtoken,$_pid,$_url,$_title,$_addr1,$_addr2,$_image,$_price,$_beds,$_baths,$_dimens,$_desc,$_currency,$_measurement,$_cnEnabled, $_anon,$_mobile,$_autoEmail, $_questionsObj,$_disabled,$_brokersEnabled);

		echo json_encode($return);
		break;
	case "deleteRegistrant":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_rid =(string)$data["rid"];

		$return = Reg_Query::deleteRegistrant($_managerID,$_vtoken,$_rid);

		echo json_encode($return);
		break;
	case "deleteProperty":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_pid =(string)$data["pid"];

		$return = Prop_Query::deleteProperty($_managerID,$_vtoken,$_pid);

		echo json_encode($return);
		break;
	case "disableProperty":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_pid =(string)$data["pid"];
		$_disabled =(string)$data["disabled"];

		$return = Prop_Query::disableProperty($_managerID,$_vtoken,$_pid,$_disabled);

		echo json_encode($return);
		break;
	case "syncProperties":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_properties = (array)$data["properties"];

		$return = Prop_Query::syncProperties($_managerID,$_vtoken,$_properties);

		echo json_encode($return);
		break;
	case "getPropertiesByManager":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Prop_Query::getPropertiesByManager($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getPropertyByID":
		//$_managerID =(string)$data["managerID"];
		//$_vtoken =(string)$data["vtoken"];
		$_propertyID = $data["propertyID"];

		$return = Prop_Query::getPropertyByID($_propertyID);

		echo json_encode($return);
		break;
	case "getFormByID":
		$_propertyID = (int)$data["propertyID"];

		$return = Prop_Query::getFormByID($_propertyID);

		echo json_encode($return);
		break;
	case "getPropertyForRegistrant":
		$_rid = (string)$data["rid"];

		$return = Prop_Query::getPropertyForRegistrant($_rid);

		echo json_encode($return);
		break;

	case "getFavedByID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_propertyID = (int)$data["propertyID"];

		$return = Prop_Query::getFavedByID($_managerID,$_vtoken,$_propertyID);

		echo json_encode($return);
		break;
	case "getRegistrants":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Reg_Query::getRegistrants($_managerID,$_vtoken);
		echo json_encode($return);
		break;
	case "getRegistrantsByPID":
		$_pid = (int)$data["pid"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Reg_Query::getRegistrantsByPID($_pid,$_managerID,$_vtoken);
		echo json_encode($return);
		break;
	case "getRegistrantsByManager":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Reg_Query::getRegistrantsByManager($_managerID, $_vtoken);
		echo json_encode($return);
		break;
	case "getNSRegistrants":
		$_propertyID = (int)$data["propertyID"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Reg_Query::getNSRegistrants($_propertyID,$_managerID,$_vtoken);
		echo json_encode($return);
		break;
	case "dumpUnsynchedData":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_registrants = (array)$data["unsynchedregistrants"];

		$return = Reg_Query::dumpUnsynchedData($_managerID,$_vtoken,$_registrants);
		//$return = array("status"=> 1);
		echo json_encode($return);
		break;
	case "syncHosted":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_hosted = (array)$data["hosted"];

		$return = Prop_Query::syncHosted($_managerID,$_vtoken,$_hosted);
		//$return = array("status"=> 99);
		echo json_encode($return);
		break;
	case "syncRegistrants":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_registrants = (array)$data["registrants"];

		$return = Reg_Query::syncRegistrants($_managerID,$_vtoken,$_registrants);
		//$return = array("status"=> 99);
		echo json_encode($return);
		break;
	case "sendTestEmail":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_type = (string)$data["type"];

		$return = Reg_Query::sendTestEmail($_managerID, $_vtoken, $_type);

		echo json_encode($return);
		break;
 	case "sendAutoEmails":
		$_propertyID = (int)$data["propertyID"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Reg_Query::sendAutoEmails($_propertyID,$_managerID, $_vtoken);

		echo json_encode($return);
		break;
	case "addRegularRegistrant":
		 $date = new DateTime();
		 $timeStamp = $date->getTimestamp();

		$_propertyID = (int)$data["propertyID"];
		$_ohID = array_key_exists("ohID", $data) ? (string)$data["ohID"] : "N/A";
		$_honorifics = array_key_exists("honorifics", $data) ? (string)$data["honorifics"] : "";
		$_name = (string)$data["name"];
		$_email = (string)$data["email"];
		$_phone = (string)$data["phone"];
		$_company = array_key_exists("company", $data) ? (string)$data["company"] : "";
		$_cloudcma = array_key_exists("cloudcma", $data) ? (string)$data["cloudcma"] : "NO";
		$_realscout = array_key_exists("realscout", $data) ? (string)$data["realscout"] : "NO";
		$_anon = (string)$data["anon"];
		$_answersObj = (array)$data["answersObj"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_marketsnap = array_key_exists("marketsnap", $data) ? (string)$data["marketsnap"] : "NO";
		$_source = array_key_exists("source", $data) ? (string)$data["source"] : "iPad";
		$_sendEmail = array_key_exists("sendEmail", $data) ? (bool)$data["sendEmail"] : false;
		$_isBroker = array_key_exists("isBroker", $data) ? (string)$data["isBroker"] : "NO";
		$_note = array_key_exists("note", $data) ? (string)$data["note"] : "";
		$_dateCreated = array_key_exists("dateCreated", $data) ? (array)$data["dateCreated"] : array("sec" => $timeStamp,"usec"=>0);

		$return = Reg_Query::addRegularRegistrant($_propertyID,$_ohID,$_honorifics,$_name,$_email,$_phone,$_company,$_anon,$_answersObj,$_marketsnap, $_source, $_managerID,$_vtoken, $_sendEmail,$_isBroker, $_cloudcma,$_realscout,$_note, $_dateCreated);
		echo json_encode($return);
		break;
	case "addSpacioRegistrant":
		$_propertyID = (int)$data["propertyID"];
		$_uname = (string)$data["uname"];
		$_name_share = (string)$data["name_share"];
		$_email_share = (string)$data["email_share"];
		$_phone_share = (string)$data["phone_share"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Reg_Query::addSpacioRegistrant($_propertyID,$_uname,$_name_share,$_email_share,$_phone_share,$_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "editRegistrant":
		$_registrantID =(string)$data["id"];
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_honorifics = array_key_exists("honorifics", $data) ? (string)$data["honorifics"] : "";
		$_name =(string)$data["name"];
		$_email =(string)$data["email"];
		$_phone =(string)$data["phone"];
		$_company = array_key_exists("company", $data) ? (string)$data["company"] : "";
		$_answersObj = array_key_exists("answersObj", $data) ? (array)$data["answersObj"] : array();
		$_note =(string)$data["note"];

		$return = Reg_Query::editRegistrant($_managerID,$_vtoken,$_registrantID,$_honorifics,$_name, $_email, $_phone,$_company,$_answersObj,$_note);

		echo json_encode($return);
		break;
	case "saveCreditCard":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_stripetoken=(string)$data["stripeToken"];

		$return = Man_Query::saveCreditCard($_managerID, $_vtoken, $_stripetoken);

		//$return = array("status" => 1);
		echo json_encode($return);
		break;
	case "upgradeAccountByID":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_stripetoken=(string)$data["stripeToken"];

		$return = Man_Query::upgradeAccountByid($_managerID, $_vtoken, $_stripetoken);

		echo json_encode($return);
		break;
	case "getScoreCalibration":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Reg_Query::getScoreCalibration($_managerID,$_vtoken);
		//$return = array("status" => 1);
		echo json_encode($return);
		break;
	case "editScoreCalibration":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_score_calibration = (array)$data["score_calibration"];

		$return = Reg_Query::editScoreCalibration($_managerID,$_vtoken,$_score_calibration);
		//$return = array("status" => 1);
		echo json_encode($return);
		break;
	case "deactivateRegistrant":
		$_rid =(string)$data["rid"];
		$_email =(string)$data["email"];

		$return = Reg_Query::deactivateRegistrant($_rid, $_email);

		echo json_encode($return);
		break;
	case "deleteForm":
		$_propertyID =(int)$data["propertyID"];

		$return = Form_Query::deleteForm($_propertyID);

		echo json_encode($return);
		break;
	case "createCSV":
		$_propertyID = (int)$data["propertyID"];
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_offset = $data["offset"];
		$_type = array_key_exists("type", $data) ? (string)$data["type"] : "buyers";

		$return = Reg_Query::createCSV($_propertyID,$_managerID,$_vtoken,$_offset, $_type);

		echo json_encode($return);
		break;
	case "getEmailTemplate":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_type = (string)$data["type"];

		$return = Man_Query::getEmailTemplate($_managerID,$_vtoken, $_type);

		echo json_encode($return);
		break;
	case "saveEmailTemplate":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_type = (string)$data["type"];
		$_messagebody = (string)$data["messagebody"];

		$return = Man_Query::saveEmailTemplate($_managerID,$_vtoken,$_messagebody,$_type);

		echo json_encode($return);
		break;
	case "resetEmailTemplate":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_type = (string)$data["type"];

		$return = Man_Query::resetEmailTemplate($_managerID,$_vtoken,$_type);

		echo json_encode($return);
		break;
	case "blacklistEmail":
		$_email = (string)$data["email"];

		$return = Reg_Query::blacklistEmail($_email);

		echo json_encode($return);
		break;
	case "getManagerToken":
		$_vtoken = (string)$data["vtoken"];
		$_email = (string)$data["email"];

		$return = Man_Query::getManagerToken($_vtoken,$_email);

		echo json_encode($return);
		break;
	case "claimAccountNB":
		$_email =(string)$data["email"];
		$_id =(string)$data["id"];
		$_pw =(string)$data["pw"];
		$_salt =(string)$data["salt"];

		$return = Man_Query::claimAccountNB($_email,$_id,$_pw,$_salt);

		echo json_encode($return);
		break;
	/* Brokerage Admin functions */
	case "addBrokerageManager":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];
		$_fname = (string)$data["fname"];
		$_lname = (string)$data["lname"];
		$_email = (string)$data["email"];
		$_pw = (string)$data["pw"];
		$_salt = (string)$data["salt"];
		$_type = (string)$data["type"];
		$_offices = (array)$data["offices"];

		$return = Brok_Query::addBrokerageManager($_managerID,$_vtoken,$_brokerageID, $_fname, $_lname, $_email,$_pw,$_salt, $_type,$_offices);

		echo json_encode($return);
		break;
	case "editBrokerageManager":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];
		$_mid = (string)$data["mid"];
		$_fname = (string)$data["fname"];
		$_lname = (string)$data["lname"];
		$_email = (string)$data["email"];
		$_type = (string)$data["type"];
		$_offices = (array)$data["offices"];

		$return = Brok_Query::editBrokerageManager($_managerID,$_vtoken,$_brokerageID,$_mid, $_fname, $_lname, $_email, $_type,$_offices);

		echo json_encode($return);
		break;
	case "deleteBrokerageManager":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$_email = (string)$data["email"];


		$return = Brok_Query::deleteBrokerageManager($_managerID,$_vtoken,$_brokerageID, $_email);

		echo json_encode($return);
		break;

	case "getClaimedAccounts":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$return = Brok_Query::getClaimedAccounts($_managerID,$_vtoken,$_brokerageID);

		echo json_encode($return);
		break;
	case "getUnclaimedAccounts":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$return = Brok_Query::getUnclaimedAccounts($_managerID,$_vtoken,$_brokerageID);

		echo json_encode($return);
		break;
	case "getClaimEmailsLog":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_email = (string)$data["email"];

		$return = Brok_Query::getClaimEmailsLog($_managerID,$_vtoken,$_email);

		echo json_encode($return);
		break;
	case "getOffices":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$return = Brok_Query::getOffices($_managerID,$_vtoken,$_brokerageID);

		echo json_encode($return);
		break;
	case "getBrokerageAdmins":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$return = Brok_Query::getBrokerageAdmins($_managerID,$_vtoken,$_brokerageID);

		echo json_encode($return);
		break;

	case "getBrokerageOpenHouses":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$return = Brok_Query::getBrokerageOpenHouses($_managerID,$_vtoken,$_brokerageID);

		echo json_encode($return);
		break;
	case "getBrokerageProperties":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$return = Brok_Query::getBrokerageProperties($_managerID,$_vtoken,$_brokerageID);

		echo json_encode($return);
		break;
	case "getBrokerageRegistrants":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_brokerageID = (string)$data["brokerageID"];

		$return = Brok_Query::getBrokerageRegistrants($_managerID,$_vtoken,$_brokerageID);

		echo json_encode($return);
		break;
	case "getBrokerageManager":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Brok_Query::getManager($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "getBrokerageInfo":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];

		$return = Brok_Query::getBrokerageInfo($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "sendClaimEmail":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_email =(string)$data["email"];

		$return = Brok_Query::sendClaimEmail($_managerID,$_vtoken,$_email);

		echo json_encode($return);
		break;
	case "sendClaimEmails":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_officeID =(string)$data["officeID"];

		$return = Brok_Query::sendClaimEmails($_managerID,$_vtoken,$_officeID);

		echo json_encode($return);
		break;
	case "getClaimLink":
		$_email =(string)$data["email"];

		$return = Brok_Query::getClaimLink($_email);

		echo json_encode($return);
		break;
	case "emailClaimLink":
		$_email =(string)$data["email"];


		$return = Brok_Query::emailClaimLink($_email);

		echo json_encode($return);
		break;
	case "claimAccount":
		$_email =(string)$data["email"];
		$_id =(string)$data["id"];
		$_from =(string)$data["from"];
		$_pw =(string)$data["pw"];
		$_salt =(string)$data["salt"];

		$return = Brok_Query::claimAccount($_email,$_id,$_pw,$_salt,$_from);

		echo json_encode($return);
		break;
	case "getBrokerageEmailTemplate":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Brok_Query::getBrokerageEmailTemplate($_managerID,$_vtoken);

		echo json_encode($return);
		break;
	case "saveBrokerageEmailTemplate":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];
		$_type = (string)$data["type"];
		$_body = (string)$data["messagebody"];

		/*
		$_signature = (string)$data["messagesignature"];
		$_claimEmail = (string)$data["messageclaim"];
		$_welcomeEmail = (string)$data["messagewelcome"];
		*/
		$return = Brok_Query::saveBrokerageEmailTemplate($_managerID,$_vtoken,$_body,$_type);

		echo json_encode($return);
		break;
	case "createAgentsCSV":
		$_managerID = (string)$data["managerID"];
		$_vtoken = (string)$data["vtoken"];

		$return = Brok_Query::createAgentsCSV($_managerID,$_vtoken);

		echo json_encode($return);
		break;

	case "forgotPassBrokerage":
		$_email =(string)$data["email"];
		$return = Brok_Query::forgotPassBrokerage($_email);

		echo json_encode($return);
		break;
	case "resetPassBrokerage":
		$_managerID =(string)$data["managerID"];
		$_rtoken =(string)$data["rtoken"];
		$_pw =(string)$data["pw"];
		$_salt =(string)$data["salt"];

		$return = Brok_Query::resetPassBrokerage($_managerID,$_rtoken,$_pw,$_salt);

		echo json_encode($return);
		break;
	case "editProfileBrokerage":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_email =(string)$data["email"];
		$_phone =(string)$data["phone"];
		$_website =(string)$data["website"];
		$_fname =(string)$data["fname"];
		$_lname =(string)$data["lname"];
		$_title =array_key_exists("title", $data) ? (string)$data["title"] : "";
		$_mobile =array_key_exists("mobile", $data) ? (string)$data["mobile"] : "";
		$_addr1 =array_key_exists("addr1", $data) ? (string)$data["addr1"] : "";
		$_addr2 =array_key_exists("addr2", $data) ? (string)$data["addr2"] : "";

		$return = Brok_Query::editProfile($_managerID,$_vtoken,$_fname, $_lname, $_title,$_email, $_phone,$_mobile, $_website, $_addr1, $_addr2);

		echo json_encode($return);
		break;
	case "changeBrokeragePassword":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_old_pass =(string)$data["old_pass"];
		$_pass =(string)$data["pass"];

		$return = Brok_Query::changeBrokeragePassword($_managerID,$_vtoken,$_old_pass,$_pass);

		echo json_encode($return);
		break;
	case "addOffice":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_brokerageID=(string)$data["brokerageID"];
		$_office=(string)$data["office"];
		$_addr1=(string)$data["addr1"];
		$_addr2=(string)$data["addr2"];

		$return = Brok_Query::addOffice($_managerID,$_vtoken, $_brokerageID, $_office, $_addr1, $_addr2);

		echo json_encode($return);
		break;
	case "addUser":
		$managerID =(string)$data["managerID"];
		$vtoken =(string)$data["vtoken"];
		$brokerageID=(string)$data["brokerageID"];
		$officeID=(string)$data["officeID"];
		$fname=(string)$data["fname"];
		$lname=(string)$data["lname"];
		$email=(string)$data["email"];
		$email2=(string)$data["email2"];
		$title=(string)$data["title"];
		$pphoto=(string)$data["pphoto"];
		$mobile=(string)$data["mobile"];
		$phone=(string)$data["phone"];
		$website=(string)$data["website"];

		$return = Brok_Query::addUser($managerID, $vtoken, $brokerageID, $officeID, $fname, $lname, $email, $email2, $title, $pphoto,$phone,$mobile, $website);

		echo json_encode($return);
		break;
	case "resolveUser":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_brokerageID=(string)$data["brokerageID"];
		$_officeID=(string)$data["officeID"];
		$_fname=(string)$data["fname"];
		$_lname=(string)$data["lname"];
		$_email=(string)$data["email"];

		$return = Brok_Query::resolveUser($_managerID,$_vtoken, $_brokerageID, $_officeID, $_fname, $_lname, $_email);

		echo json_encode($return);
		break;
	case "deleteUser":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_brokerageID=(string)$data["brokerageID"];
		$_email=(string)$data["email"];

		$return = Brok_Query::deleteUser($_managerID,$_vtoken, $_brokerageID, $_email);

		echo json_encode($return);
		break;
	case "deleteOffice":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_brokerageID=(string)$data["brokerageID"];
		$_officeID=(string)$data["officeID"];

		$return = Brok_Query::deleteOffice($_managerID,$_vtoken, $_brokerageID, $_officeID);

		echo json_encode($return);
		break;
	case "registerBrokerageManager":
		$_email =strtolower((string)$data["email"]);
		$_phone =(string)$data["phone"];
		$_website =(string)$data["website"];
		$_fname =(string)$data["fname"];
		$_lname =(string)$data["lname"];
		$_brokerageID=(string)$data["brokerageID"];
		$_pw=(string)$data["pw"];
		$_salt=(string)$data["salt"];

		$return = Brok_Query::registerBrokerageManager($_fname, $_lname, $_email, $_brokerageID, $_pw, $_salt);

		echo json_encode($return);
		break;
	case "saveBrokerageCreditCard":
		$_managerID =(string)$data["managerID"];
		$_vtoken =(string)$data["vtoken"];
		$_stripetoken=(string)$data["stripeToken"];

		$return = Brok_Query::saveBrokerageCreditCard($_managerID, $_vtoken, $_stripetoken);

		//$return = array("status" => 1);
		echo json_encode($return);
		break;
	case "getAllRegistrantsIDs":

		$return = Adm_Query::getAllRegistrantsIDs();

		echo json_encode($return);
		break;

}



?>
