<?php

require_once('../../require/mandrill/src/Mandrill.php');

/*
Onboarding_Email::accountConfirmationRequest('Chris','clerch@spac.io');
Onboarding_Email::welcomeAgentToSpacio('Chris','clerch@spac.io');
Onboarding_Email::welcomeBrokerageToSpacio('Chris','clerch@spac.io');
Onboarding_Email::proTipSpacioCanDoThat('Chris','clerch@spac.io');
Onboarding_Email::proTipAutomateOpenHouseFollowupEmails('Chris','clerch@spac.io');
Onboarding_EMail::proTipTakeNotesSeamlessly('Chris','clerch@spac.io');
Onboarding_Email::whatDoYouThink('Chris','clerch@spac.io');
Onboarding_Email::proTipStreamlineWithIntegrations('Chris','clerch@spac.io');
Onboarding_Email::trialEndingUpgradeOrReferral('Chris','clerch@spac.io');
Onboarding_Email::proTipLeadScoring('Chris','clerch@spac.io');
Onboarding_Email::trialEndsInThreeDays('Chris','clerch@spac.io','01-03-2016 12:50 GMT');
Onboarding_Email::brokerageSurveyOneMonth('Chris','clerch@spac.io');
Onboarding_Email::trialEndedUpgradeOrReferral('Chris','clerch@spac.io');
Onboarding_Email::trialEnded('Chris','clerch@spac.io');
Onboarding_Email::getSpacioForTheCompany('Chris','clerch@spac.io');
*/

class Onboarding_Email{

  static function accountConfirmationRequest($fname,$email,$managerID,$vtoken){

    $spacio_link = 'http://spac.io';
    $confirm_link = 'http://spac.io/confirmemail.php';

    $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
    $message = array(
    'html' => '<html>
      <body>
        <div style="background:#f7f7f7;padding:20px;">
          <img src="http://spac.io/assets/img/nav-logo.png" height="20">
          <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
            <p>
            Hello '.$fname.',
            </p>
            <p>
            Thank you for trying out <a href='.$spacio_link.' style="color:#999;">Spacio</a>!
            </p>
            <p>
            Before we create your account, we need to confirm your email address. Please confirm your account by clicking the button below.
            </p>
            <p>
            <a href="'.$confirm_link.'?e='.$managerID.'&v='.$vtoken.'" style="text-decoration:none;color:currentColor;">
              <div style="padding-left:15px;padding-right:15px;height:30px;line-height:30px;border:1px solid black;text-decoration:none;font-family:sans-serif;font-size:14px;text-align:center;color:black;margin:0 auto;width:100px;">
                CONFIRM
              </div>
            </a>
            </p>
            <p>
            If you are receiving this email by mistake, please let us know. You can reply to this email and we will take care of the rest.
            </p>
            <p>
            Good luck with your open houses!
            </p><br/>
            <p>The Spacio Team<br/>
            <a href='.$spacio_link.' style="color:#999;">http://spac.io/</a>
            </p>
          </div>
        </div>
      </body>
    </html>',
            'subject' => '[Action Required] Confirm Your Spacio Account',
            'from_email' => 'hello@spac.io',
            'from_name' => 'Spacio',
            'to' => array(
              array(
                'email' => $email
              )
            ),
            'headers' => array('Reply-To' => 'hello@spac.io'),
            'important' => false,
            'tags' => array('account-confirmation')
    );
    $async = true;
    $result = $mandrill->messages->send($message, $async);
  }


static function welcomeAgentToSpacio($fname,$email){

  $spacio_link = 'http://spac.io';
  $app_store_link = 'https://itunes.apple.com/ca/app/spacio-best-open-house-sign/id1017394547?mt=8';
  $download_link = 'http://spac.io/download/';
  $partners_link = 'http://spac.io/partners/';
  $pricing_link = 'http://spac.io/pricing';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          I’m Melissa Kwan, co-founder of <a href='.$spacio_link.' style="color:#999;">Spacio</a>. We really appreciate you giving it a try and sincerely hope that Spacio will be able to help you automate your open houses.
          </p>
          <p>
          Here are a few things about Spacio’s paperless open house:
          </p>
          <ol>
            <li><b>You can login from any device.</b><br/>
            Spacio is accessible from multiple devices: laptop, tablet or mobile phones. You can download the iPad app <a href='.$app_store_link.' style="color:#999;">here</a> or go to <a href=http://spac.io/dashboard  style="color:#999;">http://spac.io/dashboard </a> from your computer, tablet or android devices. The iPad app works offline, which means you don’t need internet access to run Spacio at your <a href='.$download_link.' style="color:#999;">open house</a>.</li><br/>
            <li><b>30-day free trial</b><br/>
            We created Spacio to make it easier for agents, like yourself, to manage open houses and follow up communications to potential buyers. We also work with your CRM and leading marketing solutions! Take a look at this list of our integration <a href='.$partners_link.' style="color:#999;">partners</a>.</li><br/>
            <li><b>Free access for Brokerage customers.</b><br/>
            If your brokerage uses Spacio, you automatically get free access to <a href='.$pricing_link.' style="color:#999;">Spacio Lite</a> after the trial ends. To invite your brokerage to consider Spacio, <a href="mailto:?cc=hello@spac.io, melissa@spacio&bcc=ahambali@spac.io&subject=Can we try Spacio at our brokerage?&body=I’ve recently signed up for an open house solution called Spacio, and I think it’s very helpful in helping me automate open house lead capture and follow up. Can we explore the possibility to sign up the office? From what I understand they offer special functionalities for Brokerage clients like company-wide analytics on open house activities.%0D%0A%0D%0AMaybe we could get someone from Spacio to do a demo on their brokerage solutions for us. I’m cc’ing the Spacio team so they can schedule a demo for you if you’re interested to learn more. %0D%0A%0D%0ALearn more about Spacio here: http://spac.io %0D%0A%0D%0AThanks,%0D%0A%0D%0A" style="color:#999;">send this email to your office manager</a> and we’ll take care of the rest.</li>
          </ol>
          <p>
          No more pen and paper, no more messy handwriting, and no more missed opportunities.
          </p>
          <p>
          If you have any questions, feel free to reply to this email and we will be right with you. Good luck with your open houses!
          </p><br/>
          <p>Melissa Kwan<br/>
          Co-founder & CEO</p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Welcome to Spacio! Here\'s What You Need To Know...',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Melissa at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'melissa@spac.io'),
          'important' => false,
          'tags' => array('welcome-agent-email')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function welcomeBrokerageToSpacio($fname,$email){

  $spacio_link = 'http://spac.io';
  $app_store_link = 'https://itunes.apple.com/ca/app/spacio-best-open-house-sign/id1017394547?mt=8';
  $download_link = 'http://spac.io/download/';
  $partners_link = 'http://spac.io/partners/';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          I’m Melissa Kwan, co-founder of <a href='.$spacio_link.' style="color:#999;">Spacio</a>. We really appreciate you giving it a try and sincerely hope that Spacio will be able to help you automate your open houses.
          </p>
          <p>
          Here are a few things about Spacio’s paperless open house:<br/>
          <ol>
            <li><b>You can login from any device.</b><br/>
            Spacio is accessible from multiple devices: laptop, tablet or mobile phones. You can download the iPad app <a href='.$app_store_link.' style="color:#999;">here</a> or go to <a href=http://spac.io/dashboard  style="color:#999;">http://spac.io/dashboard </a> from your computer, tablet or android devices. The iPad app works offline, which means you don’t need internet access to run Spacio at your <a href='.$download_link.' style="color:#999;">open house</a>.</li><br/>
            <li><b>Spacio is integrated with your favorite solutions.</b><br/>
            We created Spacio to make it easier for agents, like yourself, to manage open houses and follow up communications to potential buyers. We also work with your CRM and leading marketing solutions! Take a look at this list of our integration <a href='.$partners_link.' style="color:#999;">partners</a>.</li><br/>
            <li><b>Spacio auto-populates your listings.</b><br/>
            Your listings are automatically added to your Spacio account so you never have to manually enter information for an open house! Listings in Spacio are refreshed daily, and your listings will appear within 24 hours.</li>
          </ol>
          </p>
          <p>
          No more pen and paper, no more messy handwriting, and no more missed opportunities.
          </p>
          <p>
          If you have any questions, feel free to reply to this email and we will be right with you. Good luck with your open houses!
          </p><br/>
          <p>Melissa Kwan<br/>
          Co-founder & CEO</p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Welcome to Spacio! Here\'s What You Need To Know...',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Melissa at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'melissa@spac.io'),
          'important' => false,
          'tags' => array('welcome-brokerage-email')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function proTipSpacioCanDoThat($fname,$email){

  $app_store_link = 'https://itunes.apple.com/ca/app/spacio-best-open-house-sign/id1017394547?mt=8';
  $download_link = 'http://spac.io/download/';
  $partners_link = 'http://spac.io/partners/';
  $agents_link = 'http://spac.io/agents/';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          You’ve probably had a chance to login to Spacio and try some of the features for your upcoming open house.
          </p>
          <p>
          But did you know that Spacio can do more than visitor registration? For example...
          </p>
          <p>
          <b>Spacio <a href='.$download_link.' style="color:#999;">iPad app</a> works offline.</b><br/>
          You can still use our iPad app to register open house visitors even without Internet access. Visitors information will be automatically synced to your Spacio account when the app is connected to wifi connection. All you have to do is connect your iPad to wifi and open the app.
          </p>
          <p>
          <b>Take notes using your phone.</b><br/>
          You can sync your mobile phone with Spacio iPad app and take notes directly from your phone as Visitors sign in from the iPad app. All you have to do is ensure that your iPad and mobile phone are connected to wifi during the open house. Download the app <a href='.$download_link.' style="color:#999;">here</a>.
          </p>
          <p>
          <b>Verify contact information.</b><br/>
          We automatically verify contact information provided by Visitors and pull up any social profiles connected to the contact information provided.
          </p>
          <p>
          <b>Lead scoring.</b><br/>
          Our technology automatically assign scores based on your Ideal Customer Profile. Learn more <a href='.$agents_link.' style="color:#999;">here</a>.
          </p>
          <p>
          <b>Generate open house reports.</b><br/>
          After you are done with an open house, you can create a professional seller report that you can send to keep your client in the loop. You can do it by accessing the Reports tab on Spacio.
          </p>
          <p>
          <b>Integrate with your favorite services.</b><br/>
          Spacio integrates with a variety of popular CRMs and top tier <a href='.$partners_link.' style="color:#999;">marketing solutions</a> to streamline your business processes.
          </p>
          <p>
          No more pen and paper, no more messy handwriting, and no more missed opportunities.
          </p>
          <p>
          If you have any feedback or questions, feel free to reply to this email. We would love to hear from you.
          </p>
          <p>
          Good luck with your open houses!
          </p><br/>
          <p>Anastasia<br/>
          Head of Marketing, Spacio
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => '...Spacio Can Do That For Me?',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Ana at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('protip-spacio-can-do-that')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}
static function proTipAutomateOpenHouseFollowupEmails($fname,$email){

  $dashboard_link = 'http://spac.io/dashboard/';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hi '.$fname.',</p>
          <p>
          Did you know that you can automatically send follow up emails with property details and contact info from Spacio?
          </p>
          <p>
          First, you need to complete your profile on Spacio. These are the contact details your customers see in the email they receive after signing in.
          </p>
          <p style="margin-bottom:0;">
          <b>Here’s how to complete your profile:</b>
          </p>
          <ul style="margin-top:0;">
            <li>Login to <a href='.$dashboard_link.' style="color:#999;">Spacio</a>.</li>
            <li>Click My Account, and Edit Profile under the My Profile tab.</li>
            <li>Fill in the form fields.</li>
            <li>Then go to Edit Sign-In Form and enable the auto email feature.</li>
          </ul>
          <p style="margin-bottom:0;">
          <b>To enable auto emails:</b>
          </p>
          <ul style="margin-top:0;">
            <li>Click to open the menu of your open house.</li>
            <li>Then go to the Edit Sign-In Form menu of the open house you want to set this up for.</li>
            <li>After that, select Enable Auto Email, that’s it!</li>
            <li>You can test this feature by registering yourself.</li>
          </ul>
          <p>
            One caveat though, if the Visitor is working with an agent, an auto email will only be sent to their agent if they provide a contact email, and will not be sent to the Visitor directly.
          </p>
          <p>
            Questions? Let me know. Happy to hear from you.
          </p>
          <br/>
          <p>Anastasia<br/>
          Head of Marketing, Spacio
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => '[Pro Tips] Automate Open House Follow-Up Emails',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Ana at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'anastasia@spac.io'),
          'important' => false,
          'tags' => array('protip-automate-follow-up-emails')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}
static function proTipTakeNotesSeamlessly($fname,$email){

  $app_store_link = 'https://itunes.apple.com/ca/app/spacio-best-open-house-sign/id1017394547?mt=8';
  $download_link = 'http://spac.io/download/';


  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hi '.$fname.',</p>
          <p>
          Did you know that you can log into Spacio from your iPad and mobile phone at the same time? That way you can take notes directly from your phone as Visitors sign in from the iPad app.
          </p>
          <p style="margin-bottom:0;">
          <b>Here’s how you can do that:</b>
          </p>
          <ul style="margin-top:0;">
            <li>Open Spacio on your <a href='.$download_link.' style="color:#999;">mobile</a> and on your Login to <a href='.$app_store_link.' style="color:#999;">iPad</a>.</li>
            <li>Ensure that both devices have access to wifi during the open house; this is important so your mobile can ‘fetch’ contacts as Visitors sign in.</li>
            <li>Select your open house from the list and bring up the Visitors panel.</li>
            <li>Select the Visitor and scroll down to the notes section.</li>
            <li>Simply refresh your phone to fetch new data from the iPad after a Visitor registers.</li>
          </ul>
          <p>
           No wifi access? You can still take notes on Visitors within Spacio by tapping the contact button on the bottom left corner of your iPad sign-in form.
          <p>
            Questions? Let me know. Happy to hear from you.
          </p>
          <br/>
          <p>Anastasia<br/>
          Head of Marketing, Spacio
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => '[Pro Tips] Take Notes Seamlessly From Your Mobile Phone.',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Ana at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'anastasia@spac.io'),
          'important' => false,
          'tags' => array('protip-take-notes-seamlessly')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}
static function whatDoYouThink($fname,$email){

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hi '.$fname.',</p>
          <p>
          I wanted to personally thank you again for giving Spacio a try. We really appreciate it.
          </p>
          <p>
          Spacio co-founders, Melissa and Ting, built the technology because they saw first-hand how frustrating it can be to miss opportunities over things like messy handwriting or lost leads. This past year, we have added more functionalities because agents, such as yourself, give us honest feedback about their experience.
          </p>
          <p>
          I’d like to know your feedback. How is your experience using Spacio so far?
          </p>
          <p>
          Let me know. Looking forward to hearing from you!
          </p>
          <br/>
          <p>Anastasia<br/>
          Head of Marketing, Spacio
          </p>
        </div>
      </div>
    </body>
    </html>',
            'subject' => 'What Do You Think?',
            'from_email' => 'hello@spac.io',
            'from_name' => 'Ana at Spacio',
            'to' => array(
              array(
                'email' => $email
              )
            ),
            'headers' => array('Reply-To' => 'Anastasia@spac.io'),
            'important' => false,
            'tags' => array('what-do-you-think')
    );
    $async = true;
    $result = $mandrill->messages->send($message, $async);
}

static function proTipStreamlineWithIntegrations($fname,$email){

  $partners_link = 'http://spac.io/partners/';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hi '.$fname.',</p>
          <p>
          Did you know that Spacio is integrated with your favorite CRM and leading <a href='.$partners_link.' style="color:#999;">marketing solutions</a>?
          </p>
          <p style="margin-bottom:0;">
          <b>Here’s how you can do it:</b>
          </p>
          <ul style="margin-top:0;">
            <li>Login to Spacio.</li>
            <li>Go to My Account.</li>
            <li>Select Integrations. </li>
            <li>Choose from the list, et voila!</li>
          </ul>
          <p>
            Don’t see your favorite <a href='.$partners_link.' style="color:#999;">solutions</a> listed? Reply to this email and let us know. We’ll get on it.
          </p><br/>
          <p>Anastasia<br/>
          Head of Marketing, Spacio</p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => '[Pro Tips] Streamline Business Processes With Integrations',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Ana at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'anastasia@spac.io'),
          'important' => false,
          'tags' => array('protip-streamline-with-integrations')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function trialEndingUpgradeOrReferral($fname,$email){

  $pricing_link ='http://spac.io/pricing';
  $upgrade_link = 'http://spac.io/upgradeyouraccount';


  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hi '.$fname.',</p>
          <p>
          I hope you have been enjoying Spacio!
          </p>
          <p>
          I just wanted to let you know that your trial is ending in 10 days. After the trial, you can still use Spacio for only $15 per month.
          </p>
          <a href='.$upgrade_link.' style="text-decoration:none;color:currentColor;">
            <div style="padding-left:15px;padding-right:15px;height:30px;line-height:30px;border:1px solid black;text-decoration:none;font-family:sans-serif;font-size:14px;text-align:center;color:black;margin:0 auto;width:100px;">
              UPGRADE
            </div>
          </a>
          <p>
            You can continue to use it for free if your brokerage chooses to sign up for the Spacio Brokerage plan. Refer us to your office manager by sending him/her <a href="mailto:?cc=hello@spac.io, melissa@spacio&bcc=ahambali@spac.io&subject=Can we try Spacio at our brokerage?&body=I’ve recently signed up for an open house solution called Spacio, and I think it’s very helpful in helping me automate open house lead capture and follow up. Can we explore the possibility to sign up the office? From what I understand they offer special functionalities for Brokerage clients like company-wide analytics on open house activities.%0D%0A%0D%0AMaybe we could get someone from Spacio to do a demo on their brokerage solutions for us. I’m cc’ing the Spacio team so they can schedule a demo for you if you’re interested to learn more. %0D%0A%0D%0ALearn more about Spacio here: http://spac.io %0D%0A%0D%0AThanks,%0D%0A%0D%0A" style="color:#999;">this email</a> and we’ll take it from there. As a thank you, we’ll give you 3 months of Spacio subscription.
          </p>
          <p>
          To learn more, visit our <a href='.$pricing_link.' style="color:#999;">website</a>.
          </p>
          <p>
          Questions? Let me know. Happy to hear from you.
          </p>
          <br/>
          <p>Anastasia<br/>
          Head of Marketing, Spacio
          </p>
        </div>
      </div>
    </body>
    </html>',
            'subject' => 'Your Spacio Trial Is Ending. Get Spacio For Free!',
            'from_email' => 'hello@spac.io',
            'from_name' => 'Ana at Spacio',
            'to' => array(
              array(
                'email' => $email
              )
            ),
            'headers' => array('Reply-To' => 'anastasia@spac.io'),
            'important' => false,
            'tags' => array('trial-ending-upgrade-referral')
    );
    $async = true;
    $result = $mandrill->messages->send($message, $async);
}
static function proTipLeadScoring($fname,$email){

  $agents_link = 'http://spac.io/agents/';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hi '.$fname.',</p>
          <p>
            Did you know that Spacio automatically assigns a Lead Quality Score to <a href='.$agents_link.' style="color:#999;">Visitors</a> who sign in to your open house?
          </p>
          <p>
          A Lead Quality Score is assigned to Visitors based on your Ideal Customer Profile. You can tweak these preferences in My Account/Lead Scoring. The score is generated based on your preferences, i.e.: how many fields visitors fill out, whether their contact is verified, etc. This is an algorithm we are constantly improving.
          </p>
          <p>
          You can find Lead Quality Scores of each Visitor in the View Visitors Panel of each Open House.
          </p>
          <p>
          Questions? Let me know. Happy to hear from you.
          </p>
          <br/>
          <p>Anastasia<br/>
          Head of Marketing, Spacio
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => '[Pro Tips] Let Us Help You With Lead Scoring',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Ana at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'anastasia@spac.io'),
          'important' => false,
          'tags' => array('protip-lead-scoring')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}
static function trialEndsInThreeDays($fname,$email,$expiry){

  $subscribe_link = 'http://spac.io/subscibe/';

  $timeZone = 'America/New_York';
  date_default_timezone_set($timeZone);

  $expiry_date = new DateTime($expiry);

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>
            Your Spacio Free Trial is almost up!
          </p>
          <p>
          Enjoying Spacio paperless open house? Simply upgrade before '.$expiry_date->format('F j').' to keep using Spacio.
          </p>
          <a href='.$subscribe_link.' style="text-decoration:none;color:currentColor;">
            <div style="padding-left:15px;padding-right:15px;height:30px;line-height:30px;border:1px solid black;text-decoration:none;font-family:sans-serif;font-size:14px;text-align:center;color:black;margin:0 auto;width:100px;">
              SUBSCRIBE
            </div>
          </a>
          <p>
          ADD AN IMAGE FOR PRICING HERE ($15 plan).
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Only 3 Days Left In Your Free Trial!',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Ana at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'anastasia@spac.io'),
          'important' => false,
          'tags' => array('trial-ends-in-three-days')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}
static function brokerageSurveyOneMonth($fname,$email){

  $survey_link = 'http://spac.io/survey/';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>
          Hi '.$fname.',
          </p>
          <p>
            It’s been almost a month since you signed up for Spacio. Can I ask you one quick question?
          </p>
          <a href='.$survey_link.' style="text-decoration:none;color:currentColor;">
            <div style="padding-left:10px;padding-right:10px;height:30px;line-height:30px;border:1px solid black;text-decoration:none;font-family:sans-serif;font-size:14px;text-align:center;color:black;margin:0 auto;width:250px;">
              CUSTOMER SATISFACTION SURVEY
            </div>
          </a>
          <p>
            If you have any feedback or suggestions, do let me know. Always great to hear from you!
          </p>
          <p>
            Thanks in advance,
          </p>
          <br/>
          <p>
          Anastasia<br/>
          Head of Marketing, Spacio
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Quick Question For You...',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Ana at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'anastasia@spac.io'),
          'important' => false,
          'tags' => array('brokerage-survey-one-month')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function trialEndedUpgradeOrReferral($fname,$email){

  $spacio_link = 'http://spac.io';
  $pricing_link ='http://spac.io/pricing';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>
          Your Spacio Free Trial Is Up
          </p>
          <p>
          Upgrade now to avoid service interruptions for your upcoming open houses.
          </p>
          <a href='.$pricing_link.' style="text-decoration:none;color:currentColor;">
            <div style="padding-left:15px;padding-right:15px;padding-top:10px;height:40px;line-height:15px;border:1px solid black;text-decoration:none;font-family:sans-serif;font-size:14px;text-align:center;color:black;margin:0 auto;width:100px;">
              UPGRADE<br/>
              $15/month
            </div>
          </a>
          <p>
            Or.. simply introduce... us to your brokerage to continue using Spacio for free. Send <a href="mailto:?cc=hello@spac.io, melissa@spac.io&bcc=ahambali@spac.io&subject=Can we try Spacio at our brokerage?&body=I’ve recently signed up for an open house solution called Spacio, and I think it’s very helpful in helping me automate open house lead capture and follow up.%0D%0A%0D%0ACan we explore the possibility to sign up the office? From what I understand they offer special functionalities for Brokerage clients like company-wide analytics on open house activities.%0D%0A%0D%0AMaybe we could get someone from Spacio to do a demo on their brokerage solutions for us. I’m cc’ing the Spacio team so they can schedule a demo for you if you’re interested to learn more. %0D%0A%0D%0ALearn more about Spacio here: http://spac.io %0D%0A%0D%0AThanks,%0D%0A%0D%0A" style="color:#999;">this email</a> to your office manager and we will take care for it from there.
          </p>
        </div>
      </div>
    </body>
    </html>',
            'subject' => 'Last Chance...',
            'from_email' => 'hello@spac.io',
            'from_name' => 'Spacio',
            'to' => array(
              array(
                'email' => $email
              )
            ),
            'headers' => array('Reply-To' => 'hello@spac.io'),
            'important' => false,
            'tags' => array('trial-ended-upgrade-referral')
    );
    $async = true;
    $result = $mandrill->messages->send($message, $async);
}
static function trialEnded($fname,$email){

  $spacio_link = 'http://spac.io';
  $brokerages_link = 'http://spac.io/brokerages/';
  $pricing_link ='http://spac.io/pricing';
  $dashboard_link = 'http://spac.io/dashboard';
  $spacio_email = 'hello@spac.io';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>
          Hello '.$fname.',
          </p>
          <p>
            Your Spacio free trial has ended. To continue using Spacio for your open houses you will need to upgrade to Spacio For Agents for only <a href='.$pricing_link.' style="color:#999;">$15/month</a>.
          </p>
          <p>
            Alternatively, you can introduce us to your Brokerage. Spacio offers a company-wide tool for <a href='.$brokerages_link.' style="color:#999;">Brokerages</a>, which includes Spacio Lite for the <a href='.$pricing_link.' style="color:#999;">agents</a>.
          </p>
          <p>
            All you have to do is send <a href="mailto:?cc=hello@spac.io, melissa@spac.io&bcc=ahambali@spac.io&subject=Can we try Spacio at our brokerage?&body=I’ve recently signed up for an open house solution called Spacio, and I think it’s very helpful in helping me automate open house lead capture and follow up.%0D%0A%0D%0ACan we explore the possibility to sign up the office? From what I understand they offer special functionalities for Brokerage clients like company-wide analytics on open house activities.%0D%0A%0D%0AMaybe we could get someone from Spacio to do a demo on their brokerage solutions for us. I’m cc’ing the Spacio team so they can schedule a demo for you if you’re interested to learn more. %0D%0A%0D%0ALearn more about Spacio here: http://spac.io %0D%0A%0D%0AThanks,%0D%0A%0D%0A" style="color:#999;">this email</a> to your office manager and we will take care for it from there.
          </p>
          <p>
            You can still export your leads from previous open houses by logging in to your Spacio account <a href='.$dashboard_link.' style="color:#999;">here</a>.
          </p>
          <br/>
          <p>The Spacio Team<br/>
          <a href="mailto:'.$spacio_email.'?" style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
    </html>',
            'subject' => 'Your Spacio Trial Has Ended',
            'from_email' => 'hello@spac.io',
            'from_name' => 'Spacio',
            'to' => array(
              array(
                'email' => $email
              )
            ),
            'headers' => array('Reply-To' => 'hello@spac.io'),
            'important' => false,
            'tags' => array('trial-ended')
    );
    $async = true;
    $result = $mandrill->messages->send($message, $async);
}

static function getSpacioForTheCompany($fname,$email){

  $spacio_link = 'http://spac.io';
  $brokerages_link = 'http://spac.io/brokerages/';
  $melissa_email = 'lerchc@gmail.com';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>
          Hi '.$fname.',
          </p>
          <p>
            I’ve recently signed up for an open house solution called <a href='.$spacio_link.' style="color:#999;">Spacio</a>, and I think it’s very helpful in helping me automate open house lead capture and follow up.
          </p>
          <p>
            Can we explore the possibility to sign up the office? From what I understand they offer special functionalities for <a href='.$brokerages_link.' style="color:#999;">Brokerage clients</a> like company-wide analytics on open house activities.
          </p>
          <p>
            Maybe we could get someone from Spacio to do a demo on their brokerage solutions for us. I’m cc’ing the Spacio team so they can schedule a demo for you if you’re interested to learn more.
          </p>
          <p>
          Learn more about Spacio here: <a href='.$spacio_link.' style="color:#999;">http://spac.io/</a>
          </p>
          <p>
          Thanks,
          </p>
        </div>
      </div>
    </body>
    </html>',
            'subject' => 'Can we get Spacio open house for the company?',
            'from_email' => 'hello@spac.io',
            'from_name' => 'Spacio',
            'to' => array(
              array(
                'email' => $email
              ),
              array(
                'email' => $melissa_email,
                'type' => 'cc',
                'name' => 'Melissa at Spacio'
              )
            ),
            'headers' => array('Reply-To' => 'hello@spac.io'),
            'important' => false,
            'tags' => array('get-spacio-for-the-company')
    );
    $async = true;
    $result = $mandrill->messages->send($message, $async);
}
}
?>
