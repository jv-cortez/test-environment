<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Content-type: application/json');

require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

//$result = stripslashes_deep($_POST['mandrill_events']);
$result = $_POST['mandrill_events'];

//$contents = utf8_encode($result); 
$obj = json_decode($result);//, true);
$fromEmail = $obj[0]->msg->from_email;
$toTempEmail = $obj[0]->msg->email;
$html = $obj[0]->msg->html;
$text = $obj[0]->msg->text;
$subject = $obj[0]->msg->subject;
$fromName = $obj[0]->msg->from_name;

if ($html == "" || is_null($html)) {
	$msg_content = $text;	
} else {
	$msg_content = $html;
}

$rid = substr($toTempEmail, 0, -18);
$dest = substr($toTempEmail,25,-16);

//$email = new EmailParser;
//$emailParse = $email->parse($html);

$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;	
$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
$collection_senders = Db_Conn::getInstance()->getConnection()->senders;

$date = new DateTime();
$timeStamp = $date->getTimestamp();
$currentTime = new MongoDate($timeStamp);
$authorized = false;

if ($dest == "r") {
	$registrants_result = $collection_registrants->findOne(array('_id' => new MongoId($rid)));
	$sender_result = $collection_senders->findOne(array('senderEmail' => $fromEmail, 'registrantID' => $rid));
		
	if ($registrants_result["status"] != "active"){
		$authorized = true;
		$active = false;
	} else {
		$active = true;
		$authorized = true;
		
		if (is_null($sender_result)) { 
		
			$sender_document = array( "senderEmail" => $fromEmail,
									"registrantID"=> $rid,
									"dateCreated" => $currentTime);
		  
		  	$result = $collection_senders->insert($sender_document);
		  
			$actualFromEmail = $sender_document["_id"]->{'$id'};
			$actualFromEmail = $actualFromEmail."-s@private.spac.io";
			$actualToEmail = $registrants_result["email"];
			
			
		} else {
			$actualFromEmail = $sender_result["_id"]->{'$id'};
			$actualFromEmail = $actualFromEmail."-s@private.spac.io";
			$actualToEmail = $registrants_result["email"];
			
		}
		
		$emailpieces = explode("@", $actualToEmail);
			
		if($emailpieces[1] == "private.spac.io") {
			$error = true;
		} else {
			$error = false;
		}
	}
} else if ($dest == "s") {
	$registrants_result = $collection_registrants->findOne(array('email' => $fromEmail));
	$sender_result = $collection_senders->findOne(array('_id' => new MongoId($rid)));
	
	if ($registrants_result["status"] != "active"){
		$authorized = true;
		$active = false;
	} else {
		$authorized = true;	
		$active = true;
		$actualFromEmail = $registrants_result["tempEmail"];
		$actualToEmail = $sender_result["senderEmail"];
	}
	
	$emailpieces = explode("@", $actualToEmail);
			
		if($emailpieces[1] == "private.spac.io") {
			$error = true;
		} else {
			$error = false;
		}
	
}

//Find the Discussion object
//$registrants_result = $collection_registrants->findOne(array('_id' => new MongoId($rid)));	

	//if (!is_null($registrants_result) && $registrants_result["status"] == "active" && $registrants_result["email"] != "") {
	if($authorized && $active && !$error) {
		try {
			$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
			$message = array(
				'html' => $msg_content."<br/><br/><span style='font-size:12px;'><a href='http://www.spaciopro.com/unsubscribe.php?d=".$registrants_result["_id"]->{'$id'}."'><u>Unsubscribe</u></a> to this discussion thread.</span>",
				'subject' => $subject,
				'from_email' => $actualFromEmail,
				'from_name' => $fromName,
				'to' => array(
					array(
						'email' => $actualToEmail,
						)
					),
				'headers' => array('Reply-To' => $actualFromEmail),
				'important' => false,
				'tags' => array('private-email')
				);
		$async = false;
		$result = $mandrill->messages->send($message, $async);
						
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			throw $e;
		}
					
	} else if(!$authorized){
		//Do nothing
		try {
			$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
			$message = array(
				'html' => "<p>Your message was not delivered.</p><p>You are not authorized to send emails to this Buyer's secure email. Please verify that you are sending this message from the email registered with your Spacio Pro account.</p><br/><p>The Spacio Team<Br/>hello@spaciopro.com</p>",
				'subject' => "Unauthorized Email",
				'from_email' => "hello@spaciopro.com",
				'from_name' => "Spacio Pro Messaging",
				'to' => array(
					array(
						'email' => $fromEmail,
						)
					),
				'headers' => array('Reply-To' => "hello@spaciopro.com"),
				'important' => false,
				'tags' => array('unauthorized-sender')
				);
		$async = false;
		$result = $mandrill->messages->send($message, $async);
						
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			throw $e;
		}	
		
	} else if(!$active) {
		try {
			$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
			$message = array(
				'html' => "<p>Your message was not delivered.</p><p>This buyer chose to be contacted through a Spacio secure email, and has unsubscribed from this open house thread.</p><br/><p>The Spacio Team<Br/>hello@spaciopro.com</p>",
				'subject' => "Buyer Has Unsubscribed",
				'from_email' => "hello@spac.io",
				'from_name' => "Spacio Messaging",
				'to' => array(
					array(
						'email' => $fromEmail,
						)
					),
				'headers' => array('Reply-To' => "hello@spac.io"),
				'important' => false,
				'tags' => array('inactive-recipient')
				);
		$async = false;
		$result = $mandrill->messages->send($message, $async);
						
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			throw $e;
		}	
	}
		

?>