/* GLOBAL VARIABLES
----------------------------------------------------------------------------*/
Stripe.setPublishableKey('pk_live_QRFV0CvJO6Iq122LsxWt93nv');

//APP SETTINGS
var version = "1.5.1";
var source = "web";

//DEBUG MODE ON/OFF
var debug = false;

var debugTimer;
var debugCount = 0;

//SIZE OF DEVICE WINDOW
var winWidth = 0;
var winHeight = 0;

//DEVICE INFO
var deviceToken= "";
var vendorID = "";

//DATABSE VARIABLE
var db;

var inAppBrowserRef;

var alertTimer;

//APP STATUS VARIABLES
var userDevice = "none";
var loggedIn = false;
var online = true;
var forced_offline = false;
var appInBG = false;
var syncMutex = false;
var networkState;

var prevPanel = "";
var curPanel = "properties";
var curPage = "index";
var curAccountTab = "";
var curEmailsTab = "";
var curEmailTemplate ="";
var curPasscodeRestriction = "";


var curIntegrationType = "all"
var curPID = "";
var curSharePID = "";

var curRID = 0;
var curOHID = "";
var curOHObj = null;

var curSessionsCountMax = 20;
var curRankingsCountMax = 20;

var curDocIndex = -1;
var registrantDocs = [];
var signaturePad;


var main_color = "2b2b2b";

var lastPaused;
var lastResumed;


//USER VARIABLES

var user = {};
var team = {};
var brokerage = {};
var properties = {};
var registrants = {};
var registrants_inactive = {};
var hosted = {};
var report_registrants = {};
var report_properties = {};
var brand = {};
var rsFeed = {};
var ttFeed = {};
var idcFeed = {};
var zillowreviewFeed = {};

var listingsSearchResult = [];
var propertiesSearchResult = {};
var registrantsSearchResult = {};

var mlsList = {};

var registrantsBackup = [];
var cachedRegistrants = [];
var cachedProperties = [];
var cachedHosted = [];
var cachedOHs = [];
var cachedTLC = {};
var defaultQuestions = [];
var standardQuestions = [];
var customQuestions = [];

var newuser = false;
var accountCancelled = false;


var temp_questions = [];
var temp_default_questions = [];
var editedProperties = [];
var addedProperties = [];
var deletedProperties = [];
var unsynchedProperties = [];
var unsynchedRegistrants = [];

var unsynchedPropertiesBackup = {};

var customQuestionToAdd = {};
var customChoicesToAdd = [];

var default_form = {
    "autoEmail":"NO",
    "mobile":"NO",
    "autoBCC":"NO",
    "cnEnabled":"NO",
    "emailMandatory":"NO",
    "phoneMandatory":"NO",
    "autoEmailOffset":3,
    "questionsObj":[]
};

var activeSubCategories = {
    "properties-status":true,
    "visitor-quality":true,
    "visitor-representation":true,
    "visitor-financing":true,
};

var reportStats = {
    "total_properties": 0,
    "total_visitors": 0,
    "total_oh": 0,
    "active_properties": 0,
    "inactive_properties": 0,
    "visitor_with_contact": 0,
    "visitor_without_contact": 0,
    "visitor_verified": 0,
    "visitor_with_representation": 0,
    "visitor_without_representation": 0,
    "visitor_unknown_representation": 0,
    "visitor_with_financing": 0,
    "visitor_without_financing": 0,
    "visitor_unknown_financing": 0
};

var default_left_panel = '<div style="width:100%;height:100%;background-color:rgba(0,0,0,0.0);text-align:center;color:white"><div style="position:absolute;bottom:0;left:0;width:100%;height:60px;border-top:1px solid rgba(255,255,255,0.25);text-align:center;z-index:50;"><img src="img/close_white.png" height="30" style="margin-top:15px;" onClick="closeSignInForm()" class="btn"></div><div style="position:absolute;top:0px;left:0px;width:100%;height:500px;text-align:left;background-image:url(img/img_gradient_reverse.png);background-repeat:repeat-x;background-size:cover;"></div><div style="background-image:url(img/img_gradient.png);background-repeat:repeat-x;background-size:cover;height:600px;position:absolute;width:100%;bottom:0px;left:0px;z-index:10;text-align:left;"><div style="width:100%;position:absolute;bottom:60px;left:0px;box-sizing:border-box;padding:20px;"><div style="font-size:30px;color:white;margin-bottom:20px;" id="propertyTitle"></div><div class="profile-image-container" style="margin:0px auto;height:60px;width:60px;border-radius:60px;border:2px solid white;overflow:hidden;display:inline-block;"></div><div style="display:inline-block;font-size:20px;line-height:1.1;text-align:left;margin-left:10px;vertical-align:top;"><span style="font-size:20px;color:white;">Hosted By:<br/><span class="profile-fname"></span> <span class="profile-lname"></span> <span class="profile-host"></span></span><br/><span class="profile-title" style="font-size:18px;"></span><br/><div class="rs-button pulse-5s" onClick="showRSPanel()">View My Testimonials</div><div class="zillowreview-button pulse-5s" onClick="showZillowReviewPanel()">View My Reviews</div><div class="tt-button pulse-5s" onClick="showTTPanel()">View My Testimonials</div></div></div></div></div>';

var rsCloseTimer = "";
var ttCloseTimer = "";
var zillowCloseTimer = "";

var tlcCloseTimer = "";
var docusignTimer = "";
var seerepsTimer = "";
var keyboardTimer = "";

var default_lead_score_calibration = [
    {
        "question":"Are you working with an agent?",
        "type":"eq1",
        "max":2,
        "scoring":{"YES":1, "NO":2}
    },
    {
        "question":"Are you mortgage pre-approved?",
        "type":"yn",
        "max":2,
        "scoring":{"YES":2, "NO":1}
    },
    {
        "question":"At what point in the buying process are you?",
        "type":"mc6",
        "max":3,
        "scoring":{"Seeing what's out there":1, "Actively looking":2, "I'm ready":3}
    },
    {
        "question":"Do you currently rent or own?",
        "type":"mc3",
        "max":2,
        "scoring":{"Rent":1, "Own":2}
    },
    {
        "question":"Are you looking to buy or sell?",
        "type":"mc2",
        "max":3,
        "scoring":{"Buy":2, "Sell":1, "Both":3}
    },
    {
        "question":"How did you hear about this open house?",
        "type":"mc1",
        "max":1,
        "scoring":{"Any":1}
    },
    {
        "question":"Current Address?",
        "type":"text",
        "max":1,
        "scoring":{"Any":1}
    },
    {
        "question":"Are you looking for a live-in or investment property?",
        "type":"mc4",
        "max":2,
        "scoring":{"Live-in":2,"Investment":1}
    },
    {
        "question":"Do you have to sell your home before purchasing a new home?",
        "type":"yn",
        "max":2,
        "scoring":{"YES":1,"NO":2}
    },
    {
        "question":"Cash or Financing?",
        "type":"mc7",
        "max":2,
        "scoring":{"Cash":2,"Financing":1}
    },
    {
        "question":"When are you looking to move?",
        "type":"mc8",
        "max":3,
        "scoring":{"Immediately":3,"Next 2 Weeks":2,"Next 30 Days":1}
    },
    {
        "question":"What kind of budget are you working with?",
        "type":"mc9",
        "max":4,
        "scoring":{"Below $2,000":1,"$2,000 - 3,000":2,"$3,000 - 4,000":3,"$4,000 - 5,000":4}
    },
    {
        "question":"Why are you looking to move?",
        "type":"mc10",
        "max":3,
        "scoring":{"Relocating":3,"Need more space":2,"My lease is expiring":1}
    },
    {
        "question":"Would you like to be contacted with similar properties?",
        "type":"yn",
        "max":2,
        "scoring":{"YES":2,"NO":1}
    },
    {
        "question":"Please add me to your mailing list.",
        "type":"yn",
        "max":2,
        "scoring":{"YES":2,"NO":1}
    },

];


var lead_score_calibration = default_lead_score_calibration;
var schedulers_open = 1;

var openhouses_scheduled = [];

var curScheduleIndex = 0;
