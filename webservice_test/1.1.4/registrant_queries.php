<?php

require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/stripe-php-3.14.3/init.php');
require_once('integrations.php');

class Reg_Query {

	static function testEmail() {

		try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spaciopro_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Hello Ting</p>
				<p>Thank you for visiting. I would love to answer any questions you or your agent may have about the property</p>

			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				You received this email because you signed in on the Spacio app at an open house.<br/>
				No longer want to receive these messages?<br/>
				You may unsubscribe <a href="http://www.spac.io/unsubscribeme.php" style="color:#999;">here</a>.<br/>
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => 'Spacio Custom Test',
						'from_email' => 'ting@spac.io',
						'from_name' => 'Ting Sun',
						'to' => array(
							array(
								'email' => "tsun.personal@gmail.com"
							)
						),
						'headers' => array('Reply-To' =>"ting@spac.io"),
						'important' => false,
						'tags' => array('test-custom-email')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}


		$return = array("status" => 1);
		return $return;
	}


	/* ------------------------------ REGISTRANT RELATED FUNCTIONS ------------------------------*/

	static function checkUserBank($email, $salt, $pw) {


	}


	static function sendTestEmail($managerID, $vtoken, $type) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$email = $manager_result["email"];
			$name = $manager_result["fname"]." ".$manager_result["lname"];

			$sample_property = array(
									"url" => "http://www.spac.io",
									"title"=> "SAMPLE: Beautiful Home, Move in Ready",
									"addr1"=> "123 Central Park, New York, NY",
									"addr2"=> "",
									"image"=> "https://s3.amazonaws.com/spacio-user-images/spaciopro_sample.jpg",
									"price" => "2,500,000",
									"beds" => 2,
									"baths" => 2,
									"dimens" => 2000,
									"desc" => "SAMPLE: Beautiful home next to Central Park. Move in ready. Rare opportunity, contact me now for more details.\r\n\r\n*This is a sample open house entry only. Information listed here is not accurate, this is not an offering for sale.",
									"currency" => "USD",
									"measurement" => "sqft",
									"brokerageID"=>"N/A",
									"anon"=>"NO",
									"autoEmail" => "YES");

			$result = self::sendAutoEmail($sample_property,$manager_result,$email,$name, $type);

			if($result) {
				$return = array("status" => 1);
			} else {
				$return = array("status" => 2);
			}
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getFC($rid) {

		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$registrant_result = $collection_registrants->findOne(array('_id' => new MongoId($rid)));

		$email = $registrant_result["email"];
		$phone = $registrant_result["phone"];
		$pull = true;
		if(array_key_exists("fullcontact", $registrant_result)) {
			if(empty($registrant_result["fullcontact"])) {
				$pull = true;
			} else {
				$pull = false;
			}
		}

		if($pull){
			$fullcontact = array();
			if($email != "" && $email != "N/A") {
				$url="https://api.fullcontact.com/v2/person.json?email=".$email."&apiKey=6d107a2726512fdf";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$url);
				$result=curl_exec($ch);
				curl_close($ch);

				$json_result = json_decode($result, true);

				//See if full contact returns a succesful result
				if ($json_result["status"] == "200" || $json_result["status"] == 200) {
					$fullcontact = $json_result;
				} else {
					$fullcontact = array( "status" => 400);
				}

			  } else if($phone != "" && $phone != "N/A") {
				$url="https://api.fullcontact.com/v2/person.json?phone=".$phone."&apiKey=6d107a2726512fdf";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$url);
				$result=curl_exec($ch);
				curl_close($ch);

				$json_result = json_decode($result, true);

				//See if full contact returns a succesful result
				if ($json_result["status"] == "200" || $json_result["status"] == 200) {
					$fullcontact = $json_result;
				} else {
					$fullcontact = array( "status" => 400);
				}
			  } else {
				  $fullcontact = array( "status" => 0);
			  }

			  $newdata = array('$set' => array('fullcontact' => $fullcontact));
			  $u_result = $collection_registrants->update(array('_id' => new MongoId($rid)), $newdata);

			  $return = array("status" => 1);
		} else {
			$return = array("status" => 2);
		}

		return $return;

	}

	static function getScoreCalibration($managerID, $vtoken) {
		$collection_calibrations = Db_Conn::getInstance()->getConnection()->lead_score_calibration;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		$calibration_result = $collection_calibrations->findOne(array('managerID' => $managerID));

		if (!is_null($manager_result)) {

			if (!is_null($calibration_result)) {
				$return = array("status" => 1, "calibration" => $calibration_result["calibration"]);
			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function editScoreCalibration($managerID, $vtoken, $score_calibration) {
		$collection_calibrations = Db_Conn::getInstance()->getConnection()->lead_score_calibration;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		$calibration_result = $collection_calibrations->findOne(array('managerID' => $managerID));

		if (!is_null($manager_result)) {


			if (!is_null($calibration_result)) {
				$newdata = array('$set' => array('calibration' => $score_calibration));
				$u_result = $collection_calibrations->update(array('managerID' => $managerID), $newdata);
			} else {
				$date = new DateTime();
				$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);

				$document_calibration = array("managerID"=> $managerID,
										  "calibration" => $score_calibration,
										  "lastUpdated" => $currentTime);

				$result = $collection_calibrations->insert($document_calibration);
			}
			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function getRegistrants($managerID, $vtoken) {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$cursor = $collection_registrants->find(array('managerID' => $managerID));
			$cursor->sort(array('dateCreated' => -1));

			$registrants = array();
			foreach ($cursor as $doc) {
				$doc["id"] =  $doc["_id"]->{'$id'};

				if ($doc["tempEmail"] != "N/A") {
					$doc["email"] = "hidden";
				}
				if(!array_key_exists("answersObj",$doc)) {
					$doc["answersObj"] = array();
				}
				/*
				foreach ($doc["answersObj"] as $key => $aswr) {
					if(array_key_exists("question", $aswr)) {
						$aswr["questionObj"] = $aswr["question"];
						$doc["answersObj"][$key] = $aswr;
					}
				}*/

				array_push($registrants, $doc);
			}

			if(array_key_exists("teamID",$manager_result) && $manager_result["teamID"] != "" && $manager_result["teamID"] != "N/A" && $manager_result["teamRole"] == "member") {
				$team_properties = $manager_result["properties"];
				foreach ($team_properties as $team_property) {

						$team_registrants_result = $collection_registrants->find(array('pid' => $team_property, 'teamID' => $manager_result["teamID"]));
						$team_registrants_result->sort(array('dateCreated' => -1));

						foreach ($team_registrants_result as $registrant) {
							$registrant["id"] =  $registrant["_id"]->{'$id'};
							array_push($registrants, $registrant);
						}
				}
			}

			$return = array(
				"status" => 1,
				"registrants" => $registrants
			);

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getRegistrantsByPID($pid, $managerID, $vtoken) {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$cursor = $collection_registrants->find(array('pid' => $pid));
			$cursor->sort(array('dateCreated' => -1));

			$registrants = array();
			foreach ($cursor as $doc) {
				$doc["id"] =  $doc["_id"]->{'$id'};

				if ($doc["tempEmail"] != "N/A") {
					$doc["email"] = "hidden";
				}

				array_push($registrants, $doc);
			}

			$return = array(
				"status" => 1,
				"registrants" => $registrants
			);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getRegistrantsByManager($managerID, $vtoken) {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$cursor = $collection_registrants->find(array('managerID' => $managerID));
			$cursor->sort(array('dateCreated' => -1));

			$registrants = array();
			foreach ($cursor as $doc) {
				$doc["id"] =  $doc["_id"]->{'$id'};

				if ($doc["tempEmail"] != "N/A") {
					$doc["email"] = "hidden";
				}

				array_push($registrants, $doc);
			}

			if(array_key_exists("teamID",$manager_result) && $manager_result["teamID"] != "" && $manager_result["teamID"] != "N/A" && $manager_result["teamRole"] == "member") {
				$team_properties = $manager_result["properties"];
				foreach ($team_properties as $team_property) {

						$team_registrants_result = $collection_registrants->find(array('pid' => $team_property, 'teamID' => $manager_result["teamID"]));
						$team_registrants_result->sort(array('dateCreated' => -1));

						foreach ($team_registrants_result as $registrant) {
							$registrant["id"] =  $registrant["_id"]->{'$id'};
							array_push($registrants, $registrant);
						}
				}
			}

			$return = array(
				"status" => 1,
				"registrants" => $registrants
			);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}


	static function blacklistEmail($email) {
		$collection_blacklist = Db_Conn::getInstance()->getConnection()->blacklist;
		$blacklist_result = $collection_blacklist->findOne(array('email' => $email));

		if (!is_null($registrant_result)) {
			$return = array("status" => 1);
		} else {

			$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);

			$document_blacklist = array("email"=> $email,
									"dateCreated" => $currentTime);

		  	$result = $collection_blacklist->insert($document_blacklist);
			$return = array("status" => 1);
		}

		return $return;

	}

	static function deactivateRegistrant($rid, $email) {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$registrant_result = $collection_registrants->findOne(array('email' => $email, '_id' => new MongoId($rid)));

		if (!is_null($registrant_result)) {

			$newdata = array('$set' => array('status' => "inactive"));
			$u_result = $collection_registrants->update(array('_id' => new MongoId($rid)), $newdata);

			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}
		return $return;
	}



	static function deleteRegistrant($managerID,$vtoken,$rid) {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if($manager_result["teamID"] == "N/A" || $manager_result["teamID"] == "") {

				$registrant_result = $collection_registrants->findOne(array('_id' => new MongoId($rid), "managerID" => $managerID));

				if (!is_null($registrant_result)) {
					$collection_registrants->remove(array('_id' => new MongoId($rid),'managerID' => $managerID));

					$return = array(
						"status" => 1
					);
				} else {
					$return = array(
						"status" => 2
					);
				}
			} else if($manager_result["teamRole"] == "admin") {
				$registrant_result = $collection_registrants->findOne(array('_id' => new MongoId($rid), "managerID" => $managerID));

				if (!is_null($registrant_result)) {
					$collection_registrants->remove(array('_id' => new MongoId($rid)));

					$return = array(
						"status" => 1
					);
				} else {
					$return = array(
						"status" => 2
					);
				}
			} else {
				$return = array(
					"status" => 3
				);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function syncRegistrants($managerID, $vtoken, $registrants) {
		$collection = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->custom_emails;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), 'vtoken' => $vtoken));

		$dump = var_dump($registrants);

		try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spaciopro_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				'.$managerID.'<br/>
				'.$dump.'<br/>

			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				You received this email because you signed in on the Spacio app at an open house.<br/>
				No longer want to receive these messages?<br/>
				You may unsubscribe <a href="http://www.spac.io/unsubscribeme.php" style="color:#999;">here</a>.<br/>
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => 'Spacio Custom Test',
						'from_email' => 'ting@spac.io',
						'from_name' => 'Ting Sun',
						'to' => array(
							array(
								'email' => "ting@spac.io"
							)
						),
						'headers' => array('Reply-To' =>"ting@spac.io"),
						'important' => false,
						'tags' => array('test-custom-email')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}


		if (!is_null($manager_result)) {
			foreach ($registrants as $registrant) {
				$timeStamp = $registrant["dateCreated"]["sec"];
				//$timeStamp = $timeStamp / 1000;
				$currentTime = new MongoDate($timeStamp);

				$has_agent = false;
				foreach ($registrant["answers"] as $answer) {
					if($answer["question"] == "Are you working with an agent?")	{
						if($answer["answer"] == "YES") {
							$has_agent = true;
						}
					}
				}

				$note = array_key_exists("note", $registrant) ? (string)$registrant["note"] : "";

				$registrant["propertyID"] = (int)$registrant["propertyID"];
				$property_result = $collection_properties->findOne(array('propertyID' => $registrant["propertyID"]));
				$pid = $property_result["_id"]->{'$id'};
				$document_arr = array( "propertyID" => $registrant["propertyID"],
				 					"pid" => $pid,
									"source" => "iPad",
		  							"managerID" => $managerID,
									"honorifics" => $registrant["honorifics"],
		  							"name" => $registrant["name"],
									"email"=> $registrant["email"],
									"tempEmail" => "N/A",
									"phone"=> $registrant["phone"],
									"answers" => (array)$registrant["answers"],
									"brokerageID" => $manager_result["brokerageID"],
									"officeID" => $manager_result["officeID"],
									"status"=>"active",
									"autoEmailed" => true,
									"note" => $note,
									"dateCreated" => $currentTime);

		  		$result = $collection->insert($document_arr);

				if ($result) {

					$newID = $document_arr["_id"]->{'$id'};
					$tempEmail = $newID."-r@private.spac.io";

					//$registrant["propertyID"] = (int)$registrant["propertyID"];
					//$property_result = $collection_properties->findOne(array('propertyID' => $registrant["propertyID"]));

					if ($registrant["anon"] == "YES" && $registrant["email"] != "" && $registrant["email"] != "N/A") {
						$newdata = array('$set' => array('tempEmail' => $tempEmail));
						$u_result = $collection->update(array('_id' => new MongoId($newID)), $newdata);
						$registrant["email"] = $tempEmail;
					}

					$name = $registrant["name"];

					if ($name == "") {
						$name = "";
					}

					if ($property_result["price"] == "" || $property_result["price"] == "N/A") {
						$property_result["price"] = "Upon inquiry";
						$property_result["currency"] = "";
					}

					if ($property_result["addr1"] == "" || $property_result["addr1"] == "N/A") {
						$property_result["addr1"] = "Upon inquiry";
					}

					if ($property_result["addr2"] == "" || $property_result["addr2"] == "N/A") {
						$property_result["addr2"] = "";
					}

					if ($property_result["url"] == "" || $property_result["url"] == "N/A") {
						$property_result["url"] = "Upon inquiry";
					}

					if ($property_result["price"] == "" || $property_result["price"] == "N/A") {
						$property_result["price"] = "Upon inquiry";
					}

					if ($property_result["baths"] == -1) {
						$property_result["baths"] = "Upon inquiry";
					}

					if ($property_result["beds"] == -1) {
						$property_result["beds"] = "Upon inquiry";
					}

					if ($property_result["dimens"] == -1) {
						$property_result["dimens"] = "Upon inquiry";
						$property_result["measurement"] = "";
					}

					if ($property_result["desc"] == "") {
						$property_result["desc"] = "Upon inquiry";
					}

					if ($manager_result["boomtownID"] != "N/A" && $manager_result["boomtownID"] != "" && $manager_result["type"] != "freemium") {

						$answers_combined = "";

						foreach ($answers as $answer) {
							$answers_combined = $answers_combined."".$answer["questionObj"]["question"]." ".$answer["answer"]." | ";
						}

						if ($answers_combined != "") {
							$answers_combined = trim($answers_combined);
							$answers_combined = trim($answers_combined,"|");
						}

						try {
							$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
							$message = array(
								'html' => '<html><head><meta name="lead_information_version" content="1.0"><meta name="lead_source" content="spacio"><meta name="lead_type" content="Buyer"><meta name="lead_name" content="'.$name.'"><meta name="lead_email" content="'.$email.'"><meta name="lead_phone" content="'.$phone.'"><meta name="lead_message" content="'.$answers_combined.'"><meta name="lead_property_address" content="'.$property_result["addr1"].' '.$property_result["addr2"].'"></head><body>Lead Source:spacio<br>Lead Type:Buyer<br>Lead Name:'.$name.'<br>Lead Email:'.$email.'<br>Lead phone:'.$phone.'<br>Lead Message: '.$answers_combined.'<br>Lead Property Address: '.$property_result["addr1"].' '.$property_result["addr2"].'</body></html>',
								'subject' => $property_result["title"],
								'from_email' => 'hello@spac.io',
								'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
								'to' => array(
									array(
										'email' => $manager_result["boomtownID"]
									)
								),
								'headers' => array('Reply-To' => $manager_result["email"]),
								'important' => false,
								'tags' => array('boomtown-lead')
							);
							$async = false;
							$result = $mandrill->messages->send($message, $async);

						} catch(Mandrill_Error $e) {
							// Mandrill errors are thrown as exceptions
							echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
							// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
							throw $e;
						}

					}
					/*

					if ($manager_result["contactuallyID"] != "N/A" && $manager_result["contactuallyID"] != "") {

						$answers_combined = $property_result["title"]." | ";
						$answers = (array)$registrant["answers"];
						foreach ($answers as $answer) {
							$answers_combined = $answers_combined."".$answer["question"]." ".$answer["answer"]." | ";
						}

						if ($answers_combined != "") {
							$answers_combined = trim($answers_combined);
							$answers_combined = trim($answers_combined,"|");
						}

						$url = "https://www.contactually.com/api/v1/contacts.json";
						$url2 = "https://www.contactually.com/api/v1/notes.json";
						$fullname = explode(" ", $registrant["name"],2);

						if ($registrant["anon"] == "YES" && $registrant["email"] != "" && $registrant["email"] != "N/A") {
							$actualemail = $tempEmail;
						} else {
							$actualemail = $registrant["email"];
						}

						if ($manager_result["contactuallyBucket"] != "N/A" && $manager_result["contactuallyBucket"] != "") {
							$fields = http_build_query(array(
								'api_key' => $manager_result["contactuallyID"],
								'contact' => array(
									'first_name' => $fullname[0],
									'last_name' => $fullname[1],
									'email' => $actualemail,
									'phone_numbers' => $registrant["phone"],
									'add_grouping_ids' => (int)$manager_result["contactuallyBucket"],
									'tags' => "spacio"
								)
							));
						} else {
							$fields = http_build_query(array(
								'api_key' => $manager_result["contactuallyID"],
								'contact' => array(
									'first_name' => $fullname[0],
									'last_name' => $fullname[1],
									'email' => $actualemail,
									'phone_numbers' => $registrant["phone"],
									'tags' => "spacio"
								)
							));
						}
						$ch = curl_init();

						curl_setopt($ch, CURLOPT_URL,$url);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS,
								$fields);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						$server_output = curl_exec ($ch);

						$output = json_decode($server_output);

						$fields2 = http_build_query(array(
							'api_key' => $manager_result["contactuallyID"],
							'note' => array(
								'body' => $answers_combined,
								'contact_id' => $output->id
							)
						));

						$ch2 = curl_init();

						curl_setopt($ch2, CURLOPT_URL,$url2);
						curl_setopt($ch2, CURLOPT_POST, 1);
						curl_setopt($ch2, CURLOPT_POSTFIELDS,
								$fields2);
						curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);

						$server_output2 = curl_exec ($ch2);

						curl_close ($ch2);

						$id = $output->id;

						$url3 = "https://www.contactually.com/api/v1/contacts/".$id."/tags.json";
						$fields3 = http_build_query(array(
							'api_key' => $manager_result["contactuallyID"],
							'tags' => "spacio"
						));

						$ch3 = curl_init();

						curl_setopt($ch3, CURLOPT_URL,$url3);
						curl_setopt($ch3, CURLOPT_POST, 1);
						curl_setopt($ch3, CURLOPT_POSTFIELDS,
								$fields3);
						curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);

						$server_output3 = curl_exec ($ch3);

						curl_close ($ch3);

					}*/

					$emailsent = false;

					if ($registrant["email"] != "" && $registrant["email"] != "N/A" && $property_result["autoEmail"] == "YES" && !$has_agent) {
						$emailsent = true;
						$email_result = $collection_emails->findOne(array('managerID' => $managerID));

						$body = $email_result['body'];
						$signature = $email_result['signature'];

						$body = str_replace("{{name}}",$name, $body);
						$body = str_replace("{{oh_title}}",$property_result["title"], $body);
						$body = str_replace("{{oh_url}}",$property_result["url"], $body);
						$body = str_replace("{{oh_addr1}}",$property_result["addr1"], $body);
						$body = str_replace("{{oh_addr2}}",$property_result["addr2"], $body);
						$body = str_replace("{{oh_addr}}",$temp_addr, $body);
						$body = str_replace("{{oh_beds}}",$property_result["beds"], $body);
						$body = str_replace("{{oh_baths}}",$property_result["baths"], $body);
						$body = str_replace("{{oh_size}}",$property_result["dimens"]." ".$property_result["measurement"], $body);
						$body = str_replace("{{oh_price}}",$property_result["price"]." ".$property_result["currency"], $body);
						$body = str_replace("{{oh_details}}",$property_result["desc"], $body);
						$body = str_replace("{{oh_img}}","<img src='".$property_result["image"]."' width='100%'>", $body);
						$body = str_replace("{{agent_phone}}",$manager_result["phone"], $body);
						$body = str_replace("{{agent_email}}",$manager_result["email"], $body);
						$body = str_replace("{{agent_title}}",$manager_result["title"], $body);
						$body = str_replace("{{agent_company}}",$manager_result["brokerage"], $body);
						$body = str_replace("{{today}}",date('F j, Y'), $body);
						$signature = str_replace("{{profile_url}}","www.spac.io/profile/".$manager_result["ukey"], $signature);
						$signature = str_replace("{{agent_photo}}","<img src='".$manager_result["pphoto"]."' height='100'>", $signature);

						try {
							$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
							if ($manager_result["brokerageID"] == "N/A" || $manager_result["brokerageID"] == "" ) {
								$message = array(
									'html' => '<html><body><div style="background:#f7f7f7;padding:20px;"><div style="background:white;font-size:14px;padding:20px;margin-top:20px;">'.$body.'<Br/><br/>'.$signature.'</div><div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">You received this email because you signed in on the Spacio app at an open house.<br/>No longer want to receive these messages?<br/>You may unsubscribe <a href="http://www.spacio.com/unsubscribeme.php" style="color:#999;">here</a>.<br/>Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/></div></div></body></html>',
									'subject' => $property_result["title"],
									'from_email' => 'hello@spac.io',
									'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
									'to' => array(
										array(
											'email' => $registrant["email"]
										)
									),
									'headers' => array('Reply-To' => $manager_result["email"]),
									'important' => false,
									'tags' => array('new-registrant-cached')
								);
							} else {
								$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));

								$template = $brokerage_result["emailTemplate"];
								$template = str_replace("{{signature}}",$signature, $template);
								$template = str_replace("{{body}}",$body, $template);

								$message = array(
									'html' => $template,
									'subject' => $property_result["title"],
									'from_email' => 'hello@spac.io',
									'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
									'to' => array(
										array(
											'email' => $registrant["email"]
										)
									),
									'headers' => array('Reply-To' => $manager_result["email"]),
									'important' => false,
									'tags' => array('new-registrant-cached')
								);
							}
							$async = false;
							$result = $mandrill->messages->send($message, $async);
						} catch(Mandrill_Error $e) {
							// Mandrill errors are thrown as exceptions
							echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
							// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
							throw $e;
						}
						$return = array("status" => 1,"timeStamp" => $timeStamp, "propertyID" => $registrant["propertyID"]);
					} else {
						$return = array("status" => 1,"timeStamp" => $timeStamp, "propertyID2" => $registrant["propertyID"],"server" => $server_output3);
					}
				}

			}


		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function dumpUnsynchedData($managerID,$vtoken,$registrants) {
		$collection_datadump = Db_Conn::getInstance()->getConnection()->datadump;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), 'vtoken' => $vtoken));


		if (!is_null($manager_result)) {
				$date = new DateTime();
		  		$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);

				$note = array_key_exists("note", $registrant) ? (string)$registrant["note"] : "";
				 $document_arr = array(
		  							"managerID" => $managerID,
		  							"data" => $registrants,
									"dateCreated" => $currentTime);

		  		$result = $collection_datadump->insert($document_arr);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	private function randomfunction() {
		return true;
	}

	static function sendAutoEmail($property_result,$manager_result, $email, $name, $type, $client_name = "", $timestamp = "")  {
		$collection = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_emails = Db_Conn::getInstance()->getConnection()->email_templates;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$managerID = $manager_result["_id"]->{'$id'};
		if ($name == "") {
			$name = "";
		}

		if ($property_result["price"] == "" || $property_result["price"] == "N/A") {
			$property_result["price"] = "Upon inquiry";
			$property_result["currency"] = "";
		}

		$temp_addr = "Upon inquiry";

		if ($property_result["addr1"] == "" || $property_result["addr1"] == "N/A") {
			$property_result["addr1"] = "Upon inquiry";
		} else {
			$temp_addr = $property_result["addr1"];
		}

		if ($property_result["addr2"] == "" || $property_result["addr2"] == "N/A") {
			$property_result["addr2"] = "";
		} else {
			$temp_addr = $temp_addr.", ".$property_result["addr2"];
		}

		if ($property_result["desc"] == "" || $property_result["desc"] == "N/A") {
			$property_result["desc"] = "Upon inquiry";
		}

		if ($property_result["url"] == "" || $property_result["url"] == "N/A") {
			$property_result["url"] = "Upon inquiry";
		}

		if ($property_result["price"] == "" || $property_result["price"] == "N/A") {
			$property_result["price"] = "Upon inquiry";
		}

		if ($property_result["baths"] == -1) {
			$property_result["baths"] = "Upon inquiry";
		}

		if ($property_result["beds"] == -1) {
			$property_result["beds"] = "Upon inquiry";
		}

		if ($property_result["dimens"] == -1) {
			$property_result["dimens"] = "Upon inquiry";
			$property_result["measurement"] = "";
		}

		$email_result = $collection_emails->findOne(array('managerID' => $managerID, 'type' => $type));

		$body = $email_result['messagebody'];

		$body = str_replace("{{name}}",$name, $body);
		$body = str_replace("{{customers_agent_name}}",$name, $body);
		$body = str_replace("{{client_name}}",$client_name, $body);
		$body = str_replace("{{oh_title}}",$property_result["title"], $body);
		$body = str_replace("{{oh_url}}",$property_result["url"], $body);
		$body = str_replace("{{oh_addr1}}",$property_result["addr1"], $body);
		$body = str_replace("{{oh_addr2}}",$property_result["addr2"], $body);
		$body = str_replace("{{oh_addr}}",$temp_addr, $body);
		$body = str_replace("{{oh_beds}}",$property_result["beds"], $body);
		$body = str_replace("{{oh_baths}}",$property_result["baths"], $body);
		$body = str_replace("{{oh_size}}",$property_result["dimens"]." ".$property_result["measurement"], $body);
		$body = str_replace("{{oh_price}}",$property_result["price"]." ".$property_result["currency"], $body);
		$body = str_replace("{{oh_details}}",$property_result["desc"], $body);

		if(parse_url($property_result["image"], PHP_URL_HOST) == "www.spac.io") {
			$body = str_replace("{{oh_img}}<br/>","", $body);
			$body = str_replace("{{oh_img}}","", $body);
		} else {
			$body = str_replace("{{oh_img}}","<img src='".$property_result["image"]."' width='100%'>", $body);
		}

		$body = str_replace("{{agent_phone}}",$manager_result["phone"], $body);
		$body = str_replace("{{agent_email}}",$manager_result["email"], $body);
		$body = str_replace("{{agent_title}}",$manager_result["title"], $body);
		$body = str_replace("{{agent_company}}",$manager_result["brokerage"], $body);

		date_default_timezone_set("America/New_York");
		if($timestamp == "") {
			$today = date('F j, Y');
		} else {
			$today = date('F j, Y', $timestamp);
		}

		$body = str_replace("{{today}}",$today, $body);
		$body = str_replace("{{profile_url}}","www.spac.io/profile/".$manager_result["ukey"], $body );

		if(parse_url($manager_result["pphoto"], PHP_URL_HOST) == "www.spac.io") {
			$body  = str_replace("{{agent_photo}}<br/>","", $body );
			$body  = str_replace("{{agent_photo}}<br>","", $body );
			$body  = str_replace("{{agent_photo}}","", $body );
		} else {
			$body = str_replace("{{agent_photo}}","<img src='".$manager_result["pphoto"]."' height='100'>", $body);
		}
		$body  = str_replace("{{agent_name}}",$manager_result["fname"]." ".$manager_result["lname"], $body );
		$body  = str_replace("{{agent_phone}}",$manager_result["phone"], $body );
		$body  = str_replace("{{agent_email}}",$manager_result["email"], $body );
		$body  = str_replace("{{agent_title}}",$manager_result["title"], $body );
		$body  = str_replace("{{agent_company}}",$manager_result["brokerage"], $body );

		if(array_key_exists("autoBCC", $property_result)) {
			if($property_result["autoBCC"] == "YES") {
				$recipients = array(
						array(
							'email' => $email,
							'type' => 'to'
						),
						array(
							'email' => $manager_result["email"],
							'type' => 'bcc'
						)
					);
			} else {
				$recipients = array(
						array(
							'email' => $email,
							'type' => 'to'
						)
					);
			}
		} else {
			$recipients = array(
						array(
							'email' => $email,
							'type' => 'to'
						)
					);
		}
			$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');

			//Does not belong to a brokerage
			if ($manager_result["brokerageID"] == "N/A" || $manager_result["brokerageID"] == "" ) {
				$message = array(
					'html' => '<html><body><div style="background:#f7f7f7;padding:5px;line-height:1.4;"><div style="background:white;font-size:14px;padding:20px;margin-top:0px;">'.$body.'</div><div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">You received this email because you signed in on the Spacio app at an open house.<br/>No longer want to receive these messages?<br/>You may unsubscribe <a href="http://www.spacio.com/unsubscribeme.php" style="color:#999;">here</a>.<br/>Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/></div></div></body></html>',
					'subject' => $property_result["title"],
					'from_email' => 'hello@spac.io',
					'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
					'to' => $recipients,
					'headers' => array('Reply-To' => $manager_result["email"]),
					'important' => false,
					'tags' => array('new-registrant')
				);
			  } else {
				  $brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));

				  $template = $brokerage_result["emailTemplate"];
				  $template = str_replace("{{body}}",$body, $template);

				  $message = array(
					  'html' => $template,
					  'subject' => $property_result["title"],
					  'from_email' => 'hello@spac.io',
					  'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
					  'to' => $recipients,
					  'headers' => array('Reply-To' => $manager_result["email"]),
					  'important' => false,
					  'tags' => array('new-registrant')
				  );
			  }
			  $async = true;
			  $result = $mandrill->messages->send($message, $async);
			  $result = true;


		  return true;
	}

	static function sendAutoEmails($propertyID,$managerID, $vtoken) {
		$collection = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->custom_emails;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$property_result = $collection_properties->findOne(array('propertyID' => $propertyID));
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), 'vtoken' => $vtoken));

		$count = 0;
		if (!is_null($manager_result)) {
			$cursor = $collection->find(array('propertyID' => $propertyID, 'autoEmailed' => false));
			foreach ($cursor as $doc) {
				$doc["id"] =  $doc["_id"]->{'$id'};

				if($doc["tempEmail"] != "N/A" && $doc["tempEmail"] != "") {
					$email = $doc["tempEmail"];
				} else {
					$email = $doc["email"];
				}

				$has_agent = false;
				$customer_agent_name = "";
				$customer_agent_email = "";
				foreach ($doc["answersObj"] as $answer) {
					if($answer["questionObj"]["question"] == "Are you working with an agent?")	{
						if($answer["answer"] == "YES") {
							$has_agent = true;
						}
					}
					if($answer["questionObj"]["question"] == "Agent Name?")	{
						$customer_agent_name = $answer["answer"];
					}
					if($answer["questionObj"]["question"] == "Agent Contact?")	{
						$customer_agent_email = $answer["answer"];
					}
				}


				$isBroker = false;

				if(array_key_exists("isBroker", $doc)) {
					if ($doc["isBroker"] == "YES")	{
						$isBroker = true;
					}
				}



				if ($email != "" && $email != "N/A" && $property_result["autoEmail"] == "YES" && !$has_agent && !$isBroker) {
					$result = self::sendAutoEmail($property_result,$manager_result,$email,$doc["name"],"customer");
					if($result) {
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Sent'));
						$u_result = $collection->update(array('_id' => new MongoId($doc["id"])), $newdata);
						$count++;
					} else {
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Failed'));
						$u_result = $collection->update(array('_id' => new MongoId($doc["id"])), $newdata);
					}
				} else {
					if($has_agent && $customer_agent_email != "") {
						$result = self::sendAutoEmail($property_result,$manager_result,$customer_agent_email,$customer_agent_name,"agent",$doc["name"]);
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Sent to Agent'));
					} else if($has_agent && $customer_agent_email == ""){
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Not Authorized'));
					} else if ($isBroker && $email != ""){
						$result = self::sendAutoEmail($property_result,$manager_result,$email,$doc["name"],"broker");
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Sent'));
					} else if ($email == ""){
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Not Sent. No Email.'));
					} else if($property_result["autoEmail"] == "NO") {
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Not Activated'));
					} else {
						$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Not Applicable'));
					}
					$u_result = $collection->update(array('_id' => new MongoId($doc["id"])), $newdata);
				}
			}
			$return = array("status" => 1, "count" => $count);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function addRegularRegistrant($propertyID, $ohID,$honorifics, $name,$email,$phone, $company,$anon, $answersObj, $marketsnap, $source, $managerID, $vtoken,$sendEmail,$isBroker,$cloudcma, $realscout,$moxipresent,$touchcma,$presentationpro, $note,$dateCreated) {
		$collection = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->custom_emails;

		$s_result = $collection->findOne(array('email' => $email, 'propertyID' => $propertyID));
		$property_result = $collection_properties->findOne(array('propertyID' => $propertyID));
		$pid = $property_result["_id"]->{'$id'};
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), 'vtoken' => $vtoken));


		$sendToRepresented = array_key_exists("sendToRepresented", $manager_result) ? (array)$manager_result["sendToRepresented"] : array();
	  if (false) {
			//Registrant email already exists for that specific property
			$return = array("status" => 2);
	  } else {

		  //$getDate = date('Y-m-d H:i:s');
		  //$date = new DateTime();
		  //$timeStamp = $date->getTimestamp();
		  $currentTime = new MongoDate($dateCreated["sec"]);

		  $has_agent = false;
		  $hasAgent = "UNKNOWN";
		  $hasContact = "NO";
		  $hasFinancing = "UNKNOWN";
		  $customer_agent_name = "";
		  $customer_agent_email = "";

		  //Check for Financing and Agent status
		  foreach ($answersObj as $answer) {
	      if($answer["questionObj"]["question"] == "Are you working with an agent?")	{

				  if($answer["answer"] == "YES") {
					  $has_agent = true;
					  $hasAgent = "YES";
				  } else if($answer["answer"] == "NO") {
					  $hasAgent = "NO";
				  }
		  	}
			if($answer["questionObj"]["question"] == "Agent Name?")	{
				$customer_agent_name = $answer["answer"];
			}
			if($answer["questionObj"]["question"] == "Agent Contact?")	{
				$customer_agent_email = $answer["answer"];
			}

			if($answer["questionObj"]["question"] == "Are you mortgage pre-approved?")	{
				 if($answer["answer"] == "YES") {
					 $hasFinancing = "YES";
				 } else if($answer["answer"] == "NO") {
				 	$hasFinancing = "NO";
				 }
			}
	  }
		  //Check Contact Status
		  if($email != "") {
			  $hasContact = "YES";
		  }

		  if($phone != "") {
			  $hasContact = "YES";
		  }

		  if($property_result["autoEmail"] == "YES" && !$sendEmail && !$has_agent) {
			  $autoEmailed = false;
			  $emailedStatus = "Pending";
		  } else  if($property_result["autoEmail"] == "NO"){
			  $autoEmailed = true;
			  $emailedStatus = "Not Activated";
		  } else if($has_agent){
			  $autoEmailed = false;
			  $emailedStatus = "Pending";
		  } else {
			  $autoEmailed = false;
			  $emailedStatus = "Pending";
		  }

		  $fullcontact = array();
		  //Perform Full Contact People search if user is Pro
		  if($isBroker == "NO") {

			  if($email != "" && $email != "N/A") {
				$url="https://api.fullcontact.com/v2/person.json?email=".$email."&apiKey=6d107a2726512fdf";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$url);
				$result=curl_exec($ch);
				curl_close($ch);

				$json_result = json_decode($result, true);

				//See if full contact returns a succesful result
				if ($json_result["status"] == "200" || $json_result["status"] == 200) {
					$fullcontact = $json_result;
				} else {
					$fullcontact = array("status" => 400);
				}

			  } else if($phone != "" && $phone != "N/A") {
				$url="https://api.fullcontact.com/v2/person.json?phone=".$phone."&apiKey=6d107a2726512fdf";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$url);
				$result=curl_exec($ch);
				curl_close($ch);

				$json_result = json_decode($result, true);

				//See if full contact returns a succesful result
				if ($json_result["status"] == "200" || $json_result["status"] == 200) {
					$fullcontact = $json_result;
				} else {
					$fullcontact = array("status" => 400);
				}
			  } else {
				  $fullcontact = array("status" => 0);
			  }
		  }

			$omit = "NO";

			if($property_result["sample"]) {
				$omit = "YES";
			}

		  $document_arr = array( "propertyID" => $propertyID,
		  						"pid" =>$pid,
									"ohID" => $ohID,
									"source" => $source,
		  						"managerID" => $managerID,
									"brokerageID" => $manager_result["brokerageID"],
		  						"honorifics" => $honorifics,
									"name" => $name,
									"email"=> $email,
									"phone"=> $phone,
									"company"=> $company,
									"tempEmail" => "N/A",
									"officeID" => $manager_result["officeID"],
									"isBroker" => $isBroker,
									"answersObj" => $answersObj,
									"status"=>"active",
									"autoEmailed"=>$autoEmailed,
									"emailedStatus"=>$emailedStatus,
									"hasAgent" => $hasAgent,
									"hasFinancing" => $hasFinancing,
									"hasContact" => $hasContact,
									"note" => $note,
									"omit" => $omit,
									"realscout" => $realscout,
									"cloudcma" => $cloudcma,
									"moxipresent" => $moxipresent,
									"touchcma" => $touchcma,
									"presentationpro" => $presentationpro,
									"fullcontact" => $fullcontact,
									"dateCreated" => $currentTime);

			if(array_key_exists("teamID", $manager_result) && $manager_result["teamID"] != "N/A" && $manager_result["teamID"] != "") {
				$document_arr["teamID"] = $manager_result["teamID"];
				$document_arr["managerID"] = $property_result["managerID"]; //Tean owner owns this leads
				$document_arr["collectedBy"] = $managerID;
			}

		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result = $collection->insert($document_arr);

		  if ($result) {

				$return = array("status" => 1,
					"registrantID" => $document_arr["_id"]->{'$id'}
				);

				$newID = $document_arr["_id"]->{'$id'};

				if($cloudcma == "YES") {
					sendCloudCMAReport($manager_result,$property_result,$email,$name);
				}

				if($moxipresent == "YES") {
					sendMoxiPresentPresentation($manager_result,$property_result,$email,$name);
				}

				if($touchcma == "YES") {
					sendTouchCMAPresentation($manager_result,$property_result,$email,$name);
				}

				if($presentationpro == "YES") {
					sendProPresentation($manager_result,$property_result,$email,$name);
				}

				if($sendEmail || $source == "mobile-web" || $source == "mobile" || $source == "iphone") {
				  	if ($email != "" && $email != "N/A" && $property_result["autoEmail"] == "YES" && !$has_agent) {

						$result2 = self::sendAutoEmail($property_result,$manager_result,$email,$name,"customer");
						if($result2) {
							$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Sent'));
							$u_result = $collection->update(array('_id' => new MongoId($newID)), $newdata);
						} else {
							$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Failed'));
							$u_result = $collection->update(array('_id' => new MongoId($newID)), $newdata);
						}
					} else {
						if($has_agent && $property_result["autoEmail"] == "YES") {
							if($customer_agent_email != "") {
								$result2 = self::sendAutoEmail($property_result,$manager_result,$customer_agent_email,$customer_agent_name,"agent",$name);
					  			$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Sent to Agent'));
							} else {
								$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Not Applicable'));
							}
						} else if($isBroker == "YES" && $property_result["autoEmail"] == "YES") {
							$result2 = self::sendAutoEmail($property_result,$manager_result,$email,$name,"broker");
						} else if($property_result["autoEmail"] == "NO"){
							$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Not Activated'));
						} else {
							$newdata = array('$set' => array('autoEmailed' => true,'emailedStatus'=>'Not Applicable'));
						}
						$u_result = $collection->update(array('_id' => new MongoId($newID)), $newdata);
				  }
				}

				$answers_combined = "";

				foreach ($answersObj as $answer) {
					$answers_combined = $answers_combined."".$answer["questionObj"]["question"]." ".$answer["answer"]." | ";
				}

				if ($answers_combined != "") {
					$answers_combined = trim($answers_combined);
					$answers_combined = trim($answers_combined,"|");
				}

				if(!array_key_exists("reliance",$sendToRepresented)) {
					$sendToRepresented["reliance"] = "NO";
				}

				if ($manager_result["relianceEnabled"] == "YES" && !($sendToRepresented["reliance"] == "NO" && $has_agent)) {
					$relianceResults = relianceSendLead($manager_result["relianceID"],$name,$email,$phone,$answers_combined,$manager_result["email"],$property_result["title"],$dateCreated["sec"]);
					$return["reliance"] = $relianceResults;
				}

				if(!array_key_exists("rew",$sendToRepresented)) {
					$sendToRepresented["rew"] = "NO";
				}

				if ($manager_result["rewEnabled"] == "YES" && !($sendToRepresented["rew"] == "NO" && $has_agent)) {
					$fullname = explode(" ", $name,2);
					$rewAgentID = $manager_result["rewAgentID"];
					if($rewAgentID != "" && !is_null($rewAgentID)) {
						$comments = $answers_combined;
						$comments = $comments." | Visitor registered at ".$property_result["title"];
						rewCreateLead($rewAgentID,$fullname[0],$fullname[1],$email,"","","","",$phone,$comments,$manager_result["brokerageID"]);
					}
				}

				if(!array_key_exists("moxi",$sendToRepresented)) {
					$sendToRepresented["moxi"] = "NO";
				}

				if ($manager_result["moxiEnabled"] == "YES" && !($sendToRepresented["moxi"] == "NO" && $has_agent)) {


					  $platform_identifier = "54f30116-696d-11e6-b468-0050569c119a";
					  $platform_secret = "yLqa36TCNSIR0mF6rfQZuQtt";

					  $credentials = new MoxiworksPlatform\Credentials($platform_identifier, $platform_secret);
						$prop_addr = $property_result["addr1"].' '.$property_result["addr2"];


					  // this function will create a new contact in moxiworks
					  $moxi_works_contact = MoxiworksPlatform\Contact::create([
					      'moxi_works_agent_id' => $manager_result["moxiID"],
					      'partner_contact_id' => $newID,
					      'contact_name' => $name,
					      'primary_email_address' => $email,
					      'primary_phone_number' => $phone,
					      'property_street_address' => $prop_addr,
					      'note' => $answers_combined]);
								// $moxi_works_action_log = MoxiworksPlatform\ActionLog::create([
							  //     'moxi_works_agent_id' => $moxi_works_agent_id,
							  //     'partner_contact_id' => $_id,
							  //     'title' => 'Attended an open house',
							  //     'body' =>  $fname.' attended an open house at '.$listing_address]);
					//Add these if you would like to see the data.
					     // var_dump($moxi_works_contact);
					      //var_dump($moxi_works_action_log);
								$return["moxi"] = $moxi_works_contact;

				}

				if(!array_key_exists("olr",$sendToRepresented)) {
					$sendToRepresented["olr"] = "NO";
				}

				if ($manager_result["olrEnabled"] == "YES" && !($sendToRepresented["olr"] == "NO" && $has_agent)) {

					$data = array(
						"spacioUserKey" => $manager_result["ukey"],
						"name" => $name,
						"email" => $email,
						"phone" => $phone,
						"survey" => $answers_combined,
						"source" => "Spacio");
					$data_string = json_encode($data);

					$url = "https://spacio-api.olr.com/api/customer?token=05b85c09eefb42ec82f1601e17138199";

					$ch = curl_init();

					curl_setopt($ch, CURLOPT_URL,$url);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json')
					);

					$server_output = curl_exec ($ch);
					//$return["olr"] = $server_output;
					curl_close ($ch);


			  }

				if(!array_key_exists("topproducer",$sendToRepresented)) {
					$sendToRepresented["topproducer"] = "NO";
				}

				if ($manager_result["topproducerID"] != "N/A" && $manager_result["topproducerID"] != "" && ($manager_result["marketsnapID"] == "" || $manager_result["marketsnapID"] == "N/A") && $manager_result["type"] != "freemium" && !($sendToRepresented["topproducer"] == "NO" && $has_agent)) {

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html><head><meta name="lead_information_version" content="1.0"><meta name="lead_source" content="spacio"><meta name="lead_type" content="Buyer"><meta name="lead_name" content="'.$name.'"><meta name="lead_email" content="'.$email.'"><meta name="lead_phone" content="'.$phone.'"><meta name="lead_message" content="'.$answers_combined.'"><meta name="lead_property_address" content="'.$property_result["addr1"].' '.$property_result["addr2"].'"></head><body>Lead Source:spacio<br>Lead Type:Buyer<br>Lead Name:'.$name.'<br>Lead Email:'.$email.'<br>Lead phone:'.$phone.'<br>Lead Message: '.$answers_combined.'<br>Lead Property Address: '.$property_result["addr1"].' '.$property_result["addr2"].'</body></html>',
							'subject' => $property_result["title"],
							'from_email' => 'hello@spac.io',
							'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
							'to' => array(
								array(
									'email' => $manager_result["topproducerID"]
								)
							),
							'headers' => array('Reply-To' => $manager_result["email"]),
							'important' => false,
							'tags' => array('tp-lead')
						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}

				}

				if(!array_key_exists("marketsnap",$sendToRepresented)) {
					$sendToRepresented["marketsnap"] = "NO";
				}

				if ($manager_result["marketsnapID"] != "N/A" && $manager_result["marketsnapID"] != ""  && $manager_result["type"] != "freemium" && !($sendToRepresented["marketsnap"] == "NO" && $has_agent)) {

				}

				if(!array_key_exists("renthop",$sendToRepresented)) {
					$sendToRepresented["renthop"] = "NO";
				}

				if ($manager_result["realtyjugglerID"] != "N/A" && $manager_result["realtyjugglerID"] != ""  && $manager_result["type"] != "freemium" && !($sendToRepresented["renthop"] == "NO" && $has_agent)) {


					$prop_addr = $property_result["addr1"].' '.$property_result["addr2"];


						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html><head><meta name="lead_information_version" content="1.0"><meta name="lead_source" content="spacio"><meta name="lead_type" content="Buyer"><meta name="lead_name" content="'.$name.'"><meta name="lead_email" content="'.$email.'"><meta name="lead_phone" content="'.$phone.'"><meta name="lead_message" content="'.$answers_combined.'"><meta name="lead_property_address" content="'.$prop_addr.'"></head><body>Source: Spacio<br>Lead Type: Buyer<br>Name: '.$name.'<br>Email: '.$email.'<br>Phone: '.$phone.'<br>Notes: '.$answers_combined.'<br>Address: '.$prop_addr.'</body></html>',
							'subject' => $property_result["title"],
							'from_email' => 'hello@spac.io',
							'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
							'to' => array(
								array(
									'email' => $manager_result["realtyjugglerID"]
								)
							),
							'headers' => array('Reply-To' => $manager_result["email"]),
							'important' => false,
							'tags' => array('tp-lead')
						);
						$async = true;
						$result = $mandrill->messages->send($message, $async);

				}

				if(!array_key_exists("followupboss",$sendToRepresented)) {
					$sendToRepresented["followupboss"] = "NO";
				}
				if ($manager_result["followupbossID"] != "N/A" && $manager_result["followupbossID"] != "" && !($sendToRepresented["followupboss"] == "NO" && $has_agent)) {
					$apiKey = $manager_result["followupbossID"];
					$prop_addr = $property_result["addr1"].' '.$property_result["addr2"];

					// event data
					  $data = array(
						  "source" => "Spacio",
						  "type" => "Visited Open House",
						  "message" => $answers_combined,
						  "description" => "",
						  "person" => array(
							  "name" => $name,
							  "emails" => array(array("value" => $email)),
							  "phones" => array(array("value" => $phone)),
							  "source" => "Spacio",
							  "tags" => array("Spacio"),
							  "stage" => "Lead"
						  ),
						  "property" => array(
							  "street" => $prop_addr
						  )
					  );

					  	$ch = curl_init('https://api.followupboss.com/v1/events');
	  					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	  					curl_setopt($ch, CURLOPT_USERPWD, $apiKey . ':');
	  					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	 						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	  					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

							$response_fub = curl_exec($ch);
						//	$return["FB"] = curl_exec($ch);
					//$return["FB_data"] = $data;
				}

				if(!array_key_exists("mailchimp",$sendToRepresented)) {
					$sendToRepresented["mailchimp"] = "NO";
				}

				if ($manager_result["mailchimpKey"] != "N/A" && $manager_result["mailchimpKey"] != "" && $manager_result["mailchimpUsername"] != "N/A" && $manager_result["mailchimpUsername"] != "" && !($sendToRepresented["mailchimp"] == "NO" && $has_agent)) {
					$parts = explode(" ", $name);

					$temp_lname = array_pop($parts);
					$temp_fname = implode(" ", $parts);
					$prop_addr = $property_result["addr1"].' '.$property_result["addr2"];


					$mc_result = mailchimpCreateLead($manager_result["mailchimpKey"],$manager_result["mailchimpUsername"],$manager_result["mailchimpListID"],$temp_fname,$temp_lname,$phone,$prop_addr,$answers_combined,$email);

					$msg = $mc_result["msg"];
					$return["msg"] = $msg;
					$return["hash"] = $mc_result["subscriber_hash"];

				}

				if(!array_key_exists("boomtown",$sendToRepresented)) {
					$sendToRepresented["boomtown"] = "NO";
				}

				if ( $manager_result["boomtownID"] != "N/A" && $manager_result["boomtownID"] != "" && $manager_result["type"] != "freemium" && !($sendToRepresented["boomtown"] == "NO" && $has_agent)) {

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html><head><meta name="lead_information_version" content="1.0"><meta name="lead_source" content="spacio"><meta name="lead_type" content="Buyer"><meta name="lead_name" content="'.$name.'"><meta name="lead_email" content="'.$email.'"><meta name="lead_phone" content="'.$phone.'"><meta name="lead_message" content="'.$answers_combined.'"><meta name="lead_property_address" content="'.$property_result["addr1"].' '.$property_result["addr2"].'"></head><body>Lead Source:spacio<br>Lead Type:Buyer<br>Lead Name:'.$name.'<br>Lead Email:'.$email.'<br>Lead phone:'.$phone.'<br>Lead Message: '.$answers_combined.'<br>Lead Property Address: '.$property_result["addr1"].' '.$property_result["addr2"].'</body></html>',
							'subject' => $property_result["title"],
							'from_email' => 'hello@spac.io',
							'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
							'to' => array(
								array(
									'email' => $manager_result["boomtownID"]
								)
							),
							'headers' => array('Reply-To' => $manager_result["email"]),
							'important' => false,
							'tags' => array('boomtown-lead')
						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}

				}

				if(!array_key_exists("contactually",$sendToRepresented)) {
					$sendToRepresented["contactually"] = "NO";
				}

				if ($manager_result["contactuallyID"] != "N/A" && $manager_result["contactuallyID"] != ""  && $manager_result["type"] != "freemium" && !($sendToRepresented["contactually"] == "NO" && $has_agent)) {

					$fullname = explode(" ", $name,2);
					$contactually_cid = createContactuallyLead($email,$phone,$manager_result["contactuallyBucket"],$manager_result["contactuallyID"],$answers_combined,$property_result["title"],$fullname[0],$fullname[1]);
					$return["contactually_cid"] = $contactually_cid;
				}


		  } else {
				$return = array("status" => 0);
		  }

    }
	  return $return;
	}

	static function editRegistrant($managerID,$vtoken,$registrantID, $honorifics,$name, $email, $phone,$company,$answersObj,$note) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$newdata = array('$set' => array('honorifics'=>$honorifics,'name' => $name,'email' => $email,'phone' => $phone,'company' => $company,'answersObj'=> $answersObj,'note' => $note));
			$u_result = $collection_registrants->update(array('_id' => new MongoId($registrantID)), $newdata);

			$return = array(
				"status" => 1,
			);
		} else {
			// User ID not found
			$return = array("status" => 0);
		}

		return $return;
	}

	static function broadcastMessage($managerID,$vtoken,$pid,$messageBody,$rIDs,$subject,$bcc,$test) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$property_result = $collection_properties->findOne(array('_id' => new MongoId($pid)));
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), 'vtoken' => $vtoken));

		if ($property_result["price"] == "" || $property_result["price"] == "N/A") {
			$property_result["price"] = "Upon inquiry";
			$property_result["currency"] = "";
		}

		$temp_addr = "Upon inquiry";

		if ($property_result["addr1"] == "" || $property_result["addr1"] == "N/A") {
			$property_result["addr1"] = "Upon inquiry";
		} else {
			$temp_addr = $property_result["addr1"];
		}

		if ($property_result["addr2"] == "" || $property_result["addr2"] == "N/A") {
			$property_result["addr2"] = "";
		} else {
			$temp_addr = $temp_addr.", ".$property_result["addr2"];
		}

		if ($property_result["desc"] == "" || $property_result["desc"] == "N/A") {
			$property_result["desc"] = "Upon inquiry";
		}

		if ($property_result["url"] == "" || $property_result["url"] == "N/A") {
			$property_result["url"] = "Upon inquiry";
		}

		if ($property_result["price"] == "" || $property_result["price"] == "N/A") {
			$property_result["price"] = "Upon inquiry";
		}

		if ($property_result["baths"] == -1) {
			$property_result["baths"] = "Upon inquiry";
		}

		if ($property_result["beds"] == -1) {
			$property_result["beds"] = "Upon inquiry";
		}

		if ($property_result["dimens"] == -1) {
			$property_result["dimens"] = "Upon inquiry";
			$property_result["measurement"] = "";
		}

		if (!is_null($manager_result)) {
			if ($bcc == "YES") {
				$body = $messageBody;

				//$body = str_replace("{{name}}",$registrant_result["name"], $body);
				$body = str_replace("{{oh_title}}",$property_result["title"], $body);
				$body = str_replace("{{oh_url}}",$property_result["url"], $body);
				$body = str_replace("{{oh_addr1}}",$property_result["addr1"], $body);
				$body = str_replace("{{oh_addr2}}",$property_result["addr2"], $body);
				//$body = str_replace("{{oh_addr}}",$temp_addr, $body);
				$body = str_replace("{{oh_beds}}",$property_result["beds"], $body);
				$body = str_replace("{{oh_baths}}",$property_result["baths"], $body);
				$body = str_replace("{{oh_size}}",$property_result["dimens"]." ".$property_result["measurement"], $body);
				$body = str_replace("{{oh_price}}",$property_result["price"]." ".$property_result["currency"], $body);
				$body = str_replace("{{oh_details}}",$property_result["desc"], $body);
				if(parse_url($property_result["image"], PHP_URL_HOST) == "www.spac.io") {
					$body = str_replace("{{oh_img}}<br/><br/>","", $body);
					$body = str_replace("{{oh_img}}","", $body);
				} else {
					$body = str_replace("{{oh_img}}","<img src='".$property_result["image"]."' width='100%'>", $body);
				}

				$body = str_replace("{{agent_name}}",$manager_result["fname"]." ".$manager_result["lname"], $body);
				$body = str_replace("{{agent_phone}}",$manager_result["phone"], $body);
				$body = str_replace("{{agent_email}}",$manager_result["email"], $body);
				$body = str_replace("{{agent_title}}",$manager_result["title"], $body);
				$body = str_replace("{{agent_company}}",$manager_result["brokerage"], $body);
				$body = str_replace("{{today}}",date('F j, Y'), $body);
				$body = str_replace("{{profile_url}}","www.spac.io/profile/".$manager_result["ukey"], $body);
				$body = str_replace("{{agent_photo}}","<img src='".$manager_result["pphoto"]."' height='100'>", $body);

				$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');

				if($test == "YES") {
					$new_subject = "Test Email: ".$subject;
				} else {
					$new_subject = "BCC copy: ".$subject;
				}

				if ($manager_result["brokerageID"] == "N/A" || $manager_result["brokerageID"] == "" ) {
					$message = array(
						'html' => '<html><body><div style="background:#f7f7f7;padding:5px;"><div style="background:white;font-size:14px;padding:20px;margin-top:20px;">'.$body.'</div><div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">You received this email because you signed in on the Spacio app at an open house.<br/>No longer want to receive these messages?<br/>You may unsubscribe <a href="http://www.spac.io/unsubscribeme.php" style="color:#999;">here</a>.<br/>Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/></div></div></body></html>',
						'subject' => $new_subject,
						'from_email' => 'hello@spac.io',
						'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
						'to' => array(
							array(
								'email' => $manager_result["email"],
								'type' => 'to'
							)
						),
						"preserve_recipients" => false,
						'headers' => array('Reply-To' => $manager_result["email"]),
						'important' => false,
						'tags' => array('broadcast-message-bcc')
					);
				} else {
					 $brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));

						$template = $brokerage_result["emailTemplate"];
						$template = str_replace("{{body}}",$body, $template);
						$template = str_replace("{{signature}}","", $template);

						$message = array(
							'html' => $template,
							'subject' => "BCC copy: ".$subject,
							'from_email' => 'hello@spac.io',
							'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
							'to' => array(
								array(
									'email' => $manager_result["email"],
				          'type' => 'to'
								)
							),
							"preserve_recipients" => false,
							'headers' => array('Reply-To' => $manager_result["email"]),
							'important' => false,
							'tags' => array('broadcast-message-brokerage-bcc')
						);
					}
					$async = true;
					$result = $mandrill->messages->send($message, $async);
			}

			foreach ($rIDs as $rid) {
				$registrant_result = $collection_registrants->findOne(array('_id' => new MongoId($rid)));

				if(!is_null($registrant_result)) {

					$email = $registrant_result["email"];
					$unadjusted_timestamp = $registrant_result["dateCreated"]->sec;
					$offset = $manager_result["timezoneOffset"] * 60;
					$adjusted_timestamp = $unadjusted_timestamp - $offset;
					$dateCreated = date('F j, Y', $adjusted_timestamp);

					$body = $messageBody;

					$body = str_replace("{{name}}",$registrant_result["name"], $body);
					$body = str_replace("{{oh_title}}",$property_result["title"], $body);
					$body = str_replace("{{oh_url}}",$property_result["url"], $body);
					$body = str_replace("{{oh_addr1}}",$property_result["addr1"], $body);
					$body = str_replace("{{oh_addr2}}",$property_result["addr2"], $body);
					//$body = str_replace("{{oh_addr}}",$temp_addr, $body);
					$body = str_replace("{{oh_beds}}",$property_result["beds"], $body);
					$body = str_replace("{{oh_baths}}",$property_result["baths"], $body);
					$body = str_replace("{{oh_size}}",$property_result["dimens"]." ".$property_result["measurement"], $body);
					$body = str_replace("{{oh_price}}",$property_result["price"]." ".$property_result["currency"], $body);
					$body = str_replace("{{oh_details}}",$property_result["desc"], $body);

					if(parse_url($property_result["image"], PHP_URL_HOST) == "www.spac.io") {
						$body = str_replace("{{oh_img}}<br/><br/>","", $body);
						$body = str_replace("{{oh_img}}","", $body);
					} else {
						$body = str_replace("{{oh_img}}","<img src='".$property_result["image"]."' width='100%'>", $body);
					}

					$body = str_replace("{{agent_name}}",$manager_result["fname"]." ".$manager_result["lname"], $body);
					$body = str_replace("{{agent_phone}}",$manager_result["phone"], $body);
					$body = str_replace("{{agent_email}}",$manager_result["email"], $body);
					$body = str_replace("{{agent_title}}",$manager_result["title"], $body);
					$body = str_replace("{{agent_company}}",$manager_result["brokerage"], $body);
					$body = str_replace("{{today}}",$dateCreated, $body);
					$body = str_replace("{{profile_url}}","www.spac.io/profile/".$manager_result["ukey"], $body);
					$body = str_replace("{{agent_photo}}","<img src='".$manager_result["pphoto"]."' height='100'>", $body);

					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					if ($manager_result["brokerageID"] == "N/A" || $manager_result["brokerageID"] == "" ) {
						$message = array(
							'html' => '<html><body><div style="background:#f7f7f7;padding:5px;"><div style="background:white;font-size:14px;padding:20px;margin-top:20px;">'.$body.'</div><div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">You received this email because you signed in on the Spacio app at an open house.<br/>No longer want to receive these messages?<br/>You may unsubscribe <a href="http://www.spac.io/unsubscribeme.php" style="color:#999;">here</a>.<br/>Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/></div></div></body></html>',
							'subject' => $subject,
							'from_email' => 'hello@spac.io',
							'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
							'to' => array(
								array(
									'email' => $email,
									'type' => 'to'
								)
							),
							"preserve_recipients" => false,
							'headers' => array('Reply-To' => $manager_result["email"]),
							'important' => false,
							'tags' => array('broadcast-message')
						);
					} else {
						 $brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));

							$template = $brokerage_result["emailTemplate"];
							$template = str_replace("{{body}}",$body, $template);
							$template = str_replace("{{signature}}","", $template);

							$message = array(
								'html' => $template,
								'subject' => $subject,
								'from_email' => 'hello@spac.io',
								'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
								'to' => array(
									array(
										'email' => $email,
					          'type' => 'to'
									)
								),
								"preserve_recipients" => false,
								'headers' => array('Reply-To' => $manager_result["email"]),
								'important' => false,
								'tags' => array('broadcast-message-brokerage')
							);
						}
						$async = true;
						if($email != "") {
							$result = $mandrill->messages->send($message, $async);
						}
					}
				}

			$return = array("status" => 1);
		} else {
			// User ID not found
			$return = array("status" => 0);
		}
		return $return;
	}

	static function createCSV($propertyID, $managerID, $vtoken, $offset, $type) {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		if($propertyID != "all") {
			$property_result = $collection_properties->findOne(array('propertyID' => $propertyID));
		} else {
			$property_result = array("title" => "All Properties");
		}

		if (!is_null($manager_result)) {
			if($propertyID != "all") {
				$cursor = $collection_registrants->find(array('propertyID' => $propertyID, 'managerID' => $managerID));
			} else {
				$cursor = $collection_registrants->find(array('managerID' => $managerID));
			}

			date_default_timezone_set('America/Los_Angeles');
			$date = date('m-d-Y_h:i:s');

			$title = array();

			if($type == "buyers") {
				$filename = 'registrants_'.$date.'_'.$propertyID.'.csv';
				$fp = fopen('../../csv/'.$filename, 'w');

				array_push($title, "Name");
				array_push($title, "Email");
				array_push($title, "Phone");
				array_push($title, "Question 1");
				array_push($title, "Answer 1");
				array_push($title, "Question 2");
				array_push($title, "Answer 2");
				array_push($title, "Question 3");
				array_push($title, "Answer 3");
				array_push($title, "Question 4");
				array_push($title, "Answer 4");
				array_push($title, "Question 5");
				array_push($title, "Answer 5");
				array_push($title, "Question 6");
				array_push($title, "Answer 6");
				array_push($title, "Question 7");
				array_push($title, "Answer 7");
				array_push($title, "Question 8");
				array_push($title, "Answer 8");
				array_push($title, "Question 9");
				array_push($title, "Answer 9");
				array_push($title, "Note");
				array_push($title, "Date");
				array_push($title, "Time");
			} else if($type == "brokers") {
				$filename = 'registrants_'.$date.'_'.$propertyID.'.csv';
				$fp = fopen('../../csv/'.$filename, 'w');

				array_push($title, "Name");
				array_push($title, "Email");
				array_push($title, "Phone");
				array_push($title, "Company");
				array_push($title, "Has Client");
				array_push($title, "Note");
				array_push($title, "Date");
				array_push($title, "Time");
			}

			fputcsv($fp, $title);

			foreach ($cursor as $doc) {


				if($type == "brokers" && $doc["isBroker"] == "YES") {
					$registrant = array();
					array_push($registrant, $doc["name"]);
					if ($doc["tempEmail"] != "N/A") {
						array_push($registrant, $doc["tempEmail"]);
					} else {
						array_push($registrant, $doc["email"]);
					}
					array_push($registrant, $doc["phone"]);
					array_push($registrant, $doc["company"]);

					if($doc["answers"][0]["answer"] == "") {
						array_push($registrant, "Unknown");
					} else {
						array_push($registrant, $doc["answers"][0]["answer"]);
					}
					array_push($registrant, $doc["note"]);

					$unadjusted_timestamp = $doc["dateCreated"]->sec;
					$adjusted_timestamp = $unadjusted_timestamp - $offset;

					$dateCreated = date('Y-M-d', $adjusted_timestamp);
					array_push($registrant, $dateCreated);
					$timeCreated = date('h:i:s', $adjusted_timestamp);
					array_push($registrant, $timeCreated);
					fputcsv($fp, $registrant);
				} else if ($type == "buyers" && $doc["isBroker"] != "YES") {
					$registrant = array();
					array_push($registrant, $doc["name"]);
					if ($doc["tempEmail"] != "N/A") {
						array_push($registrant, $doc["tempEmail"]);
					} else {
						array_push($registrant, $doc["email"]);
					}
					array_push($registrant, $doc["phone"]);
					foreach ($doc["answers"] as $qa) {
						array_push($registrant, $qa["question"]);
						array_push($registrant, $qa["answer"]);
					}

					$num_blanks = 9-count($doc["answers"]) ;

					for ($i = 0; $i < $num_blanks; $i++) {
						array_push($registrant, "");
						array_push($registrant, "");
					}

					array_push($registrant, $doc["note"]);

					$unadjusted_timestamp = $doc["dateCreated"]->sec;
					$adjusted_timestamp = $unadjusted_timestamp - $offset;

					$dateCreated = date('Y-M-d', $adjusted_timestamp);
					array_push($registrant, $dateCreated);
					$timeCreated = date('h:i:s', $adjusted_timestamp);
					array_push($registrant, $timeCreated);
					fputcsv($fp, $registrant);

				}


			}
			 chmod('../../csv/'.$filename, 0777);
			fclose($fp);

			$f = fopen("../../csv/".$filename, "r");
			$fr = fread($f, filesize("../../csv/".$filename));
			$content = chunk_split(base64_encode($fr ));
			fclose($f);

			try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html><body><div style="background:#f7f7f7;padding:20px;"><div style="background:white;font-size:14px;padding:20px;margin-top:20px;">Please find attached the exported registrant list for "'.$property_result["title"].'".</div></div></body></html>',
							'subject' => $property_result["title"].' Registrant List',
							'from_email' => 'hello@spac.io',
							'from_name' => "Spacio",
							'to' => array(
								array(
									'email' => $manager_result["email"]
								)
							),
							'attachments'=> array(
								array(
									"type"=> "text/csv",
									"name"=> $filename,
									"content"=> $content
								)
							),
							"preserve_recipients" => false,
							'headers' => array('Reply-To' => "hello@spac.io"),
							'important' => false,
							'tags' => array('csv-email')
						);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}


			$return = array("status" => 1,"filename"=>$filename,"fp"=>error_get_last());
		} else {
			$return = array("status" => 0);
		}
		return $return;

	}

}

?>
