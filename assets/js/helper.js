$('#reqDemo').validator();
  $("#reqDemo").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
      $('#reqDemo').validator('validate');
    } else {
    event.preventDefault();
      $.ajax({
        type: "POST",
        url: "/require/process.php",
        data: $('form#reqDemo').serialize(),
        success: function(msg){
            $("#request-demo").modal('hide');
            notie.alert(1, 'Thanks! We got your request and will be in touch shortly.', 6);
        },
        error: function(){
            notie.alert(3, 'Looks like something went wrong, please try again later.', 6);
        }
    });
  }
  });
