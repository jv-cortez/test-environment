<?php

require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/stripe-php-3.14.3/init.php');



	/* ------------------------------ SHARED  FUNCTIONS ------------------------------*/

	function syncFromBank($agent, $brokerageID) {
		$collection_listings_bank = Db_Conn::getInstance()->getConnection()->listings_bank;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

		$manager_result = $collection_managers->findOne(array('email' => $agent));

		$count = 0;

		if (!is_null($manager_result)) {
			$managerID = $manager_result["_id"]->{'$id'};
			$listings_result = $collection_listings_bank->find(array('agent' => $agent));

			$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerageID));

			$date = new DateTime();
			$timeStamp = $date->getTimestamp();
			$currentTime = new MongoDate($timeStamp);

			foreach ($listings_result as $listing) {
				$id2 = $listing["id2"];
				$property_result = $collection_properties->findOne(array('managerID' => $managerID, "id2" => $id2));

				if (is_null($property_result)) {
					$nextseq = $collection_counter->findAndModify(
						 array("desc" => "propertyID"),
						 array('$inc' => array('maxCount' => 1)),
						 null,
						 array(
							"new" => true
						)
					);

					$document_property = array(
						"propertyID" => $nextseq["maxCount"],
						"teamID" => "N/A",
						"url" => $listing["url"],
						"title"=> $listing["title"],
						"addr1"=> $listing["addr1"],
						"addr2"=> $listing["addr2"],
						"image"=> $listing["image"],
						"price" => $listing["price"],
						"beds" => $listing["beds"],
						"baths" => $listing["baths"],
						"dimens" => $listing["dimens"],
						"desc" => "",
						"currency" => "USD",
						"measurement" => "sqft",
						"dateCreated" => $currentTime,
						"lastEdited" => $currentTime,
						"questions" => $brokerage_result["questions"],
						"managerID"=>$managerID,
						"brokerageID"=>$brokerageID,
						"officeID"=>$manager_result["officeID"],
						"anon"=>"NO",
						"autoEmail" => "YES",
						"cnEnabled" => "NO",
						"id2" => $listing["id2"],
						"sample" => false,
						"editable"=>true);

					$result = $collection_properties->insert($document_property);

					$count ++;
				} else {
					//update listing?
					$newdata = array(
						'$set' => array(
						"url" => $listing["url"],
						"title"=> $listing["title"],
						"addr1"=> $listing["addr1"],
						"addr2"=> $listing["addr2"],
						"image"=> $listing["image"],
						"price" => $listing["price"],
						"beds" => $listing["beds"],
						"baths" => $listing["baths"],
						"dimens" => $listing["dimens"],
						"lastEdited" => $currentTime,
						"officeID"=>$manager_result["officeID"],
						"editable"=>true));
					$u_result = $collection_properties->update(array('_id' => $property_result["_id"]), $newdata);
				}

			}

			return $count;
		} else {
			return -1;
		}

	}
	function createEmailTemplate($managerID, $brokerageID) {

		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->email_templates;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID)));

		if ($brokerageID == "N/A" || $brokerageID == "") {

			$customer = '{{name}},<br/><Br/>
Thank you for taking the time to attend the Open House for {{oh_addr1}} on {{today}}. If you have any remaining questions regarding this property or any other property, please feel free to contact me at {{agent_phone}} or by email at {{agent_email}}.<Br/><Br/>
Below you will find more information on the property.<br/><Br/>
{{oh_img}}<br/><br/>
Bedrooms: {{oh_beds}}<Br/>
Bathrooms: {{oh_baths}}<Br/>
Price: {{oh_price}}<Br/>
Details: {{oh_details}}<Br/>
More Information: {{oh_url}}<Br/><Br/>
If this particular property did not meet all of the criteria you are looking for, I am confident that I can help you locate your ideal home that meets your criteria within your designated price range to review. <Br/><Br/>
Look forward to hearing from you.<Br/><Br/><Br/>
Sincerely,<Br/><Br/>
{{agent_name}}<Br/>
{{agent_email}}<Br/>
{{agent_phone}}<br/>';

			$agent = "{{customers_agent_name}},<Br/><Br/>
I'd like to let you know that your client, {{client_name}}, attended my Open House for {{oh_addr1}} on {{today}}. If you have any questions regarding this property, please feel free to contact me at {{agent_phone}} or by email at {{agent_email}}.<Br/><Br/>
Below you will find more information on the property.<Br/><Br/>
{{oh_img}}<br/><br/>
Bedrooms: {{oh_beds}}<Br/>
Bathrooms: {{oh_baths}}<Br/>
Price: {{oh_price}}<Br/>
Details: {{oh_details}}<Br/>
More Information: {{oh_url}}<Br/><Br/>
If your client is interested in this property, I'd love to speak with you. Please let me know.<Br/><Br/>
Look forward to hearing from you.<Br/><Br/><Br/>
Sincerely,<Br/><Br/>
{{agent_name}}<Br/>
{{agent_email}}<Br/>
{{agent_phone}}<br/>";

			$broker = "{{name}},<Br/><Br/>
Thank you for taking the time to attend the Brokers Open House for {{oh_addr1}} on {{today}}. If you have any remaining questions regarding this property or any other property, please feel free to contact me at {{agent_phone}} or by email at {{agent_email}}.<Br/><Br/>
Below you will find more information on the property.<Br/><Br/>
{{oh_img}}<br/><br/>
Bedrooms: {{oh_beds}}<Br/>
Bathrooms: {{oh_baths}}<Br/>
Price: {{oh_price}}<Br/>
Details: {{oh_details}}<Br/>
More Information: {{oh_url}}<Br/><Br/>
If you have a client interested in this property, I'd love to speak with you. Please let me know.<Br/><Br/>
Look forward to hearing from you.<Br/><Br/><Br/>
Sincerely,<Br/><Br/>
{{agent_name}}<Br/>
{{agent_email}}<Br/>
{{agent_phone}}<br/>";

			$customer_template = array(
					"managerID" => $managerID,
					"messagebody" => $customer,
					"bccself" => "NO",
					"replyto" => $manager_result["email"],
					"type" => "customer",
					"dateCreated" => $currentTime,
					"lastModified" => $currentTime
				);

				$result = $collection_emails->insert($customer_template);

				$agent_template = array(
					"managerID" => $managerID,
					"messagebody" => $agent,
					"bccself" => "NO",
					"replyto" => $manager_result["email"],
					"type" => "agent",
					"dateCreated" => $currentTime,
					"lastModified" => $currentTime
				);

				$result = $collection_emails->insert($agent_template);

				$broker_template = array(
					"managerID" => $managerID,
					"messagebody" => $broker,
					"bccself" => "NO",
					"replyto" => $manager_result["email"],
					"type" => "broker",
					"dateCreated" => $currentTime,
					"lastModified" => $currentTime
				);

				$result = $collection_emails->insert($broker_template);

			return true;

		  } else {
			  $brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerageID));
		  	  $customer= $brokerage_result["customerMessageBody"];
			  $agent = $brokerage_result["agentMessageBody"];
			  $broker = $brokerage_result["brokerMessageBody"];

				$customer_template = array(
					"managerID" => $managerID,
					"messagebody" => $customer,
					"bccself" => "NO",
					"replyto" => $manager_result["email"],
					"type" => "customer",
					"dateCreated" => $currentTime,
					"lastModified" => $currentTime
				);

				$result = $collection_emails->insert($customer_template);

				$agent_template = array(
					"managerID" => $managerID,
					"messagebody" => $agent,
					"bccself" => "NO",
					"replyto" => $manager_result["email"],
					"type" => "agent",
					"dateCreated" => $currentTime,
					"lastModified" => $currentTime
				);

				$result = $collection_emails->insert($agent_template);

				$broker_template = array(
					"managerID" => $managerID,
					"messagebody" => $broker,
					"bccself" => "NO",
					"replyto" => $manager_result["email"],
					"type" => "broker",
					"dateCreated" => $currentTime,
					"lastModified" => $currentTime
				);

				$result = $collection_emails->insert($broker_template);




			  return true;
		  }

	}


	function createSampleProperty($managerID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

		$nextseq = $collection_counter->findAndModify(
			 array("desc" => "propertyID"),
			 array('$inc' => array('maxCount' => 1)),
			 null,
			 array(
				"new" => true
			)
		);

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);

		$sample_property = array(
									"propertyID" => $nextseq["maxCount"],
									"url" => "http://www.spac.io",
									"title"=> "SAMPLE: Beautiful Home, Move in Ready",
									"addr1"=> "123 Central Park, New York, NY",
									"addr2"=> "",
									"image"=> "https://s3.amazonaws.com/spacio-user-images/spaciopro_sample.jpg",
									"price" => "2,500,000",
									"beds" => 2,
									"baths" => 2,
									"dimens" => 2000,
									"desc" => "SAMPLE: Beautiful home next to Central Park. Move in ready. Rare opportunity, contact me now for more details.\r\n\r\n*This is a sample open house entry only. Information listed here is not accurate, this is not an offering for sale.",
									//"note" => $note,
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"questions" => array(
										array(
											"question"=>"Are you working with an agent?",
											"type"=>"eq1"
										)
									),
									"managerID"=>$managerID,
									"brokerageID"=>"N/A",
									"anon"=>"NO",
									"autoEmail" => "NO",
									"cnEnabled" => "NO",
									"disabled" => "NO",
									"editable" => true,
									"sample" => true);

		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result = $collection_properties->insert($sample_property);

		  if($result) {
		  	return true;
		  } else {
		  	return false;
		  }
	}

	function getBHSListings($email, $managerID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

		$url="http://media.bhsusa.com/xml/bhs-spacio.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);
		$json = json_encode($xml);
		$json_array  = json_decode($json, true);

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "bhs"));

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);

		$count = 0;
		foreach ($json_array["Listing"] as $listing) {

			$addr = $listing["PropertyAddress"];
			$addr_expl = explode(",", $addr);

			if ($addr_expl[1] != "" && $addr_expl[1] != " ") {
				$addr1 = $addr_expl[0].", ".$addr_expl[1];
			} else {
				$addr1 = $addr_expl[0];
			}

			$addr2 = $addr_expl[2].", ".$addr_expl[3];

			$price = $listing["PropertyPrice"];
			$beds = (float)$listing["PropertyBedrooms"];
			$baths= (float)$listing["PropertyBathrooms"];
			if (is_string($listing["PropertySize"])) {
				$size = (int)$listing["PropertySize"];
			} else {
				$size = -1;
			}
			$url = $listing["PropertyURL"];
			$image = $listing["PictureUrl"];
			if(is_array($image)){
				$num = rand ( 1 , 10 );
				$image = "http://www.spac.io/assets/img/stock/".$num.".jpg";
			};
			$agentemail = $listing["AgentEmail"];
			$agentemail = strtolower($agentemail);

			if (isset($listing["AgentEmail2"])) {
				$agentemail2 = $listing["AgentEmail2"];
				$agentemail2 = strtolower($agentemail2);
			} else {
				$agentemail2 = "";
			}

			if (isset($listing["AgentEmail3"])) {
				$agentemail3 = $listing["AgentEmail3"];
				$agentemail3 = strtolower($agentemail3);
			} else {
				$agentemail3 = "";
			}

			if (isset($listing["AgentEmail4"])) {
				$agentemail4 = $listing["AgentEmail4"];
				$agentemail4 = strtolower($agentemail4);
			} else {
				$agentemail4 = "";
			}

			if ($agentemail == $email || $agentemail2 == $email || $agentemail3 == $email || $agentemail4 == $email) {
				 $count++;
				  $date = new DateTime();
				  $timeStamp = $date->getTimestamp();
				  $currentTime = new MongoDate($timeStamp);

				  $nextseq = $collection_counter->findAndModify(
					  array("desc" => "propertyID"),
					  array('$inc' => array('maxCount' => 1)),
					  null,
					  array(
						  "new" => true
					  )
				  );

				  $document_property = array(
					  "propertyID" => $nextseq["maxCount"],
					  "teamID" => "N/A",
					  "url" => $url,
					  "title"=> $addr1,
					  "addr1"=> $addr,
					  "addr2"=> "",
					  "image"=> $image,
					  "price" => $price,
					  "beds" => $beds,
					  "baths" => $baths,
					  "dimens" => $size,
					  "desc" => "",
					  //"note" => $note,
					  "currency" => "USD",
					  "measurement" => "sqft",
					  "dateCreated" => $currentTime,
					  "lastEdited" => $currentTime,
					  "questions" => $brokerage_result["questions"],
					  "managerID"=>$managerID,
					  "brokerageID"=>"bhs",
					  "anon"=>"NO",
					  "autoEmail" => "YES",
					  "cnEnabled" => "NO",
					  "editable"=>false,
					  "sample"=>false);

				$result = $collection_properties->insert($document_property);

			}
		}

		return $count;

	}

	function getHalsteadListings($email, $managerID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

		$url="http://media.halstead.com/xml/halstead-spacio.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);
		$json = json_encode($xml);
		$json_array  = json_decode($json, true);

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "halstead"));

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);

		$count = 0;
		foreach ($json_array["Listing"] as $listing) {

			$addr = $listing["PropertyAddress"];
			$addr_expl = explode(",", $addr);

			if ($addr_expl[1] != "" && $addr_expl[1] != " ") {
				$addr1 = $addr_expl[0].", ".$addr_expl[1];
			} else {
				$addr1 = $addr_expl[0];
			}

			$addr2 = $addr_expl[2].", ".$addr_expl[3];

			$price = $listing["PropertyPrice"];
			$beds = (float)$listing["PropertyBedrooms"];
			$baths= (float)$listing["PropertyBathrooms"];
			if (is_string($listing["PropertySize"])) {
				$size = (int)$listing["PropertySize"];
			} else {
				$size = -1;
			}
			$url = $listing["PropertyURL"];
			$image = $listing["PictureUrl"];
			if(is_array($image)){
				$num = rand ( 1 , 10 );
				$image = "http://www.spac.io/assets/img/stock/".$num.".jpg";
			};
			$agentemail = $listing["AgentEmail"];
			$agentemail = strtolower($agentemail);

			if (isset($listing["AgentEmail2"])) {
				$agentemail2 = $listing["AgentEmail2"];
				$agentemail2 = strtolower($agentemail2);
			} else {
				$agentemail2 = "";
			}

			if (isset($listing["AgentEmail3"])) {
				$agentemail3 = $listing["AgentEmail3"];
				$agentemail3 = strtolower($agentemail3);
			} else {
				$agentemail3 = "";
			}

			if (isset($listing["AgentEmail4"])) {
				$agentemail4 = $listing["AgentEmail4"];
				$agentemail4 = strtolower($agentemail4);
			} else {
				$agentemail4 = "";
			}

			if ($agentemail == $email || $agentemail2 == $email || $agentemail3 == $email || $agentemail4 == $email) {
				 	$count++;
				  $date = new DateTime();
				  $timeStamp = $date->getTimestamp();
				  $currentTime = new MongoDate($timeStamp);

				  $nextseq = $collection_counter->findAndModify(
					  array("desc" => "propertyID"),
					  array('$inc' => array('maxCount' => 1)),
					  null,
					  array(
						  "new" => true
					  )
				  );

				  $document_property = array(
					  "propertyID" => $nextseq["maxCount"],
					  "teamID" => "N/A",
					  "url" => $url,
					  "title"=> $addr1,
					  "addr1"=> $addr,
					  "addr2"=> "",
					  "image"=> $image,
					  "price" => $price,
					  "beds" => $beds,
					  "baths" => $baths,
					  "dimens" => $size,
					  "desc" => "",
					  //"note" => $note,
					  "currency" => "USD",
					  "measurement" => "sqft",
					  "dateCreated" => $currentTime,
					  "lastEdited" => $currentTime,
					  "questions" => $brokerage_result["questions"],
					  "managerID"=>$managerID,
					  "brokerageID"=>"halstead",
					  "anon"=>"NO",
					  "autoEmail" => "YES",
					  "cnEnabled" => "NO",
					  "editable"=>false,
					  "sample"=>false);

				$result = $collection_properties->insert($document_property);

			}
		}

		return $count;
	}

	function getCoreListings($email) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "corenyc"));

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);

		$count = 0;

		$row = 1;
		if (($handle = fopen("../../feeds/corenyc/corenew.txt", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 0, "|")) !== FALSE) {
				$num = count($data);

				if ($row != 1) {
				  for ($c=0; $c < $num; $c++) {
					  if ($c == 0) {
						  $id = $data[$c];
					  }
					  if ($c == 4) {
						  $date = $data[$c];
					  }
					  if ($c == 6) {
						  $addr1 = $data[$c];
					  }
					  if ($c == 9) {
						  $city = $data[$c];
					  }
					  if ($c == 12) {
						  $state = $data[$c];
					  }
					  if ($c == 13) {
						  $zip = $data[$c];
					  }
					  if ($c == 18 ) {
						  $price = (string)$data[$c];
					  }
					  if ($c == 22) {
						  $beds = (int)$data[$c];
					  }
					  if ($c == 23) {
						  $baths_full = (float)$data[$c];
					  }
					  if ($c == 24) {
						  $baths_partial = (float)$data[$c];
					  }
					  if ($c == 30) {
						  $size = (int)$data[$c];
					  }
					  if ($c == 47) {
						  $image = $data[$c];
					  }
					  if ($c == 75) {
						  $url = $data[$c];
					  }
					  if ($c == 79) {
						  $agent1 = $data[$c];
					  }
					  if ($c == 80) {
						  $agent_email1 = $data[$c];
					  }
					  if ($c == 85) {
						  $agent2 = $data[$c];
					  }
					  if ($c == 86) {
						  $agent_email2 = $data[$c];
					  }

				  }


				$addr2 = $city.", ".$state." ".$zip;
				$baths=$baths_full;
				if ($baths_partial == 1) {
					$baths = $baths + 0.5;
				}

				if($agent_email1 == $email || $agent_email2 == $email ) {
					$count++;
					$date = new DateTime();
					$timeStamp = $date->getTimestamp();
					$currentTime = new MongoDate($timeStamp);

					$nextseq = $collection_counter->findAndModify(
									array("desc" => "propertyID"),
									array('$inc' => array('maxCount' => 1)),
									null,
									array(
										"new" => true
									)
								);

					$document_property = array(
										"teamID" => "N/A",
										"propertyID" => $nextseq["maxCount"],
										"url" => $url,
										"title"=> $addr1,
										"addr1"=> $addr1,
										"addr2"=> $addr2,
										"image"=> $image,
										"price" => $price,
										"beds" => $beds,
										"baths" => $baths,
										"dimens" => $size,
										"desc" => "",
										"currency" => "USD",
										"measurement" => "sqft",
										"dateCreated" => $currentTime,
										"lastEdited" => $currentTime,
										"questions" => $brokerage_result["questions"],
										"managerID"=>$managerID,
										"brokerageID"=>"corenyc",
										"anon"=>"NO",
										"autoEmail" => "NO",
										"cnEnabled" => "NO",
										"editable"=>true,
										"sample"=>false);
						$result = $collection_properties->insert($document_property);
					}
				}
				$row++;
			}
			fclose($handle);
		}

		return $count;
	}

	function getMdrnListings($email, $managerID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_realtymx = Db_Conn::getInstance()->getConnection()->realtymx;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "mdrn"));

		$url="http://mdrnresidential.com/admin/tools/spacio.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);
		$json = json_encode($xml);
		$json_array  = json_decode($json, true);

		$count = 0;
		foreach ($xml as $properties) {

			foreach ($properties as $property) {
				$url = $property->attributes()->url."";
				$id = $property->attributes()->id."";
				$addr = $property->location->address."";
				$apartment = $property->location->apartment."";
				$addr1 = $addr.", ".$apartment;
				$city = $property->location->city."";
				$state = $property->location->state."";
				$zipcode = $property->location->zipcode."";
				$addr2 = $city.", ".$state." ".$zipcode;

				$price = $property->details->price."";
				$beds = $property->details->bedrooms."";
				$baths= $property->details->bathrooms."";
				$size = $property->details->squareFeet."";

				if(isset($property->media->picture[0])) {
					$image = $property->media->picture[0]->attributes()->url."";
				} else {
					$num = rand ( 1 , 10 );
					$image = "http://www.spac.io/assets/img/stock/".$num.".jpg";
				}

				$agents = $property->agents->children();
				$agent_emails = array();
				foreach($agents as $agent) {
					$agent_emails[] = $agent->email."";
				}

				foreach($agent_emails as $agent_email) {

					if ($email == $agent_email) {
						$count++;
						$date = new DateTime();
						$timeStamp = $date->getTimestamp();
						$currentTime = new MongoDate($timeStamp);

						$nextseq = $collection_counter->findAndModify(
							array("desc" => "propertyID"),
							array('$inc' => array('maxCount' => 1)),
							null,
							array(
								"new" => true
							)
						);

						$document_property = array(
										"propertyID" => $nextseq["maxCount"],
										"teamID" => "N/A",
										"url" => $url,
										"title"=> $addr1,
										"addr1"=> $addr1,
										"addr2"=> $addr2,
										"image"=> $image,
										"price" => $price,
										"beds" => $beds,
										"baths" => $baths,
										"dimens" => $size,
										"desc" => "",
										//"note" => $note,
										"currency" => "USD",
										"measurement" => "sqft",
										"dateCreated" => $currentTime,
										"lastEdited" => $currentTime,
										"questions" => $brokerage_result["questions"],
										"managerID"=>$managerID,
										"brokerageID"=>"mdrn",
										"anon"=>"NO",
										"autoEmail" => "YES",
										"cnEnabled" => "NO",
										"editable"=>false);

						$result = $collection_properties->insert($document_property);

						$document_realtymx = array(
							"url" => $url,
							"id" => $id,
							"addr" => $addr1.", ".$addr2,
							"brokerageID" => "mdrn",
							"agent"=>$email,
							"datePulled" => $currentTime
						);

						$result = $collection_realtymx->insert($document_realtymx);
					}
				}
			}
		}

		return $count;
	}

	function getSpireListings($email, $managerID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_realtymx = Db_Conn::getInstance()->getConnection()->realtymx;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "spire"));

		$url="http://mdrnresidential.com/admin/tools/spacio.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);
		$json = json_encode($xml);
		$json_array  = json_decode($json, true);

		$count = 0;
		foreach ($xml as $properties) {

			foreach ($properties as $property) {
				$url = $property->attributes()->url."";
				$id = $property->attributes()->id."";
				$addr = $property->location->address."";
				$apartment = $property->location->apartment."";
				$addr1 = $addr.", ".$apartment;
				$city = $property->location->city."";
				$state = $property->location->state."";
				$zipcode = $property->location->zipcode."";
				$addr2 = $city.", ".$state." ".$zipcode;

				$price = $property->details->price."";
				$beds = $property->details->bedrooms."";
				$baths= $property->details->bathrooms."";
				$size = $property->details->squareFeet."";

				if(isset($property->media->picture[0])) {
					$image = $property->media->picture[0]->attributes()->url."";
				} else {
					$num = rand ( 1 , 10 );
					$image = "http://www.spac.io/assets/img/stock/".$num.".jpg";
				}

				$agents = $property->agents->children();
				$agent_emails = array();
				foreach($agents as $agent) {
					$agent_emails[] = $agent->email."";
				}

				foreach($agent_emails as $agent_email) {

					if ($email == $agent_email) {
						$count++;
						$date = new DateTime();
						$timeStamp = $date->getTimestamp();
						$currentTime = new MongoDate($timeStamp);

						$nextseq = $collection_counter->findAndModify(
							array("desc" => "propertyID"),
							array('$inc' => array('maxCount' => 1)),
							null,
							array(
								"new" => true
							)
						);

						$document_property = array(
										"propertyID" => $nextseq["maxCount"],
										"teamID" => "N/A",
										"url" => $url,
										"title"=> $addr1,
										"addr1"=> $addr1,
										"addr2"=> $addr2,
										"image"=> $image,
										"price" => $price,
										"beds" => $beds,
										"baths" => $baths,
										"dimens" => $size,
										"desc" => "",
										//"note" => $note,
										"currency" => "USD",
										"measurement" => "sqft",
										"dateCreated" => $currentTime,
										"lastEdited" => $currentTime,
										"questions" => $brokerage_result["questions"],
										"managerID"=>$managerID,
										"brokerageID"=>"spire",
										"anon"=>"NO",
										"autoEmail" => "YES",
										"cnEnabled" => "NO",
										"editable"=>false);

						$result = $collection_properties->insert($document_property);

						$document_realtymx = array(
							"url" => $url,
							"id" => $id,
							"addr" => $addr1.", ".$addr2,
							"brokerageID" => "spire",
							"agent"=>$email,
							"datePulled" => $currentTime
						);

						$result = $collection_realtymx->insert($document_realtymx);
					}
				}
			}
		}

		return $count;
	}


	function getSemoninListings($email, $managerID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "semonin"));

		$url="http://media.reliancenetwork.com/dynamic/feeds/zillow/Zillow_SemoninRealtors354.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);

		$count = 0;

		foreach ($xml as $property) {
			$count++;

			$original_url = $property->{'landing-page'}->{'lp-url'};
			$addr1 = $property->location->{'street-address'};
			$city = $property->location->{'city-name'};
			$zipcode = $property->location->{'zipcode'};
			$state = $property->location->{'state-code'};
			$addr2 = $city.", ".$state." ".$zipcode;
			$price = $property->details->price."";
			$beds = $property->details->{'num-bedrooms'}."";
			$baths= $property->details->{'num-bathrooms'}."";
			$size = $property->details->{'square-feet'}."";
			if(isset($property->pictures->picture[0])) {
				$image = $property->pictures->picture[0]->{'picture-url'}."";
			} else {
				$num = rand ( 1 , 10 );
				$image = "http://www.spac.io/assets/img/stock/".$num.".jpg";
			}

			$agent_url = $property->office->{'office-website'};

			$agent = $property->agent->{'agent-email'};

			$original_url = str_replace("http://www.semonin.com",$agent_url,$original_url);

			if($beds == "") {
				$beds = -1;
			}

			if($baths == "") {
				$baths = -1;
			}

			if($size == "") {
				$size = -1;
			}

			$email = trim($email);

			if ($email == $agent) {

				$date = new DateTime();
				$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);
				$property_result = $collection_properties->findOne(array('managerID' => $managerID, "addr1" => $addr1));

				if (is_null($property_result)) {
					$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

					$nextseq = $collection_counter->findAndModify(
						array("desc" => "propertyID"),
						array('$inc' => array('maxCount' => 1)),
						null,
						array(
							"new" => true
						)
					);

					$document_property = array(
									"propertyID" => $nextseq["maxCount"],
									"teamID" => "N/A",
									"url" => $original_url,
									"title"=> $addr1,
									"addr1"=> $addr1,
									"addr2"=> $addr2,
									"image"=> $image,
									"price" => $price,
									"beds" => $beds,
									"baths" => $baths,
									"dimens" => $size,
									"desc" => "",
									//"note" => $note,
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"questions" => $brokerage_result["questions"],
									"managerID"=>$manager_result["_id"]->{'$id'},
									"brokerageID"=>$manager_result["brokerageID"],
									"anon"=>"NO",
									"autoEmail" => "YES",
									"cnEnabled" => "NO",
									"editable"=>false);

					$result = $collection_properties->insert($document_property);

				}
			}
		}

		return $count;
	}

	function getCorcoranListings($email,$managerID) {
		$url="http://spac.io/oh/feeds/corcoran/ZillowCorcoran.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);
		$json = json_encode($xml);
		$json_array  = json_decode($json, true);

		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "corcoran"));

		$count = 0;
		foreach ($xml as $properties) {

			foreach ($properties as $property) {
				$count++;
				$id =  $property->ListingDetails->{'MlsId'};
				$url = $property->ListingDetails->{'ListingUrl'};
				$addr1 = $property->Location->{'StreetAddress'};
				$city = $property->Location->{'City'};
				$zipcode = $property->Location->{'Zip'};
				$state = $property->Location->{'State'};
				$addr2 = $city.", ".$state." ".$zipcode;
				$price = $property->ListingDetails->{'Price'}."";
				$beds = $property->BasicDetails->{'Bedrooms'}."";
				$baths_full= $property->BasicDetails->{'FullBathrooms'};
				$baths_half= $property->BasicDetails->{'HalfBathrooms'};

				$baths = $baths_full + $baths_half*0.5;

				$size = $property->BasicDetails->{'LivingArea'}."";
				if(isset($property->pictures->picture[0])) {
					$image = $property->pictures->picture[0]->{'PictureUrl'}."";
				} else {
					$num = rand ( 1 , 10 );
					$image = "http://www.spac.io/assets/img/stock/".$num.".jpg";
				}

				$agent = $property->Agent->{'EmailAddress'}."";
				$agent = strtolower($agent);

				if($beds == "") {
					$beds = -1;
				}

				if($baths == "") {
					$baths = -1;
				}

				if($size == "") {
					$size = -1;
				}

				/*
				print_r($id."<br/>");
				print_r($addr1."<br/>");
				print_r($addr2."<br/>");
				print_r($price."<br/>");
				print_r($beds."<br/>");
				print_r($baths."<br/>");
				print_r($size."<br/>");
				print_r($url."<br/>");
				print_r($image."<br/>");
				foreach($emails as $email) {
					print_r($email."<br/>");
				}*/



					if ($email == $agent) {
						//print_r($email." found. Adding Property.<br/>");
						$date = new DateTime();
						$timeStamp = $date->getTimestamp();
						$currentTime = new MongoDate($timeStamp);
						$property_result = $collection_properties->findOne(array('id2' => $id, "managerID" => $managerID));

						if (is_null($property_result)) {
							//print_r("Property Added.<br/>");
							$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

							$nextseq = $collection_counter->findAndModify(
								array("desc" => "propertyID"),
								array('$inc' => array('maxCount' => 1)),
								null,
								array(
									"new" => true
								)
							);

							$document_property = array(
											"propertyID" => $nextseq["maxCount"],
											"teamID" => "N/A",
											"url" => $url,
											"title"=> $addr1,
											"addr1"=> $addr1,
											"addr2"=> $addr2,
											"image"=> $image,
											"price" => $price,
											"beds" => $beds,
											"baths" => $baths,
											"dimens" => $size,
											"desc" => "",
											//"note" => $note,
											"currency" => "USD",
											"measurement" => "sqft",
											"dateCreated" => $currentTime,
											"lastEdited" => $currentTime,
											"questions" => $brokerage_result["questions"],
											"managerID"=>$managerID,
											"brokerageID"=>"corcoran",
											"anon"=>"NO",
											"autoEmail" => "YES",
											"cnEnabled" => "NO",
											"id2" => $id,
											"editable"=>false);

							$result = $collection_properties->insert($document_property);


						} else {
							//print_r("Property Already exists.<br/>");
						}


				}
				//print_r("<br/>");
			}
		}
		return $count;

	}

	function getCitihabitatsListings($email, $managerID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "citihabitats"));

		$count = 0;
		$update_count = 0;
		if (($handle = fopen("../../feeds/corcoran/citihabitats_streeteasy.txt", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 0, "\t")) !== FALSE) {
				$num = count($data);

				if (true) {
				  for ($c=0; $c < $num; $c++) {
					  if ($c == 0) {
						  $id = $data[$c];
					  }
					  if ($c == 5) {
						  $addr1 = (string)$data[$c];
					  }
					  if ($c == 7) {
						  $addr2 = (string)$data[$c];
					  }
					  if ($c == 13) {
						  $price = (string)$data[$c];
					  }
					  if ($c == 14) {
				 		 $unit = $data[$c];
			  			}
					  if ($c == 15) {
						  $beds = (float)$data[$c];
					  }
					  if ($c == 16) {
						  $baths = (float)$data[$c];
					  }
					  if ($c == 17) {
						  $half_baths = (float)$data[$c];
					  }
					  if ($c == 19 ) {
						  $size = (string)$data[$c];
					  }
					  if ($c == 28) {
						  $agent_email = (string)$data[$c];
					  }
					  if ($c == 33) {
						  $agent_email2 = (string)$data[$c];
					  }
					  if ($c == 38) {
						  $agent_email3 = (string)$data[$c];
					  }

					  if ($c == 41) {
						  $img_url = (string)$data[$c];
					  }

				  }

				$date = new DateTime();
				$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);

				$baths = $baths + 0.5*$half_baths;
				$addr1 = $addr1.' '.$unit;

				$agent_email = strtolower($agent_email);
				$agent_email2 = strtolower($agent_email2);
				$agent_email3 = strtolower($agent_email3);

				$url = "https://citihabitats.com/".$id;
				$document_property = array(
									"teamID" => "N/A",
									"url" => $url,
									"title"=> $addr1,
									"addr1"=> $addr1,
									"addr2"=> $addr2,
									"image"=> $img_url,
									"price" => $price,
									"beds" => $beds,
									"baths" => $baths,
									"dimens" => $size,
									"desc" => "",
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"questions" => $brokerage_result["questions"],
									"brokerageID"=>"citihabitats",
									"id2"=>$id,
									"anon"=>"NO",
									"disabled" => "NO",
									"autoEmail" => "YES",
									"cnEnabled" => "NO",
									"editable"=>false,
									"sample"=>false);

					if ($agent_email == $email || $agent_email2 == $email || $agent_email3 == $email) {
							//echo $agent." exists.<br/>";
							$property_result = $collection_properties->findOne(array('managerID'=>$managerID,'id2'=>$id,'brokerageID' => "citihabitats"));
							//check if property added
							if (is_null($property_result)) {
								//echo $addr1." for ".$agent." added.<br/>";
								$count++;

								$nextseq = $collection_counter->findAndModify(
									array("desc" => "propertyID"),
									array('$inc' => array('maxCount' => 1)),
									null,
									array(
										"new" => true
									)
								);

								$document_property["propertyID"] = $nextseq["maxCount"];
								$document_property["managerID"] = $managerID;

								$result = $collection_properties->insert($document_property);
							} else {
								//echo $addr1." for ".$agent." already exists.<br/>";

								//echo $addr1." for ".$agent." updated.<br/>";
								//$update_count++;
								//$newdata = array('$set' => array('autoEmail' => "NO"));
								//$update_result= $collection_properties->update(array('_id' => $property_result['_id']), $newdata);

							}
						}

				}

				$row++;

			}

			fclose($handle);
		}

		return $count;


	}


	function getPlatinumListings($email, $managerID) {

		$url="http://webservice.olr.com/streeteasy/output/plat.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);
		$json = json_encode($xml);
		$json_array  = json_decode($json, true);

		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "platinum"));

		$count = 0;
		foreach ($xml as $properties) {

			foreach ($properties as $property) {
				$count++;
				$url = $property->attributes()->url."";
				$id = $property->attributes()->id."";
				$addr = $property->location->address."";
				$apartment = $property->location->apartment."";
				$addr1 = $addr.", ".$apartment;
				$city = $property->location->city."";
				$state = $property->location->state."";
				$zipcode = $property->location->zipcode."";
				$addr2 = $city.", ".$state." ".$zipcode;

				$price = $property->details->price."";
				$beds = $property->details->bedrooms."";
				$baths= $property->details->bathrooms."";
				$size = $property->details->squareFeet."";

				$image = $property->media->photo[0]->attributes()->url."";

				$agents = $property->agents->children();
				$emails = array();
				foreach($agents as $agent) {
					$emails[] = $agent->email."";
				}

				/*
				print_r($id."<br/>");
				print_r($addr1."<br/>");
				print_r($addr2."<br/>");
				print_r($price."<br/>");
				print_r($beds."<br/>");
				print_r($baths."<br/>");
				print_r($size."<br/>");
				print_r($url."<br/>");
				print_r($image."<br/>");
				foreach($emails as $email) {
					print_r($email."<br/>");
				}*/


				foreach($emails as $agent_email) {

					if ($email == $agent_email) {
						//print_r($email." found. Adding Property.<br/>");
						$date = new DateTime();
						$timeStamp = $date->getTimestamp();
						$currentTime = new MongoDate($timeStamp);
						$property_result = $collection_properties->findOne(array('id2' => $id, "managerID" => $managerID));

						if (is_null($property_result)) {
							//print_r("Property Added.<br/>");
							$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

							$nextseq = $collection_counter->findAndModify(
								array("desc" => "propertyID"),
								array('$inc' => array('maxCount' => 1)),
								null,
								array(
									"new" => true
								)
							);

							$document_property = array(
											"propertyID" => $nextseq["maxCount"],
											"teamID" => "N/A",
											"url" => $url,
											"title"=> $addr1,
											"addr1"=> $addr1,
											"addr2"=> $addr2,
											"image"=> $image,
											"price" => $price,
											"beds" => $beds,
											"baths" => $baths,
											"dimens" => $size,
											"desc" => "",
											//"note" => $note,
											"currency" => "USD",
											"measurement" => "sqft",
											"dateCreated" => $currentTime,
											"lastEdited" => $currentTime,
											"questions" => $brokerage_result["questions"],
											"managerID"=>$managerID,
											"brokerageID"=>"platinum",
											"anon"=>"NO",
											"autoEmail" => "YES",
											"cnEnabled" => "NO",
											"id2" => $id,
											"editable"=>false);

							$result = $collection_properties->insert($document_property);


						} else {
							//print_r("Property Already exists.<br/>");
						}
					} else {
						//print_r($email." not found.<br/>");
					}

				}
				//print_r("<br/>");
			}
		}
		return $count;
	}


	function getC21RedwoodListings($email,$managerID) {
		$url="http://redwood.portalstatic.com/xml/zillow.xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

		$data = curl_exec($ch); // execute curl request
		curl_close($ch);

		$xml = simplexml_load_string($data);
		$json = json_encode($xml);
		$json_array  = json_decode($json, true);

		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => "c21redwood"));

		$count = 0;
		foreach ($xml as $properties) {

			foreach ($properties as $property) {
				$count++;
				$url = $property->{'landing-page'}->{'lp-url'};
				$addr1 = $property->location->{'street-address'};
				$city = $property->location->{'city-name'};
				$zipcode = $property->location->{'zipcode'};
				$state = $property->location->{'state-code'};
				$addr2 = $city.", ".$state." ".$zipcode;
				$price = $property->details->price."";
				$beds = $property->details->{'num-bedrooms'}."";
				$baths_full= $property->details->{'num-full-bathrooms'};
				$baths_half= $property->details->{'num-half-bathrooms'};

				$baths = $baths_full + $baths_half*0.5;

				$size = $property->details->{'living-area-square-feet'}."";
				if(isset($property->pictures->picture[0])) {
					$image = $property->pictures->picture[0]->{'picture-url'}."";
				} else {
					$num = rand ( 1 , 10 );
					$image = "http://www.spac.io/assets/img/stock/".$num.".jpg";
				}

				$agent = $property->agent->{'agent-email'}."";


				if($beds == "") {
					$beds = -1;
				}

				if($baths == "") {
					$baths = -1;
				}

				if($size == "") {
					$size = -1;
				}

				/*
				print_r($id."<br/>");
				print_r($addr1."<br/>");
				print_r($addr2."<br/>");
				print_r($price."<br/>");
				print_r($beds."<br/>");
				print_r($baths."<br/>");
				print_r($size."<br/>");
				print_r($url."<br/>");
				print_r($image."<br/>");
				foreach($emails as $email) {
					print_r($email."<br/>");
				}*/



					if ($email == $agent) {
						//print_r($email." found. Adding Property.<br/>");
						$date = new DateTime();
						$timeStamp = $date->getTimestamp();
						$currentTime = new MongoDate($timeStamp);
						$property_result = $collection_properties->findOne(array('id2' => $id, "managerID" => $managerID));

						if (is_null($property_result)) {
							//print_r("Property Added.<br/>");
							$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

							$nextseq = $collection_counter->findAndModify(
								array("desc" => "propertyID"),
								array('$inc' => array('maxCount' => 1)),
								null,
								array(
									"new" => true
								)
							);

							$document_property = array(
											"propertyID" => $nextseq["maxCount"],
											"teamID" => "N/A",
											"url" => $url,
											"title"=> $addr1,
											"addr1"=> $addr1,
											"addr2"=> $addr2,
											"image"=> $image,
											"price" => $price,
											"beds" => $beds,
											"baths" => $baths,
											"dimens" => $size,
											"desc" => "",
											//"note" => $note,
											"currency" => "USD",
											"measurement" => "sqft",
											"dateCreated" => $currentTime,
											"lastEdited" => $currentTime,
											"questions" => $brokerage_result["questions"],
											"managerID"=>$managerID,
											"brokerageID"=>"c21redwood",
											"anon"=>"NO",
											"autoEmail" => "YES",
											"cnEnabled" => "NO",
											"id2" => $id,
											"editable"=>false);

							$result = $collection_properties->insert($document_property);


						} else {
							//print_r("Property Already exists.<br/>");
						}


				}
				//print_r("<br/>");
			}
		}
		return $count;

	}


?>
