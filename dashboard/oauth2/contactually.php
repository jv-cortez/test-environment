<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

$application_id = "ff0312d73de7cba90ac50751006bd9fa4c6e0f1717c2331b0f33f8f59292c151";
$client_secret = "7134b3a560742a98a271446b260b2667e4cc7abaa4ec54125bb00db83047ae4d";
$res = $_GET;
$code = $res['code'];
echo("Code:\n");
echo($code);
echo("\n");
$access_token = "8c5d378ed47e638bcd7300c0043f73a4fb8ec022a55c8fb8ba93918af133e834";

getUserToken($application_id,$client_secret,$code,$access_token);

function getUserToken($application_id,$client_secret,$code,$access_token){

$client_id = $application_id;
$scope = "user:basic+contacts:manage+buckets:manage+notes:manage+interactions:manage";
$redirect_uri = "https://ws.spac.io/prod/oh/oauth2/contactually/";
// $redirect_uri_encode = "https%3a%2f%2fws.spac.io%2fprod%2foh%2foauth2%2fcontactually%2f";
// $response_type = "code";
$response_type = "token";


$url = "https://auth.contactually.com/oauth2/token/";

$ch = curl_init();
curl_setopt($ch,CURLOPT_URL,$url);
curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'POST');
curl_setopt($ch,CURLOPT_POST,1);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);

$headers = array(
  'Content-Type: application/json'
);

$fields = array(
  'grant_type' => 'authorization_code',
  'client_id' => $client_id,
  'client_secret' => $client_secret,
  'code' => $code,
  'redirect_uri' => $redirect_uri
);

$data_fields = json_encode($fields);

curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
curl_setopt($ch,CURLOPT_POSTFIELDS,$data_fields);
$server_output = curl_exec($ch);
curl_close($ch);
echo($server_output);
echo("\n");

$res = json_decode($server_output,true);
$token = $res['access_token'];

echo "token \n";
echo($token);

}
?>
