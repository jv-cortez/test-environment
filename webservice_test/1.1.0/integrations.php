<?php

require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/stripe-php-3.14.3/init.php');

require_once "mailchimp/mailchimp.php";
require_once "mailchimp/crm_api.php";


function mailchimpConnect($mailchimp_key, $mailchip_username, $agent_name){
  $credential = array(
    "user" => "$mailchip_username:$mailchimp_key",
    "header" => array(
        'Content-Type: application/json'
      )
  );

  //get all the list from the user
  $get_lists = array(
    "verb" => "GET",
    "list_id" => "",
    "method" => "lists",
    "subscriber_hash" => "",
    "merge_id" => "",
    "postData" => ""
  );
  $get_lists_url = array(
    "url" => mailchimp($get_lists,$mailchimp_key)
  );
  $getListArr = array_merge($get_lists, $get_lists_url, $credential);
  $allLists = crmApi($getListArr);
  if(isset($allLists['error'])){
    return array(
      'error_type' => $allLists['type'],
      'error_status' => $allLists['status'],
      'error_detail' => $allLists['detail'],
      'status' => 0
    );
  }

  //loop through all lists and find Spacio
  foreach($allLists['lists'] as $list){
    ($list['name'] == 'Spacio') ? $newListID = $list['id'] : "";
  }

  //check if Spacio list exists in user account
  if(isset($newListID)){
    //there is Spacio list
    $msg = 'Spacio list already exists!';

  } else{
    //there is no Spacio list
    //get account contact information
    $get_root = array(
      "verb" => "GET",
      "method" => "root",
      "list_id" => "",
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => ""
    );
    $get_root_url = array(
      "url" => mailchimp($get_root,$mailchimp_key)
    );
    $getAccArr = array_merge($get_root, $get_root_url, $credential);
    $accInfo = crmApi($getAccArr);
    $contact = $accInfo['contact'];
    $contact = array_merge($contact, array('address1'=>$contact['addr1']));
    $contact = array_merge($contact, array('address2'=>$contact['addr2']));
    $contact = json_encode($contact);

    //create Spacio list
    $post_list = array(
      "verb" => "POST",
      "method" => "lists",
      "list_id" => "",
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => '{
          "name":"Spacio",
          "contact": '.$contact.',
          "permission_reminder":".",
          "use_archive_bar":TRUE,
          "campaign_defaults":{
              "from_name":"'.$agent_name.'",
              "from_email":"'.$accInfo['email'].'",
              "subject":"",
              "language":"en"
              },
          "notify_on_subscribe":"",
          "notify_on_unsubscribe":"",
          "email_type_option":true,
          "visibility":"prv"
        }'
    );
    $post_list_url = array(
      "url" => mailchimp($post_list,$mailchimp_key)
    );
    $postListArr = array_merge($post_list, $post_list_url, $credential);
    $newList = crmApi($postListArr);
    if(isset($newList['error'])){
      return array(
        'error_type' => $newList['type'],
        'error_status' => $newList['status'],
        'error_detail' => $newList['detail'],
        'status' => 0
      );
    }
    $newListID = $newList['id'];

    //add phone field
    $post_phone = array(
      "verb" => "POST",
      "method" => "lists/list_id/merge-fields",
      "list_id" => $newListID,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => '{
          "tag":"PHONE",
          "name":"PHONE",
          "type":"text",
          "required":FALSE
          }'
    );
    $post_phone_url = array(
      "url" => mailchimp($post_phone,$mailchimp_key)
    );
    $postPhoneArr = array_merge($post_phone, $post_phone_url, $credential);
    $newPhoneField = crmApi($postPhoneArr);
    if(isset($newPhoneField['error'])){
      return array(
        'error_type' => $newPhoneField['type'],
        'error_status' => $newPhoneField['status'],
        'error_detail' => $newPhoneField['detail'],
        'status' => 0
      );
    }

    //add listing_address field
    $post_listing_addr = array(
      "verb" => "POST",
      "method" => "lists/list_id/merge-fields",
      "list_id" => $newListID,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => '{
          "tag":"PROP_ADDR",
          "name":"PROPERTY ADDRESS",
          "type":"text",
          "required":FALSE
          }'
    );
    $post_listing_addr_url = array(
      "url" => mailchimp($post_listing_addr,$mailchimp_key)
    );
    $postListingAddrArr = array_merge($post_listing_addr, $post_listing_addr_url, $credential);
    $newListingAddrField = crmApi($postListingAddrArr);
    if(isset($newListingAddrField['error'])){
      return array(
        'error_type' => $newListingAddrField['type'],
        'error_status' => $newListingAddrField['status'],
        'error_detail' => $newListingAddrField['detail'],
        'status' => 0
      );
    }
    $msg = 'List successfully created!';
  }

  return array(
    'msg' => $msg,
    'newListID' => $newListID,
    'status' => 1
  );
}

function mailchimpCreateLead($mailchimp_key,$mailchip_username,$list_id,$fname,$lname,$phone,$listing_address,$questions,$email){
  $credential = array(
    "user" => "$mailchip_username:$mailchimp_key",
    "header" => array(
        'Content-Type: application/json'
      )
  );

  //add member(aka lead) into list
  $post_member = array(
    "verb" => "POST",
    "method" => "lists/id/members",
    "list_id" => $list_id,
    "subscriber_hash" => "",
    "merge_id" => "",
    "postData" => '{
      "email_type":"",
      "status":"subscribed",
      "merge_fields":{
          "FNAME":"'.$fname.'",
          "LNAME":"'.$lname.'",
          "PHONE":"'.$phone.'",
          "PROP_ADDR":"'.$listing_address.'"
          },
      "email_address":"'.$email.'"
      }'
  );
  $post_member_url = array(
    "url" => mailchimp($post_member,$mailchimp_key)
  );
  $postMemberArr = array_merge($post_member, $post_member_url, $credential);
  $lead = crmApi($postMemberArr);
  if(isset($lead['error']) && $lead['title'] != 'Member Exists'){
    //if there is an error and the reason is not because member exists
    return array(
      'error_type' => $lead['type'],
      'error_status' => $lead['status'],
      'error_detail' => $lead['detail'],
      'status' => 0
    );
  } elseif (isset($lead['error']) && $lead['title'] == 'Member Exists'){
    //member exists, change to update member
    //find subscriber_hash (member id) of existing member
    $get_members = array(
      "verb" => "GET",
      "method" => "lists/list_id/members",
      "list_id" => $list_id,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => ""
    );
    $get_members_url = array(
      "url" => mailchimp($get_members,$mailchimp_key)
    );
    $getMemberArr = array_merge($get_members, $get_members_url, $credential);
    $allMembers = crmApi($getMemberArr);
    if(isset($allMembers['error'])){
      return array(
        'error_type' => $allMembers['type'],
        'error_status' => $allMembers['status'],
        'error_detail' => $allMembers['detail'],
        'status' => 0
      );
    }
    //loop through all members and find email and subscriber_hash
    foreach($allMembers['members'] as $member){
      ($member['email_address'] == $email) ? $subscriber_hash = $member['id'] : "";
    }

    //get member original information
    $get_member = array(
      "verb" => "GET",
      "method" => "lists/list_id/members",
      "list_id" => $list_id,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => ""
    );
    $get_member_url = array(
      "url" => mailchimp($get_member,$mailchimp_key)
    );
    $getMemberArr = array_merge($get_member, $get_member_url, $credential);
    $memberInfo = crmApi($getMemberArr);
    if(isset($memberInfo['error'])){
      return array(
        'error_type' => $memberInfo['type'],
        'error_status' => $memberInfo['status'],
        'error_detail' => $memberInfo['detail'],
        'status' => 0
      );
    }
    //use old information if no new info is provided
    ($fname == "") ? $fname = $memberInfo['members'][0]['merge_fields']['FNAME'] : "" ;
    ($lname == "") ? $lname = $memberInfo['members'][0]['merge_fields']['LNAME'] : "" ;
    ($phone == "") ? $phone = $memberInfo['members'][0]['merge_fields']['PHONE'] : "" ;
    ($listing_address == "") ? $listing_address = $memberInfo['members']['merge_fields']['PROP_ADDR'] : "" ;

    //update member
    $patch_member = array(
      "verb" => "PATCH",
      "method" => "lists/list_id/members/subscriber_hash",
      "list_id" => $list_id,
      "subscriber_hash" => $subscriber_hash,
      "merge_id" => "",
      "postData" => '{
        "merge_fields":{
          "FNAME":"'.$fname.'",
          "LNAME":"'.$lname.'",
          "PHONE":"'.$phone.'",
          "PROP_ADDR":"'.$listing_address.'"
          }
        }'
    );
    $patch_member_url = array(
      "url" => mailchimp($patch_member,$mailchimp_key)
    );
    $patchMemberArr = array_merge($patch_member, $patch_member_url, $credential);
    $patchMember = crmApi($patchMemberArr);
    if(isset($patchMember['error'])){
      return array(
        'error_type' => $patchMember['type'],
        'error_status' => $patchMember['status'],
        'error_detail' => $patchMember['detail'],
        'status' => 0
      );
    }

    $msg = 'Lead successfully updated';
  } else {
    //no error and member does not exist
    $msg = 'Lead successfully created!';
    $subscriber_hash = $lead['id'];
  }

  //post questions in notes if note is provided
  if($questions != ""){
    $post_note = array(
      "verb" => "POST",
      "method" => "lists/list_id/members/subscriber_hash/notes",
      "list_id" => $list_id,
      "subscriber_hash" => $subscriber_hash,
      "merge_id" => "",
      "postData" => '{"note":"'.$questions.'"}'
    );
    $post_note_url = array(
      "url" => mailchimp($post_note,$mailchimp_key)
    );
    $postNoteArr = array_merge($post_note, $post_note_url, $credential);
    $newNote = crmApi($postNoteArr);
    if(isset($newNote['error'])){
      return array(
        'error_type' => $newNote['type'],
        'error_status' => $newNote['status'],
        'error_detail' => $newNote['detail'],
        'status' => 0
      );
    }
  }

  return array(
    'msg' => $msg,
    'subscriber_hash' => $subscriber_hash,
    'status' => 1
  );
}
	

?>