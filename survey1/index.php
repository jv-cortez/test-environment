<?php

  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  require_once('../oh/config/db_conn.php');
  require_once('../oh/require/core.php');

  if($_GET["s"] == ""){
    $_surveyID = "a493ladsk";
  } else{
    $_surveyID = $_GET["s"];
  };

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="assets/img/favicon.png">

  <title>Spacio: Introducing the Paperless Open House</title>

  <link href="/assets/css/normalize.css" rel="stylesheet">
  <link href="/assets/css/website.css" rel="stylesheet">
  <link href="/assets/fonts/fonts.css" rel="stylesheet">
  <link href="/assets/css/survey.css" rel="stylesheet">
  <!-- <link href="/assets/css/emailsurveys.css" rel="stylesheet"> -->

  <link rel="shortcut icon" type="image/png" href="http://www.spac.io/assets/img/favicon.png"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <link rel="import" id="topmenu" href="/topmenu.html">
  <link rel="import" id="footer" href="/footer.html">
  <link rel="import" id="popups" href="/popups.html">
  <link rel="import" id="alertbox" href="/alertbox.html">

</head>

<body style="margin:0;padding:0;">


  <div class="spacio-top-menu">
    <div class="web-menu">
      <a href="/">
         <div class="spacio-top-menu-logo">
           <img src="/assets/img/website/spacio_logo_long.png" height="40">
         </div>
      </a>
    </div>
  </div><!-- end of spacio-top-menu -->

  <div class="popup-overlay"></div>
  <!-- POPUP OVERLAY END -->
  <div class="alert-overlay"></div>
  <!-- ALERT OVERLAY END -->

  <div class="survey-scroll-one">
   <div class="content">
     <div class="survey-title small subtitle"></div>
     <div class="survey-desc medium desc"></div>
   </div>
  </div>


  <div class="main-container">

    <form class="form-questions" id="submit_survey" method="post"></form>

  </div><!-- end of main-container  -->

  <div class="footer normal"></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/assets/js/jquery.loadTemplate.min.js"></script>
<script src="/assets/js/website.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
    insertAlertBox();
    getQuestions();
  })

// completionHandler

  function getQuestions(){

    var _query = {};

    _query["fn"] = "getSpacioSurvey";
    _query["surveyID"] = "<?php echo $_surveyID ?>";

    var json_data = _query;

    console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

    console.log(json_data);

    $.ajax({
      url: "https://ws.spac.io/stage/oh/webservice_survey_system/1.0.0/webservice_survey.php",
      type:"POST",
      data: {json : json_data},
      success: function(data, textStatus, jqXHR){

        console.log(data);

        if (data.status == 1) {
          console.log("Returned JSON Object: "+JSON.stringify(data));

          $(".survey-title").html(data["surveyName"]);
          $(".survey-desc").html(data["surveyDesc"]);

          $.each(data["questionsObj"], function(index,value) {

						if (value["type"] == "text") {
              $(".form-questions").append("<div class='form-question-row'><div class='question'>"+value["question"]+"</div><input name='question-"+index+"' type='text' value='' placeholder='Answer'></div>");
            }
            // "<span class='cn' id='cn-"+index+"'> / "+value["questionCN"]+"</span>

						if (value["type"] == "yn") {
              $(".form-questions").append("<div class='form-question-row'><div class='question'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='question-"+index+"-yes' type='radio' value='YES' name='question-"+index+"'><label for='question-"+index+"-yes'>Yes </label></div><div class='checkbox'><input id='question-"+index+"-no' type='radio' value='NO' name='question-"+index+"'><label for='question-"+index+"-no'>No </label></div></div></div>");
          	}
            // <span class='cn' id='cn-"+index+"'> / "+value["questionCN"]+"</span>
            // <span class='cn'>/ 是</span>
            // <span class='cn'> / 否</span>


            if (value["type"] == "mc") {
              $(".form-questions").append("<div class='form-question-row'><div class='question'>"+value["question"]+"</div><div class='full-width-checkboxes' id='answers-section-"+index+"'></div></div>");

// <span class='cn' id='cn-"+index+"'> / "+value["questionCN"]+"</span>

							$.each(value["choices"], function(i,v){
                $("#answers-section-"+index).append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='question-"+index+"' id='question-"+index+"-"+i+"'><label for='question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");

 // <span class='cn'>/ "+v["answerCN"]+"</span>


              });
						}
          });

          $(".form-questions").append("<div><input class='submit-button' type='submit' value='Submit'></input></div>");


          // if ($.isFunction(completionHandler)) {
          //   completionHandler();
          // }


        } else {

        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getListing error Error: "+errorThrown);
        loading(false);
      },
      dataType: "json"
    });
  }



    $( "form#submit_survey" ).submit(function( event ) {

      var _query = {};

      _query["fn"] = "postSpacioSurvey";
      _query["surveyID"] = "<?php echo $_surveyID ?>";

      _query["answersObj"] = [];

      $.each($('#submit_survey .form-question-row'), function() {
          var temp = {};
          temp["question"] = $(this).children('.question').text();
          temp["answer"] = $(this).children('.answer').val();
          _query["answersObj"].push(temp);
      });

      var json_data = _query;

      console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

      console.log(json_data);

      $.ajax({
        url: "https://ws.spac.io/stage/oh/webservice_survey_system/1.0.0/webservice_survey.php",
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){

          console.log(data);

          if (data.status == 1) {
            console.log("Returned JSON Object: "+JSON.stringify(data));
            showAlert("info","Survey Submitted","Thank you for your time.");

          } else {
            showAlert("error","Survey Failed to Submit","Sorry, we were unable to submit your survey. Please contact your account administrator.");
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getListing error Error: "+errorThrown);
          loading(false);
        },
        dataType: "json"
      });
  		event.preventDefault();
    })

</script>

</body>
</html>
