<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/stripe-php-3.14.3/init.php');
require_once("../../require/vendor/autoload.php");


	/* ------------------------------ SHARED  FUNCTIONS ------------------------------*/

	function syncCRMLSListings($mlsID, $managerID) {
		
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID)));
		
		
		$config = new \PHRETS\Configuration;
		$config->setLoginUrl('https://pt.rets.crmls.org/contact/rets/login');
		$config->setUsername('SPACIO');
		$config->setPassword('?8gtWT&W');
		
		// optional.  value shown below are the defaults used when not overridden
		$config->setRetsVersion('1.7.2'); // see constants from \PHRETS\Versions\RETSVersion
		$config->setHttpAuthenticationMethod('digest'); // or 'basic' if required 
		$config->setOption('use_post_method', true); // boolean
		$config->setOption('disable_follow_location', false); // boolean
		
		$rets = new \PHRETS\Session($config);
		
		$bulletin = $rets->Login();
			
		$results = $rets->Search("Property", "Residential", '(ListAgentMlsId='.$mlsID.'),(StandardStatus=X)', ['Limit' => 100]);
		$count = 0;
		foreach ($results as $r) {
			$property = array();
			
			$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);	
		  		  		
			
			$randnum = rand ( 1 , 10 );
			$image = "http://www.spac.io/assets/img/stock/".$randnum.".jpg";
			
			$property["mls"] = "crmls";	
			$property["mlsID"] = $r['ListingId'];
			$property["photo"] = $image;
			
			$streetNumber = $r['StreetNumberNumeric'];
			$streetName = $r['StreetName'];
			$streetName = ucwords(strtolower($streetName));
			$streetSuffix = $r['StreetSuffix'];
			$streetPrefix = $r['StreetDirPrefix'];
			$unitNumber = $r['UnitNumber'];
			$city = $r['City'];
			$state = $r['StateOrProvince'];
			$postalCode = $r['PostalCode'];
			$size = $r['LivingArea'];
			$price = $r['ListPrice'];
			$baths = $r['BathroomsTotalInteger'];
			$beds = $r['BedroomsTotal'];
			
			$addr1 = "";
			$addr2 = $city.", ".$state." ".$postalCode;
			
			$addr1 = $streetNumber;
			
			if($streetPrefix != "" && $streetPrefix != NULL) {
				$addr1 = $addr1." ".$streetPrefix;
			}
			
			$addr1 = $addr1." ".$streetName;
			
			if($streetSuffix != "" && $streetSuffix != NULL) {
				$addr1 = $addr1." ".$streetSuffix;
			}
			
			if($unitNumber != "" && $unitNumber != NULL) {
				$addr1 = $addr1." Unit ".$unitNumber;
			}
			
			$property["addr1"] = $addr1;
			$property["addr2"] = $addr2;
			$property["price"] = $price;
			$property["size"] = $size;
			$property["baths"] = $baths;
			$property["beds"] = $beds;
			
			$photos = $rets->Search("Media", "Media", 'ResourceRecordKeyNumeric='.$r['ListingKeyNumeric'], ['Limit' => 1]);

			foreach ($photos as $photo) {
				$property["photo"] = $photo["MediaURL"];
			}
			
			
			$property_result = $collection_properties->findOne(array('mlsnum' => $property["mlsID"], "mls" => $property["mls"], "managerID" => $managerID));
						
			if(is_null($property_result)) {
				$nextseq = $collection_counter->findAndModify(
					 array("desc" => "propertyID"),
					 array('$inc' => array('maxCount' => 1)),
					 null,
					 array(
						"new" => true
					)
				);
				
				
				$questions = array();
				
				if($manager_result["brokerageID"] != "N/A") {
					$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
					$brokerage_result = $collection_brokerages->findOne(array("brokerageID" => $manager_result["brokerageID"]));
					$questions = $brokerage_result["questions"];
					$autoEmail = "YES";
				} 
				
				$document_property = array(
									"propertyID" => $nextseq["maxCount"],
									"mls" => $property["mls"],
									"mlsnum" => $property["mlsID"],
									"url" => "", 
									"title"=> $property["addr1"] , 
									"addr1"=> $property["addr1"] ,
									"addr2"=> $property["addr2"] , 
									"image"=> $property["photo"] ,
									"price" => $property["price"] ,
									"beds" => $property["beds"] , 
									"baths" => $property["baths"] , 
									"dimens" => $property["size"] ,
									"desc" => "",
									//"note" => $note,
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"tlc" => "N/A",
									"questions" => $questions,
									"managerID"=>$managerID,
									"brokerageID"=>$manager_result["brokerageID"],
									"anon"=>"NO",
									"autoEmail" => "NO",
									"cnEnabled" => "NO",
									"brokersEnabled" => "NO",
									"disabled" => "NO",
									"mobile" => true,
									"editable"=>true,
									"sample" => false);
									
		  		$result = $collection_properties->insert($document_property);
				$count++;
				
			}
		}
		$rets->Disconnect();
		
		return $count;
		
	}
	
	
	
	function getCRMLSListing($listingID) {
		$config = new \PHRETS\Configuration;
		$config->setLoginUrl('https://pt.rets.crmls.org/contact/rets/login');
		$config->setUsername('SPACIO');
		$config->setPassword('?8gtWT&W');
		
		// optional.  value shown below are the defaults used when not overridden
		$config->setRetsVersion('1.7.2'); // see constants from \PHRETS\Versions\RETSVersion
		$config->setHttpAuthenticationMethod('digest'); // or 'basic' if required 
		$config->setOption('use_post_method', true); // boolean
		$config->setOption('disable_follow_location', false); // boolean
		
		$rets = new \PHRETS\Session($config);
		
		$bulletin = $rets->Login();

		$results = $rets->Search("Property", "Residential", 'ListingId='.$listingID, ['Limit' => 1]);
		
		$property = array();
		foreach ($results as $r) {
			$randnum = rand ( 1 , 10 );
			$image = "http://www.spac.io/assets/img/stock/".$randnum.".jpg";
						
			$property["mlsID"] = $r['ListingId'];
			$property["photo"] = $image;
			
			$streetNumber = $r['StreetNumberNumeric'];
			$streetName = $r['StreetName'];
			$streetName = ucwords(strtolower($streetName));
			$streetSuffix = $r['StreetSuffix'];
			$streetPrefix = $r['StreetDirPrefix'];
			$unitNumber = $r['UnitNumber'];
			$city = $r['City'];
			$state = $r['StateOrProvince'];
			$postalCode = $r['PostalCode'];
			$size = $r['LivingArea'];
			$price = $r['ListPrice'];
			$baths = $r['BathroomsTotalInteger'];
			$beds = $r['BedroomsTotal'];
			
			$addr1 = "";
			$addr2 = $city.", ".$state." ".$postalCode;
			
			$addr1 = $streetNumber;
			
			if($streetPrefix != "" && $streetPrefix != NULL) {
				$addr1 = $addr1." ".$streetPrefix;
			}
			
			$addr1 = $addr1." ".$streetName;
			
			if($streetSuffix != "" && $streetSuffix != NULL) {
				$addr1 = $addr1." ".$streetSuffix;
			}
			
			if($unitNumber != "" && $unitNumber != NULL) {
				$addr1 = $addr1." Unit ".$unitNumber;
			}
			
			$property["addr1"] = $addr1;
			$property["addr2"] = $addr2;
			$property["price"] = $price;
			$property["size"] = $size;
			$property["baths"] = $baths;
			$property["beds"] = $beds;
			
			$photos = $rets->Search("Media", "Media", 'ResourceRecordKeyNumeric='.$r['ListingKeyNumeric'], ['Limit' => 1]);

			foreach ($photos as $photo) {
				$property["photo"] = $photo["MediaURL"];
			}
		
			//var_dump($r);
			//echo "\n\n";
		}

		$rets->Disconnect();
		return $property;
	}
	
	function verifyCRMLSMember($mlsID, $managerID) {
		
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID)));
		
		$authorized = false;
		
		if($mlsID == "") {
			$mlsID = "NA";
		}
		$config = new \PHRETS\Configuration;
		$config->setLoginUrl('https://pt.rets.crmls.org/contact/rets/login');
		$config->setUsername('SPACIO');
		$config->setPassword('?8gtWT&W');
		
		// optional.  value shown below are the defaults used when not overridden
		$config->setRetsVersion('1.7.2'); // see constants from \PHRETS\Versions\RETSVersion
		$config->setHttpAuthenticationMethod('digest'); // or 'basic' if required 
		$config->setOption('use_post_method', true); // boolean
		$config->setOption('disable_follow_location', false); // boolean
		
		$rets = new \PHRETS\Session($config);
		
		$bulletin = $rets->Login();
		
		$results = $rets->Search("Member", "Member", '(MemberLoginId='.$mlsID.')', ['Limit' => 1]);
		
		foreach ($results as $r) {
			$authorized = true;
		}
		
		
		$results2 = $rets->Search("Property", "Residential", '(ListAgentMlsId='.$mlsID.'),(StandardStatus=A)', ['Limit' => 10]);
		
		foreach ($results2 as $r) {
			$property = array();
			
			$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);	
		  		  		
			
			$randnum = rand ( 1 , 10 );
			$image = "http://www.spac.io/assets/img/stock/".$randnum.".jpg";
			
			$property["mls"] = "crmls";	
			$property["mlsID"] = $r['ListingId'];
			$property["photo"] = $image;
			
			$streetNumber = $r['StreetNumberNumeric'];
			$streetName = $r['StreetName'];
			$streetName = ucwords(strtolower($streetName));
			$streetSuffix = $r['StreetSuffix'];
			$streetPrefix = $r['StreetDirPrefix'];
			$unitNumber = $r['UnitNumber'];
			$city = $r['City'];
			$state = $r['StateOrProvince'];
			$postalCode = $r['PostalCode'];
			$size = $r['LivingArea'];
			$price = $r['ListPrice'];
			$baths = $r['BathroomsTotalInteger'];
			$beds = $r['BedroomsTotal'];
			
			$addr1 = "";
			$addr2 = $city.", ".$state." ".$postalCode;
			
			$addr1 = $streetNumber;
			
			if($streetPrefix != "" && $streetPrefix != NULL) {
				$addr1 = $addr1." ".$streetPrefix;
			}
			
			$addr1 = $addr1." ".$streetName;
			
			if($streetSuffix != "" && $streetSuffix != NULL) {
				$addr1 = $addr1." ".$streetSuffix;
			}
			
			if($unitNumber != "" && $unitNumber != NULL) {
				$addr1 = $addr1." Unit ".$unitNumber;
			}
			
			$property["addr1"] = $addr1;
			$property["addr2"] = $addr2;
			$property["price"] = $price;
			$property["size"] = $size;
			$property["baths"] = $baths;
			$property["beds"] = $beds;
			
			$photos = $rets->Search("Media", "Media", 'ResourceRecordKeyNumeric='.$r['ListingKeyNumeric'], ['Limit' => 1]);

			foreach ($photos as $photo) {
				$property["photo"] = $photo["MediaURL"];
			}
			
			
			$property_result = $collection_properties->findOne(array('mlsnum' => $property["mlsID"], "mls" => $property["mls"], "managerID" => $managerID));
						
			if(is_null($property_result)) {
				$nextseq = $collection_counter->findAndModify(
					 array("desc" => "propertyID"),
					 array('$inc' => array('maxCount' => 1)),
					 null,
					 array(
						"new" => true
					)
				);
				
				
				$questions = array();
				
				if($manager_result["brokerageID"] != "N/A") {
					$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
					$brokerage_result = $collection_brokerages->findOne(array("brokerageID" => $manager_result["brokerageID"]));
					$questions = $brokerage_result["questions"];
					$autoEmail = "YES";
				} 
				
				$document_property = array(
									"propertyID" => $nextseq["maxCount"],
									"mls" => $property["mls"],
									"mlsnum" => $property["mlsID"],
									"url" => "", 
									"title"=> $property["addr1"] , 
									"addr1"=> $property["addr1"] ,
									"addr2"=> $property["addr2"] , 
									"image"=> $property["photo"] ,
									"price" => $property["price"] ,
									"beds" => $property["beds"] , 
									"baths" => $property["baths"] , 
									"dimens" => $property["size"] ,
									"desc" => "",
									//"note" => $note,
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"tlc" => "N/A",
									"questions" => $questions,
									"managerID"=>$managerID,
									"brokerageID"=>$manager_result["brokerageID"],
									"anon"=>"NO",
									"autoEmail" => "NO",
									"cnEnabled" => "NO",
									"brokersEnabled" => "NO",
									"disabled" => "NO",
									"mobile" => true,
									"editable"=>true,
									"sample" => false);
									
		  		$result = $collection_properties->insert($document_property);
				$count++;
				
			}
		}
		$rets->Disconnect();
		
		return $authorized;
		
	}
	
	function getRALDListing($listingID) {
		$config = new \PHRETS\Configuration;
		$config->setLoginUrl('http://rets-rald.realtyserver.com:6103/saskatchewan/rets/login');
		$config->setUsername('rald_spacio');
		$config->setPassword('seXQG4mUPX');
		
		// optional.  value shown below are the defaults used when not overridden
		$config->setRetsVersion('1.7.2'); // see constants from \PHRETS\Versions\RETSVersion
		$config->setHttpAuthenticationMethod('digest'); // or 'basic' if required 
		$config->setOption('use_post_method', true); // boolean
		$config->setOption('disable_follow_location', false); // boolean
		
		$rets = new \PHRETS\Session($config);
		
		$bulletin = $rets->Login();

		$results = $rets->Search("Property", "ResidentialProperty", 'id='.$listingID, ['Limit' => 1]);
		
		$property = array();
		foreach ($results as $r) {
			$randnum = rand ( 1 , 10 );
			$image = "http://www.spac.io/assets/img/stock/".$randnum.".jpg";
						
			$property["mlsID"] = $r['id'];
			$property["photo"] = $image;
			
			$streetNumber = $r['street_number'];
			$streetName = $r['street_name'];
			$streetName = ucwords(strtolower($streetName));
			$unitNumber = $r['unit_number'];
			$state = "Saskatchewan";
			$postalCode = $r['postal_code'];
			$size = $r['sqft_finished'];
			$price = $r['price_current'];
			$baths = $r['bathrooms'];
			$beds = $r['bedrooms'];
			
			$addr1 = "";
			$addr2 = $state." ".$postalCode;
			
			$addr1 = $streetNumber." ".$streetName;
						
			if($unitNumber != "" && $unitNumber != NULL) {
				$addr1 = $addr1." Unit ".$unitNumber;
			}
			
			$property["addr1"] = $addr1;
			$property["addr2"] = $addr2;
			$property["price"] = $price;
			$property["size"] = $size;
			$property["baths"] = $baths;
			$property["beds"] = $beds;
			
			//$photos = $rets->Search("Media", "Media", 'ResourceRecordKeyNumeric='.$r['ListingKeyNumeric'], ['Limit' => 1]);

			$property["photo"] = $image;
		
			//var_dump($r);
			//echo "\n\n";
		}

		$rets->Disconnect();
		return $property;
	}

?>