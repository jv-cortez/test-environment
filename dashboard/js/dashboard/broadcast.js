function showBroadcast(pid) {
    if(online) {
        $("#broadcast-bombbomb").prop('checked', false);
        if (user["type"] != "freemium" && user["type"] != "lite") {
            curPID = pid;
            if("bombbombKey" in user["integrations"]) {
              if(user["integrations"]["bombbombKey"] != "") {
                $(".broadcast-bombbomb-option").show();
              } else {
                $(".broadcast-bombbomb-option").hide();
              }
            } else {
              $(".broadcast-bombbomb-option").hide();
            }
            filterBroadcastLeads(true);
        } else {
            showAlert("Please Upgrade","Go Pro today to broadcast your Registrants.","Upgrade","Cancel",function(){showUpgradePage();});
        }

    } else {
        showAlert("Offline Mode","This function requires a live internet connection.");
    }
}

function selectBroadcastTemplate() {
    var type = $("#broadcast-email-template").val();
    $(".broadcast-cc-option").show();
    $(".broadcast-small-caption").show();
    $(".bombbomb-videos").hide();

    if(type == "none") {
         $('#broadcast-message').trumbowyg('html', "{{name}},<br/><br/>ENTER MESSAGE HERE.<Br/><Br/>{{agent_name}}<Br/>{{agent_title}}<Br/>{{agent_company}}<Br/>{{agent_phone}}<Br/>{{agent_email}}");
    } else {
        getEmailTemplate(type, function(data){
            $('#broadcast-message').trumbowyg('html', data.emailtemplate["messagebody"]);
        })
    }
}

function filterBroadcastLeadTypeChanged() {
    var broadcast_filter_type = $("input[name='broadcast-filter-type']:checked").val();

    if(broadcast_filter_type == "Visitors") {
        $("#broadcast-filter-representation-column").show();
        $("#broadcast-filter-client-column").hide();
    }

    if(broadcast_filter_type == "Brokers") {
        $("#broadcast-filter-representation-column").hide();
        $("#broadcast-filter-client-column").show();
    }

    if(broadcast_filter_type == "Visitor Representative") {
        $("#broadcast-filter-representation-column").hide();
        $("#broadcast-filter-client-column").hide();
    }



}


function filterBroadcastLeads(reload, sort, order) {
     if (typeof reload === "undefined" || reload === null) {
        reload = true;
    }
    if (typeof sort === "undefined" || sort === null) {
        sort = "dateCreated";
    }
    if (typeof order === "undefined" || order === null) {
        order = "dsc";
    }

    var broadcast_filter_type = $("input[name='broadcast-filter-type']:checked").val();
    var broadcast_filter_representation = $("input[name='broadcast-filter-representation']:checked").val();
    var broadcast_filter_client = $("input[name='broadcast-filter-client']:checked").val();

    var broadcastType, broadcastRepresentation, broadcastClient;

    var advanced_filter_settings = "";

    advanced_filter_settings += broadcast_filter_type;

    if (broadcast_filter_representation != "All" && broadcast_filter_type == "Visitors") {
        advanced_filter_settings += ", "+broadcast_filter_representation;
    }

    if (broadcast_filter_client != "All" && broadcast_filter_type == "Brokers") {
        advanced_filter_settings += ", "+broadcast_filter_client;
    }

    $("#broadcast-advanced-filter-settings").html(advanced_filter_settings);
    hideAdvancedFilter();

    if(broadcast_filter_type == "Visitors") {
        broadcastType = "visitors";
        $("#popup-broadcast-message .hasagent-header").show();
        $("#popup-broadcast-message .hasclient-header").hide();
    } else if (broadcast_filter_type == "Brokers") {
        broadcastType = "brokers";
        $("#popup-broadcast-message .hasagent-header").hide();
        $("#popup-broadcast-message .hasclient-header").show();
    } else if (broadcast_filter_type == "Visitor Representative") {
        broadcastType = "agents";
        $("#popup-broadcast-message .hasagent-header").hide();
        $("#popup-broadcast-message .hasclient-header").hide();
    }

    $("input[name='broadcast-lead-type']").val(broadcastType);

    if(broadcast_filter_representation == "All") {
        broadcastRepresentation = "all";
    } else if (broadcast_filter_representation == "Represented") {
        broadcastRepresentation = "YES";
    } else if (broadcast_filter_representation == "Not Represented") {
        broadcastRepresentation = "NO";
    }
    if(broadcast_filter_client == "All") {
        broadcastClient = "all";
    } else if (broadcast_filter_client == "Has Client") {
        broadcastClient = "YES";
    } else if (broadcast_filter_client == "No Client") {
        broadcastClient = "NO";
    }

    if(reload) { //Grab data from server
        loading(true);
        getRegistrantsByPID(curPID, "all", function(){
            selectBroadcastTemplate();
            showPopup("broadcast-message");
            if(properties[curPID]["disabled"] == "NO") {
                loadBroadcastLeads(registrants, broadcastType, broadcastRepresentation, broadcastClient, sort, order);
            } else {
                loadBroadcastLeads(registrants_inactive, broadcastType, broadcastRepresentation, broadcastClient, sort, order);
            }
            loading(false);
        });
    } else { //Don't need to grab data from server
        if(properties[curPID]["disabled"] == "NO") {
            loadBroadcastLeads(registrants, broadcastType, broadcastRepresentation, broadcastClient, sort, order);
        } else {
            loadBroadcastLeads(registrants_inactive, broadcastType, broadcastRepresentation, broadcastClient, sort, order);
        }
    }
}

function loadBroadcastLeads(leadsList,broadcastType,broadcastRepresentation,broadcastClient,sort, order) {

    var num_buyers = 0;
    var num_brokers = 0;
    var num_agents = 0;
    var checked = "";

    $(".broadcast-registrants").html("");
    var count = 0;
    var sortedBroadcastLeadsList = [];

    console.log("Loading: "+broadcastType);
    console.log("List: "+JSON.stringify(leadsList));

    $.each(leadsList, function(index, value) {
        value["rid"] = index;

        if((value["pid"] == curPID)) {
            if(broadcastType == "brokers") {
                $(".non-broker").hide();
                $('option:selected', 'select[name="broadcast-email-template"]').removeAttr('selected');
                $('select[name="broadcast-email-template"]').find('option[value="none"]').attr("selected",true);
                selectBroadcastTemplate();
                var hasClient = "NO";

                if(value["answersObj"][0]["answer"] == "YES") {
                    hasClient = "YES";
                } else if(value["answersObj"][0]["answer"] == "NO") {
                    hasClient = "NO";
                } else {
                    hasClient = "NO";
                }
                if(broadcastClient != "all") {
                  checked = "checked";
                }
                if(value["isBroker"] == "YES" && (broadcastClient == "all" || broadcastClient == hasClient)) {
                    sortedBroadcastLeadsList.push(value);
                }
            } else if(broadcastType == "visitors") {
                $(".non-broker").show();

                if(broadcastRepresentation != "all" ) {
                  checked = "checked";
                }
                if(value["isBroker"] == "NO"  && (broadcastRepresentation == "all" || broadcastRepresentation == value["hasAgent"] || (broadcastRepresentation == "NO" && value["hasAgent"] == "UNKNOWN"))) {
                    sortedBroadcastLeadsList.push(value);
                }
            } else if(broadcastType == "agents") {
                $(".non-broker").hide();
                $('option:selected', 'select[name="broadcast-email-template"]').removeAttr('selected');
                $('select[name="broadcast-email-template"]').find('option[value="none"]').attr("selected",true);
                selectBroadcastTemplate();
                checked = "checked";
                if(value["isBroker"] == "NO" && value["hasAgent"] == "YES" && value["agent_name"] != "" && value["agent_contact"] != "") {
                    sortedBroadcastLeadsList.push(value);
                }
            }
        }

    });

    if(sort == "dateCreated") {
        sortedBroadcastLeadsList = customSortDateTime(sortedBroadcastLeadsList,sort,order);
    } else  {
        sortedBroadcastLeadsList = customSort(sortedBroadcastLeadsList,sort,order);
    }

    $.each(sortedBroadcastLeadsList, function(index, value) {
        count++;
        var rid = value["rid"];
        var name = capitalizeFirstLetterEachWordSplitBySpace(value["name"]);
        var agent_name = "";
        if("agent_name" in value) {
            agent_name = capitalizeFirstLetterEachWordSplitBySpace(value["agent_name"]);
        }
        var agent_email = value["agent_contact"];
        var phone = value["phone"];
        var email = value["email"];
        var company = "";

        if("company" in value) {
            company = value["company"];
        }

        var datetime = new Date();
        if(typeof value["dateCreated"]["sec"] != "undefined") {
            datetime = new Date(value["dateCreated"]["sec"] * 1000);
        }

        if(broadcastType == "brokers") {
           var hasClient = "UNKNOWN";

            if(value["answersObj"][0]["answer"] == "YES") {
                hasClient = "YES";
            }
            if(value["answersObj"][0]["answer"] == "NO") {
                hasClient = "NO";
            }

            $(".broadcast-registrants").append("<label for='registrant-"+rid+"'><div  class='row'><div class='checkbox-column column'><input id='registrant-"+rid+"' "+checked+" type='checkbox' name='broadcast-registrant' value='"+rid+"' class='broadcast-registrant'/></div><div class='name-column column'> "+name+"</div><div class='email-column column'>"+email+"</div><div class='hasclient-column column'>"+hasClient+"</div><div class='date-column column'>"+datetime.toLocaleDateString()+"</div></div></label>");

        } else if(broadcastType == "visitors") {
            var temp_hasAgent = value["hasAgent"];
            var temp_hasAgentClass = "unknownAgent";
            if (typeof temp_hasAgent == "undefined") {
                temp_hasAgent = "N/A";
            } else {
                if (temp_hasAgent == "NO") {
                    temp_hasAgentClass = "noAgent";
                }
                if (temp_hasAgent == "YES") {
                    temp_hasAgentClass = "hasAgent";
                }
            }

            $(".broadcast-registrants").append("<label  for='registrant-"+rid+"'><div class='row'><div class='checkbox-column column'><input "+checked+" type='checkbox' name='broadcast-registrant' value='"+rid+"' class='broadcast-registrant '  id='registrant-"+rid+"'/></div><div class='name-column column'> "+name+"</div><div class='email-column column'>"+email+"</div><div class='hasagent-column column'>"+temp_hasAgent+"</div><div class='date-column column'>"+datetime.toLocaleDateString()+"</div></div></label>");
        } else if(broadcastType == "agents") {
            console.log("Showing agents");
            console.log(agent_name);
            console.log(agent_email);
            if(isValidEmailAddress(agent_email) && agent_name != "" && agent_name != null) {
                $(".broadcast-registrants").append("<label for='registrant-"+rid+"'><div class='row'><div class='checkbox-column column'><input "+checked+" type='checkbox' name='broadcast-registrant' value='"+rid+"' class='broadcast-registrant' id='registrant-"+rid+"'/></div><div class='name-column column'> "+agent_name+"</div><div class='email-column column'>"+agent_email+"</div><div class='date-column column'>"+datetime.toLocaleDateString()+"</div></div></label>");
            }
        }
    });
}

function showBroadcastSortedRegistrantsByType(sort, order) {

    var num_buyers = 0;
    var num_brokers = 0;

    var type = $("#broadcast-registrants-type-filter").val();

    if(type == "buyers") {
        $(".buyers-header").show();
        $(".brokers-header").hide();
    } else if(type == "brokers") {
        $(".buyers-header").hide();
        $(".brokers-header").show();
    }

    var pid = curPID;

    if (typeof sort === "undefined" || sort === null) {
        sort = "dateCreated";
    }

    if (typeof order === "undefined" || order === null) {
        order = "desc";
    }

    var showBrokers = false;

    if (type == "buyers") {
        showBrokers = false;
    }

    if (type == "brokers") {
        showBrokers = true;
    }

    //Clear Registrants panel
    var count = 0;

    var sorted_registrants = [];

    $.each(registrantsSearchResult, function(index, value) {
        value["rid"] = index;
        if(value["isBroker"] == "YES") {
                if((value["pid"] == pid || pid == "all")){
                    num_brokers++;
                }

            }
            if(value["isBroker"] == "NO") {
                if((value["pid"] == pid || pid == "all")){
                    num_buyers++;
                }
            }
        if(type == "brokers") {
            if(value["isBroker"] == "YES") {
                sorted_registrants.push(value);
            }
        } else if(type == "buyers") {
            if(value["isBroker"] == "NO") {
                sorted_registrants.push(value);
            }
        }
    });

    if(sort == "dateCreated") {
        sorted_registrants = customSortDateTime(sorted_registrants,"dateCreated",order);
    } else if (sort == "hasClient") {
        sorted_registrants = customSortHasClient(sorted_registrants,"hasClient",order);
    } else  {
        sorted_registrants = customSort(sorted_registrants,sort,order);
    }

    $(".broadcast-registrants").html("");
    $.each(sorted_registrants, function(i, value) {
        var index = value["rid"];
        var isBroker = false;

        if("isBroker" in value) {
            if (value["isBroker"] == "YES") {
                isBroker = true;
            }
        }

        if((value["pid"] == pid || pid == "all") && (isBroker == showBrokers)) {

            count++;
            var name = capitalizeFirstLetterEachWordSplitBySpace(value["name"]);
            var phone = value["phone"];
            var email = value["email"];
            var company = "";

            if("company" in value) {
                company = value["company"];
            }

            var score = 0;
            var hasName = false;
            var hasEmail = false;
            var hasPhone = false;
            var verified = "unverified";

            var datetime = new Date(value["dateCreated"]["sec"] * 1000);
            if(isBroker) {
                var hasClient = "--";
                var temp_hasClientClass = "unknownClient";

                if(value["answersObj"][0]["answer"] == "YES") {
                    hasClient = "Yes";
                    temp_hasClientClass = "hasClient";
                }
                if(value["answersObj"][0]["answer"] == "NO") {
                    hasClient = "No";
                    temp_hasClientClass = "noClient";
                }

                $(".broadcast-registrants").append("<div id='registrant-"+index+"' class='row' onclick='toggleInnerCheckbox(this,event)'><div class='checkbox-column column'><input type='checkbox' name='broadcast-registrant' value='"+index+"' class='broadcast-registrant "+temp_hasClientClass+"'/ onclick='toggleCheckbox(this,event)'></div><div class='name-column column'> "+name+"</div><div class='email-column column'>"+email+"</div><div class='date-column column'>"+datetime.toLocaleDateString()+"</div><div class='hasagent-column column'>"+hasClient+"</div></div>");

            } else {
                var temp_hasAgent = value["hasAgent"];
                var temp_hasAgentClass = "unknownAgent";
                if (typeof temp_hasAgent == "undefined") {
                    temp_hasAgent = "N/A";
                } else {
                    if (temp_hasAgent == "NO") {
                        temp_hasAgentClass = "noAgent";
                    }
                    if (temp_hasAgent == "YES") {
                        temp_hasAgentClass = "hasAgent";
                    }
                }

                $(".broadcast-registrants").append("<div id='registrant-"+index+"' class='row' onclick='toggleInnerCheckbox(this, event)'><div class='checkbox-column column'><input type='checkbox' name='broadcast-registrant' value='"+index+"' class='broadcast-registrant "+temp_hasAgentClass+"'onclick='toggleCheckbox(this,event)'/></div><div class='name-column column'> "+name+"</div><div class='email-column column'>"+email+"</div><div class='hasagent-column column'>"+temp_hasAgent+"</div><div class='date-column column'>"+datetime.toLocaleDateString()+"</div></div>");
            }

        }
    });

    $(".num-visitors").html(num_buyers);
    $(".num-brokers").html(num_brokers);

}

function selectAll(panel) {

  if(panel == "broadcast") {
    $('.broadcast-registrants input:checkbox').prop('checked', true);
  }

}

function deselectAll(panel) {
  if(panel == "broadcast") {
      $('.broadcast-registrants  input:checkbox').prop('checked', false);

  }

}
