<?php
  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  require_once('../oh/config/db_conn.php');
  require_once('../oh/require/core.php');

  $_propertyID = $_GET["u"];
  $_propertyID = (int)$_propertyID;

  $collection_properties = Db_Conn::getInstance()->getConnection()->properties;

  $property_result = $collection_properties->findOne(array('propertyID' => $_propertyID));

  list($width, $height) = getimagesize($property_result["image"]);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <!-- Facebook meta tags -->
  <meta property="fb:app_id" content="255792991253800" />
  <meta property="og:url" content="http://stage.spac.io/property/?u=<?php echo $_propertyID ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $property_result["title"] ?>" />
  <meta property="og:description" content="<?php echo $property_result["desc"] ?>" />
  <meta property="og:image" content="<?php echo $property_result["image"] ?>" />
  <meta property="og:site_name" content="Spacio: Introducing the Paperless Open House" />
  <meta property="og:image:height" content="<?php echo $height ?>" />
  <meta property="og:image:width" content="<?php echo $width ?>" />

  <!-- script for the twitter crawler -->
  <script>window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
      t._e.push(f);
    };

    return t;
  }(document, "script", "twitter-wjs"));</script>

  <!-- Twitter meta tags -->
  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:title" content="<?php echo $property_result["title"] ?>" />
  <meta name="twitter:description" content="<?php echo $property_result["desc"] ?>" />
  <meta name="twitter:image" content="<?php echo $property_result["image"] ?>" />

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="assets/img/favicon.png">

  <title>Spacio: Introducing the Paperless Open House</title>

  <link href="../assets/css/normalize.css" rel="stylesheet">
  <link href="../assets/css/property.css" rel="stylesheet">
  <link href="../assets/fonts/fonts.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="http://www.spac.io/assets/img/favicon.png"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

</head>

<body style="margin:0;padding:0;">

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <div class="spacio-top-menu">
    <div class="web-menu">
      <a href="/">
         <div class="spacio-top-menu-logo">
           <img src="/assets/img/website/spacio_logo_long.png" height="40">
         </div>
      </a>
    </div>
  </div>


  <div class="listing-image" style="background-image:none;">
    <div class="sharing-buttons">
      <!-- twitter sharing button -->
      <div class="twitter-button" id="twitter-button-1"></div>

      <!-- Facebook sharing button -->
      <div class="fb-share-button" data-href="http://stage.spac.io/property/?u=<?php echo $_propertyID ?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fstage.spac.io%2Fproperty%2F%3Fu%3D<?php echo $_propertyID ?>&amp;src=sdkpreparse">Share</a></div>
    </div>
    <div class="listing-info">
      <div class="title"></div>
      <div class="addr1"></div>
    </div>
  </div>

  <div class="main-container">

    <div class="main-left-container">

      <div class="agent-info">
        <div class="pphoto-container">
          <div class="pphoto" style="background-image:url('/assets/img/website/pphoto.jpg');"></div>
        </div>
        <div class="agent-contact-info">
          <div>
            <div class="name"><span class="fname"></span> <span class="lname"></span></div>
            <div class="email"></div>
            <div class="phone-number">
              <a class="phone"></a>
            </div>
            <div class="phone-number">
              <a class="mobile"></a>
            </div>
          </div>
        </div>
      </div><!-- end of agent-info -->

      <div class="brokerage-info-section">
        <div class="brokerage-logo-container">
          <div class="brokerageLogo"></div>
        </div>
        <div class="brokerage-contact-info">
          <div class="brokerageName"></div>
          <div class="office"></div>
          <div class="bWebsite">
            <a class="brokerageDomain"></a>
          </div>
        </div>
      </div><!-- end of brokerage-info-section  -->

      <!-- this will be the same as main-right-container, will only be visible in mobile  -->
      <div class="mobile-listing-info">
        <div class="stats-title"><strong>Property details:</strong></div>
        <div class="listing-stat">
          <div class="listing-stat-label">Beds:</div>
          <div class="listing-value beds"></div>
        </div>
        <div class="listing-stat">
          <div class="listing-stat-label">Baths:</div>
          <div class="listing-value baths"></div>
        </div>
        <div class="listing-stat">
          <div class="listing-stat-label">Size:</div>
          <div class="listing-value">
            <span class="dimens" ></span> <span class="measurement"></span>
          </div>
        </div>
        <div class="listing-stat">
          <div class="listing-stat-label">Price:</div>
          <div class="listing-value">
            $<span class="price"></span> <span class="currency" ></span>
          </div>
        </div>
        <div class="listing-stat-desc">
          <div class="listing-stat-label-desc">Description:</div>
          <div class="listing-value-desc"></div>
        </div>
      </div> <!-- end of mobile-listing-info -->

      <div class="agent-more-info">
        <div class="see-more-listings-button">
          <a href="" class="see-more-listings" target="
        _blank">
            <div>
              <div class="agent-more-info-button-icon"><i class="fa fa-building fa-lg" aria-hidden="true"></i></div>
              <div class="agent-more-info-button-text">See more listings</div>
            </div>
          </a>
        </div>
        <div class="request-info-button">
          <a href="" class="request-info" target="_blank">
            <div>
              <div class="agent-more-info-button-icon"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></div>
              <div class="agent-more-info-button-text">Request info</div>
            </div>
          </a>
        </div>
      </div> <!-- end of agent-more-info -->

      <div class="mobile-sharing-buttons">
        Share Property:
        <!-- twitter sharing button -->
        <div class="twitter-button" id="twitter-button-2"></div>

        <!-- Facebook sharing button -->
        <div class="fb-share-button" data-href="http://stage.spac.io/property/?u=<?php echo $_propertyID ?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fstage.spac.io%2Fproperty%2F%3Fu%3D<?php echo $_propertyID ?>&amp;src=sdkpreparse">Share</a></div>
      </div>

    </div> <!-- end of main-left-container  -->

<!-- this container will only be visible in desktop view -->
<!-- the container will be the same content as mobile-main-right-container  -->
    <div class="main-right-container">
      <div class="main-right-title"><strong>Property details:</strong></div>
      <div class="listing-stat">
        <div class="listing-stat-label">Beds:</div>
        <div class="listing-value beds"></div>
      </div>
      <div class="listing-stat">
        <div class="listing-stat-label">Baths:</div>
        <div class="listing-value baths"></div>
      </div>
      <div class="listing-stat">
        <div class="listing-stat-label">Size:</div>
        <div class="listing-value">
          <span class="dimens"></span> <span class="measurement"></span>
        </div>
      </div>
      <div class="listing-stat">
        <div class="listing-stat-label">Price:</div>
        <div class="listing-value">
          $<span class="price"></span> <span class="currency" ></span>
        </div>
      </div>
      <div class="listing-stat-desc">
        <div class="listing-stat-label-desc">Description:</div>
        <div class="listing-value-desc"></div>
      </div>
    </div> <!-- end of main-right-container -->

  </div> <!-- end of main-container  -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/assets/js/website.js"></script>
<script type="text/javascript">

  var wsURL = "../oh/webservice/1.1.5/webservice.php";

	$(document).ready(function(){
    getListing();
  })

  function getListing(completionHandler) {
      var _query = {};

      _query["fn"] = "getListingInfo";
      _query["propertyID"] = "<?php echo $_propertyID?>";

      var json_data = _query;

      console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

      console.log(json_data);

      $.ajax({
        url: "http://ws.spac.io/stage/oh/webservice_listing/1.0.0/webservice_listing.php",
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){

          console.log(data);

          if (data.status == 1) {
            console.log("Returned JSON Object: "+JSON.stringify(data));

            $(".propertyID").html("<?php echo $_propertyID?>");
            $(".brokerageName").html(data.brokerageName);
            if (data.brokerageDomain !== ""){
              $(".brokerageDomain").html(data.brokerageDomain);
              $(".brokerageDomain").attr('href', 'http://www.'+data.brokerageDomain);
            }
            $(".office").html(data.office);
            $(".brokerageLogo").attr('style', 'background-image: url('+data.brokerageLogo+')');
            $(".title").html(data.title);
            $(".addr1").html(data.addr1);
            $(".addr2").html(data.addr2);
            $(".beds").html(data.beds);
            $(".baths").html(data.baths);
            $(".dimens").html(data.dimens);
            $(".measurement").html(data.measurement);
            $(".listing-value-desc").html(data.desc);
            $(".url").html(data.url);
            $(".currency").html(data.currency);
            $(".price").html(data.price);
            $(".lastEdited").html(data.lastEdited);
            $(".fname").html(data.fname);
            $(".lname").html(data.lname);
            $(".email").html(data.email);

            if (data.image !== ""){
              $(".listing-image").attr('style', 'background-image: url('+data.image+')');
            } else {
              $(".listing-image").attr('style', 'height: 140px');
            }

            if (data.phone !== ""){
              $(".phone").html('P: '+data.phone);
              $(".phone").attr('href', 'tel:+1-'+data.phone);
            }
            if (data.mobile !== ""){
              $(".mobile").html('M: '+data.mobile);
              $(".mobile").attr('href', 'tel:+1-'+data.mobile);
            }
            $(".pphoto").attr('style', 'background-image: url('+data.pphoto+')');
            $(".see-more-listings").attr('href', 'http://stage.spac.io/agent/?g='+data.ukey);
            $(".request-info").attr('href', 'https://www.spac.io/book/' +data.pphoto);

// This function creates the share button and specifies the default tweet text.
            var url = window.location.href+'';
            twttr.widgets.createShareButton(
            url,
            document.getElementById('twitter-button-1'),
            {
              count: 'none',
              text: 'Check out this new listing',
              size: 'large'
            }).then(function (el) {
              console.log("Button created.");
              twttr.widgets.load();

            });
            twttr.widgets.createShareButton(
            url,
            document.getElementById('twitter-button-2'),
            {
              count: 'none',
              text: 'Check out this new listing',
              size: 'large'
            }).then(function (el) {
              console.log("Button created.");
              twttr.widgets.load();

            });
            twttr.widgets.load();

            if ($.isFunction(completionHandler)) {
              completionHandler();
            }

          } else {

          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getListing error Error: "+errorThrown);
          loading(false);
        },
        dataType: "json"
      });
    }

</script>

</body>
</html>
