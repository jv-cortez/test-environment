<?php

require_once('../../require/vendor/autoload.php');

/*
System_Email::brokerageTrialEnd('Chris','clerch@spac.io');
System_Email::accountDowngradeProToLite('Chris','clerch@spac.io');
System_Email::passwordReset('Chris','clerch@spac.io');
System_Email::accountUpgrade('Chris','clerch@spac.io');
System_Email::broadcastEmailFail('Chris','clerch@spac.io');
System_Email::accountConfirmationRequest('Chris','clerch@spac.io');
System_Email::confirmNewEmail('Chris','clerch@spac.io');
System_Email::requestPaymentUpdate('Chris','clerch@spac.io','Visa',1234);
System_Email::resendRequestPaymentUpdate('Chris','clerch@spac.io','Visa',1234);
System_Email::sendSpacioInvoice('Chris','Lerch','clerch@spac.io',1234,'16.22','1234556','01-08-2015 12:50 GMT','01-03-2016 12:50 GMT');
System_Email::sendCardExpiring('Chris','clerch@spac.io','Visa',1234,'2016-05-04 12:50 GMT');
*/

class System_Email{

  static function brokerageTrialEnd($fname,$email){

    $brokeragename = 'Halstead';
    $pricing_link = 'http://spac.io/pricing/';
    $dashboard_link = 'http://spac.io/dashboard';


    $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
    $message = array(
    'html' => '<html>
      <body>
        <div style="background:#f7f7f7;padding:20px;">
          <img src="http://spac.io/assets/img/nav-logo.png" height="20">
          <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
            <p>Hello '.$fname.',</p>
            <p>
            Your Spacio account has just been downgraded to Spacio Lite, which is a part of our Brokerage Offering for '.$brokeragename.' after the initial trial period. Spacio Lite is available free of charge for '.$brokeragename.'\'s agents.
            </p>
            <p>
            We hope you will continue to using Spacio Lite to automate your open houses. If you’d like to learn more about our Lite version and its functionalities for your future open houses, please refer to this <a href='.$pricing_link.' style="color:#999;">page</a>.
            </p>
            <p>
            If you’d like to upgrade to continue using all of Spacio’s features, you may do so for only $15/month. Login to your Spacio account and upgrade <a href='.$dashboard_link.' style="color:#999;">here</a>.
            </p>
            <p>
            If you’ve upgraded and received this email in error, please reply to this email as soon as possible.
            </p><br/>
            <p>Thank you,</p>
            <p>Melissa<br/>
            Co-founder & CEO</p>
          </div>
        </div>
      </body>
    </html>',
            'subject' => 'Your Spacio Account Has Been Downgraded to Spacio Lite',
            'from_email' => 'hello@spac.io',
            'from_name' => 'Melissa at Spacio',
            'to' => array(
              array(
                'email' => $email
              )
            ),
            'headers' => array('Reply-To' => 'melissa@spac.io'),
            'important' => false,
            'tags' => array('end-of-brokerage-trial')
    );
    $async = true;
    $result = $mandrill->messages->send($message, $async);
  }


static function accountDowngradeProToLite($fname,$email){

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          Your Spacio account has just been downgraded, and your credit card will no longer be charged. We are sorry to see you go, and thank you for giving us a try.
          </p>
          <p>
          Can I ask you for a favor? We\'re always looking for ways to make Spacio better, and we\'d love to hear from you. Do you mind telling me why you downgraded?
          </p>
          <p>
          If you did not downgrade your account, please reply to this email and let us know.
          </p><br/>
          <p>Thank you,</p>
          <p>Melissa<br/>
          Co-founder & CEO</p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Your Spacio Account Has Been Downgraded',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Melissa at Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'melissa@spac.io'),
          'important' => false,
          'tags' => array('account-downgraded-to-lite')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function passwordReset($fname,$email,$managerID,$rtoken){

  $reset_link = 'http://www.spac.io/resetpassword.php?e='.$managerID.'&r='.$rtoken;

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          We received a request to reset your Spacio account password.
          </p>
          <p>
          Click here to reset your password: <a href='.$reset_link.'   style="color:#999;">'.$reset_link.'</a>
          </p>
          <p>
          If you did not make this request, please disregard this email. Your password will remain the same.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Password Reset Request',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('password-reset')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}


static function accountUpgrade($fname,$email){

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          Thanks for upgrading your Spacio account! We really appreciate it and hope you enjoy Spacio’s extended functionalities.
          </p>
          <p style="margin-bottom:0;">
          <b>Here’s what you can do with Spacio:</b><br/>
          </p>
          <ul style="margin-top:0;">
            <li>Export leads into your CRM or other marketing software for newsletters and events.</li>
            <li>Customize your auto email message and signature.</li>
            <li>Integrate with your favorite services like Contactually, RealSatisfied, Follow Up Boss, and lots more!</li>
            <li>Stay connected by broadcasting messages to buyers with updates and other similar properties you list.</li>
            <li>Impress your clients by generating beautiful seller reports at the click of a button.</li>
          </ul>
          <p>
          To see this upgrade reflected in your account, you may have to log out of your account on both the website and iPad app and login again.
          </p>
          <p>
          Spacio is here to help you automate your open houses. Let us know if there’s anything we can do to make Spacio better for you. We look forward to hearing from you.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Your Spacio Account Has Been Upgraded!',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('account-upgrade')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}


static function broadcastEmailFail($fname,$email){

  $failed_email = 'ting@spac.io';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          Your Spacio broadcast message to <a href='.$failed_email.' style="color:#999;">'.$failed_email.'</a> failed to deliver.
          </p>
          <p>
          Please check the accuracy of this email address and try again.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Your Spacio Broadcast Message Failed to Deliver',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('broadcast-email-failed')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function accountConfirmationRequest($fname,$email){
// Resend Confirmation email – 3 days, 12 days

  $activate_link = 'www.linktoactivate.com';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          You recently signed up for a Spacio account to help automate your open houses. Just a quick reminder that we need to confirm your email address before you can run visitor registration at your open house using Spacio.
          </p>
          <p>
          All you have to do to confirm is click <a href='.$activate_link.' style="color:#999;">HERE</a>.
          </p>
          <p>
          If you haven’t already, download the Spacio iPad app <a href="http://spac.io/download/" style="color:#999;">HERE</a> to get started.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Your Email Confirmation Required For Spacio',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('account-confirmation')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function confirmNewEmail($fname,$email,$managerID,$vtoken){

  $activate_link = 'http://www.spac.io/confirmemail.php?e='.$managerID.'&v='.$vtoken;

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          You\'ve just changed your account email.
          </p>
          <p>
          Please confirm this new email by clicking  <a href='.$activate_link.' style="color:#999;">HERE</a>.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Email Address Changed',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('email-change-confirmation')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function requestPaymentUpdate($fname,$email,$cc_type,$last4){

  $payment_link = 'www.linktopayment.com';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          Sorry for the interruption, but we were unable to process your recent monthly payment for Spacio with your '.$cc_type.' ending in '.$last4.'. Please visit the account payment page at <a href='.$payment_link.' style="color:#999;">'.$payment_link.'</a>  to enter your payment information again or to use a different payment method.
          </p>
          <p>
          You will be able to enjoy full access to Spacio features after you update your payment method. When you have finished, we will try to verify your account again.
          </p>
          <p>
          If you have any questions, please contact us by replying to this email.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Please Update You Payment Method',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('update-payment-request')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function resendRequestPaymentUpdate($fname,$email,$cc_type,$last4){
//Unable to Process Payment; let’s do a wall when they log in but not downgrade. Asking to upgrade again will be a pain; day 3 after if no action

  $payment_link = 'www.spac.io/dashboard/';

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          We apologize for the interruption. This is a reminder to update your payment information.
          </p>
          <p>
          We were unable to authorize your '.$cc_type.' ending in '.$last4.' for your monthly Spacio subscription. Please visit the account payment page at <a href='.$payment_link.'  style="color:#999;">'.$payment_link.'</a> to enter your payment information again or to use a different payment method.
          </p>
          <p>
          When you have finished, we will try to verify your account again. If it still does not work, you may want to contact your credit card company.
          </p>
          <p>
          You will be able to enjoy full access to Spacio features after you update your payment method.
          </p>
          <p>
          If you have any questions, please contact us by replying to this email.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Action Required - Your Payment Method',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('resend-update-payment-request')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function sendSpacioInvoice($fname,$lname,$email,$last4,$amount,$invoiceID,$expiry,$dateStart){

  $timeZone = 'America/New_York';
  date_default_timezone_set($timeZone);

  $full_name = $fname . " " . $lname;

  $dashboard_link = 'http://spac.io/dashboard';


  $dateCurrent = '01-03-2016 12:50 GMT';
  $currentDate = new DateTime($dateCurrent);
  $billingDate = $currentDate;

  $startDate = new DateTime($dateStart);

  $endDate = new DateTime($expiry);

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>
          Hello '.$fname.',
          </p>
          <p>
          Thank you for using Spacio!
          </p>
          <p>
          <b>Here are your invoice details:</b><br/>
          Your card has been charged: $'.$amount.'<br/>
          Your card on file ends with: '.$last4.'<br/>
          Billing date: '.$billingDate->format('j F Y').'<br/>
          Invoice number: '.$invoiceID.'<Br/>
          Bill to: '.$full_name.'<br/>
          Description: Spacio Pro Plan ($15/month)<br/>
          Duration: '.$startDate->format('j F Y').' - '.$endDate->format('j F Y').'<br/><Br/>
          This charge will appear on your credit card statement as Spacio.<br/><Br/>

          To manage your account details, please login at <a href='.$dashboard_link.' style="color:#999;">here</a>.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Your Spacio Invoice',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('monthly-invoice')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}

static function sendCardExpiring($fname,$email,$cc_type,$last4,$expiry){
// Credit card expiry date approaching; 15 days before the month
  $timeZone = 'America/New_York';
  date_default_timezone_set($timeZone);

  $dashboard_link = 'http://spac.io/dashboard';

  $expiry_date = new DateTime($expiry);

  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
  $message = array(
  'html' => '<html>
    <body>
      <div style="background:#f7f7f7;padding:20px;">
        <img src="http://spac.io/assets/img/nav-logo.png" height="20">
        <div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
          <p>Hello '.$fname.',</p>
          <p>
          Thank you for using Spacio! We just wanted to remind you that your '.$cc_type.' ending in '.$last4.' is expiring on '.$expiry_date->format('F j').'.
          </p>
          <p>
          Please update your payment method at <a href='.$dashboard_link.' style="color:#999;">http://spac.io/dashboard </a>. We want to ensure you can use Spacio features without interruption, especially for upcoming open houses.
          </p><br/>
          <p>The Spacio Team<br/>
          <a href="hello@spac.io"  style="color:#999;">hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>',
          'subject' => 'Action required: Update your payment method',
          'from_email' => 'hello@spac.io',
          'from_name' => 'Spacio',
          'to' => array(
            array(
              'email' => $email
            )
          ),
          'headers' => array('Reply-To' => 'hello@spac.io'),
          'important' => false,
          'tags' => array('credit-card-expiring')
  );
  $async = true;
  $result = $mandrill->messages->send($message, $async);
}


}
?>
