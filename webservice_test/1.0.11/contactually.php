<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	header('Access-Control-Max-Age: 1000');
	header('Content-type: application/json');


	$data = isset( $_POST['json'] ) ? $_POST['json'] : 0;
	$fn = $data["fn"];


	switch($fn) {
		case "checkKey":
			$_apikey =(string)$data["apikey"];
			
			$url = "https://www.contactually.com/api/v1/users/current.json";
			$query = "?api_key=".$_apikey;

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$url.$query);
			curl_setopt($ch, CURLOPT_POST, 0);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
			$server_output = curl_exec ($ch);
			
			curl_close ($ch);
			
			echo json_encode($server_output ); 	
			break;
		case "getBuckets":
			$_apikey =(string)$data["apikey"];
			
			$url = "https://www.contactually.com/api/v1/groupings.json";
	
			$query = "?type=bucket&api_key=".$_apikey;
		
			$ch = curl_init();
		
			curl_setopt($ch, CURLOPT_URL,$url.$query);
			curl_setopt($ch, CURLOPT_POST, 0);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
			$server_output = curl_exec ($ch);
		
			curl_close ($ch);
			
			//$server_output = array("status" => 1);
			echo json_encode($server_output );
			break;
	
	}

?>
