<?php
	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

  require_once('../oh/config/db_conn.php');
  require_once('../oh/require/core.php');

  $propertyID = $_GET["id"];
  $propertyID = (int)$propertyID;

	$launchform = isset($_GET['lf']) ? $_GET['lf'] : 'y';

  $collection_properties = Db_Conn::getInstance()->getConnection()->properties;
	$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

  $property_result = $collection_properties->findOne(array('propertyID' => $propertyID));
  list($width, $height) = getimagesize($property_result["image"]);

	$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $property_result["brokerageID"]));

  $socialmediatitle = "";

  if($property_result["url"] == "N/A" || $property_result["url"] == "") {
    $socialmediatitle = "Check out my property and inquire/register here! https://spac.io/l/".$propertyID;
  } else {
    $socialmediatitle = "Check out my property at ".$property_result["url"]." and inquire/register here! https://spac.io/l/".$propertyID."";
  }

?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Facebook meta tags -->
    <meta property="fb:app_id" content="255792991253800" />
    <meta property="og:url" content="http://spac.io/l/<?php echo $propertyID ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $property_result["title"] ?>" />
    <meta property="og:description" content="<?php echo $socialmediatitle; ?>" />
    <meta property="og:image" content="<?php echo $property_result["image"] ?>" />
    <meta property="og:site_name" content="Spacio: Introducing the Paperless Open House" />
    <meta property="og:image:height" content="<?php echo $height ?>" />
    <meta property="og:image:width" content="<?php echo $width ?>" />

    <!-- script for the twitter crawler -->
    <script>window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };

      return t;
    }(document, "script", "twitter-wjs"));</script>

    <!-- Twitter meta tags -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?php echo $property_result["title"] ?>" />
    <meta name="twitter:description" content="<?php echo $socialmediatitle ?>" />
    <meta name="twitter:image" content="<?php echo $property_result["image"] ?>" />

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title><?php echo $property_result["addr1"] ?></title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/listing.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">
    <link href="/dashboard/css/font-awesome.min.css" rel="stylesheet">


    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>

    <div class="loading-panel full-overlay">
     <div style="display:table;width:100%;height:100%;">
         <div style="height:50px;display:table-cell;vertical-align:middle;text-align:center;color:white;">
           <span class="msg" style="-webkit-touch-callout:none;-webkit-user-select: none;">Loading...</span>
           </div>
       </div>
   </div> <!---- LOADING PANEL OVERLAY --->

     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->
    <div class="brokerage-logo-mobile"></div>

    <div class="signin-form-overlay">
      <div class="signin-form">
        <form id="propertyReg_Form">

          <div class="form-title-section">
            <span class="main-color-font">
              INQUIRE / REGISTER
            </span>
            <input type="button" value="SUBMIT" class="signin-button main-color-background" onclick="regForProperty()"/>
            <input type="button" value="CANCEL" class="cancel-button main-color-border main-color-font" onclick="closeForm()"/>

          </div>
          <div class="form-content-section">
            <div class="visitor-info-input-row">
              <div class="input-label">NAME</div>
              <input type="text" name="name" value="" id="add-registrant-name" placeholder="Name">
            </div>
            <div class="visitor-info-input-row">
              <div class="input-label">EMAIL</div>
              <input type="email" name="email" value="" id="add-registrant-email" placeholder="Email">
            </div>
            <div class="visitor-info-input-row">
              <div class="input-label">PHONE</div>
              <input type="tel" name="phone" value="" id="add-registrant-phone" placeholder="Phone">
            </div>
            <div class="answers">
    				</div>
            <div class="visitor-info-input-row comments-row">
              <div class="input-label">COMMENTS</div>
              <textarea name="note" value="" id="add-registrant-note" placeholder="Add Comments"></textarea>
            </div>

          </div>
          <div class="bottom-bar-mobile main-color-border">

            <input type="button" value="SUBMIT" class="signin-button-mobile main-color-background" onclick="regForProperty()"/>
            <input type="button" value="CANCEL" class="cancel-button-mobile main-color-font" onclick="closeForm()"/>

          </div>
        </form>
      </div>
    </div>

    <div class="listing-scroll-one">
      <div class="gmap-button-mobile">
        <a href="https://www.google.com/maps/search/?api=1&query=" target="_blank" class="gmapLink">
          <img src="https://developers.google.com/maps/images/lhimages/api/icon_placesapi.svg" height="30" style="margin-top:5px;">
        </a>
      </div>
    </div>

    <div class="left-column">
      <div class="padded-content">
        <div class="brokerage-logo"></div>
        <div class="listing-title main-color-font"></div>
        <div class="listing-address">

        </div>
        <div class="inquire-button main-color-background" onclick="launchForm()">
           INQUIRE / REGISTER
        </div>
        <div class="listing-size"></div>
        <div class="listing-beds"></div>
        <div class="listing-baths"></div>
        <div class="listing-website"></div>
        <div class="gmap-button"><a href="https://www.google.com/maps/search/?api=1&query=" target="_blank" class="gmapLink"><img src="https://developers.google.com/maps/images/lhimages/api/icon_placesapi.svg" height="18" style="position:relative;top:5px;vertical-align:baseline;"> <u>Google Map</u></a></div>

      </div>
      <div class="price-panel main-color-background">
        <div style="font-size:14px;letter-spacing:3px;margin-bottom:3px;">PRICE</div>
        <div class="listing-price"></div>
      </div>
      <div class="padded-content">
        <div style="display:flex;height:60px;width:100%;flex-direction:row;margin-bottom:20px;">
          <div class="agent-pphoto"></div>
          <div class="agent-name-container">
            <div class="agent-name"></div>
            <div class="agent-title"></div>
          </div>
        </div>

        <div class="agent-email"></div>
        <div class="agent-phone"></div>
        <div class="agent-mobile"></div>
        <div class="agent-website"></div>
				<div class="agent-facebook"></div>
				<div class="agent-twitter"></div>
				<div class="agent-instagram"></div>
				<div class="agent-linkedin"></div>

      </div>


      <a href="" target="_blank" id="agent-link">
        <div class="view-more-panel main-color-font">
          <i class="fa fa-home" style="font-size:18px;" aria-hidden="true"></i> See My Other Listings
        </div>
      </a>
    </div>

    <!-- <div class="listing-desc-container">
      <div class="main-color-font listing-desc-title" style="">
        ABOUT THIS PROPERTY
      </div>
      <div class="listing-desc">
      </div>
    </div> -->

    <a href="https://spac.io" target="_blank"><div class="powered-by-spacio">
      POWERED BY <img height="12" src="/assets/img/website/spacio_logo_short.png" style="position:relative;top:1px;"> SPACIO
    </div></a>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
    <script>
    var wsURL = "https://ws.spac.io/prod/oh/webservice_listing/1.0.0/webservice_listing.php";
    var wsURL2 = "https://spac.io/oh/webservice/1.1.6/webservice_listings.php";

    var listing = {};
		var lf = "<?php echo $launchform;?>";

    $(document).ready(function(){
      insertAlertBox();
      getListingInfo("<?php echo $propertyID; ?>", function(){
        loadListingInfo();

				if(lf != "n") {
					launchForm();
				}

      });
    });

    function getListingInfo(propertyID, completionHandler) {
      var _query = {};

			_query["fn"] = "getListingInfo";
			_query["propertyID"] = propertyID;

			loading(true);

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						loading(false);
            listing = data;
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
            showAlert("error","Listing Not Found", "Sorry, this listing no longer exists or is no longer active.");

						//forceLogout();
						//loading(false);
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

    }

    function loadListingInfo() {

      listing["price"] = listing["price"].replace("$", "");


      if(listing["dimens"] == 0 || listing["dimens"] == -1) {
        listing["dimens"] = "Upon Inquiry";
      } else {
        listing["dimens"] = listing["dimens"] + " SQFT";
      }

      if(listing["beds"] == -1) {
        listing["beds"] = "Upon Inquiry";
      }

      if(listing["baths"] == -1) {
        listing["baths"] = "Upon Inquiry";
      }

      if(listing["desc"] == "") {
        $(".listing-desc-container").hide();
      }

      if(listing["url"] == "") {
        $(".listing-website").hide();
      } else {
				listing["url"] = sanitizeURL(listing["url"]);
			}


      if(listing["phone"] == "") {
        $(".agent-phone").hide();
      }
      if(listing["mobile"] == "") {
        $(".agent-mobile").hide();
      }
      if(listing["website"] == "") {
        $(".agent-website").hide();
      } else {
				listing["website"] = sanitizeURL(listing["website"]);
			}

			if(listing["facebook"] == "") {
				$(".agent-facebook").hide();
			} else {
				listing["facebook"] = sanitizeURL(listing["facebook"]);
			}

			if(listing["twitter"] == "") {
				$(".agent-twitter").hide();
			} else {
				listing["twitter"] = sanitizeURL(listing["twitter"]);
			}

			if(listing["instagram"] == "") {
				$(".agent-instagram").hide();
			} else {
				listing["instagram"] = sanitizeURL(listing["instagram"]);
			}

			if(listing["linkedin"] == "") {
				$(".agent-linkedin").hide();
			} else {
				listing["linkedin"] = sanitizeURL(listing["linkedin"]);
			}

      if(listing["addr1"] == "" && listing["addr2"] == "") {

      } else if (listing["addr2"] == "") {
        $(".listing-address").html(listing["addr1"]);
      } else {
        $(".listing-address").html(listing["addr1"]+"<br/>"+listing["addr2"]);
      }

      $(".listing-scroll-one").css("background-image","url("+listing["image"]+")");

      $(".listing-title").html(listing["title"]);
      $(".listing-desc").html(listing["desc"]);

      $(".listing-size").html('<div class="listing-icon"><i class="fa fa-arrows-alt main-color-font" aria-hidden="true"></i></div> Size: '+listing["dimens"]);
      $(".listing-beds").html('<div class="listing-icon"><i class="fa fa-bed main-color-font" aria-hidden="true"></i></div> Beds: '+listing["beds"]);
      $(".listing-baths").html('<div class="listing-icon"><i class="fa fa-bath main-color-font" aria-hidden="true"></i></div> Baths: '+listing["baths"]);
      $(".listing-website").html('<div class="listing-icon"><i class="fa fa-info-circle main-color-font" aria-hidden="true"></i></div> <a href="'+listing["url"]+'" target="_blank"><u>More Info</u></a>');

      $(".listing-price").html("$"+listing["price"]);
      $(".agent-pphoto").css("background-image","url("+listing["pphoto"]+")");
      $(".agent-name").html(listing["fname"]+" "+listing["lname"]);
      $(".agent-title").html(listing["agent_title"]);
      $(".agent-title").html(listing["agent_title"]);
      $("#agent-link").attr("href","https://spac.io/p/"+listing["ukey"]);
      $(".agent-website").html('<div class="listing-icon"><i class="fa fa-user-circle-o main-color-font" aria-hidden="true"></i></div> <a href="'+listing["website"]+'" target="_blank"><u>Agent Website</u></a>');
      $(".agent-email").html('<a href="mailto:'+listing["email"]+'"><div class="listing-icon"><i class="fa fa-envelope main-color-font" aria-hidden="true"></i></div>'+listing["email"]+'</a>');
      $(".agent-phone").html('<a href="tel:+'+listing["phone"]+'"><div class="listing-icon"><i class="fa fa-phone main-color-font" aria-hidden="true"></i></div>'+listing["phone"]+'</a>');
      $(".agent-mobile").html('<a href="tel:+'+listing["mobile"]+'"><div class="listing-icon" style="font-size:16px;"><i class="fa fa-mobile main-color-font" aria-hidden="true"></i></div>'+listing["mobile"]+'</a>');
			// social media black
			$(".agent-facebook").html('<a href="'+listing["facebook"]+'" target="_blank"><div class="listing-icon" style="font-size:16px;"><i class="fa fa-facebook-official" aria-hidden="true"></i></div>Facebook</a>');
			$(".agent-twitter").html('<a href="'+listing["twitter"]+'" target="_blank"><div class="listing-icon" style="font-size:16px;"><i class="fa fa-twitter" aria-hidden="true"></i></div>Twitter</a>');
			$(".agent-instagram").html('<a href="'+listing["instagram"]+'" target="_blank"><div class="listing-icon" style="font-size:16px;"><i class="fa fa-instagram" aria-hidden="true"></i></div>Instagram</a>');
			$(".agent-linkedin").html('<a href="'+listing["linkedin"]+'" target="_blank"><div class="listing-icon" style="font-size:16px;"><i class="fa fa-linkedin" aria-hidden="true"></i></div>LinkedIn</a>');


      var addr = listing["addr1"]+", "+listing["addr2"];
      var enc_addr = encodeURIComponent(addr);

      $(".gmapLink").attr("href","https://www.google.com/maps/search/?api=1&query="+enc_addr);

      if(listing["account_type"] == "brokerage") {
        $(".brokerage-logo").css("background-image","url("+listing["brokerageLogo"]+")");
        $(".brokerage-logo-mobile").css("background-image","url("+listing["brokerageLogo"]+")");
        $(".main-color-border").css("border-color",listing["brokerageColors"]["primary"]);
        $(".main-color-background").css("background-color",""+listing["brokerageColors"]["primary"]);
        $(".main-color-font").css("color",""+listing["brokerageColors"]["primary"]);
      } else {

        $(".brokerage-logo").hide();
        $(".brokerage-logo-mobile").hide();
        $(".main-color-border").css("border-color","black");
        $(".main-color-background").css("background-color","black");
        $(".main-color-font").css("color","black");
      }

    }

    function launchForm() {
      $(".signin-form-overlay").show();
      $("body").css("overflow","hidden");
      $(".form-content-section .answers").html("");
      $.each(listing["questionsObj"], function(index,value) {

          if(value["type"] == "agent") {

              $(".form-content-section .answers").append("<div class='add-registrant-question-row eq1-master'><div class='question'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-registrant-question-"+index+"-yes' type='radio' value='YES' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-yes' onclick='showEQ(1)'>Yes</label></div><div class='checkbox'><input id='add-registrant-question-"+index+"-no' type='radio' value='NO' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-no' onclick='hideEQ(1)'>No</label></div></div></div>");

              $(".form-content-section .answers").append("<div class='add-registrant-question-row eq1-slave'><div class='question'>Agent Name?</div><input name='add-registrant-question-"+index+"a' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Agent Name'></div>");

              $(".form-content-section .answers").append("<div class='add-registrant-question-row eq1-slave'><div class='question'>Agent Contact?</div><input name='add-registrant-question-"+index+"b' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Agent Contact'></div>");

              $(".form-content-section .answers").append("<div class='add-registrant-question-row eq1-slave'><div class='question'>Agent Company?</div><input name='add-registrant-question-"+index+"c' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Agent Company'></div>");
              //$(".answers").append("<div class='visitor-info-question-row'><div class='question'>"+value["question"]+"</div><input name='add-registrant-answer-"+index+"' type='text' id='add-registrant-answer-"+index+"' value=''></div>");

          } else if(value["type"] == "yn") {
            $(".form-content-section .answers").append("<div class='add-registrant-question-row'><div class='question'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-registrant-question-"+index+"-yes' type='radio' value='YES' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='add-registrant-question-"+index+"-no' type='radio' value='NO' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-no'>No</label></div></div></div>");

          } else if(value["type"] == "text"){
             $(".form-content-section .answers").append("<div class='add-registrant-question-row'><div class='question'>"+value["question"]+"</div><input name='add-registrant-question-"+index+"' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Answer'></div>");

          } else if (value["type"] == "mc") {
              $(".form-content-section .answers").append("<div class='add-registrant-question-row'><div class='question'>"+value["question"]+"</div><div class='full-width-checkboxes' id='answers-section-"+index+"'></div></div>");

    					$.each(value["choices"], function(i,v){
    						$("#answers-section-"+index).append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='add-registrant-question-"+index+"' id='add-registrant-question-"+index+"-"+i+"'><label for='add-registrant-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
    					})
    				}
        });

        hideEQ(1);

    }

    function closeForm() {
      $(".signin-form-overlay").hide();
      $("body").css("overflow","auto");

    }

    function hideEQ(num) {
    	$(".eq"+num+"-slave").hide();
    }

    function showEQ(num) {
    	$(".eq"+num+"-slave").show();
    }

    function regForProperty() {

      var d = new Date();
      var n = d.getTime();
      var d_string = d.toLocaleString();

      var _fields = $( "form#propertyReg_Form" ).serializeArray();
    	var _query = {};
    	var hasAgent = false;

    	//Format form inputs into proper array
    	$.each(_fields, function(index, value) {
    		_query[value["name"]] = value["value"];
    	})

      _query["fn"] = "addVisitorInquiry";
      _query["propertyID"] = "<?php echo $propertyID; ?>";
      _query["name"] = $("#add-registrant-name").val();
    	_query["name"] = capitalizeFirstLetterEachWordSplitBySpace(_query["name"]);
      _query["phone"] = $("#add-registrant-phone").val();
      _query["email"] = $("#add-registrant-email").val();
    	_query["source"] = "web-listing";
      _query["leadType"] = "inquiry";
      _query["comment"] = _query["note"];
      _query["note"] = "Comment from Spacio Property Page Inquiry on "+d_string+": "+_query["note"];
			_query["docs"] = [];

			if("brokerageDocs" in listing) {
				if(listing["brokerageDocs"].length > 0) {
	            $.each(listing["brokerageDocs"],function(index,value){
	                _query["docs"][index] = {};
	                _query["docs"][index]["status"] = "pending";
	            })
	        }
			}


    	if(_query["name"] == "" && _query["email"] == "" && _query["phone"] == "") {
    		showAlert("error","Please Fill Out Form", "Please fill out the form before submitting.");
    	} else if(_query["email"] == "" && _query["phone"] == "") {
    		showAlert("error","Contact Information Required", "Please enter an email address or a phone number.");
    	} else {
				var answers = [];
				$.each(listing["questionsObj"], function(index, value) {
          var key = "add-registrant-question-"+index;

  				if (value["type"] == "yn") {
  					if (!(key in _query)) {
  						answers.push({"questionObj":value, "answer":""});
  					} else {
  						answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});
  					}
  				}

					if (value["type"] == "mc") {
						if (!(key in _query)) {
							answers.push({"questionObj":value, "answer":""});
						} else {
							answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});
						}
					}

					if (value["type"] == "text") {
						answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});
					}

					if (value["type"] == "agent") {

						if (!(key in _query)) {
							answers.push({"questionObj":value, "answer":""});
						} else {
							answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});

							if(_query["add-registrant-question-"+index] == "YES") {
								hasAgent = true;
								_query["add-registrant-question-"+index+"a"] = capitalizeFirstLetterEachWordSplitBySpace(_query["add-registrant-question-"+index+"a"]);
								answers.push({"questionObj":{"questionID":"agenta","question":"Agent Name?", "type":"text"}, "answer":_query["add-registrant-question-"+index+"a"]});
								answers.push({"questionObj":{"questionID":"agentb","question":"Agent Contact?", "type":"text"}, "answer":_query["add-registrant-question-"+index+"b"]});
								answers.push({"questionObj":{"questionID":"agentc","question":"Agent Company?", "type":"text"}, "answer":_query["add-registrant-question-"+index+"c"]});
							}
						}
					}
          delete _query[key];

				})



				_query["dateCreated"] = {"sec":Math.floor(n/1000),"used":0};
				_query["answersObj"] = answers;

				var json_data = _query;
				console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
				loading(true, "Registering Inquiry...");

					$.ajax({
						url: wsURL2,
						type:"POST",
						data: {json : json_data},
						success: function(data, textStatus, jqXHR){
							if (data.status == 1) {
								console.log("Returned JSON Object: "+JSON.stringify(data));
								$('#propertyReg_Form')[0].reset();
								hideEQ(1);
								showAlert("info","Successfully Registered","Thank you!");
                closeForm();

							} else if (data.status == 2){

								$('#propertyReg_Form')[0].reset();
								showAlert("info","Already Registered","You have already registered for this property.");
								hideEQ(1);
                closeForm();

							} else {
								//alert("Succesfully Registered");
							}
							loading(false);
              $("body").css("overflow","auto");

						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Register For Property Error: "+errorThrown);
							showAlert("ERROR","Visitor information not registered. There was a connection error with the server. If problem persists, please contact support at support@spac.io.");
							loading(false);
              $("body").css("overflow","auto");

						},
						dataType: "json"
					});
      	}

      }

			function sanitizeURL (string) {
		    if (!~string.indexOf("http") && string != ""){
		        string = "http://" + string;
		    }
		    return string;
			}

      function capitalizeFirstLetterEachWordSplitBySpace(string){
      	var words = string.split(" ");
      	var output = "";
      	for (i = 0 ; i < words.length; i ++){
        	lowerWord = words[i].toLowerCase();
        	lowerWord = lowerWord.trim();
        	capitalizedWord = lowerWord.slice(0,1).toUpperCase() + lowerWord.slice(1);
        	output += capitalizedWord;
        	if (i != words.length-1){
        	   output+=" ";
        	}
      	}//for
      	output[output.length-1] = '';
      	return output;
      }
    </script>
  </body>
</html>
