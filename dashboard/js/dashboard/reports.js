

function showReportPanel() {
        updateLastActive();
        console.log("showing Reports panels");
        closeAllPanels();
        $("#report-panel").show();
        $("#report-menu-item").removeClass("not-selected").addClass("selected");
        prevPanel = curPanel;
        curPanel = "report";

        //Setup DateRangePicker
        $("#daterangepicker-reporting").daterangepicker({
             datepickerOptions : {
                 numberOfMonths : 2
             },
             initialText : 'All Time...',
             onClose: function() { setTimeout(function(){ generateReport();},500)},
             onChange: function() { generateReport(); }
         });

        $("#report-property-filter").html("");
        $("#report-property-filter").append('<option value="all">All</option>');
        $.each(properties, function(index,value){
           var temp_addr = value["addr1"];
           if (temp_addr != "") {
               temp_addr = "("+value["addr1"]+")";
           }

           $("#report-property-filter").append("<option value='"+index+"'>"+value["title"]+" "+temp_addr+"</option>");
       });

        getReport();

}

function getReport() {
    loading(true, "Retrieving Reporting Data...");
    getReportRegistrantsByManager( function(){
       getHostedCount(function(){
            generateReport();
        })
    });
}

function toggleSubcategory(tar) {
    if(activeSubCategories[tar]) {
        $(".stat-subcategory."+tar).css("background-color","#ddd");
        $(".stat-subcategory."+tar).css("color","#999");
        $(".stat-subcategory."+tar+" .toggle-button").html("SHOW");
        $("."+tar+"-row").fadeOut();
        activeSubCategories[tar] = false;

    } else {
        $(".stat-subcategory."+tar).css("background-color","transparent");
        $(".stat-subcategory."+tar).css("color","#2e2e2e");
        $(".stat-subcategory."+tar+" .toggle-button").html("HIDE");
        $("."+tar+"-row").fadeIn();
        activeSubCategories[tar] = true;
    }
}

function showMoreRankings() {
    var report_pid = $("#report-properties-filter").val();
    if (report_pid == "all") {
        curRankingsCountMax += 20;
    } else {
        curSessionsCountMax += 20;
    }
    generateReport(false);
}

function generateReport(reset) {
    loading(true);
    $("#report-panel panel-content-scroll").scrollTop(0);
    if (typeof reset === "undefined" || reset === null) {
        reset = true;
    }

    if(reset) {
        curSessionsCountMax = 20;
        curRankingsCountMax = 20;
    }

    var total_properties = 0;
    var total_visitors = 0;
    var total_oh = 0;

    var active_properties = 0;
    var inactive_properties = 0;

    var visitor_with_contact = 0;
    var visitor_without_contact = 0;
    var visitor_verified = 0;

    var visitor_with_representation = 0;
    var visitor_without_representation= 0;
    var visitor_unknown_representation = 0;

    var visitor_with_financing = 0;
    var visitor_without_financing= 0;
    var visitor_unknown_financing = 0;

    var d = new Date();

    var report_pid = $("#report-property-filter").val();
    var report_daterange = $("#daterangepicker-reporting").val();
    var report_propertyID  = 0;

    if (report_pid == "all") {
        $(".rankings-subcategory").html("RANKINGS");
        $(".all-headers").show();
        $(".single-headers").hide();
        $(".properties-row").show();
    } else {
        $(".rankings-subcategory").html("SESSIONS");
        $(".all-headers").hide();
        $(".single-headers").show();
        $(".properties-row").hide();
    }
    //The inital time period is set from Epoch to today
    var report_enddate = d.getTime();
    var report_startdate = 0;

    if (report_daterange != "" && report_daterange != null) {

        report_daterange = $.parseJSON(report_daterange);
        //alert(report_daterange["start"]);
        var temp_startdate = new Date(report_daterange["start"]);
        var temp_enddate = new Date(report_daterange["end"]);

        var options = {
            year: "numeric", month: "short", day: "numeric"
        };

        var temp_startdate_string = temp_startdate.toLocaleDateString(temp_startdate.getTimezoneOffset(),options);
        var temp_enddate_string = temp_enddate.toLocaleDateString(temp_enddate.getTimezoneOffset(),options);

        report_startdate = temp_startdate.getTime();
        report_enddate = temp_enddate.getTime() + 86340000;

        console.log("START DATE: "+report_startdate);
        console.log("END DATE: "+report_enddate);
    }

    var lead_filter = "";

    $(".oh-rankings").html("");

    //Reset lead count for each open house
    $.each(properties, function(index,value){
        value["registrants"] = 0;
        value["ohCount"] = 0;

        total_properties++;
        if(value["disabled"] == "YES") {
            inactive_properties++;
        } else {
            active_properties++;
        }
    });

    $(".total-listings-num").html(total_properties);

    if (total_properties == 0) {
        total_properties = 1;
    }
    $(".active-listings-num").html(active_properties);
    $(".inactive-listings-num").html(inactive_properties);
    $(".active-listings-percentage").html(Math.floor((active_properties/total_properties)*1000)/10 +"%");
    $(".inactive-listings-percentage").html(Math.floor((inactive_properties/total_properties)*1000)/10 +"%");
    $(".active-listings-fill").css("width",Math.floor(active_properties/total_properties*100)+"%");
    $(".inactive-listings-fill").css("width",Math.floor(inactive_properties/total_properties*100)+"%");


    $.each(report_registrants, function(index,value){
        var datetime = new Date(value["dateCreated"]["sec"] * 1000);
        var datecreated = datetime.getTime();
        var day = datetime.getDay();
        var _pid = value["pid"];

        if(!(typeof properties[_pid] === "undefined")) {
            properties[_pid]["registrants"] = properties[_pid]["registrants"] + 1;
        }

        if ((report_pid == "all" || _pid == report_pid) && (datecreated > report_startdate && datecreated < report_enddate)) {
            total_visitors++;
            visitor_without_contact++;
            visitor_unknown_representation++;
            visitor_unknown_financing++;

            if(!("status" in value["fullcontact"])) {

            } else if(value["fullcontact"]["status"] == "200") {
                visitor_verified++;
            }

            if (value["hasContact"] == "YES") {
                visitor_without_contact--
                visitor_with_contact++;
            }

            if (value["hasAgent"] == "YES") {
                visitor_unknown_representation--;
                visitor_with_representation++;
            } else if (value["hasAgent"] == "NO") {
                visitor_unknown_representation--;
                visitor_without_representation++;
            }

            if (value["hasFinancing"] == "YES") {
                visitor_unknown_financing--;
                visitor_with_financing++;
            } else if (value["hasFinancing"] == "NO") {
                visitor_unknown_financing--;
                visitor_without_financing++;
            }
        }
    });


    $(".total-visitors-num").html(total_visitors);

    if (total_visitors == 0) {
        total_visitors = 1;
    }

    $(".with-contact-num").html(visitor_with_contact);
    $(".without-contact-num").html(visitor_without_contact);
    $(".verified-num").html(visitor_verified);
    $(".with-contact-percentage").html(Math.floor((visitor_with_contact/total_visitors)*1000)/10 +"%");
    $(".without-contact-percentage").html(Math.floor((visitor_without_contact/total_visitors)*1000)/10 +"%");
    $(".verified-percentage").html(Math.floor((visitor_verified/total_visitors)*1000)/10 +"%");
    $(".with-contact-fill").css("width",Math.floor(visitor_with_contact/total_visitors*100)+"%");
    $(".without-contact-fill").css("width",Math.floor(visitor_without_contact/total_visitors*100)+"%");
    $(".verified-fill").css("width",Math.floor((visitor_verified/total_visitors)*1000)/10 +"%");

    $(".with-representation-num").html(visitor_with_representation);
    $(".without-representation-num").html(visitor_without_representation);
    $(".unknown-representation-num").html(visitor_unknown_representation);
    $(".with-representation-percentage").html(Math.floor((visitor_with_representation/total_visitors)*1000)/10 +"%");
    $(".without-representation-percentage").html(Math.floor((visitor_without_representation/total_visitors)*1000)/10 +"%");
    $(".unknown-representation-percentage").html(Math.floor((visitor_unknown_representation/total_visitors)*1000)/10 +"%");
    $(".with-representation-fill").css("width",Math.floor(visitor_with_representation/total_visitors*100)+"%");
    $(".without-representation-fill").css("width",Math.floor(visitor_without_representation/total_visitors*100)+"%");
    $(".unknown-representation-fill").css("width",Math.floor(visitor_unknown_representation/total_visitors*100)+"%");

    $(".with-financing-num").html(visitor_with_financing);
    $(".without-financing-num").html(visitor_without_financing);
    $(".unknown-financing-num").html(visitor_unknown_financing);
    $(".with-financing-percentage").html(Math.floor((visitor_with_financing/total_visitors)*1000)/10 +"%");
    $(".without-financing-percentage").html(Math.floor((visitor_without_financing/total_visitors)*1000)/10 +"%");
    $(".unknown-financing-percentage").html(Math.floor((visitor_unknown_financing/total_visitors)*1000)/10 +"%");
    $(".with-financing-fill").css("width",Math.floor(visitor_with_financing/total_visitors*100)+"%");
    $(".without-financing-fill").css("width",Math.floor(visitor_without_financing/total_visitors*100)+"%");
    $(".unknown-financing-fill").css("width",Math.floor(visitor_unknown_financing/total_visitors*100)+"%");

    var sortedHostedOn = [];

    $.each(hosted, function(index,value){
        var datetime = new Date(value["dateHosted"]["sec"] * 1000);
        var datecreated = datetime.getTime();

        if ((report_pid == value["pid"] || report_pid == "all") && (datecreated > report_startdate && datecreated < report_enddate)) {
            total_oh++;

            if(!(typeof properties[value["pid"]] === "undefined")) {
                properties[value["pid"]]["ohCount"] = properties[value["pid"]]["ohCount"] + 1;
            }
            if (report_pid == value["pid"] ) {
                sortedHostedOn.push([index, value["pid"], datecreated, datetime]);
            }

        }
    });

    sortedHostedOn = sortedHostedOn.sort(function(a,b){
        return b[2] - a[2];
    });

    var temp_hostedon_count = 0;
    var unshown_hostedon_count = 0;

    $.each(sortedHostedOn, function(i,v){
        var index = v[0];
        var pid = v[1];
        var value = hosted[index];
        var datetime = v[2];

        temp_hostedon_count++;

        if(value["managerID"] != user["id"] && (user["teamRole"] == "member" || user["teamRole"] == "admin" || user["teamRole"] == "owner") ) {
            $(".oh-rankings").append("<div class='oh-row-2'><div class='title-column'><div class='property-title-row'>"+properties[value["pid"]]["title"]+"</div><div class='property-addr-row'>"+$.format.toBrowserTimeZone(datetime, "MMM d, yyyy")+", Hosted By: "+team[value["managerID"]]["fname"]+" "+team[value["managerID"]]["lname"]+"</div></div><div class='visitors-column'>"+value["leadsCount"]+"</div><div class='delete-column'></div></div>");
        } else {
             $(".oh-rankings").append("<div class='oh-row-2'><div class='title-column'><div class='property-title-row'>"+properties[value["pid"]]["title"]+"</div><div class='property-addr-row'>"+$.format.toBrowserTimeZone(datetime, "MMM d, yyyy")+"</div></div><div class='visitors-column'>"+value["leadsCount"]+"</div><div class='delete-column' onclick='confirmDeleteHosted(&#39;"+value["id"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div></div>");
        }

        if(temp_hostedon_count >= curSessionsCountMax) {
            //break;
            $(".more-bar").show();
            return false;
        }
        //$(".reporting-hosted-on").append("<div class='oh-row'>"+properties[index]["title"]+"</div>");

    });

    if(temp_hostedon_count < curSessionsCountMax) {
        $(".more-bar").hide();
    }


    var sortedList = [];

    $.each(properties, function(index,value){
        sortedList.push([index, value["registrants"]]);
    });

    sortedList = sortedList.sort(function(a,b){
        return b[1] - a[1];
    });

    var count = 0;
    var temp_oh_count = 0;
    var unshown_oh_count = 0;

    $.each(sortedList, function(i,v){
        count++;
        var index = v[0];
        var value = properties[index];

        if (report_pid == "all") {

            var temp_leads_count = value["registrants"];
            if (user["type"] == "freemium" || user["type"] == "lite") {
                temp_leads_count = "--"
            }

            temp_oh_count++;

            var temp_productivity = 0;

            if(value["ohCount"] != 0) {
                temp_productivity = Math.floor(value["registrants"]/value["ohCount"]*10)/10;
            }

            temp_productivity = temp_productivity.toFixed(2);

            $(".oh-rankings").append("<div class='oh-row'><div class='title-column'><div class='property-thumbnail' style='background-image:url("+value["image"]+")'><div class='property-ranking'>"+temp_oh_count+"</div></div><div class='property-title-row'>"+value["title"]+"</div><div class='property-addr-row'>"+value["addr1"]+"</div></div><div class='visitors-column'>"+value["registrants"]+"</div><div class='productivity-column'>"+temp_productivity+"</div></div>");

            if(temp_oh_count >= curRankingsCountMax) {
                //break;
                $(".more-bar").show();
                return false;
            }
        }

    });

    if (report_pid == "all") {
        if(temp_oh_count < curRankingsCountMax) {
            $(".more-bar").hide();
        }
    }

    $(".total-ohs-num").html(total_oh);

    reportStats["total_properties"] = total_properties;
    reportStats["active_properties"] = active_properties;
    reportStats["inactive_properties"] = inactive_properties;
    reportStats["total_visitors"] = total_visitors;
    reportStats["visitor_with_contact"] = visitor_with_contact;
    reportStats["visitor_without_contact"] = visitor_without_contact;
    reportStats["visitor_verified"] = visitor_verified;
    reportStats["visitor_with_representation"] = visitor_with_representation;
    reportStats["visitor_without_representation"] = visitor_without_representation;
    reportStats["visitor_unknown_representation"] = visitor_unknown_representation;
    reportStats["visitor_with_financing"] = visitor_with_financing;
    reportStats["visitor_without_financing"] = visitor_without_financing;
    reportStats["visitor_unknown_financing"] = visitor_unknown_financing;
    reportStats["total_oh"] = total_oh;

    setTimeout(function(){loading(false);}, 3000);
}

function showReportLeads(type) {

    if (user["type"] != "freemium" && user["type"] != "lite") {
        loading(true);
        var d = new Date();

        var report_pid = $("#report-select-project").val();
        var report_daterange = $("#daterangepicker1").val();
        var report_propertyID  = 0;

        //The inital time period is set from Epoch to today
        var report_enddate = d.getTime();
        var report_startdate = 0;

        if (report_daterange != "" && report_daterange != null) {

            report_daterange = $.parseJSON(report_daterange);
            //alert(report_daterange["start"]);
            var temp_startdate = new Date(report_daterange["start"]);
            var temp_enddate = new Date(report_daterange["end"]);
            report_startdate = temp_startdate.getTime();
            report_enddate = temp_enddate.getTime();
        }

        //alert(report_daterange );
        if (report_pid != "all") {
            report_propertyID = properties[report_pid]["propertyID"];
        }

        var leads_all = [];
        var leads_with_contact_arr = [];
        var leads_without_contact_arr = [];
        var leads_with_agent_arr = [];
        var leads_without_agent_arr = [];
        var leads_unknown_agent_arr = [];
        var leads_with_financing_arr = [];
        var leads_without_financing_arr = [];
        var leads_unknown_financing_arr = [];

        var lead_filter = "";
        if (report_pid == "all") {
            lead_filter = "Showing Leads for All Properties"
        } else {
            lead_filter = "Showing Leads for '"+properties[report_pid]["title"]+"'";
        }

        if (report_daterange != "" && report_daterange != null) {
            lead_filter = lead_filter+" from "+report_daterange["start"]+" to "+report_daterange["end"];
        }

        $("#report-registrants-filter").html(lead_filter);
        $.each(report_registrants, function(index,value){
            var datetime = new Date(value["dateCreated"]["sec"] * 1000);
            var datecreated = datetime.getTime();
            var day = datetime.getDay();

            var _propertyID = value["propertyID"];


            if ((report_pid == "all" || _propertyID == report_propertyID) && (datecreated > report_startdate && datecreated < report_enddate)) {

                if ((value["phone"] == "N/A" || value["phone"] == "") && (value["email"] == "N/A" || value["email"] == "")) {
                    leads_without_contact_arr.push(value);
                } else {
                    leads_with_contact_arr.push(value);
                }
                leads_all.push(value);
                leads_unknown_agent_arr.push(value);
                leads_unknown_financing_arr.push(value);

                var temp_lead = value;
                $.each(value["answersObj"], function(index,value){
                    if(value["questionObj"]["question"] == "Are you working with an agent?") {
                        if(value["answer"] == "YES") {
                            leads_unknown_agent_arr.pop();
                            leads_with_agent_arr.push(temp_lead);
                        }
                        if(value["answer"] == "NO") {
                            leads_unknown_agent_arr.pop();
                            leads_without_agent_arr.push(temp_lead);
                        }
                    }

                    if(value["questionObj"]["question"] == "Are you mortgage pre-approved?") {
                        if(value["answer"] == "YES") {
                            leads_unknown_financing_arr.pop();
                            leads_with_financing_arr.push(temp_lead);
                        }
                        if(value["answer"] == "NO") {
                            leads_unknown_financing_arr.pop();
                            leads_without_financing_arr.push(temp_lead);
                        }
                    }
                });
            }

        });

        var report_leads_arr = [];
        if (type=="leads_all") {
            report_leads_arr = leads_all;
            $("#report-registrants-type").html("View All Leads");
        }

        if (type=="leads_without_contact") {
            report_leads_arr = leads_without_contact_arr;
            $("#report-registrants-type").html("View Leads Without Contact Information");
        }

        if (type=="leads_with_contact") {
            report_leads_arr = leads_with_contact_arr;
            $("#report-registrants-type").html("View Leads With Contact Information");
        }

        if (type=="leads_without_agent") {
            report_leads_arr = leads_without_agent_arr;
            $("#report-registrants-type").html("View Leads Without Agent Representation");
        }

        if (type=="leads_with_agent") {
            report_leads_arr = leads_with_agent_arr;
            $("#report-registrants-type").html("View Leads With Agent Representation");
        }

        if (type=="leads_unknown_agent") {
            report_leads_arr = leads_unknown_agent_arr;
            $("#report-registrants-type").html("View Leads With Unknown Agent Representation");
        }

        if (type=="leads_without_financing") {
            report_leads_arr = leads_without_financing_arr;
            $("#report-registrants-type").html("View Leads Without Mortgage Approval");
        }

        if (type=="leads_with_financing") {
            report_leads_arr = leads_with_financing_arr;
            $("#report-registrants-type").html("View Leads With Mortgage Approval");
        }

        if (type=="leads_unknown_financing") {
            report_leads_arr = leads_unknown_financing_arr;
            $("#report-registrants-type").html("View Leads With Unknown Mortgage Approval Status");
        }

        $("#popup-view-report-registrants .popup-box-content .report-registrants").html("");

        $.each(report_leads_arr, function(index, value) {

            var name = value["name"];
            var phone = value["phone"];
            var email = value["email"];

            if (value["tempEmail"] == "N/A") {

            } else {
                email = value["tempEmail"];
            }
            console.log(email);

            var datetime = new Date(value["dateCreated"]["sec"] * 1000);

            $("#popup-view-report-registrants .popup-box-content .report-registrants").append("<div id='registrant-"+index+"' class='registrant-row' ><div class='registrant-name' style='width:200px;'>"+name+"</div><div class='registrant-email' style='width:350px;'>"+email+"</div><div class='registrant-phone' style='width:150px;'>"+phone+"</div><div class='registrant-phone' style='width:150px;'>"+datetime.toLocaleDateString()+"</div><div class='registrant-phone' style='width:125px;'>"+datetime.toLocaleTimeString()+"</div></div>");

        });

        $("#popup-view-report-registrants-scroll").scrollTop(0);
        showPopup("view-report-registrants");
        loading(false);
    } else {
        showAlert("Pro Feature","This feature is available for Pro users only.");
    }

}
