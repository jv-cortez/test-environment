function showPropertiesPanel() {
    console.log("Showing Properties Panel.");
    closeAllPanels();
    $("#properties-menu-item").removeClass("not-selected").addClass("selected");
    prevPanel = curPanel;
    curPanel = "properties";

    $("#properties-panel").show();

    //Set to default values
    $("#properties-filter-type-active").prop("checked", true);

    filterProperties(true);
}



function filterProperties(reload) {
    if (typeof reload === "undefined" || reload === null) {
        reload = true;
    }

    var properties_filter_type = $("input[name='properties-filter-type']:checked").val();

    var propertiesType;

    var propertiesSearchTerms = $("#properties-search-terms").val().toLowerCase();

    var advanced_filter_settings = "";

    advanced_filter_settings += properties_filter_type;

    $("#properties-advanced-filter-settings").html(advanced_filter_settings);
    hideAdvancedFilter();

    if(properties_filter_type == "Active") {
        propertiesType = "active";
    } else {
        propertiesType = "inactive";
    }

    if(reload) { //Grab data from server
        loading(true);
        getProperties(function(){
            //loadLeads(leadsList, leadType, leadStatus, leadVerified, leadRepresentation, leadClient, leadSearchTerms, sort, order);
            loadProperties(properties,propertiesType, propertiesSearchTerms);
            loading(false);
        });
    } else { //Don't need to grab data from server
        loadProperties(properties,propertiesType, propertiesSearchTerms);
    }
}

function loadProperties(propertiesList, filter, propertiesSearchTerms) {

    filter = filter || "active";

    console.log("Loading properties");
    var count = 0;
    $("#properties-panel .panel-content-scroll").empty();

    propertiesSearchResult = {};
    console.log(propertiesSearchTerms);
    if(propertiesSearchTerms == "") {
        propertiesSearchResult = propertiesList;
        //loading(false);
        //clearPropertiesSearchResults();
    } else {
        $.each(propertiesList, function(index,value){

                var p_title = value["title"].toLowerCase();
                var p_desc = value["desc"].toLowerCase();
                var p_addr1 = value["addr1"].toLowerCase();
                var p_addr2 = value["addr2"].toLowerCase();

                var includeProp = false;
                if(p_title.indexOf(propertiesSearchTerms) >= 0) {
                    includeProp = true;
                    console.log("Title matched.");
                } else if(p_desc.indexOf(propertiesSearchTerms) >= 0) {
                    includeProp = true;
                    console.log("Desc matched.");
                } else if(p_addr1.indexOf(propertiesSearchTerms) >= 0) {
                    includeProp = true;
                    console.log("Addr1 matched.");
                } else if(p_addr2.indexOf(propertiesSearchTerms) >= 0) {
                    includeProp = true;
                    console.log("Addr2 matched.");
                }
                if (includeProp) {
                    propertiesSearchResult[index] = value;
                }
        });

    }


    if (Object.keys(propertiesSearchResult).length == 0) {
        $(".num-properties").html(count);

        //$("#properties-panel .sub-container-scroll").append("<div class='property-item'><div style='margin:20px;font-size:16px;'>Create Your First Sign-In Form.</div></div>");
    } else {

        var d = new Date();
        var sortedList = [];

        $.each(propertiesSearchResult, function(index,value){
            sortedList.push([index, value["lastEdited"]["sec"]]);
        });

        var sortedList = sortedList.sort(function(a,b){
            return b[1] - a[1];
        });

        var all_prop_num = 0;
        var active_prop_num = 0;
        var inactive_prop_num = 0;

        $.each(sortedList, function(i,v){
            var index = v[0];
            var value = propertiesSearchResult[index];

            all_prop_num++;

            if (value["disabled"] == "YES") {
                inactive_prop_num++;
            } else {
                active_prop_num++;
            }

            $(".all-properties-num").html(all_prop_num);
            $(".active-properties-num").html(active_prop_num);
            $(".inactive-properties-num").html(inactive_prop_num);

            var lastedited_date = new Date(value["lastEdited"]["sec"]*1000);
            var lastedited = lastedited_date.toLocaleString();

            if(value["shared"] == "YES" && user["teamRole"] == "member") {
                lastedited = lastedited + "<br/>Shared By Team Owner";
            } else if(value["shared"] == "YES" && (user["teamRole"] == "owner" || user["teamRole"] == "admin")) {
                lastedited = lastedited + "<br/>Shared With Team";
            }

                var no_cache_img = value["image"];

                    if (value["image"] == "stock") {
                        no_cache_img = "img/stock/1.jpg";
                    } else {
                        var res = value["image"].split("?");

                        if (res.length > 1) {

                        } else {
                            //no_cache_img = value["image"]+"?"+d.getTime();
                        }
                    }


                if(!("isDeleted" in value)) {
                    value["isDeleted"] = false;
                }

                var propFilter = "all";

                if(value["disabled"] == "YES") {
                    propFilter = "inactive";
                } else {
                    propFilter = "active"
                }

                if (!value["isDeleted"] && (filter == "all" || propFilter == filter)) {
                    count++;
                    $("#properties-panel .panel-content-scroll").append("<div class='property-item' id='property-item-"+index+"'><div style='background-image:url("+no_cache_img+")' class='property-image'><div class='property-btns'><div class='bay-btn' onclick='bayProperty(&#39;"+index+"&#39;)' style='display:none;width:100px;height:20px;border-radius:3px;background-color:#0093e0;margin-bottom:5px;color:white;line-height:20px;letter-spacing:2px;font-size:12px;font-family:effra_medium;text-align:center;'>BUY AD</div><div class='boost-btn' onclick='boostProperty(&#39;"+index+"&#39;)' style='display:none'><img src='img/boost_badge.png' width='100'/></div><div class='xpressdocs-btn' onclick='launchXpressdocs(&#39;"+index+"&#39;)' style='display:none'><img src='img/xpressdocs_badge.png' width='100'/></div><div class='share-btn' onclick='showShareProperty(&#39;"+index+"&#39;)'><i class='fa fa-share' aria-hidden='true'></i> SHARE</div></div></div><div class='property-info'><div class='property-title main-color-font' style='width:100%;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;'>"+value["title"]+"</div><div class='property-address'>"+value["addr1"]+"</div><div class='property-lastedited'>Last Edited: "+lastedited+"</div></div><div class='property-manage-bar'><div class='property-manage-button btn main-color-background' onclick='toggleSlider(&#39;"+index+"&#39;)'><i class='fa fa-chevron-left' aria-hidden='true'></i></div><div class='property-edit-form-button btn main-color-background-lite-1' onclick='showEditForm(&#39;"+index+"&#39;)'><span>EDIT<br/>SIGN-IN FORM</span></div><div class='property-launch-form-button btn main-color-background-lite-2' onclick='launchForm(&#39;"+index+"&#39;)'><span>LAUNCH<br/>SIGN-IN FORM</span></div><div class='property-details-button btn main-color-background-lite-2' onclick='showProperty(&#39;"+index+"&#39;)'><span>PROPERTY<br/>DETAILS</span></div><div class='property-broadcast-button btn main-color-background-lite-1' onclick='showBroadcast(&#39;"+index+"&#39;)'><span>BROADCAST<br/>MESSAGE</span></div><div class='property-view-registrants-button btn main-color-background-lite-2' onclick='showVisitors(&#39;"+index+"&#39;)'><span>VIEW<br/>VISITORS</span></div></div></div>");
                }

        });


        if(user["teamRole"] == "member") {
            $(".edit-icon").hide();
        }

        if("xpressdocsEnabled" in user["integrations"]) {
          if(user["integrations"]["xpressdocsEnabled"] == "YES") {
            $(".xpressdocs-btn").show();
          }
        }

        if("boostEnabled" in user["integrations"]) {
            if(user["integrations"]["boostEnabled"] == "YES") {
                $(".boost-btn").show();
            }
        }

        if("bayEnabled" in user["integrations"]) {
            if(user["integrations"]["bayEnabled"] == "YES") {
                $(".bay-btn").show();
            }
        }

        console.log(user["integrations"]);



        $(".num-properties").html(count);

    }
}

function reloadProperties() {
		var d = new Date();
		$("#last-sync").html("Last Synced On: "+d.toLocaleString());
		online = true;
		syncMutex = true;
		loading(true, "Synching...");
		syncMutex = false;
		loading(false);
		getProperties(function(){
			filterProperties(false);
		});
}

function clearPropertiesSearch() {
    $("#properties-search-terms").val("");
    filterProperties(false);
}


function showAddProperty() {
  if(user["account_type"] == "brokerage" || user["account_type"] == "brokerage-individual" || user["account_type"] == "team") {
      if(brokerage["hasFeed"] == "YES") {
          $(".search-property-panel").show();
      } else {
          $("#search-property-panel").hide();
      }
  } else {
      $("#search-property-panel").hide();
  }

    $('#addProperty_Form')[0].reset();
    $("#image").val("");
    $("#mls").val("");
    $(".remove-image-button").hide();
    $("#image-preview-2").css("background-image","url(img/placeholder.png)");
    console.log("show add properties panel");
    showPopup("add-property");
    curPanel = "addProperty";


    if(user["brokerageID"] != "" && user["brokerageID"] != "N/A") {
        if(brokerage["hasFeed"] == "YES") {
          $(".search-property-panel").show();
        }
      }

    if(user["mls"] == "N/A" || !("mls" in user)) {
        $(".mls-field").hide();
    } else {
            $(".mls-field").show();
            $(".mls-name").html(mlsList[user["mls"]]["mls_name"]);
            $(".mls-add-disclaimer").html(mlsList[user["mls"]]["add_disclaimer"]);

    }

    if(user["country"] == "US") {
        $('#currency').val("USD");
        $('#measurement').val("sqft");
        $(".state-input-label").html("STATE");
        $(".zip-input-label").html("ZIP");
        $(".state-input-field").attr("placeholder","State");
        $(".zip-input-field").attr("placeholder","Zip Code");
    } else if(user["country"] == "CA"){
        $('#currency').val("CAD");
        $('#measurement').val("sqft");
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTAL");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postal Code");
    } else if(user["country"] == "NZ"){
        $('#currency').val("NZD");
        $('#measurement').val("sqm");
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTCODE");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postcode");
    } else if(user["country"] == "AU"){
        $('#currency').val("AUD");
        $('#measurement').val("sqft");
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTCODE");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postcode");
    } else if(user["country"] == "UK"){
        $('#currency').val("GBP");
        $('#measurement').val("sqft");
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTCODE");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postcode");
    }

}

function showProperty(pid) {
    curPanel = "editProperty";
    $("#editProperty_Form")[0].reset();

    if(properties[pid]["editable"] == false) {
        //showAlert("Edit Open House","The details of this open house cannot be edited.");
        $.each(properties[pid], function(index,value){
            $("#edit-"+index).prop( "readonly", true );
        });
    } else if(properties[pid]["shared"] == "YES" && user["teamRole"] == "member"){
         showAlert("Owner Access Only","Only Owners can access this function.");
    } else {
        $.each(properties[pid], function(index,value){
            $("#edit-"+index).prop( "readonly", false );
        });
    }

    // if(user["type"] == "brokerage" && (user["brokerageID"] == "spacio" || user["brokerageID"] == "bhhsfoxroach")) {
    //   $(".openhouse-scheduler-panel").show();
    //   getScheduledOpenhouses(pid,function(){
    //     loadOpenhouseSchedule();
    //   });
    // } else {
    //   $(".openhouse-scheduler-panel").hide();
    // }

    if (user["teamRole"] != "member" || (properties[pid]["shared"] != "YES" && user["teamRole"] == "member")) {

        if(properties[pid]["disabled"] == "YES") {
            $("#enable-property").prop( "checked", false );
        } else {
            $("#enable-property").prop( "checked", true );
        }

        updateLastActive();

        showPopup("view-property");
        $("#property-popup-scroll").scrollTop(0);
        curPID = pid;
        var d = new Date();
        $(".remove-image-button").show();

        $.each(properties[pid], function(index,value){
            //console.log(index);

            if (index == "desc") {
                $("#edit-"+index).html(value);
            } else if (index == "image") {
                $("#edit-"+index).val(value);
                    var no_cache_img = value;

                    if (no_cache_img == "stock") {
                        no_cache_img = "img/stock/1.jpg";
                    } else {
                        var res = value.split("?");

                        if (res.length > 1) {

                        } else {
                            no_cache_img = value;
                        }
                    }

                    $("#image-preview").css({"background-image":"url("+no_cache_img+")"});

            } else {
                if (value == -1) {
                    $("#edit-"+index).val("");
                } else if (value == "") {
                    $("#edit-"+index).val("");
                } else {
                    $("#edit-"+index).val(value);
                }
            }
        });

        $('#edit-currency').val(properties[pid]["currency"]);
        $('#edit-measurement').val(properties[pid]["measurement"]);
    }

    if(user["country"] == "US") {
        $(".state-input-label").html("STATE");
        $(".zip-input-label").html("ZIP");
        $(".state-input-field").attr("placeholder","State");
        $(".zip-input-field").attr("placeholder","Zip Code");
    } else if(user["country"] == "CA"){
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTAL");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postal Code");
    } else if(user["country"] == "NZ"){
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTCODE");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postcode");
    } else if(user["country"] == "AU"){
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTCODE");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postcode");
    } else if(user["country"] == "UK"){
        $(".state-input-label").html("PROVINCE");
        $(".zip-input-label").html("POSTCODE");
        $(".state-input-field").attr("placeholder","Province");
        $(".zip-input-field").attr("placeholder","Postcode");
    }
}


function toggleSlider(index) {
    if ($("#property-item-"+index+" .property-manage-bar").css("right") == "-600px") {
        $("#property-item-"+index+" .property-manage-bar").css("right","0px");
        $("#property-item-"+index+" .property-manage-bar .property-manage-button").html("<i class='fa fa-chevron-right' aria-hidden='true'></i>");
        $("#property-item-"+index+" .property-manage-bar").css("-webkit-transform","translateX(0)");
    } else {
        $("#property-item-"+index+" .property-manage-bar").css("right","-600px");
        $("#property-item-"+index+" .property-manage-bar .property-manage-button").html("<i class='fa fa-chevron-left' aria-hidden='true'></i>");
    }

}


function showShareProperty(pid) {
    $(".share-overlay").show();
    $(".share-property-title").html(properties[pid]["title"]);
    curSharePID = pid;

    var urlToShare = properties[curSharePID]["url"];
    urlToShare = encodeURIComponent(urlToShare);
    var emailShareBody = encodeURIComponent("Check out my property and see more info here!");

    if(urlToShare == "" || urlToShare == "N/A" || urlToShare == null) {
        urlToShare = "https://spac.io/l/"+properties[curSharePID]["propertyID"];
        urlToShare = encodeURIComponent(urlToShare);
        emailShareBody = encodeURIComponent("Check out my property and inquire or register for more info here!");
    }

    var emailSubject = "Check out my new property: "+properties[pid]["title"];
    emailSubject = encodeURIComponent(emailSubject);

    var emailShareLink = "mailto:?subject="+emailSubject+"&body="+emailShareBody+" "+urlToShare;

    $(".twitter-intent-link").attr("href","https://twitter.com/intent/tweet?text="+emailShareBody+"&url="+urlToShare);
    $(".email-share-link").attr("href",emailShareLink);


}

function hideShareProperty(pid) {
    $(".share-overlay").hide();
    curSharePID = "";

}

function shareProperty(platform) {
    var urlToShare = properties[curSharePID]["url"];
    var emailShareBody = "Check out my listing and see more info here!<br/><br/>"+urlToShare;

    if(urlToShare == "" || urlToShare == "N/A" || urlToShare == null) {
        urlToShare = "https://spac.io/l/"+properties[curSharePID]["propertyID"];
        emailShareBody = "Check out my listing and inquire or register for more info here!<br/><br/>"+urlToShare;

    }
    if(platform == "facebook") {
      FB.ui({
          method: 'share',
          href: urlToShare,
        }, function(response){});

    }



    if(platform == "email") {

    }

    if(platform == "copy") {
      $("#share-reg-link").val(urlToShare);
      var origSelectionStart, origSelectionEnd;
  		var elem = document.getElementById("share-reg-link");
      origSelectionStart = elem.selectionStart;
      origSelectionEnd = elem.selectionEnd;

  		var currentFocus = document.activeElement;
  		elem.focus();
  		elem.setSelectionRange(0, elem.value.length);


       var succeed;
       try {
         succeed = document.execCommand("copy");
       } catch(e) {
         succeed = false;
       }

       if(succeed) {
         showAlert("Link Copied","Property Registration Link copied to clipboard.");
         setTimeout(function(){$(".alert-overlay").hide();}, 3000);
         //alert("Copied to Clipboard");
       }
    }

}

function launchXpressdocs() {
//  $("#xpressdocs-form").submit();

  getXpressdocsData(function(xpressData){
    //alert("here");
    console.log(xpressData);
    $("#xpressdocs-form input[name='company']").val(xpressData["companyID"]);
    $("#xpressdocs-form input[name='officeid']").val(xpressData["officeID"]);
    $("#xpressdocs-form input[name='userid']").val(xpressData["userID"]);
    $("#xpressdocs-form input[name='firstname']").val(user["fname"]);
    $("#xpressdocs-form input[name='lastname']").val(user["lname"]);
    $("#xpressdocs-form input[name='email']").val(user["email"]);
    $("#xpressdocs-form input[name='directphone']").val(user["phone"]);
    $("#xpressdocs-form input[name='officephone']").val(user["phone"]);
    $("#xpressdocs-form input[name='officename']").val(xpressData["officeName"]);

    setTimeout(function(){
      $("#xpressdocs-form").submit();
      $("#xpressdocs-form")[0].reset();
    },500);

  });
}

function boostProperty(pid) {
  getBoostURL(pid,function(boostURL){
    console.log(boostURL);
    showAlert("Boost Property","Your Boost link is ready! Please click the boost button below.","<a href='"+boostURL+"' target='_blank' style='display:block;width:100%;height:inherit;text-align:center;'>Boost</a>","Cancel",null);

  });
}

function bayProperty(pid) {
  getBAYURL(pid,function(bayURL){
    console.log(bayURL);
    showAlert("Back At You","Your Back At You link is ready! Please click the promote button below.","<a href='"+bayURL+"' target='_blank' style='display:block;width:100%;height:inherit;text-align:center;'>Promote</a>","Cancel",null);

  });
}
