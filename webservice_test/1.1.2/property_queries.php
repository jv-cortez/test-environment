
<?php

require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/stripe-php-3.14.3/init.php');
require_once('../../require/vendor/autoload.php');

require_once('mls_queries.php');


class Prop_Query {

	static function reFragQuestions() {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$cursor = $collection_properties->find();

		foreach ($cursor as $doc) {
			$doc_id = $doc["_id"];
			$new_questions = array();

			foreach ($doc["questions"] as $question) {
				$new_question = array(
					"question" => $question,
					"type" => "yn"
				);
				array_push($new_questions, $new_question);
			}
			$newdata = array('$set' => array("questions" => $new_questions));
			$u_result = $collection_properties->update(array("_id" => $doc_id), $newdata);

		}

		$return = array("status" => 1);
		return $return;
	}


	/* ------------------------------ PROPERTY RELATED FUNCTIONS ------------------------------*/


	static function addCustomQuestion($managerID, $vtoken, $questionObj) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_custom_questions = Db_Conn::getInstance()->getConnection()->custom_questions;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {


			$result = $collection_custom_questions->insert($questionObj);
			$new_qid = $questionObj["_id"]->{'$id'};

			$return = array("status" => 1,"questionID" => $new_qid);

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function deleteCustomQuestion($managerID, $vtoken, $questionID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_custom_questions = Db_Conn::getInstance()->getConnection()->custom_questions;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$collection_custom_questions->remove(array('_id' => new MongoId($questionID),'managerID' => $managerID));

			$return = array("status" => 1);

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}


	static function generateCloudCMAReport($managerID, $vtoken, $pid, $cloudcmaMLS) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		$property_result = $collection_properties->findOne(array('_id' => new MongoId($pid)));

		if (!is_null($manager_result)) {
				$newdata = array('$set' => array('cloudcmaMLS' => $cloudcmaMLS));
				$u_result = $collection_properties->update(array('_id' => new MongoId($pid)), $newdata);

				$url = 'http://cloudcma.com/properties/widget?';
				// Our callback url
				$cb_url = 'http://ws.spac.io/prod/oh/webhooks/cloudcma/index.php?id=';
				  // Key from account (Settings > Lead Gen)
				  $apiKey = "98b50bb9f6da9c9578d9f8f977d51976";
				  // Email subject line (encoded)
				  $emailSubEnc = preg_replace('/\s+/', '%20', "Your CloudCMA Property Report from Spacio");
				  // Email message body (encoded)
				  $emailMsgEnc = preg_replace('/\s+/', '%20', $manager_result["fname"].", please review this CloudCMA Property Report generated from Spacio for ".$property_result["title"].". You may edit your Property Report from your CloudCMA account.");
				  // Initialize curl
				  $rest = curl_init();
				  // Build curl url with above parameters and fire away
				  curl_setopt($rest, CURLOPT_URL, $url
												  ."api_key=".$apiKey
												  ."&mlsnum=".$cloudcmaMLS
												  ."&email_to=".$manager_result["email"]
												  ."&email_subject=".$emailSubEnc
												  ."&email_msg=".$emailMsgEnc
													."&callback_url=".$cb_url.$pid
													."&job_id=".$pid);
				  curl_setopt($rest, CURLOPT_RETURNTRANSFER, true);
				  curl_setopt($rest, CURLOPT_CUSTOMREQUEST, "POST");

				  $response = curl_exec($rest);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function clearCloudCMAReport($managerID, $vtoken, $pid) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
				$newdata = array('$set' => array('cloudcmaMLS' => "",'cloudcmaPDF' => ""));
				$u_result = $collection_properties->update(array('_id' => new MongoId($pid)), $newdata);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}
	static function getMLSListing($managerID,$vtoken,$mls,$mlsID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			if($mls == "crmls") {
				$property = getCRMLSListing($mlsID);

				if (empty($property)) {
					$return = array("status" => 3);
				} else {
					$return = array("status" => 1, "property" => $property);
				}
			} else if($mls == "sask1" || $mls== "sask2" || $mls == "sask3") {
				$return = array("status" => 3);
			} else if($mls == "sask4") {
				$property = getRALDListing($mlsID);

				if (empty($property)) {
					$return = array("status" => 3);
				} else {
					$return = array("status" => 1, "property" => $property);
				}

			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function addProperty($managerID,$vtoken,$url,$title,$addr1,$addr2,$image,$price,$beds,$baths,$dimens,$desc,$currency,$measurement,$questionsObj,$cnEnabled, $anon,$autoEmail, $mls, $mlsnum, $tlc) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_forms = Db_Conn::getInstance()->getConnection()->forms;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		//Found the manager and authenticated with proper vtoken
		if (!is_null($manager_result)) {

      		//$getDate = date('Y-m-d H:i:s');
		  	$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);


		  	$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

			$nextseq = $collection_counter->findAndModify(
				 array("desc" => "propertyID"),
				 array('$inc' => array('maxCount' => 1)),
				 null,
				 array(
					"new" => true
				)
			);

		 	$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
			$collection_teams = Db_Conn::getInstance()->getConnection()->teams;

			if (is_null($questionsObj)) {
				$questionsObj = array();
			}

			if($manager_result["brokerageID"] != "N/A") {
				$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
				$brokerage_result = $collection_brokerages->findOne(array("brokerageID" => $manager_result["brokerageID"]));
				$questionsObj = $brokerage_result["questionsObj"];
				$autoEmail = "YES";
			}


		 	$document_property = array(
									"propertyID" => $nextseq["maxCount"],
									"mls" => $mls,
									"mlsnum" => $mlsnum,
									"url" => $url,
									"title"=> $title,
									"addr1"=> $addr1,
									"addr2"=> $addr2,
									"image"=> $image,
									"price" => $price,
									"beds" => $beds,
									"baths" => $baths,
									"dimens" => $dimens,
									"desc" => $desc,
									//"note" => $note,
									"currency" => $currency,
									"measurement" => $measurement,
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"tlc" => $tlc,
									"questionsObj" => $questionsObj,
									"managerID"=>$manager_result["_id"]->{'$id'},
									"brokerageID"=>$manager_result["brokerageID"],
									"officeID"=>$manager_result["officeID"],
									"anon"=>$anon,
									"autoEmail" => $autoEmail,
									"cnEnabled" => $cnEnabled,
									"brokersEnabled" => "NO",
									"disabled" => "NO",
									"mobile" => true,
									"editable"=>true,
									"sample" => false);


		  	$result = $collection_properties->insert($document_property);
				$new_pid = $document_property["_id"]->{'$id'};
			if ($image == "property_default") {
				$image = "https://s3.amazonaws.com/spacio-user-images/".$new_pid."_property.jpg";
			}

			$newdata = array('$set' => array("image" => $image));
			$u_result = $collection_properties->update(array("_id" => new MongoId($new_pid)), $newdata);

		  if ($result) {

			$return = array("status" => 1,
							"pid" => $new_pid,
							"propertyID" => $document_property["propertyID"]
							);
		  } else {
			$return = array("status" => 2);
		  }

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function addPropertySudo($email,$url,$title,$addr1,$addr2,$image,$price,$beds,$baths,$dimens,$desc,$currency,$measurement) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('email' => $email));

		//Found the manager and authenticated with proper vtoken
		if (!is_null($manager_result)) {

      		//$getDate = date('Y-m-d H:i:s');
		  	$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);


		  	$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

			$nextseq = $collection_counter->findAndModify(
				 array("desc" => "propertyID"),
				 array('$inc' => array('maxCount' => 1)),
				 null,
				 array(
					"new" => true
				)
			);

		 	$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		 	$document_property = array(
									"propertyID" => $nextseq["maxCount"],
									"url" => $url,
									"title"=> $title,
									"addr1"=> $addr1,
									"addr2"=> $addr2,
									"image"=> $image,
									"price" => $price,
									"beds" => $beds,
									"baths" => $baths,
									"dimens" => $dimens,
									"desc" => $desc,
									//"note" => $note,
									"currency" => $currency,
									"measurement" => $measurement,
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"questions" => array(),
									"managerID"=>$manager_result["_id"]->{'$id'},
									"anon"=>"NO",
									"cnEnabled" => "NO");


		  	$result = $collection_properties->insert($document_property);
			$new_pid = $document_property["_id"]->{'$id'};
			if ($image == "property_default") {
				$image = "https://s3.amazonaws.com/spacio-user-images/".$new_pid."_property.jpg";
			}

			$newdata = array('$set' => array("image" => $image));
			$u_result = $collection_properties->update(array("_id" => new MongoId($new_pid)), $newdata);

		  if ($result) {

			$return = array("status" => 1,
							"pid" => $new_pid
							);
		  } else {
			$return = array("status" => 2);
		  }

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function getHostedCount($managerID,$vtoken) {
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$cursor = $collection_hosted->find(array('managerID' => $managerID));

			$hosted = array();
			foreach ($cursor as $doc) {

				array_push($hosted, $doc);
			}

			$return = array(
				"status" => 1,
				"hosted" => $hosted
			);
		} else {

			$return = array("status" => 0);
		}

		return $return;
	}

	static function syncHosted ($managerID,$vtoken, $hosted) {
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), 'vtoken' => $vtoken));

		if (!is_null($manager_result)) {
			foreach ($hosted as $hosted_count) {
				$pid = $hosted_count["pid"];
				$dateHosted = $hosted_count["dateHosted"];
				$dayHosted = $hosted_count["dayHosted"];
				$offset = $hosted_count["offset"];

				$timeStamp = $dateHosted;
				$timeStamp = $timeStamp / 1000;
				$hostedTime = new MongoDate($timeStamp);

				$newdata = array('$set' => array("lastEdited" => $hostedTime));
				$u_result = $collection_properties->update(array("_id" => new MongoId($pid)), $newdata);

				$adjusted_timeStamp = $dateHosted;
				$adjusted_timeStamp = $adjusted_timeStamp / 1000 - $offset;

				$adjusted_tomorrow_timeStamp = strtotime('tomorrow',$adjusted_timeStamp);
				$adjusted_today_timeStamp = strtotime('today',$adjusted_timeStamp);

				$actual_tomorrow_timeStamp = $timeStamp + ($adjusted_tomorrow_timeStamp - $adjusted_timeStamp);
				$actual_today_timeStamp = $timeStamp + ($adjusted_today_timeStamp - $adjusted_timeStamp);

				$tomorrow = new MongoDate($actual_tomorrow_timeStamp);
				$today = new MongoDate($actual_today_timeStamp);

				$condition = array('managerID'=>$managerID,'pid'=>$pid,'dateHosted' => array('$lt'=>$tomorrow, '$gt'=>$today) );
				$hosted_result = $collection_hosted->findOne($condition);
				if (is_null($hosted_result)) {
					$document_hosted = array(
										"pid" => $pid,
										"managerID" => $managerID,
										"brokerageID" => $manager_result["brokerageID"],
										"officeID" => $manager_result["officeID"],
										"dateHosted" => $hostedTime,
										"dayHosted" => $dayHosted,

										);


					$result = $collection_hosted->insert($document_hosted);
				}
			}

			$return = array("status" => 1, "tommorrow" => $tomorrow, "today" => $today);

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function addCurOHObj($managerID,$vtoken,$ohObj) {
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);

		$ohID = $ohObj["ohID"];
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$oh_result = $collection_hosted->findOne(array('ohID' => $ohID, "managerID" => $managerID));
			if (!is_null($oh_result)) {

				$old_leadsCount = $oh_result["leadsCount"];
				$old_minsHosted = $oh_result["minsHosted"];

				$new_leadsCount = $old_leadsCount + $ohObj["leadsCount"];
				$new_minsHosted = $old_minsHosted + $ohObj["minsHosted"];

				$newdata = array('$set' => array("leadsCount" => $new_leadsCount,
													"minsHosted" => $new_minsHosted
				));

				$u_result = $collection_hosted->update(array("ohID" => $ohID), $newdata);
				$return = array("status" => 1);
			} else {
				$hostedTime = new MongoDate($ohObj["dateHosted"]/1000);

				$document_hosted = array(
					"ohID" => $ohID,
					"pid" => $ohObj["pid"],
					"managerID" => $managerID,
					"brokerageID" => $manager_result["brokerageID"],
					"officeID" => $manager_result["officeID"],
					"dateHosted" => $hostedTime,
					"leadsCount" => $ohObj["leadsCount"],
					"minsHosted" => $ohObj["minsHosted"],
				);


		  		$result = $collection_hosted->insert($document_hosted);

				$return = array("status" => 1);
			}
		} else {

			$return = array("status" => 0);
		}

		return $return;

	}

	static function updateHostedCount($managerID,$vtoken, $pid, $dateHosted, $dayHosted, $offset) {

		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);

		$newdata = array('$set' => array("lastEdited" => $currentTime));
		$u_result = $collection_properties->update(array("_id" => new MongoId($pid)), $newdata);

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		//$property_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$timeStamp = $dateHosted;
			$timeStamp = $timeStamp / 1000;
			$hostedTime = new MongoDate($timeStamp);

			$adjusted_timeStamp = $dateHosted;
			$adjusted_timeStamp = $adjusted_timeStamp / 1000 - $offset;


			$adjusted_tomorrow_timeStamp = strtotime('tomorrow',$adjusted_timeStamp);
			$adjusted_today_timeStamp = strtotime('today',$adjusted_timeStamp);

			$actual_tomorrow_timeStamp = $timeStamp + ($adjusted_tomorrow_timeStamp - $adjusted_timeStamp);
			$actual_today_timeStamp = $timeStamp + ($adjusted_today_timeStamp - $adjusted_timeStamp);


			/* Find if this open house has been hosted once already today */
			$tomorrow = new MongoDate($actual_tomorrow_timeStamp);
			$today = new MongoDate($actual_today_timeStamp);

			$condition = array('managerID'=>$managerID,'pid'=>$pid,'dateHosted' => array('$lt'=>$tomorrow, '$gt'=>$today) );
			$hosted_result = $collection_hosted->findOne($condition);

			if (is_null($hosted_result)) {
				$document_hosted = array(
					"pid" => $pid,
					"managerID" => $managerID,
					"brokerageID" => $manager_result["brokerageID"],
					"officeID" => $manager_result["officeID"],
									"dateHosted" => $hostedTime,
									"dayHosted" => $dayHosted,
									);


		  		$result = $collection_hosted->insert($document_hosted);

				$return = array("status" => 1);
			} else {
				$return = array("status" => 2);
			}

		} else {

			$return = array("status" => 0);
		}

		return $return;

	}

	static function saveForm($managerID,$vtoken,$pid,$questionsObj,$anon,$mobile,$autoEmail,$autoBCC,$cnEnabled,$brokersEnabled,$emailMandatory,$phoneMandatory) {

		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);

			$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

			if (is_null($questionsObj)) {
				$questionsObj = array();
			}
		 	$newdata = array('$set' => array("questionsObj" => $questionsObj,"anon" => $anon,"mobile" => $mobile,"autoEmail" => $autoEmail,"autoBCC" => $autoBCC,
									"lastEdited" => $currentTime, "cnEnabled" => $cnEnabled, "brokersEnabled" => $brokersEnabled, "emailMandatory" => $emailMandatory, "phoneMandatory" => $phoneMandatory));

			$u_result = $collection_properties->update(array("_id" => new MongoId($pid)), $newdata);

			if ($u_result) {
				$return = array("status" => 1);
		 	 } else {
				$return = array("status" => 2);
		 	 }
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editProperty($managerID,$vtoken,$pid,$url,$title,$addr1,$addr2,$image,$price,$beds,$baths,$dimens,$desc,$currency,$measurement,$cnEnabled, $anon,$mobile,$autoEmail, $questionsObj, $disabled, $brokersEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		//Found the manager and authenticated with proper vtoken
		if (!is_null($manager_result)) {

		  	$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);

		 	$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

			$desc = str_replace("<br>","\n",$desc);

			if ($image == "property_default") {
				$image = "https://s3.amazonaws.com/spacio-user-images/".$pid."_property.jpg";
			}

			$newdata = array('$set' => array("url" => $url,
									"title"=> $title,
									"addr1"=> $addr1,
									"addr2"=> $addr2,
									"image"=> $image,
									"price" => $price,
									"beds" => $beds,
									"baths" => $baths,
									"dimens" => $dimens,
									"desc" => $desc,
									"currency" => $currency,
									"measurement" => $measurement,
									"lastEdited" => $currentTime,
									"managerID"=>$managerID,
									"questionsObj"=>$questionsObj,
									"cnEnabled"=>$cnEnabled,
									"anon"=>$anon,
									"mobile"=>$mobile,
									"disabled" => $disabled,
									"brokersEnabled" => $brokersEnabled,
									"autoEmail"=>$autoEmail,
									"sample"=>false
									));

			$u_result = $collection_properties->update(array("_id" => new MongoId($pid)), $newdata);

			if ($u_result) {
				$return = array("status" => 1);
		 	 } else {
				$return = array("status" => 2);
		 	 }

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function deleteProperty($managerID,$vtoken,$pid) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if($manager_result["teamID"] == "N/A" || $manager_result["teamID"] == "") {
				$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
				$property_result = $collection_properties->findOne(array('_id' => new MongoId($pid), "managerID" => $managerID));

				if (!is_null($property_result)) {
					$collection_properties->remove(array('_id' => new MongoId($pid),'managerID' => $managerID));
					$collection_hosted->remove(array('managerID' => $managerID,'pid' => $pid));

					$return = array(
						"status" => 1
					);
				} else {
					$return = array(
						"status" => 2
					);
				}
			} else if($manager_result["teamRole"] == "admin") {
				$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
				$property_result = $collection_properties->findOne(array('_id' => new MongoId($pid), "teamID" => $manager_result["teamID"]));

				if (!is_null($property_result)) {
					$collection_properties->remove(array('_id' => new MongoId($pid),'teamID' => $manager_result["teamID"]));

					$return = array(
						"status" => 1
					);
				} else {
					$return = array(
						"status" => 2
					);
				}
			} else {
				$return = array(
					"status" => 3
				);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function disableProperty($managerID,$vtoken,$pid,$disabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$newdata = array('$set' => array("disabled" => $disabled ));
			$u_result = $collection_properties->update(array("_id" => new MongoId($pid)), $newdata);

			$return = array("status" => 1, "disabled" => $disabled);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getPropertyByID($propertyID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$property_result = $collection_properties->findOne(array('propertyID' => $propertyID));

		if (!is_null($property_result)) {

				$return = array(
					"status" => 1,
					"property" => $property_result
				);


		} else {
			$return = array("status" => 2);
		}

		return $return;
	}

	static function getFormByID($propertyID) {
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$property_result = $collection_properties->findOne(array('propertyID' => $propertyID));

		if (!is_null($property_result)) {

				$managerID = $property_result["managerID"];
				$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID)));

				$disabled = array_key_exists("disabled", $property_result) ? (string)$property_result["disabled"] : "NO";
				$brokersEnabled = array_key_exists("brokersEnabled", $property_result) ? (string)$property_result["brokersEnabled"] : "NO";

				if ($disabled == 'NO') {
					$return = array(
						"status" => 1,
						"title" => $property_result["title"],
						"image" => $property_result["image"],
						"questions" => $property_result["questions"],
						"questionsObj" => $property_result["questionsObj"],
						"mls" => $property_result["mls"],
						"mlsnum" => $property_result["mlsnum"],
						"anon" => $property_result["anon"],
						"brokersEnabled" => $brokersEnabled,
						"cnEnabled" => $property_result["cnEnabled"],
						"agent_fname" => $manager_result["fname"],
						"agent_lname" => $manager_result["lname"],
						"agent_title" => $manager_result["title"],
						"agent_email" => $manager_result["email"],
						"agent_brokerage" => $manager_result["brokerage"],
						"agent_phone" => $manager_result["phone"],
						"agent_marketsnapID" => $manager_result["marketsnapID"],
						"agent_id" => $managerID,
						"agent_vtoken" => $manager_result["vtoken"],
						"agent_pphoto" => $manager_result["pphoto"]
					);
				} else {
					$return = array("status" => 3);
				}

		} else {
			$return = array("status" => 2);
		}

		return $return;
	}

	static function getPropertyForRegistrant($rid) {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$registrant_result = $collection_registrants->findOne(array('_id' => new MongoId($rid)));

		if (!is_null($registrant_result)) {
			$property_result = $collection_properties->findOne(array('propertyID' => $registrant_result["propertyID"]));

			if (!is_null($property_result)) {
				$return = array(
					"status" => 1,
					"property" => $property_result
				);
			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function syncProperties($managerID,$vtoken,$properties) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);

			$tempIDs = array();
			$propertiesAdded = 0;
			$propertiesEdited = 0;
			$propertiesDeleted = 0;

			foreach ($properties as $key=>$property) {
				$id = (string)$property["id"];
				$url =(string)$property["url"];
				$title =(string)$property["title"];
				$addr1 =(string)$property["addr1"];
				$addr2=(string)$property["addr2"];
				$image = (string)$property["image"];
				$price = (string)$property["price"];
				$beds = (float)$property["beds"];
				$baths = (float)$property["baths"];
				$dimens = (float)$property["dimens"];
				$desc = (string)$property["desc"];
				$questions = array_key_exists("questions", $property) ? $property["questions"] : array();
				$currency = array_key_exists("currency", $property) ? (string)$property["currency"] : "N/A";
				$measurement = array_key_exists("measurement", $property) ? (string)$property["measurement"] : "N/A";
				$cnEnabled = array_key_exists("cnEnabled", $property) ? (string)$property["cnEnabled"] : "NO";
				$anon = array_key_exists("anon", $property) ? (string)$property["anon"] : "NO";
				$autoEmail = array_key_exists("autoEmail", $property) ? (string)$property["autoEmail"] : "NO";
				$mls = array_key_exists("mls", $property) ? (string)$property["mls"] : "N/A";
				$mlsnum = array_key_exists("mlsnum", $property) ? (string)$property["mlsnum"] : "N/A";
				$tlc = array_key_exists("tlc", $property) ? (string)$property["tlc"] : "N/A";
				$edited = array_key_exists("edited", $property) ? (bool)$property["edited"] : false;
				$added = array_key_exists("added", $property) ? (bool)$property["added"] : false;
				$deleted = array_key_exists("deleted", $property) ? (bool)$property["deleted"] : false;
				$mobile = array_key_exists("mobile", $property) ? (string)$property["mobile"] : "YES";

				//Sanitize Desc
				$desc = str_replace("<br>","\n",$desc);

				$first4 = substr($id, 0, 4);

				//None-Synced Property ID
				if($first == "temp" || $added) {
					$propertiesAdded++;
					$nextseq = $collection_counter->findAndModify(
						 array("desc" => "propertyID"),
						 array('$inc' => array('maxCount' => 1)),
						 null,
						 array(
							"new" => true
						)
					);

					if (is_null($questions)) {
						$questions = array();
					}

					if($manager_result["brokerageID"] != "N/A") {
						$brokerage_result = $collection_brokerages->findOne(array("brokerageID" => $manager_result["brokerageID"]));
						$questions = $brokerage_result["questions"];
						$autoEmail == "NO";
					}

					if($image == "stock") {
						$randnum = rand ( 1 , 10 );
						$image = "http://www.spac.io/assets/img/stock/".$randnum.".jpg";
					} else {
						$image == "property_default";
					}

		 			$document_property = array(
									"propertyID" => $nextseq["maxCount"],
									"mls" => $mls,
									"mlsnum" => $mlsnum,
									"teamID" => "N/A",
									"url" => $url,
									"title"=> $title,
									"addr1"=> $addr1,
									"addr2"=> $addr2,
									"image"=> $image,
									"price" => $price,
									"beds" => $beds,
									"baths" => $baths,
									"dimens" => $dimens,
									"desc" => $desc,
									"currency" => $currency,
									"measurement" => $measurement,
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"tlc" => $tlc,
									"questions" => $questions,
									"managerID"=>$manager_result["_id"]->{'$id'},
									"brokerageID"=>$manager_result["brokerageID"],
									"anon"=>$anon,
									"autoEmail" => $autoEmail,
									"cnEnabled" => $cnEnabled,
									"mobile"=>$mobile,
									"editable"=>true,
									"sample" => false);

		  			$result = $collection_properties->insert($document_property);
					$new_pid = $document_property["_id"]->{'$id'};

					$tempIDs[$id] = $new_pid;

					if ($image == "property_default") {
						$image = "https://s3.amazonaws.com/spacio-user-images/".$new_pid."_property.jpg";
					}

					$newdata = array('$set' => array("image" => $image));
					$u_result = $collection_properties->update(array("_id" => new MongoId($new_pid)), $newdata);

					$properties[$key]["id"] = $new_pid;
					$properties[$key]["image"] = $image;

				} else if($edited){
		 			$propertiesEdited++;
					if ($image == "property_default") {
						$image = "https://s3.amazonaws.com/spacio-user-images/".$id."_property.jpg";
					}

					$newdata = array('$set' => array("url" => $url,
									"title"=> $title,
									"addr1"=> $addr1,
									"addr2"=> $addr2,
									"image"=> $image,
									"price" => $price,
									"beds" => $beds,
									"baths" => $baths,
									"dimens" => $dimens,
									"desc" => $desc,
									"currency" => $currency,
									"measurement" => $measurement,
									"lastEdited" => $currentTime,
									"managerID"=>$managerID,
									"questions"=>$questions,
									"cnEnabled"=>$cnEnabled,
									"anon"=>$anon,
									"mobile"=>$mobile,
									"autoEmail"=>$autoEmail,
									"sample"=>false
									));

					$u_result = $collection_properties->update(array("_id" => new MongoId($id)), $newdata);

					unset($properties[$key]["edited"]);
				} else if($deleted) {
					$propertiesDeleted++;
					$collection_properties->remove(array('_id' => new MongoId($id),'managerID' => $managerID));
					$collection_hosted->remove(array('managerID' => $managerID,'pid' => $id));

					unset($properties[$key]);
				} else {
					$property_result = $collection_properties->findOne(array('_id' => new MongoId($id), "managerID" => $managerID));

					if(is_null($property_result)) {
						$properties[$key]["toDelete"]=true;
					}
				}
			}

			$return = array("status" => 1,
							"properties" => $properties,
							"tempIDs" => $tempIDs,
							"propertiesAdded" => $propertiesAdded,
							"propertiesDeleted" => $propertiesDeleted,
							"propertiesEdited" => $propertiesEdited);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getMLSList($managerID,$vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_mls = Db_Conn::getInstance()->getConnection()->mls;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		if (!is_null($manager_result)) {
			$cursor = $collection_mls->find();
			$cursor->sort(array('mls_name' => 1));
			$mls_list = array();

			foreach ($cursor as $doc) {
    			$mls_id = $doc["mls_id"];
				$mls_list[$mls_id] = $doc;
			}

			$return = array("status" => 1,
				"mlslist" => $mls_list
			);
		} else {
			// User ID not found or validation failure
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getPropertiesByManager($managerID,$vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if($manager_result["teamID"] == "N/A" || $manager_result["teamID"] == "") {

				if($manager_result["mls"] == "crmls") {
					//syncCRMLSListings($manager_result["mlsID"], $managerID);
				}

				if($manager_result["brokerageID"] != "N/A"){
					syncFromBank($manager_result["email"],$manager_result["brokerageID"]);
				}

				$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

				$cursor = $collection_properties->find(array('managerID' => $managerID));
				$cursor->sort(array('lastEdited' => -1));

				$properties = array();
				foreach ($cursor as $doc) {
					$doc["id"] =  $doc["_id"]->{'$id'};
					if(!array_key_exists("questionsObj",$doc)) {
						$doc["questionsObj"] = array();
					}
					array_push($properties, $doc);
				}

				$return = array(
					"status" => 1,
					"properties" => $properties
				);
			} else {
				$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

				$cursor = $collection_properties->find(array('teamID' => $manager_result["teamID"]));
				$cursor->sort(array('lastEdited' => -1));

				$properties = array();
				foreach ($cursor as $doc) {
					$doc["id"] =  $doc["_id"]->{'$id'};

					array_push($properties, $doc);
				}

				$return = array(
					"status" => 1,
					"properties" => $properties
				);
			}
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}




}

?>
