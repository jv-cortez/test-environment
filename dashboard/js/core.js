// JavaScript Document

var winWidth = 0;
var winHeight = 0;	


/*--------------UTILITY FUNCTIONS------------------*/


function getWinSize() {
		if( typeof( window.innerWidth ) == 'number' ) {
    	//Non-IEaler
    		winWidth = window.innerWidth;
    		winHeight = window.innerHeight;
  		} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
			//IE 6+ in 'standards compliant mode'
			winWidth = document.documentElement.clientWidth;
			winHeight = document.documentElement.clientHeight;
  		} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
			//IE 4 compatible
			winWidth = document.body.clientWidth;
			winHeight = document.body.clientHeight;
  		}
		
}

function randomString(_length) {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	
	var randomstring = '';
	for (var i=0; i<_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	
	return randomstring;
}

function debugLog(txt) {
	$("#console").append(txt+"<br/>");
	console.log(txt);
}

