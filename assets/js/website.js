var wsURL_new = "https://ws.spac.io/prod/oh/webservice/1.1.6/webservice.php";


insertTemplates();

function insertTemplates() {

  $(".spacio-top-menu").loadTemplate("/topmenu.html");
  $(".footer").loadTemplate("/footer.html");
  $(".popup-overlay").loadTemplate("/popups.html");


/*
  var footer = document.querySelector('#footer');
  var template2 = footer.import.querySelector('template');
  var clone2 = document.importNode(template2.content, true);

  var popups = document.querySelector('#popups');
  var template3 = popups.import.querySelector('template');
  var clone3= document.importNode(template3.content, true);


  $(".footer").html(clone2);
  $("body").append(clone3);
  */
  setTimeout(function(){
  $('#nav-icon').click(function(){
    $(this).toggleClass('open');
    $(".spacio-top-menu").toggleClass('open');
    setTimeout(function(){
        $(".spacio-mobile-menu-content").toggle(500);
    }, 500);
  });
  }, 500);
}

function insertAlertBox() {

  $(".alert-overlay").loadTemplate("/alertbox.html");

}


function showPopup(type) {


	$(".popup-error-message").html("");
  if(type == "request") {
    $(document.documentElement).css('overflow', 'hidden');
    $(".request").show();
    $(".contact").hide();
  }
  if(type == "contact") {
    $(document.documentElement).css('overflow', 'hidden');
    $(".request").hide();
    $(".contact").show();
  }
  $(".popup-overlay").fadeIn();
}

function hidePopup() {
  $(document.documentElement).css('overflow', '');
  $(".popup-overlay").fadeOut();
  $(".popup-overlay-2").fadeOut();
}

function requestDemo() {
	var query = {};
	var wsURL = "../../oh/webservice/1.1.6/webservice.php";

	query["fn"] = "requestDemo";
	query["name"] = $("input[name='request-name']").val();
	query["company"] = $("input[name='request-company']").val();
	query["phone"] = $("input[name='request-phone']").val();
	query["email"] = $("input[name='request-email']").val();
	query["product"] = $("select[name='request-product']").val();
  query["teamsize"] = $("input[name='request-teamsize']").val();
  query["brokeragesizeagents"] = $("input[name='request-brokeragesizeagents']").val();
  query["brokeragesizeoffices"] = $("input[name='request-brokeragesizeoffices']").val();

	query["message"] = $("textarea[name='request-message']").val();
  query["howhearabout"] = $("textarea[name='request-howhearabout']").val();

	var json_data = query;

	if(query["name"] == "") {
		$(".popup-error-message").html("Please include your name");
	} else if(query["company"] == "") {
		$(".popup-error-message").html("Please include your company.");
	} else if(query["email"] == "") {
		$(".popup-error-message").html("Please include your email.");
	} else if(query["product"] == "") {
		$(".popup-error-message").html("Please choose a product.");
	} else if(query["product"] == "Spacio for Teams" && query["teamsize"] == 0){
    $(".popup-error-message").html("Please include a valid team size.");
  } else if(query["product"] == "Brokerage Solution" && query["brokeragesizeagents"] == 0){
    $(".popup-error-message").html("Please include a valid number of agents.");
  } else if(query["product"] == "Brokerage Solution" && query["brokeragesizeoffices"] == 0){
    $(".popup-error-message").html("Please include a valid number of offices.");
  } else {
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					$(".popup-error-message").html("<span style='color:#00FF8F'>Thank you for your interest!</span>");
					setTimeout(function(){hidePopup();},1000);
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});
	}
}

function contactSales() {
	var query = {};
	var wsURL = "../../oh/webservice/1.1.6/webservice.php";

	query["fn"] = "contactSales";
	query["name"] = $("input[name='contact-name']").val();
	query["company"] = $("input[name='contact-company']").val();
	query["phone"] = $("input[name='contact-phone']").val();
	query["email"] = $("input[name='contact-email']").val();
	query["product"] = $("select[name='contact-product']").val();
  query["teamsize"] = $("input[name='contact-teamsize']").val();
  query["brokeragesizeagents"] = $("input[name='contact-brokeragesizeagents']").val();
  query["brokeragesizeoffices"] = $("input[name='contact-brokeragesizeoffices']").val();
	query["message"] = $("textarea[name='contact-message']").val();
  query["howhearabout"] = $("textarea[name='contact-howhearabout']").val();

	var json_data = query;

	if(query["name"] == "") {
		$(".popup-error-message").html("Please include your name.");
	} else if(query["company"] == "") {
		$(".popup-error-message").html("Please include your company.");
	} else if(query["email"] == "") {
		$(".popup-error-message").html("Please include your email.");
	} else if(query["product"] == "") {
		$(".popup-error-message").html("Please choose a product.");
	} else if(query["product"] == "Spacio for Teams" && query["teamsize"] == 0){
    $(".popup-error-message").html("Please include a valid team size.");
  } else if(query["product"] == "Brokerage Solution" && query["brokeragesizeagents"] == 0){
      $(".popup-error-message").html("Please include a valid number of agents.");
  } else if(query["product"] == "Brokerage Solution" && query["brokeragesizeoffices"] == 0){
      $(".popup-error-message").html("Please include a valid number of offices.");
  } else if(query["message"] == ""){
      $(".popup-error-message").html("Please include a message.");
  } else {
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					$(".popup-error-message").html("<span style='color:#00FF8F'>Thank you for your interest!</span>");
					setTimeout(function(){hidePopup();},1000);
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});
	}
}

function exitRequest() {
	var query = {};
	var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";

	query["fn"] = "exitRequest";
	query["name"] = $("input[name='exit-name']").val();
	query["company"] = $("input[name='exit-company']").val();
	query["email"] = $("input[name='exit-email']").val();
  query["offices"] = $("input[name='exit-offices']").val();
	query["message"] = $("textarea[name='exit-message']").val();

	var json_data = query;

	if(query["name"] == "") {
		$(".popup-error-message").html("Please include your name");
	} else if(query["company"] == "") {
		$(".popup-error-message").html("Please include your brokerage.");
	} else if(query["email"] == "") {
		$(".popup-error-message").html("Please include your email.");
	} else {
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					//$(".popup-error-message").html("<span style='color:#00FF8F'>Thank you for your interest!</span>");
					//setTimeout(function(){$(".popup-overlay-2").hide();},1000);
          $(".popup-overlay-2").hide();
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});
	}
}

function sendEmailToMelissa() {
	var query = {};
	var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";

	query["fn"] = "sendEmailToMelissa";
	query["name"] = $("input[name='send-name']").val();
	query["company"] = $("input[name='send-company']").val();
	query["email"] = $("input[name='send-email']").val();

	var json_data = query;

	if(query["name"] == "") {
		$(".popup-error-message").html("Please include your name");
	} else if(query["company"] == "") {
		$(".popup-error-message").html("Please include your brokerage.");
	} else if(query["email"] == "") {
		$(".popup-error-message").html("Please include your email.");
	} else {
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					//$(".popup-error-message").html("<span style='color:#00FF8F'>Thank you for your interest!</span>");
					//setTimeout(function(){$(".popup-overlay-2").hide();},1000);
          $(".popup-overlay-2").hide();
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});
	}
}

function showLoginOptions() {
  $(".login-overlay").fadeIn();
}

function hideLoginOptions() {
	$(".login-overlay").fadeOut();
}
function toggleProductOptions() {
  $(".product-options").toggle();
  $(".products-submenu").toggle();
  $(".login-options").hide();
}

function toggleMobileProductOptions() {
  $(".products-submenu").toggle(300);
  $(".login-options").hide();
}

function randomString(_length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

		var randomstring = '';
		for (var i=0; i<_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}

		return randomstring;
	}

  function hideAlert() {
    $(".alert-overlay").hide();
  }
  function showAlert(type, title, message, button1, button2, buttonfn1, buttonfn2) {
    if (type == "error") {
      $(".alert-icon img").attr("src","/assets/img/website/icon_error_white.png");
    }

    if (type == "warning") {
      $(".alert-icon img").attr("src","/assets/img/website/icon_warning_white.png");
    }

    if (type == "info") {
      $(".alert-icon img").attr("src","/assets/img/website/icon_info_white.png");
    }

    $(".alert-overlay").show();
  	$(".alert-title").html(title);
  	$(".alert-message").html(message);
  	$(".alert-button-1").off();
  	$(".alert-button-2").off();

  	if (typeof buttonfn1 === "undefined" || buttonfn1 === null) {
  		buttonfn1 = function() {
  			$(".alert-overlay").hide();
  		}
  	}

  	if (typeof buttonfn2 === "undefined" || buttonfn2 === null) {
  		buttonfn2 = function() {
  			$(".alert-overlay").hide();
  		}
  	}

  	if (typeof button1 === "undefined" || button1 === null) {
  		button1 = "Ok";
  		$(".alert-button-1").html(button1);
  		$(".alert-button-1").css("width","100%");
  		$(".alert-button-2").hide();
  		buttonfn1 = function() {
  			$(".alert-overlay").hide();
  		}
  		$(".alert-button-1").on("click", buttonfn1);

    	} else if (typeof button2 === "undefined" || button2 === null) {
  		$(".alert-button-1").html(button1);
  		$(".alert-button-2").hide();
  		$(".alert-button-1").css("width","100%");
  		$(".alert-button-1").on("click", buttonfn1);
  		$(".alert-button-2").on("click", buttonfn2);

  	} else {
  		$(".alert-button-1").show();
  		$(".alert-button-2").show();
  		$(".alert-button-1").css("width","50%");
  		$(".alert-button-2").css("width","50%");
  		$(".alert-button-1").html(button1);
  		$(".alert-button-2").html(button2);
  		$(".alert-button-1").off();
  		$(".alert-button-2").off();
  		$(".alert-button-1").on("click", buttonfn1);
  		$(".alert-button-2").on("click", buttonfn2);
  	}
  }

  function productChanged(box) {
    console.log(box);
    var product = $("select[name='"+box+"-product']").val();
    console.log(product);
    if (product == "Spacio for Teams") {
      $("."+box+"-teamsize").show();
      $("."+box+"-brokeragesizeagents").hide();
      $("."+box+"-brokeragesizeoffices").hide();
    } else if (product == "Brokerage Solution"){
        $("."+box+"-teamsize").hide();
        $("."+box+"-brokeragesizeagents").show();
        $("."+box+"-brokeragesizeoffices").show();
    } else if (product == "Franchise Solution"){
        $("."+box+"-teamsize").hide();
        $("."+box+"-brokeragesizeagents").hide();
        $("."+box+"-brokeragesizeoffices").hide();
    } else {
      $("."+box+"-teamsize").hide();
      $("."+box+"-brokeragesizeagents").hide();
      $("."+box+"-brokeragesizeoffices").hide();
    }
  }

  function loading(flag, msg, delay) {

		if (typeof delay === "undefined" || delay === null) {
			delay = 1000;
		}

		msg = typeof msg !== 'undefined' ? msg : "Loading...";

		if (flag) {
			$(".loading-panel .msg").html(msg);
			$(".loading-panel").show();
		} else {
			setTimeout(function(){ $(".loading-panel").hide(); }, delay);
		}
	}

	function toggleFooterSubmenu(submenu) {
		$("."+submenu+"-submenu").toggle();
		$("."+submenu+"-menu-btn").toggleClass("toggled");

	}

  function setPanicClip() {
    var embeds = [
      "<video poster='//i.imgur.com/UyRv2cK.jpg' preload='auto' autoplay='autoplay' muted='muted' loop='loop' webkit-playsinline='' style='width:100%;'><source src='//i.imgur.com/UyRv2cK.mp4' type='video/mp4'></video>",
      "<video poster='//i.imgur.com/Wiyd9P7.jpg' preload='auto' autoplay='autoplay' muted='muted' loop='loop' webkit-playsinline='' style='width:100%;'><source src='//i.imgur.com/Wiyd9P7.mp4' type='video/mp4'></video>",
      "<video poster='//i.imgur.com/GIcolIl.jpg' preload='auto' autoplay='autoplay' muted='muted' loop='loop' webkit-playsinline='' style='width:100%;'><source src='//i.imgur.com/GIcolIl.mp4' type='video/mp4'></video>",
      "<video poster='//i.imgur.com/z8NSues.jpg' preload='auto' autoplay='autoplay' muted='muted' loop='loop' webkit-playsinline='' style='width:100%;'><source src='//i.imgur.com/z8NSues.mp4' type='video/mp4'></video>",
      "<video poster='//i.imgur.com/zZMdmdn.jpg' preload='auto' autoplay='autoplay' muted='muted' loop='loop' webkit-playsinline='' style='width:100%;'><source src='//i.imgur.com/zZMdmdn.mp4' type='video/mp4'></video>",
      "<video poster='//i.imgur.com/2rdRSFD.jpg' preload='auto' autoplay='autoplay' muted='muted' loop='loop' webkit-playsinline='' style='width:100%;'><source src='//i.imgur.com/2rdRSFD.mp4' type='video/mp4'></video>"];
    var randIdx = Math.random() * embeds.length;
    randIdx = parseInt(randIdx, 10);
    var embed = embeds[randIdx];
    $('.panic-clip').append(embed);
  }

  function updateStats() {
    if (location.protocol == 'https:') {
      var urlPrefix = 'https';
    } else {
      var urlPrefix = 'http';
    }
    var metrics = [ urlPrefix+'://ws.spac.io/prod/site/stats.php' ];
    var options = { useEasing: true };
    $.each( metrics, function( index, value ) {
      $.getJSON( value, function( data ) {
        var agents = new CountUp('agents', 0, data.agents, 0, 2.5, options);
        var offices = new CountUp('offices', 0, data.offices, 0, 2.5, options);
        var projects = new CountUp('projects', 0, data.projects, 0, 2.5, options);
        var visitors = new CountUp('visitors', 0, data.visitors, 0, 2.5, options);
        agents.start();
        offices.start();
        projects.start();
        visitors.start();
        // $("#agents").html(data.agents+'<span class="xlarge" id="agentsu">'+data.agentsu+'</span>');
        // $("#offices").html(data.offices+'<span class="xlarge" id="officesu">'+data.officesu+'</span>');
        // $("#projects").html(data.projects+'<span class="xlarge" id="projectsu">'+data.projectsu+'</span>');
        // $("#visitors").html(data.visitors+'<span class="xlarge" id="visitorsu">'+data.visitorsu+'</span>');
      });
    });
  }
/*
$(".brokerages").hide();
$(".builders").hide();

function agentsSelect() {
  $(".brokerages").hide();
  $(".builders").hide();
  $(".agents").show();
}

function brokeragesSelect() {
  $(".brokerages").show();
  $(".agents").hide();
  $(".builders").hide();
}

function buildersSelect() {
  $(".brokerages").hide();
  $(".agents").hide();
  $(".builders").show();
}

$("div[id='agents']").click(function() {
  agentsSelect()
});

$("div[id='brokerages']").click(function(){
  brokeragesSelect()
});

$("div[id='builders']").click(function() {
  buildersSelect()
});

function moveCarousel() {
  var select = $('.scroll-two.active');
  var next = select.next('.scroll-two');
  select.removeClass('active');
  if (next.length) {
    next.addClass('active');
    $(".scroll-two").hide();
    $(".scroll-two.active").show();
  } else {
    $(".scroll-two:first").addClass('active');
    $(".scroll-two.active").show();
  }
}

setInterval(moveCarousel, 10000);
*/
