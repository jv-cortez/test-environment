/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
		
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("offline", this.onOffline, false);
        document.addEventListener("online", this.onOnline, false);
		$( "form#loginManager_Form" ).submit(function( event ) {
			//Serialize form inputs
			var _fields = $( this ).serializeArray();
			loginManager(_fields);
			event.preventDefault();
		});
		
		$("form#propertyReg_Form").submit(function( event ) {
			//Serialize form inputs
			var _fields = $( this ).serializeArray();
			regForProperty(_fields);
			event.preventDefault();
		});
		
		$("form#editProfile_Form").submit(function( event ) {
			//Serialize form inputs
			var _fields = $( this ).serializeArray();
			editProfile(_fields);
			event.preventDefault();
		});
		
		$( "form#broadcastMessage_Form" ).submit(function( event ) {
				
				//Serialize form inputs
				var _fields = $( this ).serializeArray();
				//broadcastMessage(_fields);
				closePopup();
				event.preventDefault();
			});
		
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
		console.log("Device ready");
		resizeWindow();
		initDB();

		
    },
	// Handle the offline event
    onOffline: function() {
        online = false;
        console.log("App is offline.");
        $(".main-container").append("<div class='offline-bar'><span class='pulse-3s'>Please connect to the internet.</span></div>");
		
        
    },
    // Handle the online event
    onOnline: function() {
	
		if (!online) {
			login();
		}
         online = true;
         $(".offline-bar").remove();
		 //login();
    },
	
    // Update DOM on a Received Event
    receivedEvent: function(id) {
		
        console.log('Received Event: ' + id);
    }
};

app.initialize();