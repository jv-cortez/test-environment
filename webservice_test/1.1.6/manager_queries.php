<?php

require_once('../../require/db_conn.php');
// require_once('../../require/core.php');
// require_once('../../require/vendor/autoload.php');
require_once('shared_queries.php');
require_once('integrations.php');

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

class Man_Query {
	/* ------------------------------ MANAGER RELATED FUNCTIONS ------------------------------*/
	// Type defaults to user, other possible: admin, superadmin

	static function unsubscribeEmail($email, $managerID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "email" => $email));

		if (!is_null($manager_result)) {
			$newdata = array('$set' => array('onboardEmail' => 'NO'));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID)), $newdata);

			$return = array("status" => 1);
		} else {
			$return = array("status" => 2);
		}

		return $return;
	}

	static function unsubscribeEmail2($email,$uid,$onboard,$system,$campaign,$claim,$type){
		$mc = Db_Conn::getInstance()->getConnection()->managers;
		$uac = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$celc = Db_Conn::getInstance()->getConnection()->campaign_email_list;
		//check type
		if($type == 'claimed'){
			$manager = $mc->findOne(['_id' => new MongoDB\BSON\ObjectID($uid), 'email' => $email]);
		}	elseif($type == 'unclaimed'){
			$unclaimedAccount = $uac->findOne(['_id' => new MongoDB\BSON\ObjectID($uid), 'email' => $email]);
			$unclaimedAccount2 = $uac->findOne(['_id' => new MongoDB\BSON\ObjectID($uid), 'email2' => $email]);
			//check which unclaimed email
			if($unclaimedAccount != ''){
				$manager = $unclaimedAccount;
			} elseif ($unclaimedAccount2 != ''){
				$manager = $unclaimedAccount2;
			}
		}	elseif($type == 'campaign'){
			$manager = $celc->findOne(['_id' => new MongoDB\BSON\ObjectID($uid), 'email' => $email]);
		} else {
			$return = ["status" => 2];
		}

		if ($manager != '') {
			//set data
			$unsubObj = [
				'onboard' => $onboard,
				'system' => $system,
				'campaign' => $campaign,
				'claim' => $claim
			];
			$newdata = ['$set' => [
				'unsubObj' => $unsubObj
			]];
			//update matching
			$mc->updateOne(['email' => $manager["email"]], $newdata);
			$uac->updateOne(['email' => $manager["email"]], $newdata);
			$uac->updateOne(['email2' => $manager["email"]], $newdata);
			$celc->updateOne(['email' => $manager["email"]], $newdata);

			$return = ['status' => 1];
		} else {
			$return = ["status" => 0];
		}

		return $return;
	}

	static function getUnsubObj($email,$managerID,$type){
		//check type
		if($type == 'claimed'){
			$mc = Db_Conn::getInstance()->getConnection()->managers;
			$manager = $mc->findOne(['_id' => new MongoDB\BSON\ObjectID($managerID), 'email' => $email]);
		}	elseif($type == 'unclaimed'){
			$uac = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
			$unclaimedAccount = $uac->findOne(['_id' => new MongoDB\BSON\ObjectID($managerID), 'email' => $email]);
			$unclaimedAccount2 = $uac->findOne(['_id' => new MongoDB\BSON\ObjectID($managerID), 'email2' => $email]);
			//check which unclaimed email
			if($unclaimedAccount != ''){
				$manager = $unclaimedAccount;
			} elseif ($unclaimedAccount2 != ''){
				$manager = $unclaimedAccount2;
			}
		}	elseif($type == 'campaign'){
			$celc = Db_Conn::getInstance()->getConnection()->campaign_email_list;
			$manager = $celc->findOne(['_id' => new MongoDB\BSON\ObjectID($managerID), 'email' => $email]);
		} else {
			$return = ["status" => 2];
		}

		if ($manager != '') {
			//check if unsubObj exists
			if(isset($manager['unsubObj'])){
				$unsubObj = $manager['unsubObj'];
			} else {
				$unsubObj = [
					'onboard' => false,
					'system' => false,
					'campaign' => false,
					'claim' => false
				];
			}

			$return = [
				'status' => 1,
				'unsubObj' => $unsubObj
			];
		} else {
			$return = ["status" => 0];
		}

		return $return;
	}

	static function checkUsersBank($email, $salt, $pw) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_users_bank = Db_Conn::getInstance()->getConnection()->users_bank;

		$manager_result = $collection_managers->findOne(array('email' => $email));

		if (!is_null($manager_result)) {
			$return = array("status" => 2);
		} else {
			$users_bank_result = $collection_users_bank->findOne(array('email' => $email, 'franchiseID' => 'sutton'));
			if (!is_null($users_bank_result)) {

				$date = new DateTime();
				$timeStamp = $date->getTimestamp() * 1000;
				$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);
				$expired = new MongoDB\BSON\UTCDateTime($timeStamp - (86400 * 1000)); // 12 hours 43200

				$vtoken = generateRandomString(16);

	  			$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

				$man_fname = $users_bank_result["fname"];
				$man_lname = $users_bank_result["lname"];
				$man_key = $man_fname."".$man_lname;
				$man_key = strtolower($man_key);
				$man_key = str_replace(' ', '', $man_key);
				$counter = 1;
				$duplicate_result = $collection_managers->findOne(array('ukey' => $man_key));

				//If there is a duplicate key, iterate counter until result is null;
				while(!is_null($duplicate_result)) {
					$counter++;
					$man_key = $man_fname."".$man_lname."".$counter;
					$man_key = strtolower($man_key);
					$man_key = str_replace(' ', '', $man_key);
					$duplicate_result = $collection_managers->findOne(array('ukey' => $man_key));
				}

				$document_manager = array("fname" => $users_bank_result["fname"],
									"lname"=> $users_bank_result["lname"],
									"ukey" => $man_key,
									"title" => $users_bank_result["title"],
									"teamID" => "N/A",
									"teamRole" => "N/A",
									"pphoto" => $users_bank_result["pphoto"],
									"email"=> $users_bank_result["email"],
									"phone" => $users_bank_result["phone"],
									"mobile" => $users_bank_result["mobile"],
									"addr1" => "",
									"addr2" => "",
									"brand" => $users_bank_result["brand"],
									"website"=>$users_bank_result["website"],
									"brokerage"=> $users_bank_result["brokerage"],
									"brokerageID"=>$users_bank_result["brokerageID"],
									"franchiseID" => $users_bank_result["franchiseID"],
									"office"=> $users_bank_result["office"],
									"officeID"=> $users_bank_result["officeID"],
									"type" => "trial",
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"topproducerID" => "N/A",
									"marketsnapID" => "N/A",
									"rsID" => "N/A",
									"rsAutoClose" => true,
									"contactuallyID" => "N/A",
									"contactuallyBucket" => "N/A",
									"emailConfirmed" => false,
									"promocode" => "N/A",
									"rtoken" => "",
									"rtDateCreated" => $expired,
									"referred" => "");

				$result = $collection_managers->insertOne($document_manager);

				$sample_result = createSampleProperty((string)$document_manager["_id"]);

				  if ($sample_result) {

					 //SEND CONFIRM EMAIL

					 $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html>
		<body>
			<div style="background:#f7f7f7;padding:20px;">
				<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
				<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
					<p>Hello '.$users_bank_result["fname"].',</p>
					<p>Please confirm your email by clicking <a href="http://www.spac.io/confirmemail.php?e='.$document_manager["_id"]->{'$id'}.'&v='.$vtoken.'">HERE</a></p><p>Download your iPad App by clicking <a href="https://itunes.apple.com/us/app/spacio-pro-best-open-house/id1017394547?ls=1&mt=8">HERE</a>. </p>
				</div>
				<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
					No longer want to receive these messages?<br/>
					You may unsubscribe <a href="http://www.spac.io/unsubscribeme.php" style="color:#999;">here</a>.<br/>
					Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
				</div>
			</div>
		</body>
	</html>',
							'subject' => 'Confirm Your Spacio Email',
							'from_email' => 'hello@spac.io',
							'from_name' => 'Spacio',
							'to' => array(
								array(
									'email' => 'ting@spac.io'
								)
							),
							'headers' => array('Reply-To' => 'hello@spac.io'),
							'important' => false,
							'tags' => array('new-registrant-confirm-email-sutton')

						);
						$async = true;
						$result = $mandrill->messages->send($message, $async);

					$return = array("status" => 1,
									// "managerID" => $document_manager["_id"]->{'$id'},
									"managerID" => (string)$document_manager["_id"],
									"vtoken" => $vtoken);
				  } else {
					$return = array("status" => 0);
				  }

			} else {
				$return = array("status" => 3);
			}
		}

		return $return;

	}

	static function claimAccountNB($email,$id, $pw, $salt, $from) {

		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('email' => $email));

		if (!is_null($manager_result) || $manager_result["type"] == "unclaimed") {

			if($manager_result["pw"] == "" && $manager_result["salt"] == "") {
				$newdata = array('$set' => array('pw' => $pw, 'salt' => $salt, 'type' => 'trial'));
				$u_result = $collection_managers->updateOne(array('email' => $email), $newdata);

				if($manager_result["referred"] == "OLR") {
					 try {
					  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					  $message = array(
						  'html' => " Hello ".$manager_result["fname"]." and welcome to Spacio!<br/><br/>
						  	Download your iPad App by clicking <a href='https://itunes.apple.com/us/app/spacio-pro-best-open-house/id1017394547?ls=1&mt=8'>HERE</a>.<br/><br/>
							<b>Now You Can Connect to Spacio Through OLR</b><br/>
							We've teamed up with OLR to further automate your open houses, making it quick and easy to set up open houses in Spacio directly from your OLR account. When buyers sign into your open house using Spacio, leads with visitors' information will automatically be created in your OLR account CRM.<br/><br/>
							<b>Creating Open Houses in Spacio Directly from OLR</b><br/>
							Simply click the 'Send to Spacio button' in any of these 3 places:<br/>
							- When you add a new listing.<br/>
							- When you schedule open houses.<br/>
							- On your listing page.<br/><Br/>
							<b>Getting Started</b><br/>
							1. Login to your account at spac.io/dashboard or via the Spacio app and complete your profile in My Account.
							2. Ready to add your first open house? Login to your OLR account and click the ͞Send to Spacio͟ button in the 3 places listed above. It’s that easy!<br/>
							3. After your open house, go to My Open Houses, locate the open house, and click on View Registrants to view, edit, and export your list of potential buyers. To send all registrants an update about your listing, go to Create Broadcast Message. All your buyer leads should will also be logged into your OLR account CRM.<br/><br/>

							<b>We hope you enjoy your free 30-day trial of Spacio Pro. With Spacio, you can:</b><br/>
							- Digitally capture leads at open houses with our sign-in form and instantly email property details to buyers. <br/>
							- Manage, cultivate, and convert more leads. Access your lists of buyers anytime to retarget other listings.<br/>
							- Stay connected by broadcasting messages to buyers with updates and other similar properties you list.<br/><br/>

							Spacio is the Paperless Open House, a digital lead collection and management tool that allows you to quickly collect buyer information, send updates to interested buyers, and generate open house reports to show your sellers how much interest their property is generating. <Br/><Br/>
							At the end of your free trial, you can choose to continue with Spacio Pro for a monthly fee. If you choose not to upgrade to Spacio Pro, we'll switch your account over to the Spacio Free plan.<br/><Br/>
							No more pen and paper, no more ugly handwriting, no more missed opportunities.<br/><Br/>
							Good luck!<Br/><Br/><br/>
							The Spacio Team<Br/>
							hello@spac.io",
						  'subject' => 'Welcome to Spacio + OLR',
						  'from_email' => 'hello@spac.io',
						  'from_name' => "Spacio",
						  'to' => array(
							  array(
								  'email' => $email
							  )
						  ),
						  'headers' => array('Reply-To' => "hello@spac.io"),
						  'important' => false,
						  'tags' => array('claim-account-nb-olr')

					  );
					  $async = false;
					  $result = $mandrill->messages->send($message, $async);

				  } catch(Mandrill_Error $e) {
					  // Mandrill errors are thrown as exceptions
					  echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					  // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					  throw $e;
				  }

				}

				$return = array("status" => 1);
			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 3);
		}


		return $return;
	}

	static function getTestimonialTree($managerID, $vtoken)  {

		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$email = $manager_result["ttID"];

			$postLead = array(
				"verb" => "GET",
				"url" => "http://rest.testimonialtree.com/integrator/search?apiKey=B03F0323-0363-4102-A703-5E1F1F312063&emailAddress=$email&returnLimit=5",
				"postData" => ''
			 );
		  $response = crmApi($postLead);

		  //parse xml return
		  $xml = simplexml_load_string($response['data']['Testimonials']);
		  $json = json_encode($xml);
		  $json_array  = json_decode($json, true);
		  $testimonialArray = $json_array['testimonial'];

		  $return = array(
			'status' => 1,
			'msg' => 'success',
			'response' => $testimonialArray
		  );
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getStandardQuestions($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_standard_questions = Db_Conn::getInstance()->getConnection()->standard_questions;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$standard_questions_result = $collection_standard_questions->find([],
				['sort' =>
					['order' => 1]
				]);
			// $standard_questions_result->sort(array('order' => 1));
			$standard_questions = array();
				foreach ($standard_questions_result as $doc) {
					// $doc["questionID"] =  $doc["_id"]->{'$id'};
					$doc["questionID"] =  (string)$doc["_id"];
					unset($doc["_id"]);
					unset($doc["order"]);

					array_push($standard_questions, $doc);
				}

			$return = array("status" => 1,
					"standard_questions" => $standard_questions
				);

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function getCustomQuestions($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_custom_questions = Db_Conn::getInstance()->getConnection()->custom_questions;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$custom_questions_result = $collection_custom_questions->find(array('managerID' => $managerID));


				$custom_questions = array();
				foreach ($custom_questions_result as $doc) {
					// $doc["questionID"] =  $doc["_id"]->{'$id'};
					$doc["questionID"] =  (string)$doc["_id"];
					unset($doc["_id"]);
					unset($doc["managerID"]);

					array_push($custom_questions, $doc);
				}


				$return = array("status" => 1,
					"custom_questions" => $custom_questions
				);


		} else {
			$return = array("status" => 0);
		}

		return $return;

	}



	static function getEmailTemplate($managerID, $vtoken, $type) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->email_templates;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$email_result = $collection_emails->findOne(array('managerID' => $managerID, 'type' => $type));

			if (!is_null($email_result)) {
				$return = array("status" => 1,
					"messagebody" => $email_result["messagebody"]
				);
			} else {

				$return = array("status" => 1,
					"messagebody" => ""
				 );
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function resetEmailTemplate($managerID, $vtoken, $type) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->email_templates;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if($manager_result["brokerageID"] == "N/A" || $manager_result["brokerageID"] == "") {
				if($type == "customer") {
					$messagebody = '{{name}},<br/><Br/>
Thank you for taking the time to attend the Open House for {{oh_addr1}} on {{today}}. If you have any remaining questions regarding this property or any other property, please feel free to contact me at {{agent_phone}} or by email at {{agent_email}}.<Br/><Br/>
Below you will find more information on the property.<br/><Br/>
{{oh_img}}<br/><br/>
Bedrooms: {{oh_beds}}<Br/>
Bathrooms: {{oh_baths}}<Br/>
Price: {{oh_price}}<Br/>
Details: {{oh_details}}<Br/>
More Information: {{oh_url}}<Br/><Br/>
If this particular property did not meet all of the criteria you are looking for, I am confident that I can help you locate your ideal home that meets your criteria within your designated price range to review. <Br/><Br/>
Look forward to hearing from you.<Br/><Br/><Br/>
Sincerely,<Br/><Br/>
{{agent_name}}<BR/>
{{agent_title}}<BR/>
{{agent_company}}<BR/>
{{agent_phone}}<BR/>
{{agent_email}}';
				}

				if($type == "agent") {
					$messagebody = "{{customers_agent_name}},<Br/><Br/>
I'd like to let you know that your client, {{client_name}}, attended my Open House for {{oh_addr1}} on {{today}}. If you have any questions regarding this property, please feel free to contact me at {{agent_phone}} or by email at {{agent_email}}.<Br/><Br/>
Below you will find more information on the property.<Br/><Br/>
{{oh_img}}<br/><br/>
Bedrooms: {{oh_beds}}<Br/>
Bathrooms: {{oh_baths}}<Br/>
Price: {{oh_price}}<Br/>
Details: {{oh_details}}<Br/>
More Information: {{oh_url}}<Br/><Br/>
If your client is interested in this property, I'd love to speak with you. Please let me know.<Br/><Br/>
Look forward to hearing from you.<Br/><Br/><Br/>
Sincerely,<Br/><Br/>
{{agent_name}}<BR/>
{{agent_title}}<BR/>
{{agent_company}}<BR/>
{{agent_phone}}<BR/>
{{agent_email}}<br/>";
				}

				if($type == "broker") {
					$messagebody = "{{name}},<Br/><Br/>
Thank you for taking the time to attend the Brokers Open House for {{oh_addr1}} on {{today}}. If you have any remaining questions regarding this property or any other property, please feel free to contact me at {{agent_phone}} or by email at {{agent_email}}.<Br/><Br/>
Below you will find more information on the property.<Br/><Br/>
{{oh_img}}<br/><br/>
Bedrooms: {{oh_beds}}<Br/>
Bathrooms: {{oh_baths}}<Br/>
Price: {{oh_price}}<Br/>
Details: {{oh_details}}<Br/>
More Information: {{oh_url}}<Br/><Br/>
If you have a client interested in this property, I'd love to speak with you. Please let me know.<Br/><Br/>
Look forward to hearing from you.<Br/><Br/><Br/>
Sincerely,<Br/><Br/>
{{agent_name}}<BR/>
{{agent_title}}<BR/>
{{agent_company}}<BR/>
{{agent_phone}}<BR/>
{{agent_email}}<br/>";
				}

				$return = array("status" => 1,
					"messagebody" => $messagebody
				);
			} else {

				$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));
				if($type == "customer") {
					$messagebody = $brokerage_result["customerMessageBody"];
				}
				if($type == "agent") {
					$messagebody = $brokerage_result["agentMessageBody"];
				}
				if($type == "broker") {
					$messagebody = $brokerage_result["brokerMessageBody"];
				}


				$return = array("status" => 1,
					"messagebody" => $messagebody
				);

			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function saveEmailTemplate($managerID, $vtoken, $messagebody, $type) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->email_templates;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$email_result = $collection_emails->findOne(array('managerID' => $managerID, 'type' => $type));

			$date = new DateTime();
			$timeStamp = $date->getTimestamp() * 1000;
			$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

			if (!is_null($email_result)) {

				$newdata = array('$set' => array('messagebody' => $messagebody, 'lastModified' => $currentTime));
				$u_result = $collection_emails->updateOne(array('managerID' => $managerID, 'type' => $type), $newdata);

				$return = array("status" => 1);
			} else {

				$customer_template = array(
					"managerID" => $managerID,
					"messagebody" => $messagebody,
					"bccself" => "NO",
					"replyto" => $manager_result["email"],
					"type" => $type,
					"dateCreated" => $currentTime,
					"lastModified" => $currentTime
				);

				$result = $collection_emails->insertOne($customer_template);
				$return = array("status" => 1);

			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}
//
//
// 	static function createDefaultTemplate() {
// 		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
// 		$collection_emails = Db_Conn::getInstance()->getConnection()->custom_emails;
// 		$cursor = $collection_managers->find();
//
// 		foreach ($cursor as $doc) {
// 			$doc_id = $doc["_id"]->{'$id'};
//
// 			$body = 'Hi {{name}},<br/><Br/>Thanks for coming by!<br/><Br/>Here are the details of today\'s open house:<br/><Br/>{{oh_title}}<br/>Address: {{oh_addr}}<br/>Website: {{oh_url}}<br/>Bed: {{oh_beds}}<br/>Bath: {{oh_baths}}<br/>Price: {{oh_price}}<br/>Details: {{oh_details}}<br/><br/>Please let me know if you have any questions, I\'m happy to help.<br/><Br/>Thank you,';
//
// 			if ($doc['brokerage'] == "" || $doc['brokerage'] == "N/A") {
// 				$brokerage = "";
// 			} else {
// 				$brokerage = $doc['brokerage']."<br/>";
// 			}
//
// 			if ($doc['email'] == "" || $doc['email'] == "N/A") {
// 				$email = "";
// 			} else {
// 				$email = $doc['email']."<br/>";
// 			}
//
// 			if ($doc['phone'] == "" || $doc['phone'] == "N/A") {
// 				$phone = "";
// 			} else {
// 				$phone = $doc['phone']."<br/>";
// 			}
//
// 			if ($doc['website'] == "" || $doc['website'] == "N/A") {
// 				$website = "";
// 			} else {
// 				$website = $doc['website']."<br/>";
// 			}
//
// 			$signature = $doc['fname'].' '.$doc['lname'].'<br/>'.$brokerage.''.$email.''.$phone.''.$website;
//
// 			$document_email = array("managerID" => $doc_id,
// 									"body" => $body,
// 									"signature" => $signature);
//
// 			$result = $collection_emails->insert($document_email);
// 		}
//
// 		$return = array("status" => 1);
// 		return $return;
//
// 	}
//
	// NEEDS ATTENTION!!! (e.g. date conversion s to ms)
	static function registerManager($fname, $lname, $email, $phone, $website, $brokerage, $pw, $salt) {

	  $date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);
		$expired = new MongoDB\BSON\UTCDateTime($timeStamp - (86400000)); // 12 hours 43200

		$vtoken = generateRandomString(16);

	  $collection = Db_Conn::getInstance()->getConnection()->managers;
	  $s_result = $collection->findOne(array('email' => $email));

	  if (!is_null($s_result)) {
			//Email already exists

			if($s_result["type"] == "unclaimed") {

				// $_id = $s_result["_id"]->{'$id'};
				$_id = (string)$s_result["_id"];

				$link = "http://spac.io/claim/?e=".$email."&u=".$_id."&nb=1";
				$return = array("status" => 3, "url"=>$link);

			} else {
				$return = array("status" => 2);
			}
	  } else {

		  	$man_fname = $fname;
			$man_lname = $lname;
			$man_key = $man_fname."".$man_lname;
			$man_key = strtolower($man_key);
			$man_key = str_replace(' ', '', $man_key);
			$counter = 1;
			$duplicate_result = $collection->findOne(array('ukey' => $man_key));

			//If there is a duplicate key, iterate counter until result is null;
			while(!is_null($duplicate_result)) {
				$counter++;
				$man_key = $man_fname."".$man_lname."".$counter;
				$man_key = strtolower($man_key);
				$man_key = str_replace(' ', '', $man_key);
				$duplicate_result = $collection->findOne(array('ukey' => $man_key));
			}

		  //$getDate = date('Y-m-d H:i:s');
		  $document_manager = array("fname" => $fname,
									"lname"=> $lname,
									"ukey"=> $man_key,
									"title" => "",
									"teamID" => "N/A",
									"teamRole" => "N/A",
									"pphoto" => "http://spac.io/assets/img/stock/pphoto.jpg",
									"email"=> $email,
									"phone" => $phone,
									"mobile" => "",
									"addr1" => "",
									"addr2" => "",
									"brand" => "",
									"website"=>$website,
									"brokerage"=> $brokerage,
									"brokerageID"=>"N/A",
									"office"=> "N/A",
									"officeID"=>"N/A",
									"type" => "free",
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"topproducerID" => "N/A",
									"marketsnapID" => "N/A",
									"rsID" => "N/A",
									"rsAutoClose" => true,
									"contactuallyID" => "N/A",
									"contactuallyBucket" => "N/A",
									"emailConfirmed" => true,
									"promocode" => "",
									"rtoken" => "",
									"rtDateCreated" => $expired);

		  $result = $collection->insertOne($document_manager);

		  $result = createSampleProperty($document_manager["_id"]->{'$id'});

		  if ($result) {

			$return = array("status" => 1,
							"managerId" => $document_manager["_id"]->{'$id'},
							"vtoken" => $vtoken);
		  } else {
			$return = array("status" => 0);
		  }


      }
	  return $return;
	}


	// NEEDS ATTENTION!!! (e.g. date conversion s to ms)
	static function registerManagerTrial($fname, $lname, $email, $phone, $mobile,$website, $pphoto,$brokerage, $pw, $salt, $promo, $confirmed,$referred) {

	  $date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);
		$expired = new MongoDB\BSON\UTCDateTime($timeStamp - (86400000)); // 12 hours 43200
		$trialEnd = new MongoDB\BSON\UTCDateTime($timeStamp + (2592000000)); // 30 days

		$vtoken = generateRandomString(16);

	  $collection = Db_Conn::getInstance()->getConnection()->managers;
	  $collection_properties = Db_Conn::getInstance()->getConnection()->properties;
	  $collection_promos = Db_Conn::getInstance()->getConnection()->promocodes;
	  $collection_brands = Db_Conn::getInstance()->getConnection()->brands;

	  $s_result = $collection->findOne(array('email' => $email));

	   $promo_result = $collection_promos->findOne(array('code' => $promo));

	   $promo_valid = true;

	   if($promo != "") {
		   if(!is_null($promo_result))  {
				  if(($currentTime->sec) > ($promo_result["expiry"]->sec)) {
					   $promo_valid = false;
				  } else  {
					  $promo_valid = true;
				  }
		   } else {
			   $promo_valid = false;
		   }
	   }

	   $email_domain = explode("@", $email);
	   $domain = $email_domain[1];

	   $brand_result = $collection_brands->findOne(array('domain' => $domain));

		if(!is_null($brand_result))  {
			$brand = $brand_result['name'];
		} else {
			$brand = "N/A";
		}

	  if (!is_null($s_result)) {
			if($s_result["type"] == "unclaimed") {

				// $_id = $s_result["_id"]->{'$id'};
				$_id = (string)$s_result["_id"];

				$link = "http://spac.io/claim/?e=".$email."&u=".$_id."&nb=1";
				$return = array("status" => 3, "url"=>$link);

			} else {
				$return = array("status" => 2);
			}
	  } else if(!$promo_valid) {
		  	$return = array("status" => 3);

		}else {

			$man_fname = $fname;
			$man_lname = $lname;
			$man_key = $man_fname."".$man_lname;
			$man_key = strtolower($man_key);
			$man_key = str_replace(' ', '', $man_key);
			$counter = 1;
			$duplicate_result = $collection->findOne(array('ukey' => $man_key));

			//If there is a duplicate key, iterate counter until result is null;
			while(!is_null($duplicate_result)) {
				$counter++;
				$man_key = $man_fname."".$man_lname."".$counter;
				$man_key = strtolower($man_key);
				$man_key = str_replace(' ', '', $man_key);
				$duplicate_result = $collection->findOne(array('ukey' => $man_key));
			}

		if($pphoto == "") {
			$pphoto = "http://spac.io/assets/img/stock/pphoto.jpg";
		}

		  //$getDate = date('Y-m-d H:i:s');
		  $document_manager = array("fname" => $fname,
									"lname"=> $lname,
									"ukey" => $man_key,
									"title" => "",
									"teamID" => "N/A",
									"teamRole" => "N/A",
									"pphoto" => $pphoto,
									"email"=> $email,
									"phone" => $phone,
									"mobile" => $mobile,
									"addr1" => "",
									"addr2" => "",
									"brand" => $brand,
									"website"=>$website,
									"brokerage"=> $brokerage,
									"brokerageID"=>"N/A",
									"office"=> "N/A",
									"officeID"=>"N/A",
									"type" => "trial",
									"trialEnds" => $trialEnd,
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"topproducerID" => "N/A",
									"marketsnapID" => "N/A",
									"rsID" => "N/A",
									"rsAutoClose" => "YES",
									"ttID" => "N/A",
									"ttAutoClose" => "YES",
									"contactuallyID" => "N/A",
									"contactuallyBucket" => "N/A",
									"emailConfirmed" => $confirmed,
									"promocode" => $promo,
									"rtoken" => "",
									"rtDateCreated" => $expired,
									"referred" => $referred);

			if($referred == "TLCNorthstar") {
				$document_manager["mls"] = "Northstar";
			}



		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result = $collection->insertOne($document_manager);

			// createEmailTemplate($document_manager["_id"]->{'$id'},"N/A");
			createEmailTemplate((string)$document_manager["_id"],"N/A");

		  $collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

			$nextseq = $collection_counter->findOneAndUpdate(
				 array("desc" => "propertyID"),
				 array('$inc' => array('maxCount' => 1))//,
				//  null,
				//  array(
				// 	"new" => true
				// )
			);

		  //$getDate = date('Y-m-d H:i:s');
		  $sample_property = array(
									"propertyID" => $nextseq["maxCount"],
									"url" => "http://www.spac.io",
									"title"=> "SAMPLE: Beautiful Home, Move in Ready",
									"addr1"=> "123 Central Park, New York, NY",
									"addr2"=> "",
									"image"=> "https://s3.amazonaws.com/spacio-user-images/spaciopro_sample.jpg",
									"price" => "2,500,000",
									"beds" => 2,
									"baths" => 2,
									"dimens" => 2000,
									"desc" => "SAMPLE: Beautiful home next to Central Park. Move in ready. Rare opportunity, contact me now for more details.\r\n\r\n*This is a sample open house entry only. Information listed here is not accurate, this is not an offering for sale.",
									//"note" => $note,
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"questions" => array(
										array(
											"question"=>"Are you working with an agent?",
											"type"=>"eq1"
										)
									),
									// "managerID"=>$document_manager["_id"]->{'$id'},
									"managerID"=>(string)$document_manager["_id"],
									"brokerageID"=>"N/A",
									"anon"=>"NO",
									"autoEmail" => "NO",
									"cnEnabled" => "NO",
									"editable" => true,
									"sample" => true);

		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result2 = $collection_properties->insertOne($sample_property);

		  if ($result) {

			  if(!$confirmed) {

				  // $new_managerID = $document_manager["_id"]->{'$id'};
					$new_managerID = (string)$document_manager["_id"];
				  Onboarding_Email::accountConfirmationRequest($fname,$email,$new_managerID,$vtoken);

			  }
			$return = array("status" => 1,
							// "managerID" => $document_manager["_id"]->{'$id'},
							"managerID" => (string)$document_manager["_id"],
							"vtoken" => $vtoken);
		  } else {
			$return = array("status" => 0);
		  }


      }
	  return $return;
	}

	// NEEDS ATTENTION!!! (e.g. date conversion s to ms)
	static function registerManagerRS($fname, $lname, $email, $phone, $mobile, $website, $brokerage, $addr1, $pw, $salt, $pphoto, $title, $rsID, $promocode ) {

	  $date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);
		$expired = new MongoDB\BSON\UTCDateTime($timeStamp - (86400000)); // 12 hours 43200

		$vtoken = generateRandomString(16);

	  $collection = Db_Conn::getInstance()->getConnection()->managers;
	  $collection_properties = Db_Conn::getInstance()->getConnection()->properties;
	  $collection_promos = Db_Conn::getInstance()->getConnection()->promocodes;
	  $collection_brands = Db_Conn::getInstance()->getConnection()->brands;

	  $s_result = $collection->findOne(array('email' => $email));



	   $email_domain = explode("@", $email);
	   $domain = $email_domain[1];

	   $brand_result = $collection_brands->findOne(array('domain' => $domain));

		if(!is_null($brand_result))  {
			$brand = $brand_result['name'];
		} else {
			$brand = "N/A";
		}

	  if (!is_null($s_result)) {
			//Email already exists
			$return = array("status" => 2);
	  } else {

			$man_fname = $fname;
			$man_lname = $lname;
			$man_key = $man_fname."".$man_lname;
			$man_key = strtolower($man_key);
			$counter = 1;
			$duplicate_result = $collection->findOne(array('ukey' => $man_key));

			//If there is a duplicate key, iterate counter until result is null;
			while(!is_null($duplicate_result)) {
				$counter++;
				$man_key = $man_fname."".$man_lname."".$counter;
				$man_key = strtolower($man_key);
				$duplicate_result = $collection->findOne(array('ukey' => $man_key));
			}

		  //$getDate = date('Y-m-d H:i:s');
		  $document_manager = array("fname" => $fname,
									"lname"=> $lname,
									"ukey" => $man_key,
									"title" => $title,
									"teamID" => "N/A",
									"teamRole" => "N/A",
									"pphoto" => $pphoto,
									"email"=> $email,
									"phone" => $phone,
									"mobile" => $mobile,
									"addr1" => $addr1,
									"addr2" => "",
									"brand" => $brand,
									"website"=>$website,
									"brokerage"=> $brokerage,
									"brokerageID"=>"N/A",
									"office"=> "N/A",
									"officeID"=>"N/A",
									"type" => "trial",
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"topproducerID" => "N/A",
									"marketsnapID" => "N/A",
									"rsID" => $rsID,
									"rsAutoClose" => true,
									"contactuallyID" => "N/A",
									"contactuallyBucket" => "N/A",
									"emailConfirmed" => false,
									"promocode" => $promocode,
									"rtoken" => "",
									"rtDateCreated" => $expired);

		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result = $collection->insertOne($document_manager);

		  $collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

			$nextseq = $collection_counter->findOneAndUpdate(
				 array("desc" => "propertyID"),
				 array('$inc' => array('maxCount' => 1))//,
				//  null,
				//  array(
				// 	"new" => true
				// )
			);

		  //$getDate = date('Y-m-d H:i:s');
		  $sample_property = array(
									"propertyID" => $nextseq["maxCount"],
									"url" => "http://www.spac.io",
									"title"=> "SAMPLE: Beautiful Home, Move in Ready",
									"addr1"=> "123 Central Park, New York, NY",
									"addr2"=> "",
									"image"=> "https://s3.amazonaws.com/spacio-user-images/spaciopro_sample.jpg",
									"price" => "2,500,000",
									"beds" => 2,
									"baths" => 2,
									"dimens" => 2000,
									"desc" => "SAMPLE: Beautiful home next to Central Park. Move in ready. Rare opportunity, contact me now for more details.\r\n\r\n*This is a sample open house entry only. Information listed here is not accurate, this is not an offering for sale.",
									//"note" => $note,
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"questions" => array(
										array(
											"question"=>"Are you working with an agent?",
											"type"=>"eq1"
										)
									),
									// "managerID"=>$document_manager["_id"]->{'$id'},
									"managerID"=>(string)$document_manager["_id"],
									"brokerageID"=>"N/A",
									"anon"=>"NO",
									"autoEmail" => "NO",
									"cnEnabled" => "NO",
									"editable" => true,
									"sammple"=> true);

		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result2 = $collection_properties->insertOne($sample_property);

		  if ($result) {

			  try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;line-height:1.4;">
				<p>'.$fname.',</p>
				<p><b>Welcome to Spacio + RealSatisfied’s Special Collaboration!</b></p> <p>Please confirm your email by clicking <a href="http://www.spac.io/confirmemail.php?e='.$document_manager["_id"]->{'$id'}.'&v='.$vtoken.'">HERE</a></p><p>Download the Spacio iPad app <a href="https://itunes.apple.com/us/app/spacio-pro-best-open-house/id1017394547?ls=1&mt=8">HERE</a>. </p><p><b>Make Your First Impression Even Better with RealSatisfied</b><br/>We know the road to building a superior customer experience begins the second your prospects meet you - and your Open House could be that moment. Spacio now lets you integrate your RealSatisfied ratings and testimonials directly into the open house sign-in form so you can capture and convert your visitors by sharing your successes with past clients. To see how this works, visit this link: <a href="http://spac.io/realsatisfied/">http://spac.io/realsatisfied/</a></p><p><b>Enjoy a 2 Week Free Trial of Spacio PRO - Upgrade before Jan 31, 2016 and receive 50% off until you cancel. No contract!</b><br/>- Digitally capture leads at open houses with our sign-in form and automatically send property details to buyers.<br/>- Manage, cultivate, and convert more leads. Access your lists of buyers anytime to retarget other listings.<br/>- Stay connected by broadcasting messages to buyers with updates and other similar properties you list.
</p><p><b>Getting started:</b><br/>1. Login to your account at www.spac.io or on the Spacio app and complete your profile in My Account.<br/>2. Go to the My Open Houses, click Add Open House and input the details of your open house. Customize your form by clicking Edit Sign-In form. Click Launch Sign-in Form and you’re all set!<br/>3. After your open house, go to the same menu and click on View Registrants to view, edit, and export your list of potential buyers. To send everyone who signed in an update about your listing, go to Create Broadcast Message.</p><p>No more pen and paper, no more ugly handwriting, no more missed opportunities.</p><p>Good luck!</p><br/><p>The Spacio Team<br/>hello@spac.io</p>
			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				No longer want to receive these messages?<br/>
				You may unsubscribe <a href="http://www.spac.io/unsubscribeme.php" style="color:#999;">here</a>.<br/>
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => 'Welcome to Spacio + RealSatisfied',
						'from_email' => 'hello@spac.io',
						'from_name' => 'Spacio',
						'to' => array(
							array(
								'email' => $email
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('new-registrant')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}

			$return = array("status" => 1,
							"managerId" => $document_manager["_id"]->{'$id'},
							"vtoken" => $vtoken);
		  } else {
			$return = array("status" => 0);
		  }


      }
	  return $return;
	}

	static function upgradeAccount($_email, $_promo, $_stripetoken, $_plan, $_taxRate, $_taxType, $_country, $_province) {
		\Stripe\Stripe::setApiKey(STRIPE_SKEY);
		$date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

		$collection_subscriptions = Db_Conn::getInstance()->getConnection()->subscriptions;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
	  $s_result = $collection_managers->findOne(array('email' => $_email));

		if (!is_null($s_result)) {

			if($s_result["referred"] == "CRMLS") {
				$_promo = "crmls";
			}

			$s_result2 = $collection_subscriptions->findOne(array('managerID' => (string)$s_result["_id"]));

			if ($s_result["type"] != "trial" && $s_result["type"] != "freemium" && $s_result["type"] != "expired") {
				$return = array("status" => 3);
			} else {
				if(!is_null($s_result2)) {

					$cus = $s_result2["cus"];

					if($_plan == "pro-std-new") {
						if ($_promo == "") {
							$sub = \Stripe\Subscription::create(array(
								"customer" => $cus,
								 "plan" => "pro-std-new",
								 "tax_percent" => $_taxRate
							));
						} else {
							$sub = \Stripe\Subscription::create(array(
								"customer" => $cus,
								 "plan" => "pro-std-new",
								 "tax_percent" => $_taxRate,
								 "coupon" => $_promo
							));
						}
					} else if($_plan == "annual-std-new") {
						if ($_promo == "") {
							$sub = \Stripe\Subscription::create(array(
								"customer" => $cus,
								 "plan" => "annual-std-new",
								 "tax_percent" => $_taxRate
							));
						} else {
							$sub = \Stripe\Subscription::create(array(
								"customer" => $cus,
								 "plan" => "annual-std-new",
								 "tax_percent" => $_taxRate,
								 "coupon" => $_promo
							));
						}
					} else if($_plan == "pro-std") {
						if ($_promo == "") {
							$sub = \Stripe\Subscription::create(array(
								"customer" => $cus,
								 "plan" => "pro-std",
								 "tax_percent" => $_taxRate
							));
						} else {
							$sub = \Stripe\Subscription::create(array(
								"customer" => $cus,
								 "plan" => "pro-std",
								 "tax_percent" => $_taxRate,
								 "coupon" => $_promo
							));
						}
					}

					$newdata_subs = array('$set' => array(
						"sub" => $sub["id"],
						"plan" => $_plan,
						"status" => "active"));

					$u_result_subs = $collection_subscriptions->updateOne(array('_id' => new MongoDB\BSON\ObjectID((string)$s_result2["_id"])), $newdata_subs);

					$return = array("status" => 1, "subID" => (string)$s_result2["_id"]);

				} else {
					//Insert the subscription first as pending (This is to resolve the issue with invoices being sent out before sub is created)


					$document_subscription = array(
						// "managerID" => $s_result["_id"]->{'$id'},
						"managerID" => (string)$s_result["_id"],
						"plan" => $_plan,
						"cus" => "pending",
						"sub" => "pending",
						"cc_type" => "pending",
						"last4" => "pending",
						"expiry" => $currentTime,
						"status" => "pending"
					);

					$subs_result = $collection_subscriptions->insertOne($document_subscription);

					try {
					  // Use Stripe's bindings...
						$token = $_stripetoken;
						if($_plan == "pro-std-new") {
							if ($_promo == "") {
								$customer = \Stripe\Customer::create(array(
								  "source" => $token,
								  "plan" => "pro-std-new",
									"tax_percent" => $_taxRate,
									"metadata" => array("country" => $_country, "province" => $_province),
								  "email" => $_email)
								);
							} else {
								$customer = \Stripe\Customer::create(array(
								  "source" => $token,
								  "plan" => "pro-std-new",
									"tax_percent" => $_taxRate,
									"metadata" => array("country" => $_country, "province" => $_province),
									"coupon" => $_promo,
								  "email" => $_email)
								);
							}
						} else if($_plan == "annual-std-new") {
							if ($_promo == "") {
								$customer = \Stripe\Customer::create(array(
								  "source" => $token,
								  "plan" => "annual-std-new",
									"tax_percent" => $_taxRate,
									"metadata" => array("country" => $_country, "province" => $_province),
								  "email" => $_email)
								);
							} else {
								$customer = \Stripe\Customer::create(array(
								  "source" => $token,
								  "plan" => "annual-std-new",
									"tax_percent" => $_taxRate,
									"metadata" => array("country" => $_country, "province" => $_province),
									"coupon" => $_promo,
								  "email" => $_email)
								);
							}
						} else if($_plan == "pro-std") {
							if ($_promo == "") {
								$customer = \Stripe\Customer::create(array(
								  "source" => $token,
								  "plan" => "pro-std",
									"tax_percent" => $_taxRate,
									"metadata" => array("country" => $_country, "province" => $_province),
								  "email" => $_email)
								);
							} else {
								$customer = \Stripe\Customer::create(array(
								  "source" => $token,
								  "plan" => "pro-std",
									"tax_percent" => $_taxRate,
									"coupon" => $_promo,
									"metadata" => array("country" => $_country, "province" => $_province),
								  "email" => $_email)
								);
							}
						}

					} catch(\Stripe\Error\Card $e) {
					  // Since it's a decline, \Stripe\Error\Card will be caught
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;

					} catch (\Stripe\Error\InvalidRequest $e) {
					  // Invalid parameters were supplied to Stripe's API
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;
					} catch (\Stripe\Error\Authentication $e) {
					  // Authentication with Stripe's API failed
					  // (maybe you changed API keys recently)
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $return = array("status" => 0,
										"error_type" =>  "Authentication Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\ApiConnection $e) {
					  // Network communication with Stripe failed
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					 $return = array("status" => 0,
										"error_type" =>  "Network Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\Base $e) {
					  // Display a very generic error to the user, and maybe send
					  // yourself an email
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $return = array("status" => 0,
										"error_type" =>  "Base Error",
										"error_param" =>  "");
						return $return;
					}


					$sub = $customer["subscriptions"]["data"][0]["id"];
					$cc_type = $customer["sources"]["data"][0]["brand"];
					$last4 = $customer["sources"]["data"][0]["last4"];

					$newdata_subs = array('$set' => array("cus" => $customer["id"],
							"sub" => $sub,
							"cc_type" => $cc_type,
							"last4" => $last4,
							"expiry" => $currentTime,
							"status" => "active"));

					$u_result_subs = $collection_subscriptions->updateOne(array('_id' => $document_subscription["_id"]), $newdata_subs);

					$newdata = array('$set' => array('type' => 'standard','cus' => $customer["id"]));
					$u_result = $collection_managers->updateOne(array('_id' => $s_result["_id"]), $newdata);

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html>
		<body>
			<div style="background:#f7f7f7;padding:20px;">
				<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
				<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">

					<p>Hello '.$s_result["fname"].',</p>

<p>Thanks for upgrading your Spacio account! We really appreciate it and hope you enjoy Spacio’s extended functionalities. </p>

Here’s what you can do with Spacio:<br/>
●	Export leads into your CRM or other marketing software for newsletters and events.<br/>
●	Customize your auto email message and signature.<br/>
●	Integrate with your favorite services like Contactually, Top Producer, Follow Up Boss, and lots more!<br/>
●	Stay connected by broadcasting messages to buyers with updates and other similar properties you list.<br/>
●	Impress your clients by generating beautiful seller reports at the click of a button.<br/><br/>

<p>To see this upgrade reflected in your account, you may have to log out of your account on both the website and iPad app and login again.</p>

<p>Spacio is here to help you automate your open houses. Let us know if there’s anything we can do to make Spacio better for you. We look forward to hearing from you.</p><br/>


The Spacio Team<br/>
hello@spac.io
				</div>
				<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
					Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
				</div>
			</div>
		</body>
	</html>',
							'subject' => 'Your account has been upgraded!',
							'from_email' => 'hello@spac.io',
							'from_name' => 'Spacio',
							'to' => array(
								array(
									'email' => $_email
								)
							),
							'headers' => array('Reply-To' => 'hello@spac.io'),
							'important' => false,
							'tags' => array('upgraded-account')

						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html>
		<body>
			<div style="background:#f7f7f7;padding:20px;">
				<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
				<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
					<p>Rejoice, we just got paid.<br/>Email:'.$_email.'<br/>
					Name:'.$s_result["fname"].' '.$s_result["lname"].'
					</p>
				</div>

			</div>
		</body>
	</html>',
							'subject' => '[SPACIO] Someone upgraded!',
							'from_email' => 'hello@spac.io',
							'from_name' => 'Spacio',
							'to' => array(
								array(
									'email' => "hello@spac.io"
								)
							),
							'headers' => array('Reply-To' => 'hello@spac.io'),
							'important' => false,
							'tags' => array('notice-upgraded-account')

						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}


					$return = array("status" => 1);
				}
			}
	  	} else {
			//Email does not exist
			$return = array("status" => 2);
		}

		return $return;
	}

	static function upgradeAccountStage($_email, $_promo, $_stripetoken) {
		\Stripe\Stripe::setApiKey("sk_test_0cz4hTHr7OPzNjnMaSgCUe0u");
		$date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

		$collection_subscriptions = Db_Conn::getInstance()->getConnection()->subscriptions;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
	  	$s_result = $collection_managers->findOne(array('email' => $_email));

		if (!is_null($s_result)) {
			if ($s_result["type"] != "trial" && $s_result["type"] != "freemium") {
				$return = array("status" => 3);
			} else {
				//Insert the subscription first as pending (This is to resolve the issue with invoices being sent out before sub is created)
				$document_subscription = array(
					// "managerID" => $s_result["_id"]->{'$id'},
					"managerID" => (string)$s_result["_id"],
					"plan" => "standard",
					"cus" => "pending",
					"sub" => "pending",
					"cc_type" => "pending",
					"last4" => "pending",
					"expiry" => $currentTime,
					"status" => "pending"
				);

				$subs_result = $collection_subscriptions->insertOne($document_subscription);

				try {
				  // Use Stripe's bindings...
				 	 $token = $_stripetoken;
					if ($_promo == "") {
						$customer = \Stripe\Customer::create(array(
						  "source" => $token,
						  "plan" => "pro-std",
						  "email" => $_email)
						);
					} else {
						$customer = \Stripe\Customer::create(array(
						  "source" => $token,
						  "plan" => "pro-std",
						  "coupon" => $_promo,
						  "email" => $_email)
						);
					}
				} catch(\Stripe\Error\Card $e) {
				  // Since it's a decline, \Stripe\Error\Card will be caught
				  $body = $e->getJsonBody();
				  $err  = $body['error'];

					$return = array("status" => 0,
									"error_type" =>  $err['type'],
									"error_code" =>  $err['code'],
									"error_param" =>  $err['param'],
									"error_msg" =>  $err['message']);
					return $return;

				} catch (\Stripe\Error\InvalidRequest $e) {
				  // Invalid parameters were supplied to Stripe's API
				  $body = $e->getJsonBody();
				  $err  = $body['error'];

					$return = array("status" => 0,
									"error_type" =>  $err['type'],
									"error_code" =>  $err['code'],
									"error_param" =>  $err['param'],
									"error_msg" =>  $err['message']);
					return $return;
				} catch (\Stripe\Error\Authentication $e) {
				  // Authentication with Stripe's API failed
				  // (maybe you changed API keys recently)
				  $return = array("status" => 0,
									"error_type" =>  "Authentication Failure",
									"error_param" =>  "");
					return $return;
				} catch (\Stripe\Error\ApiConnection $e) {
				  // Network communication with Stripe failed
				 $return = array("status" => 0,
									"error_type" =>  "Network Failure",
									"error_param" =>  "");
					return $return;
				} catch (\Stripe\Error\Base $e) {
				  // Display a very generic error to the user, and maybe send
				  // yourself an email
				  $return = array("status" => 0,
									"error_type" =>  "Base Error",
									"error_param" =>  "");
					return $return;
				} catch (Exception $e) {
					$return = array("status" => 0,
									"error_type" => "Unkown",
									"error_param" =>  "");
					return $return;
				  // Something else happened, completely unrelated to Stripe
				}


				$sub = $customer["subscriptions"]["data"][0]["id"];
				$cc_type = $customer["sources"]["data"][0]["brand"];
				$last4 = $customer["sources"]["data"][0]["last4"];

				$newdata_subs = array('$set' => array("cus" => $customer["id"],
						"sub" => $sub,
						"cc_type" => $cc_type,
						"last4" => $last4,
						"expiry" => $currentTime,
						"status" => "active"));

				$u_result_subs = $collection_subscriptions->updateOne(array('_id' => $document_subscription["_id"]), $newdata_subs);

				$newdata = array('$set' => array('type' => 'standard'));
				$u_result = $collection_managers->updateOne(array('_id' => $s_result["_id"]), $newdata);

				try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>'.$s_result["fname"].',</p>
				<p>You’ve just upgraded your Spacio to a PRO account. Thank you so much for your support, we really appreciate it!</p><p>To see this upgrade reflected in your account, you may have to logout of your account on the web and app and login again.</p><p><b>Please enjoy extended features in Spacio PRO such as:</b><br/>- Export leads to import into your CRM or other marketing software for newsletters and events.<br/>- Customize your auto email message and signature.<br/>- Integrate with CRMs like Contactually and Top Producer. Don’t see your favorite CRM? Let us know by replying to this email.<br/>- Stay connected by broadcasting messages to buyers with updates and other similar properties you list.</p><p>Please do share any feedback you have on how we can make Spacio better to help you run more effective open houses! We look forward to hearing from you.</p><br/><p>The Spacio Team<br/>hello@spac.io</p>
			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => 'Spacio Upgraded to PRO',
						'from_email' => 'hello@spac.io',
						'from_name' => 'Spacio',
						'to' => array(
							array(
								'email' => $_email
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('upgraded-account')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}

				try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Rejoice, we jut got paid.<br/>Email:'.$s_result["email"].'<br/>Name:'.$s_result["fname"].' '.$s_result["lname"].'</p>
			</div>

		</div>
    </body>
</html>',
						'subject' => 'Someone upgraded to PRO',
						'from_email' => 'hello@spac.io',
						'from_name' => 'Spacio',
						'to' => array(
							array(
								'email' => "ting@spac.io"
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('notice-upgraded-account')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}


				$return = array("status" => 1);
			}
	  	} else {
			//Email does not exist
			$return = array("status" => 2);
		}

		return $return;
	}

	static function upgradeAccountByID($managerID, $vtoken, $stripetoken) {
		\Stripe\Stripe::setApiKey(STRIPE_SKEY);
		$date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

		$collection_subscriptions = Db_Conn::getInstance()->getConnection()->subscriptions;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));


		if (!is_null($manager_result)) {
			$subscription_result = $collection_subscriptions->findOne(array('managerID' => $managerID));

			if ($manager_result["type"] != "trial" && $manager_result["type"] != "freemium") {
				$return = array("status" => 3);
			} else {

				if(!is_null($subscription_result)) {

					$cus = $subscription_result["cus"];

					try {

						// Use Stripe's bindings...
						$token = $stripetoken;

						$customer = \Stripe\Customer::retrieve($cus);
						$customer->source = $token; // obtained with Stripe.js
						$customer->save();


					} catch(\Stripe\Error\Card $e) {
					  // Since it's a decline, \Stripe\Error\Card will be caught
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;

					} catch (\Stripe\Error\InvalidRequest $e) {
					  // Invalid parameters were supplied to Stripe's API
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"cus_status" => "existing",
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;
					} catch (\Stripe\Error\Authentication $e) {
					  // Authentication with Stripe's API failed
					  // (maybe you changed API keys recently)
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $return = array("status" => 0,
										"error_type" =>  "Authentication Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\ApiConnection $e) {
					  // Network communication with Stripe failed
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					 $return = array("status" => 0,
										"error_type" =>  "Network Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\Base $e) {
					  // Display a very generic error to the user, and maybe send
					  // yourself an email
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $return = array("status" => 0,
										"error_type" =>  "Base Error",
										"error_param" =>  "");
						return $return;
					}

					$cc_type = $customer["sources"]["data"][0]["brand"];
					$last4 = $customer["sources"]["data"][0]["last4"];


					$sub = \Stripe\Subscription::create(array(
  						"customer" => $cus,
 						"plan" => "pro-std"
					));

					$newdata = array('$set' => array('type' => 'standard'));

					$newdata_subs = array('$set' => array(
						"sub" => $sub["id"],
						"cc_type" => $cc_type,
						"last4" => $last4,
						"expiry" => $currentTime,
						"status" => "active"));

					$u_result = $collection_managers->updateOne(array('_id' => $manager_result["_id"]), $newdata);
					$u_result_accounts = $collection_subscriptions->updateOne(array('managerID' => $managerID), $newdata_subs);

					$return = array("status" => 1, "customer" => $customer, "sub" => $sub);
				} else {
					//Insert the subscription first as pending (This is to resolve the issue with invoices being sent out before sub is created)
					$document_subscription = array(
						"managerID" => $managerID,
						"plan" => "standard",
						"cus" => "pending",
						"sub" => "pending",
						"cc_type" => "pending",
						"last4" => "pending",
						"expiry" => $currentTime,
						"status" => "pending"
					);

					$subs_result = $collection_subscriptions->insertOne($document_subscription);

					try {
					  // Use Stripe's bindings...
						 $token = $stripetoken;
							$customer = \Stripe\Customer::create(array(
							  "source" => $token,
							  "plan" => "pro-std",
							  "email" => $manager_result["email"])
							);

					} catch(\Stripe\Error\Card $e) {
					  // Since it's a decline, \Stripe\Error\Card will be caught
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"cus_status" => "new",
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;

					} catch (\Stripe\Error\InvalidRequest $e) {
					  // Invalid parameters were supplied to Stripe's API
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"cus_status" => "new",
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;
					} catch (\Stripe\Error\Authentication $e) {
					  // Authentication with Stripe's API failed
					  // (maybe you changed API keys recently)
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $return = array("status" => 0,
										"error_type" =>  "Authentication Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\ApiConnection $e) {
					  // Network communication with Stripe failed
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					 $return = array("status" => 0,
										"error_type" =>  "Network Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\Base $e) {
					  // Display a very generic error to the user, and maybe send
					  // yourself an email
					  $collection_subscriptions->deleteOne(array('_id' => $document_subscription["_id"]));
					  $return = array("status" => 0,
										"error_type" =>  "Base Error",
										"error_param" =>  "");
						return $return;
					}


					$sub = $customer["subscriptions"]["data"][0]["id"];
					$cc_type = $customer["sources"]["data"][0]["brand"];
					$last4 = $customer["sources"]["data"][0]["last4"];

					$newdata_subs = array('$set' => array("cus" => $customer["id"],
							"sub" => $sub,
							"cc_type" => $cc_type,
							"last4" => $last4,
							"expiry" => $currentTime,
							"status" => "active"));

					$u_result_subs = $collection_subscriptions->updateOne(array('_id' => $document_subscription["_id"]), $newdata_subs);

					$newdata = array('$set' => array('type' => 'standard'));
					$u_result = $collection_managers->updateOne(array('_id' => $manager_result["_id"]), $newdata);

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html>
		<body>
			<div style="background:#f7f7f7;padding:20px;">
				<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
				<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
					<p>'.$manager_result["fname"].',</p>
					<p>You’ve just upgraded your Spacio to a PRO account. Thank you so much for your support, we really appreciate it!</p><p>To see this upgrade reflected in your account, you may have to logout of your account on the web and app and login again.</p><p><b>Please enjoy extended features in Spacio PRO such as:</b><br/>- Export leads to import into your CRM or other marketing software for newsletters and events.<br/>- Customize your auto email message and signature.<br/>- Integrate with CRMs like Contactually and Top Producer. Don’t see your favorite CRM? Let us know by replying to this email.<br/>- Stay connected by broadcasting messages to buyers with updates and other similar properties you list.</p><p>Please do share any feedback you have on how we can make Spacio better to help you run more effective open houses! We look forward to hearing from you.</p><br/><p>The Spacio Team<br/>hello@spac.io</p>
				</div>
				<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
					Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
				</div>
			</div>
		</body>
	</html>',
							'subject' => 'Spacio Upgraded to PRO',
							'from_email' => 'hello@spac.io',
							'from_name' => 'Spacio',
							'to' => array(
								array(
									'email' => $manager_result["email"]
								)
							),
							'headers' => array('Reply-To' => 'hello@spac.io'),
							'important' => false,
							'tags' => array('upgraded-account')

						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html>
		<body>
			<div style="background:#f7f7f7;padding:20px;">
				<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
				<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
					<p>Rejoice, we just got paid. (I changed the spelling, are you proud?)<br/>Email:'.$s_result["email"].'<br/>Name:'.$s_result["fname"].' '.$s_result["lname"].'</p>
				</div>

			</div>
		</body>
	</html>',
							'subject' => 'Someone upgraded to PRO',
							'from_email' => 'hello@spac.io',
							'from_name' => 'Spacio',
							'to' => array(
								array(
									'email' => "melissa@spac.io"
								)
							),
							'headers' => array('Reply-To' => 'hello@spac.io'),
							'important' => false,
							'tags' => array('notice-upgraded-account')

						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}


					$return = array("status" => 1);
				}
			}
	  	} else {
			//Email does not exist
			$return = array("status" => 2);
		}

		return $return;
	}

	static function downgradeAccount($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_subscriptions = Db_Conn::getInstance()->getConnection()->subscriptions;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));
		if (!is_null($manager_result)) {

			if ($manager_result["type"] == "standard") {

				$subscription_result = $collection_subscriptions->findOne(array('managerID' => $managerID));
				$cus = $subscription_result["cus"];
				$sub = $subscription_result["sub"];

				\Stripe\Stripe::setApiKey("sk_live_yuTam6NeKqfH6NY5qLBrqCMg");

				$customer = \Stripe\Customer::retrieve($cus);
				$subscription = $customer->subscriptions->retrieve($sub);
				$subscription->cancel(array('at_period_end' => true));

				$newdata = array('$set' => array('status' => 'cancelled'));
				$u_result = $collection_subscriptions->updateOne(array('managerID' => $managerID), $newdata);

				 try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Hello '.$manager_result["fname"].',</p>
				<p>You\'ve just downgraded your Spacio Pro account to Spacio Free. We are sorry to see you go, but thanks for giving us a try.</p>
				<p>We hope you will continue to enjoy using Spacio Free. Your payment account will not be charged again.</p>
				<p>Is there a way that we can improve? We\'re always looking for ways to make Spacio better, and we\'d love to know why you cancelled. You can provide us with feedback by taking our brief, <a href=\'https://www.surveymonkey.com/r/9D2VYNB\'>anonymous cancellation survey</a> or by replying to this email.</p>
				<p>If you did not downgrade your account and received this email in error, please contact: hello@spac.io</p><br/>
				<p>The Spacio Team<br/>hello@spac.io</p>
			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => 'Spacio Pro Downgraded to Spacio Free',
						'from_email' => 'hello@spac.io',
						'from_name' => 'Spacio',
						'to' => array(
							array(
								'email' => $email
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('user-downgraded')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}

				try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Sadness, someone downgraded.<br/>Email:'.$manager_result["email"].'<br/>Name:'.$manager_result["fname"].' '.$manager_result["lname"].'</p>
			</div>

		</div>
    </body>
</html>',
						'subject' => 'Someone downgraded',
						'from_email' => 'hello@spac.io',
						'from_name' => 'Spacio',
						'to' => array(
							array(
								'email' => "melissa@spac.io"
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('notice-downgraded-account')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}

				$return = array("status" => 1,
								"expiry" => $subscription_result["expiry"]
				);

			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getBrand($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
	  	$collection_brands = Db_Conn::getInstance()->getConnection()->brands;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			if ($manager_result["brand"] != "N/A" && $manager_result["brand"] != "") {
				$brand_result = $collection_brands->findOne(array('name' => $manager_result["brand"]));

				if (!is_null($brand_result)) {
					$return = array("status" => 1,
								"leftpanel" => $brand_result["leftpanel"],
								"imgs" => $brand_result["imgs"],
								"font" => $brand_result["font"],
								"color" => $brand_result["color"] );
				} else {
					$return = array("status" => 2);
				}

			} else {
				$return = array("status" => 2);
			}
		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyCorpCode($managerID, $vtoken, $code) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
	  	$collection_brands = Db_Conn::getInstance()->getConnection()->brands;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if($code == "" || $code == "N/A") {
				$newdata = array('$set' => array('brand' => "N/A"));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
			} else {
				$brand_result = $collection_brands->findOne(array('code' => $code));

				if (!is_null($brand_result)) {
					$brand = $brand_result['name'];
					$newdata = array('$set' => array('brand' => $brand));
					$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);


					$return = array("status" => 1);
				} else {
					$return = array("status" => 2);
				}
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyPromoCode($managerID, $vtoken, $promo) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_promos = Db_Conn::getInstance()->getConnection()->promocodes;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		$promo_result = $collection_promos->findOne(array('code' => $promo));

	   	$promo_valid = true;

	   if($promo != "") {
		   if(!is_null($promo_result))  {
				  if(($currentTime->sec) > ($promo_result["expiry"]->sec)) {
					   $promo_valid = false;
				  } else  {
					  $promo_valid = true;
				  }
		   } else {
			   $promo_valid = false;
		   }
	   }

		if (!is_null($manager_result)) {
			if($promo_valid) {
				$newdata = array('$set' => array('promocode' => $promo));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function editOLR($managerID, $vtoken, $olrEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('olrEnabled' => $olrEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editMoxi($managerID, $vtoken, $moxiEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('moxiEnabled' => $moxiEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editMoxiPresent($managerID, $vtoken, $moxipresentEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('moxipresentEnabled' => $moxipresentEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editTouchCMA($managerID, $vtoken, $touchcmaEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('touchcmaEnabled' => $touchcmaEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editPresentationPro($managerID, $vtoken, $presentationproEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('presentationproEnabled' => $presentationproEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editREW($managerID, $vtoken, $rewEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('rewEnabled' => $rewEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editTribus($managerID, $vtoken, $tribusEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('tribusEnabled' => $tribusEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}
//
	static function editDocusign($managerID, $vtoken, $docusignEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if($manager_result["brokerageID"] == "bhhsga" || $manager_result["email"] == "melissa@spac.io") {
				$newdata = array('$set' => array('docusignEnabled' => $docusignEnabled));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);

			} else {
				$return = array("status" => 2);
			}

		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editDocusignAuto($managerID, $vtoken, $docusignAutoEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if(($manager_result["docusignEnabled"] == "YES" && $docusignAutoEnabled == "YES") || $docusignAutoEnabled == "NO") {
				$newdata = array('$set' => array('docusignAutoEnabled' => $docusignAutoEnabled));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
			} else {
				$return = array("status" => 2);
			}

		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editReliance($managerID, $vtoken, $relianceEnabled) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('relianceEnabled' => $relianceEnabled));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;
	}

	function editSendToRepresented($managerID,$vtoken,$integration,$sendToRepresented) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if(array_key_exists("sendToRepresented", $manager_result)) {
				$sendToRepresentedArray = $manager_result["sendToRepresented"];
			} else {
				$sendToRepresentedArray = array();
			}

			$sendToRepresentedArray["".$integration.""] = $sendToRepresented;

			$newdata = array('$set' => array('sendToRepresented' => $sendToRepresentedArray));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		}  else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyRenthopID($managerID, $vtoken, $renthopID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_renthop = Db_Conn::getInstance()->getConnection()->renthop_listings;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($renthopID == "") {
			$renthopID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('renthopID' => $renthopID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				if($renthopID != "N/A") {
					$url="http://www.renthop.com/api/spaciolistings/".$renthopID;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_URL, $url);
					$result = curl_exec($ch);
					curl_close($ch);

					//preg_match_all('/\{([^}]+)\}/', $result, $matches);
					$matches = explode(PHP_EOL, $result);
					$count = 0;
					foreach ($matches as $listing) {
						$obj = json_decode($listing, true);

						$property_result = $collection_properties->findOne(array('managerID' => $managerID, "renthopListingID" => $obj["renthop_listing_id"]));

						if(is_null($property_result)) {
							$count++;
							$date = new DateTime();
							$timeStamp = $date->getTimestamp() * 1000;
							$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

							$nextseq = $collection_counter->findOneAndUpdate(
								 array("desc" => "propertyID"),
								 array('$inc' => array('maxCount' => 1))//,
								//  null,
								//  array(
								// 	"new" => true
								// )
							);

							if (isset($obj["photos"][0])) {
								$image = $obj["photos"][0];
							} else {
								$stock_num = rand(1, 10);
								$image = "http://www.spac.io/assets/img/stock/".$stock_num.".jpg";
							}

							$document_property = array(
										"propertyID" => $nextseq["maxCount"],
										"renthopListingID" => $obj["renthop_listing_id"],
										"teamID" => "N/A",
										"url" => $obj["website"],
										"title"=> $obj["street_address"],
										"addr1"=> $obj["street_address"],
										"addr2"=> $obj["citystate"],
										"image"=> $image,
										"price" => $obj["price"],
										"beds" => (float)$obj["bedrooms"],
										"baths" => (float)$obj["bathrooms"],
										"dimens" => (float)$obj["square_feet"],
										"desc" => "",
										"currency" => "USD",
										"measurement" => "sqft",
										"dateCreated" => $currentTime,
										"lastEdited" => $currentTime,
										"questions" => array(),
										"managerID"=>$manager_result["_id"]->{'$id'},
										"brokerageID"=>$manager_result["brokerageID"],
										"anon"=>"NO",
										"autoEmail" => "NO",
										"cnEnabled" => "NO",
										"editable"=>true,
										"sample" => false);

							$result = $collection_properties->insertOne($document_property);
							$count++;
						}
					}
				}

				$return = array("status" => 1, "count" => $count);


		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function applyMLS($managerID, $vtoken, $mls,$mlsID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($mls == "") {
			$mls = "N/A";
		}

		if (!is_null($manager_result)) {
				$authorized = true;
				if($mls == "crmls") {
					$authorized = verifyCRMLSMember($mlsID, $managerID);
				}

				if($authorized) {
					$newdata = array('$set' => array('mls' => $mls, "mlsID" => $mlsID));
					$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

					$return = array("status" => 1);
				} else {
					$return = array("status" => 2);
				}
		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyMailchimpID($managerID, $vtoken, $mailchimp_key, $mailchimp_username) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($mailchimp_key == "") {
			$mailchimp_key = "N/A";
			$mailchimp_username = "N/A";
		}

		if($mailchimp_username == "") {
			$mailchimp_key = "N/A";
			$mailchimp_username = "N/A";
		}

		if (!is_null($manager_result)) {


				$agent_name = $manager_result["fname"]." ".$manager_result["lname"];

				if($mailchimp_username != "N/A" && $mailchimp_key != "N/A") {
					$mailchimpConnectResult = mailchimpConnect($mailchimp_key,$mailchimp_username,$agent_name);
					$newdata = array('$set' => array('mailchimpKey' => $mailchimp_key, 'mailchimpUsername' => $mailchimp_username, 'mailchimpListID' => $mailchimpConnectResult["newListID"]));
				} else {
					$newdata = array('$set' => array('mailchimpKey' => $mailchimp_key, 'mailchimpUsername' => $mailchimp_username, 'mailchimpListID' => "N/A"));
				}


				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);


				$return = array("status" => 1, "msg" => $mailchimpConnectResult["msg"] );


		} else {
			$return = array("status" => 0);
		}

		return $return;

	}



	static function applyCloudCMAID($managerID, $vtoken, $cloudcmaID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
				$newdata = array('$set' => array('cloudcmaID' => $cloudcmaID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function applyTTID($managerID, $vtoken, $ttID, $ttAutoClose) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($ttID == "") {
			$ttID = "N/A";
		}

		if (!is_null($manager_result)) {
				$newdata = array('$set' => array('ttID' => $ttID, 'ttAutoClose' => $ttAutoClose));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function applyRealScoutID($managerID, $vtoken, $realscoutKey, $realscoutEmail) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$newdata = array('$set' => array('realscoutKey' => $realscoutKey, 'realscoutEmail' => $realscoutEmail));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function applyRSID($managerID, $vtoken, $rsID, $rsAutoClose) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($rsID == "") {
			$rsID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('rsID' => $rsID, 'rsAutoClose' => $rsAutoClose));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);


		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyMSID($managerID, $vtoken, $marketsnapID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($marketsnapID == "") {
			$marketsnapID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('marketsnapID' => $marketsnapID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);


		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyBTID($managerID, $vtoken, $boomtownID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($boomtownID == "") {
			$boomtownID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('boomtownID' => $boomtownID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);


		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyTPID($managerID, $vtoken, $topproducerID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($topproducerID == "") {
			$topproducerID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('topproducerID' => $topproducerID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);


		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyRealtyJugglerID($managerID, $vtoken, $realtyjugglerID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($realtyjugglerID == "") {
			$realtyjugglerID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('realtyjugglerID' => $realtyjugglerID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function applyFollowUpBossID($managerID, $vtoken, $followupbossID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($followupbossID == "") {
			$followupbossID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('followupbossID' => $followupbossID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function applyWiseAgentID($managerID, $vtoken, $wiseagentID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($wiseagentID == "") {
			$wiseagentID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('wiseagentID' => $wiseagentID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function applyContactuallyID($managerID, $vtoken, $contactuallyID, $_contactuallyBucket) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if($contactuallyID == "") {
			$contactuallyID = "N/A";
		}

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('contactuallyID' => $contactuallyID, 'contactuallyBucket' => $_contactuallyBucket));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function getContactuallyBuckets($managerID,$vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

				$access_token = $manager_result["contactuallyID"];
			 	$url = "https://api.contactually.com/v2/buckets/";

			  $ch = curl_init();
			  curl_setopt($ch,CURLOPT_URL,$url);
			  curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'GET');
			  curl_setopt($ch,CURLOPT_POST,1);
			  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			  curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
			  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

			  $headers = array(
				'Content-Type: application/json',
				'Authorization: Bearer '.$access_token
			  );

			  curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);

			  $server_output = curl_exec($ch);
			  curl_close($ch);

			   $server_output =  json_decode($server_output, true);

			  $return = array("status" => 1, "buckets" => $server_output["data"]);
			  //echo($server_output);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function saveContactuallyBucket($managerID, $vtoken, $bucketID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

				$newdata = array('$set' => array('contactuallyBucket' => $bucketID));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);

		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editTopProducer($managerID,$vtoken,$topproducerID,$marketsnapID) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if ($topproducerID == "") {
			$topproducerID = "N/A";
		}

		if ($marketsnapID == "") {
			$marketsnapID = "N/A";
		}
		if (!is_null($manager_result)) {

			$newdata = array('$set' => array('topproducerID'=>$topproducerID,'marketsnapID' => $marketsnapID));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function checkTrial($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_subscriptions = Db_Conn::getInstance()->getConnection()->subscriptions;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		/*
		if (!is_null($manager_result)) {

			if ($manager_result["type"] == "trial") {
				$date = new DateTime();
				$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);

				if (($manager_result["dateCreated"]->sec + (60*60*24*15)) < $currentTime->sec ) {
					$return = array("status" => 2);
					$newdata = array('$set' => array('type' => "freemium"));
					$u_result = $collection_managers->update(array('_id' => new MongoId($managerID),"vtoken" => $vtoken), $newdata);

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => '<html>
		<body>
			<div style="background:#f7f7f7;padding:20px;">
				<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
				<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
					<p>Hello '.$manager_result["fname"].',</p> <p>Your Spacio 15-day unlimited-use trial period has ended. Your account has been moved to FREE.</p>
<p>If you would like to continue enjoying the benefits of our PREMIUM features, please login to your account and upgrade your plan. Login at: <a href="http://www.spac.io/dashboard">spac.io/dashboard</p></p>
<p><b>With Spacio PRO, you can:</b><br/>
- Export leads to import into your CRM or other marketing software for newsletters and events.<br/>
- Customize your auto email message and signature.<br/>
- Integrate with CRMs like Contactually and Top Producer. Don’t see your favorite CRM? Let us know by replying to this email.<br/>
- Stay connected by broadcasting messages to buyers with updates and other similar properties you list.<br/></p>
<p>For more information, please visit: spac.io</p>
<p>Please do share any feedback you have on how we can make Spacio better to help you run more effective open houses! We look forward to hearing from you.</p><br/>
<p>The Spacio Team<br/>hello@spac.io</p></div>
				<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
					Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
				</div>
			</div>
		</body>
	</html>',
							'subject' => 'Your Spacio PRO Trial Has Ended',
							'from_email' => 'hello@spac.io',
							'from_name' => "Spacio",
							'to' => array(
								array(
									'email' => $manager_result["email"]
								)
							),
							'headers' => array('Reply-To' => 'hello@spac.io'),
							'important' => false,
							'tags' => array('trial-ended')

						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}

				} else {
					$return = array("status" => 1);
				}
			} else if($manager_result["type"] == "standard") {
				$date = new DateTime();
				$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);

				$subscription_result = $collection_subscriptions->findOne(array('managerID' => $managerID));
				if ($subscription_result["expiry"]->sec < $currentTime->sec ) {
					$return = array("status" => 3);
					$newdata = array('$set' => array('type' => "freemium"));
					$u_result = $collection_managers->update(array('_id' => new MongoId($managerID),"vtoken" => $vtoken), $newdata);
				} else if ($subscription_result["status"] == "cancelled"){
					$return = array("status" => 4);
				} else {
					$return = array("status" => 1);
				}
			} else {
				$return = array("status" => 1);
			}

		} else {
			$return = array("status" => 0);
		}
		*/
		$return = array("status" => 1);
		return $return;
	}

	static function resendConfirmationEmail($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			 Onboarding_Email::accountConfirmationRequest($manager_result["fname"],$manager_result["email"],$managerID,$vtoken);

			 /*
			try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Hello '.$manager_result["fname"].',</p> <p>Please confirm your email by clicking <a href="http://www.spac.io/confirmemail.php?e='.$managerID.'&v='.$vtoken.'"><u>HERE</u></a></p><p>Download the Spacio iPad app <a href="https://itunes.apple.com/app/spacio-pro-best-open-house/id1017394547?ls=1&mt=8"><u>HERE</u></a></p><p><b>Enjoy a 2 Week Free Trial of Spacio PRO</b><br/>- Digitally capture leads at open houses with our sign-in form and instantly email property details to buyers.<br/>- Manage, cultivate, and convert more leads. Access your lists of buyers anytime to retarget other listings.<br/>- Stay connected by broadcasting messages to buyers with updates and other similar properties you list.</p><p><b>Getting Started</b><br/>- Login to your account at <a href="http://www.spac.io/dashboard">www.spac.io/dashboard</a> or on the Spacio app and complete your profile in My Account.<Br/>- Go to the My Open Houses, click Add Open House and input the details of your open house. After that, customize your form by clicking Edit Sign-In form. Click Launch Sign-in Form and you’re all set!<Br/>- After your open house, go to the same menu and click on View Registrants to view, edit, and export your list of potential buyers. To send everyone who signed in an update about your listing, go to Create Broadcast Message.</p><p>No more pen and paper, no more ugly handwriting, no more missed opportunities.</p><p>Good luck!</p><br/><p>The Spacio Team<br/>hello@spac.io</p></div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => 'Your Email Confirmation Required for Spacio',
						'from_email' => 'hello@spac.io',
						'from_name' => "Spacio",
						'to' => array(
							array(
								'email' => $manager_result["email"]
							)
						),
						'headers' => array('Reply-To' => 'hello@spacioprp.com'),
						'important' => false,
						'tags' => array('resend-confirm')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}
				*/
			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function checkEmailConfirmed($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			if($manager_result["emailConfirmed"]) {
				$return = array("status" => 1);

			}  else {
				$return = array("status" => 2);
			}
		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function confirmEmail($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$newdata = array('$set' => array('emailConfirmed' => true));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

			// Onboarding_Email::welcomeAgentToSpacio($manager_result["fname"],$manager_result["email"]);


			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}
		return $return;

	}

	static function forgotPass($email) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('email' => $email));

		if (!is_null($manager_result)) {
			$date = new DateTime();
			$timeStamp = $date->getTimestamp() * 1000;
			$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

			$rtoken = generateRandomString(16);

			$newdata = array('$set' => array('rtoken' => $rtoken,"rtDateCreated" => $currentTime));
			$u_result = $collection_managers->updateOne(array('email' => $email), $newdata);

			$managerID = (string)$manager_result["_id"];
			System_Email::passwordReset($manager_result["fname"],$email,$managerID,$rtoken);

			/*
			try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Hello '.$manager_result["fname"].',</p>
				<p>We received a request to reset your Spacio account password.</p>
				<p>Click here to reset your password: <a href="http://www.spac.io/resetpassword.php?e='.$manager_result["_id"]->{'$id'}.'&r='.$rtoken.'">http://www.spac.io/resetpassword.php?e='.$manager_result["_id"]->{'$id'}.'&v='.$rtoken.'</a></p>
				<p>If you did not make this request, please disregard this email. Your password will remain the same.</p><br/>
				<p>The Spacio Team<br/>hello@spac.io</p>
			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => '[Spacio] Password Reset Request',
						'from_email' => 'hello@spac.io',
						'from_name' => "Spacio",
						'to' => array(
							array(
								'email' => $email
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('reset-pass')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}
				*/
			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function resetPass($managerID,$rtoken,$pw,$salt) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "rtoken" => $rtoken));

		if (!is_null($manager_result)) {
			$date = new DateTime();
			$timeStamp = $date->getTimestamp() * 1000;
			$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);
			$rtoken = generateRandomString(16);
			$vtoken = generateRandomString(16);

			$newdata = array('$set' => array('rtoken' => $rtoken,"rtDateCreated" => $currentTime,"pw" => $pw, "salt" => $salt, "vtoken" => $vtoken));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID)), $newdata);

			$return = array(
				"status" => 1
			);
		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;
	}
//
// 	static function deleteManager($managerID, $vtoken) {
// 		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
//
// 		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
//
// 		if (!is_null($manager_result)) {
// 			$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
// 			$manager_result = $collection_managers->findOne(array("_id" => $managerID));
//
// 			if (!is_null($manager_result)) {
// 				$collection_managers->remove(array('_id' => $managerID));
// 				$return = array(
// 					"status" => 1
// 				);
// 			} else {
// 				$return = array(
// 					"status" => 2
// 				);
// 			}
//
// 		} else {
// 			$return = array("status" => 0);
// 		}
//
// 		return $return;
// 	}
//
// 	static function suspendManager($managerID, $status) {
// 		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
//
// 		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
//
// 		if (!is_null($manager_result)) {
// 			$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
// 			$manager_result = $collection_managers->findOne(array("managerID" => $managerID));
//
// 			if (!is_null($manager_result)) {
// 				$newdata = array('$set' => array("status" => $status));
// 				$u_result = $collection_managers->update(array("managerID" => $managerID), $newdata);
//
// 				$return = array("status" => 1);
// 			} else {
// 				//Manager not found
// 				$return = array("status" => 2);
// 			}
//
// 		} else {
// 			$return = array("status" => 0);
// 		}
//
// 		return $return;
// 	}
	static function getSaltByEmail($email) {

		$collection = Db_Conn::getInstance()->getConnection()->managers;
		$s_result = $collection->findOne(array('email' => $email), array('salt'));

		if (!is_null($s_result)) {
			$return = array(
				"status" => 1,
				"salt" => $s_result["salt"]
			);
		} else {
			$return = array(
				"status" => 0
			);
		}
		return $return;
	}

	static function getManagerToken($vtoken,$email) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection_managers->findOne(array('email' => $email));
		if (!is_null($manager_result)) {
			$return = array("status" => 1,
							"vtoken" => $manager_result["vtoken"],
							// "id" => $manager_result["_id"]->{'$id'});
							"id" => (string)$manager_result["_id"]);


		} else {
			$return = array("status" => 0);

		}
		return $return;
	}

	static function getManagerPaymentInfo($managerID,$vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_subscriptions = Db_Conn::getInstance()->getConnection()->subscriptions;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));
		if (!is_null($manager_result)) {

			$subscription_result = $collection_subscriptions->findOne(array('managerID' => $managerID));

			if (!is_null($subscription_result)) {
				$return = array("status" => 1,
						"cus" => $subscription_result["cus"],
							"cc_type" => $subscription_result["cc_type"],
							"last4" => $subscription_result["last4"]);
			} else {
				$return = array("status" => 1,
						"cus" => "N/A",
							"cc_type" => "N/A",
							"last4" => "N/A");
			}

		} else {
			$return = array("status" => 0);
		}
		return $return;
	}

	static function saveTimezone($managerID,$vtoken,$timezoneOffset) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$newdata = array('$set' => array('timezoneOffset' => $timezoneOffset));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);
		}

		$return = array("status" => 1);
		return $return;

	}

	static function getManager($managerID,$vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		$return = array();
		if (!is_null($manager_result)) {



			foreach ($manager_result as $k => $v) {
    			$return[$k] = $v;
			}

			$return["status"] = 1;
			$return["id"] = (string)$manager_result["_id"];


			unset($return['pw']);
			unset($return['dateCreated']);
			unset($return['lastActive']);

			/*
			$return = array("status" => 1,
								"email" => $manager_result["email"],
								"phone" => $manager_result["phone"],
								"mobile" => $manager_result["mobile"],
								"website" => $manager_result["website"],
								"brokerage" => $manager_result["brokerage"],
								"brokerageID"=>$manager_result["brokerageID"],
								"office" => $manager_result["office"],
								"officeID"=>$manager_result["officeID"],
								"addr1" => $manager_result["addr1"],
								"addr2" => $manager_result["addr2"],
								"pphoto" => $manager_result["pphoto"],
								"fname" => $manager_result["fname"],
								"lname" => $manager_result["lname"],
								"title" => $manager_result["title"],
								"type" => $manager_result["type"],
								"brand" => $manager_result["brand"],
								"topproducerID" => $manager_result["topproducerID"],
								"marketsnapID" => $manager_result["marketsnapID"],
								"rsID" => $manager_result["rsID"],
								"rsAutoClose" => $manager_result["rsAutoClose"],
								"contactuallyID" => $manager_result["contactuallyID"],
								"contactuallyBucket" => $manager_result["contactuallyBucket"],
								"promocode" => $manager_result["promocode"],
								"vtoken" => $manager_result["vtoken"],
								"ukey" => $manager_result["ukey"],
								"id" => $manager_result["_id"]->{'$id'},
								"teamID" => $manager_result["teamID"],
								"teamRole" => $manager_result["teamRole"]);
					*/
		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;

	}

	static function getProfile($ukey) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$profile_result = $collection_managers->findOne(array('ukey' => $ukey));

		if (!is_null($profile_result)) {
			$return = array("status" => 1,
								"email" => $profile_result["email"],
								"phone" => $profile_result["phone"],
								"mobile" => $profile_result["mobile"],
								"website" => $profile_result["website"],
								"brokerage" => $profile_result["brokerage"],
								"addr1" => $profile_result["addr1"],
								"addr2" => $profile_result["addr2"],
								"pphoto" => $profile_result["pphoto"],
								"fname" => $profile_result["fname"],
								"lname" => $profile_result["lname"],
								"title" => $profile_result["title"],
								"rsID" => $profile_result["rsID"]
								);
		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;

	}

	static function updateLastActive($managerID,$vtoken) {

		$collection = Db_Conn::getInstance()->getConnection()->managers;

		$date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);
		$newdata = array('$set' => array('lastActive' => $currentTime));
		$u_result = $collection->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID),"vtoken" => $vtoken), $newdata);

		$return = array("status" => 1);
		return $return;

	}



	static function loginManager($email, $pw) {

		$collection = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection->findOne(array('email' => $email));

		if (!is_null($manager_result)) {

			if ($manager_result["type"] == "banned" ) {
				// Email does not exist
				$return = array(
					"status" => 3
				);
			} else if ($manager_result["pw"] == $pw ) {
				//Create a temporary validation token for both client and server side
				//$vtoken = generateRandomString();
				//$newdata = array('$set' => array("vtoken" => $vtoken));
				//$u_result = $collection->update(array("email" => $_email), $newdata);

				//Login successful
				$date = new DateTime();
				$timeStamp = $date->getTimestamp() * 1000;
				$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

				$newdata = array('$set' => array('lastActive' => $currentTime));
				$u_result = $collection->updateOne(array('email' => $email), $newdata);

				$return = array();
				$return["status"] = 1;

				foreach ($manager_result as $k => $v) {
					$return[$k] = $v;
				}

				$return["id"] = (string)$manager_result["_id"];

				unset($return['pw']);
				unset($return['dateCreated']);
				unset($return['lastActive']);

			} else {
				// Wrong Password
				$return = array(
					"status" => 2
				);
			}
		} else {
			// Email does not exist
			$return = array(
				"status" => 0
			);
		}

		return $return;
		//Return JSON format of reply

	}
//
// 	static function getMLSList($managerID,$vtoken) {
// 		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
// 		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
//
// 		if (!is_null($manager_result)) {
// 			$collection_mls = Db_Conn::getInstance()->getConnection()->mls;
//
// 			$cursor = $collection_managers->find(array("fname"=>"Melissa"));
//
// 			$return = array("status" => 1, "mlslist" => $cursor);
// 		} else {
// 			// User ID not found or validation failure
// 			$return = array("status" => 0);
// 		}
//
// 		return $return;
// 	}
//
//
	static function changePassword($_managerID,$_vtoken,$_old_pass,$_pass) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($_managerID), "vtoken" => $_vtoken));

		if (!is_null($manager_result)) {

			if ($manager_result["pw"] != $_old_pass) {
				$return = array(
					"status" => 2,
				);
			} else {
				$vtoken = generateRandomString(16);
				$newdata = array('$set' => array('pw' => $_pass,'vtoken' => $vtoken));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($_managerID)), $newdata);
				$return = array(
					"status" => 1,
					"vtoken" => $vtoken
				);
			}

		} else {
			// User ID not found or validation failure
			$return = array("status" => 0);
		}

		return $return;
	}

	static function logoutManager($managerID, $vtoken) {

		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			//Logout is basically generating a new random vToken that does not get returned to the client.
			//It effectively logs out anybody else who was previously logged in.
			$vtoken = generateRandomString();
			$newdata = array('$set' => array('vtoken' => $vtoken));
			$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerId)), $newdata);

			//Logout success
			$return = array("status" => 1);

		} else {
			// User ID not found
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editProfile($managerID,$vtoken,$fname, $lname, $title, $email, $phone, $mobile, $website, $brokerage, $office, $addr1, $addr2, $pphoto) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$email = strtolower($email);
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));
		$email_result = $collection_managers->findOne(array('email' => $email, '_id' => array('$ne'=>new MongoDB\BSON\ObjectID($managerID))));

		if ($rsID == "") {
			$rsID = "N/A";
		}

		if ($office == "") {
			$office = "N/A";
		}

		if (!is_null($email_result)) {
			//Email already taken
			$return = array(
				"status" => 2
			);
		} else if (!is_null($manager_result)) {

			$old_email = $manager_result["email"];

			if ($old_email == $email) {

				if ($pphoto == "") {
					$newdata = array('$set' => array('fname' => $fname,'lname' => $lname,'title' => $title,'email' => $email,'phone' => $phone,'mobile' => $mobile,'website' => $website, 'brokerage' => $brokerage, 'office' => $office, 'addr1' => $addr1, 'addr2' => $addr2));
				} else {
					$newdata = array('$set' => array('fname' => $fname,'lname' => $lname,'title' => $title,'email' => $email,'phone' => $phone,'mobile' => $mobile,'website' => $website, 'brokerage' => $brokerage, 'office' => $office, 'addr1' => $addr1, 'addr2' => $addr2, 'pphoto' => $pphoto));
				}

				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID)), $newdata);

				$return = array(
					"status" => 1,
				);
			} else {

				$newdata = array('$set' => array('fname' => $fname,'lname' => $lname,'title' => $title,'email' => $email,'phone' => $phone,'mobile' => $mobile,'website' => $website, 'brokerage' => $brokerage,'office' => $office,'addr1' => $addr1, 'addr2' => $addr2,'pphoto' => $pphoto,'emailConfirmed' => false));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID)), $newdata);

				System_Email::confirmNewEmail($manager_result["fname"],$email,$managerID,$vtoken);
				/*
				try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Hello '.$manager_result["fname"].',</p><p>You\'ve just changed your account email. </p>
				<p>Please confirm this new email by clicking <a href="http://www.spac.io/confirmemail.php?e='.$managerID.'&v='.$vtoken.'"><u>HERE</u></a>.</p><br/><p>The Spacio Team<br/>hell@spac.io</p>

			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => '[Spacio] Email Address Changed',
						'from_email' => 'hello@spac.io',
						'from_name' => "Spacio",
						'to' => array(
							array(
								'email' => $email
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('email-changed')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}
				*/
				$return = array(
					"status" => 3,
				);
			}



		} else {
			// User ID not found
			$return = array("status" => 0);
		}

		return $return;
	}

	static function resetEmail($managerID,$vtoken,$email) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$email = strtolower($email);
		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));
		$email_result = $collection_managers->findOne(array('email' => $email, '_id' => array('$ne'=>new MongoDB\BSON\ObjectID($managerID))));

		if (!is_null($email_result)) {
			//Email already taken
			$return = array(
				"status" => 2
			);
		} else if (!is_null($manager_result)) {

				$newdata = array('$set' => array('email' => $email,'warning' => "N/A",'emailConfirmed' => false));
				$u_result = $collection_managers->updateOne(array('_id' => new MongoDB\BSON\ObjectID($managerID)), $newdata);

				try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>Hello '.$manager_result["fname"].',</p><p>You\'ve just changed your account email. </p>
				<p>Please confirm this new email by clicking <a href="http://www.spac.io/confirmemail.php?e='.$managerID.'&v='.$vtoken.'"><u>HERE</u></a>.</p><br/><p>The Spacio Team<br/>hell@spac.io</p>

			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => '[Spacio] Email Address Changed',
						'from_email' => 'hello@spac.io',
						'from_name' => "Spacio",
						'to' => array(
							array(
								'email' => $email
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('email-changed')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}

				$return = array(
					"status" => 1,
				);
		} else {
			// User ID not found
			$return = array("status" => 0);
		}

		return $return;
	}

	static function saveCreditCard($managerID, $vtoken, $stripetoken) {
		\Stripe\Stripe::setApiKey(STRIPE_SKEY);
		$collection_subscriptions = Db_Conn::getInstance()->getConnection()->subscriptions;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$subscription_result = $collection_subscriptions->findOne(array('managerID' => $managerID));

			if (!is_null($subscription_result)) {
				$cus = $subscription_result["cus"];

				$token = $stripetoken;
				try {
					$customer = \Stripe\Customer::retrieve($cus);
					$customer->source = $token; // obtained with Stripe.js
					$customer->save();

				} catch(\Stripe\Error\Card $e) {
				  // Since it's a decline, \Stripe\Error\Card will be caught
				  $body = $e->getJsonBody();
				  $err  = $body['error'];

				  $return = array("status" => 0,
									"error_type" =>  $err['type'],
									"error_code" =>  $err['code'],
									"error_param" =>  $err['param'],
									"error_msg" =>  $err['message']);
				  return $return;

				} catch (\Stripe\Error\InvalidRequest $e) {
				  // Invalid parameters were supplied to Stripe's API
				  $body = $e->getJsonBody();
				  $err  = $body['error'];

					$return = array("status" => 0,
									"error_type" =>  $err['type'],
									"error_code" =>  $err['code'],
									"error_param" =>  $err['param'],
									"error_msg" =>  $err['message']);
					return $return;
				} catch (\Stripe\Error\Authentication $e) {
				  // Authentication with Stripe's API failed
				  // (maybe you changed API keys recently)
				  $return = array("status" => 0,
									"error_type" =>  "Authentication Failure",
									"error_param" =>  "");
					return $return;
				} catch (\Stripe\Error\ApiConnection $e) {
				  // Network communication with Stripe failed
				 $return = array("status" => 0,
									"error_type" =>  "Network Failure",
									"error_param" =>  "");
					return $return;
				} catch (\Stripe\Error\Base $e) {
				  // Display a very generic error to the user, and maybe send
				  // yourself an email
				  $return = array("status" => 0,
									"error_type" =>  "Base Error",
									"error_param" =>  "");
					return $return;
				} catch (Exception $e) {
					$return = array("status" => 0,
									"error_type" => "Unkown",
									"error_param" =>  "");
					return $return;
				  // Something else happened, completely unrelated to Stripe
				}

				$cc_type = $customer["sources"]["data"][0]["brand"];
				$last4 = $customer["sources"]["data"][0]["last4"];

				$newdata = array('$set' => array("cus" => $customer["id"],
						"cc_type" => $cc_type,
						"last4" => $last4));

				$u_result_accounts = $collection_subscriptions->updateOne(array('managerID' => $managerID), $newdata);

				$return = array("status" => 1,
									"cus" => $customer["id"]);
			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 3);
		}
		return $return;
	}

	static function getReport($reportID) {
		$collection_reports = Db_Conn::getInstance()->getConnection()->user_reports;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;


		$report_result = $collection_reports->findOne(array('_id' => new MongoDB\BSON\ObjectID($reportID)));

		if (!is_null($report_result)) {
			$manager_result = $collection_managers->findOne(array('_id' =>new MongoDB\BSON\ObjectID($report_result["creator"])));

			$manager = array();
			$manager["fname"] = $manager_result["fname"];
			$manager["lname"] = $manager_result["lname"];
			$manager["email"] = $manager_result["email"];
			$manager["pphoto"] = $manager_result["pphoto"];
			$manager["phone"] = $manager_result["phone"];
			$manager["brokerageID"] = $manager_result["brokerageID"];
			$manager["company"] = $manager_result["brokerage"];

			$property = array();


			if($report_result["pid"] != "all") {
				$property_result = $collection_properties->findOne(array('_id' =>new MongoDB\BSON\ObjectID($report_result["pid"])));
				$property["title"] = $property_result["title"];
				$property["image"] = $property_result["image"];
				$property["addr1"] = $property_result["addr1"];
				$property["addr2"] = $property_result["addr2"];
			}

			$brokerage = array();

			if($manager_result["brokerageID"] != "N/A" && $manager_result["brokerageID"] != "") {
				$brokerage_result = $collection_brokerages->findOne(array('brokerageID' =>$manager_result["brokerageID"]));
				$brokerage["logos"] = $brokerage_result["logos"];
				$brokerage["colors"] = $brokerage_result["colors"];
				$brokerage["brokerageID"] = $brokerage_result["brokerageID"];
				$brokerage["brokerageName"] = $brokerage_result["brokerageName"];
			}

			$return = array(
				"status" => 1,
				"report" => $report_result,
				"manager" => $manager,
				"property" => $property,
				"brokerage" => $brokerage
			);
		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function createShareLink($managerID, $vtoken, $pid, $start, $end, $stats, $activeSubCategories, $rankings_html) {
		$collection_reports = Db_Conn::getInstance()->getConnection()->user_reports;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoDB\BSON\ObjectID($managerID), "vtoken" => $vtoken));

		$date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);

		if (!is_null($manager_result)) {
			$document_report = array(
								"creator"=> $managerID,
								"pid" => $pid,
								"start" => $start,
								"end" => $end,
								"stats" => $stats,
								"activeSubCategories" => $activeSubCategories,
								"rankings_html" => $rankings_html,
								"dateCreated" => $currentTime);

			$result = $collection_reports->insertOne($document_report);

			// $newID = $document_report["_id"]->{'$id'};
			$newID = (string)$document_report["_id"];

			$return = array("status" => 1, 	"reportID" => (string)$newID);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}
//
// 	static function getTeam($managerID,$vtoken) {
// 		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
//
// 		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
//
// 		if (!is_null($manager_result)) {
//
// 			$teamID = $manager_result["teamID"];
// 			$team_result = $collection_managers->find(array('teamID' => $teamID));
//
// 			$team = array();
//
// 			foreach ($team_result as $member) {
// 				$member_to_add = array();
// 				$member_to_add["id"] =  $member["_id"]->{'$id'};
// 				$member_to_add["fname"] =  $member["fname"];
// 				$member_to_add["lname"] =  $member["lname"];
// 				$member_to_add["email"] =  $member["email"];
// 				array_push($team, $member_to_add);
//
// 			}
//
// 			$return = array(
// 				"status" => 1,
// 				"team" => $team
// 			);
//
// 		} else {
// 			$return = array("status" => 0);
// 		}
//
// 		return $return;
// 	}
}



?>
