<?php
//get url, APIKey and credential
function wiseagent($data){
  $data['apikey'] = "HTlLoR8n0BAHFAl2uHRhjtlVyU777e1s";
  $url = "https://sync.thewiseagent.com/http/webconnect.asp";
  $options = array(
    'http' => array(
      'header' => "Content-type: application/x-www-form-urlencoded",
      'content' => http_build_query($data),
    ),
  );

  switch ($data['requestType']) {
    case 'getContacts':
      $options['http']['method'] = "GET";
      break;
    case 'webcontact':
      $options['http']['method'] = "POST";
      break;
    case 'addContactNote':
      $options['http']['method'] = "POST";
      break;
  }


  $context = stream_context_create($options);
  $res = file_get_contents($url, false, $context);

  //error handling
  if(substr($res,0,5) == 'error'){
    $json_res['error'] = substr($res,7);
  } else{
    //decode json
    $res = str_replace(
        array('{',  ":", "},{", "[", "]"),
        array('{"', '":', ",", "", ""),
        $res
    );
    $json_res = json_decode($res, true);
  };

  $json_res['input_params'] = $data;
  return $json_res;

}

?>
