<?php

require_once('impact_config.php');
require_once('impact_constants.php');


class MySQLDB {
	
	var $salt = 08721364018273460;
	var $con;
	
	/**
	 *
	 * Constructor
	 *
	 **/
	function MySQLDB() {
		$this->con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);

		if ($mthis->con->connect_errno) {
    			echo "Failed to connect to MySQL: " . $this->con->connect_error;
		}
		//$this->con=mysql_connect(DB_HOST,DB_USER,DB_PASSWORD) or die('Could not connect: ' . mysql_error());
		//mysql_select_db(DB_NAME, $this->con) or install_database();		
	}
	
	
	/**
	 *
	 * Support Functions
	 *
	 **/
	
	function generateRandomString($length = 30) {
    	return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}
	
	
	/**
	 *
	 * Core Functions
	 *
	 **/
	 function checkLogin($_user_email,$_user_pass){
		if(!get_magic_quotes_gpc()) {
	    	$_user_email = addslashes($_user_email);
      	}

     	 /* Verify that user is in database */
      	$q = "SELECT * FROM ".TBL_PREFIX."_users WHERE user_email = '$_user_email'";
      	$qresult = $this->con->query($q);
      	if(!$qresult || ($qresult->num_rows < 1)){
        	$result = array(
				//Indicates user email failure
				"status" => 1,
			); 
			return $result;
      	}

      /* Retrieve password from result, strip slashes */
	  $row = $qresult->fetch_assoc();
	  $password = stripslashes($row['user_pass']);
      $_user_pass = md5($_user_pass.$salt);

      /* Validate that password is correct */
	  if($password == $_user_pass){
		  		
        $result = array(
			"status" => 0,
			"user_id" => $row["user_id"],
			"user_fname" =>$row["user_fname"],
			"user_lname" =>$row["user_lname"],
			"user_email" =>$row["user_email"],
			"user_type" => $row["user_type"]
		);
		 
   		 /* free result set */
		 return $result;
		  
      } else{
         $result = array(
			//Indicates password failure
			"status" => 2,
		); 
		return $result;
      }
	}
	 
	function addUser($_user_fname, $_user_lname, $_user_email, $_user_pass, $_user_region, $_user_type) {
		$q = "SELECT * FROM ".TBL_PREFIX."_users WHERE user_email = '$_user_email'";
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
			$password = md5($_user_pass.$salt);
		
			$q = "INSERT INTO ".TBL_PREFIX."_users (user_fname,user_lname,user_pass,user_email,user_region,user_type) VALUES ('$_user_fname','$_user_lname','$password','$_user_email','$_user_region','$_user_type')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function addCategory($_category_name) {
		$q = "SELECT * FROM ".TBL_PREFIX."_categories WHERE category_name = '$_category_name'";
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_categories (category_name) VALUES ('$_category_name')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function addPromotion($_promotion_name) {
		$q = "SELECT * FROM ".TBL_PREFIX."_promotions WHERE promotion_name = '$_promotion_name'";
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_promotions (promotion_name) VALUES ('$_promotion_name')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function addLanguage($_language_name) {
		$q = "SELECT * FROM ".TBL_PREFIX."_languages WHERE language_name = '$_language_name'";
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_languages (language_name) VALUES ('$_language_name')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function addShottype($_shottype_name) {
		$q = "SELECT * FROM ".TBL_PREFIX."_shottypes WHERE shottype_name = '$_shottype_name'";
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_shottypes (shottype_name) VALUES ('$_shottype_name')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function addStore($_store_name,$_store_address1,$_store_address2,$_store_rep,$_store_region) {
		$q = "SELECT * FROM ".TBL_PREFIX."_stores WHERE store_name = '$_store_name'";
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_stores (store_name,store_address1,store_address2,store_rep,store_region) VALUES ('$_store_name','$_store_address1','$_store_address2','$_store_rep','$_store_region')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function addLightbox($_lightbox_name,$_lightbox_trimwidth,$_lightbox_trimheight,$_lightbox_viewwidth,$_lightbox_viewheight,$_lightbox_price) {
		$q = "SELECT * FROM ".TBL_PREFIX."_lightboxes WHERE lightbox_name = '$_lightbox_name'";
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_lightboxes (lightbox_name,lightbox_trimwidth,lightbox_trimheight,lightbox_viewwidth,lightbox_viewheight,lightbox_price) VALUES ('$_lightbox_name','$_lightbox_trimwidth','$_lightbox_trimheight','$_lightbox_viewwidth','$_lightbox_viewheight','$_lightbox_price')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	
	function addLightboxToStore($_lightbox_id, $_store_id) {
		$q = "SELECT * FROM ".TBL_PREFIX."_storebox WHERE store_id = ".$_store_id." AND lightbox_id = ".$_lightbox_id;
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_storebox (store_id,lightbox_id) VALUES ('$_store_id','$_lightbox_id')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function addStoreToUser($_user_id, $_store_id) {
		$q = "SELECT * FROM ".TBL_PREFIX."_storeuser WHERE store_id = ".$_store_id." AND user_id = ".$_user_id;
		$result = $this->con->query($q);
		
		if ($result->num_rows > 0) {
			return 2;
		} else {
		
			$q = "INSERT INTO ".TBL_PREFIX."_storeuser (store_id,user_id) VALUES ('$_store_id','$_user_id')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function deleteUser($_user_id) {
		$q = "DELETE FROM ".TBL_PREFIX."_users WHERE user_id = ".$_user_id;
		$qresult = $this->con->query($q);
		
		if ($qresult) {
			$u = "DELETE FROM ".TBL_PREFIX."_storeuser WHERE user_id = ".$_user_id;
			$uresult = $this->con->query($u);
			
			if($uresult) {
				return 0;	
			} else {
				return 1;	
			}
		} else {
			return 1;
		}
	}
	
	function deleteCategory($_category_id) {
		$q = "DELETE FROM ".TBL_PREFIX."_categories WHERE category_id = ".$_category_id;
		$qresult = $this->con->query($q);
		
		if ($qresult) {
			return 0;
		} else {
			return 1;
		}
	}
	
	function deletePromotion($_promotion_id) {
		$q = "DELETE FROM ".TBL_PREFIX."_promotions WHERE promotion_id = ".$_promotion_id;
		$qresult = $this->con->query($q);
		
		if ($qresult) {
			return 0;
		} else {
			return 1;
		}
	}
	
	function deleteLanguage($_language_id) {
		$q = "DELETE FROM ".TBL_PREFIX."_languages WHERE language_id = ".$_language_id;
		$qresult = $this->con->query($q);
		
		if ($qresult) {
			return 0;
		} else {
			return 1;
		}
	}
	
	function deleteShottype($_shottype_id) {
		$q = "DELETE FROM ".TBL_PREFIX."_shottypes WHERE shottype_id = ".$_shottype_id;
		$qresult = $this->con->query($q);
		
		if ($qresult) {
			return 0;
		} else {
			return 1;
		}
	}
	
	function deleteStore($_store_id) {
		$q = "DELETE FROM ".TBL_PREFIX."_stores WHERE store_id = ".$_store_id;
		$qresult = $this->con->query($q);
		
		if ($qresult) {
			$u = "DELETE FROM ".TBL_PREFIX."_storeuser WHERE store_id = ".$_store_id;
			$uresult = $this->con->query($u);
			
			$p = "DELETE FROM ".TBL_PREFIX."_storebox WHERE store_id = ".$_store_id;
			$presult = $this->con->query($p);
		
			if ($presult) {
				return 0;
			} else {
				return 1;	
			}
		} else {
			return 1;
		}
	}
	
	function deleteLightbox($_lightbox_id) {
		$q = "DELETE FROM ".TBL_PREFIX."_lightboxes WHERE lightbox_id = ".$_lightbox_id;
		$qresult = $this->con->query($q);
		
		if ($qresult) {
			$p = "DELETE FROM ".TBL_PREFIX."_storebox WHERE lightbox_id = ".$_lightbox_id;
			$presult = $this->con->query($p);
		
			if ($presult) {
				return 0;
			} else {
				return 1;	
			}
		} else {
			return 1;
		}
	}
	
	function deleteLightboxFromStore($_store_id,$_lightbox_id) {
		
			$p = "DELETE FROM ".TBL_PREFIX."_storebox WHERE store_id = ".$_store_id." AND lightbox_id = ".$_lightbox_id;
			$presult = $this->con->query($p);
		
			if ($presult) {
				return 0;
			} else {
				return 1;	
			}
		
	}
	
	function deleteStoreFromUser($_store_id,$_user_id) {
		
			$p = "DELETE FROM ".TBL_PREFIX."_storeuser WHERE store_id = ".$_store_id." AND user_id = ".$_user_id;
			$presult = $this->con->query($p);
		
			if ($presult) {
				return 0;
			} else {
				return 1;	
			}
		
	}
	
	function getAdmins() {
		$q = "SELECT * FROM ".TBL_PREFIX."_users WHERE user_type = 'admin'";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getUsers() {
		$q = "SELECT * FROM ".TBL_PREFIX."_users WHERE user_type = 'user'";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getAllUsers() {
		$q = "SELECT * FROM ".TBL_PREFIX."_users WHERE 1";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getCategories() {
		$q = "SELECT * FROM ".TBL_PREFIX."_categories WHERE 1 ORDER BY category_name";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getPromotions() {
		$q = "SELECT * FROM ".TBL_PREFIX."_promotions WHERE 1 ORDER BY promotion_name";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getLanguages() {
		$q = "SELECT * FROM ".TBL_PREFIX."_languages WHERE 1 ORDER BY language_name";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getShottypes() {
		$q = "SELECT * FROM ".TBL_PREFIX."_shottypes WHERE 1 ORDER BY shottype_name";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getStores() {
		$q = "SELECT * FROM ".TBL_PREFIX."_stores WHERE 1";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getLightboxes() {
		$q = "SELECT * FROM ".TBL_PREFIX."_lightboxes WHERE 1";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getLightboxesByStore($_store_id) {
		$q = "SELECT b.* FROM ".TBL_PREFIX."_lightboxes AS b,".TBL_PREFIX."_storebox AS s WHERE s.store_id = ".$_store_id." AND b.lightbox_id = s.lightbox_id";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getStoresByUser($_user_id) {
		$q = "SELECT b.* FROM ".TBL_PREFIX."_stores AS b,".TBL_PREFIX."_storeuser AS s WHERE s.user_id = ".$_user_id." AND b.store_id = s.store_id";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function editUser($_user_id,$_user_fname,$_user_lname,$_user_email,$_user_type) {
		$q = "SELECT * FROM ".TBL_PREFIX."_users WHERE user_id = ".$_user_id;
		$result = $this->con->query($q);
		
		if ($result->num_rows < 1) {
			return 2;
		} else {
		
			$q = "UPDATE ".TBL_PREFIX."_users SET user_fname = '$_user_fname',user_lname = '$_user_lname',user_email = '$_user_email',user_type = '$_user_type' WHERE user_id = ".$_user_id; 
			
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	
	function editStore($_store_id,$_store_name,$_store_address1,$_store_address2,$_store_rep,$_store_region) {
		$q = "SELECT * FROM ".TBL_PREFIX."_stores WHERE store_id = ".$_store_id;
		$result = $this->con->query($q);
		
		if ($result->num_rows < 1) {
			return 2;
		} else {
		
			$q = "UPDATE ".TBL_PREFIX."_stores SET store_name = '$_store_name',store_address1 = '$_store_address1',store_address2 = '$_store_address2',store_rep = '$_store_rep',store_region = '$_store_region' WHERE store_id = ".$_store_id; 
			
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	
	function editLightbox($_lightbox_id,$_lightbox_name,$_lightbox_trimwidth,$_lightbox_trimheight,$_lightbox_viewwidth,$_lightbox_viewheight,$_lightbox_price) {
		$q = "SELECT * FROM ".TBL_PREFIX."_lightboxes WHERE lightbox_name = '$_lightbox_name'";
		$result = $this->con->query($q);
		
		if ($result->num_rows < 1) {
			return 2;
		} else {
		
			$q = "UPDATE ".TBL_PREFIX."_lightboxes SET lightbox_name = '$_lightbox_name',lightbox_trimwidth = '$_lightbox_trimwidth',lightbox_trimheight = '$_lightbox_trimheight',lightbox_viewwidth = '$_lightbox_viewwidth',lightbox_viewheight = '$_lightbox_viewheight',lightbox_price = '$_lightbox_price' WHERE lightbox_id = ".$_lightbox_id;
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	
	function changePass($_user_id,$_user_pass) {
		$q = "SELECT * FROM ".TBL_PREFIX."_users WHERE user_id = ".$_user_id;
		$result = $this->con->query($q);
		
		if ($result->num_rows < 1) {
			return 2;
		} else {
			$password = md5($_user_pass.$salt);
			
			$q = "UPDATE ".TBL_PREFIX."_users SET user_pass = '$password' WHERE user_id = ".$_user_id; 
			
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
		}
	}
	/**
	 ************************ Ordering Functions ***********************
	 **/
	 
	 
	function addOrder($_uid, $_sid, $_details,$_price,$_rep) {
		$q = "INSERT INTO ".TBL_PREFIX."_orders (user_id,store_id,order_details,order_price,order_rep,order_datetime,order_status) VALUES ('$_uid','$_sid','$_details','$_price','$_rep',NOW(),'Pending')";
			$result = $this->con->query($q);
		
			if($result) {
				return 0;	
			} else {
				return 1;	
			}
	}
	
	function getOrders() {
		$q = "SELECT o.*, u.user_fname, u.user_lname, u.user_email, s.store_name FROM impact_orders AS o,impact_users AS u, impact_stores AS s WHERE o.user_id = u.user_id AND o.store_id = s.store_id  ORDER BY order_datetime DESC";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getPendingOrders() {
		$q = "SELECT * FROM ".TBL_PREFIX."_orders WHERE order_status = 'Pending'";
		$result = $this->con->query($q);
		
		return $result;
	}
	
	function getOrdersByUser($_user_id) {
		$q = "SELECT o.*, s.store_name FROM impact_orders AS o, impact_stores AS s WHERE o.user_id = ".$_user_id." AND o.store_id = s.store_id ORDER BY order_datetime DESC";
		$result = $this->con->query($q);
		
		return $result;
	}
  
  	function completeOrder($_oid) {
		$q = "UPDATE ".TBL_PREFIX."_orders SET order_status = 'Completed' WHERE order_id = ".$_oid; 
			
		$result = $this->con->query($q);
		
		if($result) {
			return 0;	
		} else {
			return 1;	
		}
	}
	
	function deleteOrder($_order_id) {
		$q = "SELECT * FROM ".TBL_PREFIX."_orders WHERE order_id = ".$_order_id;
		$qresult = $this->con->query($q);
		$row = $qresult->fetch_assoc();
	  	$order_status = stripslashes($row['order_status']);
	  
		if ($order_status == "Pending") {
			$p = "DELETE FROM ".TBL_PREFIX."_orders WHERE order_id = ".$_order_id;
			$presult = $this->con->query($p);
		
			if ($presult) {
				return 0;
			} else {
				return 1;	
			}
		} else {
			return 2;
		}
	}
	
	function removeOrder($_order_id) {
			$p = "DELETE FROM ".TBL_PREFIX."_orders WHERE order_id = ".$_order_id;
			$presult = $this->con->query($p);
		
			if ($presult) {
				return 0;
			} else {
				return 1;	
			}
	}
	
	function setThumbnail($_lid,$_pid,$_url) {
		$p = "DELETE FROM ".TBL_PREFIX."_thumbs WHERE lightbox_id = ".$_lid." AND promotion_id = ".$_pid;
		$presult = $this->con->query($p);
		
		if ($presult) {
			$q = "INSERT INTO ".TBL_PREFIX."_thumbs (lightbox_id,promotion_id,thumbs_url) VALUES ('$_lid','$_pid','$_url')";
			$qresult = $this->con->query($q);
			
			if ($qresult) {
				return "Thumbnail has been set for Lighbox ID ".$_lid.".";
			} else {
				return "There was an unknown error.(1)";
			}
			
		} else {
			$q = "INSERT INTO ".TBL_PREFIX."_thumbs (lightbox_id,promotion_id,thumbs_url) VALUES ('$_lid','$_pid','$_url')";
			$qresult = $this->con->query($q);
			
			if ($qresult) {
				return "Thumbnail has been set for Lighbox ID ".$_lid.".";
			} else {
				return "There was an unknown error.(1)";
			}	
		}
	}
	
	function createCSV($_sid,$_uid,$_status) {
		
		
		if ($_sid == "all") {
			$_sid_filter = "";
		} else {
			$_sid_filter = " AND o.store_id = ".$_sid." ";
		}
		
		if ($_uid == "all") {
			$_uid_filter = "";
		} else {
			$_uid_filter = " AND o.user_id = ".$_uid." ";
		}
		
		if ($_status == "all") {
			$_status_filter = "";
		}  else {
			$_status_filter = " AND o.order_status = '".$_status."' ";	
		}
		
		$q = "SELECT o.*, u.user_fname, u.user_lname, u.user_email, s.store_name FROM impact_orders AS o,impact_users AS u, impact_stores AS s WHERE o.user_id = u.user_id AND o.store_id = s.store_id".$_sid_filter.$_uid_filter.$_status_filter." ORDER BY o.order_datetime DESC";
		$result = $this->con->query($q);
		
		$rows = array();
		while($r = $result->fetch_assoc()) {
    		$rows[] = $r;
		}	
		
		date_default_timezone_set('America/Los_Angeles');
		$date = date('m-d-Y_h:i:s');
		$filename = 'order_'.$date.'.csv';

		
		$fp = fopen('../csv/'.$filename, 'w');
		
		fputcsv($fp, array('Order ID','User ID','Store ID','Order Details','Store Rep','Order Price','Order Date','Order Status','User First Name','User Last Name','User Email','Store Name'));
		
		foreach ($rows as $row) {
    		fputcsv($fp, $row);
		}
		
		fclose($fp);
		
		
		
		
		$result = array(
			"status" => 0,
			"csv" => $filename
		);

		return $result;
		
	}
 
   
	

}

$dbcon = new MySQLDB;


?>