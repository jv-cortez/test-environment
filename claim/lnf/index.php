<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_email = $_GET["e"];
  $_id = $_GET["u"];
  $_from = isset($_GET['f']) ? $_GET['f'] : "";
	$_promo = $_GET["p"];
	$_plan = $_GET["plan"];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/assets/img/favicon.png">

        <title>Spacio: Introducing the Paperless Open House</title>

        <link href="/assets/css/normalize.css" rel="stylesheet">
        <link href="/assets/css/website.css" rel="stylesheet">
        <link href="/assets/css/upgrade.css" rel="stylesheet">

        <link href="/assets/fonts/fonts.css" rel="stylesheet">

        <link rel="import" id="topmenu" href="/topmenu.html">
        <link rel="import" id="footer" href="/footer.html">
        <link rel="import" id="popups" href="/popups.html">
        <link rel="import" id="alertbox" href="/alertbox.html">

      </head>

<body style="padding-top:0;">

	<div class="spacio-top-menu"></div>
  <!-- TOP MENU END -->
	<div class="popup-overlay"></div>
  <!-- POPUP OVERLAY END -->
  <div class="alert-overlay"></div>
  <!-- ALERT OVERLAY END -->
	<div class="loading-panel">
    	<div style="display:flex;flex-direction:column;align-items:center;justify-content:center;width:100%;height:100%;">
        	<div style="height:50px;text-align:center;color:white;">
        		<span class="msg" style="-webkit-touch-callout:none;-webkit-user-select: none;">Loading...</span>
          </div>
      </div>
  </div> <!---- LOADING PANEL OVERLAY --->


  <div class="upgrade-scroll-one">
		<div class="upgrade-panel" >
				<div style="padding:120px 20px;box;box-sizing:border-box;">
					<form id="claimLNFAccount_Form" style="">
            <img src="/assets/img/website/customers/companies/lnf_horizontal_black.png" width="75%" style="margin:0 auto;margin-bottom:30px;display:block;">
						<div class="large" style="margin-bottom:20px;color:#666;">
							Acknowledgement of Account Creation
						</div>
            <div class="normal" style="margin-bottom:20px;">
							In signing up for Spacio, you will be given a 15-day free trial after entering your credit card information. On the 16th day and thereafter, you authorize Spacio to charge <span class="bold">$17.50</span> to your credit card on a recurring monthly basis until you cancel your Spacio subscription. <span class="bold">If you do not wish to continue after the free trial period</span>, you may cancel from your Spacio account under Settings anytime within the <span class="bold">15 day free trial period</span>. Subscription to Spacio is month-to-month, and you may cancel anytime. If you do not want to continue, please select Cancel. If you cancel your account will remain active until the end of the last month paid.
						</div>
            <div class="normal" style="margin-bottom:30px;">
							STEP <span class="bold">1</span>: Please set your password.
						</div>

            <div class="upgrade-input-flex">
							<div class="upgrade-label-2" >EMAIL</div>
							<div class="upgrade-seperator"></div>
							<input class="upgrade-input" type="text" name="email" value="<?php echo $_email;?>" id="email" tabindex="1"  placeholder="">
						</div>

            <div class="upgrade-input-flex">
							<div class="upgrade-label-2">SET PASSWORD</div>
							<div class="upgrade-seperator"></div>
							<input class="upgrade-input" type="password" name="pw" value="" id="pw" tabindex="2"  placeholder="">
						</div>


						<div class="normal" style="margin-bottom:30px;">
							STEP <span class="bold">2</span>: Please enter your payment information. Your credit card will only be charged after the trial period if you wish to continue.
						</div>
						<div style="30px;line-height:30px;width:100%;margin-bottom:10px;">
							<img src="/assets/img/website/mastercard.png" height="30" style="margin-right:10px;">
							<img src="/assets/img/website/visa.png" height="30" style="margin-right:10px;">
							<img src="/assets/img/website/amex.png" height="30" style="margin-right:10px;">
						</div>
						<div class="upgrade-input-flex">
							<div class="upgrade-label">CARD #</div>
							<div class="upgrade-seperator"></div>
							<input class="upgrade-input" type="text" data-stripe="number" value="" size="20" tabindex="3"  placeholder="">
						</div>
						<div class="upgrade-input-flex">
							<div class="upgrade-label">CVC</div>
							<div class="upgrade-seperator"></div>
							<input class="upgrade-input" type="text" data-stripe="cvc" value="" size="4" tabindex="4"  placeholder="">
						</div>
						<div class="upgrade-input-flex">
							<div class="upgrade-label">EXPIRATION</div>
							<div class="upgrade-seperator"></div>
							<div class="upgrade-expiration-row">
								<input class="upgrade-input-half" type="text" data-stripe="exp-month" value="" tabindex="5" size="3"  placeholder="MM" style="width:30px"> /
								<input class="upgrade-input-half" type="text" data-stripe="exp-year" value="" tabindex="6" size="4"  placeholder="YYYY" style="width:45px">
							</div>
						</div>

						<div style="width:100%;text-align:right">
              <a href="https://spac.io" target="_self">
							         <input class="upgrade-back-btn" type="button" name="prev" value="CANCEL">
              </a>
							<input class="upgrade-submit-btn" type="submit"  value="CLAIM ACCOUNT">
						</div>
					</form>
				</div>
				<div style="font-size:12px;position:absolute;bottom:20px;padding:0 20px;">
					Spacio is headquartered in Vancouver, BC, Canada with an office in New York. We are not responsible for any credit card transaction fees from your provider. Please check with your bank or financial institution before upgrading.
				</div>
			</div>


  </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>

<script type="text/javascript">
	var promo = "<?php echo $_promo;?>";
	var plan = "<?php echo $_plan;?>";
	var tax_rates = {};
	var selected_plan = "";
	var account_email = "";
	var discount_annual = 0;
	var discount_monthly = 0;
	var total = 0;

	var tax_type = "";
	var tax_rate = 0;
	var country = "";
	var province = "";

	 //Stripe.setPublishableKey('pk_test_1Xk12UcY2hyb1y3cLVTkk7Rs');
	 Stripe.setPublishableKey('pk_live_QRFV0CvJO6Iq122LsxWt93nv');

	 $(document).ready(function(){

		  insertAlertBox();

	 });





	$( "form#claimLNFAccount_Form" ).submit(function( event ) {
		var $form = $(this);
		var _fields = $( this ).serializeArray();
		var _query = {};



		$form.find('button').prop('disabled', true);

		Stripe.card.createToken($form, function(status, response) {
			if(response.error) {
				showAlert("error","Error Processing Card", response.error.message,"Ok");
			} else {
				var token = response.id;
				claimLNFAccount(token);
			}
		});
		event.preventDefault();
	});



	function claimLNFAccount(tk){
		loading(true);
		var _query = {};
		_query["fn"] = "claimLNFAccount";
		_query["email"] = $("#email").val();
		_query["stripeToken"] = tk;
		_query["pw"] = $("#pw").val();
    _query["id"] = "<?php echo $_id;?>";

    _query["email"] = _query["email"].toLowerCase();
    //Validate form inputs.
    if (_query["email"] == "" || _query["email"] == null) {
      showAlert("error","Invalid Email Format","Please enter a valid email.");
      loading(false);

    } else if(_query["pw"] == "") {
      showAlert("error","No Password","Please enter a password.");
      loading(false);

    } else if(_query["pw"].length < 6) {
      showAlert("error","Invalid Password","Passwords must be at least 6 characters.");
      loading(false);

    } else {
      var _salt = randomString(16);
      _query["salt"] = _salt;
      _query["pw"] += _salt;
      _query["pw"] = CryptoJS.SHA3(_query["pw"], { outputLength: 224 }).toString();
      var json_data = _query;
  		console.log("Outbound JSON Query: "+JSON.stringify(json_data));

  		$.ajax({
        url: "https://spac.io/oh/webservice/1.1.6/webservice.php",
  			type:"POST",
  			data: {json : json_data},
  			success: function(data, textStatus, jqXHR){
  				console.log("Returned JSON Object: "+JSON.stringify(data));
  				loading(false);
  				if (data.status == 1) {
  					$('#claimLNFAccount_Form')[0].reset();
  					showAlert("info","Thank You for Signing Up","Please login.","OK",null,function(){window.location.href="../dashboard/"},null);
  				} else if (data.status == 2) {
            showAlert("error","Account Already Registered","This email has already been claimed. Please login.","Login","Cancel", function(){
            window.location = "https://spac.io/dashboard/";
            }, null);
  				} else if (data.status == 3) {
            showAlert("error","Invalid Link","Sorry, it seems your claim link is invalid. Please contact support@spac.io");
  				} else {
  					if (data.error_type == "invalid_request_error" && data.error_param == "coupon") {
  						showAlert("error","Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
  					} else if (data.error_type =="card_error" && data.error_code == "card_declined"){
  						showAlert("error","Card Declined","Sorry, your card was declined. Please verify that you entered a valid credit card.");
  					} else {
  						showAlert("error","Unknowns","There was an unknown error. Please contact support.");
  					}
  				}
  				//debugLog("Query Status: "+data.status);

  			},
  			error: function(jqXHR, textStatus, errorThrown) {
  				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
  			},
  			dataType: "json"
  		});
    }
	}



	function randomString(_length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

		var randomstring = '';
		for (var i=0; i<_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}

		return randomstring;
	}

	function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
	};

	/*
	var agent = navigator.userAgent;
	var isIphone = ((agent.indexOf('iPhone') != -1) || (agent.indexOf('iPod') != -1)) ;
	if (isIphone) {
		$("#iphone-msg").html("Returning you to the app.");
		setTimeout(function(){window.location.href = 'SpacioiOS://'; }, 2500);

	}
	*/

</script>



</body>
</html>
