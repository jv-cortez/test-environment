<?php

    $spBaseUrl = 'https://spac.io'; //or http://<your_domain>

    $settingsInfo = array (
        'sp' => array (
            'entityId' => $spBaseUrl.'/dashboard/saml/consume',
            'assertionConsumerService' => array (
                'url' => $spBaseUrl.'/dashboard/saml/index.php?acs',
            ),
            'singleLogoutService' => array (
                'url' => $spBaseUrl.'/dashboard/saml/index.php?sls',
            ),
            'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
        ),
        'idp' => array (
            // Dev - https://app.onelogin.com/saml/metadata/673480
            // Prod - https://app.onelogin.com/saml/metadata/676087
            'entityId' => 'https://app.onelogin.com/saml/metadata/676087',
            // Dev - https://spacio-inc-dev.onelogin.com/trust/saml2/http-post/sso/673480
            // Prod - https://realtyonegroup.onelogin.com/trust/saml2/http-post/sso/676087
            'singleSignOnService' => array (
                'url' => 'https://realtyonegroup.onelogin.com/trust/saml2/http-post/sso/676087',
            ),
            // Dev - https://spacio-inc-dev.onelogin.com/trust/saml2/http-redirect/slo/673480
            // Prod - https://realtyonegroup.onelogin.com/trust/saml2/http-redirect/slo/676087
            'singleLogoutService' => array (
                'url' => 'https://realtyonegroup.onelogin.com/trust/saml2/http-redirect/slo/676087',
            ),
            // Dev -
            // MIIEIzCCAwugAwIBAgIURyZE67/skro+48oniNIxNsES07IwDQYJKoZIhvcNAQEF
            // BQAwXDELMAkGA1UEBhMCVVMxFDASBgNVBAoMC1NwYWNpbyBJbmMuMRUwEwYDVQQL
            // DAxPbmVMb2dpbiBJZFAxIDAeBgNVBAMMF09uZUxvZ2luIEFjY291bnQgMTA5Mjk3
            // MB4XDTE3MDYyMjE5MDkzM1oXDTIyMDYyMzE5MDkzM1owXDELMAkGA1UEBhMCVVMx
            // FDASBgNVBAoMC1NwYWNpbyBJbmMuMRUwEwYDVQQLDAxPbmVMb2dpbiBJZFAxIDAe
            // BgNVBAMMF09uZUxvZ2luIEFjY291bnQgMTA5Mjk3MIIBIjANBgkqhkiG9w0BAQEF
            // AAOCAQ8AMIIBCgKCAQEAyXeAOhZNrMGr3DYYA4EGUgzM8l8ktdudaKWdElTFOKg8
            // dcsATZ9YeUfm2U6HGbSryVzbWq7y5n80ItUh/JOsINJxDMp7bve/klCb9A2sYOSp
            // 9J5ZPmiPh9//hyOwsFAzafu9KpgYGfcGwId6rFA9Yjx19u28IH1fVIBHz47Uwx1M
            // oUhP5PeBiAYGeH7hp3vprboi4CFywHEzIcdM2Pf/32c34kTIkNIbs3njBnPDtqo1
            // A01Sgm8IbfHQgkQb5AeCglRRff7qEPdvbt7LV8+Fqy4jp5M7N66NdXZvG7jMH5hj
            // 98srBYIo0ift1LIRln0FELXEJAjlp/+g+K37MAjCQQIDAQABo4HcMIHZMAwGA1Ud
            // EwEB/wQCMAAwHQYDVR0OBBYEFKbk2O4zyIBBGac73dyJuPJzRo7JMIGZBgNVHSME
            // gZEwgY6AFKbk2O4zyIBBGac73dyJuPJzRo7JoWCkXjBcMQswCQYDVQQGEwJVUzEU
            // MBIGA1UECgwLU3BhY2lvIEluYy4xFTATBgNVBAsMDE9uZUxvZ2luIElkUDEgMB4G
            // A1UEAwwXT25lTG9naW4gQWNjb3VudCAxMDkyOTeCFEcmROu/7JK6PuPKJ4jSMTbB
            // EtOyMA4GA1UdDwEB/wQEAwIHgDANBgkqhkiG9w0BAQUFAAOCAQEAdoHGybnKStLT
            // 3+S4XGcmeS2RG+Yo4kyI8jS6dm3P9QFIWBVAweTLDUR7V/YMTUj9jzW8w4DyLkmc
            // 7dwFauS9DT/zL1W/Pb0ZNkMI0hhy8I9fYei8DXr8sSty3Oji1tIjLU2P1hOTvbVv
            // ybYdxmZmk5C2bIi7aSX5owuyyAYG+LkEK5L3LqWxrJBYkrLfznCqYn00YdbeYh7t
            // eW9ZeTBwpHRpX2JCA8O2qeXP8+93hzwRfwZtpKO5y8zrwmLrp27DYaQv6lheUro5
            // GF5zPd9PJ0o8Att7wWEC2V9FySrvsWe4j5W8M0DVusTSzm6Ay84Ya7httezyo2GT
            // WhCQ0Sc6wA==
            // Prod -
            // MIIELzCCAxegAwIBAgIUAPpsMnn1cKZHkrLCHj11YgYsp7owDQYJKoZIhvcNAQEF
            // BQAwYDELMAkGA1UEBhMCVVMxGTAXBgNVBAoMEFJlYWx0eSBPTkUgR3JvdXAxFTAT
            // BgNVBAsMDE9uZUxvZ2luIElkUDEfMB0GA1UEAwwWT25lTG9naW4gQWNjb3VudCA5
            // ODAxMzAeFw0xNjEyMTUyMTQ2NTVaFw0yMTEyMTYyMTQ2NTVaMGAxCzAJBgNVBAYT
            // AlVTMRkwFwYDVQQKDBBSZWFsdHkgT05FIEdyb3VwMRUwEwYDVQQLDAxPbmVMb2dp
            // biBJZFAxHzAdBgNVBAMMFk9uZUxvZ2luIEFjY291bnQgOTgwMTMwggEiMA0GCSqG
            // SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDPnS6tCBXc+HCeXxYQJb4g5nICoGKjDUlc
            // gSR7kweb2cpp/3wLT2EF5aNwyxFe32F3E8kpXbKVE3jPfvd+mn1po46LdFQcvINn
            // /b7ajbMYCsaPVakLlQwyHlciz5PPDX2PhlMGnyL7RC55FHZHtW8h82y7VxeB7A1r
            // AMxtck9bxBGFxcbhsf6JDvXxriHxtKJghhMp2MbqBnmg9qEm6bpJyoDk5HJJUMzk
            // 8EHp/X4Anke/DxDY425XCzK3B5l0rrja02OgAihmVyjlI81NvYwKYq7DZ9MsiR+u
            // gtQOShoxoP0z1R3Pbxv8OJL24m2ZQrS9uRN8pBdUexP4TAA20cYlAgMBAAGjgeAw
            // gd0wDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUQBq8Jein+2VtIcHxzU/3Gj1WFXEw
            // gZ0GA1UdIwSBlTCBkoAUQBq8Jein+2VtIcHxzU/3Gj1WFXGhZKRiMGAxCzAJBgNV
            // BAYTAlVTMRkwFwYDVQQKDBBSZWFsdHkgT05FIEdyb3VwMRUwEwYDVQQLDAxPbmVM
            // b2dpbiBJZFAxHzAdBgNVBAMMFk9uZUxvZ2luIEFjY291bnQgOTgwMTOCFAD6bDJ5
            // 9XCmR5Kywh49dWIGLKe6MA4GA1UdDwEB/wQEAwIHgDANBgkqhkiG9w0BAQUFAAOC
            // AQEAKw/x7SD0ofn7iZKG1+muquBlKT7iWN2tQBRZL3qKUQYxY05L1PBRkxdEzrRJ
            // FU6PG4YUKHGddcMVsAQYl6Si14SpDayKD7CqvR5Ab/x7nKmLxrAoKU+ckfJWjlWd
            // hkGsx7F2nMwyBbiQm5ly9fg3Sl4iKuwZu/9IZr0LedyPy6UufMoOH/XRKTYAc14w
            // 9Qn7VmdWIR8Gfh6NuSY1Gd3SNW+VJPglQZLQTcWSO4TP8puM/xdrv42f9AzAUXPd
            // ZnG4LjRwvSkkngVELxF6uLpaTDZL/fLk0cxssoPkQRE5Jwt6sAdmz1APTdT4E/gL
            // z+NySsc55S23X+ydSANEBITrbg==
            'x509cert' => '-----BEGIN CERTIFICATE-----
            MIIELzCCAxegAwIBAgIUAPpsMnn1cKZHkrLCHj11YgYsp7owDQYJKoZIhvcNAQEF
            BQAwYDELMAkGA1UEBhMCVVMxGTAXBgNVBAoMEFJlYWx0eSBPTkUgR3JvdXAxFTAT
            BgNVBAsMDE9uZUxvZ2luIElkUDEfMB0GA1UEAwwWT25lTG9naW4gQWNjb3VudCA5
            ODAxMzAeFw0xNjEyMTUyMTQ2NTVaFw0yMTEyMTYyMTQ2NTVaMGAxCzAJBgNVBAYT
            AlVTMRkwFwYDVQQKDBBSZWFsdHkgT05FIEdyb3VwMRUwEwYDVQQLDAxPbmVMb2dp
            biBJZFAxHzAdBgNVBAMMFk9uZUxvZ2luIEFjY291bnQgOTgwMTMwggEiMA0GCSqG
            SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDPnS6tCBXc+HCeXxYQJb4g5nICoGKjDUlc
            gSR7kweb2cpp/3wLT2EF5aNwyxFe32F3E8kpXbKVE3jPfvd+mn1po46LdFQcvINn
            /b7ajbMYCsaPVakLlQwyHlciz5PPDX2PhlMGnyL7RC55FHZHtW8h82y7VxeB7A1r
            AMxtck9bxBGFxcbhsf6JDvXxriHxtKJghhMp2MbqBnmg9qEm6bpJyoDk5HJJUMzk
            8EHp/X4Anke/DxDY425XCzK3B5l0rrja02OgAihmVyjlI81NvYwKYq7DZ9MsiR+u
            gtQOShoxoP0z1R3Pbxv8OJL24m2ZQrS9uRN8pBdUexP4TAA20cYlAgMBAAGjgeAw
            gd0wDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUQBq8Jein+2VtIcHxzU/3Gj1WFXEw
            gZ0GA1UdIwSBlTCBkoAUQBq8Jein+2VtIcHxzU/3Gj1WFXGhZKRiMGAxCzAJBgNV
            BAYTAlVTMRkwFwYDVQQKDBBSZWFsdHkgT05FIEdyb3VwMRUwEwYDVQQLDAxPbmVM
            b2dpbiBJZFAxHzAdBgNVBAMMFk9uZUxvZ2luIEFjY291bnQgOTgwMTOCFAD6bDJ5
            9XCmR5Kywh49dWIGLKe6MA4GA1UdDwEB/wQEAwIHgDANBgkqhkiG9w0BAQUFAAOC
            AQEAKw/x7SD0ofn7iZKG1+muquBlKT7iWN2tQBRZL3qKUQYxY05L1PBRkxdEzrRJ
            FU6PG4YUKHGddcMVsAQYl6Si14SpDayKD7CqvR5Ab/x7nKmLxrAoKU+ckfJWjlWd
            hkGsx7F2nMwyBbiQm5ly9fg3Sl4iKuwZu/9IZr0LedyPy6UufMoOH/XRKTYAc14w
            9Qn7VmdWIR8Gfh6NuSY1Gd3SNW+VJPglQZLQTcWSO4TP8puM/xdrv42f9AzAUXPd
            ZnG4LjRwvSkkngVELxF6uLpaTDZL/fLk0cxssoPkQRE5Jwt6sAdmz1APTdT4E/gL
            z+NySsc55S23X+ydSANEBITrbg==
            -----END CERTIFICATE-----
            ',
        ),
    );
