function showAccountPanel() {
    closeAllPanels();
    $("#account-menu-item").addClass("selected");
    $("#account-panel").show();
    curPanel = "account";
    showAccountTab("profile");
}

function showAccountTab(tab) {
    updateLastActive();

    if (curAccountTab == tab) {

    } else {

        $("#account-"+curAccountTab+"-tab").hide();
        $("#account-"+tab+"-tab").show();
        curAccountTab = tab;
        $(".tab").removeClass("selected").addClass("unselected");
        $("#account-"+tab+"-tab-button").removeClass("unselected").addClass("selected");

        if (tab == "profile") {
            updateLastActive();
            console.log("show profile panel");
            $(".sub-container").hide();
            $("#account-panel").show();
            //$(".menu-item").removeClass("selected").addClass("not-selected");
            //$("#account-menu-item").removeClass("not-selected").addClass("selected");
            curPanel = "account";
            $(".account-email").html(user["email"]);
            $(".account-status").html("Active");
            $("#edit-spacio-profile").val("https://spac.io/p/"+user["ukey"]+"");

            if (user["type"] == "trial") {
                $(".upgrade-button").show();
                $(".downgrade-button").hide();
            }

            if (user["type"] == "freemium" || user["type"] == "lite") {
                $(".account-plan").html("Lite");
                $(".upgrade-button").show();
                $(".downgrade-button").hide();
            }

            if (user["type"] == "standard") {
                $(".account-plan").html("Premium");

                if (accountCancelled) {
                    $(".upgrade-button").hide();
                    $(".downgrade-button").hide();
                    $(".account-status").html("Cancelled");
                } else {
                    $(".upgrade-button").hide();
                    $(".downgrade-button").show();
                }
            }

            if (user["type"] == "brokerage") {
                $(".account-plan").html("Brokerage");
                $(".downgrade-button").hide();
            }

            if (user["type"] == "free") {
                $(".account-plan").html("Platinum (Free with Full Features)");
                $(".upgrade-button").hide();
                $(".downgrade-button").hide();
            }


            $(".account-promo").html(user["promocode"]);

            if (user["promocode"] != "N/A" && user["promocode"] != "") {
                $("input[name='promocode']").val(user["promocode"]);
            }

            if (user["brand"] != "N/A") {
                $("input[name='corpcode']").val(user["brand"]);
            }
        }

        if(tab == "plan") {

            curPanel = "plan";
            $(".account-email").html(user["email"]);
            $(".account-status").html("Active");

            if (user["type"] == "trial") {
                $(".account-plan").html("Trial (30-day unlimited-use FREE trial)");
                $(".upgrade-button").show();
                $(".downgrade-button").hide();
            }

            if (user["type"] == "freemium" || user["type"] == "lite") {
                $(".account-plan").html("Lite");
                $(".upgrade-button").show();
                $(".downgrade-button").hide();
            }

            if (user["type"] == "standard") {
                $(".account-plan").html("Premium");

                if (accountCancelled) {
                    $(".upgrade-button").hide();
                    $(".downgrade-button").hide();
                    $(".account-status").html("Cancelled");
                } else {
                    $(".upgrade-button").hide();
                    $(".downgrade-button").show();
                }
                getManagerPaymentInfo();
                $(".payment-info").show();
            }

            if (user["type"] == "brokerage") {
                $(".account-plan").html("Brokerage");
                $(".downgrade-button").hide();

                if(user["brokerageID"] == "lnf") {
                  getManagerPaymentInfo();
                  $(".account-plan").html("Brokerage Opt-In");
                  $(".payment-info").show();
                  $(".downgrade-button").show();

                }
            }

            if (user["type"] == "free") {
                $(".account-plan").html("Platinum (Free with Full Features)");
                $(".upgrade-button").hide();
                $(".downgrade-button").hide();
            }

            $(".account-promo").html(user["promocode"]);

            if (user["promocode"] != "N/A" && user["promocode"] != "") {
                $("input[name='promocode']").val(user["promocode"]);
            }

            if (user["brand"] != "N/A") {
                $("input[name='corpcode']").val(user["brand"]);
            }

         }

        if (tab == "leadscoring") {

            loadScoring();

        }

    }
}

function confirmDowngradeAccount() {
	showAlert("Downgrade Plan","Are you sure you want to downgrade your plan? You will have access to Spacio  until the last day of your current billing cycle.","Downgrade","Cancel",function(){  $(".alert-overlay").hide();downgradeAccount();},function(){ $(".alert-overlay").hide();	 });
}
