<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_package = isset($_GET['pk']) ? $_GET['pk'] : 'agent';
	$_referred = isset($_GET['r']) ? $_GET['r'] : '';
	$_source = isset($_GET['s']) ? $_GET['s'] : 'none';
	$_id = isset($_GET['u']) ? $_GET['u'] : '';
	$_countdown = isset($_GET['c']) ? $_GET['c'] : 'NO';
	$_email = isset($_GET['em']) ? $_GET['em'] : '';
	$_fname = isset($_GET['fn']) ? $_GET['fn'] : '';
	$_lname = isset($_GET['ln']) ? $_GET['ln'] : '';
	$_phone = isset($_GET['tl']) ? $_GET['tl'] : '';
	$_mobile = isset($_GET['mp']) ? $_GET['mp'] : '';
	$_pphoto = isset($_GET['pp']) ? $_GET['pp'] : '';
	$_pphoto = urldecode($_pphoto);

?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Introducing the Paperless Open House</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
	 <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
    <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="signup-scroll-one" style="width:100%;background:#3e3e3e;">
      <div class="overlay-flex" style="position:relative;background:transparent;">
        <form id="registerManager_Form" action="index.php" method="POST">
					<div style="height:60px"></div>
          <input type="hidden" name="fn" value="registerManagerTrial" />
          <div class="xlarge" style="text-align:center;margin-bottom:10px;">
              Sign up for Spacio
          </div>
          <div class="normal" style="text-align:center;margin-bottom:20px;line-height:1.3;" id="custom-message">
            Get started now for a 30-day unlimited-use FREE trial.
          </div>
					<div class="" style="text-align:center;display:none;margin-top:20px;font-size:20px;margin-bottom:20px;">
						LIMITED TIME OFFER! SIGN UP NOW!
					</div>
					<div id="countdown" style="display:none;height:40px;font-size:40px;margin-bottom:20px;text-align:center;">

					</div>
					<div class="signup-input-box">
            <div class="signup-label">
              PACKAGE
            </div>
						<select class="signup-select" name="package" onchange="packageChanged()">
							<option value="agent">Spacio for Agents</option>
							<option value="team">Spacio for Teams</option>
						</select>
          </div>
					<div class="signup-input-box teamname-input-box" style="display:none;">
            <div class="signup-label">
              TEAM NAME
            </div>
            <input class="signup-input" type="text" name="team" value="" id="team" tabindex="1"  placeholder="">
          </div>
          <div class="signup-input-box">
            <div class="signup-label">
              FIRST NAME
            </div>
            <input class="signup-input" type="text" name="fname" value="<?php echo $_fname;?>" id="fname" tabindex="1"  placeholder="">
          </div>
          <div class="signup-input-box">
            <div class="signup-label">
              LAST NAME
            </div>
            <input class="signup-input" type="text" name="lname" value="<?php echo $_lname;?>" id="lname" tabindex="2"  placeholder="">
          </div>
          <div class="signup-input-box">
            <div class="signup-label">
              EMAIL
            </div>
            <input class="signup-input" type="email" name="email" value="<?php echo $_email;?>" id="email" tabindex="3"  placeholder="">
          </div>
					<div class="signup-input-box phonenumber-input-box" style="display:none;">
            <div class="signup-label">
              PHONE
            </div>
						<input class="signup-input" type="number" name="phone" value="<?php echo $_phone?>" id="phone" tabindex="4" placeholder="">
          </div>
          <div class="signup-input-box">
            <div class="signup-label">
              PASSWORD
            </div>
            <input class="signup-input" type="password" name="pw" value="" id="pw" tabindex="5" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label">
              REPEAT PASSWORD
            </div>
            <input class="signup-input" type="password" name="pw2" value="" id="pw2" tabindex="6" placeholder="">
          </div>


          <!-- <div><input type="hidden" name="phone" value="<?php echo $_phone?>" id="phone" placeholder="Phone"></div>
          <div><input type="hidden" name="mobile" value="<?php echo $_mobile?>" id="phone" placeholder="Phone"></div> -->
          <div><input type="hidden" name="website" value="" id="website" placeholder="Website"></div>
          <div><input type="hidden" name="brokerage" value="" id="brokerage" placeholder="Brokerage"></div>
          <div><input type="hidden" name="type" value="trial" id="type" placeholder="Type"></div>
          <div><input type="hidden" name="pphoto" value="<?php echo $_pphoto?>" id="pphoto"></div>
          <div><input type="hidden" name="referred" value="<?php echo $_referred?>" id="referred"></div>
          <input type="hidden" name="promo" value="" id="promo" tabindex="6" placeholder="Promo Code">

          <div style="width:100%;text-align:right">
            <input class="signup-submit-btn-orange" type="submit" name="signup" value="Sign Up">
          </div>
					  <div style="width:100%;text-align:center;margin-top:20px;">
					If your company belongs to our brokerage plan, please click <a href="/claimemail/"><u>HERE</u></a> to claim your account.
					</div>
        </form>

				<div style="position:absolute;bottom:10px;height:20px;text-align:center;font-size:12px;color:white;">
					By clicking “Sign Up” you agree to the <a href="/terms" target="_blank"><u>Terms</u></a> and <a href="/privacy" target="_blank"><u>Privacy Policy</u></a>.
				</div>
      </div>

    </div>
    <!-- SCROLL ONE END -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
		<script src="/assets/js/jquery.countdown.min.js"></script>
    <script>
		var global_referred = "<?php echo $_referred;?>";
		var countdown = "<?php echo $_countdown;?>";
		var campaign_id = "<?php echo $_id;?>";
		var source = "<?php echo $_source;?>";
		var pack = "<?php echo $_package;?>";


    $(document).ready(function(){

			console.log(global_referred);

			if (source == "campaign") {
				trackCampaign(campaign_id);
			}
      insertAlertBox();

			$("#"+global_referred+"").show();
  		if(global_referred == "TopProducer") {
  			$(".TopProducer").show();
  			$(".normal-msg").hide();

  		}
  		if(global_referred == "OLR") {
  			$(".OLR").show();
  			$(".normal-msg").hide();

  		}
  		if(global_referred == "CRMLS") {
  			$(".CRMLS").show();
  			$(".normal-msg").hide();
				$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.<br/>25% off for all CRMLS Members who upgrade.");

  		}
			if(global_referred == "CloudCMA" || global_referred == "cloudcma") {
				global_referred = "";
  			$(".CloudCMA").show();
  			$(".normal-msg").hide();
				//$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.");

				//$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.<br/>Exclusive offer for all Cloud CMA webinar attendees who upgrade.<br/><span style='font-family:effra_medium;'>25% off</span> for monthly plan at $18.75 or <span style='font-family:effra_medium;'>35% off</span> for yearly plan at $195 ($105 savings!).");
				$("#countdown").hide();
				/*
				$("#countdown").countdown(1490594400000, function(event) {
			    $(this).text(
			      event.strftime('%D days %H:%M:%S')
			    );
			  });
				*/
  		}
			if(global_referred == "bhhs") {
  			$(".normal-msg").hide();
				$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.<br/><br/><span class='bold'>Exclusive Offer</span>: 15% off monthly plan and 25% off yearly plan for all Berkshire Hathaway HomeServices agents who upgrade. That’s $75 in savings if you upgrade for the year! Only $225/year to access all of Spacio’s features.</b>");

  		}

			if(global_referred == "adwerx") {
  			$(".normal-msg").hide();
				$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.<br/><br/><span class='bold'>Limited time offer</span> for all Adwerx customers who upgrade before trial ends! 25% off for yearly plan at $225 ($75 savings!).</b>");
				$("#countdown").show();

				$("#countdown").countdown(1499189349000, function(event) {
			    $(this).text(
			      event.strftime('%D days %H:%M:%S')
			    );
			  });

  		}


			if(global_referred == "kyle") {

  			$(".normal-msg").hide();
				$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.<br/><br/><span class='bold'>LIMITED TIME OFFER</span>: <span class='bold'>30% off</span> when you upgrade for the year! That's <span class='bold'>$90 in savings</span>, and only $210 for the year to have full access to all of Spacio's features!");
				$("#countdown").show();

				var timeStamp = Math.floor(Date.now());

				if(timeStamp < 1492671569000) {
					$("#countdown").countdown(1492671569000, function(event) {
				    $(this).text(
				      event.strftime('%D days %H:%M:%S')
				    );
				  });
				} else {
					global_referred = "";
					$("#countdown").hide();
					$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.<br/><br/><span class='bold'>OFFER EXPIRED</span>: <span class='bold'>30% off</span> when you upgrade for the year! That's <span class='bold'>$90 in savings</span>, and only $210 for the year to have full access to all of Spacio's features!");

				}

  		}

			if(global_referred == "RealtyNinja") {
  			$(".CRMLS").show();
  			$(".normal-msg").hide();
				$("#custom-message").html(" Get started now for a 30-day unlimited-use FREE trial.<br/><br/><b>Exclusive Offer: 15% off for all RealtyNinja agents who upgrade.</b>");

  		}
  		if(global_referred == "Contactually") {
  			$(".Contactually").show();
  			$(".normal-msg").hide();

  		}
  		if(global_referred == "RobVivian") {
  			$(".RobVivian").show();
  			$(".normal-msg").hide();

  		}

			if(pack == "team") {
				$("select[name='package']").val("team");
				$(".teamname-input-box").show();
				$(".phonenumber-input-box").show();
			} else {
				$("select[name='package']").val("agent");
				$(".teamname-input-box").hide();
				$(".phonenumber-input-box").hide();
			}

    });

		function packageChanged() {
			pack = $("select[name='package']").val();
			if(pack == "team") {
				$(".teamname-input-box").show();
				$(".phonenumber-input-box").show();
			} else {
				$(".teamname-input-box").hide();
				$(".phonenumber-input-box").hide();

			}

		}
    $( "form#registerManager_Form" ).submit(function( event ) {
			//console.log(global_referred);
		//Serialize form inputs
		var _fields = $( this ).serializeArray();
		var _query = {};

		//Format form inputs into proper array
		$.each(_fields, function(index, value) {
				_query[value["name"]] = value["value"];
		})

		//console.log(global_referred);
		_query["email"] = _query["email"].toLowerCase();
		//Validate form inputs.
		if (_query["email"] == "" || _query["email"] == null) {
			showAlert("error","Invalid Email Format","Please enter a valid email.");
		} else if (_query["package"] == "team" && _query["phone"] == "") {
			showAlert("error", "No Phone Number", "Please enter a phone number")
		} else {

			var _query2 = {};
			_query2["fn"] = "getClaimLink";
			_query2["email"] = _query["email"];


			var json_data = _query2;
			console.log("Outbound JSON Query: "+JSON.stringify(json_data));
			$.ajax({
				url: "../webservice_test/1.1.6/webservice.php",
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					var url = data.url;

					if (data.status == 1) {
						showAlert("info","Claim Account","Your email is part of a Brokerage plan, please claim your account.","Claim My Account",null,function(){window.location.href=url},null);
					} else if (data.status == 2) {
						showAlert("error","Email Already Registered","This email has already been registered with Spacio. Please login .","Login","Cancel", function(){
									window.location = "../dashboard/";
									}, null);
					} else if (data.status == 3) {
						showAlert("error","Brokerage Plan","Your company is part of our Brokerage Solution. We will setup your account and be in touch with you shortly with login instructions.","Ok");
					} else {

						if(_query["pw"] == "") {
							showAlert("error","No Password","Please enter a password.");
						} else if(_query["pw"].length < 6) {

							showAlert("error","Invalid Password","Passwords must be at least 6 characters.");
						} else if(_query["pw"] != _query["pw2"]) {

							showAlert("error","Passwords Do Not Match","Please enter matching passwords.");
						} else {
							//deletes the "pw2" value so it never leaves the client side
							delete _query["pw2"];

							//Salt + Hash password before sending to server
							var _salt = randomString(16);
							_query["salt"] = _salt;
							_query["pw"] += _salt;
							_query["pw"] = CryptoJS.SHA3(_query["pw"], { outputLength: 224 }).toString();
							//_query["referred"] = "";
							//console.log(global_referred);
							_query["referred"] = global_referred;
							_query["package"] = pack;

							var json_data = _query;
							console.log("Outbound JSON Query: "+JSON.stringify(json_data));
							$.ajax({
								url: "../oh/webservice/1.1.6/webservice.php",
								type:"POST",
								data: {json : json_data},
								success: function(data, textStatus, jqXHR){
									console.log("Returned JSON Object: "+JSON.stringify(data));

									if (data.status == 1) {
										showAlert("info","Thank you for Signing Up","Please confirm your email before logging in.","OK",null,function(){window.location.href="../dashboard/"},null);
									} else if (data.status == 2) {
										showAlert("error","Email Already Registered","This email has already been registered with Spacio. Please login .","Login","Cancel", function(){
										window.location = "../dashboard/";
										}, null);
									} else if (data.status == 3) {
										var url = data.url;
										showAlert("error","Claim Account","Your email already exists, please claim your account.","Claim My Account",null,function(){window.location.href=url},null);
									} else {
										showAlert("error","Unknowns","There was an unknown error. Please contact support.");
									}

									//debugLog("Query Status: "+data.status);
								},
								error: function(jqXHR, textStatus, errorThrown) {
									console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
								},
								dataType: "json"
							});
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
				},
				dataType: "json"
			});

		}
		event.preventDefault();
	});


	function trackCampaign(id) {
		var _query = {};
		_query["fn"] = "trackCampaign";
		_query["id"] = id;

		var json_data = _query;

		$.ajax({
			url: "../oh/webservice/1.1.6/webservice.php",
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});

	}



    </script>
  </body>
</html>
