<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

  	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  	header("Cache-Control: post-check=0, pre-check=0", false);
  	header("Pragma: no-cache");

  	$_email = $_GET["e"];
  	$_id = $_GET["u"];
  	$_from = isset($_GET['f']) ? $_GET['f'] : "";
  	$_nb = isset($_GET['nb']) ? $_GET['nb'] : 0;


?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Get Claim Email</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
     <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="signup-scroll-one" style="background:none;padding:0px;">
      <div class="overlay" style="display:block;">
        <form id="claimEmail_Form" action="index.php" method="POST" style="height:auto;max-width:500px;">
          <input type="hidden" name="fn" value="emailClaimLink" />
          <div class="xlarge" style="text-align:center;margin-bottom:20px;">
              <img src="/assets/img/website/lnf_horizontal_white.png" height="40" class="non-mobile-logo" style="margin:0 auto;">
              <img src="/assets/img/website/lnf_vertical_white.png" width="80%" class="mobile-logo">
              <br/>
              Claim Your Spacio Account
          </div>
          <!-- <div class="normal" style="text-align:center;margin-bottom:20px;line-height:1.3;">
            First access to Spacio is available to Recharge attendees only.<br/>Please enter your email to receive a claim link.
          </div> -->
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:65px;">
              EMAIL
            </div>
            <input class="signup-input" style="" type="text" name="email" value="<?php echo $_email ?>" id="email" tabindex="1" placeholder="" >
          </div>

          <div style="width:100%;text-align:right">
            <input class="signup-submit-btn-orange" type="submit" name="signup" value="SEND CLAIM LINK">
          </div>

        <div class="normal" style="width:100%;margin-top:50px;text-align:left;">
          <a href="https://spac.io"><p style="font-family:effra_medium"><b><u>ABOUT SPACIO</u></b></p></a>
Spacio helps you monetize your open houses and maximize lead generation opportunities by automating lead capture and follow up. Our feature rich platform also helps you pull social profiles on verified contacts, generate seller reports to keep clients informed, and integrates with your favorite solutions such as <a href="http://spac.io/partners/moxiworks/" target="_blank"/><u>Moxi Works</u></a>, <a href="http://spac.io/partners/testimonialtree/" target="_blank"/><u>Testimonial Tree</u></a>, <a href="http://spac.io/partners/adwerx/" target="_blank"/><u>Adwerx</u></a>, and more! Learn more about the industry leading solutions we integrate with on our  <a href="http://spac.io/partners/" target="_blank"/><u>partners page</u></a>.
          </div>

        </form>
      </div>

    </div>
    <!-- SCROLL ONE END -->
    <div style="padding:20px 20px;width:100%;background:white;text-align:center;box-sizing:border-box;">
      Please view our <a href="/terms" target="_blank"><u>Terms</u></a> and <a href="/privacy" target="_blank"><u>Privacy Policy</u></a>.

    </div>
    <style>
    @media (min-width: 901px) {
      .mobile-logo {
        display:none;
      }

      .non-mobile-logo {
        display:block;
      }
    }

    @media (max-width: 900px) {
      .mobile-logo {
        display:block;
        margin:0 auto;
      }

      .non-mobile-logo {
        display:none;
      }
    }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
    <script>
    var email = "<?php echo $_email ?>";
    var wsURL = "../oh/webservice/1.1.3/webservice.php";

    $(document).ready(function(){
      email = email.replace(/\s+/g, '+').toLowerCase();
      insertAlertBox();

    });

    $( "form#claimEmail_Form" ).submit(function( event ) {

		//Serialize form inputs
		var _fields = $( this ).serializeArray();
		var _query = {};

		//Format form inputs into proper array
		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})

		_query["email"] = _query["email"].toLowerCase();
		//Validate form inputs.
		if (_query["email"] == "" || _query["email"] == null) {
			showAlert("error","Invalid Email Format","Please enter a valid email.");
		} else {

			//Clean up query object and send
			loading(true,"Getting Claim Link...");
			//Salt + Hash password before sending to server

			var json_data = _query;
			console.log("Outbound JSON Query: "+JSON.stringify(json_data));
			$.ajax({
				url: "http://spac.io/oh/webservice/1.1.6/webservice.php",
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {
						showAlert("info","Claim Link Sent","Please check your email to claim your Spacio account.","OK");
            $( "form#claimEmail_Form" )[0].reset();
					} else {
						showAlert("error","Email Not Recognized","Please contact support@spac.io.");
					}
					loading(false);
					//debugLog("Query Status: "+data.status);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Link Error: "+errorThrown);
				},
				dataType: "json"
			});
		}
		event.preventDefault();
	});

    </script>
  </body>
</html>
