<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$reportID = isset( $_GET['r'] ) ? $_GET['r'] : "";
?>

<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Report</title>
    <link rel="stylesheet" href="js/trumbowyg/custom/trumbowyg.css">
    <link href="js/jquery-ui.min.css" rel="stylesheet">
    <link href="js/jquery.comiseo.daterangepicker.css" rel="stylesheet">
    <link href="css/report.css" rel="stylesheet">

    <link href="fonts/fonts.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
  </head>

  <body>

    <div class="loading-container">
      <div class="loading-content">
        <div class="wrapper" data-anim="base wrapper">
          <!--<div class="circle-right" data-anim="base right"></div>-->
          <div class="circle-one" data-anim="base"></div>
          <div class="circle-two" data-anim="base"></div>
          <div class="loading-msg"></div>
        </div>
      </div>
    </div>

    <div class="report-main-container">
      <div class="report-logo-container">
        <img class="desktop-logo" src="" height="100%">
        <img class="mobile-logo"  src="" width="75%">
      </div>

      <div class="report-title-container">
        <div  class="reportType"></div>
        <div  class="report-daterange"></div>
        <div class="agent-profile-photo"></div>
        <div class="agent-information"></div>
      </div>
      <div class="report-property-container">
        <div class="property-image">

        </div>
        <div class="property-info" style="">
          <div class="property-title"></div>
          <div class="property-addr"></div>
        </div>
      </div>

      <div class="reporting-sub-container-scroll">
        <img src="img/square_eee.jpg" class="background-print-color">
        <div class="data-panel">
          <div class="data-row properties-row"> <!-- PROPERTIES ROW -->
            <div class="left">
              <div class="stat-category">LISTINGS</div>
              <div class="stat-total main-color-font total-properties-num">--</div>
              <div class="medium">Total Listings</div>
            </div>
            <div class="right">
              <img src="img/square_fff.jpg" class="background-print-color">
              <div class="stat-row" >
                  <div class="stat-subcategory properties-status">
                      STATUS
                      <div class="toggle-button" onclick="toggleSubcategory('properties-status')">HIDE</div>
                  </div>
              </div>
              <div class="stat-row properties-status-row" >
                  <div class="stat-number main-color-font active-listings-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Active Listings</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background active-listings-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font active-listings-percentage"></div>
                  </div>
              </div>
              <div class="stat-row properties-status-row" >
                  <div class="stat-number main-color-font inactive-listings-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Inactive Listings</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background inactive-listings-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font inactive-listings-percentage"></div>
                  </div>
              </div>
            </div>
          </div> <!-- PROPERTIES ROW END -->
          <div class="data-row"> <!-- LEADS ROW -->
            <div class="left" style="width:200px;">
              <div class="stat-category">VISITORS</div>
              <div class="stat-total total-visitors-num">--</div>
              <div class="medium">Total Visitors</div>
            </div>
            <div class="right">
              <img src="img/square_fff.jpg" class="background-print-color">
              <!-- VISITORS QUALITY  -->
              <div class="stat-row">
                  <div class="stat-subcategory visitor-quality">
                      QUALITY
                      <div class="toggle-button" onclick="toggleSubcategory('visitor-quality')">HIDE</div>
                  </div>
              </div>
              <div class="stat-row visitor-quality-row" >
                  <div class="stat-number main-color-font with-contact-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">With Contact Info</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background with-contact-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font with-contact-percentage"></div>
                  </div>
              </div>
              <div class="stat-row visitor-quality-row" >
                  <div class="stat-number main-color-font without-contact-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Without Contact Info</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background without-contact-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font without-contact-percentage"></div>
                  </div>
              </div>
              <div class="stat-row visitor-quality-row" style="border-bottom:1px solid #f2f2f2;">
                  <div class="stat-number main-color-font verified-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Verified</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background verified-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font verified-percentage"></div>
                  </div>
              </div> <!-- VISITORS QUALITY END -->

              <div class="stat-row"> <!-- VISITORS REPRESENTATION -->
                  <div class="stat-subcategory visitor-representation">
                      REPRESENTATION
                      <div class="toggle-button" onclick="toggleSubcategory('visitor-representation')">HIDE</div>
                  </div>
              </div>
              <div class="stat-row visitor-representation-row" >
                  <div class="stat-number main-color-font with-representation-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">With Representation</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background with-representation-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font with-representation-percentage"></div>
                  </div>
              </div>
              <div class="stat-row visitor-representation-row" >
                  <div class="stat-number main-color-font without-representation-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Without Representation</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background without-representation-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font without-representation-percentage"></div>
                  </div>
              </div>
              <div class="stat-row visitor-representation-row" style="border-bottom:1px solid #f2f2f2;">
                  <div class="stat-number main-color-font unknown-representation-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Unknown</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background unknown-representation-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font unknown-representation-percentage"></div>
                  </div>
              </div><!-- VISITORS REPRESENTATION END -->
              <div class="stat-row"><!-- VISITORS FINANCING END -->
                  <div class="stat-subcategory visitor-financing">
                      FINANCING
                      <div class="toggle-button" onclick="toggleSubcategory('visitor-financing')">HIDE</div>
                  </div>
              </div>
              <div class="stat-row visitor-financing-row" >
                  <div class="stat-number main-color-font with-financing-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">With Financing</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background with-financing-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font with-financing-percentage"></div>
                  </div>
              </div>
              <div class="stat-row visitor-financing-row" >
                  <div class="stat-number main-color-font without-financing-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Without Financing</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background without-financing-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font without-financing-percentage"></div>
                  </div>
              </div>
              <div class="stat-row visitor-financing-row" >
                  <div class="stat-number main-color-font unknown-financing-num"></div>
                  <div class="stat-percentage-container">
                    <div class="stat-desc main-color-font">Unknown</div>
                    <div class="percentage-bar">
                      <img src="img/square_eee.jpg" class="background-print-color">
                      <div class="percentage-fill main-color-background unknown-financing-fill">
                        <img src="img/square_000.jpg" class="background-print-color">
                      </div>
                    </div>
                    <div class="stat-percentage main-color-font unknown-financing-percentage"></div>
                  </div>
              </div>
            </div>
          </div> <!-- VISITORS ROW END -->
          <div class="data-row"> <!-- OH ROW -->
            <div class="left">
              <div class="stat-category">OPEN HOUSES</div>
              <div class="stat-total total-ohs-num">--</div>
              <div class="medium">Total Open Houses</div>
            </div>
            <div class="right">
              <img src="img/square_fff.jpg" class="background-print-color">
              <div style="position:relative;z-index:20">
                <div class="stat-subcategory rankings-subcategory">RANKINGS</div>
                <div class="oh-headers all-headers">
                  <div class="title-column">Property</div>
                  <div class="visitors-column">Visitors</div>
                  <div class="productivity-column">Visitors/OH</div>
                </div>
                <div class="oh-headers single-headers">
                  <div class="title-column">Date Hosted</div>
                  <div class="visitors-column">Visitors</div>
                  <div class="delete-column"></div>
                </div>
                <div class="oh-rankings">
                </div>
              </div>
            </div>
          </div> <!-- OH ROW END -->

          <div class="agent-information-mobile">
            <div class="agent-profile-photo"></div>
            <div class="agent-information"></div>
          </div>
        </div>

      </div>
    </div> <!-- END REPORT MAIN CONTAINER -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/jquery-dateFormat.min.js"></script>

    <script>

    var reportID = "<?php echo $reportID;?>";
    var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";
    var reportObj;
    var stats;
    var activeSubCategories;
    var rankings_html;

    $(document).ready(function(){
      getReport(reportID);
    });

    function getReport(reportID, completionHandler) {
    	var _query = {};

    	_query["fn"] = "getReport";
    	_query["reportID"] = reportID;

      var json_data = _query;
      console.log("Outbound JSON Query: "+JSON.stringify(json_data));

    	$.ajax({
    		url: wsURL,
    		type:"POST",
    		data: {json : json_data},
    		success: function(data, textStatus, jqXHR){
    			if (data.status == 1) {
    				reportObj = data.report;
            stats = data.report["stats"];
            rankings_html = data.report["rankings_html"];

    				console.log("Returned JSON Object: "+JSON.stringify(data));

            //$(".report-logo-container img").attr("src","img/logos/"+data.brokerage["brokerageID"]+"_logo_black.png");

            if(data.report["start"] == 0) {
              $(".report-daterange").html("All Time");
            } else {
              var start_string = $.format.toBrowserTimeZone(data.report["start"], "MMM d, yyyy");
              var end_string = $.format.toBrowserTimeZone(data.report["end"], "MMM d, yyyy");
              $(".report-daterange").html(start_string+" to "+end_string);
            }

            if (data.report["pid"] == "all") {
              $(".reportType").html("Overview Report");
              $(".report-property-container").hide();
          		$(".rankings-subcategory").html("RANKINGS");
          		$(".all-headers").show();
          		$(".single-headers").hide();
          		$(".properties-row").show();
          	} else {
              $(".reportType").html("Property Report");
          		$(".rankings-subcategory").html("SESSIONS");
          		$(".all-headers").hide();
          		$(".single-headers").show();
          		$(".properties-row").hide();
              $(".property-image").css("background-image","url("+data.property["image"]+")");
              $(".property-title").html(data.property["title"]);
              $(".property-addr").html(data.property["addr1"]+", "+data.property["addr2"]);
          	}

            $(".agent-profile-photo").html("<img src='"+data.manager["pphoto"]+"' width='100%'>");
            var temp_agent_name = data.manager["fname"]+" "+data.manager["lname"]+"<br/>";
            var temp_agent_email = data.manager["email"]+"<br/>";
            var temp_agent_phone = data.manager["phone"];
            var temp_agent_company = data.manager["company"];

            if (!(temp_agent_company == "" || temp_agent_company == "N/A")) {
              temp_agent_company = temp_agent_company + "<br/>";
            }

            $(".agent-information").html(temp_agent_name+""+temp_agent_company+""+temp_agent_email+""+temp_agent_phone);


            if(data.brokerage.length == 0) {
              $(".report-logo-container").hide();
              console.log("No brokerage");
            } else {
              $(".report-logo-container .desktop-logo").attr("src",data.brokerage["logos"]["report"]);
              $(".report-logo-container .mobile-logo").attr("src",data.brokerage["logos"]["black"]);

              console.log("Has brokerage");
            }


            generateReport(data.report);

    				if ($.isFunction(completionHandler)) {
    					completionHandler();
    				}

    			} else {
    				//forceLogout();
    			}
    			loading(false);
    		},
    		error: function(jqXHR, textStatus, errorThrown) {
    			console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getBrokerageReport error Error: "+errorThrown);
    			loading(false);
    		},
    		dataType: "json"
    	});
    }

    function toggleDonutChart(tar, percentage) {
    	$("."+tar+"-card .inner .center .stat-percentage").html(percentage);
    	if(percentage == 0){
    		$("."+tar+"-card .inner .slice-top").css("background-color","white");
        $("."+tar+"-card .inner .slice-top img").attr("src","img/square_fff.jpg");
    		$("."+tar+"-card .inner .slice-bottom").css("background-color","white");
        $("."+tar+"-card .inner .slice-bottom img").attr("src","img/square_fff.jpg");
    	} else if(percentage <= 25) {
    		var degTop = 90;
    		var degBottom = Math.floor((25 - percentage)/100 * 360);
    		$("."+tar+"-card .inner .slice-top").css("background-color","#999");
        $("."+tar+"-card .inner .slice-top img").attr("src","img/square_999.jpg");
    		$("."+tar+"-card .inner .slice-bottom").css("background-color","white");
        $("."+tar+"-card .inner .slice-bottom img").attr("src","img/square_fff.jpg");
    		$("."+tar+"-card .inner .slice-top").css("transform","rotate("+degTop+"deg)");
    		$("."+tar+"-card .inner .slice-bottom").css("transform","rotate(-"+degBottom+"deg)");
    		$("."+tar+"-card .inner .cover-top").css("transform","rotate(-"+degBottom+"deg)");
    	} else if (percentage <= 50) {
    		var degTop = 90;
    		var degBottom = Math.floor((25 - (50 - percentage))/100 * 360);
    		$("."+tar+"-card .inner .slice-top").css("background-color","#999");
        $("."+tar+"-card .inner .slice-top img").attr("src","img/square_999.jpg");
    		$("."+tar+"-card .inner .slice-bottom").css("background-color","white");
        $("."+tar+"-card .inner .slice-bottom img").attr("src","img/square_fff.jpg");
    		$("."+tar+"-card .inner .slice-bottom").css("transform","rotate("+degBottom+"deg)");
    		$("."+tar+"-card .inner .slice-top").css("transform","rotate("+degTop+"deg)");
    		$("."+tar+"-card .inner .cover-top").css("transform","rotate("+degBottom+"deg)");

    	} else if (percentage <= 75) {
    		var degTop = 90;
    		var degBottom = Math.floor((75 - percentage)/100 * 360);
    		$("."+tar+"-card .inner .slice-top").css("background-color","#999");
        $("."+tar+"-card .inner .slice-top img").attr("src","img/square_999.jpg");
    		$("."+tar+"-card .inner .slice-bottom").css("background-color","#999");
        $("."+tar+"-card .inner .slice-bottom img").attr("src","img/square_999.jpg");
    		$("."+tar+"-card .inner .slice-top").css("transform","rotate("+degTop+"deg)");
    		$("."+tar+"-card .inner .slice-top").css("-webkit-transform","rotate("+degTop+"deg)");
    		$("."+tar+"-card .inner .slice-bottom").css("transform","rotate(-"+degBottom+"deg)");
    		$("."+tar+"-card .inner .slice-bottom").css("-webkit-transform","rotate(-"+degBottom+"deg)");
    		$("."+tar+"-card .inner .cover-top").css("transform","rotate(90deg)");
    	} else {
    		var degTop = 90;
    		var degBottom = Math.floor((25-(100 - percentage))/100 * 360);
    		$("."+tar+"-card .inner .slice-top").css("background-color","#999");
        $("."+tar+"-card .inner .slice-top img").attr("src","img/square_999.jpg");
    		$("."+tar+"-card .inner .slice-bottom").css("background-color","#999");
        $("."+tar+"-card .inner .slice-bottom img").attr("src","img/square_999.jpg");
    		$("."+tar+"-card .inner .slice-top").css("transform","rotate("+degTop+"deg)");
    		$("."+tar+"-card .inner .slice-top").css("-webkit-transform","rotate("+degTop+"deg)");
    		$("."+tar+"-card .inner .slice-bottom").css("transform","rotate("+degBottom+"deg)");
    		$("."+tar+"-card .inner .slice-bottom").css("-webkit-transform","rotate("+degBottom+"deg)");
    		$("."+tar+"-card .inner .cover-top").css("transform","rotate(90deg)");

    	}
    }

    function generateReport(report) {
      var total_properties = report["stats"]["total_properties"];
    	var total_visitors = report["stats"]["total_visitors"];
    	var total_oh = report["stats"]["total_oh"];

    	var active_properties = report["stats"]["active_properties"];
    	var inactive_properties = report["stats"]["inactive_properties"];

    	var visitor_with_contact = report["stats"]["visitor_with_contact"];
    	var visitor_without_contact = report["stats"]["visitor_without_contact"];
    	var visitor_verified = report["stats"]["visitor_verified"];

    	var visitor_with_representation = report["stats"]["visitor_with_representation"];
    	var visitor_without_representation= report["stats"]["visitor_without_representation"];
    	var visitor_unknown_representation = report["stats"]["visitor_unknown_representation"];

    	var visitor_with_financing = report["stats"]["visitor_with_financing"];
    	var visitor_without_financing= report["stats"]["visitor_without_financing"];
    	var visitor_unknown_financing = report["stats"]["visitor_unknown_financing"];

      var report_pid = report["pid"];

      if(report["activeSubCategories"]["properties-status"] == "false") {
        $(".properties-status").hide();
      }
      if(report["activeSubCategories"]["visitor-quality"] == "false") {
          $(".visitor-quality").hide();
      }
      if(report["activeSubCategories"]["visitor-representation"] == "false") {
          $(".visitor-representation").hide();
      }
      if(report["activeSubCategories"]["visitor-financing"] == "false") {
          $(".visitor-financing").hide();
      }

      $(".total-properties-num").html(total_properties);

      if (total_properties == 0) {
           total_properties = 1;
       }
       $(".active-listings-num").html(active_properties);
       $(".inactive-listings-num").html(inactive_properties);
       $(".active-listings-percentage").html(Math.floor((active_properties/total_properties)*1000)/10 +"%");
       $(".inactive-listings-percentage").html(Math.floor((inactive_properties/total_properties)*1000)/10 +"%");
       $(".active-listings-fill").css("width",Math.floor(active_properties/total_properties*100)+"%");
       $(".inactive-listings-fill").css("width",Math.floor(inactive_properties/total_properties*100)+"%");

       $(".total-visitors-num").html(total_visitors);

   if (total_visitors == 0) {
       total_visitors = 1;
   }

   $(".with-contact-num").html(visitor_with_contact);
   $(".without-contact-num").html(visitor_without_contact);
   $(".verified-num").html(visitor_verified);
   $(".with-contact-percentage").html(Math.floor((visitor_with_contact/total_visitors)*1000)/10 +"%");
   $(".without-contact-percentage").html(Math.floor((visitor_without_contact/total_visitors)*1000)/10 +"%");
   $(".verified-percentage").html(Math.floor((visitor_verified/total_visitors)*1000)/10 +"%");
   $(".with-contact-fill").css("width",Math.floor(visitor_with_contact/total_visitors*100)+"%");
   $(".without-contact-fill").css("width",Math.floor(visitor_without_contact/total_visitors*100)+"%");
   $(".verified-fill").css("width",Math.floor((visitor_verified/total_visitors)*1000)/10 +"%");

   $(".with-representation-num").html(visitor_with_representation);
   $(".without-representation-num").html(visitor_without_representation);
   $(".unknown-representation-num").html(visitor_unknown_representation);
   $(".with-representation-percentage").html(Math.floor((visitor_with_representation/total_visitors)*1000)/10 +"%");
   $(".without-representation-percentage").html(Math.floor((visitor_without_representation/total_visitors)*1000)/10 +"%");
   $(".unknown-representation-percentage").html(Math.floor((visitor_unknown_representation/total_visitors)*1000)/10 +"%");
   $(".with-representation-fill").css("width",Math.floor(visitor_with_representation/total_visitors*100)+"%");
   $(".without-representation-fill").css("width",Math.floor(visitor_without_representation/total_visitors*100)+"%");
   $(".unknown-representation-fill").css("width",Math.floor(visitor_unknown_representation/total_visitors*100)+"%");

   $(".with-financing-num").html(visitor_with_financing);
   $(".without-financing-num").html(visitor_without_financing);
   $(".unknown-financing-num").html(visitor_unknown_financing);
   $(".with-financing-percentage").html(Math.floor((visitor_with_financing/total_visitors)*1000)/10 +"%");
   $(".without-financing-percentage").html(Math.floor((visitor_without_financing/total_visitors)*1000)/10 +"%");
   $(".unknown-financing-percentage").html(Math.floor((visitor_unknown_financing/total_visitors)*1000)/10 +"%");
   $(".with-financing-fill").css("width",Math.floor(visitor_with_financing/total_visitors*100)+"%");
   $(".without-financing-fill").css("width",Math.floor(visitor_without_financing/total_visitors*100)+"%");
   $(".unknown-financing-fill").css("width",Math.floor(visitor_unknown_financing/total_visitors*100)+"%");

      $(".total-ohs-num").html(total_oh);

      $(".oh-rankings").html(report["rankings_html"]);
    }


    function loading(flag, msg, delay) {

    	if (typeof delay === "undefined" || delay === null) {
    		delay = 1000;
    	}
    	if (typeof msg === "undefined" || msg === null) {
    		msg = "Loading";
    	}

    	if(flag) {
    		$(".loading-msg").html("Loading...");
    		$(".loading-container").fadeIn();
    	} else {
    		$(".loading-container").fadeOut();
    	}
    }




    </script>



  </body>
</html>
