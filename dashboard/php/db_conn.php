<?php

require_once('db_config.php');

class Db_Conn {

    static protected $_instance;

    protected $db = null;

    final protected function __construct() {
		$dbhost1 = DB_HOST_1;
		$dbhost2 = DB_HOST_2;
		$dbhost3 = DB_HOST_3;
		$dbrset = DB_RSET;
		$dbport = DB_PORT;
		$dbname = DB_NAME;
		$dbuser = DB_USER;
		$dbpass = DB_PASS;
		
        $m = new MongoClient("mongodb://$dbuser:$dbpass@$dbhost1:$dbport,$dbhost2:$dbport,$dbhost3:$dbport/$dbname?replicaSet=$dbrset");
        $this->db = $m->selectDB("$dbname");
    }

    static public function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getConnection() {
        return $this->db;
    }

    final protected function __clone() { }
}

?>