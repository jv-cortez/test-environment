<?php

header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Content-type: application/json');

require_once('admin_queries.php');

$data = isset( $_POST['json'] ) ? $_POST['json'] : 0;
$fn = $data["fn"];
$tk = $data["token"];

$token = "thecowlevelisalie";

if ($tk == $token) {
	switch($fn) {
		case "createKeys":		
			$return = Adm_Query::createKeys();
			echo json_encode($return); 	
			break;	
		case "moveDeletedRegistrants":
			$return = Adm_Query::moveDeletedRegistrants();
			echo json_encode($return); 	
			break;	
		case "calculateHosted":
			$return = Adm_Query::calculateHosted();
			echo json_encode($return); 	
			break;	
	}
} else {
	$return = array("status" => 99);
	echo json_encode($return); 		
}


?>