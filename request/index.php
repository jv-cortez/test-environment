<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

  	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  	header("Cache-Control: post-check=0, pre-check=0", false);
  	header("Pragma: no-cache");

    $_brokerageID = isset($_GET['b']) ? $_GET['b'] : "";
?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Request Account</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
     <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="signup-scroll-one" style="background:none;">
      <div class="overlay">
        <form id="requestAccount_Form" action="index.php" method="POST" style="height:auto">
          <input type="hidden" name="fn" value="requestAccount" />
          <div class="xlarge" style="text-align:center;margin-bottom:20px;">
              Request a Spacio Account
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              FIRST NAME
            </div>
            <input class="signup-input" style="" type="text" name="fname" value="" id="fname" tabindex="1" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              LAST NAME
            </div>
            <input class="signup-input" style="" type="text" name="lname" value="" id="lname" tabindex="2" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              TITLE
            </div>
            <input class="signup-input" style="" type="text" name="title" value="" id="title" tabindex="3" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              EMAIL
            </div>
            <input class="signup-input" style="" type="email" name="email" value="" id="email" tabindex="4" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              PHONE
            </div>
            <input class="signup-input" style="" type="text" name="phone" value="" id="phone" tabindex="5" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              MOBILE
            </div>
            <input class="signup-input" style="" type="text" name="mobile" value="" id="mobile" tabindex="6" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              WEBSITE
            </div>
            <input class="signup-input" style="" type="text" name="website" value="" id="website" tabindex="7" placeholder="" >
          </div>
          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              PHOTO URL
            </div>
            <input class="signup-input" style="" type="text" name="pphoto" value="" id="pphoto" tabindex="8" placeholder="" >
          </div>

          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              OFFICE
            </div>
            <select class="signup-select " style="" name="officeID" id="officeID" tabindex="8">
              <option>None</option>
            </select>
          </div>

          <div><input type="hidden" name="brokerageID" value="<?php echo $_brokerageID ?>" id="brokerageID" placeholder=""></div>

          <div style="width:100%;text-align:right">
            <input class="signup-submit-btn-orange" type="submit" name="signup" value="REQUEST ACCOUNT">
          </div>
        </form>
      </div>

    </div>
    <!-- SCROLL ONE END -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
    <script>

    var brokerageID = "<?php echo $_brokerageID ?>";
	  var offices = [];
    var wsURL = "https://ws.spac.io/prod/oh/webservice_brokerage/1.1.0/webservice_brokerages.php";


    $(document).ready(function(){
      insertAlertBox();
      if(brokerageID != "") {
        getBrokerageOffices(brokerageID);

      } else {
        showAlert("error","Invalid Request Link","Sorry, the request link you used is invalid. Please contact support@spac.io.");
      }

    });

    function confirmClaimAccount() {

    }

    function getBrokerageOffices(em, id) {
      loading(true);
      var _query = {};
      _query["brokerageID"] = brokerageID;
      _query["fn"] = "getBrokerageOfficesByID";

      var json_data = _query;
      console.log("Outbound JSON Query: "+JSON.stringify(json_data));
      $.ajax({
        url: wsURL,
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){
          console.log("Returned JSON Object: "+JSON.stringify(data));

          if (data.status == 1) {
            offices = data.offices
          } else {
            offices = "";
          }
          loading(false);
          $("#officeID").html("<option value=''>Please select your office</option>");

          $.each(offices, function(index,value){
              $("#officeID").append("<option value='"+value["officeID"]+"'>"+value["office"]+"</option>");

          })
          //debugLog("Query Status: "+data.status);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Account Error: "+errorThrown);
        },
        dataType: "json"
      });

    }

    function requestAccount(_fields) {
      var _query = {};

  		//Format form inputs into proper array
  		$.each(_fields, function(index, value) {
  			_query[value["name"]] = value["value"];
  		})

  		_query["email"] = _query["email"].toLowerCase();
  		//Validate form inputs.
  		if (_query["email"] == "" || _query["email"] == null) {
  			showAlert("error","Invalid Email Format","Please enter a valid email.");
  		} else if(_query["fname"] == "" || _query["lname"] == "") {
			     showAlert("error","Mandatory Field","Please provide a first name and a last name.");
		  } else if(_query["officeID"] == null || _query["officeID"] == "") {
			     showAlert("error","Mandatory Field","Please select an office.");
		  } else {

        if(_query["pphoto"] == "") {
				      _query["pphoto"] = "http://spac.io/dashboard/img/stock/pphoto.jpg";

			  }

  			loading(true,"Requesting Your Account");
  			var json_data = _query;
  			console.log("Outbound JSON Query: "+JSON.stringify(json_data));
  			$.ajax({
  				url: wsURL,
  				type:"POST",
  				data: {json : json_data},
  				success: function(data, textStatus, jqXHR){
  					console.log("Returned JSON Object: "+JSON.stringify(data));

  					if (data.status == 1) {
  						showAlert("info","Request Sent","Your request has been received by the system. It may take up to an hour for the request to be verified. Once the request is verified, you will receive a claim link in your email that will direct you on how to claim your Spacio account.");
  					} else if (data.status == 2) {
  						showAlert("error","Email Already Registered","This email has already been registered with Spacio. Please contact support@spac.io if you are having issues login in or claiming your account.");
  					}
  					loading(false);
  					//debugLog("Query Status: "+data.status);
  				},
  				error: function(jqXHR, textStatus, errorThrown) {
  					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Account Error: "+errorThrown);
  				},
  				dataType: "json"
  			});
  		}
    }

    $( "form#requestAccount_Form" ).submit(function( event ) {

  		//Serialize form inputs
  		var _fields = $( this ).serializeArray();
      if(brokerageID == "lnf") {
        showAlert("info","Acknowledgement of Account Creation","In signing up for Spacio, you authorize us to charge $17.50 to your Long & Foster agent account on a recurring monthly basis until you cancel your Spacio subscription. You are fully responsible for all charges made to your agent account. Subscription to Spacio is month-to-month, and you may cancel anytime. If you do not want to continue, please select Cancel. ","Ok","Cancel",function(){
          requestAccount(_fields);
        });
      } else {
        requestAccount(_fields);
      }
  		event.preventDefault();
  	});

    </script>
  </body>
</html>
