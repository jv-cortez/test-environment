<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");




?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Introducing the Paperless Open House</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <style>
    .loading-ellipsis {
    }

    .loading-ellipsis:after {
      overflow: hidden;
      display: inline-block;
      vertical-align: bottom;
      -webkit-animation: ellipsis steps(4,end) 900ms infinite;
      animation: ellipsis steps(4,end) 900ms infinite;
      content: "\2026"; /* ascii code for the ellipsis character */
      width: 0px;
    }

    @keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes side-pan {
        0% {background-position: left center;}
      50% {background-position: right center;}
      100% {background-position: left center;}
    }
    </style>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
	 <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
    <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->
    <div style="height:100vh;width:100%;background:#eee;display:flex;flex-direction:column;align-items:center;justify-content:center;">
      <div style="font-size:40px;width:400px" class="loading-ellipsis">
        Logging into Spacio
      </div>
      <div style="font-size:20px;width:600px;display:none;line-height:1.4" class="error-message">
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
		<script src="/assets/js/jquery.countdown.min.js"></script>
    <script>

    var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";




    $(document).ready(function(){
      window.location.href = "https://sso.moxiworks.com/oauth/authorize/?client_id=d2c3e9f1be33f1fb8c3fe2edf07146a6b9a6d787ffc7395251c8e4637d37743f&redirect_uri=https%3a%2f%2fws.spac.io%2fprod%2foh%2foauth2%2fmoxi%2f&response_type=code";
    });



    </script>

  </body>
