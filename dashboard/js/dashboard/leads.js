function showVisitors(pid) {
    console.log("Showing Visitors Panel.");

    if(curPanel != "visitors") {
      prevPanel = curPanel;
      curPanel = "visitors";
    }


    $("#popup-view-visitors .table-content").html(""); //Clear Leads Table
    showPopup("view-visitors");

    curPID = pid;

    //Set to default values
    $("#visitors-filter-type-visitors").prop("checked", true);
    $("#visitors-filter-status-active").prop("checked", true);
    $("#visitors-filter-representation-all").prop("checked", true);
    $("#visitors-filter-verified-all").prop("checked", true);

    $(".visitors-property-address").html(properties[pid]["title"]);
    $(".visitors-property-photo").css("background-image","url("+properties[pid]["image"]+")");
    filterVisitors(true);

}


function filterVisitors(reload, sort, order) {
     if (typeof reload === "undefined" || reload === null) {
        reload = true;
    }
    if (typeof sort === "undefined" || sort === null) {
        sort = "dateCreated";
    }
    if (typeof order === "undefined" || order === null) {
        order = "desc";
    }

    $("#popup-view-visitors .header").removeClass("sorted");
    $("#popup-view-visitors ."+sort+"-header").addClass("sorted");
    $("#popup-view-visitors ."+sort+"-header .sort-box").removeClass("desc");
    $("#popup-view-visitors ."+sort+"-header .sort-box").removeClass("asc");
    $("#popup-view-visitors ."+sort+"-header .sort-box").addClass(order);
    $("#popup-view-visitors ."+sort+"-header").data("sort",order);

    var visitors_filter_type = $("input[name='visitors-filter-type']:checked").val();
    var visitors_filter_verified = $("input[name='visitors-filter-verified']:checked").val();
    var visitors_filter_representation = $("input[name='visitors-filter-representation']:checked").val();
    var visitors_filter_client = $("input[name='visitors-filter-client']:checked").val();

    var visitorType, visitorVerified, visitorRepresentation, visitorClient;

    var visitorSearchTerms = $("#visitors-search-terms").val().toLowerCase();

    var advanced_filter_settings = "";

    advanced_filter_settings += visitors_filter_type;

    if (visitors_filter_verified != "All" && visitors_filter_type == "Visitors") {
        advanced_filter_settings += ", "+visitors_filter_verified;
    }
    if (visitors_filter_representation != "All" && visitors_filter_type == "Visitors") {
        advanced_filter_settings += ", "+visitors_filter_representation;
    }
    if (visitors_filter_client != "All" && visitors_filter_type == "Brokers") {
        advanced_filter_settings += ", "+visitors_filter_client;
    }

    $("#visitors-advanced-filter-settings").html(advanced_filter_settings);
    hideAdvancedFilter();

    if(visitors_filter_type == "Visitors") {
        visitorType = "visitors";
        $("#popup-view-visitors .hasagent-header").show();
        $("#popup-view-visitors .hasclient-header").hide();
    } else if (visitors_filter_type == "Brokers") {
        visitorType = "brokers";
        $("#popup-view-visitors .hasagent-header").hide();
        $("#popup-view-visitors .hasclient-header").show();
    }

    if(visitors_filter_verified == "All") {
        visitorVerified = "all";
    } else if (visitors_filter_verified == "Verified") {
        visitorVerified = "YES";
    } else if (visitors_filter_verified == "Not Verified") {
        visitorVerified = "NO";
    }
    if(visitors_filter_representation == "All") {
        visitorRepresentation = "all";
    } else if (visitors_filter_representation == "Represented") {
        visitorRepresentation = "YES";
    } else if (visitors_filter_representation == "Not Represented") {
        visitorRepresentation = "NO";
    }
    if(visitors_filter_client == "All") {
        visitorClient = "all";
    } else if (visitors_filter_client == "Has Client") {
        visitorClient = "YES";
    } else if (visitors_filter_client == "No Client") {
        visitorClient = "NO";
    }

    if(reload) { //Grab data from server
        loading(true);
        getRegistrantsByPID(curPID, "all",function(visitorsList){
            if(properties[curPID]["disabled"] == "NO") {
                loadVisitors(registrants, visitorType, visitorVerified, visitorRepresentation, visitorClient, visitorSearchTerms, sort, order);
            } else {
                loadVisitors(registrants_inactive, visitorType, visitorVerified, visitorRepresentation, visitorClient, visitorSearchTerms, sort, order);
            }
            loading(false);
        });
    } else { //Don't need to grab data from server
        if(properties[curPID]["disabled"] == "NO") {
            loadVisitors(registrants, visitorType, visitorVerified, visitorRepresentation, visitorClient, visitorSearchTerms, sort, order);
        } else {
            loadVisitors(registrants_inactive, visitorType, visitorVerified, visitorRepresentation, visitorClient, visitorSearchTerms, sort, order);
        }
    }
}


function loadVisitors(visitorsList, visitorType, visitorVerified, visitorRepresentation, visitorClient, visitorSearchTerms, sort, order) {
    console.log("Loading Visitors");
    $("#popup-view-visitors .table-content").html("");
    var count = 0;
    var sortedVisitorsList = [];
    var hasClient = "UNKNOWN";
    var verified = "NO";

    $.each(visitorsList, function(index, value) {
        value["rid"] = index;
        //value["leadscore"] = calculateLeadScore(index);

        var l_name = value["name"].toLowerCase();
        var l_email = value["email"].toLowerCase();
        var l_phone = value["phone"].toLowerCase();

        var includeLead = false;
        if(l_name.indexOf(visitorSearchTerms) >= 0) {
            includeLead = true;
        } else if(l_email.indexOf(visitorSearchTerms) >= 0) {
            includeLead = true;
        } else if(l_phone.indexOf(visitorSearchTerms) >= 0) {
            includeLead = true;
        } else if(visitorSearchTerms == "") {
            includeLead = true;
        }
        console.log(value["pid"]+" = "+curPID);

        if(value["pid"] == curPID) {

            if(visitorType == "brokers") {

              if(value["isBroker"] == "YES") {
                if(value["answersObj"][0]["answer"] == "YES") {
                  hasClient = "YES";
                } else if(value["answersObj"][0]["answer"] == "NO") {
                  hasClient = "NO";
                } else {
                  hasClient = "UNKNOWN";
                }
              }

                if(includeLead && value["isBroker"] == "YES" && (visitorClient == "all" || visitorClient == hasClient || (visitorClient == "NO" && hasClient == "UNKNOWN"))) {
                    //console.log("CLIENT: "+value["answersObj"][0]["answer"]);
                    value["hasClient"] = hasClient;
                    sortedVisitorsList.push(value);
                }
            } else if(visitorType == "visitors") {

                if("fullcontact" in value) {
                    if(!("status" in value["fullcontact"])) {

                    } else if (value["fullcontact"]["status"] == 200) {
                        verified = "YES";
                    }
                }

                if(includeLead && value["isBroker"] == "NO" && (visitorVerified == "all" || visitorVerified == verified) && (visitorRepresentation == "all" || visitorRepresentation == value["hasAgent"] || (visitorRepresentation == "NO" && value["hasAgent"] == "UNKNOWN"))) {
                    sortedVisitorsList.push(value);
                }
            }
        }

    });

    if(sort == "dateCreated") {
        sortedVisitorsList = customSortDateTime(sortedVisitorsList,sort,order);
    } else  {
        sortedVisitorsList = customSort(sortedVisitorsList,sort,order);
    }

    $.each(sortedVisitorsList, function(index, value) {
        count++;
        var rid = value["rid"];
        var name = capitalizeFirstLetterEachWordSplitBySpace(value["name"]);
        var phone = value["phone"];
        var email = value["email"];
        var company = "";

        if("company" in value) {
            company = value["company"];
        }
        var score = 0;
        var hasName = false;
        var hasEmail = false;
        var hasPhone = false;
        var verified_icon = '<i style="color:#ddd;font-size:20px;" class="fa fa-check-circle" aria-hidden="true"></i>';

        var leadPhoto = "img/stock/pphoto.jpg";

        var datetime = new Date();
        if(typeof value["dateCreated"]["sec"] != "undefined") {
            datetime = new Date(value["dateCreated"]["sec"] * 1000);
        }


        if("fullcontact" in value && online) {
            if(!("status" in value["fullcontact"])) {

            } else if (value["fullcontact"]["status"] == 200 && user["type"] != "freemium" && user["type"] != "lite") {
                verified_icon = '<i style="color:#578dff;font-size:20px;" class="fa fa-check-circle" aria-hidden="true"></i>';
            }
            if(("photos" in value["fullcontact"]) && user["type"] != "freemium" && user["type"] != "lite") {
                leadPhoto = value["fullcontact"]["photos"][0]["url"];
            }
        }

        if(visitorType == "brokers") {

                $("#popup-view-visitors .table-content").append("<div id='lead-"+rid+"' class='row' onclick='showRegistrant(&#39;"+rid+"&#39;)'><div class='photo-column column'><div class='lead-photo' style='background-image:url("+leadPhoto+")'></div></div><div class='name-column column'> "+name+"</div><div class='email-column column'> "+email+"</div><div class='verified-column column'> "+verified_icon+"</div><div class='hasclient-column column'>"+value["hasClient"]+"</div><div class='date-column column'> "+datetime.toLocaleDateString()+"</div></div>");

        } else if(visitorType == "visitors") {
                $("#popup-view-visitors .table-content").append("<div id='lead-"+rid+"' class='row' onclick='showRegistrant(&#39;"+rid+"&#39;)'><div class='photo-column column'><div class='lead-photo' style='background-image:url("+leadPhoto+")'></div></div><div class='name-column column'> "+name+"</div><div class='email-column column'> "+email+"</div><div class='verified-column column'> "+verified_icon+"</div><div class='hasagent-column column'>"+value["hasAgent"]+"</div><div class='date-column column'> "+datetime.toLocaleDateString()+"</div></div>");

        }
    });
}
function toggleVisitorsSort(column) {
    var cur_sort = $("#popup-view-visitors ."+column+"-header").data("sort");
    if (cur_sort == "asc") {
        cur_sort = "desc";
    } else if (cur_sort == "desc"){
        cur_sort = "asc";
    }

    filterVisitors(false, column, cur_sort);
}




function showLeadsPanel() {
    console.log("Showing Leads Panel.");
    $(".popup-overlay").hide();

    closeAllPanels();
    prevPanel = curPanel;
    curPanel = "leads";
    $("#leads-panel .table-content").html(""); //Clear Leads Table
    $("#leads-menu-item").removeClass("not-selected").addClass("selected");
    $("#leads-panel").show();

    curPID = "all";

    //Set to default values
    $("#leads-filter-type-visitors").prop("checked", true);
    $("#leads-filter-status-active").prop("checked", true);
    $("#leads-filter-representation-all").prop("checked", true);
    $("#leads-filter-verified-all").prop("checked", true);

    $("#leads-property").html("All Properties");
    $("#leads-filter-status-column").show();
    filterLeads(true);

}

function toggleLeadsSort(column) {
    var cur_sort = $("#leads-panel ."+column+"-header").data("sort");
    if (cur_sort == "asc") {
        cur_sort = "desc";
    } else if (cur_sort == "desc"){
        cur_sort = "asc";
    }

    filterLeads(false, column, cur_sort);
}


function filterLeads(reload, sort, order) {
    if (typeof reload === "undefined" || reload === null) {
        reload = true;
    }
    if (typeof sort === "undefined" || sort === null) {
        sort = "dateCreated";
    }
    if (typeof order === "undefined" || order === null) {
        order = "desc";
    }

    $("#leads-panel .header").removeClass("sorted");
    $("#leads-panel ."+sort+"-header").addClass("sorted");
    $("#leads-panel ."+sort+"-header .sort-box").removeClass("desc");
    $("#leads-panel ."+sort+"-header .sort-box").removeClass("asc");
    $("#leads-panel ."+sort+"-header .sort-box").addClass(order);
    $("#leads-panel ."+sort+"-header").data("sort",order);

    var leads_filter_type = $("input[name='leads-filter-type']:checked").val();
    var leads_filter_status = $("input[name='leads-filter-status']:checked").val();
    var leads_filter_verified = $("input[name='leads-filter-verified']:checked").val();
    var leads_filter_representation = $("input[name='leads-filter-representation']:checked").val();
    var leads_filter_client = $("input[name='leads-filter-client']:checked").val();

    var leadType, leadStatus, leadVerified, leadRepresentation, leadClient;

    var leadSearchTerms = $("#leads-search-terms").val().toLowerCase();

    var advanced_filter_settings = "";

    advanced_filter_settings += leads_filter_type;

    advanced_filter_settings += ", "+leads_filter_status;


    if (leads_filter_verified != "All" && leads_filter_type == "Visitors") {
        advanced_filter_settings += ", "+leads_filter_verified;
    }
    if (leads_filter_representation != "All" && leads_filter_type == "Visitors") {
        advanced_filter_settings += ", "+leads_filter_representation;
    }
    if (leads_filter_client != "All" && leads_filter_type == "Brokers") {
        advanced_filter_settings += ", "+leads_filter_client;
    }

    $("#leads-advanced-filter-settings").html(advanced_filter_settings);
    hideAdvancedFilter();

    if(leads_filter_type == "Visitors") {
        leadType = "visitors";
        $("#leads-panel .hasagent-header").show();
        $("#leads-panel .hasclient-header").hide();
    } else if (leads_filter_type == "Brokers") {
        leadType = "brokers";
        $("#leads-panel .hasagent-header").hide();
        $("#leads-panel .hasclient-header").show();
    }

    if(leads_filter_status == "Active Listings") {
        leadStatus = "active";
    } else if (leads_filter_status == "Inactive Listings") {
        leadStatus = "inactive";
    }
    if(leads_filter_verified == "All") {
        leadVerified = "all";
    } else if (leads_filter_verified == "Verified") {
        leadVerified = "YES";
    } else if (leads_filter_verified == "Not Verified") {
        leadVerified = "NO";
    }
    if(leads_filter_representation == "All") {
        leadRepresentation = "all";
    } else if (leads_filter_representation == "Represented") {
        leadRepresentation = "YES";
    } else if (leads_filter_representation == "Not Represented") {
        leadRepresentation = "NO";
    }
    if(leads_filter_client == "All") {
        leadClient = "all";
    } else if (leads_filter_client == "Has Client") {
        leadClient = "YES";
    } else if (leads_filter_client == "No Client") {
        leadClient = "NO";
    }

    if (curPID == "all" && leadStatus == "inactive") {
        reload = true;
    }

    if(reload) { //Grab data from server
        loading(true);
        getRegistrantsByPID(curPID, leadStatus,function(leadsList){
            loadLeads(leadsList, leadType, leadStatus, leadVerified, leadRepresentation, leadClient, leadSearchTerms, sort, order);
            loading(false);
        });
    } else { //Don't need to grab data from server
        if(leadStatus == "active") {
            loadLeads(registrants, leadType, leadStatus, leadVerified, leadRepresentation, leadClient, leadSearchTerms, sort, order);
        } else {
            loadLeads(registrants_inactive, leadType, leadStatus, leadVerified, leadRepresentation, leadClient, leadSearchTerms, sort, order);
        }
    }

}


function loadLeads(leadsList,leadType,leadStatus,leadVerified,leadRepresentation,leadClient,leadSearchTerms,sort, order) {

    $("#leads-panel .table-content").html("");
    var count = 0;
    var sortedLeadsList = [];
    var hasClient = "UNKNOWN";
    var verified = "NO";


    $.each(leadsList, function(index, value) {
        value["rid"] = index;

        var l_name = value["name"].toLowerCase();
        var l_email = value["email"].toLowerCase();
        var l_phone = value["phone"].toLowerCase();

        var includeLead = false;
        if(l_name.indexOf(leadSearchTerms) >= 0) {
            includeLead = true;
        } else if(l_email.indexOf(leadSearchTerms) >= 0) {
            includeLead = true;
        } else if(l_phone.indexOf(leadSearchTerms) >= 0) {
            includeLead = true;
        } else if(leadSearchTerms == "") {
            includeLead = true;
        }

        if(leadType == "brokers") {
            if(value["isBroker"] == "YES") {
              if(value["answersObj"][0]["answer"] == "YES") {
                hasClient = "YES";
              } else if(value["answersObj"][0]["answer"] == "NO") {
                hasClient = "NO";
              } else {
                hasClient = "UNKNOWN";
              }
            }

            if(includeLead && value["isBroker"] == "YES" && (leadClient == "all" || leadClient == hasClient || (leadClient == "NO" && hasClient == "UNKNOWN"))) {
                value["hasClient"] = hasClient;
                sortedLeadsList.push(value);
            }
        } else if(leadType == "visitors") {

            if("fullcontact" in value) {
                if(!("status" in value["fullcontact"])) {

                } else if (value["fullcontact"]["status"] == 200) {
                    verified = "YES";
                }
            }

            if(includeLead && value["isBroker"] == "NO" && (leadVerified == "all" || leadVerified == verified) && (leadRepresentation == "all" || leadRepresentation == value["hasAgent"] || (leadRepresentation == "NO" && value["hasAgent"] == "UNKNOWN"))) {
                sortedLeadsList.push(value);
            }

        }
    });

    if(sort == "dateCreated") {
        sortedLeadsList = customSortDateTime(sortedLeadsList,sort,order);
    } else  {
        sortedLeadsList = customSort(sortedLeadsList,sort,order);
    }


    $.each(sortedLeadsList, function(index, value) {
        count++;
        var rid = value["rid"];
        var name = capitalizeFirstLetterEachWordSplitBySpace(value["name"]);
        var phone = value["phone"];
        var email = value["email"];
        var company = "";

        if("company" in value) {
            company = value["company"];
        }
        var score = 0;
        var hasName = false;
        var hasEmail = false;
        var hasPhone = false;
        var verified_icon = '<i style="color:#ddd;font-size:20px;" class="fa fa-check-circle" aria-hidden="true"></i>';

        var leadPhoto = "img/stock/pphoto.jpg";

        var datetime = new Date();
        if(typeof value["dateCreated"]["sec"] != "undefined") {
            datetime = new Date(value["dateCreated"]["sec"] * 1000);
        }



        if("fullcontact" in value && online) {
            if(!("status" in value["fullcontact"])) {

            } else if (value["fullcontact"]["status"] == 200 && user["type"] != "freemium" && user["type"] != "lite") {
                verified_icon = '<i style="color:#578dff;font-size:20px;" class="fa fa-check-circle" aria-hidden="true"></i>';
            }
            if(("photos" in value["fullcontact"]) && user["type"] != "freemium" && user["type"] != "lite") {
                leadPhoto = value["fullcontact"]["photos"][0]["url"];
            }
        }

        if(leadType == "brokers") {
              $("#leads-panel .table-content").append("<div id='lead-"+rid+"' class='row' onclick='showRegistrant(&#39;"+rid+"&#39;)'><div class='photo-column column'><div class='lead-photo' style='background-image:url("+leadPhoto+")'></div></div><div class='name-column column'> "+name+"</div><div class='email-column column'> "+email+"</div><div class='verified-column column'> "+verified_icon+"</div><div class='hasclient-column column'>"+value["hasClient"]+"</div><div class='date-column column'> "+datetime.toLocaleDateString()+"</div></div>");

        } else if(leadType == "visitors") {
           if(rid.substring(0,4) != "temp") {
                $("#leads-panel .table-content").append("<div id='lead-"+rid+"' class='row' onclick='showRegistrant(&#39;"+rid+"&#39;)'><div class='photo-column column'><div class='lead-photo' style='background-image:url("+leadPhoto+")'></div></div><div class='name-column column'> "+name+"</div><div class='email-column column'> "+email+"</div><div class='verified-column column'> "+verified_icon+"</div><div class='hasagent-column column'>"+value["hasAgent"]+"</div><div class='date-column column'> "+datetime.toLocaleDateString()+"</div></div>");
            } else {
                $("#leads-panel .table-content").append("<div id='lead-"+rid+"' class='row' onclick='showRegistrant(&#39;"+rid+"&#39;)'><div class='photo-column column'><div class='lead-photo' style='background-image:url("+leadPhoto+")'></div></div><div class='name-column column unsynched'> "+name+"</div><div class='email-column column'> "+email+"</div><div class='verified-column column'> "+verified_icon+"</div><div class='hasagent-column column'>"+value["hasAgent"]+"</div><div class='date-column column'> "+datetime.toLocaleDateString()+"</div></div>");

            }

        }

        var _property_addr;
        try {
            _property_addr = properties[value["pid"]]["addr1"];
            if (_property_addr == "") {_property_addr = properties[value["pid"]]["title"];};
        } catch(err) {
            _property_addr = "Property Not Found";
        }

        $("#leads-panel .table-content").append("<div class='sub-row'><div class='lead-registered-at'>From: <span style=''>"+_property_addr+"</span></div></div>");

    });

}

function filterVisitorTypeChanged() {
    var visitors_filter_type = $("input[name='visitors-filter-type']:checked").val();

    if(visitors_filter_type == "Visitors") {
        $("#visitors-filter-representation-column").show();
        $("#visitors-filter-verified-column").show();
        $("#visitors-filter-client-column").hide();
    }

    if(visitors_filter_type == "Brokers") {
        $("#visitors-filter-representation-column").hide();
        $("#visitors-filter-verified-column").hide();
        $("#visitors-filter-client-column").show();
    }

    if(visitors_filter_type == "Visitors Representative") {
        $("#visitors-filter-representation-column").hide();
        $("#visitors-filter-verified-column").hide();
        $("#visitors-filter-client-column").hide();
    }
}


function filterLeadTypeChanged() {
    var leads_filter_type = $("input[name='leads-filter-type']:checked").val();

    if(leads_filter_type == "Visitors") {
        $("#leads-filter-representation-column").show();
        $("#leads-filter-verified-column").show();
        $("#leads-filter-client-column").hide();
    }

    if(leads_filter_type == "Brokers") {
        $("#leads-filter-representation-column").hide();
        $("#leads-filter-verified-column").hide();
        $("#leads-filter-client-column").show();
    }

    if(leads_filter_type == "Visitors Representative") {
        $("#leads-filter-representation-column").hide();
        $("#leads-filter-verified-column").hide();
        $("#leads-filter-client-column").hide();
    }
}


function showAddLead() {
    prevPanel = curPanel;
    curPanel = "addlead";
    if(online) {
          $("#popup-view-visitors").hide();

        showPopup("add-visitor");
        var pid = curPID;

        if(pid in properties) {
            $("#add-registrant-property").html(properties[pid]["title"]);

            if(properties[pid]["autoEmail"] == "YES") {

              $("#add-registrant-autoemail-warning").html("Your Auto Email is enabled. A follow up email will be sent to the Visitor or their agent after they've been added.");
            } else {
              $("#add-registrant-autoemail-warning").html("");
            }

          } else {
            $("#add-registrant-property").html("PROPERTY NOT FOUND");
          }

          var d = new Date();
          var n = d.getDay();
          var date_timestamp = d.getTime();

          $("#add-registrant-datecreated").val($.format.date(date_timestamp, "yyyy-MM-dd"));
          $("#add-registrant-timecreated").val("14:00:00");

          $("#popup-add-visitor .answers").html("");
          $.each(properties[pid]["questionsObj"], function(index,value) {

              if(value["type"] == "agent") {

                  $("#popup-add-visitor .answers").append("<div class='form-question-row eq1-master'><div class='question'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-registrant-question-"+index+"-yes' type='radio' value='YES' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-yes' onclick='showEQ(1)'>Yes</label></div><div class='checkbox'><input id='add-registrant-question-"+index+"-no' type='radio' value='NO' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-no' onclick='hideEQ(1)'>No</label></div></div></div>");

                  $("#popup-add-visitor .answers").append("<div class='form-question-row eq1-slave'><div class='question'>Agent Name?</div><input name='add-registrant-question-"+index+"a' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Agent Name' style='text-transform: capitalize;' autocapitalize=words></div>");

                  $("#popup-add-visitor .answers").append("<div class='form-question-row eq1-slave'><div class='question'>Agent Contact?</div><input name='add-registrant-question-"+index+"b' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Agent Email or Phone'></div>");

                  $("#popup-add-visitor .answers").append("<div class='form-question-row eq1-slave'><div class='question'>Agent Company?</div><input name='add-registrant-question-"+index+"c' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Agent Company' style='text-transform: capitalize;'></div>");
                  //$(".answers").append("<div class='visitor-info-question-row'><div class='question'>"+value["question"]+"</div><input name='add-registrant-answer-"+index+"' type='text' id='add-registrant-answer-"+index+"' value=''></div>");

              } else if(value["type"] == "yn") {
                $("#popup-add-visitor .answers").append("<div class='form-question-row'><div class='question'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-registrant-question-"+index+"-yes' type='radio' value='YES' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='add-registrant-question-"+index+"-no' type='radio' value='NO' name='add-registrant-question-"+index+"'><label for='add-registrant-question-"+index+"-no'>No</label></div></div></div>");

              } else if(value["type"] == "text"){
                 $("#popup-add-visitor .answers").append("<div class='form-question-row'><div class='question'>"+value["question"]+"</div><input name='add-registrant-question-"+index+"' type='text' id='add-registrant-answer-"+index+"' value='' placeholder='Answer'></div>");

              } else if (value["type"] == "mc") {
                  $("#popup-add-visitor .answers").append("<div class='form-question-row'><div class='question'>"+value["question"]+"</div><div class='full-width-checkboxes' id='answers-section-"+index+"'></div></div>");

                            $.each(value["choices"], function(i,v){
                                $("#answers-section-"+index).append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='add-registrant-question-"+index+"' id='add-registrant-question-"+index+"-"+i+"'><label for='add-registrant-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
                            })
                        }
            });
        hideEQ(1);

    } else {
        showAlert("Offline Mode","This function requires a live internet connection.");

    }

}

function clearLeadsSearch() {
    $("#leads-search-terms").val("");
    filterLeads(false);
}

function clearVisitorsSearch() {
    $("#visitors-search-terms").val("");
    filterVisitors(false);
}

function reloadRegistrants() {
    if (curPanel == "leads") {
        showLeadsPanel(curPID);
    } else if (curPanel == "visitors") {
        showVisitors(curPID);
    } else {
        if (prevPanel == "leads") {
            filterLeads();
        } else if (prevPanel == "visitors") {
            filterVisitors();
        }
    }
}

function reloadRegistrant() {
    showRegistrant(curRID);
}

function showRegistrant(index) {
  var temp_leads_arr = {};

  curRID = index;

  if(curRID in registrants) {
    temp_leads_arr = registrants;
  } else if(curRID in registrants_inactive) {
    temp_leads_arr = registrants_inactive;
  }

  if(curPanel != "visitor") {
    prevPanel = curPanel;
    curPanel = "visitor";
  }

  if(prevPanel == "visitors") {
    $("#popup-view-visitors").hide();
  }

    var pid = temp_leads_arr[curRID]["pid"];
  /**** Check if is broker ****/
    var isBroker = false;
    if("isBroker" in temp_leads_arr[curRID]) {
        if (temp_leads_arr[curRID]["isBroker"] == "YES") {
            isBroker = true;
        }
    }

  /**** LOAD VISITOR PHOTO ****/
  var reg_pphoto = "img/stock/pphoto.jpg";
  if("fullcontact" in temp_leads_arr[curRID]) {
        if("photos" in temp_leads_arr[curRID]["fullcontact"]) {
            reg_pphoto = temp_leads_arr[curRID]["fullcontact"]["photos"][0]["url"];
        }
  }
  $(".visitor-pphoto").css("background-image","url("+reg_pphoto+")");


  /**** LOAD VISITOR INFO ****/
  var score = calculateLeadScore(curRID);


  $("#rid").val(temp_leads_arr[curRID]["id"]);
  temp_leads_arr[curRID]["name"] = capitalizeFirstLetterEachWordSplitBySpace(temp_leads_arr[curRID]["name"]);
  $(".visitor-name").html(temp_leads_arr[curRID]["name"]);
  var datetime = new Date(temp_leads_arr[curRID]["dateCreated"]["sec"] * 1000);
  $(".visitor-score").html("Lead Score: "+score);

  $("#registrant-datetime").html(datetime.toLocaleString());

  if(pid in properties) {
    $("#registrant-property").html(properties[pid]["title"]);
  } else {
    $("#registrant-property").html("PROPERTY NOT FOUND");
  }

  if(user["type"] == "team") {
    console.log("Collected by team member: "+ JSON.stringify(team) + " "+temp_leads_arr[curRID]["collectedBy"]);

    try {
      $("#registrant-collectedBy").html(team["team_members"][temp_leads_arr[curRID]["collectedBy"]]["fname"]+" "+ team["team_members"][temp_leads_arr[curRID]["collectedBy"]]["lname"]);
    } catch(e) {
      $("#registrant-collectedBy").html("Unknown");
    }
  } else {
    $("#registrant-collectedBy").html(user["fname"]+" "+user["lname"]);
  }

  if("emailedStatus" in temp_leads_arr[curRID]) {
        if(user["type"] == "freemium") {
            $("#registrant-email-status").html("Auto email status available for paid users only.");
        } else if (temp_leads_arr[curRID]["emailedStatus"] == "Pending"){
            $("#registrant-email-status").html("Not Applicable");
        } else {
            $("#registrant-email-status").html(temp_leads_arr[curRID]["emailedStatus"]);
        }

    } else {
        $("#registrant-email-status").html("Not Applicable.");
    }

  $("#registrant-name").val(temp_leads_arr[curRID]["name"]);
    $("#registrant-email").val(temp_leads_arr[curRID]["email"]);
    $("#registrant-phone").val(temp_leads_arr[curRID]["phone"]);

  if(isBroker) {
        $("#registrant-company").val(temp_leads_arr[curRID]["company"]);
        $("#company-row").show();
    } else {
        $("#company-row").hide();
    }

  $("#popup-view-registrant .answers").html("");
  $.each(temp_leads_arr[curRID]["answersObj"], function(index,value) {
      if(value["questionObj"]["type"] == "agent") {
        if(value["answer"] == "") {
           $("#popup-view-registrant .answers").append("<div class='form-question-row'><div class='question'>"+value["questionObj"]["question"]+"</div><div class='answer'>UNKNOWN</div></div>");
        } else {
          $("#popup-view-registrant .answers").append("<div class='form-question-row'><div class='question'>"+value["questionObj"]["question"]+"</div><div class='answer'>"+value["answer"]+"</div></div>");
        }
      } else if(value["questionObj"]["type"] == "yn") {
        if(value["answer"] == "YES") {
             $("#popup-view-registrant .answers").append("<div class='form-question-row'><div class='question'>"+value["questionObj"]["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='edit-answer-"+index+"-yes' type='radio' value='YES' name='edit-answer-"+index+"' checked><label for='edit-answer-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='edit-answer-"+index+"-no' type='radio' value='NO' name='edit-answer-"+index+"'><label for='edit-answer-"+index+"-no'>No</label></div></div></div>");
        } else if(value["answer"] == "NO"){
             $("#popup-view-registrant .answers").append("<div class='form-question-row'><div class='question'>"+value["questionObj"]["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='edit-answer-"+index+"-yes' type='radio' value='YES' name='edit-answer-"+index+"'><label for='edit-answer-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='edit-answer-"+index+"-no' type='radio' value='NO' name='edit-answer-"+index+"' checked><label for='edit-answer-"+index+"-no'>No</label></div></div></div>");
        } else {
           $("#popup-view-registrant .answers").append("<div class='form-question-row'><div class='question'>"+value["questionObj"]["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='edit-answer-"+index+"-yes' type='radio' value='YES' name='edit-answer-"+index+"'><label for='edit-answer-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='edit-answer-"+index+"-no' type='radio' value='NO' name='edit-answer-"+index+"'><label for='edit-answer-"+index+"-no'>No</label></div></div></div>");
        }

      } else {
         $("#popup-view-registrant .answers").append("<div class='form-question-row'><div class='question'>"+value["questionObj"]["question"]+"</div><input name='edit-answer-"+index+"' type='text' id='edit-answer-"+index+"' value=''></div>");

         $("#edit-answer-"+index+"").val(value["answer"]);
      }
    });

  $("#registrant-note").val(temp_leads_arr[curRID]["note"]);

  /**** LOAD VISITOR DOCS ****/
  $(".registrant-docs").hide();
  $(".registrant-docs-subtitle").hide();
  $(".registrant-docs").html("");
  $(".docs").html("");
  if("docs" in temp_leads_arr[curRID] && user["integrations"]["docsEnabled"] == "YES") {
    if(temp_leads_arr[curRID]["docs"].length > 0) {
      $(".registrant-docs").show();
      $(".registrant-docs-subtitle").show();

      $.each(brokerage["docs"],function(index,value){
        console.log(curRID);
        try {
          if(temp_leads_arr[curRID]["docs"][index]["status"] == "pending") {
             $(".registrant-docs").append("<div id='doc-"+index+"' class='doc-row-2' onclick='loadDoc("+index+")'><div class='icon'><div class='completed'><i class='fa fa-check-circle' aria-hidden='true'></i></div><i class='fa fa-file-text' aria-hidden='true'></i></div><div class='doc-name'>"+value["title"]+" (Unsigned)</div></div>");
         } else {
           var doc_status = temp_leads_arr[curRID]["docs"][index]["status"];
           if (doc_status == "signed") {
             doc_status = "Signed";
           } else if (doc_status == "declined") {
             doc_status = "Declined";
           } else if (doc_status == "with_broker") {
             doc_status = "With Broker";
           } else {
             doc_status = "Unsigned";
           }

           $(".registrant-docs").append("<div id='doc-"+index+"' class='doc-row-2' onclick='loadDoc("+index+")'><div class='icon'><div class='completed' style='display:block;'><i class='fa fa-check-circle' aria-hidden='true'></i></div><i class='fa fa-file-text' aria-hidden='true'></i></div><div class='doc-name'>"+value["title"]+" ("+doc_status+")</div></div>");
         }
        } catch(e) {

        }


      })
    }
  }

  /**** LOAD VISITOR INTEGRATION STATUSES ****/
  $(".registrant-integrations-subtitle").hide();
  $(".registrant-integrations").html("");
  if("log" in temp_leads_arr[curRID]) {
    if(!$.isEmptyObject(temp_leads_arr[curRID]["log"])) {

      $.each(temp_leads_arr[curRID]["log"], function(i,v){
        console.log(i);
        if(i == "adwerxStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>Adwerx</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "agencyStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>The Agency Salesforce</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "boojStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["status"] == "Failed") {
            $(".registrant-integrations").append("<div class='integration-name-row'>Lyon Connect</div><div class='integration-status-row'>"+v["error"]+"<div class='view-button main-color-font' onclick='resyncBoojLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
          } else if(v["status"] == "Success"){
            $(".registrant-integrations").append("<div class='integration-name-row'>Lyon Connect</div><div class='integration-status-row'>"+v["status"]+"</div>");
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>Lyon Connect</div><div class='integration-status-row'>"+v["status"]+"<div class='view-button main-color-font' onclick='resyncBoojLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
          }
        }

        if(i == "boomtownStatus") {
          $(".registrant-integrations-subtitle").show();

          $(".registrant-integrations").append("<div class='integration-name-row'>Boomtown</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "contactuallyStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["cid"] == "") {
            try {
              if("base" in v["response"]["errors"]) {
                $(".registrant-integrations").append("<div class='integration-name-row'>Contactually</div><div class='integration-status-row'>"+v["response"]["errors"]["base"][0]+"</div>");
              } else {
                $(".registrant-integrations").append("<div class='integration-name-row'>Contactually</div><div class='integration-status-row'>"+v["response"]["errors"][0]+"<div class='view-button main-color-font' onclick='resyncContactuallyLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
              }
            } catch(e) {
              $(".registrant-integrations").append("<div class='integration-name-row'>Contactually</div><div class='integration-status-row'>Unknown Error.<div class='view-button main-color-font' onclick='resyncContactuallyLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
            }
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>Contactually</div><div class='integration-status-row'>Success (ContactID: "+v["cid"]+")</div>");
          }
        }

        if(i == "corelogicStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["status"] != "Success") {
            $(".registrant-integrations").append("<div class='integration-name-row'>Agent Achieve</div><div class='integration-status-row'>"+v["status"]+"<div class='view-button main-color-font' onclick='resyncCorelogicLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>Agent Achieve</div><div class='integration-status-row'>"+v["status"]+"</div>");
          }
        }

        if(i == "followupbossStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["status"] == "Failed") {
            $(".registrant-integrations").append("<div class='integration-name-row'>Follow Up Boss</div><div class='integration-status-row'>Failed<div class='view-button main-color-font' onclick='resyncFollowupbossLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>Follow Up Boss</div><div class='integration-status-row'>"+v["status"]+"</div>");
          }
        }

        if(i == "idcStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["status"] == "Failed") {
            $(".registrant-integrations").append("<div class='integration-name-row'>IDC</div><div class='integration-status-row'>Failed<div class='view-button main-color-font' onclick='resyncIDCLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
          } else if(v["status"] == "Success"){
            $(".registrant-integrations").append("<div class='integration-name-row'>IDC</div><div class='integration-status-row'>"+v["status"]+"</div>");
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>IDC</div><div class='integration-status-row'>"+v["status"]+"</div>");
          }
        }

        if(i == "moxiEngageStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["status"] == "FAILED" || v["status"] == "failed" || v["status"] == "Failed") {
            $(".registrant-integrations").append("<div class='integration-name-row'>Moxi Engage</div><div class='integration-status-row'>Failed<div class='view-button main-color-font' onclick='resyncMoxiLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
          } else if(v["status"] == "Success") {
            $(".registrant-integrations").append("<div class='integration-name-row'>Moxi Engage</div> <div class='integration-status-row'>Success</div>");
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>Moxi Engage</div> <div class='integration-status-row'>"+v["status"]+"<div class='view-button main-color-font' onclick='resyncMoxiLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
          }
        }

        if(i == "nhintranetStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>NextHome/RealtyWorld Intranet</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "reavolveStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["success"] == true || v["success"] == "true") {
            $(".registrant-integrations").append("<div class='integration-name-row'>Realvolve</div><div class='integration-status-row'>Success</div>");
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>Realvolve</div><div class='integration-status-row'>Failed</div>");
          }

        }

        if(i == "relianceStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>Reliance CRM</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "realscoutStatus") {
          $(".registrant-integrations-subtitle").show();
          if(v["status"] == "Failed") {
            $(".registrant-integrations").append("<div class='integration-name-row'>RealScout</div><div class='integration-status-row'>"+v["error_msg"]+"</div>");
          } else {
            $(".registrant-integrations").append("<div class='integration-name-row'>RealScout</div><div class='integration-status-row'>Success</div>");
          }
        }

        if(i == "renthopStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>RentHop / RealtyJuggler</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "rewStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>Real Estate Webmasters</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "topproducerStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>Top Producer</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }

        if(i == "wiseagentStatus") {
          $(".registrant-integrations-subtitle").show();
          $(".registrant-integrations").append("<div class='integration-name-row'>Wise Agent</div><div class='integration-status-row'>"+v["status"]+"</div>");
        }
      })

    }
  } else {
    temp_leads_arr[curRID]["log"] = [];
  }

  if("integrations" in user) {
    if(!$.isEmptyObject(user["integrations"])) {
        $.each(user["integrations"], function(i,v){
          if(i == "contactuallyID" && v != "" && v!= "N/A") {

            if(!("contactuallyStatus" in temp_leads_arr[curRID]["log"])) {
                $(".registrant-integrations-subtitle").show();

              $(".registrant-integrations").append("<div class='integration-name-row'>Contactually</div><div class='integration-status-row'>Not Sent.<div class='view-button main-color-font' onclick='resyncContactuallyLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
            }
          }

          if(i == "idcEnabled" && v == "YES") {
            if(!("idcStatus" in temp_leads_arr[curRID]["log"])) {
            $(".registrant-integrations-subtitle").show();
              $(".registrant-integrations").append("<div class='integration-name-row'>IDC</div><div class='integration-status-row'>Not Sent.<div class='view-button main-color-font' onclick='resyncIDCLead()'><i class='fa fa-refresh' aria-hidden='true'></i></div></div>");
            }
          }
        });
    }
  }

  /**** LOAD VISITOR SOCIAL PROFILES ****/
   if(isBroker) {
      $(".registrant-fullcontact").html("<div class='fc-social-profile' style='background:none;'>Contact verification not available for Broker Open House.</div>");

   } else {
      $(".registrant-fullcontact").html("<div class='fc-social-profile' style='background:none;'>Contact not verified. No additional information found.</div>");

   }
  $(".verified-icon").hide();
  $(".unverified-icon").show();

  if("fullcontact" in temp_leads_arr[curRID]) {
        if(temp_leads_arr[curRID]["fullcontact"]["status"] == "200") {
            if(user["type"] == "freemium") {
        $(".registrant-fullcontact").html("<div class='fc-social-profile' style='background:none;'>Data available to Pro users only.</div>");

                $(".verified-icon").hide();
                $(".unverified-icon").show();
            } else if("socialProfiles" in temp_leads_arr[curRID]["fullcontact"]) {
                $(".registrant-fullcontact").html("");
        $.each(temp_leads_arr[curRID]["fullcontact"]["socialProfiles"], function(i, v){
                    $(".registrant-fullcontact").append("<a href='"+v["url"]+"' target='_blank'><div class='fc-social-profile'>"+v["typeName"]+" Profile<div class='view-button main-color-font'><i class='fa fa-eye' aria-hidden='true'></i></div></div></a>");
                })
                $(".verified-icon").show();
                $(".unverified-icon").hide();
            }
        }
    }

    showPopup("view-registrant");


    /***** Handle RealScout and Cloud CMA settings for Visitor *****/
    if("realscoutEmail" in  user && "realscout" in temp_leads_arr[curRID]) {
        if(user["realscoutEmail"] == "") {
            $("#popup-view-registrant .realscout").hide();
        } else {
            $("#popup-view-registrant .realscout").show();
            $("#popup-view-registrant .realscout-answer").html(temp_leads_arr[curRID]["realscout"]);
        }
    } else {
        $(".realscout").hide();
    }

    if("cloudcmaMLS" in  properties[temp_leads_arr[curRID]["pid"]] && "cloudcma" in temp_leads_arr[curRID] && ("cloudcmaID" in user && user["integrations"]["cloudcmaID"] != "")) {
        if(properties[temp_leads_arr[curRID]["pid"]]["cloudcmaMLS"] == "" || properties[temp_leads_arr[curRID]["pid"]]["cloudcmaMLS"] == "N/A") {
            $("#popup-view-registrant .cloudcma").hide();
        } else {
            $("#popup-view-registrant .cloudcma").show();
            $("#popup-view-registrant .cloudcma-answer").html(temp_leads_arr[curRID]["cloudcma"]);
        }
    } else {
        $("#popup-view-registrant .cloudcma").hide();
    }

    if("moxipresentURL" in  properties[temp_leads_arr[curRID]["pid"]] && "moxipresent" in temp_leads_arr[curRID] && user["integrations"]["moxipresentEnabled"] == "YES") {
             if(properties[temp_leads_arr[curRID]["pid"]]["moxipresentURL"] == "" || properties[temp_leads_arr[curRID]["pid"]]["moxipresentURL"] == "N/A") {
                     $("#popup-view-registrant .moxipresent").hide();
             } else {
                     $("#popup-view-registrant .moxipresent").show();
                     $("#popup-view-registrant .moxipresent-answer").html(temp_leads_arr[curRID]["moxipresent"]);
             }
     } else {
             $("#popup-view-registrant .moxipresent").hide();
     }

     if("touchcmaURL" in  properties[temp_leads_arr[curRID]["pid"]] && "touchcma" in temp_leads_arr[curRID] && user["integrations"]["touchcmaEnabled"] == "YES") {
             if(properties[temp_leads_arr[curRID]["pid"]]["touchcmaURL"] == "" || properties[temp_leads_arr[curRID]["pid"]]["touchcmaURL"] == "N/A") {
                     $("#popup-view-registrant .touchcma").hide();
             } else {
                     $("#popup-view-registrant .touchcma").show();
                     $("#popup-view-registrant .touchcma-answer").html(temp_leads_arr[curRID]["touchcma"]);
             }
     } else {
             $("#popup-view-registrant .touchcma").hide();
     }

        if("log" in temp_leads_arr[curRID]) {
             if("docusign" in temp_leads_arr[curRID]["log"]) {
                     $(".docusign-status").show();
                     if(temp_leads_arr[curRID]["log"]["docusign"] == "Signed") {
                             $("#registrant-docusign-status").html("Document signed.");
                     } else  if(temp_leads_arr[curRID]["log"]["docusign"] == "Declined") {
                             $("#registrant-docusign-status").html("Customer declined to sign.");
                     } else {
                             $("#registrant-docusign-status").html("Signing process incomplete.");
                     }

             } else {
                     $(".docusign-status").hide();
             }

             if("homekeepr" in temp_leads_arr[curRID]["log"]) {
            if(!("homekeeprEnabled" in user["integrations"]) || user["integrations"]["homekeeprEnabled"] != "YES") {
                $("#popup-view-registrant .homekeepr").hide();
            } else {
                $("#popup-view-registrant .homekeepr").show();
                $("#popup-view-registrant .homekeepr-answer").html(temp_leads_arr[curRID]["log"]["homekeepr"]);
            }
        } else {
            $("#popup-view-registrant .homekeepr").hide();
        }
     } else {
        $(".docusign-status").hide();
        $("#popup-view-registrant .homekeepr").hide();
     }
  /*****************************************************************/

    if (user["type"] == "freemium" || isBroker) {
        $("#registrant-score").html("--");
        temp_leads_arr[curRID]["leadscore"] = "--";
    } else {
        var score = calculateLeadScore(index);
        temp_leads_arr[curRID]["leadscore"] = score;
        $("#registrant-score").html(score);
    }

}
