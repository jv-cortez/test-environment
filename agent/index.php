<?php
  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  require_once('../oh/config/db_conn.php');
  require_once('../oh/require/core.php');

  $_agentID = $_GET["g"];

  $collection_managers = Db_Conn::getInstance()->getConnection()->managers;

  $manager_result = $collection_managers->findOne(array('ukey' => $_agentID));

  list($width, $height) = getimagesize($manager_result["pphoto"]);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="assets/img/favicon.png">

  <title>Spacio: Introducing the Paperless Open House</title>

  <!-- Facebook meta tags -->
  <meta property="fb:app_id" content="255792991253800" />
  <meta property="og:url" content="http://stage.spac.io/agent/?g=<?php echo $_agentID ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $manager_result["fname"] .' ' . $manager_result["lname"] ?>" />
  <meta property="og:description" content="<?php echo $manager_result["brokerage"] ?>" />
  <meta property="og:image" content="<?php echo $manager_result["pphoto"] ?>" />
  <meta property="og:site_name" content="Spacio: Introducing the Paperless Open House" />
  <meta property="og:image:height" content="<?php echo $height ?>" />
  <meta property="og:image:width" content="<?php echo $width ?>" />

  <!-- script for the twitter crawler -->
  <script>window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
      t._e.push(f);
    };

    return t;
  }(document, "script", "twitter-wjs"));</script>

  <!-- Twitter meta tags -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="<?php echo $manager_result["fname"] .' ' . $manager_result["lname"] ?>" />
  <meta name="twitter:description" content="<?php echo $manager_result["title"] ?>" />
  <meta name="twitter:image" content="<?php echo $manager_result["pphoto"] ?>" />

  <link href="../assets/css/normalize.css" rel="stylesheet">
  <link href="../assets/css/agent.css" rel="stylesheet">
  <link href="../assets/fonts/fonts.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="http://www.spac.io/assets/img/favicon.png"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

</head>

<body style="margin:0;padding:0;">

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <div class="spacio-top-menu">
    <div class="web-menu">
      <a href="/">
         <div class="spacio-top-menu-logo">
           <img src="/assets/img/website/spacio_logo_long.png" height="40">
         </div>
      </a>
    </div>
  </div><!-- end of spacio-top-menu -->

  <div class="main-container">

    <div class="main-left-container">

      <div class="agent-photo-section">
        <div class="pphoto-container">
          <div class="pphoto" style="background-image:url('/assets/img/website/pphoto.jpg');"></div>
        </div>
        <div class="name-container">
          <div class="name"><span class="fname"></span> <span class="lname"></span></div>
          <div class="title"></div>
        </div>
      </div> <!-- end of agent-photo-section -->

      <div class="agent-info-section">
        <div class="agent-info-titles">
          <div class="phone-number-title"></div>
          <div class="mobile-phone-number-title"></div>
          <div class="website-title"></div>
          <div class="email-title"></div>
        </div>
        <div class="agent-info-values">
          <div class="phone-number">
            <a class="phone"></a>
          </div>
          <div class="phone-number">
            <a class="mobile"></a>
          </div>
          <div class="web">
            <a class="website"></a>
          </div>
          <div class="email"></div>
        </div>
      </div><!-- end of agent-info-section -->

      <div class="sharing-buttons">
        Share Profile:
        <div class="twitter-button" id="twitter-button"></div>

        <!-- Facebook share button code -->
        <div class="fb-share-button" data-href="http://stage.spac.io/agent/?g=<?php echo $_agentID ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fstage.spac.io%2Fagent%2F%3Fg%3D<?php echo $_propertyID ?>&amp;src=sdkpreparse">Share</a></div>
      </div> <!-- end of sharing-buttons -->

      <div class="brokerage-info-section">
        <div class="brokerage-logo-container">
          <div class="brokerageLogo"></div>
        </div>
        <div class="brokerage-contact-info">
          <div class="brokerageName"></div>
          <div class="office"></div>
          <div class="bWebsite">
            <a class="brokerageDomain"></a>
          </div>
        </div>
      </div><!-- end of brokerage-info-section  -->





    </div> <!-- end of main-left-container  -->

<!-- this container will  be visible in desktop view on the right -->
<!-- in mobile, this will be one of the last sections in line with above -->
    <div class="main-right-container">
      <div class="main-right-title"><strong>Active Listings:</strong></div>
      <div class="listings">
      <!-- The listings html will load with javascript here -->
      </div>
    </div> <!-- end of main-right-container -->

  </div><!-- end of main-container  -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/assets/js/website.js"></script>
<script type="text/javascript">

  var wsURL = "../oh/webservice/1.1.5/webservice.php";

	$(document).ready(function(){
    getAgent();
  })

  function getAgent(completionHandler) {
      var _query = {};

      _query["fn"] = "getAgentInfo";
      _query["agentID"] = "<?php echo $_agentID?>";

      var json_data = _query;

      console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

      console.log(json_data);

      $.ajax({
        url: "http://ws.spac.io/stage/oh/webservice_agent/1.0.0/webservice_agent.php",
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){

          console.log(data);

          if (data.status == 1) {
            console.log("Returned JSON Object: "+JSON.stringify(data));

            $(".agentID").html("<?php echo $_agentID ?>");
            $(".brokerageName").html(data.brokerageName);
            if (data.brokerageDomain !== ""){
              $(".brokerageDomain").html(data.brokerageDomain);
              $(".brokerageDomain").attr('href', 'http://www.'+data.brokerageDomain);
            }
            $(".office").html(data.office);
            $(".fname").html(data.fname);
            $(".lname").html(data.lname);
            $(".title").html(data.title);
            if (data.email !== ""){
              $(".email-title").html('Email:');
              $(".email").html(data.email);
              $(".email").attr('href', data.email);
            }
            if (data.phone !== ""){
              $(".phone-number-title").html('Phone:');
              $(".phone").html(data.phone);
              $(".phone").attr('href', 'tel:+1-'+data.mobile);
            }
            if (data.mobile !== ""){
              $(".mobile-phone-number-title").html('Mobile:');
              $(".mobile").html(data.mobile);
              $(".mobile").attr('href', 'tel:+1-'+data.mobile);
            }
            if (data.website !== ""){
              $(".website-title").html('Website:');
              $(".website").html(data.website);
              $(".website").attr('href', data.website);
            }
            $(".pphoto").attr('style', 'background-image: url('+data.pphoto+')');
            $(".brokerageLogo").attr('style', 'background-image: url('+data.brokerageLogo+')');

            $(".listings").html("");
            $.each(data.properties, function(index, value){
              $(".listings").append("<a href='http://stage.spac.io/property/?u="+value['propertyID']+"'><div class='listing'><div class='listing-image-container'><div class='listing-image' style='background-image:url("+value['image']+")'></div></div><div class='listing-details'><div class='listing-address'>"+value['addr1']+", "+value['addr2']+"</div><div class='listing-price'>$"+value['price']+"</div></div></div></a>")
            });

// This function creates the share button and specifies the default tweet text.
            var url = window.location.href+'';
            twttr.widgets.createShareButton(
            url,
            document.getElementById('twitter-button'),
            {
              count: 'none',
              text: 'This realtor was great to work with!'
            }).then(function (el) {
              console.log("Button created.");
              twttr.widgets.load();

            });
            twttr.widgets.load();


            if ($.isFunction(completionHandler)) {
              completionHandler();
            }

          } else {

          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getListing error Error: "+errorThrown);
          loading(false);
        },
        dataType: "json"
      });
    }
</script>

</body>
</html>
