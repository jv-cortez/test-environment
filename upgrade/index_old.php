<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_email = $_GET["e"];
	$_promo = $_GET["p"];
	$_plan = $_GET["plan"];
	$_grandfather = $_GET["g"];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/assets/img/favicon.png">

        <title>Spacio: Introducing the Paperless Open House</title>

        <link href="/assets/css/normalize.css" rel="stylesheet">
        <link href="/assets/css/website.css" rel="stylesheet">
        <link href="/assets/css/upgrade.css" rel="stylesheet">

        <link href="/assets/fonts/fonts.css" rel="stylesheet">

        <link rel="import" id="topmenu" href="/topmenu.html">
        <link rel="import" id="footer" href="/footer.html">
        <link rel="import" id="popups" href="/popups.html">
        <link rel="import" id="alertbox" href="/alertbox.html">

      </head>

<body style="padding-top:0;">

	<div class="spacio-top-menu"></div>
    <!-- TOP MENU END -->
	 <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
    <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="upgrade-scroll-one">
      <div class="overlay-flex">
      	 <form id="upgradeToPremium_Form" style="">
         	 <input type="hidden" name="fn" value="upgradeAccount" />
             <input type="hidden" name="promo" value="<?php echo $_promo?>">
              <div style="width:100%;height:60px;"></div>
							<div class="xlarge" style="text-align:center;margin-bottom:10px;">
         	    Upgrade Your Spacio Account
          		</div>

              <div class="normal" style="text-align:center;margin-bottom:20px;line-height:1.3;" id="custom-message">

              </div>
							<div class="upgrade-input-box" style="line-height:40px;">
								<div class="upgrade-label">
                  Country
                </div>
								<select name="country" class="upgrade-select" onchange="countryChanged(this)">
									<option value="US">United States</option>
									<option value="CA">Canada</option>
									<option value="Other">Other</option>
								</select>
							</div>
							<div class="upgrade-input-box province-input-box" style="line-height:40px;display:none;">
								<div class="upgrade-label">
                  Province
                </div>
								<select name="province" class="upgrade-select" onchange="provinceChanged(this)">
									<option value="none">Select a Province</option>
									<option value="AB">Alberta</option>
									<option value="BC">British Columbia</option>
									<option value="MB">Manitoba</option>
									<option value="NB">New Brunswick</option>
									<option value="NL">Newfoundland</option>
									<option value="NS">Nova Scotia</option>
									<option value="NT">North West Territories</option>
									<option value="NU">Nunavut</option>
									<option value="ON">Ontario</option>
									<option value="PE">Prince Edward Island</option>
									<option value="QC">Quebec</option>
									<option value="SK">Saskatchewan</option>
									<option value="YT">Yukon</option>
								</select>
							</div>
							<div class="upgrade-input-box-2">
                <div class="upgrade-label-2">
                  Plan
                </div>
								<div class="upgrade-inputs" style="line-height:1.4;font-size:16px;">
									<input type="radio" name="plan" value="annual-std-new" id="annual-plan" checked>
									<div style="margin-left:5px;font-size:20px;display:inline-block;vertical-align:top;">Annual Plan<br/>
										$240 / year ($60 savings!)
										<div class="taxes-annual"></div>
									</div><br/><Br/>
									<input type="radio" name="plan" value="pro-std-new" id="monthly-plan">
									<div style="margin-left:5px;font-size:20px;display:inline-block;vertical-align:top;">Monthly Plan<br/>
										<span class="monthly-amount">$25</span> / month
										<div class="taxes-monthly"></div>
									</div>

								</div>
              </div>
              <div class="upgrade-input-box">
                <div class="upgrade-label">
                  Email
                </div>
                <input class="upgrade-input" type="text" name="email" value="<?php echo $_email;?>" id="email" tabindex="1"  placeholder="">
              </div>
              <div class="upgrade-input-box">
                <div class="upgrade-label">
                  Card Number
                </div>
                <input class="upgrade-input" type="text" data-stripe="number" value="" size="20" tabindex="2"  placeholder="">
              </div>
              <div class="upgrade-input-box">
                <div class="upgrade-label">
                  CVC
                </div>
                <input class="upgrade-input" type="text" data-stripe="cvc" value="" size="4" tabindex="3"  placeholder="">
              </div>
              <div class="upgrade-input-box" style="line-height:40px;">
                <div class="upgrade-label">
                  Expiration
                </div>
                <input class="upgrade-input" type="text" data-stripe="exp-month" value="" tabindex="4" size="2"  placeholder="MM" style="width:30px"> /
                <input class="upgrade-input" type="text" data-stripe="exp-year" value="" tabindex="4" size="4"  placeholder="YYYY" style="width:45px">
              </div>
							<div class="small">
								*Prices listed in U.S. Dollars.
							</div>
              <div style="width:100%;text-align:right">
            	<input class="upgrade-submit-btn" type="submit" name="upgrade" value="Upgrade">
          		</div>
         </form>
      </div>
     </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>

<script type="text/javascript">
	var promo = "<?php echo $_promo;?>";
	var plan = "<?php echo $_plan;?>";
	var grandfathered = "<?php echo $_grandfather;?>";
	var tax_rates = {};

	var tax_type = "";
	var tax_rate = 0;
	var country = "";
	var province = "";

	 //Stripe.setPublishableKey('pk_test_1Xk12UcY2hyb1y3cLVTkk7Rs');
	 Stripe.setPublishableKey('pk_live_QRFV0CvJO6Iq122LsxWt93nv');

	 $(document).ready(function(){

		  insertAlertBox();

			if(grandfathered == "yes") {
				$(".monthly-amount").html("$15");
			}
	 });

	$( "form#upgradeToPremium_Form" ).submit(function( event ) {
		var $form = $(this);
		var _fields = $( this ).serializeArray();
		var _query = {};

		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})

		if(_query["email"] == ""){
			showAlert("error","Email Required", "Please enter the email assigned to your Spacio account.","Ok");
		} else {
			$form.find('button').prop('disabled', true);
			Stripe.card.createToken($form, function(status, response) {
				if(response.error) {
					showAlert("error","Error Processing Card", response.error.message,"Ok");
				} else {
					var token = response.id;
					_query["stripeToken"] = token;
					_query["taxRate"] = tax_rate;
					_query["taxType"] = tax_type;
					_query["country"] = country;
					_query["province"] = province;

					if(grandfathered == "yes") {
						_query["stripeToken"] = "pro-std";
					}

					var json_data = _query;
					console.log("Outbound JSON Query: "+JSON.stringify(json_data));

					$.ajax({
						url: "../oh/webservice/1.1.6/webservice.php",
						type:"POST",
						data: {json : json_data},
						success: function(data, textStatus, jqXHR){
							console.log("Returned JSON Object: "+JSON.stringify(data));

							if (data.status == 1) {
								showAlert("info","Thank you for Upgrading","Thank You for Upgrading! Please login again to see the upgrade reflected.","Sign In","Cancel", function(){
								window.location = "http://spac.io/dashboard/";
								}, null);
							} else if (data.status == 3) {
								showAlert("info","No need to upgrade","You already have an upgraded Spacio account. Please Sign In.","Login","Cancel", function(){
								window.location = "http://spac.io/dashboard/";
								}, null);
							} else {
								if (data.error_type == "invalid_request_error" && data.error_param == "coupon") {
									showAlert("error","Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
								} else if (data.error_type =="card_error" && data.error_code == "card_declined"){
									showAlert("error","Card Declined","Sorry, your card was declined. Please verify that you entered a valid credit card.");
								} else {
									showAlert("error","Unknowns","There was an unknown error. Please contact support.");
								}
							}
							//debugLog("Query Status: "+data.status);

						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
						},
						dataType: "json"
					});

				}

			});
		}
		event.preventDefault();
	});



	function randomString(_length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

		var randomstring = '';
		for (var i=0; i<_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}

		return randomstring;
	}

	function countryChanged(target) {
		country = $(target).val();

		if(country == "CA") {
			$(".province-input-box").show();
			getTaxRate("CA");
		} else {
			$(".province-input-box").hide();
		}
		//alert($(target).val());
	}

	function provinceChanged(target) {
		province = $(target).val();

		if(tax_rates[province]["average_rate"]["label"] == "HST") {
			tax_rate = tax_rates[province]["average_rate"]["rate"] * 100;
			tax_type = tax_rates[province]["average_rate"]["label"];
		} else {
			tax_rate = tax_rates[province]["minimum_rate"]["rate"] * 100;
			tax_type = tax_rates[province]["minimum_rate"]["label"];
		}

		$(".taxes-annual").html("(+"+Math.floor(tax_rate)+"% "+tax_type+")");
		$(".taxes-monthly").html("(+"+Math.floor(tax_rate)+"% "+tax_type+")");
		//alert($(target).val());
	}

	function getTaxRate(code) {
		var _query = {};

		_query["code"] = code;
		var json_data = _query;
		console.log("Outbound JSON Query: "+JSON.stringify(json_data));

		$.ajax({
			url: "https://ws.spac.io/prod/oh/webservice_taxjar/getTaxRate.php",
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				$.each(data, function(index,value){
					tax_rates[value["region_code"]] = value;
				});
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});

	}
	/*
	var agent = navigator.userAgent;
	var isIphone = ((agent.indexOf('iPhone') != -1) || (agent.indexOf('iPod') != -1)) ;
	if (isIphone) {
		$("#iphone-msg").html("Returning you to the app.");
		setTimeout(function(){window.location.href = 'SpacioiOS://'; }, 2500);

	}
	*/

</script>



</body>
</html>
