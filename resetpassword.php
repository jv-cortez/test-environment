<?php
	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  $_id = $_GET["e"];
  $_rtoken = $_GET["r"];
?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Reset Password</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
     <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="signup-scroll-one">
      <div class="overlay-flex">
        <form id="resetPass_Form" action="index.php" method="POST" style="height:auto">
          <input type="hidden" name="fn" value="resetPass" />
          <div class="xlarge" style="text-align:center;margin-bottom:20px;">
              Choose a New Password
          </div>

          <div class="signup-input-box">
            <div class="signup-label">
              Password
            </div>
            <input class="signup-input" type="password" name="pw" value="" id="pw" tabindex="100"  placeholder="">
          </div>

          <div class="signup-input-box">
            <div class="signup-label">
              Repeat Password
            </div>
            <input class="signup-input" type="password" name="pw2" value="" id="pw2" tabindex="101"  placeholder="">
          </div>

          <div style="width:100%;text-align:right">
            <input class="signup-submit-btn" type="submit" name="signup" value="RESET PASSWORD">
          </div>
        </form>
      </div>

    </div>
    <!-- SCROLL ONE END -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
    <script>

    var wsURL = "../oh/webservice/1.1.0/webservice.php";

    $(document).ready(function(){
      insertAlertBox();

    });

    $( "form#resetPass_Form" ).submit(function( event ) {

		//Serialize form inputs
		var _fields = $( this ).serializeArray();
		var _query = {};

		//Format form inputs into proper array
		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];

		})

		//Validate form inputs.
		if (_query["pw"] == "") {
			showAlert("error","Choose A Password","Please enter a password.");
		} else if (_query["pw"] != _query["pw2"]) {
			showAlert("error","Passwords Must Match","Please enter matching passwords.");
		} else if(_query["pw"].length < 6) {
			showAlert("error","Invalid Password","Passwords must be at least 6 characters.");
		} else {

			//deletes the "pw2" value so it never leaves the client side
			delete _query["pw2"];

			//Salt + Hash password before sending to server
			var _salt = randomString(16);
			_query["managerID"] = "<?php echo $_id?>";
			_query["rtoken"] = "<?php echo $_rtoken?>";

			_query["salt"] = _salt;
			_query["pw"] += _salt;
			_query["pw"] = CryptoJS.SHA3(_query["pw"], { outputLength: 224 }).toString();

			var json_data = _query;
			console.log("Outbound JSON Query: "+JSON.stringify(json_data));
			$.ajax({
				url: wsURL_new,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {
						showAlert("info","Password Successfully Reset","Your password has been successfully reset.","Ok",null, function(){ window.location.href="http://www.spac.io/dashboard/"},null);
					} else {
						showAlert("error","Error","We could not reset your password. The reset link may have expired. Please request a new reset link or contact support.");
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
				},
				dataType: "json"
			});
		}
		event.preventDefault();
	});

    </script>
  </body>
</html>
