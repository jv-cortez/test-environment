
<?php

require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/stripe-php-3.14.3/init.php');

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

class Sys_Query {
	/* ------------------------------ MANAGER RELATED FUNCTIONS ------------------------------*/
	// Type defaults to user, other possible: admin, superadmin
	
	static function checkSystemStatus($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_system_status = Db_Conn::getInstance()->getConnection()->system_status;
		
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		
		if (!is_null($manager_result)) {
			$system_status_result = $collection_system_status->findOne(array('_id' => new MongoId("58334935f9b2a12ef0211074")));
			$return = array("status" => 1,
				"system_status" => $system_status_result
			);
			
			
			
		} else {
			$return = array("status" => 0);
		}
		
		return $return;
		
		
	}

}



?>