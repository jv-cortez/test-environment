<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_accessToken = isset($_GET['a']) ? $_GET['a'] : '';
  $_email = isset($_GET['e']) ? $_GET['e'] : '';
	$_unclaimed_id = isset($_GET['u']) ? $_GET['u'] : '';

?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Introducing the Paperless Open House</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <style>
    .loading-ellipsis {
    }

    .loading-ellipsis:after {
      overflow: hidden;
      display: inline-block;
      vertical-align: bottom;
      -webkit-animation: ellipsis steps(4,end) 900ms infinite;
      animation: ellipsis steps(4,end) 900ms infinite;
      content: "\2026"; /* ascii code for the ellipsis character */
      width: 0px;
    }

    @keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes side-pan {
        0% {background-position: left center;}
      50% {background-position: right center;}
      100% {background-position: left center;}
    }
    </style>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
	 <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
    <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->
    <div style="height:100vh;width:100%;background:#eee;display:flex;flex-direction:column;align-items:center;justify-content:center;">
      <div style="font-size:40px;width:400px" class="loading-ellipsis">
        Logging into Spacio
      </div>
			<div style="font-size:40px;width:400px;display:none;" class="choose-dashboard">
        Please choose which dashboard you would like to access:<br/><Br/>
				<div style="cursor:pointer;display:inline-block;width:195px;padding:20px;box-sizing:border-box;background-color:#333;color:white;font-size:20px;text-align:center;" onclick="goToAgentDashboard()">
					Agent<br/>Dashboard
				</div>
				<div style="cursor:pointer;display:inline-block;width:195px;padding:20px;box-sizing:border-box;background-color:#666;color:white;font-size:20px;text-align:center;" onclick="goToBrokerageDashboard()">
					Manager<br/>Dashboard
				</div>
      </div>
      <div style="font-size:20px;width:600px;display:none;line-height:1.4" class="error-message">
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
		<script src="../js/mobile-detect.js"></script>
		<script src="/assets/js/jquery.countdown.min.js"></script>
    <script>
    var a = "<?php echo $_accessToken;?>";
    var email = "<?php echo $_email;?>";
		var uid = "<?php echo $_unclaimed_id;?>";
    var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";

		var md = new MobileDetect(window.navigator.userAgent);

    $(document).ready(function(){
			var os = md.os();
			var isIphone = 	md.is('iPhone');
			var tablet = 	md.tablet();

			if(os == "iOS" || os == "AndroidOS") {
				if(uid != "") {
					claimAccount();
				} else {
					loginManagerByAccessToken();
				}
			} else {
				checkAccountTypes();
			}
    });

		function goToBrokerageDashboard() {
			window.location.href = 'https://spac.io/brokerages/dashboard/sso/?e='+email+'&a='+a;
		}

		function goToAgentDashboard() {
			if(uid != "") {
				claimAccount();
			} else {
				loginManagerByAccessToken();
			}
		}

		function checkAccountTypes(completionHandler) {
			var _query = {};
	 		_query["fn"] = "checkAccountTypes";
	 		_query["email"] = email;
			_query["atoken"] = a;

			var json_data = _query;
    	console.log("Outbound JSON Query: "+JSON.stringify(json_data));

			$.ajax({
				 url: wsURL,
				 type:"POST",
				 data: {json : json_data},
				 success: function(data, textStatus, jqXHR){
						 console.log("Returned JSON Object: "+JSON.stringify(data));

						 if (data.status == 1) {
							 console.log("Has brokerage admin account");
							 $(".loading-ellipsis").hide();
							 $(".choose-dashboard").show();
						 } else if (data.status == 2) {
							 	if(uid != "") {
				 					claimAccount();
				 				} else {
				 					loginManagerByAccessToken();
				 				}
						 } else {
							 $(".loading-ellipsis").hide();
							 $(".error-message").show();
							 $(".error-message").html("Unknown Error. Please contact support.");
						 }
						 //loading(false);
						 //debugLog("Query Status: "+data.status);
				 },
				 error: function(jqXHR, textStatus, errorThrown) {
						 console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Account Error: "+errorThrown);
				 },
				 dataType: "json"
		 });

		}

		function claimAccount() {

			var _query = {};
	 		_query["fn"] = "claimAccount";
	 		_query["id"] = uid;
	 		_query["email"] = email;
	 		_query["pw"] = generateRandString(16);
	 		_query["from"] = "59a7e3fea0d2ca211c7f2979";

			var _salt = generateRandString(16);

    	//Salt + Hash password before sending to server

    	_query["salt"] = _salt;
    	_query["pw"] += _salt;
    	_query["pw"] = CryptoJS.SHA3(_query["pw"], { outputLength: 224 }).toString();

    	var json_data = _query;

			$.ajax({
				 url: wsURL,
				 type:"POST",
				 data: {json : json_data},
				 success: function(data, textStatus, jqXHR){
						 console.log("Returned JSON Object: "+JSON.stringify(data));

						 if (data.status == 1) {
							 var os = md.os();
								var isIphone = 	md.is('iPhone');
								var tablet = 	md.tablet();


								if(os == "iOS") {
									if(isIphone) {
										window.location.href = 'spaciom://?i='+data.managerID+"&v="+data.vtoken;
									} else if(tablet == "iPad") {
										window.location.href = 'spacio://?i='+data.managerID+"&v="+data.vtoken;
									}
								} else if(os == "AndroidOS") {
									if(tablet !== null) {
										window.location.href = 'spacio://?i='+data.managerID+"&v="+data.vtoken;
									} else {
										window.location.href = 'spaciom://?i='+data.managerID+"&v="+data.vtoken;
									}
								} else {
									localStorage['id'] = data.managerID;
									localStorage['vtoken'] = data.vtoken;
									window.location.href = 'https://spac.io/dashboard/';
								}
						 } else if (data.status == 2) {
							 $(".loading-ellipsis").hide();
	             $(".error-message").show();
	             $(".error-message").html("Unknown Error. Please contact support. (Status code 2)");

						 } else {
							 $(".loading-ellipsis").hide();
							 $(".error-message").show();
							 $(".error-message").html("Unknown Error. Please contact support.");

						 }
						 //loading(false);
						 //debugLog("Query Status: "+data.status);
				 },
				 error: function(jqXHR, textStatus, errorThrown) {
						 console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Account Error: "+errorThrown);
				 },
				 dataType: "json"
		 });

		}

    function loginManagerByAccessToken() {

    	var _query = {};

      _query["fn"] = "loginManagerByAccessToken";
      _query["a"] = a;
      _query["email"] = email;

      var json_data = _query;

      $.ajax({
        url: wsURL,
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){
          console.log("Returned JSON Object: "+JSON.stringify(data));

          if (data.status == 1) {


						var os = md.os();
						var isIphone = 	md.is('iPhone');
						var tablet = 	md.tablet();


						if(os == "iOS") {
							if(isIphone) {
								window.location.href = 'spaciom://?i='+data.creds["id"]+"&v="+data.creds["vtoken"];
							} else if(tablet == "iPad") {
								window.location.href = 'spacio://?i='+data.creds["id"]+"&v="+data.creds["vtoken"];
							}
						} else if(os == "AndroidOS") {
							if(tablet !== null) {
								window.location.href = 'spacio://?i='+data.creds["id"]+"&v="+data.creds["vtoken"];
							} else {
								window.location.href = 'spaciom://?i='+data.creds["id"]+"&v="+data.creds["vtoken"];
							}
						} else {
							localStorage['id'] = data.creds["id"];
	            localStorage['vtoken'] = data.creds["vtoken"];
	            window.location.href = 'https://spac.io/dashboard/';
						}

          } else {
            $(".loading-ellipsis").hide();
            $(".error-message").show();
            $(".error-message").html("We could not log you in. This may be because you have yet to claim your Spacio account. You can receive a claim link by visiting this page:<br/><br/><u><a style='font-family:effra_regular;' href='https://spac.io/claimemail/'>https://spac.io/claimemail/</a></u>");

          }

        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Login Error: "+errorThrown);
          $(".error-msg").html("Connection Error.");
        },
        dataType: "json"
      });

    }

		function generateRandString(l)
		{
		    var text = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		    for( var i=0; i < l; i++ )
		        text += possible.charAt(Math.floor(Math.random() * possible.length));

		    return text;
		}

    </script>

  </body>
