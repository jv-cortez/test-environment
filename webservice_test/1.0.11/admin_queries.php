<?php

require_once('../../require/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/vendor/autoload.php');


class Adm_Query {

	function getAllRegistrantsIDs() {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;

		$rids = array();
		$registrants = $collection_registrants->find();
		foreach ($registrants as $registrant) {
			if(array_key_exists("fullcontact", $registrant)) {
				if(empty($registrant["fullcontact"])) {
					$rids[] = $registrant["_id"]->{'$id'};
				}
			}  else {
				$rids[] = $registrant["_id"]->{'$id'};
			}
		}

		$return = array("status" => 1, "rids" => $rids);
		return $return;
	}

	function calculateHosted() {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		$cursor = $collection_registrants->find();
		$count = 0;
		foreach ($cursor as $doc) {
			$hostedTime = $doc["dateCreated"]."";
			$propertyID = $doc["propertyID"];

			$date = $hostedTime/1000;
			$dayHosted = date( "l", $date);

			$property_result = $collection_properties->findOne(array("propertyID" => $propertyID));

			if (!is_null($property_result)) {
				$pid = $property_result["_id"]->{'$id'};
				$managerID = $doc["managerID"];

				$plus_one_day = $date + 86400 * 1000;
				$tomorrow = new MongoDB\BSON\UTCDateTime($plus_one_day);
				$today = new MongoDB\BSON\UTCDateTime($date);
				
				$condition = array('managerID'=>$managerID,'pid'=>$pid,'dateHosted' => array('$lt'=>$tomorrow, '$gt'=>$today) );
				$hosted_result = $collection_hosted->findOne($condition);

				if (is_null($hosted_result)) {
					$count++;

					$document_hosted = array(
										"pid" => $pid,
										"managerID" => $managerID,
										"dateHosted" => $hostedTime,
										"dayHosted" => $dayHosted,
										);


					$result = $collection_hosted->insertOne($document_hosted);

				}
			}

		}

		$return = array("status" => 1, "count" => $count, "tomorrow" => $datestring);
		return $return;
	}

	function moveDeletedRegistrants() {
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_deleted_registrants = Db_Conn::getInstance()->getConnection()->deleted_registrants;


		$cursor = $collection_registrants->find();
		$count = 0;
		foreach ($cursor as $doc) {
			$propertyID = $doc["propertyID"];
			$property_result = $collection_properties->findOne(array("propertyID" => $propertyID));

			if (is_null($property_result)) {
				$collection_registrants->deleteOne(array('_id' => $doc["_id"]));
				//$collection_deleted_registrants->insert($doc);
				$count++;

			}
		}

		$return = array("status" => 1, "count" => $count);
		return $return;

	}

	function createKeys() {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$cursor = $collection_managers->find();
		foreach ($cursor as $doc) {
			$man_fname = $doc["fname"];
			$man_lname = $doc["lname"];
			$man_key = $man_fname."".$man_lname;
			$man_key = strtolower($man_key);
			$counter = 1;
			$duplicate_result = $collection_managers->findOne(array('ukey' => $man_key));

			//If there is a duplicate key, iterate counter until result is null;
			while(!is_null($duplicate_result)) {
				$counter++;
				$man_key = $man_key."".$counter;
				$duplicate_result = $collection_managers->findOne(array('ukey' => $man_key));
			}

			$newdata = array('$set' => array('ukey' => $man_key));
			$u_result = $collection_managers->updateOne(array('_id' => $doc["_id"]), $newdata);
		}

		$return = array("status" => 1);
		return $return;

	}

	function createDefaultTemplate() {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->custom_emails;
		$cursor = $collection_managers->find();
		foreach ($cursor as $doc) {
			$doc_id = $doc["_id"]->{'$id'};

			$body = '<p>Hi {{name}},</p><p>Thanks for coming by!</p><p> Here are the details of today\'s open house:</p>{{oh_title}}<br/>Address: {{oh_addr1}} {{oh_addr2}}<br/>Website: {{oh_url}}<br/>Bed: {{oh_beds}}<br/>Bath: {{oh_baths}}<br/>Price: {{oh_price}}<br/>Details: {{oh_details}}<br/><p>Please let me know if you have any questions, I\'m happy to help.</p><br/><p>Thank you,</p>';

			if ($doc['brokerage'] == "" || $doc['brokerage'] == "N/A") {
				$brokerage = "";
			} else {
				$brokerage = $doc['brokerage']."<br/>";
			}

			if ($doc['email'] == "" || $doc['email'] == "N/A") {
				$email = "";
			} else {
				$email = $doc['email']."<br/>";
			}

			if ($doc['phone'] == "" || $doc['phone'] == "N/A") {
				$phone = "";
			} else {
				$phone = $doc['phone']."<br/>";
			}

			if ($doc['website'] == "" || $doc['website'] == "N/A") {
				$website = "";
			} else {
				$website = $doc['website']."<br/>";
			}

			$signature = $doc['fname'].' '.$doc['lname'].'<br/>'.$brokerage.''.$email.''.$phone.''.$website;

			$document_email = array("managerID" => $doc_id,
									"body" => $body,
									"signature" => $signature);

			$result = $collection_emails->insertOne($document_email);
		}

		$return = array("status" => 1);
		return $return;

	}

	function registerManager($fname, $lname, $email, $phone, $website, $brokerage, $pw, $salt) {

	  $date = new DateTime();
		$timeStamp = $date->getTimestamp() * 1000;
		$currentTime = new MongoDB\BSON\UTCDateTime($timeStamp);
		$expired = new MongoDB\BSON\UTCDateTime($timeStamp - (86400 * 1000)); // 12 hours 43200 * 1000

		$vtoken = generateRandomString(16);

	  $collection = Db_Conn::getInstance()->getConnection()->managers;
	  $s_result = $collection->findOne(array('email' => $email));

	  if (!is_null($s_result)) {
			//Email already exists
			$return = array("status" => 2);
	  } else {

		  //$getDate = date('Y-m-d H:i:s');
		  $document_manager = array("fname" => $fname,
									"lname"=> $lname,
									"title" => "",
									"teamID" => "N/A",
									"teamRole" => "N/A",
									"pphoto" => "http://spaciopro.com/img/stock/pphoto.jpg",
									"email"=> $email,
									"phone" => $phone,
									"mobile" => "",
									"addr1" => "",
									"addr2" => "",
									"brand" => "",
									"website"=>$website,
									"brokerage"=> $brokerage,
									"type" => "trial",
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"topproducerID" => "N/A",
									"marketsnapID" => "N/A",
									"rsID" => "N/A",
									"emailConfirmed" => true,
									"promocode" => "",
									"rtoken" => "",
									"rtDateCreated" => $expired);

		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result = $collection->insertOne($document_manager);

		  $collection_properties = Db_Conn::getInstance()->getConnection()->properties;

		  $collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;

			$nextseq = $collection_counter->findOneAndUpdate(
				 array("desc" => "propertyID"),
				 array('$inc' => array('maxCount' => 1)),
				 array(),
				 array(
					"new" => true
				)
			);

		  $sample_property = array(
									"propertyID" => $nextseq["maxCount"],
									"url" => "http://www.spaciopro.com",
									"title"=> "SAMPLE: Beautiful Home, Move in Ready",
									"addr1"=> "123 Central Park, New York, NY",
									"addr2"=> "",
									"image"=> "https://s3.amazonaws.com/spacio-user-images/spaciopro_sample.jpg",
									"price" => "2,500,000",
									"beds" => 2,
									"baths" => 2,
									"dimens" => 2000,
									"desc" => "SAMPLE: Beautiful home next to Central Park. Move in ready. Rare opportunity, contact me now for more details.\r\n\r\n*This is a sample open house entry only. Information listed here is not accurate, this is not an offering for sale.",
									//"note" => $note,
									"currency" => "USD",
									"measurement" => "sqft",
									"dateCreated" => $currentTime,
									"lastEdited" => $currentTime,
									"questions" => array(
										array(
											"question"=>"Are you working with an agent?",
											"type"=>"eq1"
										)
									),
									"managerID"=>$document_manager["_id"]->{'$id'},
									"anon"=>"NO",
									"autoEmail" => "NO",
									"cnEnabled" => "NO");

		  //$collection = Db_Conn::getInstance()->getConnection()->managers;
		  $result2 = $collection_properties->insertOne($sample_property);

		  if ($result) {

			$return = array("status" => 1,
							"managerId" => $document_manager["_id"]->{'$id'},
							"vtoken" => $vtoken);
		  } else {
			$return = array("status" => 0);
		  }


      }
	  return $return;
	}

}

?>
