<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_accessToken = isset($_GET['a']) ? $_GET['a'] : '';
  $_email = isset($_GET['e']) ? $_GET['e'] : '';


?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Introducing the Paperless Open House</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <style>
    .loading-ellipsis {
    }

    .loading-ellipsis:after {
      overflow: hidden;
      display: inline-block;
      vertical-align: bottom;
      -webkit-animation: ellipsis steps(4,end) 900ms infinite;
      animation: ellipsis steps(4,end) 900ms infinite;
      content: "\2026"; /* ascii code for the ellipsis character */
      width: 0px;
    }

    @keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes side-pan {
        0% {background-position: left center;}
      50% {background-position: right center;}
      100% {background-position: left center;}
    }
    </style>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
	 <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
    <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->
    <div style="height:100vh;width:100%;background:#eee;display:flex;flex-direction:column;align-items:center;justify-content:center;">
      <div style="font-size:40px;width:400px" class="loading-ellipsis">
        Logging into Spacio
      </div>
      <div style="font-size:20px;width:600px;display:none;line-height:1.4" class="error-message">
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
		<script src="../js/mobile-detect.js"></script>
		<script src="/assets/js/jquery.countdown.min.js"></script>
    <script>
    var a = "<?php echo $_accessToken;?>";
    var email = "<?php echo $_email;?>";
    var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";




    $(document).ready(function(){
			var md = new MobileDetect(window.navigator.userAgent);

			//alert("Is iPhone: "+md.is('iPhone'));
			//alert("OS :"+md.os());
			alert("Is Tablet :"+md.tablet() );          // null
			console.log("User Agent :"+md.userAgent() );
      //loginManagerByAccessToken();
    });

    function loginManagerByAccessToken() {

    	var _query = {};

      _query["fn"] = "loginManagerByAccessToken";
      _query["a"] = a;
      _query["email"] = email;

      var json_data = _query;

      $.ajax({
        url: wsURL,
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){
          console.log("Returned JSON Object: "+JSON.stringify(data));

          if (data.status == 1) {

            localStorage['id'] = data.creds["id"];
            localStorage['vtoken'] = data.creds["vtoken"];
            window.location.href = 'https://spac.io/dashboard/';


          } else {
            $(".loading-ellipsis").hide();
            $(".error-message").show();
            $(".error-message").html("We could not log you in. This may be because you have yet to claim your Spacio account. You can receive a claim link by visiting this page:<br/><br/><u><a style='font-family:effra_regular;' href='https://spac.io/claimemail/'>https://spac.io/claimemail/</a></u>");

          }

        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Login Error: "+errorThrown);
          $(".error-msg").html("Connection Error.");
        },
        dataType: "json"
      });

    }

    </script>

  </body>
