<?php

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_rid = isset($_GET['r']) ? $_GET['r'] : '';
	$_index = isset($_GET['i']) ? $_GET['i'] : '';
	$_bid = isset($_GET['b']) ? $_GET['b'] : '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../css/font-awesome.min.css" rel="stylesheet">

<title>Spacio | Document</title>
</head>

<body>
	<style>

* {
	margin:0;
	padding:0;
}

body {
	font-size:14px;
}

input:disabled, textarea:disabled {
    color: black!important;
		opacity: 1!important;
}

.disclosure-form-content input {
	position:absolute;
	z-index:10;
}

.disclosure-form-content input[type="text"] {
	border-radius:0px;
}

#doc-inputs {
	position:absolute;
    top:0px;
    left:0px;
}

.sig {
	z-index:10;
	position:absolute;
	background-size:contain!important;
	background-repeat:no-repeat;
	background-position:center center;
	background-color:white!important;
	text-align: center;
}

.checkbox {
	position:absolute;
	text-align: center;
	line-height: 17px;
	box-sizing: border-box;
	padding-top:3px;
	font-size:12px!important;
}

input[type="text"] {
	border-radius:0px!important;
	border:none!important;
	background:transparent!important;
	font-size:14px;
}


	</style>
	<div style="position:relative;width:768px;" class="disclosure-form-content">
    	<img id="disclosure-form-image" src="" width="100%">
      	<span id="doc-inputs">

      	</span>

    </div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


	<script src="//cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js"></script>
	<script src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>

    <script>
		var wsURL = "https://ws.spac.io/prod/oh/webservice/1.1.6/webservice.php";

		var rid = "<?php echo $_rid;?>";
		var bid = "<?php echo $_bid;?>";
		var index = <?php echo $_index;?>;

		var curDisclosureForm = {};
		var formData = {};
		var projectDocs = [];

		$(document).ready(function(){

			getDocs(bid,function(){
				getVisitorDocData(rid,function(){
					loadForm();
				})
			});
		});

		function loadForm() {
			$("#disclosure-form-image").attr("src", "../img/docs/"+projectDocs[index]["name"]+".jpg");
			$("#doc-inputs").html("");

			$.each(projectDocs[index]["input"], function(index,value){

						if (value["type"] == "text") {
							$("#doc-inputs").append("<input type='text' value='"+value["value"]+"' name='"+value["name"]+"' style='top:"+value["top"]+"px;left:"+value["left"]+"px;width:"+value["width"]+"px;height:"+value["height"]+"px;"+value["style"]+"' readonly>");

							if("data" in formData) {
								$("input[type='text'][name='"+value["name"]+"']").val(formData["data"][value["name"]]);
							}

						}

						if (value["type"] == "checkbox") {
							$("#doc-inputs").append("<div id='"+value["name"]+"' class='checkbox' style='top:"+value["top"]+"px;left:"+value["left"]+"px;width:"+value["width"]+"px;height:"+value["height"]+"px;"+value["style"]+";font-size:12px;'></div>");
							if("data" in formData) {
								if(formData["data"][value["name"]] == "YES") {
									$("#"+value["name"]).html("<i class='fa fa-check' aria-hidden='true'></i>");

								}
								//$("input[type='checkbox'][name='"+value["name"]+"'][value='"+formData["data"][value["name"]]+"']").last().prop("checked", true);
							}
						}

						if (value["type"] == "sig") {
							$("#doc-inputs").append("<div id='"+value["id"]+"' class='sig' style='background-image:none;top:"+value["top"]+"px;left:"+value["left"]+"px;width:"+value["width"]+"px;height:"+value["height"]+"px;"+value["style"]+"'><img src='' height='100%'></div>");
							if("data" in formData) {
								if (value["id"] in formData["data"]) {
									if(formData["data"][value["id"]] != "") {
										//$("#"+value["id"]).html("");
										$("#"+value["id"]+" img").attr("src",formData["data"][value["id"]]);

										//$("#"+value["id"]).html("");
										//$("#"+value["id"]).css("background-image","url("+formData["data"][value["id"]]+")");

										//$("#"+value["id"]).css("background-size","contain");
										//$("#"+value["id"]).css("background-repeat","no-repeat");
										//$("#"+value["id"]).css("background-position","center center");

										//$("#"+value["id"]).css("background-size","contain");
										//$("#"+value["id"]).css("background-repeat","no-repeat");
										//$("#"+value["id"]).css("background-position","center center");
									}
								}
							}
						}
					});
		}

		function getDocs(brokerageID, completionHandler) {

			var _query = {};

			_query["fn"] = "getDocs";
			_query["brokerageID"] = brokerageID;

			//loading(true, "Loading...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						projectDocs = data.docs;
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getDisclosureFormByID error Error: "+errorThrown);
				},
				dataType: "json"
			});

		}

		function getVisitorDocData(registrantID, completionHandler) {
			var _query = {};

			_query["fn"] = "getVisitorDocData";
			_query["rid"] = registrantID;


			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						//curDisclosureForm = data.disclosureForm;
						formData = data.docs[index];
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getForm error Error: "+errorThrown);
				},
				dataType: "json"
			});

		}



	</script>
</body>
</html>
