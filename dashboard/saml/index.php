<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

/**
 *  SAML Handler
 */

 require_once('../../oh/config/db_conn.php');
 require_once('../../oh/require/core.php');
 $collection_managers = Db_Conn::getInstance()->getConnection()->managers;
 $collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;



session_start();

require_once 'require/_toolkit_loader.php';

require_once 'settings.php';

$auth = new OneLogin_Saml2_Auth($settingsInfo);

if (isset($_GET['sso'])) {
    $auth->login();

    # If AuthNRequest ID need to be saved in order to later validate it, do instead
    # $ssoBuiltUrl = $auth->login(null, array(), false, false, true);
    # $_SESSION['AuthNRequestID'] = $auth->getLastRequestID();
    # header('Pragma: no-cache');
    # header('Cache-Control: no-cache, must-revalidate');
    # header('Location: ' . $ssoBuiltUrl);
    # exit();

} else if (isset($_GET['sso2'])) {
    $returnTo = $spBaseUrl.'/demo1/attrs.php';
    $auth->login($returnTo);
} else if (isset($_GET['slo'])) {
    $returnTo = null;
    $paramters = array();
    $nameId = null;
    $sessionIndex = null;
    $nameIdFormat = null;

    if (isset($_SESSION['samlNameId'])) {
        $nameId = $_SESSION['samlNameId'];
    }
    if (isset($_SESSION['samlSessionIndex'])) {
        $sessionIndex = $_SESSION['samlSessionIndex'];
    }
    if (isset($_SESSION['samlNameIdFormat'])) {
        $nameIdFormat = $_SESSION['samlNameIdFormat'];
    }

    $auth->logout($returnTo, $paramters, $nameId, $sessionIndex, false, $nameIdFormat);

    # If LogoutRequest ID need to be saved in order to later validate it, do instead
    # $sloBuiltUrl = $auth->logout(null, $paramters, $nameId, $sessionIndex, true);
    # $_SESSION['LogoutRequestID'] = $auth->getLastRequestID();
    # header('Pragma: no-cache');
    # header('Cache-Control: no-cache, must-revalidate');
    # header('Location: ' . $sloBuiltUrl);
    # exit();

} else if (isset($_GET['acs'])) {
    if (isset($_SESSION) && isset($_SESSION['AuthNRequestID'])) {
        $requestID = $_SESSION['AuthNRequestID'];
    } else {
        $requestID = null;
    }

    $auth->processResponse($requestID);

    $errors = $auth->getErrors();

    if (!empty($errors)) {
        print_r('<p>'.implode(', ', $errors).'</p>');
    }

    if (!$auth->isAuthenticated()) {
        echo "<p>Not authenticated</p>";
        exit();
    }

    $_SESSION['samlUserdata'] = $auth->getAttributes();

    $temp_attr = $auth->getAttributes();
    $_SESSION['samlNameId'] = $auth->getNameId();
    $_SESSION['samlNameIdFormat'] = $auth->getNameIdFormat();
    $_SESSION['samlSessionIndex'] = $auth->getSessionIndex();
    unset($_SESSION['AuthNRequestID']);
    if (isset($_POST['RelayState']) && OneLogin_Saml2_Utils::getSelfURL() != $_POST['RelayState']) {
        $manager_email = $temp_attr["Email"][0];

        $manager_result = $collection_managers->findOne(array('email' => $manager_email));

        if(!is_null($manager_result)) {
          $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $charsLength = strlen($chars);
          $atoken = '';
           for ($i = 0; $i < 16; $i++) {
               $atoken .= $chars[rand(0, $charsLength - 1)];
           }

           $newdata = array('$set' => array("accessToken" => $atoken));
			     $u_result = $collection_managers->update(array("_id" => $manager_result["_id"]), $newdata);

          $auth->redirectTo("https://spac.io/dashboard/sso/?e=".$manager_email."&a=".$atoken);
        } else {
          $unclaimed_result = $collection_unclaimed_accounts->findOne(array('$or' => array(array('email' => $manager_email),array('email2' => $manager_email)), 'status'=>'unclaimed','active' => array('$ne' => false)));

            if(!is_null($unclaimed_result)) {
              $_id = $unclaimed_result["_id"]->{'$id'};

			        $managerID = "570b6da5d4c6cd2e97fc74f0";
			        $link = "https://spac.io/claim/?e=".$manager_email."&u=".$_id."&f=".$managerID."&b=".$unclaimed_result["brokerageID"];

              $auth->redirectTo($link);

            } else {
                $auth->redirectTo("https://spac.io/dashboard/notfound.html");
            }
        }
    }
} else if (isset($_GET['sls'])) {
    if (isset($_SESSION) && isset($_SESSION['LogoutRequestID'])) {
        $requestID = $_SESSION['LogoutRequestID'];
    } else {
        $requestID = null;
    }

    $auth->processSLO(false, $requestID);
    $errors = $auth->getErrors();
    if (empty($errors)) {
        print_r('<p>Sucessfully logged out</p>');
    } else {
        print_r('<p>'.implode(', ', $errors).'</p>');
    }
}

if (isset($_SESSION['samlUserdata'])) {
    if (!empty($_SESSION['samlUserdata'])) {
        $attributes = $_SESSION['samlUserdata'];
        echo 'You have the following attributes:<br>';
        echo '<table><thead><th>Name</th><th>Values</th></thead><tbody>';
        foreach ($attributes as $attributeName => $attributeValues) {
            echo '<tr><td>' . htmlentities($attributeName) . '</td><td><ul>';
            foreach ($attributeValues as $attributeValue) {
                echo '<li>' . htmlentities($attributeValue) . '</li>';
            }
            echo '</ul></td></tr>';
        }
        echo '</tbody></table>';
        echo '<br>Debug:<br>';
        var_dump($attributes);
    } else {
        echo "<p>You don't have any attribute</p>";
    }

    echo '<p><a href="?slo" >Logout</a></p>';
} else {
    echo '<p><a href="?sso" >Login</a></p>';
    echo '<p><a href="?sso2" >Login and access to attrs.php page</a></p>';
}
