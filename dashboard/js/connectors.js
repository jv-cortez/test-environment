// JavaScript Document

var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";
var wsURL_mls = "https://spac.io/oh/webservice/1.1.6/webservice_mls.php";
var wsURL_new = "https://ws.spac.io/prod/oh/webservice/1.1.7/webservice.php";

function initSpacio(completionHandler) {
        if (user["id"] != null && user["vtoken"] != null )    {
            var _query = {};
            var d = new Date()
            var n = d.getTimezoneOffset();

            //Validation info
            _query["fn"] = "initSpacio";
            _query["managerID"] = user["id"];
            _query["vtoken"] = user["vtoken"];

            //Device, Platform and App info
            _query["timezoneOffset"] = n;
            _query["version"] = navigator.appVersion;
						if (navigator.userAgent.search("MSIE") >= 0) {
							_query["model"] = "Internet Explorer";
				    } else if (navigator.userAgent.search("Chrome") >= 0) {
							_query["model"] = "Chrome";
				    } else if (navigator.userAgent.search("Firefox") >= 0) {
							_query["model"] = "Firefox";
				    } else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
							_query["model"] = "Safari";
				    } else if (navigator.userAgent.search("Opera") >= 0) {
							_query["model"] = "Opera";
				    } else {
							_query["model"] = "Other";
						}
            _query["platform"] = navigator.platform;
            _query["manufacturer"] = "Web";
            _query["uuid"] = "";
            _query["isVirtual"] = false;
            _query["spacio_version"] = version;

            loading(true);

            var json_data = _query;
            console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

            $.ajax({
                url: wsURL_new,
                type:"POST",
                data: {json : json_data},
                success: function(data, textStatus, jqXHR){
                    if (data.status == 1) {

                        //Save User Info
                        $.each(data["user"], function(index, value){
                            if (index != "_id") {
                                user[index] = value;
                            }
                        })
                        localStorage["user"] = JSON.stringify(user);

                        //Save Brokerage Info
                        brokerage = data.brokerage;
                        localStorage["brokerage"] = JSON.stringify(brokerage);

                        //Save Team Info
                        team = data.team;
                        localStorage["team"] = JSON.stringify(team);

                        //Handle Brand info Caching
                        brand = data.brand;
                        localStorage["brand"] = JSON.stringify(brand);

                        if(data.system_status["serverStatus"] == "maintenance") {
                            showAlert("Server Maintenance","We are currently running maintenance on our services and will be back up in "+data.system_status["maintenanceDuration"] +". We apologize for the inconvenience.","OK",null,function(){if($.isFunction(completionHandler)){completionHandler();}},null);
                        } else {
                            //loading(false);
                            if ($.isFunction(completionHandler)) {
                                completionHandler();
                            }
                        }

                        loading(false);
                    } else {
                        console.log("Could not logout on server! Force Logout. Status:"+data.status);
                        forceLogout();
                    }
                    //loading(false);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getManager error Error: "+errorThrown);
                    loading(false);
                },
                dataType: "json"
            });

        } else {
            forceLogout();
        }

}


function ssoContactually() {
	$.ajax({
		url: "https://auth.contactually.com/oauth2/authorize/?client_id=ff0312d73de7cba90ac50751006bd9fa4c6e0f1717c2331b0f33f8f59292c151&scope=user:basic+contacts:manage+buckets:manage+notes:manage+interactions:manage&redirect_uri=https%3a%2f%2fws.spac.io%2fprod%2foh%2foauth2%2fcontactually&response_type=token",
		type:"POST",
		success: function(data, textStatus, jqXHR){
			console.log("Returned JSON Object: "+JSON.stringify(data));

		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
		},
		dataType: "json"
	});
}

function addTouchCMAPresentation() {
	if(online) {
		var _query = {};
		_query["fn"] = "addTouchCMAPresentation";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;
		_query["touchcmaURL"] = $("input[name='touchcmaURL']").val();

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Presentation Added","A sample email Visitors will receive has been sent to your email for review.");
					loading(false);
					properties[curPID]["touchcmaURL"] = $("input[name='touchcmaURL']").val();
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}

function clearTouchCMAPresentation() {
	if(online) {
		var _query = {};
		_query["fn"] = "clearTouchCMAPresentation";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Presentation Cleared","There is no longer a TouchCMA Presentation attached to this property.");
					loading(false);
					$("input[name='touchcmaURL']").val("");
					properties[curPID]["touchcmaURL"] = "";
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}

function addProPresentation() {
	if(online) {
		var _query = {};
		_query["fn"] = "addProPresentation";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;
		_query["presentationproURL"] = $("input[name='presentationproURL']").val();

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Presentation Added","A sample email Visitors will receive has been sent to your email for review.");
					loading(false);
					properties[curPID]["presentationproURL"] = $("input[name='presentationproURL']").val();
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: addPresentationPRo Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}

function clearProPresentation() {
	if(online) {
		var _query = {};
		_query["fn"] = "clearProPresentation";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Presentation Cleared","There is no longer a presentation attached to this property.");
					loading(false);
					$("input[name='presentationproURL']").val("");
					properties[curPID]["presentationproURL"] = "";
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}



function addMoxiPresentPresentation() {
	if(online) {
		var _query = {};
		_query["fn"] = "addMoxiPresentPresentation";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;
		_query["moxipresentURL"] = $("input[name='moxipresentURL']").val();

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Presentation Added","A sample email Visitors will receive has been sent to your email for review.");
					loading(false);
					properties[curPID]["moxipresentURL"] = $("input[name='moxipresentURL']").val();
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}

function clearMoxiPresentPresentation() {
	if(online) {
		var _query = {};
		_query["fn"] = "clearMoxiPresentPresentation";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Presentation Cleared","There is no longer a Moxi Present Presentation attached to this property.");
					loading(false);
					$("input[name='moxipresentURL']").val("");
					properties[curPID]["moxipresentURL"] = "";
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}


function generateCloudCMAReport() {
	if(online) {
		var _query = {};
		_query["fn"] = "generateCloudCMAReport";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;
		_query["cloudcmaMLS"] = $("input[name='cloudcmaMLS']").val();

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Report Generated","A Cloud CMA Property Report has been generated for this property and sent to your email for review.");
					loading(false);
					properties[curPID]["cloudcmaMLS"] = $("input[name='cloudcmaMLS']").val();
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}

function clearCloudCMAReport() {
	if(online) {
		var _query = {};
		_query["fn"] = "clearCloudCMAReport";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					showAlert("Report Cleared","There is no longer a Cloud CMA Property Report attached to this property.");
					loading(false);
					$("input[name='cloudcmaMLS']").val("");
					properties[curPID]["cloudcmaMLS"] = "";
					localStorage["properties"] = JSON.stringify(properties);
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
			},
			dataType: "json"
		});
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}

function addCustomQuestion(completionHandler) {
	var questionType = "";
	var questionText ="";

	if(curPanel == "editform") {
		questionType = $("#popup-edit-form input[name='custom-question-type']:checked").val();
		questionText = $("#popup-edit-form input[name='custom-question-text']").val();
	}

	if(curPanel == "defaultform") {
		 questionType = $("#popup-default-form input[name='custom-question-type']:checked").val();
		 questionText = $("#popup-default-form input[name='custom-question-text']").val();
 	}

		if (questionText == "") {
			showAlert("Error", "Cannot create a blank question.");
		} else if(questionType == "mc" && customChoicesToAdd.length < 2) {
			showAlert("Error", "Please add a minimum of 2 answers.");
		} else {
			customQuestionToAdd["question"] =questionText;
			customQuestionToAdd["questionCN"] ="";
			customQuestionToAdd["type"] =questionType;
			customQuestionToAdd["managerID"] =user["id"];

			if(questionType == "mc") {
				customQuestionToAdd["choices"] =customChoicesToAdd;
			}

			var _query = {};
			_query["fn"] = "addCustomQuestion";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["questionObj"] = customQuestionToAdd;

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {
						customQuestionToAdd["questionID"] = data.questionID;
						delete customQuestionToAdd["managerID"];

            if(curPanel == "editform") {
                temp_questions.push(customQuestionToAdd);
                loadQuestions(temp_questions);
            }

             if(curPanel == "defaultform") {

                temp_default_questions.push(customQuestionToAdd);
                loadDefaultQuestions(temp_default_questions);
            }

						customQuestionToAdd = {};
						closeQuestionsPopup();

						$("#addCustomQuestion_form")[0].reset();
						$("#addCustomQuestion_form2")[0].reset();
						loading(false);

						getCustomQuestions();

						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}
					} else {
						forceLogout();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: addCustomQuestion Error: "+errorThrown);
				},
				dataType: "json"
			});
		}


}

function deleteCustomQuestion(questionID) {

		var _query = {};
		_query["fn"] = "deleteCustomQuestion";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["questionID"] = questionID;

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {
					//closeQuestionsPopup();
					loading(false);

					getCustomQuestions();

					if ($.isFunction(completionHandler)) {
						completionHandler(data);
					}
				} else {
					forceLogout();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: addCustomQuestion Error: "+errorThrown);
			},
			dataType: "json"
		});



}



function getStandardQuestions(completionHandler) {
		var _query = {};
		_query["fn"] = "getStandardQuestions";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					//console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {

						standardQuestions = data.standard_questions;
						localStorage["standardQuestions"] = JSON.stringify(standardQuestions);
                        $(".standard-questions").html("");
                        $.each(standardQuestions, function(index, value) {
                            if(value["type"] == "agent") {
                                $("#popup-edit-form .standard-questions").append("<div class='form-question-row' id='add-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-question-"+index+"-yes' type='radio' value='YES' name='add-question-"+index+"'><label for='add-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='add-question-"+index+"-no' type='radio' value='NO' name='add-question-"+index+"'><label for='add-question-"+index+"-no'>No</label></div></div><div class='add-question-button' onclick='addQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");

                                $("#popup-default-form .standard-questions").append("<div class='form-question-row' id='add-default-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-default-question-"+index+"-yes' type='radio' value='YES' name='add-default-question-"+index+"'><label for='add-default-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='add-default-question-"+index+"-no' type='radio' value='NO' name='add-default-question-"+index+"'><label for='add-default-question-"+index+"-no'>No</label></div></div><div class='add-question-button' onclick='addDefaultQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");
                            }
                            if(value["type"] == "yn") {
                                 $("#popup-edit-form .standard-questions").append("<div class='form-question-row' id='add-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-question-"+index+"-yes' type='radio' value='YES' name='add-question-"+index+"'><label for='add-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='add-question-"+index+"-no' type='radio' value='NO' name='add-question-"+index+"'><label for='add-question-"+index+"-no'>No</label></div></div><div class='add-question-button' onclick='addQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");

                                $("#popup-default-form .standard-questions").append("<div class='form-question-row' id='add-default-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='add-default-question-"+index+"-yes' type='radio' value='YES' name='add-default-question-"+index+"'><label for='add-default-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='add-default-question-"+index+"-no' type='radio' value='NO' name='add-default-question-"+index+"'><label for='add-default-question-"+index+"-no'>No</label></div></div><div class='add-question-button' onclick='addDefaultQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");
                            }
                            if(value["type"] == "text") {
                                $("#popup-edit-form .standard-questions").append("<div class='form-question-row' id='add-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><input name='add-question-"+index+"' type='text' value='' placeholder='Text Answer' readonly><div class='add-question-button' onclick='addQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");

                                $("#popup-default-form .standard-questions").append("<div class='form-question-row' id='add-default-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><input name='add-default-question-"+index+"' type='text' value='' placeholder='Text Answer' readonly><div class='add-question-button' onclick='addDefaultQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");

                            }
                            if(value["type"] == "mc") {
                                $("#popup-edit-form .standard-questions").append("<div class='form-question-row' id='add-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='full-width-checkboxes answers-section' id='add-answers-section-"+index+"'></div><div class='add-question-button' onclick='addQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");

                                $.each(value["choices"], function(i, v) {
                                    $("#add-question-"+value["questionID"]+" .answers-section").append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='add-question-"+index+"' id='add-question-"+index+"-"+i+"'><label for='add-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
                                });

                                $("#popup-default-form .standard-questions").append("<div class='form-question-row' id='add-default-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='full-width-checkboxes answers-section' id='add-default-answers-section-"+index+"'></div><div class='add-question-button' onclick='addDefaultQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;standard&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div></div>");

                                $.each(value["choices"], function(i, v) {
                                    $("#add-default-question-"+value["questionID"]+" .answers-section").append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='add-default-question-"+index+"' id='add-default-question-"+index+"-"+i+"'><label for='add-default-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
                                });

                            }
                        });
						loading(false);
						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}
					} else {
						forceLogout();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error:  Error: "+errorThrown);
				},
				dataType: "json"
			});

}

function getCustomQuestions(completionHandler) {
		var _query = {};
		_query["fn"] = "getCustomQuestions";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		loading(true);
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {


						customQuestions = data.custom_questions;
						localStorage["customQuestions"] = JSON.stringify(customQuestions);
						loading(false);


                        $(".custom-questions").html("");

                        if(customQuestions.length < 1) {

                        } else {
                            $.each(customQuestions, function(index, value) {

                                if(value["type"] == "yn") {
                                    $("#popup-edit-form .custom-questions").append("<div class='form-question-row' id='add-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='custom-question-"+index+"-yes' type='radio' value='YES' name='custom-question-"+index+"'><label for='custom-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='custom-question-"+index+"-no' type='radio' value='NO' name='custom-question-"+index+"'><label for='custom-question-"+index+"-no'>No</label></div></div><div class='add-question-button' onclick='addQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;custom&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div><div class='delete-question-button' onclick='confirmDeleteCustomQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div></div>");

                                    $("#popup-default-form .custom-questions").append("<div class='form-question-row' id='add-default-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='custom-default-question-"+index+"-yes' type='radio' value='YES' name='custom-default-question-"+index+"'><label for='custom-default-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='custom-default-question-"+index+"-no' type='radio' value='NO' name='custom-default-question-"+index+"'><label for='custom-default-question-"+index+"-no'>No</label></div></div><div class='add-question-button' onclick='addDefaultQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;custom&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div><div class='delete-question-button' onclick='confirmDeleteCustomQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div></div>");
                                }
                                if(value["type"] == "text") {
                                    $("#popup-edit-form .custom-questions").append("<div class='form-question-row' id='add-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><input name='add-question-"+index+"' type='text' value='' placeholder='Text Answer' readonly><div class='add-question-button' onclick='addQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;custom&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div><div class='delete-question-button' onclick='confirmDeleteCustomQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div></div>");

                                    $("#popup-default-form .custom-questions").append("<div class='form-question-row' id='add-default-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><input name='add-default-question-"+index+"' type='text' value='' placeholder='Text Answer' readonly><div class='add-question-button' onclick='addDefaultQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;custom&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div><div class='delete-question-button' onclick='confirmDeleteCustomQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div></div>");

                                }
                                if(value["type"] == "mc") {

                                    $("#popup-edit-form .custom-questions").append("<div class='form-question-row' id='add-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='full-width-checkboxes answers-section' id='add-answers-section-"+index+"'></div><div class='add-question-button' onclick='addQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;custom&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div><div class='delete-question-button' onclick='confirmDeleteCustomQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div></div>");

                                    $.each(value["choices"], function(i, v) {
                                        $("#add-question-"+value["questionID"]+" .answers-section").append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='add-question-"+index+"' id='add-question-"+index+"-"+i+"'><label for='add-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
                                    });

                                     $("#popup-default-form .custom-questions").append("<div class='form-question-row' id='add-default-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='full-width-checkboxes answers-section' id='add-default-answers-section-"+index+"'></div><div class='add-question-button' onclick='addDefaultQuestion(&#39;"+value["questionID"]+"&#39;,"+index+",&#39;custom&#39;)'><i class='fa fa-plus' aria-hidden='true'></i></div><div class='delete-question-button' onclick='confirmDeleteCustomQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div></div>");

                                    $.each(value["choices"], function(i, v) {
                                        $("#add-default-question-"+value["questionID"]+" .answers-section").append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='add-default-question-"+index+"' id='add-default-question-"+index+"-"+i+"'><label for='add-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
                                    });

                                }
                            });
                        }

						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}
					} else {
						forceLogout();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: get Custom Question Error: "+errorThrown);
				},
				dataType: "json"
			});
}

function checkSystemStatus(completionHandler) {
		var _query = {};
		_query["fn"] = "checkSystemStatus";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		loading(true);
		//console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
		console.log("Checking System Status...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
						console.log("Success: System Status Retrieved.");
					if (data.status == 1) {


						if(data.system_status["serverStatus"] == "maintenance") {
							showAlert("Server Maintenance","We are currently running maintenance on our services and will be back up in "+data.system_status["maintenanceDuration"] +". We apologize for the inconvenience.","OK",null,function(){checkSystemStatus(completionHandler)},null);
						} else {
							loading(false);
							if ($.isFunction(completionHandler)) {
								completionHandler(data);
							}
						}

						loading(false);
						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}
					} else {
						forceLogout();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: checkSystemStatus Error: "+errorThrown);
				},
				dataType: "json"
			});
}

function getFormByID(auto) {
	if (typeof auto === "undefined" || auto === null) {
    	auto = false;
  	}

	var _query = {};

	_query["fn"] = "getFormByID";
	_query["propertyID"] = $("input[name='propertyID']").val();
	property["propertyID"] = _query["propertyID"];
	var json_data = _query;
	loading(true, "Retrieving Sign In Form...");
	console.log("Outgoing JSON Object: "+JSON.stringify(json_data));
	$.ajax({
		url: wsURL,
		type:"POST",
		data: {json : json_data},
		success: function(data, textStatus, jqXHR){
			if (data.status == 1) {
				console.log("Returned JSON Object: "+JSON.stringify(data));


				property["title"] = data.title;
				property["image"] = data.image;
				property["questions"] = data.questions;
				property["mls"] = data.mls;
				property["mlsnum"] = data.mlsnum;
				property["cnEnabled"] = data.cnEnabled;

				user["fname"] = data.agent_fname;
				user["lname"] = data.agent_lname;
				user["email"] = data.agent_email;
				user["phone"] = data.agent_phone;
				user["title"] = data.agent_title;
				user["brokerage"] = data.agent_brokerage;
				user["marketsnapID"] = data.agent_marketsnapID;
				user["id"] = data.agent_id;
				user["vtoken"] = data.agent_vtoken;
				user["pphoto"] = data.agent_pphoto;

				if (!auto) {
					history.pushState("propertyID", "", property["propertyID"] +"/");
				}
				getTLC();
				launchForm();

				$(".signin-panel").css("top","0px");
			} else {
				showAlert("Invalid Open House ID","Sorry, we can't find an open house matching this ID.");
			}
			loading(false);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getEmailTemplate error Error: "+errorThrown);
			loading(false);
		},
		dataType: "json"
	});

}

function loginManagerWithARC(fields) {

	if (online) {
		var _fields = fields;
		var _query = {};

		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})

		if (_query["user"] == "" || _query["user"] == null) {
			$(".error-msg").html("Please enter an username.");
		} else {

			loading(true,"Authenticating...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 0) {
          	showAlert("Access Denied","Invalid ARC username/password.");
					} else if (data.status == 1) {
            user["id"] = data.managerID;
            localStorage["id"] = user["id"];
            user["vtoken"] = data.vtoken;
            localStorage["vtoken"] = user["vtoken"];
						initSpacio(function(){
                login();
            });

					} else if (data.status == 2) {
            showAlert("Unknown Error","There was an unknown error. Please contact support@spac.io.");

					} else if (data.status == 3) {
            claimAccount(data.id, data.email, _query["password"]);

					} else if (data.status == 4) {
            showAlert("Account Not Found","We could not find a Spacio account matching your ARC credentials. Please contact support@spac.io.");

					} else {

					}
                    loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Login Error: "+errorThrown);
					$(".error-msg").html("Connection Error.");
                    loading(false);
				},
				dataType: "json"
			});
		}

	} else {
    showAlert("No Internet Connection","Please connect to the internet to login.");
  }

}

function claimAccount(uid, email, pw) {
    var _query = {};
    _query["fn"] = "claimAccount";
    _query["id"] = uid;
    _query["email"] = email;
    _query["pw"] = pw;
    _query["from"] = "59a7e3fea0d2ca211c7f2979";

    loading(true,"Claiming Your Account...");
  	var _salt = generateRandString(16);

    //Salt + Hash password before sending to server

    _query["salt"] = _salt;
    _query["pw"] += _salt;
    _query["pw"] = CryptoJS.SHA3(_query["pw"], { outputLength: 224 }).toString();

    var json_data = _query;
    console.log("Outbound JSON Query: "+JSON.stringify(json_data));
    $.ajax({
        url: wsURL,
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){
            console.log("Returned JSON Object: "+JSON.stringify(data));

            if (data.status == 1) {
                user["id"] = data.managerID;
                localStorage["id"] = user["id"];
                user["vtoken"] = data.vtoken;
                localStorage["vtoken"] = user["vtoken"];
								initSpacio(function(){
		                login();
		            });
            } else if (data.status == 2) {
                user["id"] = data.managerID;
                localStorage["id"] = user["id"];
                user["vtoken"] = data.vtoken;
                localStorage["vtoken"] = user["vtoken"];
								initSpacio(function(){
		                login();
		            });
            } else {

                showAlert("error","Invalid Link","Sorry, it seems your claim link is invalid. Please contact your account administrator.");

            }
            loading(false);
            //debugLog("Query Status: "+data.status);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Account Error: "+errorThrown);
        },
        dataType: "json"
    });

}

function loginManager(fields) {
	console.log("Login...");
	var _fields = fields;
	var _query = {};
	var _query2 = {};

	//Format form inputs into proper array
	$.each(_fields, function(index, value) {
		_query[value["name"]] = value["value"];
	})

	//Validate form inputs.
	if(debug) {
		localStorage['id'] = _query["email"];
		localStorage['vtoken'] = _query["password"];
		user["id"] = localStorage['id'];
		user["vtoken"] = localStorage['vtoken'];
		initSpacio(function(){
				login();
		});
	} else if (_query["email"] == "" || _query["email"] == null) {
		$(".error-msg").html("Please enter a valid email.");
	} else {
		loading(true,"Authenticating...");
		//Get salt first
		//Duplicate query object for getSalt operation
		_query2 = $.extend({}, _query);

		//Format getSalt query properly / remove password value
		_query2["fn"] = "getSalt";
		delete _query2["password"];

		//Clean up query object and send
		var json_data = _query2;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Salt Value: "+data.salt);

					if(data.temp) {

					} else {
						_query["password"] += data.salt;

						console.log(CryptoJS.SHA3("qwerty", { outputLength: 224 }).toString(CryptoJS.enc.Hex));
						_query["password"] = CryptoJS.SHA3(_query["password"], { outputLength: 224 }).toString();
					}

					console.log(_query);
					//Format login form values into JSON
					json_data = _query;

					console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
					//Send login informationt to server

					$.ajax({
						url: wsURL,
						type:"POST",
						data: {json : json_data},
						success: function(data, textStatus, jqXHR){
							console.log("Returned JSON Object: "+JSON.stringify(data));

							if (data.status == 1) {

								if (data.status == 1) {
								          //$(".error-msg").html("");

								if (data.emailConfirmed) {
									$.each(data, function(index, value){
										console.log(index+": "+value);
										if (index != "_id") {
											user[index] = value;
											//localStorage[index] =value;
										}
									})

									localStorage['id'] = user["id"];
									localStorage['vtoken'] = user["vtoken"];

									initSpacio(function(){
			                login();
			            });

								} else {
									user["id"] = data.id;
									user["vtoken"] = data.vtoken;
									showAlert("Please Confirm Your Email","Please confirm your email before logging in. For support, please contact: hello@spac.io","Resend Confirmation","Cancel",function(){resendConfirmationEmail()},function(){$(".alert-overlay").hide();});
									hideKeyboard();
								}

							} else {
                showAlert("Login Error","Invalid email and/or password.");
							}

							} else if(data.status == 3){
								$(".error-msg").html("Please contact support.");
							}else {
								$(".error-msg").html("Invalid email and/or password.");
							}
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Login Error: "+errorThrown);
							$(".error-msg").html("Connection Error.");
						},
						dataType: "json"
					});

				} else {
					$(".error-msg").html("Invalid email and/or password.");
					console.log("Could not retrieve salt");
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Salt Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	}
}


function getEmailTemplate(type,completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "getEmailTemplate";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["type"] = type;
			loading(true, "Getting Your Email Template...");

			var json_data = _query;
			//console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			console.log("Getting Email Template...");

			$.ajax({
				url: wsURL_new,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						console.log("Email Template Retrieved.");
						$('#template-email-message').trumbowyg('html', data.emailtemplate["messagebody"]);
            $('#template-email-subject').val(data.emailtemplate["subject"]);

						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}

					} else {
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getEmailTemplate error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

		} else {
			forceLogout();
		}
}

function getBBVideos(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "getBBVideos";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			loading(true, "Getting Your BB Video");

			var json_data = _query;
			//console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			console.log("Getting BB Video...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {
						console.log("BB Videos Retrieved.");

						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}
					} else if (data.status == 2) {
						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}
					} else {
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getBBVideos error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

		} else {
			forceLogout();
		}
}

function resetEmailTemplate(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "resetEmailTemplate";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["type"] = curEmailTemplate;

			loading(true, "Resetting Your Email Template To Default...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));

						$('#template-email-message').trumbowyg('html', data.messagebody);
            $('#template-email-subject').val('{{oh_title}}');

						saveEmailTemplate();

					} else {
						forceLogout(completionHandler);
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getEmailTemplate error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

		} else {
			forceLogout();
		}


}

function saveEmailTemplate(completionHandler) {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else {
	var _query = {};

	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];
	_query["fn"] = "saveEmailTemplate";
	_query["type"] = curEmailTemplate;
	_query["messagebody"] = $('#template-email-message').trumbowyg('html');
  _query["subject"] = $('#template-email-subject').val();

	loading(true, "Saving Email Template...");

	var json_data = _query;
	console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

	$.ajax({
				url: wsURL_new,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));

						if ($.isFunction(completionHandler)) {
							completionHandler(data);
						}

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: save email template error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
	}
}

function getManagerPaymentInfo(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};
		_query["fn"] = "getManagerPaymentInfo";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if(data.cus != "N/A") {
						$("payment").show();

						if(data.cc_type == "pending" || data.last4 == "pending") {
							$("#payment-info").html("No credit card on file.");
						} else {
							$("#payment-info").html(data.cc_type+" ending in "+data.last4);
						}
					} else {
						$("payment").hide();
						$("#payment-info").html("No credit card on file.");
					}

					if ($.isFunction(completionHandler)) {
						completionHandler();
					}

				} else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getManager error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function getManager(completionHandler) {
		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "getManager";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

			loading(true, "Getting User Info");

			var json_data = _query;
		//	console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			console.log("Retrieving User Information...");
			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {

						brokerage = data.brokerage;

						console.log("Success: User Information Retrieved.");
						$.each(data, function(index, value){
							//console.log(index+": "+value);
							if (index != "_id") {
								user[index] = value;
								localStorage[index] =value;
							}
						})

						user["brokerage"] = data.company;

						if (!('mlsID' in user)) {
							user["mlsID"] = "";
						}

            if (!('integrations' in user)) {
							user["integrations"] = {};
						}

						if(brokerage["status"] == "expired"){
            	showAlert("Brokerage Plan Expired","Your brokerage account has expired. Please contact your company administrator.","Ok",null, function(){getManager();});
            } else if(brokerage["status"] == "suspended"){
                showAlert("Brokerage Plan Suspended","Your brokerage account has been temporarily suspended. Please contact your company administrator.","Ok",null, function(){getManager();});
            } else {
							saveTimezone();
							if ($.isFunction(completionHandler)) {
								completionHandler();
							}
						}


					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getManager error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

		} else {
			forceLogout();
		}
}

function downgradeAccount() {

	if (user["id"] != null && user["vtoken"] != null)	{
		var _query = {};

		_query["fn"] = "downgradeAccount";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
		loading(true, "downgrading");
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Account downgraded.");
					console.log("Returned JSON Query: "+ JSON.stringify(data));
					var expiry_date = new Date(data.expiry["sec"]*1000);
					var expiry = expiry_date.toLocaleString();

					showAlert("Account Downgraded","Your account has now been downgraded. You will have access to the full features of Spacio until "+expiry+".","Ok",null);
					loading(false);
					//checkTrial();
				} else {
					showAlert("Unable to Downgrade","You are not on an upgraded plan.","Ok",null);
					loading(false);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Logout error Error: "+errorThrown);
			},
			dataType: "json"
		});
 	}

}


function checkTrial(completionHandler) {
	console.log("Checking Trial Expiry");
	if (user["id"] != null && user["vtoken"] != null)	{
		var _query = {};

		_query["fn"] = "checkTrial";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Validity Status: "+data.status);
				if (data.status == 1) {
					console.log("Account valid.");
					loading(false);

				} else if (data.status == 2){
					user["type"] = "freemium";
					localStorage["type"] = "freemium";
					showAlert("Trial Account Expired","Your trial account with Spacio Pro has now expired. You are currently using the free version of Spacio Pro. Certain features will not be available.","Ok",null);

				} else if (data.status == 3){
					user["type"] = "freemium";
					localStorage["type"] = "freemium";
					showAlert("Premium Account Expired","Your trial account with Spacio Pro has now expired. You are currently using the free version of Spacio Pro. Certain features will not be available.","Ok",null);

				} else if (data.status == 4){
					accountCancelled = true;

				}
				if ($.isFunction(completionHandler)) {
						completionHandler();
					}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Logout error Error: "+errorThrown);
			},
			dataType: "json"
		});
 	}
}

function editOLR() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editOLR";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-olrEnabled").is(':checked')) {
			_query["olrEnabled"] = "YES";
		} else {
			_query["olrEnabled"] = "NO";
		}
		loading(true, "Saving OLR Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function getTLC() {

	if("mlsnum" in properties[curPID]) {
		var mlsnum = properties[curPID]["mlsnum"];
		console.log(mlsnum);
		$(".tlc-number").show();

		if(user["mls"]=="Northstar" && mlsnum != "N/A" && mlsnum != "") {
				$.ajax({
					url: "http://api.tlcengine.com/V2/api/nsmls/listings/mlsids/"+mlsnum,
					method:"GET",
					contentType:"application/json; charset=utf-8",
					data: [],
					beforeSend: function (xhr) {
						xhr.setRequestHeader ("Authorization", "bearer 16E18DB9-26A1-4552-ABBA-6C2C33E17DBA");
					},
					success: function(data, textStatus, jqXHR){
					   //console.log("Returned JSON Object: "+JSON.stringify(data.ListingDetail.Listing));

						var tlc = data.TLCInfo[0].TLC;
						var tlc_commutecost = data.TLCInfo[0].CommuteCost;
						var tlc_utilities = data.TLCInfo[0].Utilities;
						var tlc_mortgage = data.TLCInfo[0].MortgagePayment;

						tlc = tlc.toFixed(2);
						tlc_commutecost = tlc_commutecost.toFixed(2);
						tlc_utilities = tlc_utilities.toFixed(2);
						tlc_mortgage = tlc_mortgage.toFixed(2);

						var tlc_others = tlc-tlc_commutecost-tlc_utilities-tlc_mortgage;

						tlc_others = tlc_others.toFixed(2);

						var tlc_term = data.TLCInfo[0].LoanTypeDisplay;
						var tlc_city= data.TLCInfo[0].City;
						var tlc_commutetime = data.TLCInfo[0].CommuteTime;
						var tlc_apr = data.TLCInfo[0].KnownHomeLoanAPR;

						$(".tlc").html("$"+tlc);
						$(".tlc-commutecost").html("$"+tlc_commutecost);
						$(".tlc-mortgage").html("$"+tlc_mortgage);
						$(".tlc-utilities").html("$"+tlc_utilities);
						$(".tlc-others").html("$"+tlc_others);

						$(".tlc-term").html(tlc_term);
						$(".tlc-apr").html(tlc_apr);
						$(".tlc-city").html(tlc_city);
						$(".tlc-commutetime").html(tlc_commutetime);

						loading(false);
					},
					error: function(jqXHR, textStatus, errorThrown) {
					   console.log("error:"+jqXHR.responseText+" test:"+textStatus+" Error: "+errorThrown);

					   if(jqXHR.responseText='[{"ErrorMessage":"Listing does not exist.","ErrorCode":null}]') {
						showAlert("Invalid MLS Listing Number", "Sorry, this MLS Listing Number does not exist.");
					   }
					   loading(false);
					},
					dataType: "json"
				});


		} else {
			$(".tlc-number").hide();
		}
	} else {
		$(".tlc-number").hide();
	}

}

function getMLSListing() {
	loading(true,"Getting Listing Information...");
	var mlsnum = $("input[name='mlsnum']").val();

	if(user["mls"]=="Northstar") {
		$.ajax({
			url: "http://api.tlcengine.com/V2/api/nsmls/listings/mlsids/"+mlsnum,
			method:"GET",
			contentType:"application/json; charset=utf-8",
			data: [],
			beforeSend: function (xhr) {
    			xhr.setRequestHeader ("Authorization", "bearer 16E18DB9-26A1-4552-ABBA-6C2C33E17DBA");
			},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data.ListingDetail.Listing));

				var temp_addr = "";
				var streetdirprefix = data.ListingDetail.Listing.STREETDIRPREFIX;
				var streetdirsuffix = data.ListingDetail.Listing.STREETDIRSUFFIX;
				var streetname = data.ListingDetail.Listing.STREETNAME;
				var streetnumber = data.ListingDetail.Listing.STREETNUMBER;
				var streetsuffix = data.ListingDetail.Listing.STREETSUFFIX;

				temp_addr = streetnumber+" "+streetdirprefix+" "+streetname+" "+streetsuffix+" "+streetdirsuffix;
				temp_addr = temp_addr.replace(/\s+/g, ' ');

				$("#title").val(temp_addr);
				$("#addr1").val(temp_addr);
				$("#addr2").val(data.ListingDetail.Listing.CITY+", "+data.ListingDetail.Listing.STATEORPROVINCE+" "+data.ListingDetail.Listing.POSTALCODE);
				$("#price").val(data.ListingDetail.Listing.LISTPRICE);
				$("#dimens").val(data.ListingDetail.Listing.LIVINGAREA);
				$("#beds").val(data.ListingDetail.Listing.BEDROOMSTOTAL);
				$("#baths").val(data.ListingDetail.Listing.BATHROOMSTOTALINTEGER);
				try {
					$("#image").val(data.Photos[0].Items[0].Url);
					$("#image-preview").css({"background-image":"url("+data.Photos[0].Items[0].Url+")"});
				} catch(e) {
					$("#image").val("https://spac.io/assets/img/stock/1.jpg");
					$("#image-preview").css({"background-image":"url(https://spac.io/assets/img/stock/1.jpg)"});
				}

				$("#mls").val("Northstar");

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" Error: "+errorThrown);

				if(jqXHR.responseText='[{"ErrorMessage":"Listing does not exist.","ErrorCode":null}]') {
					showAlert("Invalid MLS Listing Number", "Sorry, this MLS Listing Number does not exist.");
				}
				loading(false);
			},
			dataType: "json"
		});

	} else {
		var _query = {};
		_query["fn"] = "getMLSListing";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["mlsnum"] = mlsnum;
		_query["mls"] = user["mls"];

		var json_data = _query;
		$.ajax({
			url: wsURL_mls,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					$("#title").val(data.property["addr1"]);
					$("#addr1").val(data.property["addr1"]);
					$("#addr2").val(data.property["addr2"]);
					$("#price").val(data.property["price"]);
					$("#dimens").val(data.property["size"]);
					$("#beds").val(data.property["beds"]);
					$("#baths").val(data.property["baths"]);
					$("#desc").val(data.property["desc"]);
					$("#image").val(data.property["photo"]);
					$("#image-preview-2").css({"background-image":"url("+data.property["photo"]+")"});
					$("#mls").val(user["mls"]);
				} else if (data.status == 2){
					//console.log("Could not logout on server! Force Logout");
					showAlert("Invalid MLS","Sorry we do not support this MLS.");
				} else if (data.status == 3){
					//console.log("Could not logout on server! Force Logout");
					showAlert("No Listing Found","We could not find a listing matching this MLS ID.");
				} else if (data.status == 4){
					//console.log("Could not logout on server! Force Logout");
					showAlert("Not Authorized","Sorry, you are not authorized to access this MLS data.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	}


}

function getMLSList(completionHandler) {
		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "getMLSList";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

			loading(true, "Loading...");

			var json_data = _query;
			//console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			console.log("Retrieving MLS List...");
			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						//console.log("Returned JSON Object: "+JSON.stringify(data));
						console.log("MLS List Retrieved");
						mlsList = data.mlslist;
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
						forceLogout();
					}
					//loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getMLSList error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

		} else {
			forceLogout();
		}
}

function addCurOHObj(completionHandler) {
	var d = new Date();
	var endTime = d.getTime();


	if(curOHObj != null) {
		curOHObj["endTime"] = endTime;
		var minsHosted = (curOHObj["endTime"] - curOHObj["startTime"])/1000/60;

		if (minsHosted > 300) {
			minsHosted = 300;
		}

		minsHosted = Math.floor(minsHosted);
		curOHObj["minsHosted"] = minsHosted;

		if(minsHosted < 60 && curOHObj["leadsCount"] == 0) {
			//No point in saving;
			curOHObj = null;
			curOHID = "";
		} else {
				var _query = {};
				_query["fn"] = "addCurOHObj";
				_query["managerID"] = user["id"];
				_query["vtoken"] = user["vtoken"];
				_query["ohObj"] = curOHObj;

				var json_data = _query;
				console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

				$.ajax({
					url: wsURL,
					type:"POST",
					data: {json : json_data},
					success: function(data, textStatus, jqXHR){

						console.log("Returned JSON Object: "+JSON.stringify(data));

						if (data.status == 1) {
							curOHObj = null;
							curOHID = "";

						} else {
							//console.log("Could not logout on server! Force Logout");
							//showAlert("Not Authorized","Sorry, you are not authorized to access this MLS data.");
						}

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

						//loading(false);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
						loading(false);
					},
					dataType: "json"
				});


		}

	} else {
		curOHObj = null;
		curOHID = "";

		if ($.isFunction(completionHandler)) {
			completionHandler();
		}
	}

}

function saveTimezone() {
		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};
			var d = new Date()
			var n = d.getTimezoneOffset();

			_query["fn"] = "saveTimezone";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["timezoneOffset"] = n;
      _query["ipadVersion"] = version;
			_query["platform"] = "web";

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
						forceLogout();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: saveTimezone error Error: "+errorThrown);
				},
				dataType: "json"
			});

		}

}


function updateHostedCount(pid) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var d = new Date();
		var n = d.getTime();

		var weekday = new Array(7);
		weekday[0]=  "Sunday";
		weekday[1] = "Monday";
		weekday[2] = "Tuesday";
		weekday[3] = "Wednesday";
		weekday[4] = "Thursday";
		weekday[5] = "Friday";
		weekday[6] = "Saturday";

		var day = weekday[d.getDay()];
		var offset = new Date().getTimezoneOffset();
		offset = offset * 60;

		var _query = {};

		_query["fn"] = "updateHostedCount";
		_query["pid"] = pid;
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["dateHosted"] = n;
		_query["dayHosted"] = day;
		_query["offset"] = offset;

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Hosted Count Updated.");

				}

				if (data.status == 2) {
					console.log("Hosted Count Not Updated.");

				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Update Hosted Count Error: "+errorThrown);
			},
			dataType: "json"
		});
 	}

}

function updateLastActive() {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "updateLastActive";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];


		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Last Active Updated.");

				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Logout error Error: "+errorThrown);
			},
			dataType: "json"
		});
 	}
}

function updatePropertyLastActive(pid) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "updatePropertyLastActive";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = pid;

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Last Active Updated.");

				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Logout error Error: "+errorThrown);
			},
			dataType: "json"
		});
 	}
}

function logoutManager() {

		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "logoutManager";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

			loading(true, "Logging Out...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						//console.log("Returned JSON Object: "+JSON.stringify(data));
						forceLogout();

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Logout error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}

}

function broadcastTestMessage(completionHandler) {
	var type = $(".broadcast-email-template").val();

	//showAlert("Message Sent","A test message has been sent to your email.","Ok",null);
	if(user["type"] != "freemium") {
		if (user["id"] != null && user["vtoken"] != null )	{
			loading(true, "Broadcasting...");
			var _query = {};

			if($("#broadcast-bombbomb").is(':checked')) {
				_query["fn"] = "sendBBVideo";
				_query["emails"] = user["email"];
				_query["bbVidID"]= $("#broadcast-bombbomb-video").val();
				_query["managerID"] = user["id"];
				_query["vtoken"] = user["vtoken"];
				_query["subject"] = $("input[name='broadcast-email-subject']").val();
				_query["subject"] = "[TEST] "+_query["subject"];
				_query["pid"] = curPID;
				_query["message"] = $('#broadcast-message').trumbowyg('html');;
				_query["test"] = "YES";

			} else {
				_query["fn"] = "broadcastMessage";
				_query["managerID"] = user["id"];
				_query["vtoken"] = user["vtoken"];
				_query["emails"] = [];
				_query["subject"] = $("input[name='broadcast-email-subject']").val();
				_query["pid"] = curPID;
				_query["message"] = $('#broadcast-message').trumbowyg('html');;
				_query["bcc"] = "YES";
				_query["test"] = "YES";
			}

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Query: "+ JSON.stringify(data));

					if (data.status == 1) {

						loading(false);
						showAlert("Message Sent","A test message has been sent to your email.","Ok",null);
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
						//console.log("Could not logout on server! Force Logout");
						//forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: send test broadcast Error: "+errorThrown);
					showAlert("Unknown Error","There was an error while attempting to broadcast your message. Please contact support.","Ok");
					loading(false);
				},
				dataType: "json"
			});

		}

	} else {
		showAlert("Please Upgrade","Go Pro today to broadcast your Registrants.","Upgrade","Cancel",function(){showUpgradePage();});
	}
}

function broadcastMessage(completionHandler) {
	var type = $(".broadcast-email-template").val();


	if(user["type"] != "freemium") {
		if (user["id"] != null && user["vtoken"] != null )	{
			loading(true, "Broadcasting...");
			var _query = {};

			var emails_list = $(".broadcast-registrants input:checkbox:checked").map(function(){
      		return $(this).val();
    	}).get();

			var emails_list_2 = $(".broadcast-registrants input:checkbox:checked").map(function(){
					var temp_rid = $(this).val();
					console.log(temp_rid);
					if(properties[curPID]["disabled"] == "NO") {
						return registrants[temp_rid]["email"];

					} else if(properties[curPID]["disabled"] == "YES") {
						return registrants_inactive[temp_rid]["email"];
					}
    	}).get();


			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

			_query["subject"] = $("input[name='broadcast-email-subject']").val();
			_query["pid"] = curPID;
			_query["message"] = $('#broadcast-message').trumbowyg('html');;
			_query["test"] = "NO";

			if ($("#broadcast-bcc").is(':checked')) {
				_query["bcc"] = "YES";
			} else {
				_query["bcc"] = "NO";
			}

			if(emails_list.length < 1) {

				showAlert("No Visitors Selected","Please select Visitors you would like to send a broadcast message to.");
				loading(false);
			} else if($("#broadcast-bombbomb").is(':checked')) {
				_query["fn"] = "sendBBVideo";
				_query["emails"] = emails_list_2;
				_query["bbVidID"]= $(".broadcast-bombbomb-video").val();
				var json_data = _query;
				console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

				$.ajax({
					url: wsURL,
					type:"POST",
					data: {json : json_data},
					success: function(data, textStatus, jqXHR){
						console.log("Returned JSON Query: "+ JSON.stringify(data));

						if (data.status == 1) {
							$(".broadcast-registrant").prop('checked', false);
							//$('#broadcast-message').trumbowyg('empty');
							//reloadRegistrants();
							loading(false);
							showAlert("Message Sent","Your message has been successfully sent to all selected Visitors.","Ok",null);
							if ($.isFunction(completionHandler)) {
								completionHandler();
							}
						} else {
							//console.log("Could not logout on server! Force Logout");
							//forceLogout();
						}
						loading(false);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
						showAlert("Unknown Error","There was an error while attempting to broadcast your message. Please contact support.","Ok");
						loading(false);
					},
					dataType: "json"
				});


			} else {
				_query["fn"] = "broadcastMessage";
				_query["emails"] = emails_list;
				var json_data = _query;
				console.log("Outbound JSON Query: "+ JSON.stringify(json_data));


				$.ajax({
					url: wsURL,
					type:"POST",
					data: {json : json_data},
					success: function(data, textStatus, jqXHR){
						console.log("Returned JSON Query: "+ JSON.stringify(data));

						if (data.status == 1) {
							$(".broadcast-registrant").prop('checked', false);
							//$('#broadcast-message').trumbowyg('empty');
							//reloadRegistrants();
							loading(false);
							showAlert("Message Sent","Your message has been successfully sent to all selected Visitors.","Ok",null);
							if ($.isFunction(completionHandler)) {
								completionHandler();
							}
						} else {
							//console.log("Could not logout on server! Force Logout");
							//forceLogout();
						}
						loading(false);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
						showAlert("Unknown Error","There was an error while attempting to broadcast your message. Please contact support.","Ok");
						loading(false);
					},
					dataType: "json"
				});

			}
		}

	} else {
		showAlert("Please Upgrade","Go Pro today to broadcast your Registrants.","Upgrade","Cancel",function(){showUpgradePage();});
	}
}

function getDefaultForm(completionHandler) {
		if (user["id"] != null && user["vtoken"] != null )	{
      var _query = {};

			_query["fn"] = "getDefaultForm";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

      var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
            default_form = data.default_form;
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else if(data.status == 2) {
            if ($.isFunction(completionHandler)) {
							completionHandler();
						}
          } else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Save Form Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
  } else {
      forceLogout();
  }
}

function saveDefaultForm(completionHandler) {
		if (user["id"] != null && user["vtoken"] != null )	{
      var _query = {};

			_query["fn"] = "saveDefaultForm";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["default_form"] = {};

      if ($("#default-allow-mobile").is(':checked')) {
				_query["default_form"]["mobile"] = "YES";
				default_form["mobile"] = "YES";
			} else {
				_query["default_form"]["mobile"] = "NO";
				default_form["mobile"] = "NO";
			}
            if ($("#default-allow-autoEmail").is(':checked')) {
				_query["default_form"]["autoEmail"] = "YES";
				default_form["autoEmail"] = "YES";
			} else {
				_query["default_form"]["autoEmail"] = "NO";
				default_form["autoEmail"] = "NO";
			}
			if ($("#default-allow-bcc").is(':checked')) {
				_query["default_form"]["autoBCC"] = "YES";
				default_form["autoBCC"] = "YES";
			} else {
				_query["default_form"]["autoBCC"] = "NO";
				default_form["autoBCC"] = "NO";
			}

      if ($("#default-allow-cnEnabled").is(':checked')) {
				_query["default_form"]["cnEnabled"] = "YES";
				default_form["cnEnabled"] = "YES";
			} else {
				_query["default_form"]["cnEnabled"] = "NO";
				default_form["cnEnabled"] = "NO";
			}
            if ($("#default-allow-MandatoryEmail").is(':checked')) {
				_query["default_form"]["emailMandatory"] = "YES";
				default_form["emailMandatory"] = "YES";
			} else {
				_query["default_form"]["emailMandatory"] = "NO";
				default_form["emailMandatory"] = "NO";
			}

			if ($("#default-allow-MandatoryPhone").is(':checked')) {
				_query["default_form"]["phoneMandatory"] = "YES";
				default_form["phoneMandatory"] = "YES";
			} else {
				_query["default_form"]["phoneMandatory"] = "NO";
				default_form["phoneMandatory"] = "NO";
			}

      _query["default_form"]["autoEmailOffset"] =  $("#default-autoEmail-offset").val();
      default_form["autoEmailOffset"] = $("#default-autoEmail-offset").val();;

      _query["default_form"]["fontSize"] =  $("#default-font-size").val();
      default_form["fontSize"] = $("#default-font-size").val();

      _query["default_form"]["questionsObj"] = temp_default_questions;
      default_form["questionsObj"] = temp_default_questions;

      loading(true, "Saving...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
            showAlert("Default Form Saved","Your default form settings have been saved and will be applied to newly added properties only. Default settings will not be applied to previously added properties.");
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Save Form Error: "+errorThrown);
          showAlert("Error","Your default form could not be saved.");
          hidePopup("default-form");
					loading(false);
				},
				dataType: "json"
			});
        } else {

        }


}

function saveForm(pid, completionHandler) {

		if (user["id"] != null && user["vtoken"] != null )	{

			var _query = {};

			_query["fn"] = "saveForm";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["pid"] = pid;
			_query["questionsObj"] = properties[pid]["questionsObj"];

			if ($("#allow-mobile").is(':checked')) {
				_query["mobile"] = "YES";
				properties[pid]["mobile"] = "YES";
			} else {
				_query["mobile"] = "NO";
				properties[pid]["mobile"] = "NO";
			}

			if ($("#allow-autoEmail").is(':checked')) {
				_query["autoEmail"] = "YES";
				properties[pid]["autoEmail"] = "YES";
			} else {
				_query["autoEmail"] = "NO";
				properties[pid]["autoEmail"] = "NO";
			}

			if ($("#allow-bcc").is(':checked')) {
				_query["autoBCC"] = "YES";
				properties[pid]["autoBCC"] = "YES";
			} else {
				_query["autoBCC"] = "NO";
				properties[pid]["autoBCC"] = "NO";
			}

			if ($("#allow-passcode").is(':checked')) {
				_query["passcodeProtection"] = "YES";
				user["passcodeProtection"] = "YES";
			} else {
				_query["passcodeProtection"] = "NO";
				user["passcodeProtection"] = "NO";
			}

      _query["pin"] =  $("#edit-pin").val();
      user["pin"] = $("#edit-pin").val();

			if ($("#allow-cnEnabled").is(':checked')) {
				_query["cnEnabled"] = "YES";
				properties[pid]["cnEnabled"] = "YES";
			} else {
				_query["cnEnabled"] = "NO";
				properties[pid]["cnEnabled"] = "NO";
			}

			if ($("#enable-brokers").is(':checked')) {
				_query["brokersEnabled"] = "YES";
				properties[pid]["brokersEnabled"] = "YES";
			} else {
				_query["brokersEnabled"] = "NO";
				properties[pid]["brokersEnabled"] = "NO";
			}

			if ($("#allow-MandatoryEmail").is(':checked')) {
				_query["emailMandatory"] = "YES";
				properties[pid]["emailMandatory"] = "YES";
			} else {
				_query["emailMandatory"] = "NO";
				properties[pid]["emailMandatory"] = "NO";
			}

			if ($("#allow-MandatoryPhone").is(':checked')) {
				_query["phoneMandatory"] = "YES";
				properties[pid]["phoneMandatory"] = "YES";
			} else {
				_query["phoneMandatory"] = "NO";
				properties[pid]["phoneMandatory"] = "NO";
			}

			_query["autoEmailOffset"] =  $("#autoEmail-offset").val();
      properties[pid]["autoEmailOffset"] = $("#autoEmail-offset").val();

      _query["fontSize"] =  $("#font-size").val();
      properties[pid]["fontSize"] = $("#font-size").val();

			loading(true, "Saving...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						hideKeyboard();
						localStorage["properties"] = JSON.stringify(properties);
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Save Form Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}

}

function getProperties(completionHandler) {

		console.log("fetching properties from server");

		if (user["id"] != null && user["vtoken"] != null )	{

			var _query = {};

			_query["fn"] = "getPropertiesByManager";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

			loading(true);

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {
						//console.log("Returned JSON Object: "+JSON.stringify(data));
						properties = {};
						report_properties = {};

						var d = new Date();
            var d_localstring = d.toLocaleString();
						$("#last-sync").html("Last Synced On: " + d_localstring);

						$.each(data.properties, function(index,value){
							properties[value["id"]] = value;
							if(!("disabled" in properties[value["id"]])) {
								properties[value["id"]]["disabled"] = "NO";
							}

							if(!("emailMandatory" in properties[value["id"]])) {
								properties[value["id"]]["emailMandatory"] = "NO";
							}

							if(!("phoneMandatory" in properties[value["id"]])) {
								properties[value["id"]]["phoneMandatory"] = "NO";
							}

						});

						propertiesSearchResult = $.extend({}, properties);
						report_properties = $.extend({}, properties);
						console.log("Property List Length: "+Object.keys(properties).length);

						loading(false);

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
						loading(false);
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}

}

function addVisitor() {
    var _fields = $( "form#addRegistrant_Form" ).serializeArray();
    var _query = {};
    var hasAgent = false;

    //Format form inputs into proper array
    $.each(_fields, function(index, value) {
        _query[value["name"]] = value["value"];
    })

    _query["source"] = source;
    _query["name"] = capitalizeFirstLetterEachWordSplitBySpace(_query["name"]);
    _query["managerID"] = user["id"];
    _query["vtoken"] = user["vtoken"];
    _query["fn"] = "addRegularRegistrant";
    _query["ohID"] = "N/A";
    _query["docs"] = [];
    _query["isBroker"] = "NO";
    _query["pid"] = curPID;
    _query["propertyID"] = properties[curPID]["propertyID"];

    if(user["type"] == "brokerage") {
        if(brokerage["docs"].length > 0) {
            $.each(brokerage["docs"],function(index,value){
                _query["docs"][index] = {};
                _query["docs"][index]["status"] = "pending";
            })
        }
    }

    var answers = [];

    $.each(properties[curPID]["questionsObj"], function(index, value) {

        if (value["type"] == "yn") {
            var key = "add-registrant-question-"+index;
            if (!(key in _query)) {
                answers.push({"questionObj":value, "answer":""});
            } else {
                answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});
            }
        }

        if (value["type"] == "mc") {
            var key = "add-registrant-question-"+index;
            if (!(key in _query)) {
                answers.push({"questionObj":value, "answer":""});
            } else {
                answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});
            }
        }

        if (value["type"] == "text") {
            answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});
        }

        if (value["type"] == "agent") {
            var key = "add-registrant-question-"+index;
            if (!(key in _query)) {
                answers.push({"questionObj":value, "answer":""});
            } else {
                answers.push({"questionObj":value, "answer":_query["add-registrant-question-"+index]});

                if(_query["add-registrant-question-"+index] == "YES") {
                    hasAgent = true;
                    _query["question-"+index+"a"] = capitalizeFirstLetterEachWordSplitBySpace(_query["add-registrant-question-"+index+"a"]);
                    answers.push({"questionObj":{"questionID":"agenta","question":"Agent Name?", "type":"text"}, "answer":_query["add-registrant-question-"+index+"a"]});
                    answers.push({"questionObj":{"questionID":"agentb","question":"Agent Contact?", "type":"text"}, "answer":_query["add-registrant-question-"+index+"b"]});
                    answers.push({"questionObj":{"questionID":"agentc","question":"Agent Company?", "type":"text"}, "answer":_query["add-registrant-question-"+index+"c"]});
                }
            }
        }

    })
    if(_query["name"] == "" && _query["email"] == "" && _query["phone"] == "") {
        showAlert("Please Fill Out Form", "Please fill out the form before submitting.");
    } else if(_query["email"] != "" && !isValidEmailAddress(_query["email"])) {
        showAlert("Incorrect Email Format", "Please check the format of the email entered.");
    } else {

        if (user["id"] != null && user["vtoken"] != null ) {
            var o = new Date();
            var n = o.getTimezoneOffset() * 60 * 1000;

            var date_string = $("#add-registrant-datecreated").val();
            var date_timestamp = Date.parse(date_string);
            var date_timestamp = date_timestamp + n;

            var time_string = $("#add-registrant-timecreated").val();
            var time_arr = time_string.split(":");
            var time_hours = parseInt(time_arr[0]);
            var time_minutes = parseInt(time_arr[1]);

            var time_ms = (time_hours*60*60*1000) + (time_minutes*60*1000);

            var created_timestamp = date_timestamp + time_ms;

            _query["dateCreated"] = {"sec":Math.floor(created_timestamp/1000),"used":0};
            _query["answersObj"] = answers;

            var json_data = _query;
            console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
            loading(true, "Registering for Property...");

            $.ajax({
                url: wsURL,
                type:"POST",
                data: {json : json_data},
                success: function(data, textStatus, jqXHR){
                    if (data.status == 1) {
                        console.log("Returned JSON Object: "+JSON.stringify(data));
                        hideKeyboard();
                        $('#addRegistrant_Form')[0].reset();
                        hideEQ(1);

                        curRID = data.registrantID;
                        registrants[curRID] = {};
                        registrants[curRID]["name"] = _query["name"];
                        registrants[curRID]["email"] = _query["email"];
                        registrants[curRID]["phone"] = _query["phone"];
                        registrants[curRID]["answersObj"] = _query["answersObj"];
                        registrants[curRID]["docs"] = _query["docs"];

                        showAlert("Visitor Added","Visitor information has been succesfully added.");
												alertTimer = setTimeout(function(){ $(".alert-overlay").hide(); }, 3000);
                        $("#popup-add-visitor").hide();
                        showVisitors(curPID);
                    } else if (data.status == 2){

                        $('#propertyReg_Form')[0].reset();
                        hideEQ(1);
                        showAlert("Already Registered","Visitor already registered for this property.");
                        //console.log("Could not logout on server! Force Logout");
                        //forceLogout();
                    } else {
                        //alert("Succesfully Registered");
                    }
                    loading(false);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Register For Property Error: "+errorThrown);


                   showAlert("ERROR","Visitor information not registered. Please check wifi connection or turn off wifi to continue using Spacio in offline mode. You may do this by turning on airplane mode.");

                    loading(false);
                },
                dataType: "json"
            });
        } else {
            forceLogout();
        }
    }
}

function regForProperty() {
	var _fields = $( "form#propertyReg_Form" ).serializeArray();
	var _query = {};
	var hasAgent = false;

	//Format form inputs into proper array
	$.each(_fields, function(index, value) {
		_query[value["name"]] = value["value"];
	})

	_query["name"] = capitalizeFirstLetterEachWordSplitBySpace(_query["name"]);
	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];
	_query["source"] = "web";
	_query["ohID"] = curOHID;
  _query["docs"] = [];

	if(user["type"] == "brokerage") {
		if(brokerage["docs"].length > 0) {
			$.each(brokerage["docs"],function(index,value){
				_query["docs"][index] = {};
				_query["docs"][index]["status"] = "pending";
			})
		}
	}

	var showBrokersSignin = false;
	if ("brokersEnabled" in properties[curPID]) {
		if(properties[curPID]["brokersEnabled"] == "YES") {
			showBrokersSignin = true;
		} else {
			showBrokersSignin = false;
		}
	} else {
		showBrokersSignin = false;
	}

	if(_query["name"] == "" && _query["email"] == "" && _query["phone"] == "") {
		showAlert("Please Fill Out Form", "Please fill out the form before submitting.");
	} else if(_query["email"] == "" && user["docusignEnabled"] == "YES") {
		showAlert("Email Required", "Please enter a valid email address.");
	} else if(_query["email"] == "" && properties[curPID]["emailMandatory"] == "YES") {
		showAlert("Email Required", "Please enter an email adddress.");
	} else if(_query["phone"] == "" && properties[curPID]["phoneMandatory"] == "YES") {
		showAlert("Phone Required", "Please enter a phone number.");
	} else {
			if (user["id"] != null && user["vtoken"] != null )	{

				var answers = [];

				if(showBrokersSignin) {
					var key = "question-0";
					if (!(key in _query)) {
						answers.push({"questionObj":{"questionID":"brokerquestion","question":"Do you have a client in mind for this listing?","type":"yn"}, "answer":""});
					} else {
						answers.push({"questionObj":{"questionID":"brokerquestion","question":"Do you have a client in mind for this listing?","type":"yn"}, "answer":_query["question-0"]});
					}

					key = "question-1";
					if (!(key in _query)) {
						answers.push({"questionObj":{"questionID":"brokerquestion2","question":"How well priced is this property?","type":"mc"}, "answer":""});
					} else {
						answers.push({"questionObj":{"questionID":"brokerquestion2","question":"How well priced is this property?","type":"mc"}, "answer":_query["question-1"]});
					}

		      key = "question-2";
					if (!(key in _query)) {
						answers.push({"questionObj":{"questionID":"brokerquestion3","question":"Please provide some feedback on this property:","type":"textarea"}, "answer":""});
					} else {
						answers.push({"questionObj":{"questionID":"brokerquestion3","question":"Please provide some feedback on this property:","type":"textarea"}, "answer":_query["question-2"]});
					}

				} else {

					$.each(properties[curPID]["questionsObj"], function(index, value) {

						if (value["type"] == "yn") {
							var key = "question-"+index;
							if (!(key in _query)) {
								answers.push({"questionObj":value, "answer":""});
							} else {
								answers.push({"questionObj":value, "answer":_query["question-"+index]});
							}
						}

						if (value["type"] == "mc") {
							var key = "question-"+index;
							if (!(key in _query)) {
								answers.push({"questionObj":value, "answer":""});
							} else {
								if (_query["question-"+index] == "Other" || _query["question-"+index] == "Others") {
									_query["question-"+index] = _query["question-"+index] + ": "+ $("input[name='question-"+index+"-other']").val();
								}

								answers.push({"questionObj":value, "answer":_query["question-"+index]});
							}
						}

						if (value["type"] == "text") {
							answers.push({"questionObj":value, "answer":_query["question-"+index]});
						}

						if (value["type"] == "agent") {
							var key = "question-"+index;
							if (!(key in _query)) {
								answers.push({"questionObj":value, "answer":""});
							} else {
								answers.push({"questionObj":value, "answer":_query["question-"+index]});

								if(_query["question-"+index] == "YES") {
									hasAgent = true;
									_query["question-"+index+"a"] = capitalizeFirstLetterEachWordSplitBySpace(_query["question-"+index+"a"]);
									answers.push({"questionObj":{"questionID":"agenta","question":"Agent Name?", "type":"text"}, "answer":_query["question-"+index+"a"]});
									answers.push({"questionObj":{"questionID":"agentb","question":"Agent Contact?", "type":"text"}, "answer":_query["question-"+index+"b"]});
									answers.push({"questionObj":{"questionID":"agentc","question":"Agent Company?", "type":"text"}, "answer":_query["question-"+index+"c"]});
								}
							}
						}

					})

				}

				var d = new Date();
				var n = d.getTime();

				_query["dateCreated"] = {"sec":Math.floor(n/1000),"used":0};
				_query["answersObj"] = answers;

					var json_data = _query;
					console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
					loading(true, "Registering for Property...");

					$.ajax({
						url: wsURL,
						type:"POST",
						data: {json : json_data},
						success: function(data, textStatus, jqXHR){
							if (data.status == 1) {
								console.log("Returned JSON Object: "+JSON.stringify(data));
								$('#propertyReg_Form')[0].reset();
								hideEQ(1);

								curRID = data.registrantID;
								registrants[curRID] = {};
								registrants[curRID]["name"] = _query["name"];
								registrants[curRID]["email"] = _query["email"];
								registrants[curRID]["phone"] = _query["phone"];
								registrants[curRID]["answersObj"] = _query["answersObj"];
								registrants[curRID]["docs"] = _query["docs"];

								$("#reg-form-scroll").scrollTop(0);
								curOHObj["leadsCount"]++;
								if( _query["email"] != user["email"]) {
									curOHObj["leadsCount"]++;
								}

								if("docsEnabled" in user["integrations"]) {
                    if(user["integrations"]["docsEnabled"] != "YES" || user["integrations"]["docsAuto"] != "YES" || (brokerage["docsToRepresented"] == "NO" && hasAgent)) {
                        showAlert("Successfully Registered","Thank you!");
                        alertTimer = setTimeout(function(){ $(".alert-overlay").hide(); }, 3000);
                        //showSelectDocs();
                    } else {
                        showSeeReps();
                    }
                } else  if("docusign" in data) {
                  if(data.docusign["status"] == 1) {
                      $(".docusign-overlay").show();

                      $(".docusign-overlay").off();
                      $(".docusign-overlay").on("click", function(){
                          $(".docusign-overlay").hide();
                          clearTimeout(docusignTimer);
													$(".docusign-iframe-overlay").show();
													$("#docusign-iframe").attr("src", data.docusign["docusign_url"]);
                          //openURL(data.docusign["docusign_url"]);
                      });

                      docusignTimer = setTimeout(function(){
                          $(".docusign-overlay").hide();
													$(".docusign-iframe-overlay").show();
													$("#docusign-iframe").attr("src", data.docusign["docusign_url"]);
                          //openURL(data.docusign["docusign_url"]);
                      }, 5000);
                  } else {
										showAlert("Successfully Registered","Thank you!");
										alertTimer = setTimeout(function(){ $(".alert-overlay").hide(); }, 3000);
                  }

	              } else {
									showAlert("Successfully Registered","Thank you!");
									setTimeout(function(){ $(".alert-overlay").hide(); }, 3000);
								}


							} else if (data.status == 2){

								$('#propertyReg_Form')[0].reset();
								showAlert("Already Registered","You have already registered for this property.");
								setTimeout(function(){ $(".alert-overlay").hide(); }, 3000);

								hideEQ(1);
								//console.log("Could not logout on server! Force Logout");
								//forceLogout();
							} else {
								//alert("Succesfully Registered");
							}
							loading(false);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Register For Property Error: "+errorThrown);
							showAlert("ERROR","Visitor information not registered. There was a connection error with the server. If problem persists, please contact support at support@spac.io.");
							loading(false);
						},
						dataType: "json"
					});


			} else {
				//forceLogout();
			}

	}

}


function getRegistrants(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "getRegistrants";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Loading Registrants...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					//console.log("Returned JSON Object: "+JSON.stringify(data));

					registrants = {};

					$.each(data.registrants, function(index,value){
						registrants[value["id"]] = value;
						if(!("isBroker" in value)) {
							value["isBroker"] = "NO";
						}
					});
					//console.log("Registrants List Length: "+Object.keys(registrants).length);

					registrantsSearchResult = $.extend({}, registrants);

					if ($.isFunction(completionHandler)) {
						completionHandler();
					}
				} else {
					//console.log("Could not logout on server! Force Logout");
					forceLogout();
				}
				//loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Registrants Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

 	}
}

function getReportRegistrantsByManager(completionHandler) {
    if (user["id"] != null && user["vtoken"] != null )    {
        var _query = {};

        _query["fn"] = "getRegistrantsByManager";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        //loading(true, "Loading Registrants...");

        var json_data = _query;
        console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

        $.ajax({
            url: wsURL,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){
                if (data.status == 1) {
                    //console.log("Returned JSON Object: "+JSON.stringify(data));
                    report_registrants = {};
                    report_registrants = data.registrants;
                    if ($.isFunction(completionHandler)) {
                        completionHandler();
                    }
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    forceLogout();
                }
                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Registrant Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });
     }
}

function getHostedCount(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "getHostedCount";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {

					hosted = {};
					hosted = data.hosted;
					if ($.isFunction(completionHandler)) {
						completionHandler();
					}
				} else {
					//console.log("Could not logout on server! Force Logout");
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Registrant Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	}
}

function getRegistrantsByManager(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "getRegistrantsByManager";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		//loading(true, "Loading Registrants...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					//console.log("Returned JSON Object: "+JSON.stringify(data));
					report_registrants = {};
					report_registrants = data.registrants;
					if ($.isFunction(completionHandler)) {
						completionHandler();
					}
				} else {
					//console.log("Could not logout on server! Force Logout");
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Registrant Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
 	}
}

function getRegistrantsByPID(pid, status, completionHandler) {
    if (user["id"] != null && user["vtoken"] != null )    {
        var _query = {};

        _query["fn"] = "getRegistrantsByPID";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];
        _query["pid"] = pid;
        _query["status"] = status;

        var json_data = _query;
        console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
        console.log("Get "+status+" registrants for PID: "+pid);
        $.ajax({
            url: wsURL,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){
                if (data.status == 1) {
                    console.log("Get Registrants By PID Success.");
                    //console.log("Returned JSON Object: "+JSON.stringify(data));
                    registrants = {};


                    $.each(data.registrants, function(index,value){
                        if(!("isBroker" in value)) {
                            value["isBroker"] = "NO";
                        }
                        if(value["status"] == "active") {
                            registrants[value["id"]] = value;
                        } else {
                            registrants_inactive[value["id"]] = value;
                        }
                    });

                    if ($.isFunction(completionHandler)) {
                        if(_query["status"] == "active") {
                            completionHandler(registrants);
                        } else {
                            completionHandler(registrants_inactive);
                        }
                    }
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    forceLogout();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Registrants By PID Error: "+errorThrown);
                if ($.isFunction(completionHandler)) {
                    var empty_obj = {};
                    completionHandler(empty_obj);
                }
            },
            dataType: "json"
        });
     }
}


function editProfile(fields, completionHandler) {
	console.log(fields);
	if (user["id"] != null && user["vtoken"] != null )	{

		var _fields = fields;

		var _query = {};

		//Format form inputs into proper array
		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})

		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if ((_query["pphoto"] == "" || _query["pphoto"] == "N/A")) {
			_query["pphoto"] = "https://spac.io/assets/img/stock/pphoto.jpg";
			user["pphoto"] = _query["pphoto"];
		}


		if (!isValidEmailAddress(_query["email"])) {
			console.log(_query["email"]);
			showAlert("Invalid Email Address","You must enter a valid email address.");
		} else {
			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			loading(true, "Saving Changes...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));

						$.each(_query, function(index, value) {
							if (index != "fn") {
								user[index] = value;
								localStorage[index] = value;
							}
						})

						console.log(user["pphoto"]);
						loadProfile();
						showAlert("Changes Saved","Your changes have been saved.");

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else if (data.status == 2){
						console.log("Could not edit Profile! Email already taken.");
						showAlert("Email Already Taken","Sorry, this email has already been taken.");
					} else if (data.status == 3){
						console.log("Email Changed.");
						user = {};
						properties = {};

						localStorage.removeItem("id");
						localStorage.removeItem("vtoken");
						$('#loginManager_Form')[0].reset();
						showAlert("Email Changed","You must confirm this email address to login to this account again.","OK",null,
							function(){
								$(".alert-overlay").hide();
								forceLogout();
							}
							,null);
					} else if (data.status == 0){
						console.log("Could not edit Profile! Force Logout "+data.status);
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Beacon Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}

	} else {
		forceLogout();
	}

}

function editVisitor(completionHandler) {

	var temp_leads_arr = {};

          if(curRID in registrants) {
            temp_leads_arr = registrants;

          } else if(curRID in registrants_inactive) {
            temp_leads_arr = registrants_inactive;

          }


	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};
		_query["fn"] = "editRegistrant";
		_query["id"] = curRID;
		_query["name"] = $("#registrant-name").val();
		_query["email"] = $("#registrant-email").val();
		_query["phone"] = $("#registrant-phone").val();
		_query["note"] = $("#registrant-note").val();

		$.each(temp_leads_arr[curRID]["answersObj"],function(index, value){
			if(value["questionObj"]["type"] == "yn") {
				var key = "edit-answer-"+index;

				temp_leads_arr[curRID]["answersObj"][index]["answer"] = $("input[name='edit-answer-"+index+"']:checked").val();

			} else if(value["questionObj"]["question"] != "Are you working with an agent?") {
				temp_leads_arr[curRID]["answersObj"][index]["answer"] = $("#edit-answer-"+index).val();
			}
	 	});

		_query["answersObj"] = temp_leads_arr[curRID]["answersObj"];
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
		loading(true, "Saving Changes...");

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					 console.log("Returned JSON Object: "+JSON.stringify(data));
					 reloadRegistrants();
					 showAlert("Changes Saved","Your changes have been saved.");
					 alertTimer = setTimeout(function(){ $(".alert-overlay").hide(); }, 3000);
					 if ($.isFunction(completionHandler)) {
							 completionHandler();
					 }
				} else if (data.status == 2){
					console.log("Could not edit Profile! Email already taken.");
					showAlert("Email Already Taken","Sorry, this email has already been taken.");
				} else if (data.status == 0){
					console.log("Could not edit Profile! Force Logout "+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Visitor Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editRegistrant(fields, completionHandler) {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _fields = fields;
		var _query = {};

		//Format form inputs into proper array
		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})

		$.each(registrants[curRID]["answersObj"],function(index, value){
			if(value["questionObj"]["question"] == "Are you mortgage pre-approved?") {
				var key = "edit-answer-"+index;
				if (!(key in _query)) {
					registrants[curRID]["answersObj"][index]["answer"] = "";
				} else {
					registrants[curRID]["answersObj"][index]["answer"] = _query["edit-answer-"+index];
				}

			} else if(value["questionObj"]["question"] != "Are you working with an agent?") {
				registrants[curRID]["answersObj"][index]["answer"] = $("#edit-answer-"+index).val();
			}
	 	});

		_query["answersObj"] = registrants[curRID]["answersObj"];
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
		loading(true, "Saving Changes...");

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Returned JSON Object: "+JSON.stringify(data));

					$.each(_fields, function(index, value) {
						if (value["name"] != "fn") {
							registrants[curRID][value["name"]] = value["value"];
							console.log(registrants[curRID][value["name"]]);
						}
					})
					reloadRegistrants();
					showAlert("Changes Saved","Your changes have been saved.");

					if ($.isFunction(completionHandler)) {
						completionHandler();
					}

				} else if (data.status == 2){
					console.log("Could not edit Profile! Email already taken.");
					showAlert("Email Already Taken","Sorry, this email has already been taken.");
				} else if (data.status == 0){
					console.log("Could not edit Profile! Force Logout "+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Beacon Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});


	} else {
		forceLogout();
	}

}

function disableProperty() {

		var _query = {};
		_query["fn"] = "disableProperty";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;

		if ($("#enable-property").is(':checked')) {
			_query["disabled"] = "NO";
			properties[curPID]["disabled"] = "NO";
		} else {
			_query["disabled"] = "YES";
			properties[curPID]["disabled"] = "YES";
		}
		loading(true,"Saving...");
		var json_data = _query;
		console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {

					//localStorage["properties"] = JSON.stringify(properties);
					filterProperties();

				} else if(data.status == 0){
					//console.log("Could not logout on server! Force Logout");
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Disable Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});


}


function deleteHosted(hid, completionHandler) {
	if (online) {
		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "deleteHosted";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["hid"] = hid;

			loading(true, "Deleting Open House Session...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {
						getHostedCount(function(){
							if ($.isFunction(completionHandler)) {
								completionHandler();
							}
						});

					} else if(data.status == 2){
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else if(data.status == 0){
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Delete Registrant Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}
	}
}

function deleteRegistrant(rid) {
		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};

			_query["fn"] = "deleteRegistrant";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["rid"] = curRID;

			loading(true, "Deleting Registrant...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {
						delete registrants[rid];
						$("#popup-view-registrant").hide();
            loading(false);
            if(prevPanel == "visitors") {
                showVisitors(curPID);
            }
            if(prevPanel == "leads") {
                showLeadsPanel();
            }
					} else if(data.status == 2){
						delete registrants[rid];
            loading(false);
            $("#popup-view-registrant").hide();
            if(prevPanel == "visitors") {
                showVisitors(curPID);
            }
            if(prevPanel == "leads") {
                showLeadsPanel(curPID);
            }

					} else if(data.status == 0){
						//console.log("Could not logout on server! Force Logout");
            loading(false);
						forceLogout();
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Delete Registrant Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}

}


function createCSV(panel) {
		if (user["id"] != null && user["vtoken"] != null )	{
			var offset = new Date().getTimezoneOffset();
			if(curPID != "all") {
				propertyID = properties[curPID]["propertyID"];
			} else {
				propertyID = "all";
			}
			var _query = {};

			if(panel == "visitors") {
				_query["type"] = $("input[name='visitors-filter-type']:checked").val();
			}
			if(panel == "leads") {
				_query["type"] = $("input[name='leads-filter-type']:checked").val();
			}

			_query["type"] = _query["type"].toLowerCase();
			_query["fn"] = "createCSV";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["propertyID"] = propertyID;
			_query["offset"] = offset;

			if($("#registrants-filter-back").css("left") == "0px") {
				_query["type"] = "buyers";
			}

			if($("#registrants-filter-back").css("left") == "100px") {
				_query["type"] = "brokers";
			}

			loading(true, "Creating CSV File...");

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {


						showAlert("CSV File Sent", "Your CSV file has been sent to your email.");

					} else if (data.status == 0){
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Registrant Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}
}


function changePassword(fields,completionHandler) {

	var _fields = fields;
	var _query = {};

	//Format form inputs into proper array
	$.each(_fields, function(index, value) {
		_query[value["name"]] = value["value"];
	})

	if (_query["pass"] != _query["pass2"]) {
		showAlert("Passwords Must Match","Please enter matching passwords.");
	} else if (user["id"] != null && user["vtoken"] != null )	{
		var _query2 = {}

		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		_query2["fn"] = "getSalt";
		_query2["email"] = user["email"];

		loading(true, "Saving...");

		var json_data = _query2;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Returned JSON Object: "+JSON.stringify(data));
					console.log("Salt Value: "+data.salt);

					//Salt + hash password with returned value from server.
					_query["pass"] += data.salt;
					_query["pass"] = CryptoJS.SHA3(_query["pass"], { outputLength: 224 }).toString();

					_query["old_pass"] += data.salt;
					_query["old_pass"] = CryptoJS.SHA3(_query["old_pass"], { outputLength: 224 }).toString();
					delete _query["pass2"];
					var json_data = _query;
					console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

					$.ajax({
						url: wsURL,
						type:"POST",
						data: {json : json_data},
						success: function(data, textStatus, jqXHR){
							if (data.status == 1) {
								console.log("Returned JSON Object: "+JSON.stringify(data));

								user["vtoken"] = data.vtoken;
								localStorage["vtoken"] = data.vtoken;
								$('#changePassword_Form')[0].reset();
								showAlert("Password Changed","Password successfully changed.");
								if ($.isFunction(completionHandler)) {
									completionHandler();
								}

							} else if (data.status == 2) {
								showAlert("Invalid Password","The current password you entered is not valid.");

							} else {
								showAlert("Unknown Error","There was an unknown error.");
							}
							loading(false);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Salt Error: "+errorThrown);
							loading(false);
						},
						dataType: "json"
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				  console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Salt Error: "+errorThrown);
				  loading(false);
			 },
			 dataType: "json"
		  });
 	}

}

function addProperty(fields) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _fields = fields;
		var _query = {};


		//Format form inputs into proper array
		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})

		var img_field = _query["image"];

		if(_query["baths"] == "") {
			_query["baths"] = -1;
		}

		if(_query["beds"] == "") {
			_query["beds"] = -1;
		}

		if(_query["[price"] == "") {
			_query["price"] = "N/A";
		}

		if(_query["dimens"] == "") {
			_query["dimens"] = -1;
		}

		if (localStorage.getItem("defaultQuestions") === null) {
			defaultQuestions = [];
		} else {
			defaultQuestions = JSON.parse(localStorage["defaultQuestions"]);
		}

		_query["questionsObj"] = defaultQuestions;

		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		_query["desc"] = _query["desc"].replace(/\\n/g, "<br>");
		//_query["note"] = _query["note"].replace(/\\n/g, "<br>");

		if (_query["image"] == "" || _query["image"] == "N/A") {
			var randomNum = Math.floor((Math.random() * 10) + 1);
			_query["image"] = "https://spac.io/assets/img/stock/"+randomNum+".jpg";
		}

		if(_query["title"] == "") {
			showAlert("Title is a mandatory field","Please enter a Title.");
			//alert("Please enter a Title.");
		} else {
			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			loading(true, "Adding Property...");
			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));

						$('#addProperty_Form')[0].reset();
						getProperties(function(){
            	hidePopup("add-property");
							filterProperties(false);
						});
						loading(false);


					} else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
						loading(false);
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add Property Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}

	} else {
		forceLogout();
	}

}

function editTopProducer(fields) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _fields = fields;
		var _query = {};
		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})

		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Saving...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Integration Complete","Your Sign In form is now integrated with Top Producer / Market Snapshot.");

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not save your settings. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyCorpCode() {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "applyCorpCode"
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["corpcode"] =	$("input[name='corpcode']").val();
		_query["corpcode"] = _query["corpcode"].toLowerCase();

		loading(true, "Applying Corporate Branding Code...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Branding Code Applied","Your Corporate Branding Code has been applied successfully. Please reload your dashboard to have your corporate branding applied.");
					user["brand"] = _query["corpcode"];
					getBrand();
					//checkBrand();

				} else if (data.status == 2) {
					showAlert("Branding Code Invalid","Sorry, the Corporate Branding Code you provided does not exist.");

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearCorpCode() {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "applyCorpCode"
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["corpcode"] = "";
		$("input[name='corpcode']").val("");

		loading(true, "Applying Corporate Branding Code...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Branding Code Cleared","You no longer have a branded design attached to your account. Please reload the dashboard to have this change reflected.");
					user["brand"] = "N/A";
					//checkBrand();
					getBrand();

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function applyPromoCode() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyPromoCode";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["promo"] =	$("input[name='promocode']").val();

		loading(true, "Applying Promo Code...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Promo Code Applied","Your promo code has been succesfully applied to your account.");
					user["promocode"] = _query["promo"];

				} else if (data.status == 2) {
					showAlert("Branding Code Invalid","Sorry, the Promo Code you provided does not exist or has expired.");

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function verifyMLSEmail() {

	if($("select[name='mls']").val() == "Northstar") {
		$(".user-mls-id").hide();
		loading(true,"Verifying Your Email...");
		var mls_email =	user["email"];
		if(mls_email != "ting@spac.io" && mls_email != "melissa@spac.io") {
			$.ajax({
				url: "http://api.tlcengine.com/v2/api/nsmls/agents/verify",
				method:"POST",
				contentType:"application/x-www-form-urlencoded",
				data: {"email":mls_email},
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", "bearer 16E18DB9-26A1-4552-ABBA-6C2C33E17DBA");
				},
				success: function(data, textStatus, jqXHR){

					console.log("Returned JSON Object: "+data);
					if(data) {

					} else {
						$("select[name='mls']").val("N/A");
						showAlert("Verify Email","Sorry, your email cannot be verified with Northstar MLS. Please change the email you registered Spacio with in My Account/My Profile to your Northstar MLS email.");
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" Error: "+errorThrown);
				},
				dataType: "json"
			});
		} else {
			loading(false);
		}
	} else {

	}
}



function applyMLS() {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};
		_query["fn"] = "applyMLS";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["mls"] =	$("select[name='mls']").val();
		_query["mlsID"] =	$("input[name='userMLSID']").val();

		loading(true, "Saving MLS...");

		var json_data = _query;

		console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL_mls,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("MLS Setting","Your MLS settings have been saved.");
					user["mls"] = _query["mls"];

				} else if(data.status == 2){
					showAlert("Invalid MLS ID","Sorry, you did not provide a valid MLS ID.");
				}else {
					showAlert("Error","We could not apply this mls at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});


	} else {
		forceLogout();
	}

}

function clearMLS() {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};
		_query["fn"] = "applyMLS";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["mls"] =	"N/A";
		_query["mlsID"] ="";

		loading(true, "Clearing MLS...");

		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("MLS Setting","Your MLS setting have been cleared.");
					user["mls"] = _query["mls"];
					user["mlsID"] = _query["mlsID"];
					$("select[name='mls']").val("N/A");
					$("input[name='userMLSID']").val("");
				} else {
					showAlert("Error","We could not clear mls at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});


	} else {
		forceLogout();
	}
}




function sendAutoEmails() {
	var _query = {};
	_query["fn"] = "sendAutoEmails";
	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];
	_query["propertyID"] = properties[curPID]["propertyID"];

	var json_data = _query;

	console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
}

function editProperty(fields) {
	if(properties[curPID]["editable"] == false) {
		showAlert("Open House Details","The details of this open house was added using your company's feed and cannot be edited. Please contact your listings administrator for changes.");
	} else if (user["id"] != null && user["vtoken"] != null )	{
		var _fields = fields;
		var _query = {};
		// var fileChooser = document.getElementById('file-chooser');
		// var file = fileChooser.files[0];

		//Format form inputs into proper array
		$.each(_fields, function(index, value) {
			_query[value["name"]] = value["value"];
		})


		//_query["title"] = capitalizeFirstLetterEachWordSplitBySpace(_query["title"]);

		if(_query["baths"] == "") {
			_query["baths"] = -1;
		}

		if(_query["beds"] == "") {
			_query["beds"] = -1;
		}

		if(_query["[price"] == "") {
			_query["price"] = "N/A";
		}

		if(_query["dimens"] == "") {
			_query["dimens"] = -1;
		}


		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = curPID;
		_query["desc"] = _query["desc"].replace(/\\n/g, "<br>");
		_query["cnEnabled"] = properties[curPID]["cnEnabled"];
		_query["questionsObj"] = properties[curPID]["questionsObj"];
		_query["autoEmail"] = properties[curPID]["autoEmail"];
		_query["disabled"] = properties[curPID]["disabled"];
		if ("mobile" in properties[curPID]) {
			_query["mobile"] = properties[curPID]["mobile"];
		}

		if ((_query["image"] == "" || _query["image"] == "N/A") ) {
			var randomNum = Math.floor((Math.random() * 10) + 1);
			_query["image"] = "https://spac.io/assets/img/stock/"+randomNum+".jpg";
		}

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
		loading(true, "Saving Changes...");
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					//console.log("Returned JSON Object: "+JSON.stringify(data));
					getProperties(function(){
						filterProperties(false);
						hidePopup("view-property");
					});

				} else {
					//console.log("Could not logout on server! Force Logout");
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}

function deleteProperty(pid) {
	if (user["id"] != null && user["vtoken"] != null )	{
		var _query = {};

		_query["fn"] = "deleteProperty";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = pid;

		loading(true, "Deleting Open House...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				if (data.status == 1) {

					delete properties[pid];
					delete propertiesSearchResult[pid];
					hidePopup("view-property");
					filterProperties();
				} else if(data.status == 2){
					delete properties[pid];
					delete propertiesSearchResult[pid];
					hidePopup("view-property");
					filterProperties();
					//console.log("Could not logout on server! Force Logout");
				} else if(data.status == 0){
					//console.log("Could not logout on server! Force Logout");
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Delete Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
 	}

}

function getBrand(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null)	{
		var _query = {};

		_query["fn"] = "getBrand";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));


		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				if (data.status == 1) {

					brand["leftpanel"] = data.leftpanel;
					brand["imgs"] = data.imgs;
					brand["font"] = data.font;
					brand["color"] = data.color;

					localStorage["brand"] = JSON.stringify(brand);

					//$('.signin-left-panel').append("<div class='leads-list-button' onclick='showRegistrants(curPID)'></div>");

					if ($.isFunction(completionHandler)) {
						completionHandler();
					}
				} else if (data.status == 2){
					if ($.isFunction(completionHandler)) {
						completionHandler();
					}

				} else {

				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: get brand error Error: "+errorThrown);
			},
			dataType: "json"
		});
	}
}

function uploadToS3(image, fileName, bucketName, completionHandler) {

		console.log("file name:"+ fileName);
		console.log("bucket name:"+ bucketName);
		console.log("image type:"+ image.type);

		var image_url = "https://s3.amazonaws.com/spacio-user-images/"+fileName;
		var credentials = {accessKeyId: 'AKIAJDN53KWKDV634WPQ', secretAccessKey: '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'};
		console.log("here");
		AWS.config.update(credentials);
		AWS.config.region = 'us-east-1';
		console.log("here2");
		var bucket = new AWS.S3({params: {Bucket: bucketName}});
		var params = {Key: fileName, ContentType: image.type, Body: image};
		console.log("here3");
		//loading(true, "Uploading Picture");
		bucket.upload(params, function (err, data) {
			if (err) {
				alert(err);
			} else {
				console.log("Successfully Uploaded");
				if ($.isFunction(completionHandler)) {
					completionHandler();
				}
			}
			loading(false);
		});
}



function getZillowReview() {

  var _query = {};
  _query["fn"] = "getZillowReview";
  _query["managerID"] = user["id"];
  _query["vtoken"] = user["vtoken"];

  var json_data = _query;
  console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

  $.ajax({
      url: wsURL_new,
      type:"POST",
      data: {json : json_data},
      success: function(data, textStatus, jqXHR){
          console.log("Returned JSON Object: "+JSON.stringify(data));

          if (data.status == 1) {
              zillowreviewFeed = data.response;
              localStorage["zillowreviewFeed"] = JSON.stringify(zillowreviewFeed);
          } else {
              console.log("Zillow Review Failed");
          }
          //loading(false);
      },
      error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: zillowreviewFeed error Error: "+errorThrown);
      },
      dataType: "json"
  });

}

function getIDCTestimonials(completionHandler) {

		//alert("TT");
		var _query = {};
		_query["fn"] = "getIDCTestimonials";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				if (data.status == 1) {
					console.log("Returned JSON Object: "+JSON.stringify(data));
					idcFeed = data.testimonials;
					if ($.isFunction(completionHandler)) {
						completionHandler();
					}
				} else {

				}
				//loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getIDCTestimonials error Error: "+errorThrown);
			},
			dataType: "json"
		});
}

function getTestimonialTree(completionHandler) {

		//alert("TT");
		var _query = {};
		_query["fn"] = "getTestimonialTree";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("TT Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					ttFeed = data.response;
					if ($.isFunction(completionHandler)) {
						completionHandler();
					}
				} else {

				}
				//loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getTestimonialTree error Error: "+errorThrown);
			},
			dataType: "json"
		});
}



function getRS(completionHandler) {
		if (user["integrations"]["rsID"] != "" && user["integrations"]["rsID"] != "N/A") {
			console.log("Getting RS...");
				$.get('https://secure.realsatisfied.com/rss/agent/'+user["integrations"]["rsID"],
					function(xml){
						rsFeed = $.xml2json(xml);
						//localStorage["rsFeed"] = JSON.stringify(rsFeed);
						console.log("RS Feed retrieved.");

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					});
		}

}




function resendConfirmationEmail() {

	var _query = {};
	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];
	_query["fn"] = "resendConfirmationEmail";

	var json_data = _query;
	console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				$(".alert-overlay").hide();
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Resend confirmation Error: "+errorThrown);
				$(".alert-overlay").hide();
				loading(false);
			},
			dataType: "json"
		});

}

function sendTestEmail() {

		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};
			_query["fn"] = "sendTestEmail";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["type"] = curEmailTemplate;

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			loading(true,"Sending Test Email...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {
						showAlert("Send Test Email","A test email has been sent to you.");
					}

					if (data.status == 2) {
						showAlert("Send Test Email","There was error trying to send you a test email.");
					}

					if (data.status == 0) {
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Update Hosted Count Error: "+errorThrown);
				},
				dataType: "json"
			});

		}

}

function sendTestEmailWithPID() {

		if (user["id"] != null && user["vtoken"] != null )	{
			var _query = {};
			_query["fn"] = "sendTestEmail";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["type"] = "customer";
      _query["pid"] = curPID;

			if(properties[curPID]["brokersEnabled"] == "YES") {
				_query["type"] = "broker";
			}

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			loading(true,"Sending Test Email...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));
					if (data.status == 1) {
						showAlert("Send Test Email","A test email has been sent to you.");
					}

					if (data.status == 2) {
						showAlert("Send Test Email","There was error trying to send you a test email.");
					}

					if (data.status == 0) {
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Update Hosted Count Error: "+errorThrown);
				},
				dataType: "json"
			});

		}
}


function resetDefaultScoring(completionHandler) {
	lead_score_calibration = default_lead_score_calibration;

		if (user["id"] != null && user["vtoken"] != null )	{

			var _query = {};

			_query["fn"] = "editScoreCalibration";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["score_calibration"] = lead_score_calibration;

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			loading(true, "Resetting Default Preferences...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						loading(false);
						loadScoring();
						showAlert("Reset Default","Ideal Customer Profile reset to default preferences.");
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else if(data.status == 2){
						lead_score_calibration = default_lead_score_calibration;
					} else {
						console.log("Could not edit score calibration! Force Logout "+data.status);
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Score Calibration Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});


		} else {
			forceLogout();
		}

}

function getScoreCalibration(completionHandler) {
	var _query = {};
	_query["fn"] = "getScoreCalibration";
	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];

	var json_data = _query;
	console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
	loading(true, "Retrieving Preferences...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						//loading(false);

						$.each(default_lead_score_calibration, function(index,value){
							var found = false;
							$.each(lead_score_calibration, function(i,v){
								if (value["question"] == v["question"]) {
									found = true;
								}
							})
							if(!found) {
								lead_score_calibration.push(value)
							}
						})

						lead_score_calibration = data.calibration;
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else if(data.status == 2) {
						lead_score_calibration = default_lead_score_calibration;
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
						console.log("Could not edit score calibration! Force Logout "+data.status);
						forceLogout();
					}
					//loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Score Calibration Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
}

function editScoreCalibration(fields, completionHandler) {
	console.log(fields);

	var _fields = {};
	$.each(fields, function(index, value) {
		_fields[value["name"]] = value["value"];
	})

	if ("q0" in _fields) {
		if(_fields["q0"] == "INDIFF") {
			lead_score_calibration[0]["scoring"]["YES"] = 1;
			lead_score_calibration[0]["scoring"]["NO"] = 1;
			lead_score_calibration[0]["max"] = 1;
		} else if(_fields["q0"] == "YES") {
			lead_score_calibration[0]["scoring"]["YES"] = 2;
			lead_score_calibration[0]["scoring"]["NO"] = 1;
		} else {
			lead_score_calibration[0]["scoring"]["YES"] = 1;
			lead_score_calibration[0]["scoring"]["NO"] = 2;
		}
	}
	if ("q1" in _fields) {

		if(_fields["q1"] == "INDIFF") {
			lead_score_calibration[1]["scoring"]["YES"] = 1;
			lead_score_calibration[1]["scoring"]["NO"] = 1;
			lead_score_calibration[1]["max"] = 1;
		} else if(_fields["q1"] == "YES") {
			lead_score_calibration[1]["scoring"]["YES"] = 2;
			lead_score_calibration[1]["scoring"]["NO"] = 1;
		} else {
			lead_score_calibration[1]["scoring"]["YES"] = 1;
			lead_score_calibration[1]["scoring"]["NO"] = 2;
		}
	}
	if ("q3" in _fields) {
		if(_fields["q3"] == "INDIFF") {
			lead_score_calibration[3]["scoring"]["Rent"] = 1;
			lead_score_calibration[3]["scoring"]["Own"] = 1;
			lead_score_calibration[3]["max"] = 1;
		} else if(_fields["q3"] == "rent") {
			lead_score_calibration[3]["scoring"]["Rent"] = 2;
			lead_score_calibration[3]["scoring"]["Own"] = 1;
		} else {
			lead_score_calibration[3]["scoring"]["Rent"] = 1;
			lead_score_calibration[3]["scoring"]["Own"] = 2;
		}
	}
	if ("q4" in _fields) {
		if(_fields["q4"] == "INDIFF") {
			lead_score_calibration[4]["scoring"]["Buy"] = 1;
			lead_score_calibration[4]["scoring"]["Sell"] = 1;
		} else if(_fields["q4"] == "buy") {
			lead_score_calibration[4]["scoring"]["Buy"] = 2;
			lead_score_calibration[4]["scoring"]["Sell"] = 1;
		} else {
			lead_score_calibration[4]["scoring"]["Buy"] = 1;
			lead_score_calibration[4]["scoring"]["Sell"] = 2;
		}
	}
	if ("q7" in _fields) {
		if(_fields["q7"] == "INDIFF") {
			lead_score_calibration[7]["scoring"]["Live-in"] = 1;
			lead_score_calibration[7]["scoring"]["Investment"] = 1;
			lead_score_calibration[7]["max"] = 1;
		} else if(_fields["q7"] == "livein") {
			lead_score_calibration[7]["scoring"]["Live-in"] = 2;
			lead_score_calibration[7]["scoring"]["Investment"] = 1;
		} else {
			lead_score_calibration[7]["scoring"]["Live-in"] = 1;
			lead_score_calibration[7]["scoring"]["Investment"] = 2;
		}
	}
	if ("q8" in _fields) {
		if(_fields["q8"] == "INDIFF") {
			lead_score_calibration[8]["scoring"]["YES"] = 1;
			lead_score_calibration[8]["scoring"]["NO"] = 1;
			lead_score_calibration[8]["max"] = 1;
		} else if(_fields["q8"] == "YES") {
			lead_score_calibration[8]["scoring"]["YES"] = 2;
			lead_score_calibration[8]["scoring"]["NO"] = 1;
		} else {
			lead_score_calibration[8]["scoring"]["YES"] = 1;
			lead_score_calibration[8]["scoring"]["NO"] = 2;
		}
	}
	if ("q9" in _fields) {
		if(_fields["q9"] == "INDIFF") {
			lead_score_calibration[9]["scoring"]["Cash"] = 1;
			lead_score_calibration[9]["scoring"]["Financing"] = 1;
			lead_score_calibration[9]["max"] = 1;
		} else if(_fields["q9"] == "cash") {
			lead_score_calibration[9]["scoring"]["Cash"] = 2;
			lead_score_calibration[9]["scoring"]["Financing"] = 1;
		} else {
			lead_score_calibration[9]["scoring"]["Cash"] = 1;
			lead_score_calibration[9]["scoring"]["Financing"] = 2;
		}
	}

		if (user["id"] != null && user["vtoken"] != null )	{

			var _query = {};

			_query["fn"] = "editScoreCalibration";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["score_calibration"] = lead_score_calibration;

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
			loading(true, "Saving Preferences...");

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						loading(false);
						localStorage["lead_score_calibration"] = JSON.stringify(lead_score_calibration);

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else {
						console.log("Could not edit score calibration! Force Logout "+data.status);
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Score Calibration Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

		} else {
			forceLogout();
		}

}

function saveCreditCard(_fields, form) {

	var $form = $(form);
	var _query = {};

	$.each(_fields, function(index, value) {
		_query[value["name"]] = value["value"];
	})

	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];
	_query["fn"] = "saveCreditCard";

	loading(true,"Updating Payment Info...");
	$form.find('button').prop('disabled', true);

	Stripe.card.createToken($form, function(status, response) {
				if(response.error) {
					showAlert("Error Processing Card", response.error.message,"Ok");
					loading(false);
				} else {
					var token = response.id;
					_query["stripeToken"] = token;

					var json_data = _query;
					console.log("Outbound JSON Query: "+JSON.stringify(json_data));

					$.ajax({
						url: wsURL,
						type:"POST",
						data: {json : json_data},
						success: function(data, textStatus, jqXHR){
							console.log("Returned JSON Object: "+JSON.stringify(data));

							if (data.status == 1) {
								showAlert("Payment Information Updated","Thank you, your new card information has been saved.");

								getManagerPaymentInfo(function(){

									$('#saveCreditCard_Form')[0].reset();
									//$("#payment-info").html(data.cc_type+" ending in "+data.last4);
									loading(false);
								});

								loading(false);
								//alert(data.cus);
							} else if (data.status == 3) {
								showAlert("Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
								loading(false);
							} else {
								if (data.error_type == "invalid_request_error" && data.error_param == "coupon") {
									showAlert("Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
								} else if (data.error_type =="card_error" && data.error_code == "card_declined"){
									showAlert("Card Declined","Sorry, your card was declined. Please verify that you entered a valid credit card.");
								} else {
									showAlert("Unknowns","There was an unknown error. Please contact support.");
								}
								loading(false);
							}
							//debugLog("Query Status: "+data.status);

						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
						},
						dataType: "json"
					});
				}
			});


}


function upgradeAccount(_fields, form) {

	var $form = $(form);
	var _query = {};


	$.each(_fields, function(index, value) {
		_query[value["name"]] = value["value"];
	})

	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];
	_query["fn"] = "upgradeAccountByID";

	loading(true,"Loading...");
	$form.find('button').prop('disabled', true);

	Stripe.card.createToken($form, function(status, response) {
				if(response.error) {
					showAlert("Error Processing Card", response.error.message,"Ok");
					loading(false);
				} else {
					var token = response.id;
					_query["stripeToken"] = token;

					var json_data = _query;
					console.log("Outbound JSON Query: "+JSON.stringify(json_data));

					$.ajax({
						url: wsURL,
						type:"POST",
						data: {json : json_data},
						success: function(data, textStatus, jqXHR){
							console.log("Returned JSON Object: "+JSON.stringify(data));

							if (data.status == 1) {


								loading(false);
								location.reload();
								//alert(data.cus);
							} else if (data.status == 3) {
								showAlert("Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
								loading(false);
							} else {
								if (data.error_type == "invalid_request_error" && data.error_param == "coupon") {
									showAlert("Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
								} else if (data.error_type =="card_error" && data.error_code == "card_declined"){
									showAlert("Card Declined","Sorry, your card was declined. Please verify that you entered a valid credit card.");
								} else {
									showAlert("Unknowns","There was an unknown error. Please contact support.");
								}
								loading(false);
							}
							//debugLog("Query Status: "+data.status);

						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
						},
						dataType: "json"
					});
				}
			});

}


function createShareLink(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )	{

		var d = new Date();

		var report_pid = $("#report-property-filter").val();
		var report_daterange = $("#daterangepicker-reporting").val();

		var report_enddate = d.getTime();
		var report_startdate = 0;

		if (report_daterange != "" && report_daterange != null) {

			report_daterange = $.parseJSON(report_daterange);
			//alert(report_daterange["start"]);
			var temp_startdate = new Date(report_daterange["start"]);
			var temp_enddate = new Date(report_daterange["end"]);

			var options = {
	    		year: "numeric", month: "short", day: "numeric"
			};

			var temp_startdate_string = temp_startdate.toLocaleDateString(temp_startdate.getTimezoneOffset(),options);
			var temp_enddate_string = temp_enddate.toLocaleDateString(temp_enddate.getTimezoneOffset(),options);

			report_startdate = temp_startdate.getTime();
			report_enddate = temp_enddate.getTime();
		}

		var rankings_html = "";
		rankings_html = $(".oh-rankings").html();

		var _query = {};

		_query["fn"] = "createShareLink";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["pid"] = report_pid;
		_query["start"] = report_startdate;
		_query["end"] = report_enddate;
		_query["stats"] = reportStats;
		_query["activeSubCategories"] = activeSubCategories;
		_query["rankings_html"] = rankings_html;

		loading(true, "Loading...");

			var json_data = _query;

			//console.log("MSG: "+msg);
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {

						console.log("Returned JSON Object: "+JSON.stringify(data));

						showAlert("Share Link <span id='copied'></span>","Copy and paste this link to share with anyone you want to see this report:<br/><br/><input id='copylink' style='padding:5px;background:#eee;width:90%;height:30px;font-size:16px;line-height:30px;' value='https://spac.io/dashboard/reports.php?r="+data.reportID+"'/>","Copy","Cancel",function(){
							var origSelectionStart, origSelectionEnd;
							var elem = document.getElementById("copylink");
        					origSelectionStart = elem.selectionStart;
        					origSelectionEnd = elem.selectionEnd;

							var currentFocus = document.activeElement;
   							elem.focus();
							elem.setSelectionRange(0, elem.value.length);

							var succeed;
							try {
								succeed = document.execCommand("copy");
							} catch(e) {
								succeed = false;
							}

							if(succeed) {
								$("#copied").html("(Copied)");
									setTimeout(function(){$(".alert-overlay").hide();}, 3000);
								//alert("Copied to Clipboard");
							}
						});
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}


					} else if (data.status == 2) {
						console.log("Returned JSON Object: "+JSON.stringify(data));

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}

					} else {
						console.log("Could not logout on server! Force Logout. Status:"+data.status);
						forceLogout();
					}
					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: createShareLink error Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});


		} else {
			forceLogout();
		}
}

function searchListings() {

    if($("#listings-search-terms").val() == "") {
        showAlert("Invalid Search Term","Cannot have blank search term.");
    } else if (online) {
		console.log("Search Listings...");

		if (user["id"] != null && user["vtoken"] != null )	{

			var _query = {};

			_query["fn"] = "searchListingsBank";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
      _query["searchTerms"] = $("#listings-search-terms").val();

			loading(true);

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
                        console.log("Returned JSON Object: "+JSON.stringify(data));
                        loading(false);
                        $(".listings-search-results").html("");
                        listingsSearchResult = data.searchResults;

                        if(listingsSearchResult.length == 0) {
														$(".listings-search-results").html("<div style='font-family:effra_bold;padding-left:20px;box-sizing:border:box;font-size:18px;height:40px;line-height:40px;color:#333;'>No properties found.</div>")
														$(".search-results-count").html("");
                        } else {
                            $.each(data.searchResults, function(index,value){
                                $(".listings-search-results").append("<div class='listing-result'><div class='listing-image' style='background-image:url("+value["image"]+")'></div><div class='listing-info'><div class='listing-address main-color-font'>"+value["addr1"]+", "+value["addr2"]+"</div><div class='listing-agent'>Listing Agent: "+value["agent"]+"</div></div><div class='add-listing-button' onclick='confirmAddListing("+index+")'><i class='fa fa-plus' aria-hidden='true'></i></div></div>")
                            });

                            if(listingsSearchResult.length == 1) {
                                $(".search-results-count").html("1 property found.");
                             } else {
                                $(".search-results-count").html(listingsSearchResult.length+" properties found.");

                             }
                        }
					} else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
						loading(false);
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}
	} else {
        showAlert("Offline Mode","This function requires a live internet connection.");

    }


}

function addListing(index) {
    if (online) {
		console.log("Adding Listing...");

		if (user["id"] != null && user["vtoken"] != null )	{

			var _query = {};

			_query["fn"] = "addListing";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
            _query["listingID"] = listingsSearchResult[index]["id"];

			loading(true);

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						getProperties(function(){
								loading(false);
								hidePopup("add-property");
								clearListingsSearchResults();
								filterProperties(false);
						});
					} else if (data.status == 2) {
                showAlert("Duplicate Property","This property is already in your account.");
                loading(false);
					} else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
						loading(false);
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}
	} else {
        showAlert("Offline Mode","This function requires a live internet connection.");

    }

}

function getTeam(completionHandler) {
		console.log("fetching team members from server");

		if (user["id"] != null && user["vtoken"] != null )	{

			var _query = {};

			_query["fn"] = "getTeam";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

			loading(true);

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));


						if (localStorage.getItem("team") === null) {
							team = {};
						} else {
							team = JSON.parse(localStorage["team"]);
						}

						$.each(data.team, function(index,value){
							team[value["id"]] = value;
						});
						localStorage["team"] = JSON.stringify(team);

						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
						//console.log("Could not logout on server! Force Logout");
						forceLogout();
						loading(false);
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
		}
}

function addDocData(docIndex, status,docData, completionHandler) {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};

		_query["fn"] = "addDocData";
		_query["registrantID"] = curRID;
		_query["data"] = docData;
		_query["status"] = status;
    _query["docIndex"] = docIndex;
		_query["userID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Saving...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					//console.log("Returned JSON Object: "+JSON.stringify(data));

					registrantDocs[docIndex] = {};

          registrants[curRID]["docs"][docIndex] = {};
          registrants[curRID]["docs"][docIndex]["status"] = status;
          registrants[curRID]["docs"][docIndex]["data"] = docData;

					if(curPanel == "form") {
						$("#doc-"+docIndex+" .icon .completed").show();
						$(".sign-doc-panel").hide();
						$(".select-docs-panel").show();
					} else {
						$("#doc-"+docIndex+" .icon .completed").show();
						$(".docs-panel").hide();
						$(".sign-doc-panel").hide();
						showRegistrant(curRID);
					}


    		} else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: addDisclosureForm error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}

function scheduleOpenhouses(ohs_to_schedule, completionHandler) {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};

		_query["fn"] = "scheduleOpenhouses";

    _query["ohs"] = ohs_to_schedule;
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Saving...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Result JSON Query: "+ JSON.stringify(data));

				if (data.status == 1) {
          if ($.isFunction(completionHandler)) {
              completionHandler();
          }
        } else if(data.status == 2){
					showAlert("Permission Denied","You can only schedule open houses for listings you own.");
					if ($.isFunction(completionHandler)) {
              completionHandler();
          }
				} else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: addDisclosureForm error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}

function getScheduledOpenhouses(pid, completionHandler) {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};

		_query["fn"] = "getScheduledOpenhouses";
        _query["pid"] = pid;
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Saving...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
          openhouses_scheduled = data.scheduled_openhouses;
          console.log(data);
          if ($.isFunction(completionHandler)) {
              completionHandler();
          }
        } else if(data.status == 2){
					openhouses_scheduled = [{"id":"restricted"}];
					console.log(data);
					if ($.isFunction(completionHandler)) {
							completionHandler();
					}
				} else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: addDisclosureForm error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}

function deleteScheduledOpenhouse(oid, AAListingID, completionHandler) {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "deleteScheduledOpenhouse";
    _query["oid"] = oid;
		_query["AAListingID"] = AAListingID;
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "");
		$(".alert-overlay").hide();

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Returned JSON Query: "+ JSON.stringify(data));

            if ($.isFunction(completionHandler)) {
                completionHandler();
            }
        } else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: deleteScheduledOpenhouses error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}

function editScheduledOpenhouse(oh, completionHandler) {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};

		_query["fn"] = "editScheduledOpenhouse";
        _query["oh"] = oh;
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Saving...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Inbound JSON Query: "+ JSON.stringify(data));

                    if ($.isFunction(completionHandler)) {
                        completionHandler();
                    }
                } else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: deleteScheduledOpenhouses error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}

function applyInAppBranding() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};

		_query["fn"] = "applyInAppBranding";
    _query["iabCode"] = $("#iabCode").val();
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Saving...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Inbound JSON Query: "+ JSON.stringify(data));

				if (data.status == 1) {
					location.reload();

        } else if(data.status == 2){
					showAlert("Error","Invalid InApp Branding Code. Ask Ting.");
				}else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: applyInAppBranding error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}

function clearInAppBranding() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};

		_query["fn"] = "clearInAppBranding";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Saving...");

		var json_data = _query;
		console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				if (data.status == 1) {
					console.log("Inbound JSON Query: "+ JSON.stringify(data));
					location.reload();

        } else {
					console.log("Could not logout on server! Force Logout. Status:"+data.status);
					forceLogout();
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: deleteScheduledOpenhouses error Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}


function editNotificationsSettings() {

    if (user["id"] != null && user["vtoken"] != null )    {
        user["notifications"] = {};

        if ($("#newleadsnotification").is(':checked')) {
            user["notifications"]["newleads"] = "YES";
        } else {
            user["notifications"]["newleads"] = "NO";
        }

        if ($("#autoemailstatusnotification").is(':checked')) {
            user["notifications"]["autoemailstatus"] = "YES";
        } else {
            user["notifications"]["autoemailstatus"] = "NO";
        }

        if ($("#readreceiptnotification").is(':checked')) {
            user["notifications"]["readreceipt"] = "YES";
        } else {
            user["notifications"]["readreceipt"] = "NO";
        }

        var _query = {};

        _query["fn"] = "editNotificationsSettings";
        _query["notifications"] = user["notifications"];
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        loading(true, "Saving...");

        var json_data = _query;
        console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){
                if (data.status == 1) {

                    //showAlert("Settings Saved","Your email notifications settings have been saved.");

                } else {
                    console.log("Could not logout on server! Force Logout. Status:"+data.status);
                    forceLogout();
                }
                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: deleteScheduledOpenhouses error Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });
    } else {
        forceLogout();
    }

}


/*


*/
