<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$propertyID = isset($_GET['id']) ? $_GET['id'] : '';
	$color = isset($_GET['c']) ? $_GET['c'] : '#2e2e2e';
	$color2 = isset($_GET['c2']) ? $_GET['c2'] : '2e2e2e';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <title>Spacio: Print Instructions</title>

	<link href="/dashboard/js/jquery-ui.min.css" rel="stylesheet">
	<link href="/fonts/fonts.css" type="text/css" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
		<link rel="icon" href="/assets/img/favicon.png">

	<script type="text/javascript" src="/dashboard/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="/dashboard/js/functions.js"></script>
	<script type="text/javascript" charset="utf-8" src="/dashboard/js/connectors.js"></script>
  <script type="text/javascript" charset="utf-8" src="/dashboard/js/jquery.qrcode.min.js"></script>

    <script src="/dashboard/js/jquery-ui.min.js"></script>
    <script src="/dashboard/js/moment.js"></script>

  </head>

<body>

	<div class="main-container" style="font-family:proxima_nova_rgregular;">
    	<div class="center-container">
        	<span style="font-size:40px;" class="property-title">441 East 57th Street 2</span><Br/><Br/>
            <span style="font-size:20px;color:#666;">Hosted By:</span><br/><br/>
            <div class="profile-picture" style="border:2px solid #<?php echo $color;?>"><img src=""/></div><br/>
            <span class="" style="font-size:24px;">
            	<span class="profile-name">Melissa Kwan</span> of <span class="profile-brokerage">Spacio Realty</span>
            </span><br/>
            <span style="font-size:20px;color:#666;" class="profile-title">Licensed Agent</span><br/><Br/>
            <br/><div class="seperator" style="width:300px;margin:10px auto;border-bottom:3px solid #<?php echo $color;?>"></div><br/>
            <span class="" style="font-size:24px;">Please Sign In From Your Mobile at</span><br/>

             <br/><br/>
             <div id="qrcode"></div>
						 <br/><br/>
              <span style="font-size:40px;">spac.io/l/<?php echo $propertyID;?></span><br/>
        </div>

    </div>

<script type="text/javascript">

	var propertyID = "<?php echo $propertyID;?>";
	var color = "<?php echo $color;?>";
	var color2 = "<?php echo $color2;?>";
	var property = {};
	var user = {};

	$(document).ready(function(){
		$("#qrcode").qrcode({
    		"text": "https://spac.io/l/"+propertyID
		});

		$(".profile-picture").css("border","3px solid #"+color);
		$(".seperator").css("border-bottom","3px solid #"+color);
		$(".property-propertyID").css("color","#"+color);
		getFormByID();

	})

	function getFormByID() {

	  var _query = {};

	  _query["fn"] = "getFormByID";
	  _query["propertyID"] = propertyID;
	  property["propertyID"] = _query["propertyID"];
	  var json_data = _query;
	  loading(true, "Retrieving Printout Instructions...");
	  console.log("Outgoing JSON Object: "+JSON.stringify(json_data));
	  $.ajax({
		  url: wsURL,
		  type:"POST",
		  data: {json : json_data},
		  success: function(data, textStatus, jqXHR){
			  if (data.status == 1) {
				  console.log("Returned JSON Object: "+JSON.stringify(data));

				  property["title"] = data.title;
				  property["image"] = data.image;
				  property["questions"] = data.questions;
				  property["mls"] = data.mls;
				  property["mlsnum"] = data.mlsnum;
				  property["anon"] = data.anon;
				  property["cnEnabled"] = data.cnEnabled;

				  user["fname"] = data.agent_fname;
				  user["lname"] = data.agent_lname;
				  user["email"] = data.agent_email;
				  user["phone"] = data.agent_phone;
				  user["title"] = data.agent_title;
				  user["brokerage"] = data.agent_brokerage;
				  user["marketsnapID"] = data.agent_marketsnapID;
				  user["id"] = data.agent_id;
				  user["vtoken"] = data.agent_vtoken;
				  user["pphoto"] = data.agent_pphoto;

				  $(".property-title").html(property["title"]);
				  $(".property-propertyID").html(propertyID);
				  $(".profile-picture img").attr("src",user["pphoto"]);
				  $(".profile-name").html(user["fname"]+" "+user["lname"]);
				  $(".profile-brokerage").html(user["brokerage"]);
				  $(".profile-title").html(user["title"]);

			  } else {
				  showAlert("Invalid Listing ID","Sorry, we can't find a listing matching this ID.");
			  }
			  loading(false);
		  },
		  error: function(jqXHR, textStatus, errorThrown) {
			  console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getEmailTemplate error Error: "+errorThrown);
			  loading(false);
		  },
		  dataType: "json"
	  });

  }


</script>

<style>
/***************************** ALERT BOX ****************************/
.main-container {
	width:850px;
	height:1100px;
	text-align:center;
	display:table;
	box-sizing:border-box;
	border:2px dashed #999;
}

.center-container {
	display:table-cell;
	vertical-align:middle;
	width:100%;
	padding:40px;

}

.profile-picture {
	margin:0 auto;
	border-radius:100px;
	width:100px;
	height:100px;
	border:3px solid #2e2e2e;
	overflow:hidden;
}

.profile-picture img {
	height:100px;
	width:100px;
}

#qrcode {
    padding-left: 0;
    padding-right: 0;
    margin-left: auto;
    margin-right: auto;
    display: block;
    width: 300px;
	height:300px;
	
}



</style>

</body>
</html>
