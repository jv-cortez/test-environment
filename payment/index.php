<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$chargeID = $_GET["i"];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/assets/img/favicon.png">

        <title>Spacio: Introducing the Paperless Open House</title>

        <link href="/assets/css/normalize.css" rel="stylesheet">
        <link href="/assets/css/website.css" rel="stylesheet">
        <link href="/assets/css/payment.css" rel="stylesheet">

        <link href="/assets/fonts/fonts.css" rel="stylesheet">

        <link rel="import" id="topmenu" href="/topmenu.html">
        <link rel="import" id="footer" href="/footer.html">
        <link rel="import" id="popups" href="/popups.html">
        <link rel="import" id="alertbox" href="/alertbox.html">

      </head>

<body style="padding-top:0;">

	<div class="spacio-top-menu"></div>
  <!-- TOP MENU END -->
	<div class="popup-overlay"></div>
  <!-- POPUP OVERLAY END -->
  <div class="alert-overlay"></div>
  <!-- ALERT OVERLAY END -->
	<div class="loading-panel">
    	<div style="display:flex;flex-direction:column;align-items:center;justify-content:center;width:100%;height:100%;">
        	<div style="height:50px;text-align:center;color:white;">
        		<span class="msg" style="-webkit-touch-callout:none;-webkit-user-select: none;">Loading...</span>
          </div>
      </div>
  </div> <!---- LOADING PANEL OVERLAY --->


  <div class="payment-scroll-one">
		<div class="payment-panel">

					<form id="payment_Form" style="padding-top:120px;">
            <div class="bold" style="width:100%;height:30px;line-height:30px;letter-spacing:3px;margin-bottom:0px;">
              BILL TO
            </div>
            <div class="charge-info bill-to" style="margin-top:10px;">

						</div>


            <div class="charge-info">
							<div class="bold" style="letter-spacing:3px;margin-bottom:10px;">ITEM DESCRIPTION</div>
							<div class="charge-type">
								<div class="left-column"></div>
								<div class="right-column"></div>
							</div>
						</div>

            <div class="bold" style="width:100%;height:30px;line-height:30px;letter-spacing:3px;margin-bottom:20px;">
              CARD INFO
            </div>
						<div style="30px;line-height:30px;width:100%;margin-bottom:10px;">
							<img src="/assets/img/website/mastercard.png" height="30" style="margin-right:10px;">
							<img src="/assets/img/website/visa.png" height="30" style="margin-right:10px;">
							<img src="/assets/img/website/amex.png" height="30" style="margin-right:10px;">
						</div>
            <div class="payment-input-flex">
							<div class="payment-label">Email*</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" name="email" value="" size="20" tabindex="0"  placeholder="">
						</div>
            <div class="payment-input-flex">
							<div class="payment-label">Name on Card</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="name" value="" size="20" tabindex="1"  placeholder="">
						</div>
						<div class="payment-input-flex">
							<div class="payment-label">Card #</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="number" value="" size="20" tabindex="2"  placeholder="">
						</div>
						<div class="payment-input-flex">
							<div class="payment-label">CVC</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="cvc" value="" size="4" tabindex="3"  placeholder="">
						</div>
						<div class="payment-input-flex">
							<div class="payment-label">Expiration</div>
							<div class="payment-seperator"></div>
              <div style="flex-grow:1;height:40px;">
  							<div class="payment-expiration-row" style="flex-grow:1">
  								<input class="payment-input-half" type="text" data-stripe="exp-month" value="" tabindex="4" size="3"  placeholder="MM" style="width:30px"> /
  								<input class="payment-input-half" type="text" data-stripe="exp-year" value="" tabindex="5" size="4"  placeholder="YYYY" style="width:45px">
  							</div>
              </div>
						</div>
            <div class="bold" style="width:100%;height:60px;line-height:60px;letter-spacing:3px;">
              BILLING ADDRESS
            </div>
            <div class="payment-input-flex">
							<div class="payment-label">Address Line 1</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="address-line1" value="" size="4" tabindex="6"  placeholder="">
						</div>
            <div class="payment-input-flex">
							<div class="payment-label">Address Line 2</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="address-line2" value="" size="4" tabindex="7"  placeholder="">
						</div>
            <div class="payment-input-flex">
							<div class="payment-label">City</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="address-city" value="" size="4" tabindex="8"  placeholder="">
						</div>
            <div class="payment-input-flex">
							<div class="payment-label">State/Province</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="address-state" value="" size="4" tabindex="9"  placeholder="">
						</div>
            <div class="payment-input-flex">
							<div class="payment-label">Zip/Postal Code</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="address-zip" value="" size="4" tabindex="10"  placeholder="">
						</div>
            <div class="payment-input-flex">
							<div class="payment-label">Country</div>
							<div class="payment-seperator"></div>
							<input class="payment-input" type="text" data-stripe="address-country" value="" size="4" tabindex="11"  placeholder="">
						</div>



						<div style="width:100%;text-align:right">
							<input class="payment-submit-btn" type="submit"  value="PROCESS PAYMENT">
						</div>
					</form>
				</div>

  </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>

<script type="text/javascript">
  var chargeID = "<?php echo $chargeID;?>";

	// Stripe.setPublishableKey('pk_test_1Xk12UcY2hyb1y3cLVTkk7Rs');
	 Stripe.setPublishableKey('pk_live_QRFV0CvJO6Iq122LsxWt93nv');

	 $(document).ready(function(){
		  insertAlertBox();
      getChargeInfo();
	 });

   $( "form#payment_Form" ).submit(function( event ) {
 		var $form = $(this);
 		var _fields = $( this ).serializeArray();
 		var _query = {};

 		$form.find('button').prop('disabled', true);
    //alert("HERE");
    //processPayment(chargeID, "");
 		Stripe.card.createToken($form, function(status, response) {
 			if(response.error) {
 				showAlert("error","Error Processing Card", response.error.message,"Ok");
 			} else {
 				var token = response.id;
 				processPayment(chargeID, token);
 			}
 		});
 		event.preventDefault();
 	});

   function getChargeInfo() {
     loading(true);
     var _query = {};
     _query["fn"] = "getChargeInfo";
     _query["chargeID"] = chargeID;

     var json_data = _query;
 		console.log("Outbound JSON Query: "+JSON.stringify(json_data));
    $.ajax({
			url: "https://ws.spac.io/prod/oh/webservice_payments/1.0.0/webservice_payments.php",
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

          console.log(data.charge);
          $(".bill-to").html(data.charge["billToName"]+"<br/>"+data.charge["billToAddress"]);
          $(".charge-type .left-column").html(data.charge["desc"]);
          $(".charge-type .right-column").html("$"+data.charge["amount"]+".00");
          loading(false);
				} else {
					showAlert("error","Charge Not Found","We could not find the charge matchings this ID. Please contact support at support@spac.io.");
					loading(false);
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				showAlert("error","Unknown Error","Please contact support at support@spac.io.");

				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Paynment Info Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

   }

   function processPayment2(c,t) {
     loading(true);
     var email = $("input[name='email']").val();
     //alert(email);
 		var _query = {};
 		_query["fn"] = "processPayment";
 		_query["chargeID"] = c;
 		_query["stripeToken"] = t;
     _query["email"] = email;

 		var json_data = _query;
     //console.log("Outbound JSON Query: "+json_data);
     console.log("Outbound JSON Query: "+JSON.stringify(json_data));
   }

	function processPayment(cID, tk){

    //alert(tk);
		loading(true);
		var _query = {};
    var email = $("input[name='email']").val();

		_query["fn"] = "processPayment";
		_query["chargeID"] = cID;
		_query["stripeToken"] = tk;
    _query["email"] = email;

		var json_data = _query;
    //console.log("Outbound JSON Query: "+json_data);
    console.log("Outbound JSON Query: "+JSON.stringify(json_data));

		$.ajax({
      url: "https://ws.spac.io/prod/oh/webservice_payments/1.0.0/webservice_payments.php",
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				loading(false);
				if (data.status == 1) {
					$('#payment_Form')[0].reset();
					showAlert("info","Thank you for your payment","Your payment has been succesfully processed.");
				} else if (data.status == 2) {
					$('#payment_Form')[0].reset();
					showAlert("info","Already Paid For","This item has already been paid for.");
				} else {
					if (data.error_type == "invalid_request_error" && data.error_param == "coupon") {
						showAlert("error","Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
					} else if (data.error_type =="card_error" && data.error_code == "card_declined"){
						showAlert("error","Card Declined","Sorry, your card was declined. Please verify that you entered a valid credit card.");
					} else {
						showAlert("error","Unknowns","There was an unknown error. Please contact support@spac.io.");
					}
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});


	}



	function randomString(_length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

		var randomstring = '';
		for (var i=0; i<_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}

		return randomstring;
	}

	function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
	};

	/*
	var agent = navigator.userAgent;
	var isIphone = ((agent.indexOf('iPhone') != -1) || (agent.indexOf('iPod') != -1)) ;
	if (isIphone) {
		$("#iphone-msg").html("Returning you to the app.");
		setTimeout(function(){window.location.href = 'SpacioiOS://'; }, 2500);

	}
	*/

</script>



</body>
</html>
