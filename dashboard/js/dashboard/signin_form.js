
function launchForm(pid) {

    $(".signin-right-panel").off();
    $(".signin-left-panel").off();
    $("#propertyReg_Form input").off();
    console.log("Launching Form");

    curPanel = "form";

    var propDisabled = false;
    if("disabled" in properties[pid]) {
        if(properties[pid]["disabled"] == "YES") {
            propDisabled = true;
        }
    }

    if(propDisabled) {
        showAlert("Property Inactive","Please activate property in Property Details.");

    } else {

        loading(true,"Launching sign-in form...");

        createOHObj(pid);
        var d = new Date();
        properties[pid]["lastEdited"]["sec"] = Math.floor(d.getTime()/1000);

        getBrand(function(){
            if (!$.isEmptyObject(brand)) {
                var strNewString = brand["leftpanel"];

                $.each(brand["imgs"], function(index,value){
      						var re = new RegExp(value, "g");
                  strNewString = strNewString.replace(re,"img/brands/"+value);
      						//strNewString = $('.signin-left-panel').html().replace(re,"img/brands/"+value);
      					});

                if (brand["leftpanel"] != "default" && brand["leftpanel"] != "custom") {
                    $(".signin-left-panel").html(brand["leftpanel"]);
                }

                if (brand["font"] != "default") {
                    $(".signin-button").css("font-family",brand["font"]);
                    $(".signin-title").css("font-family",brand["font"]);
                }

                if (brand["color"] != "default") {
                    $(".signin-button").css("background-color",brand["color"]);
                    $(".signin-title").css("color",brand["color"]);
                    $(".signin-line").css("border-bottom","2px solid "+brand["color"]);
                    $(".signin-mobile .propertyID").css("color",brand["color"]);
                }

                $('.signin-left-panel').html(strNewString);
            } else {
                $(".signin-left-panel").html(default_left_panel);
                $(".signin-button").css("font-family","proxima_nova_light");
                $(".signin-title").css("font-family","proxima_nova_light");
                $(".signin-button").css("background-color","#2b2b2b");
                $(".signin-title").css("color","#2b2b2b");
                $(".signin-mobile .propertyID").css("color","#2b2b2b");
                $(".signin-line").css("border-bottom","2px solid "+"#2b2b2b");
                $(".brand-logo").hide();
                $(".profile-image-container").css("border","2px solid white");
            }

            loading(false);
            updateLastActive();
            var fontSize = "regular";

            $('.signin-left-panel').append("<div class='leads-list-button' onclick='confirmShowRegistrants(curPID)'></div>");

            loadProfile();
            curPage = "form";
            curPID = pid;

            var d = new Date();

            var no_cache_img = properties[pid]["image"];

            if(no_cache_img == "stock") {
                no_cache_img = "img/stock/1.jpg";
            } else {
                var res = properties[pid]["image"].split("?");
                if (res.length > 1) {
                } else {
                    no_cache_img = properties[pid]["image"];
                }
            }

            $(".signin-left-panel").css("background-image","url("+no_cache_img+")");
            $(".signin-mobile").show();

                if ("mobile" in properties[pid]) {
                    if(properties[pid]["mobile"] == "NO") {
                        $(".signin-mobile").hide();
                    }
                }

                if(properties[pid]["emailMandatory"] == "YES" || user["docusignEnabled"] == "YES" || user["integrations"]["docsEnabled"] == "YES") {
                    $(".email-mandatory").show();
                } else {
                    $(".email-mandatory").hide();
                }

                if(properties[pid]["phoneMandatory"] == "YES") {
                    $(".phone-mandatory").show();
                } else {
                    $(".phone-mandatory").hide();
                }

                $("#propertyReg_Form input[name='pid']").val(pid);
                $("#propertyReg_Form input[name='propertyID']").val(properties[pid]["propertyID"]);
                $("#propertyRegSpacio_Form input[name='propertyID']").val(properties[pid]["propertyID"]);
                $("#propertyTitle").html(properties[pid]["title"]);
                $("#propertyAddress").html(properties[pid]["addr1"]);

                $("#propertyID").html(" Spacio ID: "+properties[pid]["propertyID"]+"");
                $(".propertyID").html(properties[pid]["propertyID"]);

                if (user["brokerage"] == "" || user["brokerage"] == "N/A") {
                    $(".profile-host").html("");
                } else {
                    $(".profile-host").html("of "+user["brokerage"]);
                }
                $(".profile-title").html(user["title"]);


                $(".signin-overlay").fadeIn();

                $(".form-questions").html("");

                var showBrokersSignin = false;
                if ("brokersEnabled" in properties[pid]) {
                    if(properties[pid]["brokersEnabled"] == "YES") {
                        showBrokersSignin = true;
                    } else {
                        showBrokersSignin = false;
                    }
                } else {
                    showBrokersSignin = false;
                }

                if(showBrokersSignin) {
                    $(".brokers-signin").show();
                    $("#propertyReg_Form input[name='isBroker']").val("YES");

                    $(".form-questions").append("<div class='form-question-row'><div class='question'>Do you have a client in mind for this listing?<span class='cn'></span></div><div class='half-width-checkboxes'><div class='checkbox'><input id='question-0-yes' type='radio' value='YES' name='question-0'><label for='question-0-yes'>Yes <span class='cn'>/ 是</span></label></div><div class='checkbox'><input id='question-0-no' type='radio' value='NO' name='question-0'><label for='question-0-no'>No <span class='cn'> / 否</span></label></div></div></div>");

                    $(".form-questions").append("<div class='form-question-row'><div class='question'>How well priced is this property?<span class='cn'></span></div><div class='full-width-checkboxes' id='answers-section-1'><div class='checkbox'><input type='radio' value='Overpriced' name='question-1' id='question-1-1'><label for='question-1-1'>Overpriced</label></div><div class='checkbox'><input type='radio' value='Just Right' name='question-1' id='question-1-2'><label for='question-1-2'>Just Right</label></div><div class='checkbox'><input type='radio' value='Underpriced' name='question-1' id='question-1-3'><label for='question-1-3'>Underpriced</label></div></div></div>");

                    $(".form-questions").append("<div class='form-question-row'><div class='question' style='margin-bottom:0px;'>Please provide some feedback on this property:</div><textarea name='question-2' placeholder='Add comments'></textarea></div>");

                } else {
                    $(".brokers-signin").hide();
                    $("#propertyReg_Form input[name='isBroker']").val("NO");
                    $.each(properties[pid]["questionsObj"], function(index,value) {

                        if (value["type"] == "text") {
                            $(".form-questions").append("<div class='form-question-row'><div class='question'>"+value["question"]+"<span class='cn' id='cn-"+index+"'> / "+value["questionCN"]+"</span></div><input name='question-"+index+"' type='text' value='' placeholder='Answer'></div>");
                        }

                        if (value["type"] == "yn") {
                            $(".form-questions").append("<div class='form-question-row'><div class='question'>"+value["question"]+"<span class='cn' id='cn-"+index+"'> / "+value["questionCN"]+"</span></div><div class='half-width-checkboxes'><div class='checkbox'><input id='question-"+index+"-yes' type='radio' value='YES' name='question-"+index+"'><label for='question-"+index+"-yes'>Yes <span class='cn'>/ 是</span></label></div><div class='checkbox'><input id='question-"+index+"-no' type='radio' value='NO' name='question-"+index+"'><label for='question-"+index+"-no'>No <span class='cn'> / 否</span></label></div></div></div>");
                        }

                        if (value["type"] == "agent") {
                            $(".form-questions").append("<div class='form-question-row eq1-master'><div class='question'>"+value["question"]+"<span class='cn' id='cn-"+index+"'> / "+value["questionCN"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='question-"+index+"-yes' type='radio' value='YES' name='question-"+index+"'><label for='question-"+index+"-yes' onclick='showEQ(1)'>Yes <span class='cn'> / 是</span></label></div><div class='checkbox'><input id='question-"+index+"-no' type='radio' value='NO' name='question-"+index+"'><label for='question-"+index+"-no' onclick='hideEQ(1)'>No <span class='cn'>/ 否</span></label></div></div></div>");

                            $(".form-questions").append("<div class='form-question-row eq1-slave'><div class='question'>Agent Name?<span class='cn'><br/>地产经纪名字?</span></div><input name='question-"+index+"a' type='text' value='' placeholder='Agent Name' autocapitalize=word style='text-transform: capitalize;'></div>");

                            $(".form-questions").append("<div class='form-question-row eq1-slave'><div class='question'>Agent Contact?<span class='cn'><br/>地产经纪电子邮件?</span></div><input name='question-"+index+"b' type='text' value='' placeholder='Agent Email or Phone'></div>");

                            $(".form-questions").append("<div class='form-question-row eq1-slave'><div class='question'>Agent Company?<span class='cn'><br/>地产经纪公司?</span></div><input name='question-"+index+"c' type='text' value='' placeholder='Agent Company' style='text-transform: capitalize;'></div>");

                            hideEQ(1); //Hide the initial agent questions.

                        }


                        if (value["type"] == "mc") {
                            $(".form-questions").append("<div class='form-question-row'><div class='question'>"+value["question"]+"<span class='cn' id='cn-"+index+"'> / "+value["questionCN"]+"</span></div><div class='full-width-checkboxes' id='answers-section-"+index+"'></div></div>");


                            $.each(value["choices"], function(i,v){
                                v["answer"] = v["answer"].replace(/\'/g, "&#039;");

                                if(v["type"] == "radio-text") {
                                  $("#answers-section-"+index).append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='question-"+index+"' id='question-"+index+"-"+i+"' onclick='mcAnswerChanged(&#39;"+index+"&#39;)'><label for='question-"+index+"-"+i+"'>"+v["answer"]+" <span class='cn'>/ "+v["answerCN"]+"</span></label></div>");

                                  $("#answers-section-"+index).append("<input type='text' value='' name='question-"+index+"-other' id='question-"+index+"-"+i+"-other' placeholder='Please specify.'>");
                                } else {
                                  $("#answers-section-"+index).append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='question-"+index+"' id='question-"+index+"-"+i+"' onclick='mcAnswerChanged(&#39;"+index+"&#39;)'><label for='question-"+index+"-"+i+"'>"+v["answer"]+" <span class='cn'>/ "+v["answerCN"]+"</span></label></div>");
                                }

                            })
                        }


                    })
                };

                if ("fontSize" in properties[pid]) {
                  fontSize = properties[pid]["fontSize"];

                  if (fontSize == "small") {
                    $("#propertyReg_Form").css("font-size","14px");
                  }

                  if (fontSize == "regular") {
                    $("#propertyReg_Form").css("font-size","16px");
                  }

                  if (fontSize == "large") {
                    $("#propertyReg_Form").css("font-size","18px");
                  }
                }

                $(".brokerage-disclaimer").html("");
                if(user["type"] == "brokerage") {
                  $(".brokerage-disclaimer").html(brokerage["disclaimer"]);
                }


                $(".realscout").hide();


                if("cloudcmaMLS" in  properties[pid] && (user["integrations"]["cloudcmaID"] != "" && "cloudcmaID" in user["integrations"])) {
                    if(properties[pid]["cloudcmaMLS"] == "" || properties[pid]["cloudcmaMLS"] == "N/A") {
                        $(".cloudcma").hide();
                    } else {
                        $(".cloudcma").show();
                    }
                } else {
                    $(".cloudcma").hide();
                }

                if("moxipresentURL" in  properties[pid] && user["integrations"]["moxipresentEnabled"] == "YES") {
                    if(properties[pid]["moxipresentURL"] == "" || properties[pid]["moxipresentURL"] == "N/A") {
                        $(".moxipresent").hide();
                    } else {
                        $(".moxipresent").show();
                    }
                } else {
                    $(".moxipresent").hide();
                }

                if("touchcmaURL" in  properties[pid] && user["integrations"]["touchcmaEnabled"] == "YES") {
                    if(properties[pid]["touchcmaURL"] == "" || properties[pid]["touchcmaURL"] == "N/A") {
                        $(".touchcma").hide();
                    } else {
                        $(".touchcma").show();
                    }
                } else {
                    $(".touchcma").hide();
                }

                if("homekeeprEnabled" in  user["integrations"] ) {
                    if(user["integrations"]["homekeeprEnabled"] == "YES") {
                        $(".homekeepr").show();
                    } else {
                        $(".homekeepr").hide();
                    }
                } else {
                    $(".homekeepr").hide();
                }

            if (properties[pid]["cnEnabled"] == "YES") {
                $(".cn").show();
            } else {
                $(".cn").hide();
            }

            if("zillowreviewID" in user["integrations"]) {
                if(user["integrations"]["zillowreviewID"] == "N/A" || user["integrations"]["zillowreviewID"] == "" ) {
                    $(".zillowreview-button").hide();
                } else {
                    $(".zillowreview-button").show();
                    $(".zillowreview-testimonials-scroll").html("");

                    $.each(zillowreviewFeed, function(index, value){
                        $(".zillowreview-testimonials-scroll").append("<div class='zillowreview-testimonial-desc'>&#147;"+value["description"]+"&#148;</div>");
                        $(".zillowreview-testimonials-scroll").append("<div class='zillowreview-testimonial-title'>- "+value["reviewer"]+"</div>");
                    });
                }
             } else {
                 $(".zillowreview-button").hide();
             }

            if("ttID" in user["integrations"]) {
                if(user["integrations"]["ttID"] == "N/A" || user["integrations"]["ttID"] == "" ) {
                    $(".tt-button").hide();
                } else {

                    $(".tt-button").show();
                    $(".tt-testimonials-scroll").html("");

                    $.each(ttFeed, function(index, value){
                        $(".tt-testimonials-scroll").append("<div class='tt-testimonial-desc'>&#147;"+value["clientTestimonial"]+"&#148;</div>");
                        $(".tt-testimonials-scroll").append("<div class='tt-testimonial-title'>- "+value["clientSignature"]+"</div>");

                    });
                }
             } else {
                 $(".tt-button").hide();
             }

             if("idcTestimonialID" in user["integrations"]) {
                 if(user["integrations"]["idcTestimonialID"] == "N/A" || user["integrations"]["idcTestimonialID"] == "" ) {
                     $(".idc-button").hide();
                 } else {
                     $(".idc-button").show();
                     $(".idc-testimonials-scroll").html("");

                     $.each(idcFeed, function(index, value){
                         $(".idc-testimonials-scroll").append("<div class='idc-testimonial-desc'>"+value["testimonial"]+"</div>");
                         $(".idc-testimonials-scroll").append("<div class='idc-testimonial-title'>- "+value["from"]+"</div>");

                     });
                 }
              } else {
                  $(".idc-button").hide();
              }

              if("rsID" in user["integrations"]) {
                 if (user["integrations"]["rsID"] == "N/A" || user["integrations"]["rsID"] == "") {
                     $(".rs-button").hide();
                 } else {
                     $(".rs-button").show();

                     try {
                         if("overall_satisfaction" in rsFeed["channel"]) {
                             $(".rs-satisfaction").html(rsFeed["channel"]["overall_satisfaction"]+"%");
                         } else {
                             $(".rs-satisfaction").html("--");
                         }

                         if("recommendation_rating" in rsFeed["channel"]) {
                             $(".rs-recommendation").html(rsFeed["channel"]["recommendation_rating"]+"%");
                         } else {
                             $(".rs-recommendation").html("--");
                         }

                         if("performance_rating" in rsFeed["channel"]) {
                             $(".rs-performance").html(rsFeed["channel"]["performance_rating"]+"%");
                         } else {
                                 $(".rs-performance").html("--");
                         }

                         $(".rs-testimonials-scroll").html("");


                         if ($.isArray(rsFeed["channel"]["item"])) {

                             $.each(rsFeed["channel"]["item"], function(index, value){
                                 $(".rs-testimonials-scroll").append("<div class='rs-testimonial-desc'>&#147;"+value["description"]+"&#148;</div>");
                                 $(".rs-testimonials-scroll").append("<div class='rs-testimonial-title'>- "+value["title"]+"</div>");

                             });
                         } else {
                             try {
                                 $(".rs-testimonials-scroll").append("<div class='rs-testimonial-desc'>&#147;"+rsFeed["channel"]["item"]["description"]+"&#148;</div>");
                                 $(".rs-testimonials-scroll").append("<div class='rs-testimonial-title'>- "+rsFeed["channel"]["item"]["title"]+"</div>");
                             } catch(error) {
                               console.error(error);
                               // expected output: SyntaxError: unterminated string literal
                               // Note - error messages will vary depending on browser
                             }

                         }
                     } catch(error) {
                           console.error(error);
                           // expected output: SyntaxError: unterminated string literal
                           // Note - error messages will vary depending on browser
                         }
                 }
             }


            $("#propertyReg_Form")[0].reset();
            $("#reg-form-scroll").scrollTop(0);

        });
    }
}


function pauseOpenHouse() {
    $(".signin-overlay").hide();
    $(".resume-oh-overlay").show();
}


function resumeOpenHouse() {
    $(".signin-overlay").fadeIn();
    $(".resume-oh-overlay").hide();
}

function confirmShowRegistrants(pid) {
    if(user["passcodeProtection"] == "YES") {
        curPasscodeRestriction = "showregistrants";
        showPasscodeWindow();
    } else {
        //pauseOpenHouse()
        //showLeadsPanel(pid);
        showVisitors(curPID);
    }
}

function closeSignInForm() {

    if(user["passcodeProtection"] == "YES") {
        curPasscodeRestriction = "closeform";
        showPasscodeWindow();
    } else {
        showAlert("Close Sign-In Form",
            "Are you sure you want to end this open house?",
            "Yes",
            "Cancel",
            function(){
                endOpenHouse();
            });
    }
}

function endOpenHouse(){
    $(".alert-overlay").hide();
    updateLastActive();
    curPage = "index";
    $(".signin-overlay").hide();

    //var d = new Date();
    //curOHObj["endTime"] = d.getTime();
    addCurOHObj();

    //filterProperties();
}


function showPasscodeWindow() {
    $(".passcode-panel").show();
    $("#passcode").val("");
    $("#passcode-error").html("");
}

function hidePasscodeWindow() {
    $(".passcode-panel").hide();
}

function verifyPasscode() {
    var temp_passcode = $("#passcode").val();

    if(temp_passcode == user["pin"]) {
        hidePasscodeWindow();

        if(curPasscodeRestriction == "showregistrants") {
            showVisitors(curPID);
        }
         if(curPasscodeRestriction == "closeform") {
            endOpenHouse();
        }

    } else {
        $("#passcode-error").html("WRONG PASSCODE");
    }
}

function mcAnswerChanged(index) {
    var temp_val = $("input[name='question-"+index+"']:checked").val();

    if(temp_val == "Other" || temp_val == "Others" ) {
      $("input[name='question-"+index+"-other']").show();
      $("input[name='question-"+index+"-other']").focus();

    } else {
      $("input[name='question-"+index+"-other']").hide();
    }

}
