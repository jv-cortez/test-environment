<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

  	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  	header("Cache-Control: post-check=0, pre-check=0", false);
  	header("Pragma: no-cache");

  	$_email = $_GET["e"];
  	$_id = $_GET["u"];
  	$_from = isset($_GET['f']) ? $_GET['f'] : "";
    $_brokerageID = isset($_GET['b']) ? $_GET['b'] : "";
  	$_nb = isset($_GET['nb']) ? $_GET['nb'] : 0;
?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Claim Account</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
     <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="signup-scroll-one" style="background:none;">
      <div class="overlay-flex">
        <form id="claimAccount_Form" action="index.php" method="POST" style="height:auto">
          <input type="hidden" name="fn" value="claimAccount" />
          <div class="xlarge" style="text-align:center;margin-bottom:20px;">
              Claim Your Spacio Account
          </div>

          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              EMAIL
            </div>
            <input class="signup-input" style="width:calc(100% - 175px);" type="text" name="email" value="<?php echo $_email ?>" id="email" tabindex="1" placeholder="" readonly="true">
          </div>

          <div class="signup-input-box">
            <div class="signup-label" style="flex-basis:130px;">
              SET PASSWORD
            </div>
            <input class="signup-input" style="width:calc(100% - 175px);" type="text" name="pw" value="" id="pw" tabindex="2" placeholder="" >
          </div>


          <div><input type="hidden" name="id" value="<?php echo $_id ?>" id="id" placeholder=""></div>
          <div><input type="hidden" name="from" value="<?php echo $_from ?>" id="id" placeholder=""></div>

          <div style="width:100%;text-align:right">
            <input class="signup-submit-btn-orange" type="submit" name="signup" value="CLAIM ACCOUNT">
          </div>
        </form>
      </div>

    </div>
    <!-- SCROLL ONE END -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
    <script>
    var email = "<?php echo $_email ?>";
	  var nb = <?php echo $_nb ?>;
    var brokerageID = "<?php echo $_brokerageID ?>";
	  var rsFeed = {};
	  var user = {};
    var wsURL = "../oh/webservice/1.1.6/webservice.php";

    $(document).ready(function(){
      email = email.replace(/\s+/g, '+').toLowerCase();
      insertAlertBox();

      getUnclaimedBrokerage(email, "<?php echo $_id ?>" );

    });

    function confirmClaimAccount() {


    }

    function getUnclaimedBrokerage(em, id) {
      loading(true);
      var _query = {};
      _query["email"] = em;
      _query["id"] = id;
      _query["fn"] = "getUnclaimedBrokerage";

      var json_data = _query;
      console.log("Outbound JSON Query: "+JSON.stringify(json_data));
      $.ajax({
        url: "../oh/webservice/1.1.6/webservice.php",
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){
          console.log("Returned JSON Object: "+JSON.stringify(data));

          if (data.status == 1) {
            brokerageID = data.brokerageID;

            if(brokerageID == "lnf") {
              
              window.location.href = 'https://spac.io/claim/lnf/?e='+email+'&u=<?php echo $_id ?>';

            }
          } else {
            brokerageID = "";
          }
          loading(false);
          //debugLog("Query Status: "+data.status);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Account Error: "+errorThrown);
        },
        dataType: "json"
      });

    }

    function claimAccount(_fields) {
      var _query = {};

  		//Format form inputs into proper array
  		$.each(_fields, function(index, value) {
  			_query[value["name"]] = value["value"];
  		})

  		_query["email"] = _query["email"].toLowerCase();
  		//Validate form inputs.
  		if (_query["email"] == "" || _query["email"] == null) {
  			showAlert("error","Invalid Email Format","Please enter a valid email.");
  		} else if(_query["pw"] == "") {
  			showAlert("error","No Password","Please enter a password.");
  		} else if(_query["pw"].length < 6) {
  			showAlert("error","Invalid Password","Passwords must be at least 6 characters.");
  		} else {
  			if(nb == 1) {
  				_query["fn"] = "claimAccountNB";
  			}

  			//Clean up query object and send

  			loading(true,"Claiming Your Account...");
  			//Salt + Hash password before sending to server
  			var _salt = randomString(16);
  			_query["salt"] = _salt;
  			_query["pw"] += _salt;
  			_query["pw"] = CryptoJS.SHA3(_query["pw"], { outputLength: 224 }).toString();

  			var json_data = _query;
  			console.log("Outbound JSON Query: "+JSON.stringify(json_data));
  			$.ajax({
  				url: "../oh/webservice/1.1.6/webservice.php",
  				type:"POST",
  				data: {json : json_data},
  				success: function(data, textStatus, jqXHR){
  					console.log("Returned JSON Object: "+JSON.stringify(data));

  					if (data.status == 1) {
  						showAlert("info","Thank You for Signing Up","Please login.","OK",null,function(){window.location.href="../dashboard/"},null);
  					} else if (data.status == 2) {
  						showAlert("error","Email Already Registered","This email has already been registered with Spacio. Please login.","Login","Cancel", function(){
  						window.location = "../dashboard/";
  						}, null);
  					} else {
  						if(nb == 1) {
  							showAlert("error","Invalid Link","Sorry, it seems your claim link is invalid. Please contact hello@spac.io");
  						} else {
  							showAlert("error","Invalid Link","Sorry, it seems your claim link is invalid. Please contact your account administrator.");
  						}
  					}
  					loading(false);
  					//debugLog("Query Status: "+data.status);
  				},
  				error: function(jqXHR, textStatus, errorThrown) {
  					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Claim Account Error: "+errorThrown);
  				},
  				dataType: "json"
  			});
  		}
    }

    $( "form#claimAccount_Form" ).submit(function( event ) {

  		//Serialize form inputs
  		var _fields = $( this ).serializeArray();
      if(brokerageID == "lnf") {
        showAlert("info","Acknowledgement of Account Creation","In signing up for Spacio, you authorize us to charge $17.50 to your Long & Foster agent account on a recurring monthly basis until you cancel your Spacio subscription. You are fully responsible for all charges made to your agent account. Subscription to Spacio is month-to-month, and you may cancel anytime. If you do not want to continue, please select Cancel. ","Ok","Cancel",function(){
          claimAccount(_fields);
        });
      } else {
        claimAccount(_fields);
      }
  		event.preventDefault();
  	});

    </script>
  </body>
</html>
