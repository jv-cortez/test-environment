<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

  	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  	header("Cache-Control: post-check=0, pre-check=0", false);
  	header("Pragma: no-cache");

  	$_email = $_GET["e"];
    $_uid = $_GET["u"];
    $_type = isset( $_GET['t'] ) ? $_GET['t'] : 'claimed';

?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Unsubscribe</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>

    <style>
      @media (max-width: 800px) {
        .unsubscribe_form {
          width:90%;
        }
      }

      @media (min-width: 801px) {
        .unsubscribe_form {
          width:420px;
        }
      }
    </style>

    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
     <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="signup-scroll-one" style="background:#999;">
      <div class="overlay-flex" style="min-height:calc(100vh - 120px);">
        <form id="unsubscribe_Form" action="index.php" method="POST" style="height:auto;margin:0 auto;">
          <input type="hidden" name="fn" value="unsubscribeEmail" />
          <input type="hidden" name="uid" value="<?php echo $_uid ?>" />
          <div class="xlarge" style="text-align:center;margin-bottom:20px;">
              Unsubscribe
          </div>
          <div class="normal" style="text-align:left;margin-bottom:20px;line-height:1.4;">
            <div style="margin-bottom:15px;font-size:20px;">
            Please select the list you wish to unsubscribe <span style='color:#e77f24'><?php echo $_email ?></span> from:</div>
            <div style="margin-left:20px;">
              <input type="checkbox" value="true" id="unsub-onboard"> <span>Onboarding Emails</span><Br/>
              <input type="checkbox" value="true" id="unsub-campaign"> <span>Campaign Emails</span><Br/>
              <spac class="unsub-claim">
                <input type="checkbox" value="true" id="unsub-claim"> <span>Claim Emails</span>
              </span><Br/>
            </div>

          </div>


          <div style="width:100%;text-align:left">
            <input class="signup-submit-btn" type="submit" name="signup" value="UNSUBSCRIBE">
          </div>
        </form>
      </div>

    </div>
    <!-- SCROLL ONE END -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
    <script>
    var email = "<?php echo $_email ?>";
    var uid = "<?php echo $_uid ?>";
    var type = "<?php echo $_type ?>"
    var wsURL ="https://ws.spac.io/prod/oh/webservice/1.1.6/webservice.php";

    $(document).ready(function(){
      email = email.replace(/\s+/g, '+').toLowerCase();
      insertAlertBox();


      if(type == "unclaimed") {
        $(".unsub-claim").show();
      } else {
        $(".unsub-claim").hide();
      }
      getUnsubObj();
    });


    function getUnsubObj() {
      var _query = {};

  		//Format form inputs into proper array
      _query["fn"] = "getUnsubObj";
  		_query["email"] = email;
      _query["managerID"] = uid;
      _query["type"] = type;

      loading(true,"Loading...");

      var json_data = _query;
			console.log("Outbound JSON Query: "+JSON.stringify(json_data));

      $.ajax({
				url: wsURL_new,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {
                $("#unsub-onboard").prop('checked', data.unsubObj["onboard"]);
                $("#unsub-system").prop('checked', data.unsubObj["system"]);
                $("#unsub-claim").prop('checked', data.unsubObj["claim"]);
                $("#unsub-campaign").prop('checked', data.unsubObj["campaign"]);
						//showAlert("info","Unsubscribed","You have unsubscribed to the selected mailing list.","OK");
					} else {
						//showAlert("error","Invalid Link","Sorry, your unsubscribe link is invalid. Please contact support at support@spac.io.");
					}
					loading(false);
					//debugLog("Query Status: "+data.status);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Unsubscribe Error: "+errorThrown);
				},
				dataType: "json"
			});

    }

    $( "form#unsubscribe_Form" ).submit(function( event ) {

		//Serialize form inputs
		var _fields = $( this ).serializeArray();
		var _query = {};

		//Format form inputs into proper array
    _query["fn"] = "unsubscribeEmail2";
		_query["email"] = email;
    _query["uid"] = uid;
    _query["type"] = type;

    _query["system"] = "false";
    _query["onboard"] = "false";
    _query["campaign"] = "false";
    _query["claim"] = "false";

    if($("#unsub-onboard").is(':checked')) {
      _query["onboard"] = "true";
    }
    if($("#unsub-campaign").is(':checked')) {
      _query["campaign"] = "true";
    }
    if($("#unsub-claim").is(':checked')) {
      _query["claim"] = "true";
    }


		_query["email"] = _query["email"].toLowerCase();
		//Validate form inputs.
		if (_query["email"] == "" || _query["email"] == null) {
			showAlert("error","Invalid Email Format","Please enter a valid email.");
		} else {

			//Clean up query object and send
			loading(true,"Loading...");
			//Salt + Hash password before sending to server

			var json_data = _query;
			console.log("Outbound JSON Query: "+JSON.stringify(json_data));
			$.ajax({
				url: wsURL_new,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {
						showAlert("info","Unsubscribed","You have unsubscribed to the selected mailing list.","OK");
					} else {
						showAlert("error","Invalid Link","Sorry, your unsubscribe link is invalid. Please contact support at support@spac.io.");
					}
					loading(false);
					//debugLog("Query Status: "+data.status);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Unsubscribe Error: "+errorThrown);
				},
				dataType: "json"
			});
		}
		event.preventDefault();
	});

    </script>
  </body>
</html>
