<?php

require_once('admin_queries.php');
require_once('brokerage_queries.php');
require_once('form_queries.php');
require_once('manager_queries.php');
require_once('mls_queries.php');
require_once('property_queries.php');
require_once('registrant_queries.php');
require_once('shared_queries.php');

// VARIABLES
// ADMIN QUERIES
$fname = 'Test';
$lname = 'Ing';
$email = 'maintenance@spac.io';
$phone = '111-222-3333';
$website = 'testrealty.com';
$brokerage = 'Spacio Realty';
// Following for ting@spac.io
$pw = '8c15306b8042a6c1ea40f12ba69943eb279e41bd9743f8d8bca2410f';
$salt = 'Ff9PTtoAFCnnDyp8';


// BROKERAGE QUERIES
$brokerageID = 'spacio';
$id = '574de32eaace0fff7d959302'; // One of the unclaimed users in stage db (bostonlogic)
$from = '574de033d4c618c0cca3a44b'; // bostonlogic
$managerID = '574de033d4c618c0cca3a44b';
$vtoken = 'EN5H7sl08uIv2hyF';
$officeID = 'mdrn';
$body = '';
$signature = '';
$claimEmail = '';
$welcomeEmail = '';
$email2 = 'vseow@spac.io';
$title = 'Pro Tester';
$pphoto = 'http://spac.io/projects/dashboard/img/projects/style_logo2.png';
$mobile = '222-333-4444';
$website = 'http://spac.io';
$office = 'Spacio Testing';
$addr1 = '123 Fake Street';
$addr2 = 'Unit 123';
$code = '';
$promo = 'ohspacio';
$stripetoken = '';
$ukey = '';
$_managerID = '';
$_vtoken = '';
$_old_pass = '';
$_pass = '';
$rtoken = '';


// FORM QUERIES
$propertyID = '10252';


// MANAGER QUERIES
$signature = '';
$confirmed = '';
$referred = '';
$rsID = '';
$promocode = '';
$_email = '';
$_promo = '';
$_stripetoken = '';
$olrEnabled = '';
$renthopID = '';
$mls = '';
$mlsID = '';
$mailchimp_key = '';
$mailchimp_username = '';
$rsAutoClose = '';
$marketsnapID = ''; // May need to change managerID and vtoken for each of these
$boomtownID = '';
$topproducerID = '';
$realtyjugglerID = '';
$followupbossID = '';
$wiseagentID = '';
$contactuallyID = '';
$contactuallyBucket = '';
$status = '';


// MLS QUERIES
$listingID = '';


// PROPERTY QUERIES
$url = 'http://gramercy.house/events';
$image = 'http://spac.io/projects/dashboard/img/projects/gramercy_hero.jpg';
$price = '1234567';
$beds = '5';
$baths = '5';
$dimens = '12345';
$desc = 'This a test, brah';
$currency = 'USD';
$measurement = 'sqft';
$questions = array(
      							"eq1" => 'Test?',
      							"text" => 'Yes sir'
      						);
$cnEnabled = 'YES';
$anon = 'NO';
$autoEmail = 'YES';
$mlsnum = 'TR16717733';
$tlc = '';
$hosted = '';
$pid = '';
$dateHosted = '';
$dayHosted = '';
$offset = '';
$brokersEnabled = 'YES';
$emailMandatory = 'YES';
$phoneMandatory = 'YES';
$questions = '';
$disabled = '';
$rid = '';
$properties = '';


// REGISTRANT QUERIES
$score_calibration = '';
$registrants = '';
$property_result = '';
$manager_result = '';
$timestamp = '';
$propertyID = '';
$honorifics = '';
$answers = '';
$marketsnap = '';
$source = '';
$sendEmail = '';
$isBroker = '';
$note = '';
$registrantID = '';
$honorifics = '';
$name = '';
$phone = '';
$company = '';
$note = '';
$message = '';
$emails = '';
$offset = '';
$type = '';


// SHARED QUERIES
$agent = '';


// Results in comments after echos after initial runs

echo "Running ADMIN QUERIES Tests...\n";
$adm_getAllRegistrantsIDs = Adm_Query::getAllRegistrantsIDs();
$adm_calculateHosted = Adm_Query::calculateHosted();
$adm_moveDeletedRegistrants = Adm_Query::moveDeletedRegistrants();
$adm_createKeys = Adm_Query::createKeys();
$adm_createDefaultTemplate = Adm_Query::createDefaultTemplate();
$adm_registerManager = Adm_Query::registerManager($fname, $lname, $email, $phone, $website, $brokerage, $pw, $salt);
echo "adm_getAllRegistrantsIDs: {$adm_getAllRegistrantsIDs['status']}\n";
echo "adm_calculateHosted: {$adm_calculateHosted['status']}\n";
echo "adm_moveDeletedRegistrants: {$adm_moveDeletedRegistrants['status']}\n";
echo "adm_createKeys: {$adm_createKeys['status']}\n";
echo "adm_createDefaultTemplate: {$adm_createDefaultTemplate['status']}\n";
echo "adm_registerManager: {$adm_registerManager['status']}\n";


echo "\nRunning BROKERAGE QUERIES Tests...\n";
$brok_registerBrokerageManager = Brok_Query::registerBrokerageManager($fname, $lname, $email, $brokerageID, $pw, $salt);
$brok_claimAccount = Brok_Query::claimAccount($email, $id, $pw, $salt, $from);
$brok_getClaimLink = Brok_Query::getClaimLink($email);
$brok_sendClaimEmail = Brok_Query::sendClaimEmail($managerID, $vtoken, $email);
$brok_sendClaimEmails = Brok_Query::sendClaimEmails($managerID, $vtoken, $officeID);
$brok_getBrokerageEmailTemplate = Brok_Query::getBrokerageEmailTemplate($managerID, $vtoken);
$brok_saveBrokerageEmailTemplate = Brok_Query::saveBrokerageEmailTemplate($managerID, $vtoken, $body, $signature, $claimEmail, $welcomeEmail);
$brok_createDefaultTemplate = Brok_Query::createDefaultTemplate();
$brok_getBrokerageRegistrants = Brok_Query::getBrokerageRegistrants($managerID, $vtoken, $brokerageID);
$brok_getBrokerageProperties = Brok_Query::getBrokerageProperties($managerID, $vtoken, $brokerageID);
$brok_getBrokerageOpenHouses = Brok_Query::getBrokerageOpenHouses($managerID, $vtoken, $brokerageID);
$brok_deleteUser = Brok_Query::deleteUser($managerID, $vtoken,  $brokerageID, $email);
$brok_addUser = Brok_Query::addUser($managerID, $vtoken, $brokerageID, $officeID, $fname, $lname, $email, $email2, $title, $pphoto,$phone,$mobile, $website);
$brok_resolveUser = Brok_Query::resolveUser($managerID, $vtoken, $brokerageID, $officeID, $fname, $lname, $email);
$brok_addOffice = Brok_Query::addOffice($managerID, $vtoken, $brokerageID, $office, $addr1, $addr2);
$brok_getBrokerageInfo = Brok_Query::getBrokerageInfo($managerID, $vtoken);
$brok_getOffices = Brok_Query::getOffices($managerID, $vtoken, $brokerageID);
$brok_getClaimedAccounts = Brok_Query::getClaimedAccounts($managerID, $vtoken, $brokerageID);
$brok_getUnclaimedAccounts = Brok_Query::getUnclaimedAccounts($managerID, $vtoken, $brokerageID);
$brok_getClaimEmailsLog = Brok_Query::getClaimEmailsLog($managerID, $vtoken, $email);
$brok_getBrand = Brok_Query::getBrand($managerID, $vtoken);
$brok_applyCorpCode = Brok_Query::applyCorpCode($managerID, $vtoken, $code);
$brok_applyPromoCode = Brok_Query::applyPromoCode($managerID, $vtoken, $promo);
$brok_getBrokerageSaltByEmail = Brok_Query::getBrokerageSaltByEmail($email);
$brok_getManagerToken = Brok_Query::getManagerToken($vtoken, $email);
$brok_saveBrokerageCreditCard = Brok_Query::saveBrokerageCreditCard($managerID, $vtoken, $stripetoken);
$brok_getManager = Brok_Query::getManager($managerID, $vtoken);
$brok_getProfile = Brok_Query::getProfile($ukey);
$brok_updateLastActive = Brok_Query::updateLastActive($managerID, $vtoken);
$brok_loginBrokerageManager = Brok_Query::loginBrokerageManager($email, $pw);
$brok_changeBrokeragePassword = Brok_Query::changeBrokeragePassword($_managerID,$_vtoken,$_old_pass,$_pass);
echo "brok_registerBrokerageManager: {$brok_registerBrokerageManager['status']}\n";
// echo "brok_claimAccount: {$brok_claimAccount['status']}\n";
echo "brok_getClaimLink: {$brok_getClaimLink['status']}\n";
echo "brok_sendClaimEmail: {$brok_sendClaimEmail['status']}\n";
echo "brok_sendClaimEmails: {$brok_sendClaimEmails['status']}\n";
echo "brok_getBrokerageEmailTemplate: {$brok_getBrokerageEmailTemplate['status']}\n";
echo "brok_saveBrokerageEmailTemplate: {$brok_saveBrokerageEmailTemplate['status']}\n";
echo "brok_createDefaultTemplate: {$brok_createDefaultTemplate['status']}\n";
echo "brok_getBrokerageRegistrants: {$brok_getBrokerageRegistrants['status']}\n";
echo "brok_getBrokerageProperties: {$brok_getBrokerageProperties['status']}\n";
echo "brok_getBrokerageOpenHouses: {$brok_getBrokerageOpenHouses['status']}\n";
echo "brok_getClaimEmailsLog: {$brok_getClaimEmailsLog['status']}\n";
echo "brok_addUser: {$brok_addUser['status']}\n";
echo "brok_resolveUser: {$brok_resolveUser['status']}\n";
echo "brok_addOffice: {$brok_addOffice['status']}\n";
echo "brok_getBrokerageInfo: {$brok_getBrokerageInfo['status']}\n";
echo "brok_getOffices: {$brok_getOffices['status']}\n";
echo "brok_getClaimedAccounts: {$brok_getClaimedAccounts['status']}\n";
echo "brok_getUnclaimedAccounts: {$brok_getUnclaimedAccounts['status']}\n";
echo "brok_getClaimEmailsLog: {$brok_getClaimEmailsLog['status']}\n";
echo "brok_getBrand: {$brok_getBrand['status']}\n";
echo "brok_applyCorpCode: {$brok_applyCorpCode['status']}\n";
echo "brok_applyPromoCode: {$brok_applyPromoCode['status']}\n";
echo "brok_getBrokerageSaltByEmail: {$brok_getBrokerageSaltByEmail['status']}\n";
echo "brok_getManagerToken: {$brok_getManagerToken['status']}\n";
echo "brok_saveBrokerageCreditCard: {$brok_saveBrokerageCreditCard['status']}\n";
echo "brok_getManager: {$brok_getManager['status']}\n";
echo "brok_getProfile: {$brok_getProfile['status']}\n";
echo "brok_updateLastActive: {$brok_updateLastActive['status']}\n";
echo "brok_loginBrokerageManager: {$brok_loginBrokerageManager['status']}\n";
echo "brok_changeBrokeragePassword: {$brok_changeBrokeragePassword['status']}\n";


echo "\nRunning FORM QUERIES Tests...\n";
$form_deleteForm = Form_Query::deleteForm($propertyID);
echo "form_deleteForm: {$form_deleteForm['status']}\n";


echo "\nRunning MANAGER QUERIES Tests...\n";
$man_claimAccountNB = Man_Query::claimAccountNB($email,$id, $pw, $salt, $from);
$man_getEmailTemplate = Man_Query::getEmailTemplate($managerID, $vtoken);
$man_resetEmailTemplate = Man_Query::resetEmailTemplate($managerID, $vtoken);
$man_saveEmailTemplate = Man_Query::getEmailTemplate($managerID, $vtoken, $body, $signature);
$man_createDefaultTemplate = Man_Query::createDefaultTemplate();
$man_registerManager = Man_Query::registerManager($fname, $lname, $email, $phone, $website, $brokerage, $pw, $salt);
$man_registerManagerTrial = Man_Query::registerManagerTrial($fname, $lname, $email, $phone, $mobile, $website, $pphoto, $brokerage, $pw, $salt, $promo, $confirmed, $referred);
$man_registerManagerRS = Man_Query::registerManagerRS($fname, $lname, $email, $phone, $mobile, $website, $brokerage, $addr1, $pw, $salt, $pphoto, $title, $rsID, $promocode);
$man_upgradeAccount = Man_Query::upgradeAccount($_email, $_promo, $_stripetoken);
$man_upgradeAccountStage = Man_Query::upgradeAccountStage($_email, $_promo, $_stripetoken);
$man_upgradeAccountByID = Man_Query::upgradeAccountByID($managerID, $vtoken, $stripetoken);
$man_downgradeAccount = Man_Query::downgradeAccount($managerID, $vtoken);
$man_getBrand = Man_Query::getBrand($managerID, $vtoken);
$man_applyCorpCode = Man_Query::applyCorpCode($managerID, $vtoken, $code);
$man_applyPromoCode = Man_Query::applyPromoCode($managerID, $vtoken, $promo);
$man_editOLR = Man_Query::editOLR($managerID, $vtoken, $olrEnabled);
$man_applyRenthopID = Man_Query::applyRenthopID($managerID, $vtoken, $renthopID);
$man_applyMLS = Man_Query::applyMLS($managerID, $vtoken, $mls, $mlsID);
$man_applyMailchimpID = Man_Query::applyMailchimpID($managerID, $vtoken, $mailchimp_key, $mailchimp_username);
$man_applyRSID = Man_Query::applyRSID($managerID, $vtoken, $rsID, $rsAutoClose);
$man_applyMSID = Man_Query::applyCorpCode($managerID, $vtoken, $marketsnapID);
$man_applyBTID = Man_Query::applyCorpCode($managerID, $vtoken, $boomtownID);
$man_applyTPID = Man_Query::applyCorpCode($managerID, $vtoken, $topproducerID);
$man_applyRealtyJugglerID = Man_Query::applyRealtyJugglerID($managerID, $vtoken, $realtyjugglerID);
$man_applyFollowUpBossID = Man_Query::applyFollowUpBossID($managerID, $vtoken, $followupbossID);
$man_applyWiseAgentID = Man_Query::applyCorpCode($managerID, $vtoken, $wiseagentID);
$man_applyContactuallyID = Man_Query::applyContactuallyID($managerID, $vtoken, $contactuallyID, $contactuallyBucket);
$man_editTopProducer = Man_Query::editTopProducer($managerID, $vtoken, $topproducerID, $marketsnapID);
$man_checkTrial = Man_Query::checkTrial($managerID, $vtoken);
$man_resendConfirmationEmail = Man_Query::resendConfirmationEmail($managerID, $vtoken);
$man_confirmEmail = Man_Query::confirmEmail($managerID, $vtoken);
$man_forgotPass = Man_Query::forgotPass($email);
$man_resetPass = Man_Query::resetPass($managerID, $rtoken, $pw, $salt);
$man_deleteManager = Man_Query::deleteManager($managerID, $vtoken);
$man_suspendManager = Man_Query::suspendManager($managerID, $status);
$man_getSaltByEmail = Man_Query::getSaltByEmail($email);
$man_getManagerToken = Man_Query::getManagerToken($vtoken, $email);
$man_getManagerPaymentInfo = Man_Query::getManagerPaymentInfo($managerID, $vtoken);
$man_getManager = Man_Query::getManager($managerID, $vtoken);
$man_getProfile = Man_Query::getProfile($ukey);
$man_updateLastActive = Man_Query::updateLastActive($managerID, $vtoken);
$man_loginManager = Man_Query::loginManager($email, $pw);
$man_getMLSList = Man_Query::getMLSList($managerID, $vtoken);
$man_changePassword = Man_Query::changePassword($_managerID, $_vtoken, $_old_pass, $_pass);
$man_logoutManager = Man_Query::logoutManager($managerID, $vtoken);
$man_editProfile = Man_Query::editProfile($managerID, $vtoken, $fname, $lname, $title, $email, $phone, $mobile, $website, $brokerage, $office, $addr1, $addr2, $pphoto);
$man_resetEmail = Man_Query::resetEmail($managerID, $vtoken, $email);
$man_saveCreditCard = Man_Query::saveCreditCard($managerID, $vtoken, $stripetoken);
echo "man_claimAccountNB: {$man_claimAccountNB['status']}\n";
echo "man_getEmailTemplate: {$getEmailTemplate['status']}\n";
echo "man_resetEmailTemplate: {$man_resetEmailTemplate['status']}\n";
echo "man_saveEmailTemplate: {$man_saveEmailTemplate['status']}\n";
echo "man_createDefaultTemplate: {$man_createDefaultTemplate['status']}\n";
echo "man_registerManager: {$man_registerManager['status']}\n";
echo "man_registerManagerTrial: {$man_registerManagerTrial['status']}\n";
echo "man_registerManagerRS: {$man_registerManagerRS['status']}\n";
echo "man_upgradeAccount: {$man_upgradeAccount['status']}\n";
echo "man_upgradeAccountStage: {$man_upgradeAccountStage['status']}\n";
echo "man_upgradeAccountByID: {$man_upgradeAccountByID['status']}\n";
echo "man_downgradeAccount: {$man_downgradeAccount['status']}\n";
echo "man_getBrand: {$man_getBrand['status']}\n";
echo "man_applyCorpCode: {$man_applyCorpCode['status']}\n";
echo "man_applyPromoCode: {$man_applyPromoCode['status']}\n";
echo "man_editOLR: {$man_editOLR['status']}\n";
echo "man_applyRenthopID: {$man_applyRenthopID['status']}\n";
echo "man_applyMLS: {$man_applyMLS['status']}\n";
echo "man_applyMailchimpID: {$man_applyMailchimpID['status']}\n";
echo "man_applyRSID: {$man_applyRSID['status']}\n";
echo "man_applyMSID: {$man_applyMSID['status']}\n";
echo "man_applyBTID: {$man_applyBTID['status']}\n";
echo "man_applyTPID: {$man_applyTPID['status']}\n";
echo "man_applyRealtyJugglerID: {$man_applyRealtyJugglerID['status']}\n";
echo "man_applyFollowUpBossID: {$man_applyFollowUpBossID['status']}\n";
echo "man_applyWiseAgentID: {$man_applyWiseAgentID['status']}\n";
echo "man_applyContactuallyID: {$man_applyContactuallyID['status']}\n";
echo "man_editTopProducer: {$man_editTopProducer['status']}\n";
echo "man_checkTrial: {$man_checkTrial['status']}\n";
echo "man_resendConfirmationEmail: {$man_resendConfirmationEmail['status']}\n";
echo "man_confirmEmail: {$man_confirmEmail['status']}\n";
echo "man_confirmEmail: {$man_confirmEmail['status']}\n";
echo "man_forgotPass: {$man_forgotPass['status']}\n";
echo "man_resetPass: {$man_resetPass['status']}\n";
echo "man_deleteManager: {$man_deleteManager['status']}\n";
echo "man_suspendManager: {$man_suspendManager['status']}\n";
echo "man_getSaltByEmail: {$man_getSaltByEmail['status']}\n";
echo "man_getManagerToken: {$man_getManagerToken['status']}\n";
echo "man_getManagerPaymentInfo: {$man_getManagerPaymentInfo['status']}\n";
echo "man_getManager: {$man_getManager['status']}\n";
echo "man_getProfile: {$man_getProfile['status']}\n";
echo "man_updateLastActive: {$man_updateLastActive['status']}\n";
echo "man_loginManager: {$man_loginManager['status']}\n";
echo "man_getMLSList: {$man_getMLSList['status']}\n";
echo "man_changePassword: {$man_changePassword['status']}\n";
echo "man_logoutManager: {$man_logoutManager['status']}\n";
echo "man_editProfile: {$man_editProfile['status']}\n";
echo "man_resetEmail: {$man_resetEmail['status']}\n";
echo "man_saveCreditCard: {$man_saveCreditCard['status']}\n";


echo "\nRunning MLS QUERIES Tests...\n";
$syncCRMLSListings = syncCRMLSListings($mlsID, $managerID);
$getCRMLSListing = getCRMLSListing($listingID);
$verifyCRMLSMember = verifyCRMLSMember($mlsID, $managerID);
$getRALDListing = getRALDListing($listingID);
echo "syncCRMLSListings: {$syncCRMLSListings['status']}\n";
echo "getCRMLSListing: {$getCRMLSListing['status']}\n";
echo "verifyCRMLSMember: {$verifyCRMLSMember['status']}\n";
echo "getRALDListing: {$getRALDListing['status']}\n";


echo "\nRunning PROPERTY QUERIES Tests...\n";
$reFragQuestions = Prop_Query::reFragQuestions();
$getMLSListing = Prop_Query::getMLSListing($managerID,$vtoken,$mls,$mlsID);
$addProperty = Prop_Query::addProperty($managerID,$vtoken,$url,$title,$addr1,$addr2,$image,$price,$beds,$baths,$dimens,$desc,$currency,$measurement,$questions,$cnEnabled, $anon,$autoEmail, $mls, $mlsnum, $tlc);
$addPropertySudo = Prop_Query::addPropertySudo($email,$url,$title,$addr1,$addr2,$image,$price,$beds,$baths,$dimens,$desc,$currency,$measurement);
$getHostedCount = Prop_Query::getHostedCount($managerID,$vtoken);
$syncHosted = Prop_Query::syncHosted ($managerID,$vtoken, $hosted);
$updateHostedCount = Prop_Query::updateHostedCount($managerID,$vtoken, $pid, $dateHosted, $dayHosted, $offset);
$saveForm = Prop_Query::saveForm($managerID,$vtoken,$pid,$questions,$anon,$mobile,$autoEmail,$cnEnabled,$brokersEnabled,$emailMandatory,$phoneMandatory);
$editProperty = Prop_Query::editProperty($managerID,$vtoken,$pid,$url,$title,$addr1,$addr2,$image,$price,$beds,$baths,$dimens,$desc,$currency,$measurement,$cnEnabled, $anon,$mobile,$autoEmail, $questions, $disabled, $brokersEnabled);
$deleteProperty = Prop_Query::deleteProperty($managerID,$vtoken,$pid);
$disableProperty = Prop_Query::disableProperty($managerID,$vtoken,$pid,$disabled);
$getPropertyByID = Prop_Query::getPropertyByID($propertyID);
$getFormByID = Prop_Query::getFormByID($propertyID);
$getPropertyForRegistrant = Prop_Query::getPropertyForRegistrant($rid);
$syncProperties = Prop_Query::syncProperties($managerID,$vtoken,$properties);
$getMLSList = Prop_Query::getMLSList($managerID,$vtoken);
$getPropertiesByManager = Prop_Query::getPropertiesByManager($managerID,$vtoken);
echo "reFragQuestions: {$reFragQuestions['status']}\n";
echo "getMLSListing: {$getMLSListing['status']}\n";
echo "addProperty: {$addProperty['status']}\n";
echo "addPropertySudo: {$addPropertySudo['status']}\n";
echo "getHostedCount: {$getHostedCount['status']}\n";
echo "syncHosted: {$syncHosted['status']}\n";
echo "updateHostedCount: {$updateHostedCount['status']}\n";
echo "saveForm: {$saveForm['status']}\n";
echo "editProperty: {$editProperty['status']}\n";
echo "deleteProperty: {$deleteProperty['status']}\n";
echo "disableProperty: {$disableProperty['status']}\n";
echo "getPropertyByID: {$getPropertyByID['status']}\n";
echo "getFormByID: {$getFormByID['status']}\n";
echo "getPropertyForRegistrant: {$getPropertyForRegistrant['status']}\n";
echo "syncProperties: {$syncProperties['status']}\n";
echo "getMLSList: {$getMLSList['status']}\n";
echo "getPropertiesByManager: {$getPropertiesByManager['status']}\n";


echo "\nRunning REGISTRANT QUERIES Tests...\n";
$reg_testEmail = Reg_Query::testEmail();
$reg_sendTestEmail = Reg_Query::sendTestEmail($managerID, $vtoken);
$reg_getFC = Reg_Query::getFC($rid);
$reg_getScoreCalibration = Reg_Query::getScoreCalibration($managerID, $vtoken);
$reg_editScoreCalibration = Reg_Query::editScoreCalibration($managerID, $vtoken, $score_calibration);
$reg_getRegistrants = Reg_Query::getRegistrants($managerID, $vtoken);
$reg_getRegistrantsByPID = Reg_Query::getRegistrantsByPID($pid, $managerID, $vtoken);
$reg_getRegistrantsByManager = Reg_Query::getRegistrantsByManager($managerID, $vtoken);
$reg_blacklistEmail = Reg_Query::blacklistEmail($email);
$reg_deactivateRegistrant = Reg_Query::deactivateRegistrant($rid, $email);
$reg_deleteRegistrant = Reg_Query::deleteRegistrant($managerID,$vtoken,$rid);
$reg_syncRegistrants = Reg_Query::syncRegistrants($managerID, $vtoken, $registrants);
$reg_dumpUnsynchedData = Reg_Query::dumpUnsynchedData($managerID,$vtoken,$registrants);
$reg_randomfunction = Reg_Query::randomfunction();;
$reg_sendAutoEmail = Reg_Query::sendAutoEmail($property_result,$manager_result, $email, $name, $timestamp = "") ;
$reg_sendAutoEmails = Reg_Query::sendAutoEmails($propertyID,$managerID, $vtoken);
$reg_addRegularRegistrant = Reg_Query::addRegularRegistrant($propertyID, $honorifics, $name,$email,$phone, $company,$anon, $answers, $marketsnap, $source, $managerID, $vtoken,$sendEmail,$isBroker,$note);
$reg_editRegistrant = Reg_Query::editRegistrant($managerID,$vtoken,$registrantID, $honorifics,$name, $email, $phone,$company,$note);
$reg_broadcastMessage = Reg_Query::broadcastMessage($managerID,$vtoken,$pid,$message,$emails);
$reg_createCSV = Reg_Query::createCSV($propertyID, $managerID, $vtoken, $offset, $type);
echo "testEmail: {$testEmail['status']}\n";
echo "sendTestEmail: {$sendTestEmail['status']}\n";
echo "getFC: {$getFC['status']}\n";
echo "getScoreCalibration: {$getScoreCalibration['status']}\n";
echo "editScoreCalibration: {$editScoreCalibration['status']}\n";
echo "getRegistrants: {$getRegistrants['status']}\n";
echo "getRegistrantsByPID: {$getRegistrantsByPID['status']}\n";
echo "getRegistrantsByManager: {$getRegistrantsByManager['status']}\n";
echo "blacklistEmail: {$blacklistEmail['status']}\n";
echo "deactivateRegistrant: {$deactivateRegistrant['status']}\n";
echo "deleteRegistrant: {$deleteRegistrant['status']}\n";
echo "syncRegistrants: {$syncRegistrants['status']}\n";
echo "dumpUnsynchedData: {$dumpUnsynchedData['status']}\n";
echo "randomfunction: {$randomfunction['status']}\n";
echo "sendAutoEmail: {$sendAutoEmail['status']}\n";
echo "sendAutoEmails: {$sendAutoEmails['status']}\n";
echo "addRegularRegistrant: {$addRegularRegistrant['status']}\n";
echo "editRegistrant: {$editRegistrant['status']}\n";
echo "broadcastMessage: {$broadcastMessage['status']}\n";
echo "createCSV: {$createCSV['status']}\n";


echo "\nRunning SHARED QUERIES Tests...\n";
$syncFromBank = syncFromBank($agent, $brokerageID);
$createEmailTemplate = createEmailTemplate($managerID, $brokerageID);
$createSampleProperty = createSampleProperty($managerID);
$getBHSListings = getBHSListings($email, $managerID);
$getHalsteadListings = getHalsteadListings($email, $managerID);
$getCoreListings = getCoreListings($email);
$getMdrnListings = getMdrnListings($email, $managerID);
$getSpireListings = getSpireListings($email, $managerID);
$getSemoninListings = getSemoninListings($email, $managerID);
$getCorcoranListings = getCorcoranListings($email,$managerID);
$getCitihabitatsListings = getCitihabitatsListings($email, $managerID);
$getPlatinumListings = getPlatinumListings($email, $managerID);
$getC21RedwoodListings = getC21RedwoodListings($email,$managerID);
echo "syncFromBank: {$syncFromBank['status']}\n";
echo "createEmailTemplate: {$createEmailTemplate['status']}\n";
echo "createSampleProperty: {$createSampleProperty['status']}\n";
echo "getBHSListings: {$getBHSListings['status']}\n";
echo "getHalsteadListings: {$getHalsteadListings['status']}\n";
echo "getCoreListings: {$getCoreListings['status']}\n";
echo "getMdrnListings: {$getMdrnListings['status']}\n";
echo "getSpireListings: {$getSpireListings['status']}\n";
echo "getSemoninListings: {$getSemoninListings['status']}\n";
echo "getCorcoranListings: {$getCorcoranListings['status']}\n";
echo "getCitihabitatsListings: {$getCitihabitatsListings['status']}\n";
echo "getPlatinumListings: {$getPlatinumListings['status']}\n";
echo "getC21RedwoodListings: {$getC21RedwoodListings['status']}\n";

?>
