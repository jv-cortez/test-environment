function closeAllPanels() {
    hideAdvancedFilter();
    $(".menu-item").removeClass("selected").addClass("not-selected");
    $(".submenu-item").removeClass("selected").addClass("not-selected");
    $(".sub-container").hide();
    $(".panel").hide();
}


function showTagsExplanation() {
    $(".tags-explanation-panel").addClass("opened");

}

function hideTagsExplanation() {
    $(".tags-explanation-panel").removeClass("opened");

}

function toggleExplanation(panel) {
    $("#explain-"+panel+"-btn").toggleClass("toggled");
    //$("#explain-"+panel+"-btn").hide();
    $("#"+panel+"-explanation").toggle();

}

function showApplyInAppBranding(){

}
