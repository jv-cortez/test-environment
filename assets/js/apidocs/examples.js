function output(id, input) {
    // document.body.appendChild(document.createElement('pre')).innerHTML = inp;
    $("#" + id).append("<pre>" + input + "</pre>");
}

// function syntaxHighlight(json) {
//     json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
//     return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
//         var cls = 'number';
//         if (/^"/.test(match)) {
//             if (/:$/.test(match)) {
//                 cls = 'key';
//             } else {
//                 cls = 'string';
//             }
//         } else if (/true|false/.test(match)) {
//             cls = 'boolean';
//         } else if (/null/.test(match)) {
//             cls = 'null';
//         }
//         return '<span class="' + cls + '">' + match + '</span>';
//     });
// }

var propRespObj = {
  "1": {
    "title": "Beautiful Waterfront Apartment in English Bay",
    "price": "2300000",
    "beds": 2,
    "baths": 2,
    "dimens": -1,
    "type": "Apartment",
    "email": "hello[at]spac.io",
    "addr1": "231 Beach Ave.",
    "addr2": "",
    "url": "https://spac.io",
    "sid": 10191,
    "mlsNo": "AB123456",
    "image": "https://s3.amazonaws.com/spacio-user-images/55c6ca75aace0f500cabb8b5_property.jpg"
  },
  "2": {
    "title": "Penthouse In East Village, Close To Restaurants",
    "price": "$2,500,000",
    "beds": -1,
    "baths": -1,
    "dimens": -1,
    "type": "Apartment",
    "email": "hello[at]spac.io",
    "addr1": "324 East 14th Street",
    "addr2": "",
    "url": "",
    "sid": 10512,
    "mlsNo": "CD234567",
    "image": "https://s3.amazonaws.com/spacio-user-images/560b2f2caace0f3c758b4567_property.jpg"
  },
  // ... // Full response truncated for brevity
};

var propRespBObj = {
  "title": "Beautiful Waterfront Apartment in English Bay",
  "price": "2300000",
  "beds": 2,
  "baths": 2,
  "dimens": -1,
  "type": "Apartment",
  "email": "hello[at]spac.io",
  "addr1": "231 Beach Ave.",
  "addr2": "",
  "url": "https://spac.io",
  "sid": 10191,
  "mlsNo": "AB123456",
  "image": "https://s3.amazonaws.com/spacio-user-images/55c6ca75aace0f500cabb8b5_property.jpg"
};

var regRespObj = {
  "1": {
    "name": "Scott Lang",
    "email": "sl@example.com",
    "phone": "234-567-8900",
    "hasAgent": "NO",
    "hasFinancing": "NO",
    "answersObj": {
      "0": {
        "questionObj": {
          "question": "Are you working with an agent?",
          "questionCN": "您有否地产经纪代理?",
          "type": "agent",
          "caption": "Are you working with an agent? (Agent name and contact fields will appear if Yes is selected)",
          "questionID": "583632da2b3077566f53af21"
        },
        "answer": "NO"
      },
      "1": {
        "questionObj": {
          "questionCN": "您有否抵押贷款批准?",
          "type": "yn",
          "caption": "Are you mortgage pre-approved?",
          "question": "Are you mortgage pre-approved?",
          "questionID": "583632da2b3077566f53af1b"
        },
        "answer": "NO"
      },
      "2": {
        "questionObj": {
          "question": "Do you currently rent or own?",
          "questionCN": "您現在住的是?",
          "type": "mc",
          "choices": {
            "0": {
              "answer": "Own",
              "answerCN": "购买",
              "type": "radio"
            },
            "1": {
              "answer": "Rent",
              "answerCN": "租赁",
              "type": "radio"
            }
          },
          "questionID": "5910f9d492127dee0c2bb8cf"
        },
        "answer": "Own"
      },
      "3": {
        "questionObj": {
          "question": "How did you hear about this open house?",
          "questionCN": "您是如何听说到我们的",
          "type": "text",
          "questionID": "5910f9f392127d7e052bb8cf"
        },
        "answer": "N/A"
      }
    },
    "dateCreated": "2017-05-08 11:11:54"
  },
  // ... // Full response truncated for brevity
};

var atRespObj = {
  "email": "agent@example.com",
  "ukey": "agentUkey",
  "accessToken": "ZtKH5J76DN5DyQ3R"
};

var iakErrObj = {
  "errorID": "not-authorized",
  "errorDesc": "API key is not authorized"
};
var rleErrObj = {
  "errorID": "rate-reached",
  "errorDesc": "API rate limit reached (Next reset at: 2017-01-01 0:00:00 UTC)"
};
var iuErrObj = {
  "errorID": "invalid-ukey",
  "errorDesc": "Ukey not found"
};
var isErrObj = {
  "errorID": "invalid-sid",
  "errorDesc": "No Spacio ID found"
};
var nrErrObj = {
  "errorID": "no-results",
  "errorDesc": "No registrants found"
};
var ieErrObj = {
  "errorID": "invalid-email",
  "errorDesc": "No user found"
};
var adErrObj = {
  "errorID": "access-denied",
  "errorDesc": "Insufficient privileges for specified resource"
};

var propRespStr = JSON.stringify(propRespObj, undefined, 4);
var propRespBStr = JSON.stringify(propRespBObj, undefined, 4);
var regRespStr = JSON.stringify(regRespObj, undefined, 4);
var atRespStr = JSON.stringify(atRespObj, undefined, 4);

var iakErrStr = JSON.stringify(iakErrObj, undefined, 4);
var rleErrStr = JSON.stringify(rleErrObj, undefined, 4);
var iuErrStr = JSON.stringify(iuErrObj, undefined, 4);
var isErrStr = JSON.stringify(isErrObj, undefined, 4);
var nrErrStr = JSON.stringify(nrErrObj, undefined, 4);
var ieErrStr = JSON.stringify(ieErrObj, undefined, 4);
var adErrStr = JSON.stringify(adErrObj, undefined, 4);

output("prop-resp", propRespStr);
output("prop-resp-b", propRespBStr);
output("reg-resp", regRespStr);
output("at-resp", atRespStr);

output("invalid-apikey-err", iakErrStr);
output("rate-reached-err", rleErrStr);
output("invalid-ukey-err", iuErrStr);
output("invalid-sid-err", isErrStr);
output("no-results-err", nrErrStr);
output("invalid-email-err", ieErrStr);
output("access-denied-err", adErrStr);
// output(syntaxHighlight(str));
