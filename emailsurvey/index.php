<?php
  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  require_once('../oh/config/db_conn.php');
  require_once('../oh/require/core.php');

  $_registrantID = $_GET["r"];
  $_managerID = $_GET["m"];
  $_propertyID = $_GET["p"];
  $_email_response = $_GET["e"];

?>

<!-- http://stage.spac.io/emailsurvey?r=55a07277aace0fdc2b8b456b&m=57924d1992127dd9218b4567&p=559fa540aace0f3b268b4567&e=true -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="assets/img/favicon.png">

  <title>Spacio: Introducing the Paperless Open House</title>

  <link href="/assets/css/normalize.css" rel="stylesheet">
  <link href="/assets/css/website.css" rel="stylesheet">
  <link href="/assets/fonts/fonts.css" rel="stylesheet">
  <link href="/assets/css/emailsurveys.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="http://www.spac.io/assets/img/favicon.png"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <link rel="import" id="topmenu" href="/topmenu.html">
  <link rel="import" id="footer" href="/footer.html">
  <link rel="import" id="popups" href="/popups.html">
  <link rel="import" id="alertbox" href="/alertbox.html">

</head>

<body style="margin:0;padding:0;">


  <div class="spacio-top-menu">
    <div class="web-menu">
      <a href="/">
         <div class="spacio-top-menu-logo">
           <img src="/assets/img/website/spacio_logo_long.png" height="40">
         </div>
      </a>
    </div>
  </div><!-- end of spacio-top-menu -->

  <div class="popup-overlay"></div>
  <!-- POPUP OVERLAY END -->
  <div class="alert-overlay"></div>
  <!-- ALERT OVERLAY END -->

  <div class="main-container">

      <div class="title"><strong>Survey Questions:</strong></div>
      <div class="questions">
          <form id="submit_survey" method="post">
            <div class="response">
              <div class='question survey-question-1'></div>
              <input type="text" value="Response" class='answer'/>
            </div>
            <div class="response">
              <div class='question survey-question-2'></div>
              <input type="text" value="Response" class='answer'/>
            </div>
            <div class="response">
              <div class='question survey-question-3'></div>
              <input type="text" value="Response" class='answer'/>
            </div>
            <div>
              <input class="submit-button" type="submit" value="Submit"></input>
            </div>
          </form>

      <!-- The listings html will load with javascript here -->
      </div>
    <!-- </div>  -->

  </div><!-- end of main-container  -->

  <!-- <div class="footer normal"></div> -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/assets/js/jquery.loadTemplate.min.js"></script>
<script src="/assets/js/website.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
    insertAlertBox();
    getQuestions();
  })


  function getQuestions(){

    var _query = {};

    _query["fn"] = "postAgentSurvey";
    _query["registrantID"] = "<?php echo $_registrantID?>";
    _query["managerID"] = "<?php echo $_managerID?>";
    _query["propertyID"] = "<?php echo $_propertyID?>";
    _query["responses"] = {};
    _query["responses"][0] = {};
    _query["responses"][0]["question"] = "Is this property fairly priced?";
    _query["responses"][0]["answer"] = "<?php echo $_email_response?>";

    var json_data = _query;

    console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

    console.log(json_data);

    $.ajax({
      url: "https://ws.spac.io/stage/oh/webservice_survey_system/1.0.0/webservice_survey.php",
      type:"POST",
      data: {json : json_data},
      success: function(data, textStatus, jqXHR){

        console.log(data);

        if (data.status == 1) {
          console.log("Returned JSON Object: "+JSON.stringify(data));

          $(".survey-question-1").html("What is your favorite color shirt for cocktail parties with friends?");
          $(".survey-question-2").html("What is your favorite dog?");
          $(".survey-question-3").html("What is your favorite animal?");

          // this will need to be an each loop that populates the survey_forms

          // if ($.isFunction(completionHandler)) {
          //   completionHandler();
          // }


        } else {

        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getListing error Error: "+errorThrown);
        loading(false);
      },
      dataType: "json"
    });
  }



    $( "form#submit_survey" ).submit(function( event ) {


      var _query = {};

      _query["fn"] = "postAgentSurvey";
      _query["registrantID"] = "<?php echo $_registrantID?>";
      _query["managerID"] = "<?php echo $_managerID?>";
      _query["propertyID"] = "<?php echo $_propertyID?>";
      _query["responses"] = [];

      $.each($('#submit_survey .response'), function() {
          var temp = {};
          temp["question"] = $(this).children('.question').text();
          temp["answer"] = $(this).children('.answer').val();
          _query["responses"].push(temp);
      });

      var json_data = _query;

      console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

      console.log(json_data);

      $.ajax({
        url: "https://ws.spac.io/stage/oh/webservice_survey_system/1.0.0/webservice_survey.php",
        type:"POST",
        data: {json : json_data},
        success: function(data, textStatus, jqXHR){

          console.log(data);

          if (data.status == 1) {
            console.log("Returned JSON Object: "+JSON.stringify(data));
            showAlert("info","Survey Submitted","Thank you for your time.");

          } else {
            showAlert("error","Survey Failed to Submit","Sorry, we were unable to submit your survey. Please contact your account administrator.");
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: getListing error Error: "+errorThrown);
          loading(false);
        },
        dataType: "json"
      });
  		event.preventDefault();
    })

</script>

</body>
</html>
