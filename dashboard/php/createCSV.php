<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Content-type: application/json');

require_once('db_conn.php');
require_once('core.php');

$data = isset( $_POST['json'] ) ? $_POST['json'] : 0;
$fn = $data["fn"];

$_propertyID = (int)$data["propertyID"];
$_managerID = (string)$data["managerID"];
$_vtoken = (string)$data["vtoken"];

$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;	
$collection_managers = Db_Conn::getInstance()->getConnection()->managers;	
		
$manager_result = $collection_managers->findOne(array('_id' => new MongoId($_managerID), "vtoken" => $_vtoken));
		
		if (!is_null($manager_result)) {
	
			
	
			$cursor = $collection_registrants->find(array('propertyID' => $propertyID));
			
			date_default_timezone_set('America/Los_Angeles');
			$date = date('m-d-Y_h:i:s');
			$filename = 'registrants_'.$date.'_'.$_propertyID.'.csv';

			$fp = fopen('../csv/'.$filename, 'w');
			
			foreach ($cursor as $doc) {
				$registrant = array();
				array_push($registrant, $doc["name"]);
				if ($doc["tempEmail"] != "N/A") {
					array_push($registrant, $doc["tempEmail"]);	
				} else {
					array_push($registrant, $doc["email"]);	
				}
				array_push($registrant, $doc["phone"]);
				foreach ($doc["answers"] as $qa) {
					array_push($registrant, $qa["question"]." ".$qa["answer"]);
				}
				fputcsv($fp, $registrant);
			}
			
			fclose($fp);

			$return = array(
				"status" => 1
			);
		} else {
			$return = array("status" => $collection_managers);
		}
		
		echo json_encode($return); 	
	

?>