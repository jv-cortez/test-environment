<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Content-type: application/json');

// Load the database connector
require_once 'require/db_conn.php';

// Connect and load collection
$cMans = Db_Conn::getInstance()->getConnection()->managers;

// Count documents in 'managers' collection and print out
$rMans = $cMans->find();
// echo "\nNumber of users: ".$rMans."\n";
// foreach ($rMans as $document) {
//   echo "<pre>" .var_export($document, true). "</pre>";
// }
?>
