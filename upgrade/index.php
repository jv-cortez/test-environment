<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_email = isset($_GET['e']) ? $_GET['e'] : '';
	$_promo = $_GET["p"];
	$_plan = $_GET["plan"];
	$_grandfather = $_GET["g"];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/assets/img/favicon.png">

        <title>Spacio: Introducing the Paperless Open House</title>

        <link href="/assets/css/normalize.css" rel="stylesheet">
        <link href="/assets/css/website.css" rel="stylesheet">
        <link href="/assets/css/upgrade.css" rel="stylesheet">

        <link href="/assets/fonts/fonts.css" rel="stylesheet">

        <link rel="import" id="topmenu" href="/topmenu.html">
        <link rel="import" id="footer" href="/footer.html">
        <link rel="import" id="popups" href="/popups.html">
        <link rel="import" id="alertbox" href="/alertbox.html">

      </head>

<body style="padding-top:0;">

	<div class="spacio-top-menu"></div>
  <!-- TOP MENU END -->
	<div class="popup-overlay"></div>
  <!-- POPUP OVERLAY END -->
  <div class="alert-overlay"></div>
  <!-- ALERT OVERLAY END -->
	<div class="loading-panel">
    	<div style="display:flex;flex-direction:column;align-items:center;justify-content:center;width:100%;height:100%;">
        	<div style="height:50px;text-align:center;color:white;">
        		<span class="msg" style="-webkit-touch-callout:none;-webkit-user-select: none;">Loading...</span>
          </div>
      </div>
  </div> <!---- LOADING PANEL OVERLAY --->


  <div class="upgrade-scroll-one">
		<div class="upgrade-panel">

				<div class="step-1">
					<form id="step1_Form" style="">
						<div class="xlarge" style="margin-bottom:20px;color:#666;">
						Upgrade Your Spacio Account
						</div>
						<div class="normal regular" style="margin-bottom:30px;">
						STEP <span class="bold">1</span> OF <span class="bold">2</span>: Please enter your email and choose your plan. <span class="bold">All promotions will be applied at the next step.</span>
						</div>
						<div class="upgrade-input-flex">
							<div class="upgrade-label">EMAIL</div>
							<div class="upgrade-seperator"></div>
							<input class="upgrade-input" type="text" name="email" value="<?php echo $_email;?>" id="email" tabindex="1"  placeholder="">
						</div>

						<div class="upgrade-input-flex">
							<div class="upgrade-label">COUNTRY</div>
							<div class="upgrade-seperator"></div>
							<select name="country" class="upgrade-select" onchange="countryChanged(this)" tabindex="2">
								<option value="US">United States</option>
								<option value="CA">Canada</option>
								<option value="Other">Other</option>
							</select>
						</div>

						<div class="upgrade-input-flex province-input-box" style="display:none;">
							<div class="upgrade-label">PROVINCE</div>
							<div class="upgrade-seperator"></div>
							<select name="province" class="upgrade-select" onchange="provinceChanged(this)" tabindex="3">
								<option value="none">Select a Province</option>
								<option value="AB">Alberta</option>
								<option value="BC">British Columbia</option>
								<option value="MB">Manitoba</option>
								<option value="NB">New Brunswick</option>
								<option value="NL">Newfoundland</option>
								<option value="NS">Nova Scotia</option>
								<option value="NT">North West Territories</option>
								<option value="NU">Nunavut</option>
								<option value="ON">Ontario</option>
								<option value="PE">Prince Edward Island</option>
								<option value="QC">Quebec</option>
								<option value="SK">Saskatchewan</option>
								<option value="YT">Yukon</option>
							</select>
						</div>

						<div class="upgrade-input-flex-2">
							<div class="upgrade-label">PLAN</div>
							<div class="upgrade-seperator"></div>
							<div class="upgrade-plans">
								<div class="upgrade-plan">
									<div class="upgrade-radio">
										<input type="radio" value="annual-std-new" name="plan" checked />
									</div>
									<label>Annual Plan - <span style="text-decoration:line-through">$300/year</span> $255/year<br/>
										15% Discount / $45 Savings!</label>
								</div>
								<div class="upgrade-plan">
									<div class="upgrade-radio">
										<input type="radio" value="pro-std-new" name="plan" />
									</div>
									<label>Monthly Plan - $25/month</label>
								</div>

							</div>
						</div>

						<!-- <div class="upgrade-plans">
							<div class="upgrade-plan monthly-plan" onclick="selectPlan('pro-std-new')">
								<div class="title">MONTHLY</div>
								<div style="margin-top:65px;">$<span class="large">25</span>
									/ <span style="letter-spacing:3px">MONTH</span></div>
							</div>
							<div class="upgrade-plan annual-plan" onclick="selectPlan('annual-std-new')">
								<div class="title">ANNUAL</div>
								<div style="margin-top:60px;">$<span class="large">255</span>
									/ <span style="letter-spacing:3px">YEAR</span><br/>(15% Discount)</div>
							</div>
						</div> -->

						<div style="width:100%;text-align:right">
							<input class="upgrade-next-btn" type="button" name="next" value="Next" onclick="nextStep()">
						</div>
					</form>
				</div>
				<div class="step-2">
					<form id="upgradeToPremium_Form" style="">
						<div class="xlarge" style="margin-bottom:20px;color:#666;">
							Almost Done.
						</div>
						<div class="normal" style="margin-bottom:30px;">
							STEP <span class="bold">2</span> OF <span class="bold">2</span>: Please enter your payment information.
						</div>
						<div style="30px;line-height:30px;width:100%;margin-bottom:10px;">
							<img src="/assets/img/website/mastercard.png" height="30" style="margin-right:10px;">
							<img src="/assets/img/website/visa.png" height="30" style="margin-right:10px;">
							<img src="/assets/img/website/amex.png" height="30" style="margin-right:10px;">
						</div>
						<div class="upgrade-input-flex">
							<div class="upgrade-label">CARD #</div>
							<div class="upgrade-seperator"></div>
							<input class="upgrade-input" type="text" data-stripe="number" value="" size="20" tabindex="-1"  placeholder="">
						</div>
						<div class="upgrade-input-flex">
							<div class="upgrade-label">CVC</div>
							<div class="upgrade-seperator"></div>
							<input class="upgrade-input" type="text" data-stripe="cvc" value="" size="4" tabindex="-1"  placeholder="">
						</div>
						<div class="upgrade-input-flex">
							<div class="upgrade-label">EXPIRATION</div>
							<div class="upgrade-seperator"></div>
							<div class="upgrade-expiration-row">
								<input class="upgrade-input-half" type="text" data-stripe="exp-month" value="" tabindex="-1" size="3"  placeholder="MM" style="width:30px"> /
								<input class="upgrade-input-half" type="text" data-stripe="exp-year" value="" tabindex="-1" size="4"  placeholder="YYYY" style="width:45px">
							</div>
						</div>

						<div class="plan-info">
							<div class="bold" style="letter-spacing:3px;margin-bottom:10px;">MY PLAN</div>
							<div class="plan-type">
								<div class="left-column"></div>
								<div class="right-column"></div>
							</div>
							<div class="plan-tax">
								<div class="left-column">Tax</div>
								<div class="right-column"></div>
							</div>
							<div class="plan-discount">
								<div class="left-column">Discount</div>
								<div class="right-column"></div>
							</div>
							<div class="plan-total">
								<div class="left-column">Total</div>
								<div class="right-column"></div>
							</div>
						</div>

						<div style="width:100%;text-align:right">
							<input class="upgrade-back-btn" type="button" name="prev" value="Back" onclick="prevStep()">
							<input class="upgrade-submit-btn" type="submit"  value="Upgrade">
						</div>
					</form>
					<div style="font-size:12px;margin-top:20px;">
						Spacio is headquartered in Vancouver, BC, Canada with an office in New York. We are not responsible for any credit card transaction fees from your provider. Please check with your bank or financial institution before upgrading.
					</div>
				</div>

			</div>


  </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>

<script type="text/javascript">
	var promo = "<?php echo $_promo;?>";
	var plan = "<?php echo $_plan;?>";
	var grandfathered = "<?php echo $_grandfather;?>";
	var tax_rates = {};
	var selected_plan = "";
	var account_email = "<?php echo $_email;?>";
	var discount_annual = 0;
	var discount_monthly = 0;
	var total = 0;
	var tax_type = "";
	var tax_rate = 0;
	var country = "";
	var province = "";

	 //Stripe.setPublishableKey('pk_test_1Xk12UcY2hyb1y3cLVTkk7Rs');
	 Stripe.setPublishableKey('pk_live_QRFV0CvJO6Iq122LsxWt93nv');

	 $(document).ready(function(){

		  insertAlertBox();
			$("#email").val(account_email);
			if(plan == "bi") {
				selected_plan = "brokerage-individual";
				country = "US";
				getAccountInfo(account_email);
			}
	 });

	 function nextStep() {
		 selected_plan = $("input[name='plan']:checked").val();
		 account_email = $("#email").val();
		 if( account_email== "") {
			 showAlert("error","Email Required", "Please enter the email assigned with your Spacio account.","Ok");
		 } else if(!isValidEmailAddress(account_email)) {
			 showAlert("error","Invalid Email", "Please enter a valid email address.","Ok");
		 } else {
			 account_email = $("#email").val();
			 getAccountInfo(account_email);
		 }
	 }

	 function prevStep() {
		 $(".step-1").css("left","0px");
		 $(".step-2").css("left","100%");
	 }

	$( "form#upgradeToPremium_Form" ).submit(function( event ) {
		var $form = $(this);
		var _fields = $( this ).serializeArray();
		var _query = {};

		$form.find('button').prop('disabled', true);

		Stripe.card.createToken($form, function(status, response) {
			if(response.error) {
				showAlert("error","Error Processing Card", response.error.message,"Ok");
			} else {
				var token = response.id;
				upgradeAccount(account_email, selected_plan, country, province, token);
			}
		});
		event.preventDefault();
	});

	function getAccountInfo(t_email) {
		loading(true);
		var _query = {};
		_query["fn"] = "getAccountInfo";
		_query["email"] = t_email;

		var json_data = _query;
		console.log("Outbound JSON Query: "+JSON.stringify(json_data));
		$.ajax({
			url: "https://spac.io/oh/webservice/1.1.6/webservice.php",
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					discount_annual = data.discount_annual;
					discount_monthly = data.discount_monthly;

					if(data.astatus == "active" && (data.type == "standard" || data.type == "free" || data.type == "brokerage" || data.type == "team")) {
						showAlert("info","Already Upgraded","There is an upgraded account associated with this email. Please login.","Login","Cancel", function(){
						window.location = "https://spac.io/dashboard/";
						}, null);
						loading(false);
					} else {
						if(discount_annual == 0 && selected_plan == "annual-std-new") {
							discount_annual = 15;
						}

						if(selected_plan == "pro-std-new") {
							$(".plan-type .left-column").html("Monthly Plan (<span style='cursor:pointer' class='bold' onclick='prevStep()'>change</span>)");
							$(".plan-type .right-column").html("$25/month");
							total = 25;

							$(".plan-discount .right-column").html(discount_monthly+"%");
							total = total * ((100 - discount_monthly)/100) * (1 + tax_rate/100);
						}
						if(selected_plan == "annual-std-new") {
							$(".plan-type .left-column").html("Annual Plan (<span style='cursor:pointer' class='bold' onclick='prevStep()'>change</span>)");
							$(".plan-type .right-column").html("$300/year");
							total = 300;
							$(".plan-discount .right-column").html(discount_annual+"%");
							total = total * ((100 - discount_annual)/100) * (1 + tax_rate/100);
						}

						if(selected_plan == "brokerage-individual" && data.type == "brokerage-individual") {
							$(".plan-type .left-column").html("Brokerage Opt-In");
							$(".plan-type .right-column").html("$25/month");
							total = 25;

							$(".plan-discount .right-column").html(discount_monthly+"%");
							total = total * ((100 - discount_monthly)/100) * (1 + tax_rate/100);
						}

						if(tax_rate == 0) {
							$(".plan-tax .right-column").html("0.00");
						} else {
							$(".plan-tax .right-column").html("%"+tax_rate+".00 ("+tax_type+")");
						}

						if(data.referred != null && data.referred != "") {
							$(".plan-discount .left-column").html("Discount ("+data.referred+")");
						} else {
							$(".plan-discount .left-column").html("Discount");
						}

						total = total.toFixed(2);
						$(".plan-total .right-column").html("$"+total);

						$(".step-1").css("left","-100%");
		 			 	$(".step-2").css("left","0px");
						loading(false);
					}
				} else {
					showAlert("error","Email Not Found","We could not find a Spacio account matching this email. Please contact support at support@spac.io.");
					loading(false);
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				showAlert("error","Unknown Error","Please contact support at support@spac.io.");

				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	}

	function upgradeAccount(t_email, t_plan, t_country, t_province, tk){
		loading(true);
		var _query = {};
		_query["fn"] = "upgradeAccount";
		_query["email"] = t_email;
		_query["promo"] = "";
		_query["plan"] = t_plan;
		_query["stripeToken"] = tk;
		_query["taxRate"] = tax_rate;
		_query["taxType"] = tax_type;
		_query["country"] = t_country;
		_query["province"] = t_province;

		var json_data = _query;
		console.log("Outbound JSON Query: "+JSON.stringify(json_data));

		$.ajax({
			url: "../oh/webservice/1.1.6/webservice.php",
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				loading(false);
				if (data.status == 1) {
					$('#upgradeToPremium_Form')[0].reset();
					showAlert("info","Thank you for Upgrading","Thank You for Upgrading! Please login again to see the upgrade reflected.","Sign In","Cancel", function(){
					window.location = "http://spac.io/dashboard/";
					}, null);
				} else if (data.status == 3) {
					showAlert("info","No need to upgrade","You already have an upgraded Spacio account. Please Sign In.","Login","Cancel", function(){
					window.location = "http://spac.io/dashboard/";
					}, null);
				} else {
					if (data.error_type == "invalid_request_error" && data.error_param == "coupon") {
						showAlert("error","Invalid Promo Code","Sorry, this coupon has either expired or does not exist.");
					} else if (data.error_type =="card_error" && data.error_code == "card_declined"){
						showAlert("error","Card Declined","Sorry, your card was declined. Please verify that you entered a valid credit card.");
					} else {
						showAlert("error","Unknowns","There was an unknown error. Please contact support.");
					}
				}
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});
	}



	function randomString(_length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

		var randomstring = '';
		for (var i=0; i<_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}

		return randomstring;
	}

	function countryChanged(target) {
		country = $(target).val();

		if(country == "CA") {
			$(".province-input-box").show();
			getTaxRate("CA");
		} else {
			$(".province-input-box").hide();
		}
		//alert($(target).val());
	}

	function provinceChanged(target) {
		province = $(target).val();

		if(tax_rates[province]["average_rate"]["label"] == "HST") {
			tax_rate = tax_rates[province]["average_rate"]["rate"] * 100;
			tax_type = tax_rates[province]["average_rate"]["label"];
		} else {
			tax_rate = tax_rates[province]["minimum_rate"]["rate"] * 100;
			tax_type = tax_rates[province]["minimum_rate"]["label"];
		}

		$(".taxes-annual").html("(+"+Math.floor(tax_rate)+"% "+tax_type+")");
		$(".taxes-monthly").html("(+"+Math.floor(tax_rate)+"% "+tax_type+")");
		//alert($(target).val());
	}

	function getTaxRate(code) {
		var _query = {};

		_query["code"] = code;
		var json_data = _query;
		console.log("Outbound JSON Query: "+JSON.stringify(json_data));

		$.ajax({
			url: "https://ws.spac.io/prod/oh/webservice_taxjar/getTaxRate.php",
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));

				$.each(data, function(index,value){
					tax_rates[value["region_code"]] = value;
				});
				//debugLog("Query Status: "+data.status);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
			},
			dataType: "json"
		});

	}

	function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
	};

	/*
	var agent = navigator.userAgent;
	var isIphone = ((agent.indexOf('iPhone') != -1) || (agent.indexOf('iPod') != -1)) ;
	if (isIphone) {
		$("#iphone-msg").html("Returning you to the app.");
		setTimeout(function(){window.location.href = 'SpacioiOS://'; }, 2500);

	}
	*/

</script>



</body>
</html>
