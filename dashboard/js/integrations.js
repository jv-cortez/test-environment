function applyAdwerxID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyAdwerxID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["adwerxID"] = $("input[name='adwerxID']").val();

    console.log("OUTBOUND JSON Object: "+JSON.stringify(_query));

		loading(true, "Saving Adwerx Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Adwerx Settings","Your Adwerx settings have been saved.");
					user["integrations"]["adwerxID"] = _query["adwerxID"];

					$("#edit-adwerxID").val(user["integrations"]["adwerxID"]);

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearAdwerxID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyAdwerxID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["adwerxID"] = "";


		loading(true, "Saving Adwerx Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Adwerx Settings","Your Adwerx settings have been cleared.");
					user["integrations"]["adwerxID"] = "";

					$("#edit-adwerxID").val("");

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}

function clearBoomtownRefreshToken() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "clearBoomtownRefreshToken";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		loading(true, "Clearing Boomtown Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL_new,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Boomtown Integration Removed","Your Spacio account is no longer connected to Boomtown.");
					user["integrations"]["boomtownRefreshToken"] = "";
					showIntegrationsPanel();
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}

function applyRealvolveID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRealvolveID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["realvolveID"] =	$("input[name='realvolveID']").val();

		loading(true, "Saving Follow Up Boss API Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["realvolveID"] == "") {
						showAlert("Realvolve Settings","Your Realvolve settings have been cleared.");
						user["integrations"]["realvolveID"] = "";
					} else {
						showAlert("Realvolve Settings","Your Realvolve settings have been saved.");
						user["integrations"]["realvolveID"] = _query["realvolveID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearRealvolveID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRealvolveID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["realvolveID"] =	"";

		loading(true, "Saving Follow Up Boss API Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["realvolveID"] == "") {
						showAlert("Realvolve Settings","Your Realvolve settings have been cleared.");
						user["integrations"]["realvolveID"] = "";
					} else {
						showAlert("Realvolve Settings","Your Realvolve settings have been saved.");
						user["integrations"]["realvolveID"] = _query["realvolveID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}



function applyCloudCMAID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyCloudCMAID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["cloudcmaID"] = $("input[name='cloudcmaID']").val();

		loading(true, "Saving Cloud CMA Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Cloud CMA Settings","Your Cloud CMA settings have been saved.");
					user["integrations"]["cloudcmaID"] = _query["cloudcmaID"];

					$("#edit-cloudcmaID").val(user["integrations"]["cloudcmaID"]);

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearCloudCMAID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyCloudCMAID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["cloudcmaID"] = "";


		loading(true, "Saving Cloud CMA Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Cloud CMA Settings","Your Cloud CMA settings have been cleared.");
					user["integrations"]["cloudcmaID"] = "";

					$("#edit-cloudcmaID").val("");

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyBombbombKey() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyBombbombKey";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["bombbombKey"] = $("input[name='bombbombKey']").val();

		loading(true, "Saving BombBomb Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("BombBomb Settings","Your BombBomb settings have been saved.");
					user["integrations"]["bombbombKey"] = _query["bombbombKey"];

					$("#edit-bombbombKey").val(user["integrations"]["bombbombKey"]);

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearBombbombKey() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyBombbombKey";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["bombbombKey"] = "";

		loading(true, "Saving BombBomb Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("BombBomb Settings","Your BombBomb settings have been cleared.");
					user["integrations"]["bombbombKey"] = _query["bombbombKey"];

					$("#edit-bombbombKey").val(user["integrations"]["bombbombKey"]);

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function applyRealScoutID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRealScoutID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["realscoutEmail"] = $("input[name='realscoutEmail']").val();


		loading(true, "Saving RealScout Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL_new,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("RealScout Settings","Your RealScout settings have been saved.");
					user["integrations"]["realscoutEmail"] = _query["realscoutEmail"];
					$("#edit-realscoutEmail").val(user["integrations"]["realscoutEmail"]);
				} else if(data.status == 2){
                    showAlert("Integration Unavailable","This integration is available for brokerage customers only.");
                    $("#edit-realscoutEmail").val("");

                } else if(data.status == 3){
                    showAlert("Integration Unavailable","This integration is not available for your brokerage.");
                    $("#edit-realscoutEmail").val("");

                } else if(data.status == 4){
                    showAlert("Integration Error","Invalid email. Please contact RealScout to verify that your email is part of your brokerage's RealScout account.");
                    $("#edit-realscoutEmail").val("");

                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not change this setting at the moment. Please contact support.");
                    $("#edit-realscoutEmail").val("");
                }

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearRealScoutID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRealScoutID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["realscoutEmail"] = "";


		loading(true, "Saving RealScout Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("RealScout Settings Cleared","You no longer have RealScout linked to your account.");
					user["integrations"]["realscoutEmail"] = "";

					$("#edit-realscoutEmail").val(user["integrations"]["realscoutEmail"]);

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyTTID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyTTID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["ttID"] = $("input[name='ttID']").val();

		if ($("#edit-ttAutoClose").is(':checked')) {
			_query["ttAutoClose"] = "YES";
			user["integrations"]["ttAutoClose"] = "YES";
			localStorage["ttAutoClose"] = "YES";
		} else {
			_query["ttAutoClose"] = "NO";
			user["integrations"]["ttAutoClose"] = "NO";
			localStorage["ttAutoClose"] = "NO";
		}

		loading(true, "Saving Testimonial Tree Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["ttID"] == "") {
						showAlert("Testimonial Tree Email Cleared","You no longer have a Testimonial Tree Email linked to your account.");
						user["integrations"]["ttID"] = "";
					} else {
						showAlert("Testimonial Tree Settings","Your Testimonial Tree settings have been saved.");
						user["integrations"]["ttID"] = _query["ttID"];
					}

					if (user["integrations"]["ttAutoClose"] == "YES") {
						$("#edit-ttAutoClose").prop( "checked", true );
					} else {
						$("#edit-ttAutoClose").prop( "checked", false );
					}

					$("#edit-ttID").val(user["integrations"]["ttID"]);
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearTTID() {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyTTID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["ttID"] = "";
		$("input[name='ttID']").val("");

		loading(true, "Clearing Testimonial Tree Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Testimonial Tree Email Cleared","You no longer have a Testimonial Tree Email linked to your account.");
					user["integrations"]["ttID"] = "";
					$("#edit-ttID").val("");

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyZillowReviewID() {
    if (user["id"] != null && user["vtoken"] != null )    {

        var _query = {};
        _query["fn"] = "applyZillowReviewID";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];
        _query["zillowreviewID"] = $("input[name='zillowreviewID']").val();
        if ($("#edit-zillowreviewAutoClose").is(':checked')) {
            _query["zillowreviewAutoClose"] = "YES";
            user["integrations"]["zillowreviewAutoClose"] = "YES";
            localStorage["zillowreviewAutoClose"] = "YES";
        } else {
            _query["zillowreviewAutoClose"] = "NO";
            user["integrations"]["zillowreviewAutoClose"] = "NO";
            localStorage["zillowreviewAutoClose"] = "NO";
        }

        loading(true, "Saving Zillow Review Email...");
        var json_data = _query;
        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {

                    if(_query["zillowreviewID"] == "") {
                        showAlert("Zillow Review Email Cleared","You no longer have a Zillow Review Email linked to your account.");
                        user["integrations"]["zillowreviewID"] = "";
                    } else {
                        showAlert("Zillow Review Settings","Your Zillow Review settings have been saved.");
                        user["integrations"]["zillowreviewID"] = _query["zillowreviewID"];
                        getZillowReview();
                    }

                    if (user["integrations"]["zillowreviewAutoClose"] == "YES") {
                        $("#edit-zillowreviewAutoClose").prop( "checked", true );
                    } else {
                        $("#edit-zillowreviewAutoClose").prop( "checked", false );
                    }
                    $("#edit-zillowreviewID").val(user["integrations"]["zillowreviewID"]);

                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not apply this code at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });

    } else {
        forceLogout();
    }

}

function clearZillowReviewID() {
    if (user["id"] != null && user["vtoken"] != null )    {

        var _query = {};
        _query["fn"] = "applyZillowReviewID";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];
        _query["zillowreviewID"] = "";
        _query["zillowreviewAutoClose"] = "NO";

        $("input[name='zillowreviewID']").val("");

        loading(true, "Saving Zillow Review Email...");
        var json_data = _query;
        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {

                    if(_query["zillowreviewID"] == "") {
                        showAlert("Zillow Review Email Cleared","You no longer have a Zillow Review Email linked to your account.");
                        user["integrations"]["zillowreviewID"] = "";
                    } else {
                        showAlert("Zillow Review Settings","Your Zillow Review settings have been saved.");
                        user["integrations"]["zillowreviewID"] = _query["zillowreviewID"];
                    }

                    if (user["integrations"]["zillowreviewAutoClose"] == "YES") {
                        $("#edit-zillowreviewAutoClose").prop( "checked", true );
                    } else {
                        $("#edit-zillowreviewAutoClose").prop( "checked", false );
                    }
                    $("#edit-zillowreviewID").val(user["integrations"]["zillowreviewID"]);

                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not apply this code at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Clear Zillow Review Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });

    } else {
        forceLogout();
    }

}


function applyRSID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRSID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rsID"] = $("input[name='rsID']").val();
		if ($("#edit-rsAutoClose").is(':checked')) {
			_query["rsAutoClose"] = "YES";
			user["integrations"]["rsAutoClose"] = "YES";
			localStorage["rsAutoClose"] = "YES";
		} else {
			_query["rsAutoClose"] = "NO";
			user["integrations"]["rsAutoClose"] = "NO";
			localStorage["rsAutoClose"] = "NO";
		}

		loading(true, "Saving RealSatisfied Vanity Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["rsID"] == "") {
						showAlert("RealSatisfied Vanity Key Cleared","You no longer have a RS Vanity Key linked to your account.");
						user["integrations"]["rsID"] = "";
					} else {
						showAlert("RealSatisfied Settings","Your RealSatisfied settings have been saved.");
						user["integrations"]["rsID"] = _query["rsID"];
					}

					if (user["integrations"]["rsAutoClose"] == "YES") {
						$("#edit-rsAutoClose").prop( "checked", true );
					} else {
						$("#edit-rsAutoClose").prop( "checked", false );
					}
					$("#edit-rsID").val(user["integrations"]["rsID"]);

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearRSID() {

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRSID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rsID"] = "";
		$("input[name='rsID']").val("");

		loading(true, "Clearing RealSatisfied Vanity Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("RealSatisfied Vanity Key Cleared","You no longer have a RealSatisfied Vanity Key linked to your account.");
					user["integrations"]["rsID"] = "";
					$("#edit-rsID").val("");

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function startContactuallyIntegration() {
	//Check Validty of contactually Key
		checkContactuallyKey(function(){
			if($("#buckets-list option").length > 1) {
				applyContactuallyID();
			} else {
				getContactuallyBuckets(function(){
					//Apply key and bucket
					applyContactuallyID();
				})
			}
		});
}

function checkContactuallyKey(completionHandler) {
	var api_key = $("input[name='contactuallyID']").val();
	var _query = {};
	_query["fn"] = "checkKey";
	_query["apikey"] = api_key;
	var json_data = _query;
	console.log("OutBound JSON Object: "+JSON.stringify(json_data));
	loading(true);
	$.ajax({
			url: "http://spac.io/oh/webservice/1.0.10/contactually.php",
			type:"POST",
			cache:false,
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				var results = JSON.parse(data);
				if ("error" in results) {
					showAlert("Invalid API Integration Key","Sorry, your API Integration Key does not seem to be valid.");
					user["integrations"]["contactuallyID"] = "N/A";
					user["integrations"]["contactuallyBucket"] = "N/A";
					$("input[name='contactuallyID']").val("");
					$("#buckets-list").html("");
					$("#buckets-list").append("<option value='N/A'>None</option>");
				} else {
					if ($.isFunction(completionHandler)) {
						completionHandler();
					}
				}
				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
}

function getContactuallyBuckets(completionHandler) {
	var _query = {};
	_query["fn"] = "getContactuallyBuckets";
	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];

	var json_data = _query;
	//console.log("OutBound JSON Object: "+JSON.stringify(json_data));
	console.log("Retrieving Contactually Buckets...");
	loading(true, "Getting your bucket list...");
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				//console.log("Returned JSON Object: "+JSON.stringify(data));
				console.log("Contactually Buckets Retrieved.")
				loading(false);
				var buckets = data["buckets"];


				$("#buckets-list").html("");
				$("#buckets-list").append("<option value='N/A'>Select a Bucket (Optional)</option>");
				$.each(buckets,function(index,value){
						$("#buckets-list").append("<option value='"+value.id+"'>"+value.name+"</option>");
				});

				if ($.isFunction(completionHandler)) {
					completionHandler();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Bucket Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
}

function applyContactuallyID(token, completionHandler) {
	var api_key = $("input[name='contactuallyID']").val();

	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyContactuallyID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["contactuallyID"] =	token;
		_query["contactuallyBucket"] =	"";

		loading(true, "Saving Contactually Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					//showAlert("Contactually Settings","Your Contactually settings have been saved.");
					user["integrations"]["contactuallyID"] = _query["contactuallyID"];
					user["integrations"]["contactuallyBucket"] = _query["contactuallyBucket"];

					$("input[name='contactuallyID']").val(user["integrations"]["contactuallyID"]);
					getContactuallyBuckets(function(){
						$("select[name='contactuallyBucket']").val(user["integrations"]["contactuallyBucket"]);
						$("#contactually-pre").hide();
						$("#contactually-post").show();
						showIntegrationsPanel();
						toggleIntegration('contactually');
					});

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
	} else {
		forceLogout();
	}
}

function saveContactuallyBucket() {
	var _query = {};
	_query["fn"] = "saveContactuallyBucket";
	_query["managerID"] = user["id"];
	_query["vtoken"] = user["vtoken"];
	_query["bucketID"] = $("select[name='contactuallyBucket']").val();
	var json_data = _query;
	console.log("OutBound JSON Object: "+JSON.stringify(json_data));

	loading(true, "Saving your bucket...");
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){
				console.log("Returned JSON Object: "+JSON.stringify(data));
				user["integrations"]["contactuallyBucket"] = _query["bucketID"];
				loading(false);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Save Bucket Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

}

function clearContactuallyID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyContactuallyID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["contactuallyID"] =	"";
		_query["contactuallyBucket"] =	"";

			loading(true, "Clearing Contactually Settings...");
			var json_data = _query;
			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){

					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {

						showAlert("Contactually Settings","Your Contactually settings have been cleared.");
						user["integrations"]["contactuallyID"] = "";
						user["integrations"]["contactuallyBucket"] = "";
						$("input[name='contactuallyID']").val("");
						$("#buckets-list").html("");
						$("#buckets-list").append("<option value='N/A'>None</option>");

						$("#contactually-post").hide();
						$("#contactually-pre").show();

					} else {
						//console.log("Could not logout on server! Force Logout");
						//showAlert("Error","We could not apply this code at the moment. Please contact support.");
					}

					loading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});
	} else {
		forceLogout();
	}

}

function applyTPID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyTPID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["topproducerID"] =	$("input[name='topproducerID']").val();

		loading(true, "Saving Top Producer Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["topproducerID"] == "") {
						showAlert("Top Producer Settings","Your Top Producer settings have been cleared.");
						user["integrations"]["topproducerID"] = "";
					} else {
						showAlert("Top Producer Settings","Your Top Producer settings have been saved.");
						user["integrations"]["topproducerID"] = _query["topproducerID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyRealtyJugglerID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRealtyJugglerID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["realtyjugglerID"] =	$("input[name='realtyjugglerID']").val();

		loading(true, "Saving RealtyJuggler Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["realtyjugglerID"] == "") {
						showAlert("RealtyJuggler Settings","Your RealtyJuggler settings have been cleared.");
						user["integrations"]["realtyjugglerID"] = "";
					} else {
						showAlert("RealtyJuggler Settings","Your RealtyJuggler settings have been saved.");
						user["integrations"]["realtyjugglerID"] = _query["realtyjugglerID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyFollowUpBossID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyFollowUpBossID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["followupbossID"] =	$("input[name='followupbossID']").val();

		loading(true, "Saving Follow Up Boss API Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["followupbossID"] == "") {
						showAlert("Follow Up Boss Settings","Your Follow Up Boss settings have been cleared.");
						user["integrations"]["followupbossID"] = "";
					} else {
						showAlert("Follow Up Boss Settings","Your Follow Up Boss settings have been saved.");
						user["integrations"]["followupbossID"] = _query["followupbossID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearFollowUpBossID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyFollowUpBossID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["followupbossID"] =	"";

		loading(true, "Saving Follow Up Boss API Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["followupbossID"] == "") {
						showAlert("Follow Up Boss Settings","Your Follow Up Boss settings have been cleared.");
						user["integrations"]["followupbossID"] = "";
					} else {
						showAlert("Follow Up Boss Settings","Your Follow Up Boss settings have been saved.");
						user["integrations"]["followupbossID"] = _query["followupbossID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyWiseAgentID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyWiseAgentID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["wiseagentID"] =	$("input[name='wiseagentID']").val();

		loading(true, "Saving Wise Agent API Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["wiseagentID"] == "") {
						showAlert("Wise Agent Settings","Your Wise Agent  settings have been cleared.");
						user["integrations"]["wiseagentID"] = "";
					} else {
						showAlert("Wise Agent  Settings","Your Wise Agent  settings have been saved.");
						user["integrations"]["wiseagentID"] = _query["wiseagentID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearWiseAgentID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyWiseAgentID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["wiseagentID"] =	"";

		loading(true, "Saving Wise Agent API Key...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["wiseagentID"] == "") {
						showAlert("Wise Agent Settings","Your Wise Agent settings have been cleared.");
						user["integrations"]["wiseagentID"] = "";
					} else {
						showAlert("Wise Agent  Settings","Your Wise Agent settings have been saved.");
						user["integrations"]["wiseagentID"] = _query["wiseagentID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyMailchimpID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyMailchimpID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["mailchimp_key"] =	$("input[name='mailchimp_key']").val();
		_query["mailchimp_username"] =	$("input[name='mailchimp_username']").val();
		console.log("Outbound JSON Object: "+JSON.stringify(_query));
		loading(true, "Saving Mailchimp Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				   if(_query["mailchimp_key"] == "" || _query["mailchimp_username"] == "") {
						showAlert("Mailchimp Settings","Your Mailchimp settings have been cleared.");
						user["integrations"]["mailchimp_key"] = "";
						user["integrations"]["mailchimp_username"] = "";

				   } else {
						showAlert("Mailchimp Settings","Your Mailchimp Settings have been saved.");
						user["integrations"]["mailchimp_key"] = _query["mailchimp_key"];
						user["integrations"]["mailchimp_username"] = _query["mailchimp_username"];
				   }
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function clearMailchimpID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyMailchimpID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["mailchimp_key"] = "";
		_query["mailchimp_username"] = "";
		console.log("Outbound JSON Object: "+JSON.stringify(_query));
		loading(true, "Saving Mailchimp Settings...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				   if(_query["mailchimp_key"] == "" || _query["mailchimp_username"] == "") {
						showAlert("Mailchimp Settings","Your Mailchimp settings have been cleared.");
						user["integrations"]["mailchimp_key"] = "";
						user["integrations"]["mailchimp_username"] = "";
						$("input[name='mailchimp_key']").val("");
						$("input[name='mailchimp_username']").val("");

				   } else {
						showAlert("Mailchimp Settings","Your Mailchimp Settings have been saved.");
						user["mailchimp_key"] = _query["mailchimp_key"];
						user["mailchimp_username"] = _query["mailchimp_username"];
				   }
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function applyBTID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyBTID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["boomtownID"] =	$("input[name='boomtownID']").val();

		loading(true, "Saving Boomtown Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				   if(_query["boomtownID"] == "") {
						showAlert("BoomTown Settings","Your BoomTown settings have been cleared.");
						user["integrations"]["boomtownID"] = "";
				   } else {
						showAlert("BoomTown Settings","Your BoomTown Settings have been saved.");
						user["integrations"]["boomtownID"] = _query["boomtownID"];
				   }
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function applyRenthopID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRenthopID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["renthopID"] = $("input[name='renthopID']").val();

		loading(true, "Saving Renthop Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned RH JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["renthopID"] == "") {
						showAlert("RentHop and RealtyHop Settings","Your RentHop / RealtyHop settings have been cleared.");
						user["integrations"]["renthopID"] = "";
					} else {
						if (data.count == 0) {
							showAlert("RentHop and RealtyHop Settings","Sorry, we did not find any listings attached to this email.");
						} else {
							showAlert("RentHop and RealtyHop Settings","Your RentHop / RealtyHop listings have been added.");
							reloadProperties();
						}
						user["integrations"]["renthopID"] = _query["renthopID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearRenthopID() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRenthopID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["renthopID"] = "";

		loading(true, "Saving Renthop Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["renthopID"] == "") {
						showAlert("RentHop and RealtyHop Settings","Your RentHop / RealtyHop settings have been cleared.");
						user["integrations"]["renthopID"] = "";
						$("input[name='renthopID']").val("");
					} else {

						user["integrations"]["renthopID"] = _query["renthopID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearRealtyJugglerID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyRealtyJugglerID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["realtyjugglerID"] =	"";

		loading(true, "Saving RealtyJuggler Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["realtyjugglerID"] == "") {
						showAlert("RealtyJuggler Settings","Your RealtyJuggler settings have been cleared.");
						user["integrations"]["realtyjugglerID"] = "";
					} else {
						showAlert("RealtyJuggler Settings","Your RealtyJuggler settings have been saved.");
						user["integrations"]["realtyjugglerID"] = _query["realtyjugglerID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function applyMSID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyMSID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["marketsnapID"] = $("input[name='marketsnapID']").val();

		loading(true, "Saving Market Snapshot Email...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					if(_query["topproducerID"] == "") {
						showAlert("Market Snapshot Settings","Your Market Snapshot settings have been cleared.");
						user["integrations"]["marketsnapID"] = "";
					} else {
						showAlert("Market Snapshot Settings","Your Market Snapshot settings have been saved.");
						user["integrations"]["marketsnapID"] = _query["marketsnapID"];
					}

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearBTID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyBTID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["boomtownID"] = "";
		$("input[name='boomtownID']").val("");

		loading(true, "Clearing BoomTown Integration...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if(_query["boomtownID"] == "") {
					showAlert("BoomTown Settings","Your BoomTown settings have been cleared.");
					user["integrations"]["boomtownID"] = "";
				} else {
					showAlert("BoomTown Settings","Your BoomTown Settings have been saved.");
					user["integrations"]["boomtownID"] = _query["boomtownID"];
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}



function clearTPID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyTPID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["topproducerID"] = "";
		$("input[name='topproducerID']").val("");

		loading(true, "Clearing Top Producer Integration...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Top Producer Settings","Your Top Producer settings have been cleared.");
					user["integrations"]["topproducerID"] = "";

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this ID. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function clearMSID() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyMSID";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["marketsnapID"] = "";
		$("input[name='marketsnapID']").val("");

		loading(true, "Clearing Market Snapshot Integration...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

					showAlert("Market Snapshot Settings","Your Market Snapshot settings have been cleared.");
					user["integrations"]["topproducerID"] = "";

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not clear the ID. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function editOLR() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editOLR";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-olrEnabled").is(':checked')) {
			_query["olrEnabled"] = "YES";
		} else {
			_query["olrEnabled"] = "NO";
		}
		loading(true, "Saving OLR Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editTouchCMA() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editTouchCMA";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-touchcmaEnabled").is(':checked')) {
			_query["touchcmaEnabled"] = "YES";
		} else {
			_query["touchcmaEnabled"] = "NO";
		}
		loading(true, "Saving TouchCMA Setting...");
        console.log("Outbound JSON Object: "+JSON.stringify(_query));
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
          user["integrations"]["touchcmaEnabled"] =_query["touchcmaEnabled"];
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit TouchCMA Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editMoxiPresent() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editMoxiPresent";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-moxipresentEnabled").is(':checked')) {
			_query["moxipresentEnabled"] = "YES";
		} else {
			_query["moxipresentEnabled"] = "NO";
		}
		loading(true, "Saving Moxi Present Setting...");
        console.log("Outbound JSON Object: "+JSON.stringify(_query));
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
            user["integrations"]["moxipresentEnabled"] =_query["moxipresentEnabled"];
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Moxi Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editPresentationPro() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editPresentationPro";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-presentationproEnabled").is(':checked')) {
			_query["presentationproEnabled"] = "YES";
		} else {
			_query["presentationproEnabled"] = "NO";
		}
		loading(true, "Saving Presentation Pro Setting...");
        console.log("Outbound JSON Object: "+JSON.stringify(_query));
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
          user["integrations"]["presentationproEnabled"] =_query["presentationproEnabled"];
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit TouchCMA Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function editMoxi() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editMoxi";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-moxiEnabled").is(':checked')) {
			_query["moxiEnabled"] = "YES";
		} else {
			_query["moxiEnabled"] = "NO";
		}
		loading(true, "Saving Moxiworks Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Moxi Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editHomekeepr() {
    if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editHomekeepr";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-homekeeprEnabled").is(':checked')) {
			_query["homekeeprEnabled"] = "YES";
		} else {
			_query["homekeeprEnabled"] = "NO";
		}

		loading(true, "Saving Homekeepr Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
          user["integrations"]["homekeeprEnabled"] = _query["homekeeprEnabled"];
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}

function applyHomekeeprURL() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "applyHomekeeprURL";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["homekeeprURL"] = $("input[name='homekeeprURL']").val();

		loading(true, "Saving Homekeepr URL...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

                    showAlert("Homekeepr URL","Your HomeKeepr URL has been saved.");
                    user["integrations"]["homekeeprURL"] = _query["homekeeprURL"];

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not apply this code at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Property Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}

function editIDC() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editIDC";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-idcEnabled").is(':checked')) {
			_query["idcEnabled"] = "YES";
		} else {
			_query["idcEnabled"] = "NO";
		}
		loading(true, "Saving IDC Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editAgency() {
	if (user["id"] != null && user["vtoken"] != null )	{

        if("agencyID" in user["integrations"]) {
		var _query = {};
		_query["fn"] = "editAgency";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-agencyEnabled").is(':checked')) {
			_query["agencyEnabled"] = "YES";
		} else {
			_query["agencyEnabled"] = "NO";
		}
		loading(true, "Saving Salesforce Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
        } else {
            showAlert("Integration Unavailable","This integration is for brokerage customers only. If you would like your brokerage setup, please contact support@spac.io.");

        }

	} else {
		forceLogout();
	}
}

function editBay() {
    if (user["id"] != null && user["vtoken"] != null )    {

        var _query = {};
        _query["fn"] = "editBay";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        if($("#edit-bayEnabled").is(':checked')) {
            _query["bayEnabled"] = "YES";
        } else {
            _query["bayEnabled"] = "NO";
        }

        loading(true, "Saving BAY Setting...");
        var json_data = _query;
        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {
                    user["integrations"]["bayEnabled"] = _query["bayEnabled"];
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not change this setting at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });


    } else {
        forceLogout();
    }

}

function editKvcore() {
    if (user["id"] != null && user["vtoken"] != null )    {

        var _query = {};
        _query["fn"] = "editKvcore";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        if($("#edit-kvcoreEnabled").is(':checked')) {
            _query["kvcoreEnabled"] = "YES";
        } else {
            _query["kvcoreEnabled"] = "NO";
        }

        loading(true, "Saving kvCORE Setting...");
        var json_data = _query;
        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {
                    user["integrations"]["kvcoreEnabled"] = _query["kvcoreEnabled"];
                } else if(data.status == 2){
                    showAlert("Integration Unavailable","This integration is available for brokerage customers only.");
                    $("#edit-kvcoreEnabled").prop( "checked", false );
                    user["integrations"]["kvcoreEnabled"] = "NO";
                } else if(data.status == 3){
                    showAlert("Integration Unavailable","This integration is not available for your brokerage.");
                    $("#edit-kvcoreEnabled").prop( "checked", false );
                    user["integrations"]["kvcoreEnabled"] = "NO";
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not change this setting at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });
    } else {
        forceLogout();
    }

}

function editBoost() {
    if (user["id"] != null && user["vtoken"] != null )    {

        var _query = {};
        _query["fn"] = "editBoost";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        if($("#edit-boostEnabled").is(':checked')) {
            _query["boostEnabled"] = "YES";
        } else {
            _query["boostEnabled"] = "NO";
        }

        loading(true, "Saving Boost Setting...");
        var json_data = _query;
        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {
                    user["integrations"]["boostEnabled"] = _query["boostEnabled"];
                } else if(data.status == 2){
                    showAlert("Integration Unavailable","This integration is available for brokerage customers only.");
                    $("#edit-boostEnabled").prop( "checked", false );
                    user["integrations"]["boostEnabled"] = "NO";
                } else if(data.status == 3){
                    showAlert("Integration Unavailable","This integration is not available for your brokerage.");
                    $("#edit-boostEnabled").prop( "checked", false );
                    user["integrations"]["boostEnabled"] = "NO";
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not change this setting at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });


    } else {
        forceLogout();
    }

}

function editBostonlogic() {
    if (user["id"] != null && user["vtoken"] != null )    {

        var _query = {};
        _query["fn"] = "editBostonlogic";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        if($("#edit-bostonlogicEnabled").is(':checked')) {
            _query["bostonlogicEnabled"] = "YES";
        } else {
            _query["bostonlogicEnabled"] = "NO";
        }

        loading(true, "Saving Boost Setting...");
        var json_data = _query;
        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {
                    user["integrations"]["bostonlogicEnabled"] = _query["bostonlogicEnabled"];
                } else if(data.status == 2){
                    showAlert("Integration Unavailable","This integration is available for brokerage customers only.");
                    $("#edit-bostonlogicEnabled").prop( "checked", false );
                    user["integrations"]["bostonlogicEnabled"] = "NO";
                } else if(data.status == 3){
                    showAlert("Integration Unavailable","This integration is not available for your brokerage.");
                    $("#edit-bostonlogicEnabled").prop( "checked", false );
                    user["integrations"]["bostonlogicEnabled"] = "NO";
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not change this setting at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });


    } else {
        forceLogout();
    }

}

function getXpressdocsData(completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )    {

			var _query = {};
			_query["fn"] = "getXpressdocsData";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];

			loading(true, "Getting Xpressdoc Data...");
			var json_data = _query;
			$.ajax({
					url: wsURL_new,
					type:"POST",
					data: {json : json_data},
					success: function(data, textStatus, jqXHR){

							console.log("Returned JSON Object: "+JSON.stringify(data));

							if (data.status == 1) {
								if ($.isFunction(completionHandler)) {
									//alert("here");
									completionHandler(data.xpressdocsData);
								}
							} else if(data.status == 2){
								showAlert("Missing Username","You need a valid Xpressdoc Username to use this feature.");

							}

							loading(false);
					},
					error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
							loading(false);
					},
					dataType: "json"
			});

	} else {
			forceLogout();
	}
}

function getBAYURL(pid,completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )    {

			var _query = {};
			_query["fn"] = "getBAYURL";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["pid"] = pid;

			loading(true, "Getting BAY URL...");
			var json_data = _query;
			$.ajax({
					url: wsURL_new,
					type:"POST",
					data: {json : json_data},
					success: function(data, textStatus, jqXHR){

							console.log("Returned JSON Object: "+JSON.stringify(data));

							if (data.status == 1) {
								if ($.isFunction(completionHandler)) {
									completionHandler(data.bayURL);
								}
							} else if(data.status == 3){
									showAlert("Missing Information","This property does not have all the required information to be promoted.");
							}

							loading(false);
					},
					error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
							loading(false);
					},
					dataType: "json"
			});


	} else {
			forceLogout();
	}
}

function getBoostURL(pid,completionHandler) {
	if (user["id"] != null && user["vtoken"] != null )    {

			var _query = {};
			_query["fn"] = "getBoostURL";
			_query["managerID"] = user["id"];
			_query["vtoken"] = user["vtoken"];
			_query["pid"] = pid;

			loading(true, "Getting Boost URL...");
			var json_data = _query;
			$.ajax({
					url: wsURL_new,
					type:"POST",
					data: {json : json_data},
					success: function(data, textStatus, jqXHR){

							console.log("Returned JSON Object: "+JSON.stringify(data));

							if (data.status == 1) {
								if ($.isFunction(completionHandler)) {
									completionHandler(data.destURL);
								}
							} else if(data.status == 2){
								showAlert("Missing Username","You need a valid Boost Username to use this feature.");

							} else if(data.status == 3){
									showAlert("Missing Information","This property does not have all the required information to be boosted.");

							}

							loading(false);
					},
					error: function(jqXHR, textStatus, errorThrown) {
							console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
							loading(false);
					},
					dataType: "json"
			});


	} else {
			forceLogout();
	}
}

function editBooj() {
    if (user["id"] != null && user["vtoken"] != null )    {

        if(user["brokerageID"] == "lyon" || user["brokerageID"] == "spacio") {
        var _query = {};
        _query["fn"] = "editBooj";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        if($("#edit-boojEnabled").is(':checked')) {
            _query["boojEnabled"] = "YES";
        } else {
            _query["boojEnabled"] = "NO";
        }

        loading(true, "Saving Booj Setting...");
        var json_data = _query;
        $.ajax({
            url: wsURL,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {
                    user["integrations"]["boojEnabled"] = _query["boojEnabled"];
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not change this setting at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });
        } else {
            showAlert("Integration Unavailable","This integration is for Lyon Real Estate customers only. If you would like your brokerage setup, please contact support@spac.io.");
            $("#edit-boojEnabled").prop( "checked", false );
        }

    } else {
        forceLogout();
    }

}


function editCorelogic() {
	if (user["id"] != null && user["vtoken"] != null )	{

        if("corelogicID" in user["integrations"]) {
		var _query = {};
		_query["fn"] = "editCorelogic";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-corelogicEnabled").is(':checked')) {
			_query["corelogicEnabled"] = "YES";
		} else {
			_query["corelogicEnabled"] = "NO";
		}

		loading(true, "Saving CoreLogic Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
                    user["integrations"]["corelogicEnabled"] = _query["corelogicEnabled"];
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
        } else {
            showAlert("Integration Unavailable","This integration is for brokerage customers only. If you would like your brokerage setup, please contact support@spac.io.");
            $("#edit-corelogicEnabled").prop( "checked", false );
        }

	} else {
		forceLogout();
	}

}

function editRealScoutEnableOptIn() {
    if (user["id"] != null && user["vtoken"] != null )    {

        if($("#edit-realscoutEmail").val() == "") {
            $("#edit-realscoutEnableOptIn").prop( "checked", false );
            showAlert("Need RealScout Email","You must enter a valid RealScout email to enable Visitor Opt In.");
        } else {
            var _query = {};
            _query["fn"] = "editRealScoutEnableOptIn";
            _query["managerID"] = user["id"];
            _query["vtoken"] = user["vtoken"];

            if($("#edit-realscoutEnableOptIn").is(':checked')) {
                _query["realscoutEnableOptIn"] = "YES";
            } else {
                _query["realscoutEnableOptIn"] = "NO";
            }
            loading(true, "Saving RealScout Setting...");
            var json_data = _query;
            $.ajax({
                url: wsURL,
                type:"POST",
                data: {json : json_data},
                success: function(data, textStatus, jqXHR){

                    console.log("Returned JSON Object: "+JSON.stringify(data));

                    if (data.status == 1) {
                        user["integrations"]["realscoutEnableOptIn"] =_query["realscoutEnableOptIn"];
                    } else {
                        //console.log("Could not logout on server! Force Logout");
                        showAlert("Error","We could not change this setting at the moment. Please contact support.");
                    }

                    loading(false);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
                    loading(false);
                },
                dataType: "json"
            });

        }

    } else {
        forceLogout();
    }

}


function editREW() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editREW";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-rewEnabled").is(':checked')) {
			_query["rewEnabled"] = "YES";
		} else {
			_query["rewEnabled"] = "NO";
		}
		loading(true, "Saving REW Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editDocusign() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editDocusign";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-docusignEnabled").is(':checked')) {
			_query["docusignEnabled"] = "YES";
		} else {
			_query["docusignEnabled"] = "NO";
		}
		loading(true, "Saving DocuSign Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
                    user["integrations"]["docusignEnabled"] = _query["docusignEnabled"];
				} else if(data.status == 2){
                    showAlert("Integration Unavailable","This integration is not available for your brokerage.");
                    $("#edit-docusignEnabled").prop( "checked", false );
                    user["integrations"]["docusignEnabled"] = "NO";
                } else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Tribus Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editDocusignAuto() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editDocusignAuto";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-docusignAutoEnabled").is(':checked')) {
			_query["docusignAutoEnabled"] = "YES";
		} else {
			_query["docusignAutoEnabled"] = "NO";
		}
		loading(true, "Saving DocuSign Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
                    user["integrations"]["docusignAutoEnabled"] = _query["docusignAutoEnabled"];
				} else if(data.status == 2){
                    showAlert("Option Unavailable","You must have DocuSign enabled first.");
                    $("#edit-docusignAutoEnabled").prop( "checked", false );
                    user["integrations"]["docusignAutoEnabled"] = "NO";
                } else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Tribus Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editXpressdocs() {
    if (user["id"] != null && user["vtoken"] != null )    {

        var _query = {};
        _query["fn"] = "editXpressdocs";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];

        if($("#edit-xpressdocsEnabled").is(':checked')) {
            _query["xpressdocsEnabled"] = "YES";
        } else {
            _query["xpressdocsEnabled"] = "NO";
        }

        loading(true, "Saving Xpressdocs Setting...");
        var json_data = _query;
        $.ajax({
            url: wsURL_new,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){

                console.log("Returned JSON Object: "+JSON.stringify(data));

                if (data.status == 1) {
                    user["integrations"]["xpressdocsEnabled"] = _query["xpressdocsEnabled"];
                } else if(data.status == 2){
                    showAlert("Integration Unavailable","This integration is available for brokerage customers only.");
                    $("#edit-xpressdocsEnabled").prop( "checked", false );
                    user["integrations"]["xpressdocsEnabled"] = "NO";
                } else if(data.status == 3){
                    showAlert("Integration Unavailable","This integration is not available for your brokerage.");
                    $("#edit-xpressdocsEnabled").prop( "checked", false );
                    user["integrations"]["xpressdocsEnabled"] = "NO";
                } else {
                    //console.log("Could not logout on server! Force Logout");
                    showAlert("Error","We could not change this setting at the moment. Please contact support.");
                }

                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit REW Error: "+errorThrown);
                loading(false);
            },
            dataType: "json"
        });


    } else {
        forceLogout();
    }

}

function editDocs() {
	if (user["id"] != null && user["vtoken"] != null )	{
		if(user["type"] == "brokerage" && "docs" in brokerage) {
		var _query = {};
		_query["fn"] = "editDocs";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-docsEnabled").is(':checked')) {
			_query["docsEnabled"] = "YES";
		} else {
			_query["docsEnabled"] = "NO";
		}

		loading(true, "Saving Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL_new,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
                    user["integrations"]["docsEnabled"] = _query["docsEnabled"];
                    if(_query["docsEnabled"] == "NO") {
                       $("#edit-docsAuto").prop( "checked", false );
                       user["integrations"]["docsAuto"] = "NO";
                    } else {
                        showAlert("E-Signature Enabled","This feature requires wifi connection.");
                    }
				} else if(data.status == 2){
                    showAlert("Integration Unavailable","This integration is available for brokerage customers only.");
                    $("#edit-docsEnabled").prop( "checked", false );
                    $("#edit-docsAuto").prop( "checked", false );
                    user["integrations"]["docsEnabled"] = "NO";
                    user["integrations"]["docsAuto"] = "NO";

                } else if(data.status == 3){
                    showAlert("Integration Unavailable","This integration is not available for your brokerage.");
                    $("#edit-docsEnabled").prop( "checked", false );
                    $("#edit-docsAuto").prop( "checked", false );
                    user["integrations"]["docsEnabled"] = "NO";
                    user["integrations"]["docsAuto"] = "NO";
                } else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Tribus Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});
		} else {
            $("#edit-docsEnabled").prop( "checked", false );
            showAlert("Integration Unavailable","This integration is for brokerage customers only. If you would like your brokerage setup, please contact support@spac.io.");

        }
	} else {
		forceLogout();
	}


}

function editDocsAuto() {
	if (user["id"] != null && user["vtoken"] != null )	{
		if(user["type"] == "brokerage" && "docs" in brokerage) {
		var _query = {};
		_query["fn"] = "editDocsAuto";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-docsAuto").is(':checked')) {
			_query["docsAuto"] = "YES";
		} else {
			_query["docsAuto"] = "NO";
		}

		loading(true, "Saving Setting...");

		var json_data = _query;
        console.log("OUTBOUND JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
                    user["integrations"]["docsAuto"] = _query["docsAuto"];
				} else if(data.status == 2){
                    showAlert("Option Unavailable","You must have E-Signature enabled first.");
                    $("#edit-docsAuto").prop( "checked", false );
                    user["integrations"]["docsAuto"] = "NO";
                } else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Tribus Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
    } else {
        showAlert("Integration Unavailable","This integration is for brokerage customers only. If you would like your brokerage setup, please contact support@spac.io.");
    }

}

function editReliance() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editReliance";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-relianceEnabled").is(':checked')) {
			_query["relianceEnabled"] = "YES";
		} else {
			_query["relianceEnabled"] = "NO";
		}
		loading(true, "Saving Reliance Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Tribus Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function editTribus() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editTribus";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];

		if($("#edit-tribusEnabled").is(':checked')) {
			_query["tribusEnabled"] = "YES";
		} else {
			_query["tribusEnabled"] = "NO";
		}
		loading(true, "Saving Tribus Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit Tribus Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}


function editSendToRepresented(integration) {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "editSendToRepresented";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["integration"] = integration;

		if($("#"+integration+"SendToRepresented").is(':checked')) {
			_query["sendToRepresented"] = "YES";
		} else {
			_query["sendToRepresented"] = "NO";
		}
		loading(true, "Saving Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {

				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","We could not change this setting at the moment. Please contact support.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit sendToRepresented Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function launchDocusign(evt,index) {
    evt.stopPropagation();
        var _query = {};
        _query["fn"] = "getDocusignURL";
        _query["managerID"] = user["id"];
        _query["vtoken"] = user["vtoken"];
        _query["rid"] = index;
        var json_data = _query;
        console.log("Outbound JSON Query: "+ JSON.stringify(json_data));
        loading(true, "Launching Docusign...");

        $.ajax({
            url: wsURL,
            type:"POST",
            data: {json : json_data},
            success: function(data, textStatus, jqXHR){
                console.log("Returned JSON Object: "+JSON.stringify(data));
                if (data.status == 1) {

                    if("docusign" in data) {
											$(".docusign-iframe-overlay").show();
											$("#docusign-iframe").attr("src", data.docusign["docusign_url"]);
											//var win = window.open(data.docusign["docusign_url"], '_blank');

                        //openURL(data.docusign["docusign_url"]);
                    } else {
                    }

                } else if (data.status == 2){

                } else {
                    //alert("Succesfully Registered");
                }
                loading(false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Register For Property Error: "+errorThrown);

                loading(false);
            },
            dataType: "json"
        });


}

function resyncContactuallyLead() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "resyncContactuallyLead";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rid"] = curRID;

		loading(true, "Saving Setting...");
		var json_data = _query;

		console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("Visitor Info Resynced","Visitor information has successfully added to Contactually.");
					registrants[curRID]["log"]["contactuallyStatus"] = data.contactuallyStatus;
					showRegistrant(curRID);
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","Could not add visitor information to Contactually.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error:Resync Contactually Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}

function resyncCorelogicLead() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "resyncCorelogicLead";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rid"] = curRID;

		loading(true, "Saving Setting...");
		var json_data = _query;

		console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("Visitor Info Resynced","Visitor information has successfully added to Agent Achieve.");
					registrants[curRID]["log"]["corelogicStatus"] = data.corelogicStatus;
					showRegistrant(curRID);
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","Could not add visitor information to Agent Achieve.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error:Resync IDC Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}

function resyncBoojLead() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "resyncBoojLead";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rid"] = curRID;

		loading(true, "Saving Setting...");
		var json_data = _query;

		console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("Visitor Info Resynced","Visitor information has successfully added to Lyon Connect.");
					registrants[curRID]["log"]["boojStatus"] = data.boojStatus;
					showRegistrant(curRID);
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","Could not add visitor information to Lyon Connect.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error:Resync Booj Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}


function resyncIDCLead() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "resyncIDCLead";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rid"] = curRID;

		loading(true, "Saving Setting...");
		var json_data = _query;

		console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("Visitor Info Resynced","Visitor information has successfully added to IDC.");
					registrants[curRID]["log"]["idcStatus"] = data.idcStatus;
					showRegistrant(curRID);
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","Could not add visitor information to IDC.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error:Resync IDC Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}
}
function resyncFollowupbossLead() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "resyncFollowupbossLead";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rid"] = curRID;


		loading(true, "Saving Setting...");
		var json_data = _query;

		console.log("Outbound JSON Object: "+JSON.stringify(json_data));

		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("Visitor Info Resynced","Visitor information has successfully added to Follow Up Boss.");
					registrants[curRID]["log"]["followupbossStatus"] = data.followupbossStatus;
					showRegistrant(curRID);
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","Could not add visitor information to Follow Up Boss.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error:Resync Followupboss Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}

function resyncMoxiLead() {
	if (user["id"] != null && user["vtoken"] != null )	{

		var _query = {};
		_query["fn"] = "moxiResyncLead";
		_query["managerID"] = user["id"];
		_query["vtoken"] = user["vtoken"];
		_query["rid"] = curRID;

		loading(true, "Saving Setting...");
		var json_data = _query;
		$.ajax({
			url: wsURL,
			type:"POST",
			data: {json : json_data},
			success: function(data, textStatus, jqXHR){

				console.log("Returned JSON Object: "+JSON.stringify(data));

				if (data.status == 1) {
					showAlert("Visitor Info Resynced","Visitor information has successfully added to Moxi Engage.");
					registrants[curRID]["log"]["moxiEngageStatus"] = data.moxiEngageStatus;
					showRegistrant(curRID);
				} else {
					//console.log("Could not logout on server! Force Logout");
					showAlert("Error","Could not add visitor information to Moxi Engage.");
				}

				loading(false);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Edit sendToRepresented Error: "+errorThrown);
				loading(false);
			},
			dataType: "json"
		});

	} else {
		forceLogout();
	}

}
