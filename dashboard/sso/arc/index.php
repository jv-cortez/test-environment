<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

  require_once('../../../oh/config/db_conn.php');
  require_once('../../../oh/require/core.php');


  $collection_managers = Db_Conn::getInstance()->getConnection()->managers;
  $collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;

  foreach ($_POST as $key => $value) {
      if($key == "UserID") {
        $userID = (int)$value;
      }

      if($key == "UserEmail") {
        $userEmail = $value;
      }
    }

		$userEmail = strtolower($userEmail);
		$userEmail = trim($userEmail);

  //  $userEmail = "george.maynes@foxroach.com";

  $manager_result = $collection_managers->findOne(array('email' => $userEmail));

	if($userEmail != "") {
		if(!is_null($manager_result)) {

	    if($manager_result["integrations"]["corelogicID"] == $userID) {
	      $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	      $charsLength = strlen($chars);
	      $atoken = '';
	       for ($i = 0; $i < 16; $i++) {
	           $atoken .= $chars[rand(0, $charsLength - 1)];
	       }

	       $newdata = array('$set' => array("accessToken" => $atoken));
	       $u_result = $collection_managers->update(array("_id" => $manager_result["_id"]), $newdata);

	       $redirectLink = "https://spac.io/dashboard/sso/?e=".$userEmail."&a=".$atoken;
	    } else {
	      $redirectLink = "https://spac.io/dashboard/notfound.html";
	    }
	  } else {
	    $unclaimed_result = $collection_unclaimed_accounts->findOne(array('$or' => array(array('email' => $userEmail),array('email2' => $userEmail)), 'status'=>'unclaimed','active' => array('$ne' => false)));

	      if(!is_null($unclaimed_result)) {
					$screen_content = "<div style='font-size:20px;line-height:1.2;width:400px;'>You have not activated your Spacio account yet.<br/><br/>Please go to <a href='https://spac.io/dashboard' target='_blank' style='display:inline;text-decoration:underline;'>spac.io/dashboard</a>, click on <b>Login with Partners SSO -> Login with ARC</b> to activate your Spacio account for the first time.</div>";
	        // $_id = $unclaimed_result["_id"]->{'$id'};
					//
	        // $managerID = "570b6da5d4c6cd2e97fc74f0";
	        // $redirectLink = "https://spac.io/claim/?e=".$userEmail."&u=".$_id."&f=".$managerID."&b=".$unclaimed_result["brokerageID"];

	      } else {
	        $redirectLink = "https://spac.io/dashboard/notfound.html";

	        //$auth->redirectTo("https://spac.io/dashboard/notfound.html");
	      }
	  }
	} else {

			$screen_content = "Unknown Error.<br/>Please contact support@spac.io";
	}


?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Introducing the Paperless Open House</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <style>
    .loading-ellipsis {
    }

    .loading-ellipsis:after {
      overflow: hidden;
      display: inline-block;
      vertical-align: bottom;
      -webkit-animation: ellipsis steps(4,end) 900ms infinite;
      animation: ellipsis steps(4,end) 900ms infinite;
      content: "\2026"; /* ascii code for the ellipsis character */
      width: 0px;
    }

    @keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes ellipsis {
      to {
        width: 1.25em;
      }
    }

    @-webkit-keyframes side-pan {
        0% {background-position: left center;}
      50% {background-position: right center;}
      100% {background-position: left center;}
    }
    </style>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
	 <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
    <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->
    <div style="height:100vh;width:100%;font-size:30px;background:#eee;display:flex;flex-direction:column;align-items:center;justify-content:center;">

			<?php echo $screen_content;?>
      <!-- <div style="font-size:40px;width:400px" class="loading-ellipsis">
        Logging into Spacio
      </div>
      <div style="font-size:20px;width:600px;display:none;line-height:1.4" class="error-message">
      </div> -->
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
		<script src="/assets/js/jquery.countdown.min.js"></script>
    <script>

    var wsURL = "https://spac.io/oh/webservice/1.1.6/webservice.php";


    $(document).ready(function(){
      window.location.href = "<?php echo $redirectLink; ?>";
    });



    </script>

  </body>
