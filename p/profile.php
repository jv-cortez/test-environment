<?php

	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
  require_once('../oh/config/db_conn.php');
  require_once('../oh/require/core.php');

	$ukey = $_GET["id"];

  $collection_managers = Db_Conn::getInstance()->getConnection()->managers;

  $manager_result = $collection_managers->findOne(array('ukey' => $ukey));

  list($width, $height) = getimagesize($manager_result["pphoto"]);

?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Facebook meta tags -->
  <meta property="fb:app_id" content="255792991253800" />
  <meta property="og:url" content="http://spac.io/p/<?php echo $ukey ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $manager_result["fname"] .' ' . $manager_result["lname"] ?>" />
  <meta property="og:description" content="<?php echo $manager_result["brokerage"] ?>" />
  <meta property="og:image" content="<?php echo $manager_result["pphoto"] ?>" />
  <meta property="og:site_name" content="Spacio: Introducing the Paperless Open House" />
  <meta property="og:image:height" content="<?php echo $height ?>" />
  <meta property="og:image:width" content="<?php echo $width ?>" />

  <!-- script for the twitter crawler -->
  <script>window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
      t._e.push(f);
    };

    return t;
  }(document, "script", "twitter-wjs"));</script>

  <!-- Twitter meta tags -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="<?php echo $manager_result["fname"] .' ' . $manager_result["lname"] ?>" />
  <meta name="twitter:description" content="<?php echo $manager_result["title"] ?>" />
  <meta name="twitter:image" content="<?php echo $manager_result["pphoto"] ?>" />

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title><?php echo $manager_result["fname"] .' ' . $manager_result["lname"]  ?></title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/profile.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">
		<link href="/dashboard/css/font-awesome.min.css" rel="stylesheet">


    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>

     <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->
    <div class="brokerage-logo">

    </div>

    <div class="profile-scroll-one">
      <div class="padded-content">
        <div class="agent-pphoto"></div>
        <div class="agent-name"></div>
        <div class="agent-title"></div>

        <div class="agent-email"></div>
        <div class="agent-phone"></div>
        <div class="agent-mobile"></div>
        <div class="agent-website"></div>

				<!-- social media block -->
				<div class="agent-socialMedia-container">
					<div class="agent-facebook"></div>
					<div class="agent-twitter"></div>
					<div class="agent-instagram"></div>
					<div class="agent-linkedin"></div>
				</div>

      </div>
      <div class="agent-properties">

      </div>


    </div>
    <a href="https://spac.io" target="_blank"><div class="powered-by-spacio">
      POWERED BY <img height="12" src="/assets/img/website/spacio_logo_short.png" style="position:relative;top:1px;"> SPACIO
    </div></a>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script src="/assets/js/sha3.js"></script>
    <script>
    var wsURL = "https://ws.spac.io/prod/oh/webservice_agent/1.0.0/webservice_agent.php";
    var listings = [];
    var agent = {};
    $(document).ready(function(){
      insertAlertBox();
      getAgentInfo("<?php echo $ukey; ?>", function(){
        loadAgentInfo();
      });
    });

    function getAgentInfo(ukey, completionHandler) {
      var _query = {};

			_query["fn"] = "getAgentInfo";
			_query["agentID"] = ukey;

			loading(true);

			var json_data = _query;
			console.log("Outbound JSON Query: "+ JSON.stringify(json_data));

			$.ajax({
				url: wsURL,
				type:"POST",
				data: {json : json_data},
				success: function(data, textStatus, jqXHR){
					console.log("Returned JSON Object: "+JSON.stringify(data));

					if (data.status == 1) {
						console.log("Returned JSON Object: "+JSON.stringify(data));
						loading(false);
            agent = data;
            listings = data.properties;
            console.log(listings.length);
						if ($.isFunction(completionHandler)) {
							completionHandler();
						}
					} else {
						forceLogout();
						loading(false);
					}

				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Get Properties Error: "+errorThrown);
					loading(false);
				},
				dataType: "json"
			});

    }

    function loadAgentInfo() {

      if(agent["phone"] == "") {
        $(".agent-phone").hide();
      }
      if(agent["mobile"] == "") {
        $(".agent-mobile").hide();
      }
      if(agent["website"] == "") {
        $(".agent-website").hide();
      } else {
				agent["website"] = sanitizeURL(agent["website"]);
			}
			if(agent["facebook"] == "") {
				$(".agent-facebook").hide();
			} else {
				agent["facebook"] = sanitizeURL(agent["facebook"]);
			}

			if(agent["twitter"] == "") {
				$(".agent-twitter").hide();
			} else {
				agent["twitter"] = sanitizeURL(agent["twitter"]);
			}

			if(agent["instagram"] == "") {
				$(".agent-instagram").hide();
			} else {
				agent["instagram"] = sanitizeURL(agent["instagram"]);
			}

			if(agent["linkedin"] == "") {
				$(".agent-linkedin").hide();
			} else {
				agent["linkedin"] = sanitizeURL(agent["linkedin"]);
			}



      $(".agent-pphoto").css("background-image","url("+agent["pphoto"]+")");
      $(".agent-name").html(agent["fname"]+" "+agent["lname"]);
      $(".agent-title").html(agent["title"]);

      $(".agent-email").html('<a href="mailto:'+agent["email"]+'"><div class="listing-icon"><i class="fa fa-envelope main-color-font" aria-hidden="true"></i></div>'+agent["email"]+'</a>');
      $(".agent-phone").html('<a href="tel:'+agent["phone"]+'"><div class="listing-icon"><i class="fa fa-phone main-color-font" aria-hidden="true"></i></div>'+agent["phone"]+'</a>');
      $(".agent-mobile").html('<a href="tel:'+agent["mobile"]+'"><div class="listing-icon" style="font-size:16px;"><i class="fa fa-mobile main-color-font" aria-hidden="true"></i></div>'+agent["mobile"]+'</a>');
      $(".agent-properties").html("");
      $(".agent-website").html('<div class="listing-icon"><i class="fa fa-user-circle-o main-color-font" aria-hidden="true"></i></div> <a href="'+agent["website"]+'" target="_blank"><u>Agent Website</u></a>');
			// social media black
			$(".agent-facebook").html('<a href="'+agent["facebook"]+'" target="_blank"><div class="listing-icon" style="font-size:24px;"><i class="fa fa-facebook-official" aria-hidden="true"></i></div></a>');
			$(".agent-twitter").html('<a href="'+agent["twitter"]+'" target="_blank"><div class="listing-icon" style="font-size:24px;"><i class="fa fa-twitter" aria-hidden="true"></i></div></a>');
			$(".agent-instagram").html('<a href="'+agent["instagram"]+'" target="_blank"><div class="listing-icon" style="font-size:24px;"><i class="fa fa-instagram" aria-hidden="true"></i></div></a>');
			$(".agent-linkedin").html('<a href="'+agent["linkedin"]+'" target="_blank"><div class="listing-icon" style="font-size:24px;"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a>');


      $.each(listings,function(index,value){
        value["price"] = value["price"].replace("$", "");

        if(value["beds"] == -1) {
          value["beds"] = "Upon Inquiry";
        }

        if(value["baths"] == -1) {
          value["baths"] = "Upon Inquiry";
        }

        if(value["dimens"] == 0 || value["dimens"] == -1) {
          value["dimens"] = "Upon Inquiry";
        } else {
          value["dimens"] = value["dimens"] + " SQFT";
        }

        $(".agent-properties").append("<a href='https://spac.io/l/"+value["propertyID"]+"' class='agent-property' id='agent-property-"+index+"'><div class='listing-image'></div><div class='listing-details'><div class='listing-title'></div><div class='listing-price'></div><div class='listing-size'></div><div class='listing-beds'></div><div class='listing-baths'></div></div></a>")

        $("#agent-property-"+index+" .listing-image").css("background-image","url("+value["image"]+")");

        $("#agent-property-"+index+" .listing-title").html(value["addr1"]);
        $("#agent-property-"+index+" .listing-size").html('<div class="listing-icon"><i class="fa fa-arrows-alt main-color-font" aria-hidden="true"></i></div> Size: '+value["dimens"]);
        $("#agent-property-"+index+" .listing-beds").html('<div class="listing-icon"><i class="fa fa-bed main-color-font" aria-hidden="true"></i></div> Beds: '+value["beds"]);
        $("#agent-property-"+index+" .listing-baths").html('<div class="listing-icon"><i class="fa fa-bath main-color-font" aria-hidden="true"></i></div> Baths: '+value["baths"]);
        $("#agent-property-"+index+" .listing-price").html("$"+value["price"]);
      })

      if(agent["type"] == "brokerage") {
        $(".brokerage-logo").css("background-image","url("+agent["brokerageLogo"]+")");
        $(".main-color-border").css("border-color",agent["brokerageColors"]["primary"]);
        $(".main-color-background").css("background-color",""+agent["brokerageColors"]["primary"]);
        $(".main-color-font").css("color",""+agent["brokerageColors"]["primary"]);
      } else {

        $(".brokerage-logo").hide();
        $(".main-color-border").css("border-color","black");
        $(".main-color-background").css("background-color","black");
        $(".main-color-font").css("color","black");
      }

    }

		function sanitizeURL (string) {
	    if (!~string.indexOf("http") && string != ""){
	        string = "http://" + string;
	    }
	    return string;
		}

    </script>
  </body>
</html>
