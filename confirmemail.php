<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	$_id = $_GET["e"];
	$_vtoken = $_GET["v"];

?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.png">

    <title>Spacio: Reset Password</title>

    <link href="/assets/css/normalize.css" rel="stylesheet">
    <link href="/assets/css/website.css" rel="stylesheet">
    <link href="/assets/css/signup.css" rel="stylesheet">

    <link href="/assets/fonts/fonts.css" rel="stylesheet">

    <link rel="import" id="topmenu" href="/topmenu.html">
    <link rel="import" id="footer" href="/footer.html">
    <link rel="import" id="popups" href="/popups.html">
    <link rel="import" id="alertbox" href="/alertbox.html">

  </head>

  <body>
    <div class="spacio-top-menu">

    </div>
    <!-- TOP MENU END -->
     <div class="popup-overlay"></div>
    <!-- POPUP OVERLAY END -->
     <div class="alert-overlay"></div>
    <!-- ALERT OVERLAY END -->

    <div class="signup-scroll-one">
      <div class="overlay-flex">
        <div id="msg" class="large" style="color:white;text-align:center;line-height:1.4;" ></div>
      </div>

    </div>
    <!-- SCROLL ONE END -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/jquery.loadTemplate.min.js"></script>
    <script src="/assets/js/website.js"></script>
    <script>

    var wsURL = "../oh/webservice/1.1.6/webservice.php";

    $(document).ready(function(){
      insertAlertBox();
      confirmEmail();
    });



  	function confirmEmail() {
  		var _query = {};

  		_query["managerID"] = "<?php echo $_id?>";
  		_query["vtoken"] = "<?php echo $_vtoken?>";
  		_query["fn"] = "confirmEmail";

  		var json_data = _query;
  		console.log("Outbound JSON Query: "+JSON.stringify(json_data));
  		$.ajax({
  			url: wsURL,
  			type:"POST",
  			data: {json : json_data},
  			success: function(data, textStatus, jqXHR){
  				console.log("Returned JSON Object: "+JSON.stringify(data));

  				if (data.status == 1) {
  					$("#msg").html("Your email has been confirmed!<br/>Login to Spacio and add your first open house <a href='dashboard' style='color:#e87e04'><u>HERE</u></a>.");

  				} else {
  					$("#msg").html("Your email could not be confirmed.");
  				}

  			},
  			error: function(jqXHR, textStatus, errorThrown) {
  				console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Add User Error: "+errorThrown);
  			},
  			dataType: "json"
  		});

  	}
    </script>
  </body>
</html>
