<?php

require_once('../../config/db_conn.php');
require_once('../../require/core.php');
require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/stripe-php-3.14.3/init.php');
require_once('shared_queries.php');


class Brok_Query {

	/* ------------------------------ MANAGER RELATED FUNCTIONS ------------------------------*/
	// Type defaults to user, other possible: admin, superadmin

	static function registerBrokerageManager($fname, $lname, $email,  $brokerageID, $pw, $salt) {

	  	$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);
		$expired = new MongoDate($timeStamp - (86400)); // 12 hours 43200

		$vtoken = generateRandomString(16);

	  $collection = Db_Conn::getInstance()->getConnection()->brokerage_managers;
	  $s_result = $collection->findOne(array('email' => $email));

	  if (!is_null($s_result)) {
			//Email already exists
			$return = array("status" => 2);
	  } else {

		  //$getDate = date('Y-m-d H:i:s');
		  $document_manager = array("fname" => $fname,
									"lname"=> $lname,
									"title" => "",
									"pphoto" => "http://spac.io/assets/img/stock/pphoto.jpg",
									"email"=> $email,
									"brokerageID"=>$brokerageID,
									"type" => "brokerage",
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"emailConfirmed" => true,
									"promocode" => "",
									"rtoken" => "",
									"rtDateCreated" => $expired);

		  $result = $collection->insert($document_manager);

		if ($result) {

			$return = array("status" => 1,
							"managerId" => $document_manager["_id"]->{'$id'},
							"vtoken" => $vtoken);
		  } else {
			$return = array("status" => 0);
		  }


      }
	  return $return;
	}

	static function addBrokerageManager($managerID,$vtoken,$brokerageID, $fname, $lname, $email,$pw,$salt,$type,$offices) {

	  	$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);
		$expired = new MongoDate($timeStamp - (86400)); // 12 hours 43200

		$email = strtolower($email);

		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$s_result = $collection_brokerage_managers->findOne(array('email' => $email));

			 if (!is_null($s_result)) {
				//Email already exists
				$return = array("status" => 2);
	  		} else {

			   $new_vtoken = generateRandomString(16);
		  //$getDate = date('Y-m-d H:i:s');
		  $document_manager = array("fname" => $fname,
									"lname"=> $lname,
									"title" => "",
									"pphoto" => "http://spac.io/assets/img/stock/pphoto.jpg",
									"email"=> $email,
									"brokerageID"=>$brokerageID,
									"type" => $type,
									"offices" => $offices,
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $new_vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"emailConfirmed" => true,
									"promocode" => "",
									"rtoken" => "",
									"rtDateCreated" => $expired);

		  $result = $collection_brokerage_managers->insert($document_manager);

			if ($result) {

				$return = array("status" => 1,
								"managerId" => $document_manager["_id"]->{'$id'},
								"vtoken" => $vtoken);
			  } else {
				$return = array("status" => 0);
			  }
		  }
		} else {
			$return = array("status" => 10, "managerID" => $managerID, "votken" => $vtoken);
		}
	  return $return;
	}

	static function editBrokerageManager($managerID,$vtoken,$brokerageID, $mid,$fname, $lname, $email,$type,$offices) {

	  	$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);
		$expired = new MongoDate($timeStamp - (86400)); // 12 hours 43200

		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {


		  $newdata = array('$set' =>array("fname" => $fname,
									"lname"=> $lname,
									"email"=> $email,
									"type" => $type,
									"offices" => $offices));

		  $u_result = $collection_brokerage_managers->update(array('_id' => new MongoId($mid)), $newdata);


		  $return = array("status" => 1);
		} else {
		  $return = array("status" => 0);
		}

	  return $return;
	}

	static function claimAccount($email,$id, $pw, $salt, $from) {
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_counter = Db_Conn::getInstance()->getConnection()->id_counter;
		$collection_emails = Db_Conn::getInstance()->getConnection()->custom_emails;

		$unclaimed_result = $collection_unclaimed_accounts->findOne(array('_id' => new MongoId($id),'$or' => array(array('email' => $email),array('email2' => $email))));


		$manager_result = $collection_managers->findOne(array('email' => $email));
		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($from)));


		if (!is_null($unclaimed_result)) {
			if (!is_null($manager_result)) {
				//Account already claimed
				$return = array("status" => 2);
			} else {
				$newdata = array('$set' => array('status' => 'claimed'));
				$u_result = $collection_unclaimed_accounts->update(array('_id' => new MongoId($id)), $newdata);

				$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $unclaimed_result["brokerageID"]));
				$template = $brokerage_result["emailTemplate"];

				$welcome = $brokerage_result["welcome"];

				$_id = $unclaimed_result["_id"]->{'$id'};

				$welcome = str_replace("{{name}}",$unclaimed_result["fname"], $welcome);
				$welcome = str_replace("{{app_link}}","<a href='https://spac.io/download'><u>HERE</u></a>", $welcome);
				$welcome = str_replace("{{help_link}}","<a href='https://spac.io/manual'><u>HERE</u></a>", $welcome);

				$template = str_replace("{{signature}}","", $template);
				$template = str_replace("{{body}}",$welcome, $template);

				$date = new DateTime();
				$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);
				$expired = new MongoDate($timeStamp - (86400)); // 12 hours 43200

				$vtoken = generateRandomString(16);

				$man_fname = $unclaimed_result["fname"];
				$man_lname = $unclaimed_result["lname"];
				$man_key = $man_fname."".$man_lname;
				$man_key = strtolower($man_key);
				$man_key = str_replace(' ', '', $man_key);
				$counter = 1;
				$duplicate_result = $collection_managers->findOne(array('ukey' => $man_key));

				//If there is a duplicate key, iterate counter until result is null;
				while(!is_null($duplicate_result)) {
					$counter++;
					$man_key = $man_fname."".$man_lname."".$counter;
					$man_key = strtolower($man_key);
					$man_key = str_replace(' ', '', $man_key);
					$duplicate_result = $collection_managers->findOne(array('ukey' => $man_key));
				}

				 $document_manager = array("fname" => $unclaimed_result["fname"],
									"lname"=> $unclaimed_result["lname"],
									"ukey"=> $man_key,
									"title" => $unclaimed_result["title"],
									"teamID" => "N/A",
									"teamRole" => "N/A",
									"pphoto" => $unclaimed_result["pphoto"],
									"email"=> $email,
									"phone" => $unclaimed_result["phone"],
									"mobile" => $unclaimed_result["mobile"],
									"addr1" => $unclaimed_result["addr1"],
									"addr2" => $unclaimed_result["addr2"],
									"brand" => $unclaimed_result["brokerageID"],
									"website"=>$unclaimed_result["website"],
									"brokerage"=> $brokerage_result["brokerageName"],
									"brokerageID"=>$unclaimed_result["brokerageID"],
									"office"=> $unclaimed_result["office"],
									"officeID"=>$unclaimed_result["officeID"],
									"type" => "brokerage",
									"pw" => $pw,
									"salt" => $salt,
									"vtoken" => $vtoken,
									"dateCreated" => $currentTime,
									"lastActive" => $currentTime,
									"topproducerID" => "N/A",
									"marketsnapID" => "N/A",
									"rsID" => "N/A",
									"rsAutoClose" => "YES",
									"ttID" => "N/A",
									"ttAutoClose" => "YES",
									"contactuallyID" => "N/A",
									"contactuallyBucket" => "N/A",
									"emailConfirmed" => true,
									"promocode" => "",
									"rtoken" => "",
									"rtDateCreated" => $expired);

		  		$result = $collection_managers->insert($document_manager);

				/*
				$body = $brokerage_result["body"];

				$signature = $brokerage_result["signature"];
				$signature = str_replace("{{agent_name}}",$unclaimed_result["fname"]." ".$unclaimed_result["lname"], $signature);
				$signature = str_replace("{{agent_title}}",$unclaimed_result["title"], $signature);
				$signature = str_replace("{{agent_company}}",$brokerage_result["brokerageName"], $signature);
				$signature = str_replace("{{agent_phone}}",$unclaimed_result["phone"], $signature);
				$signature = str_replace("{{agent_email}}",$unclaimed_result["email"], $signature);

				$document_email = array("managerID" => $document_manager["_id"]->{'$id'},
										  "body" => $body,
										  "signature" => $signature);

				$result2 = $collection_emails->insert($document_email);
				*/
				$sample_result = createSampleProperty($document_manager["_id"]->{'$id'});

				$email_template = createEmailTemplate($document_manager["_id"]->{'$id'}, $unclaimed_result["brokerageID"]);

				if ($unclaimed_result["brokerageID"]=="halstead") {
					//$listings_result = getHalsteadListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="bhs") {
					//$listings_result = getBHSListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="corenyc") {
					//$listings_result = getCoreListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="mdrn") {
					//$listings_result = getMdrnListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="spire") {
					$listings_result = getSpireListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="semonin") {
					$listings_result = getSemoninListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="corcoran") {
					//$listings_result = getCorcoranListings($email,$document_manager["_id"]->{'$id'});
					$sync_result = syncFromBank($email,"corcoran");
				} else if ($unclaimed_result["brokerageID"]=="citihabitats") {
					//$listings_result = getCitihabitatsListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="platinum") {
					$listings_result = getPlatinumListings($email,$document_manager["_id"]->{'$id'});
				} else if ($unclaimed_result["brokerageID"]=="c21redwood") {
					$listings_result = getC21RedwoodListings($email,$document_manager["_id"]->{'$id'});
				}


				try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => $template,
						'subject' => 'Welcome To Spacio',
						'from_email' => "hello@spac.io",
						'from_name' => $brokerage_manager_result["fname"]." ".$brokerage_manager_result["lname"],
						'to' => array(
							array(
								'email' => $email
							)
						),
						'headers' => array('Reply-To' => $brokerage_manager_result["email"]),
						'important' => false,
						'tags' => array('welcome-email-brokerage')
					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}

				Onboarding_Email::welcomeBrokerageToSpacio($unclaimed_result["fname"],$email);

				$return = array("status" => 1);
			}
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function getClaimLink($email) {

		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$unclaimed_result = $collection_unclaimed_accounts->findOne(array('$or' => array(array('email' => $email),array('email2' => $email)), 'status'=>'unclaimed'));

		$_id = $unclaimed_result["_id"]->{'$id'};

		if(!is_null($unclaimed_result)){
			$managerID = "570b6da5d4c6cd2e97fc74f0";
			$link = "http://spac.io/claim/?e=".$email."&u=".$_id."&f=".$managerID."";
			$return = array("status" => 1, "url" => $link);

		} else {
			$manager_result = $collection_managers->findOne(array('email' => $email));
			if(!is_null($manager_result)){
				$return = array("status" => 2);
			} else {
				$return = array("status" => 0);
			}
		}
		return $return;
	}

	static function emailClaimLink($email) {

		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$unclaimed_result = $collection_unclaimed_accounts->findOne(array('$or' => array(array('email' => $email),array('email2' => $email)), 'status'=>'unclaimed'));



		if(!is_null($unclaimed_result)){
			$_id = $unclaimed_result["_id"]->{'$id'};

			$managerID = "570b6da5d4c6cd2e97fc74f0";
			$link = "http://spac.io/claim/?e=".$email."&u=".$_id."&f=".$managerID."";

			$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
			$message = array(
				'html' => "<html>
    <body>
      <div style='background:#f7f7f7;padding:20px;'>
        <img src='http://spac.io/assets/img/nav-logo.png' height='20'>
        <div style='background:white;font-size:14px;padding:20px;margin-top:20px;'>
          Hello ".$unclaimed_result["fname"].",<br/><Br/>Click here to claim your Spacio account:<br/><a href='".$link."'>".$link."</a>.<br/><br/>
		  <p>The Spacio Team<br/>
          <a href='hello@spac.io'  style='color:#999;'>hello@spac.io</a>
          </p>
        </div>
      </div>
    </body>
  </html>",
				'subject' => 'Claim Your Spacio Account',
				'from_email' => 'hello@spac.io',
				'from_name' => "Spacio",
				'to' => array(
					array(
						'email' => $email
					)
				),
				'headers' => array('Reply-To' => "hello@spac.io"),
				'important' => false,
				'tags' => array('claim-account-link')

			);
			$async = true;
			$result = $mandrill->messages->send($message, $async);

			$return = array("status" => 1);

		} else {

			$return = array("status" => 0);

		}
		return $return;
	}

	static function sendClaimEmail($managerID, $vtoken, $email) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
				$collection_claim_emails_log = Db_Conn::getInstance()->getConnection()->claim_emails_log;

		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));
			$unclaimed_result = $collection_unclaimed_accounts->findOne(array('email' => $email));

			$claim = $brokerage_result["claim"];
			$template = $brokerage_result["emailTemplate"];

			$_id = $unclaimed_result["_id"]->{'$id'};

			$claim = str_replace("{{name}}",$unclaimed_result["fname"], $claim);
			$claim = str_replace("{{claim_link}}","<a href='http://spac.io/claim/?e=".$email."&u=".$_id."&f=".$managerID."'><u>HERE</u></a>", $claim);
			$claim = str_replace("{{app_link}}","<a href='https://itunes.apple.com/app/spacio-pro-best-open-house/id1017394547?mt=8'><u>HERE</u></a>", $claim);
			$claim = str_replace("{{help_link}}","<a href='http://spac.io/help/'><u>HERE</u></a>", $claim);

			$claim = str_replace("{{email}}",$email, $claim);

			$template = str_replace("{{signature}}","", $template);
			$template = str_replace("{{body}}",$claim, $template);

			try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => $template,
							'subject' => 'Claim Your Spacio Account',
							'from_email' => 'hello@spac.io',
							'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
							'to' => array(
								array(
									'email' => $email
								)
							),
							'headers' => array('Reply-To' => $manager_result["email"]),
							'important' => false,
							'tags' => array('claim-account')

						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}

			$date = new DateTime();
		  	$timeStamp = $date->getTimestamp();
		  	$currentTime = new MongoDate($timeStamp);

			$document_log = array(
				"uid" => $_id,
				"email" => $email,
				"dateCreated" => $currentTime,
				"status" => "Sent"
			);

			$result = $collection_claim_emails_log->insert($document_log);

			if(array_key_exists("email2", $unclaimed_result) ) {

				$email2 = $unclaimed_result["email2"];
				if ($email2 != "") {
					$claim = $brokerage_result["claim"];
					$template = $brokerage_result["emailTemplate"];

					$_id = $unclaimed_result["_id"]->{'$id'};

					$claim = str_replace("{{name}}",$unclaimed_result["fname"], $claim);
					$claim = str_replace("{{claim_link}}","<a href='http://spac.io/claim/?e=".$email2."&u=".$_id."&f=".$managerID."'><u>HERE</u></a>", $claim);
					$claim = str_replace("{{app_link}}","<a href='https://itunes.apple.com/app/spacio-pro-best-open-house/id1017394547?mt=8'><u>HERE</u></a>", $claim);
					$claim = str_replace("{{help_link}}","<a href='http://spac.io/help/'><u>HERE</u></a>", $claim);
					$claim = str_replace("{{email}}",$email2, $claim);

					$template = str_replace("{{signature}}","", $template);
					$template = str_replace("{{body}}",$claim, $template);

					try {
						$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
						$message = array(
							'html' => $template,
							'subject' => 'Claim Your Spacio Account',
							'from_email' => 'hello@spac.io',
							'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
							'to' => array(
								array(
									'email' => $email2
								)
							),
							'headers' => array('Reply-To' => $manager_result["email"]),
							'important' => false,
							'tags' => array('claim-account')

						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);

					} catch(Mandrill_Error $e) {
						// Mandrill errors are thrown as exceptions
						echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
						// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
						throw $e;
					}

				}
			}

			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}



		return $return;

	}


	static function sendClaimEmails($managerID, $vtoken, $officeID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_claim_emails_log = Db_Conn::getInstance()->getConnection()->claim_emails_log;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));

			if($officeID == "all") {
				$unclaimed_result = $collection_unclaimed_accounts->find(array('brokerageID' => $manager_result["brokerageID"], 'status' => 'unclaimed'));
			} else {
				$unclaimed_result = $collection_unclaimed_accounts->find(array('brokerageID' => $manager_result["brokerageID"], 'status' => 'unclaimed', 'officeID' => $officeID));
			}

			$count = 0;
			foreach ($unclaimed_result as $account) {

				$claim = $brokerage_result["claim"];
				$template = $brokerage_result["emailTemplate"];

				$_id = $account["_id"]->{'$id'};

				$claim = str_replace("{{name}}",$account["fname"], $claim);


				$claim = str_replace("{{claim_link}}","<a href='http://spac.io/claim/?e=".$account["email"]."&u=".$_id."&f=".$managerID."'><u>HERE</u></a>", $claim);
				$claim = str_replace("{{app_link}}","<a href='https://itunes.apple.com/app/spacio-pro-best-open-house/id1017394547?mt=8'><u>HERE</u></a>", $claim);
				$claim = str_replace("{{help_link}}","<a href='http://spac.io/help/'><u>HERE</u></a>", $claim);

				$template = str_replace("{{signature}}","", $template);
				$template = str_replace("{{body}}",$claim, $template);


				$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
				$message = array(
					'html' => $template,
					'subject' => 'Claim Your Spacio Account',
					'from_email' => 'hello@spac.io',
					'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
					'to' => array(
						array(
							'email' => $account["email"]
						)
					),
					'headers' => array('Reply-To' => $manager_result["email"]),
					'important' => false,
					'tags' => array('claim-account-blast')

				);
				$async = true;
				$result = $mandrill->messages->send($message, $async);
				$count++;

				$date = new DateTime();
		  		$timeStamp = $date->getTimestamp();
		  		$currentTime = new MongoDate($timeStamp);

				$document_log = array(
					"uid" => $_id,
					"email" => $account["email"],
					"dateCreated" => $currentTime,
					"status" => "Sent"
				);

				$result = $collection_claim_emails_log->insert($document_log);

				if(array_key_exists("email2", $account) ) {

					$email2 = $account["email2"];
					if ($email2 != "") {
						$claim = $brokerage_result["claim"];
						$template = $brokerage_result["emailTemplate"];

						$_id = $account["_id"]->{'$id'};

						$claim = str_replace("{{name}}",$account["fname"], $claim);


						$claim = str_replace("{{claim_link}}","<a href='http://spac.io/claim/?e=".$email2."&u=".$_id."&f=".$managerID."'><u>HERE</u></a>", $claim);
						$claim = str_replace("{{app_link}}","<a href='https://itunes.apple.com/app/spacio-pro-best-open-house/id1017394547?mt=8'><u>HERE</u></a>", $claim);
						$claim = str_replace("{{help_link}}","<a href='http://spac.io/help/'><u>HERE</u></a>", $claim);

						$template = str_replace("{{signature}}","", $template);
						$template = str_replace("{{body}}",$claim, $template);

							$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
							$message = array(
								'html' => $template,
								'subject' => 'Claim Your Spacio Account',
								'from_email' => 'hello@spac.io',
								'from_name' => $manager_result["fname"].' '.$manager_result["lname"],
								'to' => array(
									array(
										'email' => $email2
									)
								),
								'headers' => array('Reply-To' => $manager_result["email"]),
								'important' => false,
								'tags' => array('claim-account-blast')

							);
							$async = true;
							$result = $mandrill->messages->send($message, $async);
							$count++;

					}
				}

			}
			$return = array("status" => 1, "sent" => $count);
		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function getBrokerageEmailTemplate($managerID, $vtoken) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));

			if (!is_null($brokerage_result)) {
				$return = array("status" => 1,
					"customer" => $brokerage_result["customerMessageBody"],
					"agent" => $brokerage_result["agentMessageBody"],
					"broker" => $brokerage_result["brokerMessageBody"],
					"messageclaim" => $brokerage_result["claim"],
					"messagewelcome" => $brokerage_result["welcome"]
				);
			} else {

				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function saveBrokerageEmailTemplate($managerID, $vtoken, $body, $type) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $manager_result["brokerageID"]));

			if (!is_null($brokerage_result)) {

				if($type == "customer") {
					$newdata = array('$set' => array('customerMessageBody' => $body));
				} else if($type == "agent") {
					$newdata = array('$set' => array('agentMessageBody' => $body));
				} else if($type == "broker") {
					$newdata = array('$set' => array('brokerMessageBody' => $body));
				} else if($type == "claim") {
					$newdata = array('$set' => array('claim' => $body));
				} else if($type == "welcome") {
					$newdata = array('$set' => array('welcome' => $body));
				}




				$u_result = $collection_brokerages->update(array('brokerageID' => $manager_result["brokerageID"]), $newdata);


				$return = array("status" => 1);
			} else {


				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}


	static function createDefaultTemplate() {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_emails = Db_Conn::getInstance()->getConnection()->custom_emails;
		$cursor = $collection_managers->find();

		foreach ($cursor as $doc) {
			$doc_id = $doc["_id"]->{'$id'};

			$body = '<p>Hi {{name}},</p><p>Thanks for coming by!</p><p> Here are the details of today\'s open house:</p>{{oh_title}}<br/>Address: {{oh_addr1}} {{oh_addr2}}<br/>Website: {{oh_url}}<br/>Bed: {{oh_beds}}<br/>Bath: {{oh_baths}}<br/>Price: {{oh_price}}<br/>Details: {{oh_details}}<br/><p>Please let me know if you have any questions, I\'m happy to help.</p><br/><p>Thank you,</p>';

			if ($doc['brokerage'] == "" || $doc['brokerage'] == "N/A") {
				$brokerage = "";
			} else {
				$brokerage = $doc['brokerage']."<br/>";
			}

			if ($doc['email'] == "" || $doc['email'] == "N/A") {
				$email = "";
			} else {
				$email = $doc['email']."<br/>";
			}

			if ($doc['phone'] == "" || $doc['phone'] == "N/A") {
				$phone = "";
			} else {
				$phone = $doc['phone']."<br/>";
			}

			if ($doc['website'] == "" || $doc['website'] == "N/A") {
				$website = "";
			} else {
				$website = $doc['website']."<br/>";
			}

			$signature = $doc['fname'].' '.$doc['lname'].'<br/>'.$brokerage.''.$email.''.$phone.''.$website;

			$document_email = array("managerID" => $doc_id,
									"body" => $body,
									"signature" => $signature);

			$result = $collection_emails->insert($document_email);
		}

		$return = array("status" => 1);
		return $return;

	}

	static function getBrokerageRegistrants($managerID, $vtoken, $brokerageID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerageID));
		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$registrants_result = $collection_registrants->find(array('brokerageID' => $brokerageID));

			$leads = array();

			if($brokerage_result["exportAuthorized"] == "NO") {
				foreach ($registrants_result as $registrant) {
					unset($registrant["fullcontact"]);

					$registrant["id"] =  $registrant["_id"]->{'$id'};
					$registrant["name"] = "hidden";

					if($registrant["email"] != "" && $registrant["email"] != "N/A") {
						$registrant["email"] = "hidden";
					}
					if($registrant["phone"] != "" && $registrant["phone"] != "N/A") {
						$registrant["phone"] = "hidden";
					}
					array_push($leads, $registrant);
				}
			} else {
				foreach ($registrants_result as $registrant) {
					unset($registrant["fullcontact"]);
					$registrant["id"] =  $registrant["_id"]->{'$id'};

					array_push($leads, $registrant);
				}
			}

			$return = array(
				"status" => 1,
				"leads" => $leads
			);
		} else {
			$return = array(
				"status" => 2
			);
		}

		return $return;
	}

	static function getBrokerageProperties($managerID, $vtoken, $brokerageID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_properties = Db_Conn::getInstance()->getConnection()->properties;
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$properties_result = $collection_properties->find(array('brokerageID' => $brokerageID));

			$properties = array();
			foreach ($properties_result as $property) {
				$property["id"] =  $property["_id"]->{'$id'};
				$propertyID = $property["propertyID"];
				$registrants_result = $collection_registrants->find(array('propertyID' => $propertyID));
				$property["num_leads"] = $registrants_result->count();

				array_push($properties, $property);
			}

			$return = array(
				"status" => 1,
				"properties" => $properties
			);
		} else {
			$return = array(
				"status" => 2
			);
		}

		return $return;
	}

	static function getBrokerageAdmins($managerID, $vtoken, $brokerageID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$admins_result = $collection_brokerage_managers->find(array('brokerageID' => $brokerageID));

			$admins = array();
			foreach ($admins_result as $doc) {
				$doc["id"] =  $doc["_id"]->{'$id'};
				array_push($admins, $doc);
			}

			$return = array(
				"status" => 1,
				"admins" => $admins
			);

		} else {
			$return = array(
				"status" => 2
			);
		}

		return $return;
	}


	static function getBrokerageOpenHouses($managerID, $vtoken, $brokerageID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$hosted_result = $collection_hosted->find(array('brokerageID' => $brokerageID));

			$hosted = array();
			foreach ($hosted_result as $doc) {
				$doc["id"] =  $doc["_id"]->{'$id'};
				array_push($hosted, $doc);
			}

			$return = array(
				"status" => 1,
				"hosted" => $hosted
			);

		} else {
			$return = array(
				"status" => 2
			);
		}

		return $return;
	}

	static function deleteBrokerageManager($managerID, $vtoken,  $brokerageID, $email) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken, "brokerageID" => $brokerageID));

		if (!is_null($brokerage_manager_result)) {
			$collection_brokerage_managers->remove(array('email' => $email, 'brokerageID' => $brokerageID));


				$return = array(
					"status" => 1
				);


		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;

	}


	static function deleteUser($managerID, $vtoken,  $brokerageID, $email) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_unclaimed = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerageID));

		if (!is_null($brokerage_manager_result)) {
			$unclaimed_result = $collection_unclaimed->findOne(array('email' => $email));
			$manager_result = $collection_managers->findOne(array('email' => $email, 'brokerageID' => $brokerageID));

			if(!is_null($unclaimed_result)) {
				$collection_unclaimed->remove(array('email' => $email));
				$return = array(
					"status" => 1
				);
			}

			if(!is_null($manager_result)) {
				$newdata = array('$set' => array('type' => 'freemium', 'brokerageID' => 'N/A', 'office' => 'N/A','officeID' => 'N/A', 'addr1' => "", 'addr2' => ""));
				$u_result = $collection_managers->update(array('email' => $email, 'brokerageID'=>$brokerageID), $newdata);

				$return = array(
					"status" => 1
				);
			}

		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;

	}

	static function addUser($managerID, $vtoken, $brokerageID, $officeID, $fname, $lname, $email, $email2, $title, $pphoto,$phone,$mobile, $website) {
     	$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_offices = Db_Conn::getInstance()->getConnection()->offices;
		$collection_unclaimed = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerageID));
		$office_result = $collection_offices->findOne(array('officeID' => $officeID));

		$email = trim($email);
		$email = strtolower($email);

		if (!is_null($brokerage_manager_result)) {
			$date = new DateTime();
			$timeStamp = $date->getTimestamp();
			$currentTime = new MongoDate($timeStamp);

			$unclaimed_result = $collection_unclaimed->findOne(array('$or' =>array(array('email' => $email), array('email2' => $email))));
			$manager_result = $collection_managers->findOne(array('$or' =>array(array('email' => $email), array('email' => $email2))));

			if(!is_null($unclaimed_result) || !is_null($manager_result)) {
				$return = array(
					"status" => 2
				);
			} else {
				$document_unclaimed = array("fname" => $fname,
									"lname"=> $lname,
									"title" => $title,
									"pphoto" => $pphoto,
									"email"=> $email,
									"email2"=> $email2,
									"phone" => $phone,
									"mobile" => $mobile,
									"addr1" => $office_result["addr1"],
									"addr2" => $office_result["addr2"],
									"brand" => $brokerageID,
									"website"=>$website,
									"brokerage"=> $brokerage_result["brokerageName"],
									"brokerageID"=>$brokerageID,
									"office"=> $office_result["office"],
									"officeID"=>$officeID,
									"dateCreated" => $currentTime,
									"status" => "unclaimed"
									);
				$result = $collection_unclaimed->insert($document_unclaimed);

				$return = array(
					"status" => 1
				);
			}

		} else {
			$return = array(
				"status" => 0
			);
		}

	 	 return $return;
	}

	static function resolveUser($managerID, $vtoken, $brokerageID, $officeID, $fname, $lname, $email) {
     	$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_offices = Db_Conn::getInstance()->getConnection()->offices;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerageID));
		$office_result = $collection_offices->findOne(array('officeID' => $officeID));

		if (!is_null($brokerage_manager_result)) {
			$date = new DateTime();
			$timeStamp = $date->getTimestamp();
			$currentTime = new MongoDate($timeStamp);

			$manager_result = $collection_managers->findOne(array('email' => $email, "type" => "brokerage"));

			if(!is_null($manager_result)) {
				$newdata = array('$set' => array('type' => 'brokerage',
											'brokerageID' => $brokerageID,
											'office' => $office_result["office"],
											'officeID' => $officeID,
											'addr1' => $office_result["addr1"],
											'addr2' => $office_result["addr2"]));
				$u_result = $collection_managers->update(array('email' => $email), $newdata);


				$return = array(
					"status" => 1,
					"office" =>$office_result["office"]
				);
			} else {


				$return = array(
					"status" => 2
				);
			}

		} else {
			$return = array(
				"status" => 0
			);
		}

	 	 return $return;
	}

	static function addOffice($managerID, $vtoken,  $brokerageID, $office, $addr1, $addr2) {
     	$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_offices = Db_Conn::getInstance()->getConnection()->offices;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$date = new DateTime();
			$timeStamp = $date->getTimestamp();
			$currentTime = new MongoDate($timeStamp);

			$counter = 1;
			$officeID = $brokerageID."-".$counter;
			$officeID = strtolower($officeID);

			$duplicate_result = $collection_offices->findOne(array('officeID' => $officeID));

			//If there is a duplicate office id, iterate counter until result is null;
			while(!is_null($duplicate_result)) {
				$counter++;
				$officeID = $brokerageID."-".$counter;
				$officeID = strtolower($officeID);
				$duplicate_result = $collection_offices->findOne(array('officeID' => $officeID));
			}

			$document_office = array("brokerageID" => $brokerageID,
									"office" => $office,
									"officeID" => $officeID,
									"addr1"=>$addr1,
									"addr2"=>$addr2,
									"dateCreated"=>$currentTime);

			$result = $collection_offices->insert($document_office);

			$return = array(
				"status" => 1
			);
		} else {
			$return = array(
				"status" => 0
			);
		}

	 	 return $return;
	}

	static function getBrokerageInfo($managerID,$vtoken) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		$return = array();
		if (!is_null($brokerage_manager_result)) {
			$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerage_manager_result["brokerageID"]));
			$return["status"] = 1;

			foreach ($brokerage_result as $k => $v) {
    			$return[$k] = $v;
			}

			$return["id"] = $brokerage_result["_id"]->{'$id'};

			unset($return['body']);
			unset($return['signature']);
			unset($return['claim']);
			unset($return['questions']);
			unset($return['emailTemplate']);
			unset($return['welcome']);

		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;

	}
	static function getOffices($managerID, $vtoken, $brokerageID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_offices = Db_Conn::getInstance()->getConnection()->offices;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$offices_result = $collection_offices->find(array('brokerageID' => $brokerageID));

			$offices = array();
			foreach ($offices_result as $office) {
				$office["id"] =  $office["_id"]->{'$id'};
				array_push($offices, $office);
			}

			$return = array(
				"status" => 1,
				"offices" => $offices
			);

		} else {
			$return = array(
				"status" => 2
			);
		}
		return $return;
	}

	static function getClaimedAccounts($managerID, $vtoken, $brokerageID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;
		$collection_hosted = Db_Conn::getInstance()->getConnection()->open_house_hosted;
		//$collection_registrants = Db_Conn::getInstance()->getConnection()->registrants;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$managers_result = $collection_managers->find(array('brokerageID' => $brokerageID));

			$managers = array();
			foreach ($managers_result as $manager) {
				$manager["id"] =  $manager["_id"]->{'$id'};
				$manager["status"] = "claimed";

				//$registrant_result = $collection_registrants->find(array('managerID' => $manager["id"]));
				//$hosted_result = $collection_hosted->find(array('managerID' => $manager["id"]));

				$manager["num_leads"] = 0;;
				$manager["num_ohs"] = 0;
				array_push($managers, $manager);
			}

			$return = array(
				"status" => 1,
				"managers" => $managers
			);

		} else {
			$return = array(
				"status" => 2
			);
		}
		return $return;

	}

	static function getUnclaimedAccounts($managerID, $vtoken, $brokerageID) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_unclaimed_accounts = Db_Conn::getInstance()->getConnection()->unclaimed_accounts;
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$unclaimed_result = $collection_unclaimed_accounts->find(array('brokerageID' => $brokerageID, 'status' => 'unclaimed'));

			$unclaimed = array();
			foreach ($unclaimed_result as $account) {
				$account["id"] =  $account["_id"]->{'$id'};
				$account["num_leads"] =  0;
				$account["num_ohs"] =  0;
				array_push($unclaimed, $account);
			}

			$return = array(
				"status" => 1,
				"unclaimed" => $unclaimed
			);

		} else {
			$return = array(
				"status" => 2
			);
		}
		return $return;
	}

	static function getClaimEmailsLog($managerID, $vtoken, $email) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$collection_claim_emails_log = Db_Conn::getInstance()->getConnection()->claim_emails_log;

		$brokerage_manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($brokerage_manager_result)) {
			$log_result = $collection_claim_emails_log->find(array("email" => $email));


			$claim_log = array();
			foreach ($log_result as $log) {

				array_push($claim_log, $log);
			}

			//$log_result->sort(array("dateCreated" => -1));
			$return = array(
				"status" => 1,
				"claim_log" => $claim_log
			);

		} else {
			$return = array(
				"status" => 0
			);
		}
		return $return;
	}

	static function getBrand($managerID, $vtoken) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
	  	$collection_brands = Db_Conn::getInstance()->getConnection()->brands;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			if ($manager_result["brand"] != "N/A" && $manager_result["brand"] != "") {
				$brand_result = $collection_brands->findOne(array('name' => $manager_result["brand"]));

				if (!is_null($brand_result)) {
					$return = array("status" => 1,
								"leftpanel" => $brand_result["leftpanel"],
								"imgs" => $brand_result["imgs"],
								"font" => $brand_result["font"],
								"color" => $brand_result["color"] );
				} else {
					$return = array("status" => 2);
				}

			} else {
				$return = array("status" => 2);
			}
		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyCorpCode($managerID, $vtoken, $code) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
	  	$collection_brands = Db_Conn::getInstance()->getConnection()->brands;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			if($code == "" || $code == "N/A") {
				$newdata = array('$set' => array('brand' => "N/A"));
				$u_result = $collection_managers->update(array('_id' => new MongoId($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
			} else {
				$brand_result = $collection_brands->findOne(array('code' => $code));

				if (!is_null($brand_result)) {
					$brand = $brand_result['name'];
					$newdata = array('$set' => array('brand' => $brand));
					$u_result = $collection_managers->update(array('_id' => new MongoId($managerID),"vtoken" => $vtoken), $newdata);


					$return = array("status" => 1);
				} else {
					$return = array("status" => 2);
				}
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}

	static function applyPromoCode($managerID, $vtoken, $promo) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$collection_promos = Db_Conn::getInstance()->getConnection()->promocodes;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		$promo_result = $collection_promos->findOne(array('code' => $promo));

	   	$promo_valid = true;

	   if($promo != "") {
		   if(!is_null($promo_result))  {
				  if(($currentTime->sec) > ($promo_result["expiry"]->sec)) {
					   $promo_valid = false;
				  } else  {
					  $promo_valid = true;
				  }
		   } else {
			   $promo_valid = false;
		   }
	   }

		if (!is_null($manager_result)) {
			if($promo_valid) {
				$newdata = array('$set' => array('promocode' => $promo));
				$u_result = $collection_managers->update(array('_id' => new MongoId($managerID),"vtoken" => $vtoken), $newdata);

				$return = array("status" => 1);
			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 0);
		}

		return $return;

	}




	static function getBrokerageSaltByEmail($email) {

		$collection = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$s_result = $collection->findOne(array('email' => $email), array('salt'));

		if (!is_null($s_result)) {
			$return = array(
				"status" => 1,
				"salt" => $s_result["salt"]
			);
		} else {
			$return = array(
				"status" => 0
			);
		}
		return $return;
	}

	static function getManagerToken($vtoken,$email) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$manager_result = $collection_managers->findOne(array('email' => $email));
		if (!is_null($manager_result)) {
			$return = array("status" => 1,
							"vtoken" => $manager_result["vtoken"],
							"id" => $manager_result["_id"]->{'$id'});


		} else {
			$return = array("status" => 0);

		}
		return $return;
	}

	static function saveBrokerageCreditCard($managerID, $vtoken, $stripetoken) {
		\Stripe\Stripe::setApiKey(STRIPE_SKEY);
		$collection_brokerages = Db_Conn::getInstance()->getConnection()->brokerages;
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$brokerageID = $manager_result["brokerageID"];
			$brokerage_result = $collection_brokerages->findOne(array('brokerageID' => $brokerageID));

			if (!is_null($brokerage_result)) {
				$cus = $brokerage_result["cus"];

				if($cus == "" || $cus == "N/A") {
					try {
					  // Use Stripe's bindings...
						 $token = $stripetoken;

							$customer = \Stripe\Customer::create(array(
							  "source" => $token,
							  "metadata" => array("brokerageID" => $brokerageID),
							  "description" => $brokerageID." Brokerage",
							  "email" => $manager_result["email"])
							);

					} catch(\Stripe\Error\Card $e) {
					  // Since it's a decline, \Stripe\Error\Card will be caught
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

					  $return = array("status" => 0,
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
					  return $return;

					} catch (\Stripe\Error\InvalidRequest $e) {
					  // Invalid parameters were supplied to Stripe's API
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;
					} catch (\Stripe\Error\Authentication $e) {
					  // Authentication with Stripe's API failed
					  // (maybe you changed API keys recently)
					  $return = array("status" => 0,
										"error_type" =>  "Authentication Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\ApiConnection $e) {
					  // Network communication with Stripe failed
					 $return = array("status" => 0,
										"error_type" =>  "Network Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\Base $e) {
					  // Display a very generic error to the user, and maybe send
					  // yourself an email
					  $return = array("status" => 0,
										"error_type" =>  "Base Error",
										"error_param" =>  "");
						return $return;
					} catch (Exception $e) {
						$return = array("status" => 0,
										"error_type" => "Unkown",
										"error_param" =>  "");
						return $return;
					  // Something else happened, completely unrelated to Stripe
					}


					$cc_type = $customer["sources"]["data"][0]["brand"];
					$last4 = $customer["sources"]["data"][0]["last4"];

					$newdata = array('$set' => array("cus" => $customer["id"],
							"cc_type" => $cc_type,
							"last4" => $last4));

					$u_result_subs = $collection_brokerages->update(array('brokerageID' => $brokerageID), $newdata);

					$return = array("status" => 1,
										"cus" => $customer["id"]);
				} else {
					$token = $stripetoken;
					try {
						$customer = \Stripe\Customer::retrieve($cus);
						$customer->source = $token; // obtained with Stripe.js
						$customer->save();

					} catch(\Stripe\Error\Card $e) {
					  // Since it's a decline, \Stripe\Error\Card will be caught
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

					  $return = array("status" => 0,
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
					  return $return;

					} catch (\Stripe\Error\InvalidRequest $e) {
					  // Invalid parameters were supplied to Stripe's API
					  $body = $e->getJsonBody();
					  $err  = $body['error'];

						$return = array("status" => 0,
										"error_type" =>  $err['type'],
										"error_code" =>  $err['code'],
										"error_param" =>  $err['param'],
										"error_msg" =>  $err['message']);
						return $return;
					} catch (\Stripe\Error\Authentication $e) {
					  // Authentication with Stripe's API failed
					  // (maybe you changed API keys recently)
					  $return = array("status" => 0,
										"error_type" =>  "Authentication Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\ApiConnection $e) {
					  // Network communication with Stripe failed
					 $return = array("status" => 0,
										"error_type" =>  "Network Failure",
										"error_param" =>  "");
						return $return;
					} catch (\Stripe\Error\Base $e) {
					  // Display a very generic error to the user, and maybe send
					  // yourself an email
					  $return = array("status" => 0,
										"error_type" =>  "Base Error",
										"error_param" =>  "");
						return $return;
					} catch (Exception $e) {
						$return = array("status" => 0,
										"error_type" => "Unkown",
										"error_param" =>  "");
						return $return;
					  // Something else happened, completely unrelated to Stripe
					}

					$return = array("status" => 1,
										"cus" => $customer["id"]);
				}

			} else {
				$return = array("status" => 2);
			}

		} else {
			$return = array("status" => 3);
		}
		return $return;
	}

	static function getManager($managerID,$vtoken) {
		$collection_brokerage_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$manager_result = $collection_brokerage_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {
			$return = array("status" => 1,
								"email" => $manager_result["email"],
								"brokerageID"=>$manager_result["brokerageID"],
								"fname" => $manager_result["fname"],
								"lname" => $manager_result["lname"],
								"title" => $manager_result["title"],
								"type" => $manager_result["type"],
								"offices" => $manager_result["offices"],
								"promo" => $manager_result["promocode"],
								"vtoken" => $manager_result["vtoken"],
								"id" => $manager_result["_id"]->{'$id'});
		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;

	}

	static function getProfile($ukey) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->managers;
		$profile_result = $collection_managers->findOne(array('ukey' => $ukey));

		if (!is_null($profile_result)) {
			$return = array("status" => 1,
								"email" => $profile_result["email"],
								"phone" => $profile_result["phone"],
								"mobile" => $profile_result["mobile"],
								"website" => $profile_result["website"],
								"brokerage" => $profile_result["brokerage"],
								"addr1" => $profile_result["addr1"],
								"addr2" => $profile_result["addr2"],
								"pphoto" => $profile_result["pphoto"],
								"fname" => $profile_result["fname"],
								"lname" => $profile_result["lname"],
								"title" => $profile_result["title"],
								"rsID" => $profile_result["rsID"]
								);
		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;

	}

	static function updateLastActive($managerID,$vtoken) {

		$collection = Db_Conn::getInstance()->getConnection()->managers;

		$date = new DateTime();
		$timeStamp = $date->getTimestamp();
		$currentTime = new MongoDate($timeStamp);
		$newdata = array('$set' => array('lastActive' => $currentTime));
		$u_result = $collection->update(array('_id' => new MongoId($managerID),"vtoken" => $vtoken), $newdata);

		$return = array("status" => 1);
		return $return;

	}

	static function loginBrokerageManager($email, $pw) {

		$collection = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$s_result = $collection->findOne(array('email' => $email));

		if (!is_null($s_result)) {

			if ($s_result["pw"] == $pw ) {
				//Create a temporary validation token for both client and server side
				//$vtoken = generateRandomString();
				//$newdata = array('$set' => array("vtoken" => $vtoken));
				//$u_result = $collection->update(array("email" => $_email), $newdata);

				//Login successful
				$date = new DateTime();
				$timeStamp = $date->getTimestamp();
				$currentTime = new MongoDate($timeStamp);

				$newdata = array('$set' => array('lastActive' => $currentTime));
				$u_result = $collection->update(array('email' => $email), $newdata);



				$return = array("status" => 1,
								"email" => $s_result["email"],
								"brokerageID" => $s_result["brokerageID"],
								"fname" => $s_result["fname"],
								"lname" => $s_result["lname"],
								"title" => $s_result["title"],
								"type" => $s_result["type"],
								"offices" => $s_result["offices"],
								"vtoken" => $s_result["vtoken"],
								"promo" => $s_result["promocode"],
								"emailConfirmed" => $s_result["emailConfirmed"],
								"id" => $s_result["_id"]->{'$id'});

			} else {
				// Wrong Password
				$return = array(
					"status" => 2
				);
			}
		} else {
			// Email does not exist
			$return = array(
				"status" => 0
			);
		}

		return $return;
		//Return JSON format of reply

	}



	static function changeBrokeragePassword($_managerID,$_vtoken,$_old_pass,$_pass) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;
		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($_managerID), "vtoken" => $_vtoken));

		if (!is_null($manager_result)) {

			if ($manager_result["pw"] != $_old_pass) {
				$return = array(
					"status" => 2,
				);
			} else {
				$vtoken = generateRandomString(16);
				$newdata = array('$set' => array('pw' => $_pass,'vtoken' => $vtoken));
				$u_result = $collection_managers->update(array('_id' => new MongoId($_managerID)), $newdata);
				$return = array(
					"status" => 1,
					"vtoken" => $vtoken
				);
			}

		} else {
			// User ID not found or validation failure
			$return = array("status" => 0);
		}

		return $return;
	}

	static function logoutManager($managerID, $vtoken) {

		$collection_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));

		if (!is_null($manager_result)) {

			//Logout is basically generating a new random vToken that does not get returned to the client.
			//It effectively logs out anybody else who was previously logged in.
			$vtoken = generateRandomString();
			$newdata = array('$set' => array('vtoken' => $vtoken));
			$u_result = $collection_managers->update(array('_id' => new MongoId($managerId)), $newdata);

			//Logout success
			$return = array("status" => 1);

		} else {
			// User ID not found
			$return = array("status" => 0);
		}

		return $return;
	}

	static function resetPassBrokerage($managerID,$rtoken,$pw,$salt) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "rtoken" => $rtoken));

		if (!is_null($manager_result)) {
			$date = new DateTime();
			$timeStamp = $date->getTimestamp();
			$currentTime = new MongoDate($timeStamp);
			$rtoken = generateRandomString(16);
			$vtoken = generateRandomString(16);

			$newdata = array('$set' => array('rtoken' => $rtoken,"rtDateCreated" => $currentTime,"pw" => $pw, "salt" => $salt, "vtoken" => $vtoken));
			$u_result = $collection_managers->update(array('_id' => new MongoId($managerID)), $newdata);

			$return = array(
				"status" => 1
			);
		} else {
			$return = array(
				"status" => 0
			);
		}

		return $return;
	}

	static function forgotPassBrokerage($email) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$manager_result = $collection_managers->findOne(array('email' => $email));

		if (!is_null($manager_result)) {
			$date = new DateTime();
			$timeStamp = $date->getTimestamp();
			$currentTime = new MongoDate($timeStamp);

			$rtoken = generateRandomString(16);

			$newdata = array('$set' => array('rtoken' => $rtoken,"rtDateCreated" => $currentTime));
			$u_result = $collection_managers->update(array('email' => $email), $newdata);

			try {
					$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
					$message = array(
						'html' => '<html>
    <body>
   		<div style="background:#f7f7f7;padding:20px;">
			<img src="http://spac.io/dashboard/img/spacio_logo.png" height="20">
			<div style="background:white;font-size:14px;padding:20px;margin-top:20px;">
				<p>'.$manager_result["fname"].',</p>
				<p>We received a request to reset your account password for Spacio Brokerage Admin Dashboard. If you did not make this request, please disregard this email and your password will remain the same.</p><p>Click here to reset your password: <a href="http://www.spac.io/brokerage/resetpassword.php?e='.$manager_result["_id"]->{'$id'}.'&r='.$rtoken.'">http://www.spac.io/brokerage/resetpassword.php?e='.$manager_result["_id"]->{'$id'}.'&v='.$rtoken.'</a></p><br/><p>The Spacio Team<br/>hello@spac.io</p>
			</div>
			<div style="font-size:10px;text-align:center;margin-top:20px;padding-bottom:20px;">
				Please view our <a href="http://www.spac.io/privacy/" style="color:#999;">Privacy Policy</a> and <a href="http://www.spac.io/terms/"  style="color:#999;">Terms of Service.</a><br/>
			</div>
		</div>
    </body>
</html>',
						'subject' => 'Spacio Brokerage Admin Dashboard Password Reset',
						'from_email' => 'hello@spac.io',
						'from_name' => "Spacio Pro",
						'to' => array(
							array(
								'email' => $email
							)
						),
						'headers' => array('Reply-To' => 'hello@spac.io'),
						'important' => false,
						'tags' => array('reset-pass-brokerage')

					);
					$async = false;
					$result = $mandrill->messages->send($message, $async);

				} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
					throw $e;
				}

			$return = array("status" => 1);
		} else {
			$return = array("status" => 0);
		}

		return $return;
	}

	static function editProfile($managerID,$vtoken,$fname, $lname, $title, $email, $phone, $mobile, $website, $addr1, $addr2) {
		$collection_managers = Db_Conn::getInstance()->getConnection()->brokerage_managers;

		$manager_result = $collection_managers->findOne(array('_id' => new MongoId($managerID), "vtoken" => $vtoken));
		$email_result = $collection_managers->findOne(array('email' => $_email, '_id' => array('$ne'=>new MongoId($managerID))));

		if ($rsID == "") {
			$rsID = "N/A";
		}

		if (!is_null($email_result)) {
			//Email already taken
			$return = array(
				"status" => 2
			);
		} else if (!is_null($manager_result)) {

				$newdata = array('$set' => array('fname' => $fname,'lname' => $lname,'title' => $title,'email' => $email,'phone' => $phone,'mobile' => $mobile,'website' => $website,'addr1' => $addr1, 'addr2' => $addr2));


				$u_result = $collection_managers->update(array('_id' => new MongoId($managerID)), $newdata);

				$return = array(
					"status" => 1,
				);



		} else {
			// User ID not found
			$return = array("status" => 0);
		}

		return $return;
	}

}

?>
