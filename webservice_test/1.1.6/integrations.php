<?php

require_once('../../require/db_conn.php');
// require_once('../../require/core.php');

// require_once "mailchimp/mailchimp.php";
// require_once "mailchimp/crm_api.php";

function realScoutCreateLead($authToken,$agent_email,$lead_email,$name,$addr2,$price,$beds,$baths,$sqft){

  $parts = explode(" ", $name);

  $lname = array_pop($parts);
  $fname = implode(" ", $parts);

	//sanitize addr2 to postalCode
	$addr2 = str_replace(' ','',$addr2);
	$addr2 = str_replace(',','',$addr2);
	$postalCode = substr($addr2, -5, 5);
	(is_numeric($postalCode)) ? $postalCode = (int)$postalCode : $postalCode = '';
	//build postData
	$postData = "{
		\"agent_email\": \"$agent_email\",
		\"client\": {
			\"email\": \"$lead_email\",
			\"first_name\": \"$fname\",
			\"last_name\": \"$lname\",";

	//generate postData string if postalCode exists and there exists one of the search field
	if($postalCode != '' && ($price != '' || $beds != '' || $baths != '' || $sqft != '')){

		//add postal code to postData
		$postData .= "\"saved_search\": {\"postal_code\": \"$postalCode\",";

		//add $price to postData
		if($price != ''){
			$price =	str_replace(',','',$price);
			$price =	str_replace('$','',$price);
			$price = (int)$price;
			$price_min = floor($price * 0.85);
			$price_max = ceil($price * 1.15);
			$postData .= "\"price_min\": \"$price_min\",\"price_max\": \"$price_max\",";
		}

		//add beds to postData
		if($beds != ''){
			$beds = (float)$beds;
			$beds_min = floor($beds);
			$beds_max = ceil($beds);
			$postData .= "\"beds_min\": \"$beds_min\",\"beds_max\": \"$beds_max\",";
		}

		//add baths to postData
		if($baths != ''){
			$baths = (float)$baths;
			$baths_min = floor($baths);
			$baths_max = ceil($baths);
			$postData .= "\"baths_min\": \"$baths_min\",\"baths_max\": \"$baths_max\",";
		}
		//add sqft to postData
		if($sqft != ''){
			$sqft = (int)$sqft;
			$sqft_min = floor($sqft * 0.85);
			$sqft_max = ceil($sqft * 1.15);
			$postData .= "\"sqft_min\": \"$sqft_min\",\"sqft_max\": \"$sqft_max\",";
		}

		//take out the tailing comma and attach the closing brackets to string
		$postData = substr($postData, 0, -1);
		$postData .= "}}}";
	} else {
		//take out the tailing comma and attach the closing brackets to string
		$postData = substr($postData, 0, -1);
		$postData .= "}}";
	}

	//get data
	$credential = array(
    "header" => array(
        "Authorization: Token $authToken",
        "Content-Type: application/json"
      )
    );
  $getAgents = array(
    "verb" => "POST",
    "url" => "https://www.realscout.com/api/vendor/clients",
    "postData" => $postData
     );
  $getAgentsArr = array_merge($getAgents, $credential);
  $res = crmApi($getAgentsArr);
  if(isset($res['error'])){
    //if there is an error and the reason is not because member exists
    return array(
      'error_msg' => $res['error'],
      'error_code' => $res['http_code']['http_code'],
      'status' => 0
    );
  }

  return array(
    'status' => 1,
    'msg' => 'success',
    'response' => $res
  );
}

function rewCreateLead($agent_id,$first_name,$last_name,$email,$address,$city,$state,$zip,$phone,$comments,$brokerageID){

  if($brokerageID == "justinhavre") {
    $rewAPIKey = "ed014f692e1bc13611650af8401a9400ece9b07b64eee3c243836fbaaa7da67c";
    $rewEndpoint = "http://www.justinhavre.com/api/crm/v1/";
  } else {
    $rewAPIKey = "6d34c8dd625aa7728cd440e75597b7d9b181ac559a2d540bf134013d9e38c5d4";
    $rewEndpoint = "http://dev.sutton.com/api/crm/v1/";
  }
  $credential = array(
    "User-Agent" => "myAPIClient/1.0",
    "header" => array(
      'X-REW-API-Key:'.$rewAPIKey
      )
    );

  //POST lead
  echo "Calling API to POST lead! \n";
  $postLead = array(
    "verb" => "POST",
    "url" => $rewEndpoint."leads",
    "postData" => array(
      "first_name" => $first_name,
      "last_name" => $last_name,
      "email" => $email,
      "agent_id" => $agent_id,
      "address"=> $address,
      "city" => $city,
      "state" => $state,
      "zip" => $zip,
      "phone" => $phone,
      "comments" => $comments,
      "origin" => "Spacio"
    )
  );
  $postLeadArr = array_merge($postLead, $credential);
  $response = crmApi($postLeadArr);
  if(isset($response['error']) && strpos($response['error']['message'], 'already exists')){
    //if there is an error because member exists

    //create array for updating
    $postArray = array(
      "agent_id" => $agent_id,
      "origin" => "Spacio"
    );
    ($first_name == "") ? "" : $postArray = array_merge($postArray, array("first_name" => $first_name));
    ($last_name == "") ? "" : $postArray = array_merge($postArray, array("last_name" => $last_name));
    ($address == "") ? "" : $postArray = array_merge($postArray, array("address" => $address));
    ($city == "") ? "" : $postArray = array_merge($postArray, array("city" => $city));
    ($state == "") ? "" : $postArray = array_merge($postArray, array("state" => $state));
    ($zip == "") ? "" : $postArray = array_merge($postArray, array("zip" => $zip));
    ($phone == "") ? "" : $postArray = array_merge($postArray, array("phone" => $phone));
    ($comments == "") ? "" : $postArray = array_merge($postArray, array("comments" => $comments));

    //UPDATE lead
    $updateLead = array(
      "verb" => "POST",
      "url" => $rewEndpoint."leads/{$email}",
      "postData" => $postArray
    );
    $updateLeadArr = array_merge($updateLead, $credential);
    $response = crmApi($updateLeadArr);
    if(isset($response['error'])){
      //if there is an error while updating lead
      return array(
        'error_msg' => $response['error'],
        'error_code' => $response['http_code']['http_code'],
      );
    }
  } else {
    //if there is an error while creating lead and the reason is not because member exists
    return array(
      'error_msg' => $response['error'],
      'error_code' => $response['http_code']['http_code'],
    );
  }

  return array(
    'msg' => 'success',
    'response' => $response
  );
}


function mailchimpConnect($mailchimp_key, $mailchip_username, $agent_name){
  $credential = array(
    "user" => "$mailchip_username:$mailchimp_key",
    "header" => array(
        'Content-Type: application/json'
      )
  );

  //get all the list from the user
  $get_lists = array(
    "verb" => "GET",
    "list_id" => "",
    "method" => "lists",
    "subscriber_hash" => "",
    "merge_id" => "",
    "postData" => ""
  );
  $get_lists_url = array(
    "url" => mailchimp($get_lists,$mailchimp_key)
  );
  $getListArr = array_merge($get_lists, $get_lists_url, $credential);
  $allLists = crmApi($getListArr);
  if(isset($allLists['error'])){
    return array(
      'error_type' => $allLists['type'],
      'error_status' => $allLists['status'],
      'error_detail' => $allLists['detail'],
      'status' => 0
    );
  }

  //loop through all lists and find Spacio
  foreach($allLists['lists'] as $list){
    ($list['name'] == 'Spacio') ? $newListID = $list['id'] : "";
  }

  //check if Spacio list exists in user account
  if(isset($newListID)){
    //there is Spacio list
    $msg = 'Spacio list already exists!';

  } else{
    //there is no Spacio list
    //get account contact information
    $get_root = array(
      "verb" => "GET",
      "method" => "root",
      "list_id" => "",
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => ""
    );
    $get_root_url = array(
      "url" => mailchimp($get_root,$mailchimp_key)
    );
    $getAccArr = array_merge($get_root, $get_root_url, $credential);
    $accInfo = crmApi($getAccArr);
    $contact = $accInfo['contact'];
    $contact = array_merge($contact, array('address1'=>$contact['addr1']));
    $contact = array_merge($contact, array('address2'=>$contact['addr2']));
    $contact = json_encode($contact);

    //create Spacio list
    $post_list = array(
      "verb" => "POST",
      "method" => "lists",
      "list_id" => "",
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => '{
          "name":"Spacio",
          "contact": '.$contact.',
          "permission_reminder":".",
          "use_archive_bar":TRUE,
          "campaign_defaults":{
              "from_name":"'.$agent_name.'",
              "from_email":"'.$accInfo['email'].'",
              "subject":"",
              "language":"en"
              },
          "notify_on_subscribe":"",
          "notify_on_unsubscribe":"",
          "email_type_option":true,
          "visibility":"prv"
        }'
    );
    $post_list_url = array(
      "url" => mailchimp($post_list,$mailchimp_key)
    );
    $postListArr = array_merge($post_list, $post_list_url, $credential);
    $newList = crmApi($postListArr);
    if(isset($newList['error'])){
      return array(
        'error_type' => $newList['type'],
        'error_status' => $newList['status'],
        'error_detail' => $newList['detail'],
        'status' => 0
      );
    }
    $newListID = $newList['id'];

    //add phone field
    $post_phone = array(
      "verb" => "POST",
      "method" => "lists/list_id/merge-fields",
      "list_id" => $newListID,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => '{
          "tag":"PHONE",
          "name":"PHONE",
          "type":"text",
          "required":FALSE
          }'
    );
    $post_phone_url = array(
      "url" => mailchimp($post_phone,$mailchimp_key)
    );
    $postPhoneArr = array_merge($post_phone, $post_phone_url, $credential);
    $newPhoneField = crmApi($postPhoneArr);
    if(isset($newPhoneField['error'])){
      return array(
        'error_type' => $newPhoneField['type'],
        'error_status' => $newPhoneField['status'],
        'error_detail' => $newPhoneField['detail'],
        'status' => 0
      );
    }

    //add listing_address field
    $post_listing_addr = array(
      "verb" => "POST",
      "method" => "lists/list_id/merge-fields",
      "list_id" => $newListID,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => '{
          "tag":"PROP_ADDR",
          "name":"PROPERTY ADDRESS",
          "type":"text",
          "required":FALSE
          }'
    );
    $post_listing_addr_url = array(
      "url" => mailchimp($post_listing_addr,$mailchimp_key)
    );
    $postListingAddrArr = array_merge($post_listing_addr, $post_listing_addr_url, $credential);
    $newListingAddrField = crmApi($postListingAddrArr);
    if(isset($newListingAddrField['error'])){
      return array(
        'error_type' => $newListingAddrField['type'],
        'error_status' => $newListingAddrField['status'],
        'error_detail' => $newListingAddrField['detail'],
        'status' => 0
      );
    }
    $msg = 'List successfully created!';
  }

  return array(
    'msg' => $msg,
    'newListID' => $newListID,
    'status' => 1
  );
}

function mailchimpCreateLead($mailchimp_key,$mailchip_username,$list_id,$fname,$lname,$phone,$listing_address,$questions,$email){

  $credential = array(
    "user" => "$mailchip_username:$mailchimp_key",
    "header" => array(
        'Content-Type: application/json'
      )
  );

  //add member(aka lead) into list
  $post_member = array(
    "verb" => "POST",
    "method" => "lists/id/members",
    "list_id" => $list_id,
    "subscriber_hash" => "",
    "merge_id" => "",
    "postData" => '{
      "email_type":"",
      "status":"subscribed",
      "merge_fields":{
          "FNAME":"'.$fname.'",
          "LNAME":"'.$lname.'",
          "PHONE":"'.$phone.'",
          "PROP_ADDR":"'.$listing_address.'"
          },
      "email_address":"'.$email.'"
      }'
  );
  $post_member_url = array(
    "url" => mailchimp($post_member,$mailchimp_key)
  );
  $postMemberArr = array_merge($post_member, $post_member_url, $credential);
  $lead = crmApi($postMemberArr);
  if(isset($lead['error']) && $lead['title'] != 'Member Exists'){
    //if there is an error and the reason is not because member exists
    return array(
      'error_type' => $lead['type'],
      'error_status' => $lead['status'],
      'error_detail' => $lead['detail'],
      'status' => 0
    );
  } elseif (isset($lead['error']) && $lead['title'] == 'Member Exists'){
    //member exists, change to update member
    //find subscriber_hash (member id) of existing member
    $get_members = array(
      "verb" => "GET",
      "method" => "lists/list_id/members",
      "list_id" => $list_id,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => ""
    );
    $get_members_url = array(
      "url" => mailchimp($get_members,$mailchimp_key)
    );
    $getMemberArr = array_merge($get_members, $get_members_url, $credential);
    $allMembers = crmApi($getMemberArr);
    if(isset($allMembers['error'])){
      return array(
        'error_type' => $allMembers['type'],
        'error_status' => $allMembers['status'],
        'error_detail' => $allMembers['detail'],
        'status' => 0
      );
    }
    //loop through all members and find email and subscriber_hash
    foreach($allMembers['members'] as $member){
      ($member['email_address'] == $email) ? $subscriber_hash = $member['id'] : "";
    }

    //get member original information
    $get_member = array(
      "verb" => "GET",
      "method" => "lists/list_id/members",
      "list_id" => $list_id,
      "subscriber_hash" => "",
      "merge_id" => "",
      "postData" => ""
    );
    $get_member_url = array(
      "url" => mailchimp($get_member,$mailchimp_key)
    );
    $getMemberArr = array_merge($get_member, $get_member_url, $credential);
    $memberInfo = crmApi($getMemberArr);
    if(isset($memberInfo['error'])){
      return array(
        'error_type' => $memberInfo['type'],
        'error_status' => $memberInfo['status'],
        'error_detail' => $memberInfo['detail'],
        'status' => 0
      );
    }
    //use old information if no new info is provided
    ($fname == "") ? $fname = $memberInfo['members'][0]['merge_fields']['FNAME'] : "" ;
    ($lname == "") ? $lname = $memberInfo['members'][0]['merge_fields']['LNAME'] : "" ;
    ($phone == "") ? $phone = $memberInfo['members'][0]['merge_fields']['PHONE'] : "" ;
    ($listing_address == "") ? $listing_address = $memberInfo['members']['merge_fields']['PROP_ADDR'] : "" ;

    //update member
    $patch_member = array(
      "verb" => "PATCH",
      "method" => "lists/list_id/members/subscriber_hash",
      "list_id" => $list_id,
      "subscriber_hash" => $subscriber_hash,
      "merge_id" => "",
      "postData" => '{
        "merge_fields":{
          "FNAME":"'.$fname.'",
          "LNAME":"'.$lname.'",
          "PHONE":"'.$phone.'",
          "PROP_ADDR":"'.$listing_address.'"
          }
        }'
    );
    $patch_member_url = array(
      "url" => mailchimp($patch_member,$mailchimp_key)
    );
    $patchMemberArr = array_merge($patch_member, $patch_member_url, $credential);
    $patchMember = crmApi($patchMemberArr);
    if(isset($patchMember['error'])){
      return array(
        'error_type' => $patchMember['type'],
        'error_status' => $patchMember['status'],
        'error_detail' => $patchMember['detail'],
        'status' => 0
      );
    }

    $msg = 'Lead successfully updated';
  } else {
    //no error and member does not exist
    $msg = 'Lead successfully created!';
    $subscriber_hash = $lead['id'];
  }

  //post questions in notes if note is provided
  if($questions != ""){
    $post_note = array(
      "verb" => "POST",
      "method" => "lists/list_id/members/subscriber_hash/notes",
      "list_id" => $list_id,
      "subscriber_hash" => $subscriber_hash,
      "merge_id" => "",
      "postData" => '{"note":"'.$questions.'"}'
    );
    $post_note_url = array(
      "url" => mailchimp($post_note,$mailchimp_key)
    );
    $postNoteArr = array_merge($post_note, $post_note_url, $credential);
    $newNote = crmApi($postNoteArr);
    if(isset($newNote['error'])){
      return array(
        'error_type' => $newNote['type'],
        'error_status' => $newNote['status'],
        'error_detail' => $newNote['detail'],
        'status' => 0
      );
    }
  }

  return array(
    'msg' => $msg,
    'subscriber_hash' => $subscriber_hash,
    'status' => 1
  );
}

function createContactuallyLead($email,$phone,$contactually_bucket_id,$access_token,$answers_combined,$listing_address,$f_name,$l_name){

  $url = "https://api.contactually.com/v2/contacts/";

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
  curl_setopt($ch,CURLOPT_POST,1);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

  $headers = array(
    'Content-Type: application/json',
    'Authorization: Bearer '.$access_token
  );

  if($email == "") {
    $email_array = array();

  } else {
    $email_array = array(array(
     "label" => "Visitor",
     "address" => $email
   ));
  }


  $phone_array = array(array(
    "label" => "Personal",
    "number" => $phone
  ));

  $data = array(
    "data" => array(
      "first_name" => $f_name,
      "last_name" => $l_name,
      "company" => "",
      "title" => "",
      "email_addresses" => $email_array,
      "phone_numbers" => $phone_array,
      "tags" => array('Spacio', $listing_address)
  ));



  $data_fields = json_encode($data);

  curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
  curl_setopt($ch,CURLOPT_POSTFIELDS,$data_fields);
  $server_output = curl_exec($ch);
  curl_close($ch);



  $res = json_decode($server_output,true);
  $contact_id = "";
  if(array_key_exists("data", $res)) {
    if(array_key_exists("id", $res["data"])) {
      $contact_id = $res['data']['id'];
    }
  }

  if($contactually_bucket_id != "" && !is_null($contact_id)) {
    addLeadtoContactuallyBucket($contact_id,$access_token,$contactually_bucket_id);
    addNotetoContactuallyContact($contact_id,$access_token,$answers_combined,$listing_address);
  }

  return $contact_id;
  //addNotetoContactuallyContact($contact_id,$application_id,$access_token,$answers_combined,$listing_address);
}

function addLeadtoContactuallyBucket($contact_id,$access_token,$contactually_bucket_id){
  $url = "https://api.contactually.com/v2/buckets/".$contactually_bucket_id."/contacts";

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
  curl_setopt($ch,CURLOPT_POST,1);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

  $headers = array(
    'Content-Type: application/json',
    'Authorization: Bearer '.$access_token
  );


  $data = array(
    "data" => array(
      "id" => $contact_id
  ));

  $data_fields = json_encode($data);

  curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
  curl_setopt($ch,CURLOPT_POSTFIELDS,$data_fields);
  $server_output = curl_exec($ch);
  curl_close($ch);
}

function addNotetoContactuallyContact($contact_id,$access_token,$answers_combined,$listing_address){

  $url = "https://api.contactually.com/v2/interactions/";

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
  curl_setopt($ch,CURLOPT_POST,1);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

  $headers = array(
    'Content-Type: application/json',
    'Authorization: Bearer '.$access_token
  );

  $body = "Attended a Spacio Open House\nProperty Address:".$listing_address."\nForm Answers:\n".$answers_combined;

  $participants = array(
    'contact_id' => $contact_id
  );

  $data = array(
    "data" => array(
      'body' => $body,
      'initiated_by_contact' => false,
      'subject' => "Spacio Open House: ".$listing_address,
      'type' => "other",
      "participants" => $participants
  ));

  $data_fields = json_encode($data);

  curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
  curl_setopt($ch,CURLOPT_POSTFIELDS,$data_fields);
  $server_output = curl_exec($ch);
  curl_close($ch);
}

function sendMoxiPresentPresentation($manager,$property, $email, $name) {

  $company = "";
  if($manager["brokerage"] != "") {
    $company = $manager["brokerage"]."<br/>";
  }

  try {
		$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
		$message = array(
			'html' => $name.",<br/><br/>Thank you for attending my open house for ".$property['title'].".<br/><Br/>Here is the digital presentation you requested: ".$property["moxipresentURL"]."<br/><br/>If there's anything else I can help you with, please let me know.<br/><br/><br/>".$manager['fname']." ".$manager['lname']."<br/>".$company."".$manager["email"]."<br/>".$manager["phone"],
			'subject' => 'Your Digital Presentation for '.$property["title"],
			'from_email' => 'hello@spac.io',
			'from_name' => $manager['fname'].' '.$manager['lname'],
			'to' => array(
				array(
					'email' => $email,
          'type' => 'to'
				),array(
					'email' => $manager["email"],
          'type' => 'bcc'
				)
			),
			'headers' => array('Reply-To' => $manager["email"]),
			'important' => false,
			'tags' => array('moxipresent-presentation-email')
		);
		$async = false;
		$result = $mandrill->messages->send($message, $async);

	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		throw $e;
	}
}

function sendTouchCMAPresentation($manager,$property, $email, $name) {
  $company = "";
  if($manager["brokerage"] != "") {
    $company = $manager["brokerage"]."<br/>";
  }

  try {
		$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
		$message = array(
			'html' => $name.",<br/><br/>Thank you for attending my open house for ".$property['title'].".<br/><Br/>Here is the digital presentation you requested: ".$property["touchcmaURL"]."<br/><br/>If there's anything else I can help you with, please let me know.<br/><br/><br/>".$manager['fname']." ".$manager['lname']."<br/>".$company."".$manager["email"]."<br/>".$manager["phone"],
			'subject' => 'Your Digital Presentation for '.$property["title"],
			'from_email' => 'hello@spac.io',
			'from_name' => $manager['fname'].' '.$manager['lname'],
			'to' => array(
				array(
					'email' => $email,
          'type' => 'to'
				),array(
					'email' => $manager["email"],
          'type' => 'bcc'
				)
			),
			'headers' => array('Reply-To' => $manager["email"]),
			'important' => false,
			'tags' => array('moxipresent-presentation-email')
		);
		$async = false;
		$result = $mandrill->messages->send($message, $async);

	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		throw $e;
	}
}

function sendProPresentation($manager,$property, $email, $name) {

  $company = "";
  if($manager["brokerage"] != "") {
    $company = $manager["brokerage"]."<br/>";
  }

  try {
		$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
		$message = array(
			'html' => $name.",<br/><br/>Thank you for attending my open house for ".$property['title'].".<br/><Br/>Here is the digital presentation you requested: ".$property["presentationproURL"]."<br/><br/>If there's anything else I can help you with, please let me know.<br/><br/><br/>".$manager['fname']." ".$manager['lname']."<br/>".$company."".$manager["email"]."<br/>".$manager["phone"],
			'subject' => 'Your Digital Presentation for '.$property["title"],
			'from_email' => 'hello@spac.io',
			'from_name' => $manager['fname'].' '.$manager['lname'],
			'to' => array(
				array(
					'email' => $email,
          'type' => 'to'
				),array(
					'email' => $manager["email"],
          'type' => 'bcc'
				)
			),
			'headers' => array('Reply-To' => $manager["email"]),
			'important' => false,
			'tags' => array('presentationpro-email')
		);
		$async = false;
		$result = $mandrill->messages->send($message, $async);

	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		throw $e;
	}
}

function sendCloudCMAReport($manager,$property, $email, $name) {
  $company = "";
  if($manager["brokerage"] != "") {
    $company = $manager["brokerage"]."<br/>";
  }


  try {
		$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
		$message = array(
			'html' => $name.",<br/><br/>Thank you for attending my open house for ".$property['title'].".<br/><Br/>Here is the Property Report you requested: ".$property["cloudcmaPDF"]."<br/><br/>If there's anything else I can help you with, please let me know.<br/><br/><br/>".$manager['fname']." ".$manager['lname']."<br/>".$company."".$manager["email"]."<br/>".$manager["phone"],
			'subject' => 'Your Property Report from '.$property["title"],
			'from_email' => 'hello@spac.io',
			'from_name' => $manager['fname'].' '.$manager['lname'],
			'to' => array(
				array(
					'email' => $email,
          'type' => 'to'
				),array(
					'email' => $manager["email"],
          'type' => 'bcc'
				)
			),
			'headers' => array('Reply-To' => $manager["email"]),
			'important' => false,
			'tags' => array('cloudcma-pdf-email')
		);
		$async = false;
		$result = $mandrill->messages->send($message, $async);

	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		throw $e;
	}
}

function relianceSendLead($relianceID, $name, $email, $phone, $answers_combined, $agent_email, $property_addr, $dateCreated) {

  $relianceURL = "a".$relianceID."@leadgen.reliancenetwork.com";
  $mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
	$message = array(
		'html' => '<html>
		<head>
								<meta name="lead_information_version" content="1.0">
		<meta name="lead_count" content="1">
								<meta name="lead_source" content="Spacio">
								<meta name="lead_type" content="Buyer">
								<meta name="lead_name" content="'.$name.'">
								<meta name="lead_email" content="'.$email.'">
								<meta name="lead_phone" content="'.$phone.'">
								<meta name="lead_message" content="'.$answers_combined.'">
								<meta name="lead_property_address" content="'.$property_addr.'">
								<meta name="lead_agent_email" content="'.$agent_email.'">
                <meta name="lead_date_created" content="'.$dateCreated.'">
		</head>

		<body>
								Lead Source:Spacio<br>
								Lead Type:Buyer<br>
								Lead Name:'.$name.'<br>
								Lead Email:'.$email.'<br>
								Lead phone: '.$phone.'<br>
								Lead Message: '.$answers_combined.'<br>
								Lead Property Address: '.$property_addr.' <br>
								Lead Agent Email:'.$agent_email.'<br/>
                Lead Date Created:'.$dateCreated.'
		</body>
</html>',
		'subject' => 'Spacio Lead',
		'from_email' => 'hello@spac.io',
		'from_name' => 'Spacio',
		'to' => array(
			array(
				'email' => $relianceURL,
				'type' => 'to'
			)
		),
		'headers' => array('Reply-To' =>"ting@spac.io"),
		'important' => false,
		'tags' => array('reliance-lead-email')

	);
	$async = true;
	$result = $mandrill->messages->send($message, $async);

  return $result;
}

function getDocuSignUrl($rid, $name, $email, $aname, $pname, $ndate, $file, $subject) {
    $docusign_return = array();
    // DocuSign account credentials & Integrator Key
    $username = "b4153bbe-daf4-4143-946a-7bcb17b6343d"; // CHANGE!
    $password = 'lCnit3L2w9'; // CHANGE!
    $integrator_key = "131b531e-1489-46e4-975c-54d268c757e1"; // CHANGE!
    // Alt test key: 95db09c5-f1c1-42b8-85ab-56838766fbe7
    $host = "https://www.docusign.net/restapi"; // CHAxNGE!
    // create a new DocuSign configuration and assign host and header(s)
    $config = new DocuSign\eSign\Configuration();
    $config->setHost($host);
    $config->addDefaultHeader("X-DocuSign-Authentication", "{\"Username\":\"" . $username . "\",\"Password\":\"" . $password . "\",\"IntegratorKey\":\"" . $integrator_key . "\"}");
    // Throws exception with following message: '[401] Error connecting to the
    // API (https://demo.docusign.net/restapi/v2/login_information)' if API down
    // or creds incorrect
    try {
    /////////////////////////////////////////////////////////////////////////
    // STEP 1:  Login() API
    /////////////////////////////////////////////////////////////////////////
    // instantiate a new docusign api client
      $apiClient = new DocuSign\eSign\ApiClient($config);
      // we will first make the Login() call which exists in the AuthenticationApi...
      $authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);
      // optional login parameters
      $options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();
      // call the login() API
      $loginInformation = $authenticationApi->login($options);
      // parse the login results
      if(isset($loginInformation) && count($loginInformation) > 0)
      {
          // note: defaulting to first account found, user might be a
          // member of multiple accounts
          $loginAccount = $loginInformation->getLoginAccounts()[0];
          if(isset($loginInformation))
          {
              $accountId = $loginAccount->getAccountId();
              if(!empty($accountId))
              {

                  //echo "Account ID = $accountId\n";
              }
          }
      }
      /////////////////////////////////////////////////////////////////////////
      // STEP 2:  Create & Send Envelope with Embedded Recipient
      /////////////////////////////////////////////////////////////////////////
      // set recipient information
      $recipientName = $name; // CHANGE!
      $recipientEmail = $email; // CHANGE!
      // configure the document we want signed
      $documentFileName = "/bhhsgeorgia_nda.pdf"; // CHANGE!
      $documentName = $file; // CHANGE!
      // instantiate a new envelopeApi object
      $envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);
      // Add a document to the envelope
      $document = new DocuSign\eSign\Model\Document();
      $document->setDocumentBase64(base64_encode(file_get_contents(__DIR__ . $documentFileName)));
      $document->setName($documentName);
      $document->setDocumentId("1");
      // Custom fields on form
      $to = new \DocuSign\eSign\Model\Text();
      $to->setXPosition("75");
      $to->setYPosition("70");
      $to->setDocumentId("1");
      $to->setPageNumber("1");
      $to->setRecipientId("1");
      $to->setValue($aname);
      $to->setRequired("false");

      $from = new \DocuSign\eSign\Model\Text();
      $from->setXPosition("75");
      $from->setYPosition("85");
      $from->setDocumentId("1");
      $from->setPageNumber("1");
      $from->setRecipientId("1");
      $from->setValue($name);
      $from->setRequired("false");

      $prop = new \DocuSign\eSign\Model\Text();
      $prop->setXPosition("360");
      $prop->setYPosition("70");
      $prop->setDocumentId("1");
      $prop->setPageNumber("1");
      $prop->setRecipientId("1");
      $prop->setValue($pname);
      $prop->setRequired("false");

      $date = new \DocuSign\eSign\Model\Text();
      $date->setXPosition("360");
      $date->setYPosition("85");
      $date->setDocumentId("1");
      $date->setPageNumber("1");
      $date->setRecipientId("1");
      $date->setValue($ndate);
      $date->setRequired("false");

      $date2 = new \DocuSign\eSign\Model\Text();
      $date2->setXPosition("185");
      $date2->setYPosition("670");
      $date2->setDocumentId("1");
      $date2->setPageNumber("1");
      $date2->setRecipientId("1");
      $date2->setValue($ndate);
      $date2->setRequired("false");

      // $text->setLocked("true");
      // $tabs = new DocuSign\eSign\Model\Tabs();

      // Create a |SignHere| tab somewhere on the document for the recipient to sign
      $signHere = new \DocuSign\eSign\Model\SignHere();
      $signHere->setXPosition("40");
      $signHere->setYPosition("635");
      $signHere->setDocumentId("1");
      $signHere->setPageNumber("1");
      $signHere->setRecipientId("1");




      // $date3->setConditionalParentValue("On");
      // add the signature tab to the envelope's list of tabs
      $tabs = new DocuSign\eSign\Model\Tabs();
      $tabs->setSignHereTabs([$signHere]);
      $tabs->setTextTabs([$to, $from, $prop, $date, $date2]);
      // add a signer to the envelope
      $signer = new \DocuSign\eSign\Model\Signer();
      $signer->setEmail($recipientEmail);
      $signer->setName($recipientName);
      $signer->setRecipientId("1");
      $signer->setTabs($tabs);
      $signer->setClientUserId($rid);  // must set this to embed the recipient! // CHANGE!
      // Add a recipient to sign the document
      $recipients = new DocuSign\eSign\Model\Recipients();
      $recipients->setSigners(array($signer));
      $envelop_definition = new DocuSign\eSign\Model\EnvelopeDefinition();
      $envelop_definition->setEmailSubject($subject); // CHANGE!
      // set envelope status to "sent" to immediately send the signature request
      $envelop_definition->setStatus("sent");
      $envelop_definition->setRecipients($recipients);
      $envelop_definition->setDocuments(array($document));
      // create and send the envelope! (aka signature request)
      $envelop_summary = $envelopeApi->createEnvelope($accountId, $envelop_definition, null);
      //echo "$envelop_summary\n";
      /////////////////////////////////////////////////////////////////////////
      // STEP 3:  Request Recipient View (aka signing URL)
      /////////////////////////////////////////////////////////////////////////
      // instantiate a RecipientViewRequest object
      $recipient_view_request = new \DocuSign\eSign\Model\RecipientViewRequest();
      // set where the recipient is re-directed once they are done signing
      $recipient_view_request->setReturnUrl("http://ws.spac.io/prod/oh/webhooks/docusign/?rid=$rid"); // CHANGE!
      // STATUSES
      // - adds ?event=signing_complete to return URL on successful sign
      // - adds ?event=decline on decline
      // - adds ?event=cancel for "Finish Later"
      // - adds ?event=ttl_expired if returning to stale form

      // configure the embedded signer
      $recipient_view_request->setUserName($recipientName);
      $recipient_view_request->setEmail($recipientEmail);
      // must reference the same clientUserId that was set for the recipient when they
      // were added to the envelope in step 2
      $recipient_view_request->setClientUserId($rid); // CHANGE!
      // used to indicate on the certificate of completion how the user authenticated
      $recipient_view_request->setAuthenticationMethod("email");
      // generate the recipient view! (aka embedded signing URL)
      $signingView = $envelopeApi->createRecipientView($accountId, $envelop_summary->getEnvelopeId(), $recipient_view_request);
      $docusign_return["status"] = 1;
      $docusign_return["docusign_url"] = $signingView->getUrl();

      return $docusign_return;

    } catch (DocuSign\eSign\ApiException $ex) {
      $docusign_return["status"] = 2;
      $docusign_return["error"] = $ex->getResponseBody()->errorCode;

      return $docusign_return;

    }
}

/*
function addCustomNotetoContactuallyContact($contact_id,$application_id,$access_token,$listing_address,$notes){

  $client_id = $application_id;
  $url = "https://api.contactually.com/v2/interactions/";

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
  curl_setopt($ch,CURLOPT_POST,1);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

  $headers = array(
    'Content-Type: application/json',
    'Authorization: Bearer '.$access_token
  );

  $body = "Notes from ".$listing_address." visit \nNotes:\n".$notes;

  $participants = array(
    'contact_id' => $contact_id
  );

  $data = array(
    "data" => array(
      'body' => $body,
      'initiated_by_contact' => false,
      'subject' => "Notes from: ".$listing_address,
      'type' => "other",
      "participants" => $participants
  ));

  $data_fields = json_encode($data);

  curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
  curl_setopt($ch,CURLOPT_POSTFIELDS,$data_fields);
  $server_output = curl_exec($ch);
  curl_close($ch);
  echo($server_output);
}*/


?>
