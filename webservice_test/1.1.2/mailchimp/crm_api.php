<?php
function crmApi($arr){

  $ch = curl_init($arr['url']);
  if(isset($arr['header'])){
    curl_setopt($ch, CURLOPT_HTTPHEADER, $arr['header']);
  }
  // curl_setopt($process, CURLOPT_TIMEOUT, 60);
  if(isset($arr['user'])){
    curl_setopt($ch, CURLOPT_USERPWD, $arr['user']);
  }

  switch($arr['verb']){
    case "GET" :
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      break;

    case ("POST") :
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $arr['postData']);
      break;

    case ("PATCH") :
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $arr['postData']);
      break;

    case ("PUT") :
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $arr['postData']);
      break;

    case ("DELETE") :
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
      break;
  }

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $res = curl_exec($ch);
  $res = json_decode($res,true);
  $info = curl_getinfo($ch);
  if(isset($info['http_code']) && $info['http_code'] >= 300){
    //echo 'an error has occured';
    $res["error"] = $info;
  } else{
    //echo "reponse ok!";
  }

  curl_close($ch);
  $res["input_params"] = $arr;
  return $res;

}

?>
