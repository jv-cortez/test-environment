<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Content-type: application/json');

require_once('../../require/mandrill/src/Mandrill.php');
require_once('../../require/spacio_db_config.php');
require_once('../../require/simple_html_dom.php');
require_once('../../core.php');
require_once('../../parsers/generic.php');
require_once('require/vendor/willdurand/email-reply-parser/src/autoload.php');

use EmailReplyParser\Parser\EmailParser;

// $result = stripslashes_deep($_POST['mandrill_events']);
$result = $_POST['mandrill_events'];

//$contents = utf8_encode($result);
$obj = json_decode($result);//, true);
$fromEmail = $obj[0]->msg->from_email;
$toTempEmail = $obj[0]->msg->email;
$html = $obj[0]->msg->html;
$text = $obj[0]->msg->text;
$subject = $obj[0]->msg->subject;
$fromName = $obj[0]->msg->from_name;

$did = substr($toTempEmail, 0, -15);
$email = new EmailParser;
$emailParse = $email->parse($html);


// Configuration
	$dbhost1 = DB_HOST_1;
	$dbhost2 = DB_HOST_2;
	$dbhost3 = DB_HOST_3;
	$dbrset = DB_RSET;
	$dbport = DB_PORT;
	$dbname = DB_NAME;
	$dbuser = DB_USER;
	$dbpass = DB_PASS;

// Connect to spacio database
	$m = new MongoClient("mongodb://$dbuser:$dbpass@$dbhost1:$dbport,$dbhost2:$dbport,$dbhost3:$dbport/$dbname?replicaSet=$dbrset");
	$db = $m->$dbname;

// Get the clippings collection
	$collection = $db->discussions;
	$collection_whitelist = $db->whitelist;
	$collection_blacklist = $db->blacklist;
	$collection_clippings = $db->spaces;
	$collection_users = $db->users;
	$collection_transactions = $db->transactions;
	$collection_hoftd = $db->hoftd;


	//Find the Discussion object
	$discussion_result = $collection->findOne(array('_id' => new MongoId($did)));
	if (!is_null($discussion_result) && $discussion_result["status"] == "active") {
		$senderID = $discussion_result["senderID"];
		$user_result = $collection_users->findOne(array('_id' => new MongoId($senderID)));

		if ($fromEmail == $discussion_result["recipientEmail"]) {
			$fromTempEmail = $discussion_result["rTempEmail"];
			sendPushNewMessage($discussion_result["senderID"],$discussion_result["clippingID"]);

			$clipping_result = $collection_clippings->findOne(array('_id' => new MongoId($discussion_result["clippingID"])));
			if (!is_null($clipping_result)) {
				$date = new MongoDate();
				$newdata = array('$set' => array('newmessage' => 'YES','lastEdited' => $date));
				$u_result = $collection_clippings->update(array('_id' => new MongoId($discussion_result["clippingID"])), $newdata);
			}

		} else if ($fromEmail == $user_result["email"]){
			$fromTempEmail = $discussion_result["sTempEmail"];
		} else {
			$fromTempEmail = "N/A";
		}

		if ($toTempEmail == $discussion_result["rTempEmail"]) {
			$toEmail = $discussion_result["recipientEmail"];
		} else if ($toTempEmail == $discussion_result["sTempEmail"]){
			$toEmail = $user_result["email"];
		} else {
			$toEmail = "N/A";
		}

		if ($toEmail != "N/A" && $fromTempEmail != "N/A"){
			$date = new MongoDate();
			$actual_fromEmail = $fromTempEmail;
			$actual_fromName = $fromName;

			$disc_fromName = $fromEmail;

			if ($fromTempEmail == $discussion_result["rTempEmail"]) {
				$actual_fromEmail = $fromEmail;

			}

			if ($fromTempEmail == $discussion_result["sTempEmail"]) {
				$actual_fromName = "Spacio User: ".$user_result["uname"];
				$disc_fromName = "Me";
			}

			$clean_text = preg_replace('/(^\w.+:\n)?(^>.*(\n|$))+/mi', '', $text);

			$clean_text = preg_replace('/\nOn(.*?)<(.*?)>(.*?)wrote:(.*?)$/si', '', $clean_text);

			//$clean_text = strip_tags($clean_text);

			$new_message = array (
				"datetimestamp" => $date,
				"sender" => $disc_fromName,
				"content" => $clean_text
			);

			$new_message_payload = $discussion_result["messages"];
			array_push($new_message_payload, $new_message);

			$newdata = array('$set' => array('messages' => $new_message_payload));
			$u_result = $collection->update(array('_id' => new MongoId($did)), $newdata);

			try {
				$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
				$message = array(
					'html' => $html,
					'subject' => $subject,
					'from_email' => $actual_fromEmail,
					'from_name' => $actual_fromName,
					'to' => array(
						array(
							'email' => $toEmail
							)
						),
					'headers' => array('Reply-To' => $fromTempEmail),
					'important' => false,
					'tags' => array('discussion-thread')
					);
				$async = false;
				$result = $mandrill->messages->send($message, $async);

			} catch(Mandrill_Error $e) {
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
				throw $e;
			}

		} else if($fromTempEmail == "N/A") {
			try {
				$mandrill = new Mandrill('JA1OAf9gtDpJT9T6p-YvGQ');
				$message = array(
					'html' => '<p>Sorry, you do not have permission to join in on this discussion.</p><div style="margin-top:20px"><img src="http://dash.spac.io/img/spacio_logo.png" style="width:100px;height:auto;"></div>',
					'subject' => "Invalid permission",
					'from_email' => "no-reply@spac.io",
					'from_name' => "Spacio Messaging System",
					'to' => array(
						array(
							'email' => $fromEmail
							)
						),
					'headers' => array('Reply-To' => "no-reply@spac.io"),
					'important' => false,
					'tags' => array('invalid-permission')
					);
				$async = false;
				$result = $mandrill->messages->send($message, $async);

			} catch(Mandrill_Error $e) {
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
				throw $e;
			}
		}


	} else {
		//Do nothing

	}



?>
