<?php
//get url, APIKey and credential
function mailchimp($arg,$mailchimp_key){
  $ROOT_URL = "https://".substr($mailchimp_key,-3).".api.mailchimp.com/3.0";

  $url = array(
    "GET" => array(
      "lists/list_id/merge-fields/merge_id" => "{$ROOT_URL}/lists/{$arg['list_id']}/merge-fields/{$arg['merge_id']}", //Get a specific merge field
      "lists/list_id/merge-fields" => "{$ROOT_URL}/lists/{$arg['list_id']}/merge-fields", //Get all merge fields for a list
      "lists/list_id/members/subscriber_hash" => "{$ROOT_URL}/lists/{$arg['list_id']}/members/{$arg['subscriber_hash']}", //Get information about a specific list member
      "lists/list_id/members" => "{$ROOT_URL}/lists/{$arg['list_id']}/members", //Get information about members in a list
      "lists/list_id" => "{$ROOT_URL}/lists/{$arg['list_id']}", //info of id list
      "lists" => "{$ROOT_URL}/lists", //info all lists
      "root" => "{$ROOT_URL}/" //account info
    ),
    "POST" => array(
      "lists/list_id/members/subscriber_hash/notes" => "{$ROOT_URL}/lists/{$arg['list_id']}/members/{$arg['subscriber_hash']}/notes",
      "lists/list_id/merge-fields" => "{$ROOT_URL}/lists/{$arg['list_id']}/merge-fields", //Add a new merge field
      "lists/id/members" => "{$ROOT_URL}/lists/{$arg['list_id']}/members", // Add a new list member
      "lists" => "{$ROOT_URL}/lists" //new list
    ),
    "PATCH" => array(
      "lists/list_id/merge-fields/merge_id" => "{$ROOT_URL}/lists/{$arg['list_id']}/merge-fields/{$arg['merge_id']}", //	Update a merge field
      "lists/list_id/members/subscriber_hash" => "{$ROOT_URL}/lists/{$arg['list_id']}/members/{$arg['subscriber_hash']}", //Update a list member
      "lists/list_id" => "{$ROOT_URL}/lists/{$arg['list_id']}" 	//Update a specific list
    ),
    "PUT" => array(
      "lists/list_id/members/subscriber_hash" => "{$ROOT_URL}/lists/{$arg['list_id']}/members/{$arg['subscriber_hash']}" //Add or update a list member
    ),
    "DELETE" => array(
      "lists/list_id/merge-fields/merge_id" => "lists/{$arg['list_id']}/merge-fields/{$arg['merge_id']}", //	Delete a merge field
      "lists/list_id/members/subscriber_hash" => "{$ROOT_URL}/lists/{$arg['list_id']}/members/{$arg['subscriber_hash']}", //Remove a list member
      "lists/list_id" => "{$ROOT_URL}/lists/{$arg['list_id']}" //Delete a list
    )
  );
  return $url[$arg['verb']][$arg['method']];

  };

?>
