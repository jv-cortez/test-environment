// JavaScript Document

/* GENERAL FUNCTIONS
----------------------------------------------------------------------------*/

function showIABMenu() {
	if(user["brokerageID"] == "spacio") {
		$(".iab-overlay").show();
	}
}

function hideIABMenu() {
	if(user["brokerageID"] == "spacio") {
		$(".iab-overlay").hide();
	}
}

function toggleDebug() {
	clearTimeout(debugTimer);
	debugTimer = setTimeout(function(){
		debugCount = 0;
		debug = false;
	},500);
	debugCount++;
	if(debugCount > 1) {
		$("#email").prop("type","text");
		clearTimeout(debugTimer);
		debug = true;
		$(".debug-toggle i").show();
	}
}

function capitalizeFirstLetterEachWordSplitBySpace(string){
	var words = string.split(" ");
	var output = "";
	for (i = 0 ; i < words.length; i ++){
	lowerWord = words[i].toLowerCase();
	lowerWord = lowerWord.trim();
	capitalizedWord = lowerWord.slice(0,1).toUpperCase() + lowerWord.slice(1);
	output += capitalizedWord;
	if (i != words.length-1){
	output+=" ";
	}
	}//for
	output[output.length-1] = '';
	return output;
}

function copyToClipboard(el) {
    //var text = $("#"+el).val();
		var origSelectionStart, origSelectionEnd;
		var elem = document.getElementById(el);
    origSelectionStart = elem.selectionStart;
    origSelectionEnd = elem.selectionEnd;

		var currentFocus = document.activeElement;
		elem.focus();
		elem.setSelectionRange(0, elem.value.length);

		var succeed;
		try {
			succeed = document.execCommand("copy");
		} catch(e) {
			succeed = false;
		}

		if(succeed) {
			showAlert("Link Copied","Property Registration Link copied to clipboard.");
			setTimeout(function(){$(".alert-overlay").hide();}, 3000);
			//alert("Copied to Clipboard");
		}
    //cordova.plugins.clipboard.copy(text);
}


/* CUSTOM EVENT HANDLERS
----------------------------------------------------------------------------*/

function cleanAccountData() {
	//Clean data that doesn't belong to you

	properties = JSON.parse(localStorage["properties"]);
	$.each(properties, function(index,value){
		if(value["managerID"] != user["id"]) {
			delete properties[index];
		}
	});

	localStorage["properties"] = JSON.stringify(properties);

	registrants = JSON.parse(localStorage["registrants"]);
	$.each(registrants, function(index,value){
		if(value["managerID"] != user["id"]) {
			delete registrants[index];
		}
	});

	localStorage["registrants"] = JSON.stringify(registrants);
}

function loginWithARC() {
	$(".sso-overlay").hide();

    $(".arc-login-panel").show();
}

function closeLoginWithARC() {
    $(".arc-login-panel").hide();
}


function login() {
	document.title ="Spacio: Dashboard";
	//$('html head').find('title').text("Spacio: Dashboard");
	loading(true);
	$(".login-panel").hide();
	$(".main-container").show();
	$(".left-menu-bar").fadeIn();
  $(".left-panel").css({"width":"200px"});
  $(".right-panel").css({"width":"calc(100vw - 200px)"});
  $(".menu-logo").css("background-image","url(img/spacio-logo-white-long.png)");
  $(".left-menu-bar .top-bar").css("background-color","#4b4b4b");
  $("#teamdashboard-menu-item").hide();


	var d = new Date();
  var n = d.getTime();
  n = n / 1000;

    if(user["account_status"] == "expired" && user["account_type"] != "team" && user["account_type"] != "brokerage") {
			 var temp_email = user["email"];
			 getProperties(function(){
				 getRegistrants(function(){
					 var uri = exportAllAsCSV(registrants);
					 console.log("Date Expired: "+user["dateExpired"]);

					 var days_left = Math.floor(30 - (n - user["dateExpired"])/ (60 * 60 *24));
					 if(user["account_type"] == "trial") {
							 if(days_left > 0) {
									 showAlert("Trial Expired","Please upgrade or export your leads.","Upgrade","<a href='"+uri+"' download='Visitors.csv'><div style='width:inherit;height:inherit;line-height:inherit;'>Export</div></a>", function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email, '_blank');},function(){forceLogout();});
							 } else {
									 showAlert("Trial Expired","Please upgrade your account.","Upgrade","Cancel", function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email, '_blank');},function(){forceLogout();});
							 }
					 } else if(user["account_type"] == "standard") {
							 if(days_left > 0) {
									 showAlert("Account Expired","Please restore your account or export your leads.","Restore","<a href='"+uri+"' download='Visitors.csv'><div style='width:inherit;height:inherit;line-height:inherit;'>Export</div></a>", function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email, '_blank');},function(){forceLogout();});
							 } else {
								 	 showAlert("Account Expired","Please restore your account.","Restore","Cancel", function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email, '_blank');},function(){forceLogout();});
							 }
					 } else if(user["account_type"] == "brokerage-individual") {
							 if(days_left > 0) {
								 showAlert("Account Expired","Please restore your account or export your leads. You have <span class='bold'>"+days_left+" days</span> left to export your vistors data.","Restore","<a href='"+uri+"' download='Visitors.csv'><div style='width:inherit;height:inherit;line-height:inherit;'>Export</div></a>", function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email+"&plan=bi", '_blank');},function(){forceLogout();});

							 } else {
								 	showAlert("Account Expired","Please restore your account.","Restore",null, function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email+"&plan=bi", '_blank');});
									 //ashowAlert("Account Expired","Please upgrade your account.","Upgrade",null, function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email+"&plan=bi";)});
							 }
					 }
					});
			 })


    } else if(user["account_status"] == "offboarded") {
				var temp_email = user["email"];
				 getProperties(function(){
					 getRegistrants(function(){
						 var uri = exportAllAsCSV(registrants);
						 console.log("Date Expired: "+user["dateExpired"]);

						 var days_left = Math.floor(30 - (n - user["dateExpired"])/ (60 * 60 *24));
						 if(user["account_type"] == "brokerage") {
							 	if(days_left > 0) {
									showAlert("Account Suspended","You have been removed from your Brokerage plan. Please export your leads and/or signup for a new Spacio account. You have <span class='bold'>"+days_left+" days</span> left to export your vistors data. If this is a mistake, please contact support@spac.io.","Ok","<a href='"+uri+"' download='Visitors.csv'><div style='width:inherit;height:inherit;line-height:inherit;'>Export</div></a>", function(){forceLogout();window.open("https://spac.io/signup/", '_blank');},function(){forceLogout();});

								} else {
								 	showAlert("Account Suspended","You have been removed from your Brokerage plan. Please signup for a new Spacio account.","Ok",null, function(){forceLogout();window.open("https://spac.io/signup/", '_blank');});
								}
		         } else if(user["account_type"] == "brokerage-individual") {
							 if(days_left > 0) {
								 showAlert("Account Suspended","You have been removed from your Brokerage plan. Please export your leads and/or signup for a new Spacio account. You have <span class='bold'>"+days_left+" days</span> left to export your vistors data. If this is a mistake, please contact support@spac.io.","Ok","<a href='"+uri+"' download='Visitors.csv'><div style='width:inherit;height:inherit;line-height:inherit;'>Export</div></a>", function(){forceLogout();window.open("https://spac.io/signup/", '_blank');},function(){forceLogout();});

							 } else {
								 showAlert("Account Suspended","You have been removed from your Brokerage plan. Please signup for a new Spacio account.","Ok",null, function(){forceLogout();window.open("https://spac.io/signup/", '_blank');});
							 }
						 } else if(user["account_type"] == "team") {
							 if(days_left > 0) {
								 showAlert("Account Suspended","You have been removed from your Team plan. Please upgrade your account or export your leads. You have <span class='bold'>"+days_left+" days</span> left to export your vistors data. If this is a mistake, please contact support@spac.io.","Upgrade","<a href='"+uri+"' download='Visitors.csv'><div style='width:inherit;height:inherit;line-height:inherit;'>Export</div></a>", function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email+"", '_blank');},function(){forceLogout();});

							 } else {
								 showAlert("Account Suspended","You have been removed from your Team plan. Please upgrade your account.","Upgrade",null, function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email+"", '_blank');});
							 }
						 }
					 });
				 });

    } else if(user["account_type"] == "trial") {

			var trial_days_left = Math.floor((user["trialEnds"] - n)/ (60 * 60 *24));

			$(".upgrade-button").show();
			$(".trial-status").html(trial_days_left+" days left until trial ends");
			$(".trial-status").show();
			$(".downgrade-button").hide();

    } else if(user["account_type"] == "standard") {

    } else if(user["account_type"] == "brokerage" || user["account_type"] == "brokerage-individual") {
				if(brokerage["inAppBranding"] == "YES") {
						//var store = cordova.file.dataDirectory;

						var lite1 = hexToRGB(brokerage["colors"]["primary"],0.75);
						var lite2 = hexToRGB(brokerage["colors"]["primary"],0.85);
						var lite3 = hexToRGB(brokerage["colors"]["primary"],0.95);

						$("<style>").prop("type", "text/css").html(".main-color-font {color:"+brokerage["colors"]["primary"]+"!important;} .main-color-background {background-color:"+brokerage["colors"]["primary"]+"!important;} .main-color-border {border-color:"+brokerage["colors"]["primary"]+"!important;} .main-color-background-lite-1 {background-color:"+lite1+"!important} .main-color-background-lite-2 {background-color:"+lite2+"!important} .main-color-background-lite-3 {background-color:"+lite3+"!important}").appendTo("head");
					 main_color = brokerage["colors"]["primary"];
					 $(".left-menu-bar .top-bar").css("background-color",main_color);
					$(".menu-logo").css("background-image","url(https://spac.io/dashboard/img/brands/"+brokerage["logos"]["inappbranding"]+")");

				}

       if(brokerage["status"] == "expired"){

						var temp_email = user["email"];
						 getProperties(function(){
							 getRegistrants(function(){
								 var uri = exportAllAsCSV(registrants);
								 console.log("Date Expired: "+user["dateExpired"]);

								 var days_left = Math.floor(30 - (n - user["dateExpired"])/ (60 * 60 *24));
									 	if(days_left > 0) {
											showAlert("Brokerage Plan Expired","Your brokerage's Spacio plan has expired. Please upgrade your account or export your leads. You have <span class='bold'>"+days_left+" days</span> left to export your vistors data. If this is a mistake, please contact support@spac.io.","Upgrade","<a href='"+uri+"' download='Visitors.csv'><div style='width:inherit;height:inherit;line-height:inherit;'>Export</div></a>", function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email+"", '_blank');},function(){forceLogout();});

										} else {
										 	showAlert("Brokerage Plan Expired","Your brokerage's Spacio plan has expired. Please upgrade your account.","Upgrade",null, function(){forceLogout();window.open("https://spac.io/upgrade/?e="+temp_email+"", '_blank');});
										}
							 });
						 });

        } else if(brokerage["status"] == "suspended"){
            showAlert("Brokerage Plan Suspended","Your brokerage's Spacio plan has been temporarily suspended. Please contact your company administrator.","Ok",null, function(){forceLogout();});
        }
    } else if(user["account_type"] == "team") {
			if(team["account_status"] == "expired"){
					 showAlert("Team Plan Expired","Your Team's Spacio plan has expired. If this is a mistake, please contact support@spac.io.","Ok", null, function(){forceLogout();},null);
			 } else if(team["account_status"] == "suspended"){
					 showAlert("Team Plan Suspended","Your Team's Spacio plan has been temporarily suspended. Please contact your company administrator.","Ok",null, function(){forceLogout();});
			 }
        $("#teamdashboard-menu-item").show();
    }

  // if("warning" in user && user["warning"] == "reset_email") {
  //     showAlert("Please Reset Email","You have been removed from your Brokerage plan. Please change your registered email. If this is a mistake, please contact support@spac.io.<br/><br/><input type='email' name='reset_email' placeholder='Enter New Email' value='' style='font-size:16px;border-bottom:1px solid #ddd;font-family:proxima_nova_light;border-radius:0px;width:100%;padding-bottom:5px;'><br/><Br/><span id='reset-email-error' style='color:red;'></span>","Ok","Contact",function(){resetEmail();},function(){contactSupport();})
  // } else if("warning" in user && user["warning"] == "reset_email"){
	//
  // }

	// if(user["type"] == "brokerage" && user["account_status"] == "offboarded") {
	// 	showAlert("Please Reset Account","You have been removed from your Brokerage plan. Please reset your account. If this is a mistake, please contact support@spac.io.","Reset",null,function(){resetAccount();});
	// } else if(user["type"] != "expired"){
	//
  //   $(".main-top-bar").show();
  //   $(".login-left-panel").fadeOut();
  //   $(".left-menu-bar").fadeIn();
  //   $(".login-right-panel").fadeOut();
  //   $(".left-panel").css({"width":"200px"});
  //   $(".right-panel").css({"width":"calc(100vw - 200px)"});
	//
  //   if(user["type"] == "freemium" || user["type"] == "lite") {
  //       $(".go-premium").show();
  //       $(".pro").hide();
  //       $(".free").show();
  //   }
	//
	// 	if (user["type"] == "trial" || user["type"] == "freemium") {
	// 		$(".upgrade-button").show();
	// 	} else {
	// 		$(".upgrade-button").hide();
	// 	}
	//
  //   if(user["type"] == "team") {
  //     $("#teamdashboard-menu-item").show();
  //   } else {
  //     $("#teamdashboard-menu-item").hide();
  //   }



    loadProfile();
		if("rsID" in user["integrations"]) {
			getRS();
		}
		if("ttID" in user["integrations"]) {
				 if(user["integrations"]["ttID"] != "" && user["integrations"]["ttID"] != "N/A") {
						 getTestimonialTree();
				 }
		 }
		 if("idcTestimonialID" in user["integrations"]) {
				 getIDCTestimonials();
		 }
		 if("zillowreviewID" in user["integrations"]) {
				 if(user["integrations"]["zillowreviewID"] != "") {
						 getZillowReview();
				 }
		 }

    getMLSList();


		getDefaultForm(function(){
				var fn = $(document).getUrlParam("fn");

				if(fn == "contactually") {
						user["contactuallyID"] = $(document).getUrlParam("token");
						applyContactuallyID(user["contactuallyID"]);
				}

				 showPropertiesPanel();
				 loading(false);
		});

}

function showUpgradePage() {
	$(".alert-overlay").hide();

	window.open("https://spac.io/upgrade/?e="+user["email"], '_blank');
}


function exportAllAsCSV(reg_list) {

	var csvData = [];
  var tmpArr = [];
  var tmpStr = '';
	var isBroker = "";

	console.log("Exporting Visitors");
	console.log(reg_list);

	tmpArr =['"First Name"','"Last Name"','"Email"','"Phone"','"Question 1"','"Answer 1"','"Question 2"','"Answer 2"','"Question 3"','"Answer 3"','"Question 4"','"Answer 4"','"Question 5"','"Answer 5"','"Question 6"','"Answer 6"','"Question 7"','"Answer 7"','"Question 8"','"Answer 8"','"Question 9"','"Answer 9"','"Note"','"Date"','"Time"','"Property Title"','"Property Address"'];

	csvData.push(tmpArr.join(','));



	$.each(reg_list,function(index, value){

		var name = value["name"];
		var phone = value["phone"];
		var email = value["email"];

		if (value["tempEmail"] == "N/A") {

		} else {
			var email = value["tempEmail"];
		}

		var first_name, last_name;
		var name_array = name.split(" ");
		first_name = name_array[0];

		if (name_array.length == 2) {
			last_name = name_array[name_array.length - 1];
		} else if (name_array.length == 3) {
			last_name = name_array[name_array.length - 2]+" "+name_array[name_array.length - 1];
		} else if (name_array.length == 4) {
			last_name = name_array[name_array.length - 3]+" "+name_array[name_array.length - 2]+" "+name_array[name_array.length - 1];;
		} else {
			last_name = "";
		}

		var datetime = new Date(value["dateCreated"]["sec"] * 1000);
		tmpArr = [];
		tmpArr =['"'+first_name+'"','"'+last_name+'"','"'+email+'"','"'+phone+'"'];

		var question_count = 0;
		$.each(value["answersObj"],function(index,value){
			question_count++;
			tmpArr.push('"'+value["questionObj"]["question"]+'"');
			tmpArr.push('"'+value["answer"]+'"');
		})

		var blank_count = 9 - question_count;

		for(var i = 0; i < blank_count; i++) {
			tmpArr.push('""');
			tmpArr.push('""');
		}

		tmpArr.push('"'+value["note"]+'"');
		tmpArr.push('"'+datetime.toLocaleDateString()+'"');
		tmpArr.push('"'+datetime.toLocaleTimeString()+'"');

		if(typeof properties[value["pid"]] === "undefined") {
			tmpArr.push('"Property not found"');
			tmpArr.push('"Property not found"');
		} else {
			tmpArr.push('"'+properties[value["pid"]]["title"]+'"');
			tmpArr.push('"'+properties[value["pid"]]["addr1"]+'"');
		}

		csvData.push(tmpArr.join(','));

	})

	var output = csvData.join('\n');
  	var uri = 'data:application/csv;charset=UTF-8,' + encodeURIComponent(output);
	return uri;


}

function logout() {
	forceLogout();
}

function forceLogout() {
	$(".alert-overlay").hide();
	var cssLink = $("<link rel='stylesheet' type='text/css' href='css/default.css'>");
	$("head").append(cssLink);
	main_color = "9A6B52";

	$(".menu-logo").html("");

	$(".login-panel").fadeIn();
  $(".panel").fadeOut();
	$("#email").val(user["email"]);
	user = {};
	properties = {};
  brokerage = {};
  team = {};
  brand = {};

	localStorage.removeItem("id");
	localStorage.removeItem("vtoken");
  loggedIn = false;
	loading(false);
}

function showIntegrationType(type) {

		if(type == "all") {
			$(".free-integration").show();
			$(".paid-integration").show();
		}

		if(type == "free") {
			$(".free-integration").show();
			$(".paid-integration").hide();
		}

		if(type == "paid") {
			$(".free-integration").hide();
			$(".paid-integration").show();
		}
		curIntegrationType = type;
		$(".integration-tab").removeClass("selected").addClass("unselected");
		$("#integration-"+type+"-tab-button").removeClass("unselected").addClass("selected");




}


function showNotificationsPanel() {
        updateLastActive();
        console.log("show integrations panel");
        closeAllPanels();
        $("#notifications-menu-item").removeClass("not-selected").addClass("selected");
        $("#notifications-panel").show();
        prevPanel = curPanel;
        curPanel = "notifications";

        if("notifications" in user) {
            if (user["notifications"]["newleads"] == "YES") {
                $("#newleadsnotification").prop( "checked", true );
            } else {
                 $("#newleadsnotification").prop( "checked", false );
            }

            if (user["notifications"]["autoemailstatus"] == "YES") {
                $("#autoemailstatusnotification").prop( "checked", true );
            } else {
                 $("#autoemailstatusnotification").prop( "checked", false );
            }

            if (user["notifications"]["readreceipt"] == "YES") {
                $("#readreceiptnotification").prop( "checked", true );
            } else {
                 $("#readreceiptnotification").prop( "checked", false );
            }
        } else {
            $("#newleadsnotification").prop( "checked", false );
            $("#autoemailstatusnotification").prop( "checked", false );
            $("#readreceiptnotification").prop( "checked", false );
        }
}


function showIntegrationsPanel() {
	if(online) {
		updateLastActive();
		console.log("show integrations panel");
		closeAllPanels();
    $("#integrations-menu-item").removeClass("not-selected").addClass("selected");

		$("#integrations-panel").show();
      prevPanel = curPanel;

		curPanel = "integrations";

        $(".integration-name").css("background-image","url(img/arrow_down.png)");
        $(".integration-desc").hide();
        $(".integration-content").hide();

		//Load Integrations Settings

		if("integrations" in user) {
            if("adwerxID" in user["integrations"]) {
                $("input[name='adwerxID']").val(user["integrations"]["adwerxID"]);
            }

            if("agencyEnabled" in user["integrations"]) {
                 if (user["integrations"]["agencyEnabled"] == "YES") {
                     $("#edit-agencyEnabled").prop( "checked", true );
                 } else {
                     $("#edit-agencyEnabled").prop( "checked", false );
                 }
            }

						if("bayEnabled" in user["integrations"]) {
                if (user["integrations"]["bayEnabled"] == "YES") {
                    $("#edit-bayEnabled").prop( "checked", true );
                } else {
                    $("#edit-bayEnabled").prop( "checked", false );
                }
            }

						if("boostEnabled" in user["integrations"]) {
                if (user["integrations"]["boostEnabled"] == "YES") {
                    $("#edit-boostEnabled").prop( "checked", true );
                } else {
                    $("#edit-boostEnabled").prop( "checked", false );
                }
            }

            if("bombbombKey" in user["integrations"]) {
                 $("input[name='bombbombKey']").val(user["integrations"]["bombbombKey"]);
            }

            if ("boomtownID" in user["integrations"]) {
                if(user["integrations"]["boomtownID"] != "N/A") {
                    $("input[name='boomtownID']").val(user["integrations"]["boomtownID"]);
                }
            }

						if ("boomtownRefreshToken" in user["integrations"]) {
							if (user["integrations"]["boomtownRefreshToken"] == "N/A" || user["integrations"]["boomtownRefreshToken"] == "") {
								$("#boomtown-authorize-link").attr("href","http://leads.boomtownroi.com/app/oauth?client_id=f9910f43-4c13-4c2f-97a7-ff12c8694ab0&redirect_url=https://ws.spac.io/prod/oh/oauth2/boomtown&scope=all&state="+user["id"]);
								$("#boomtown-authorize-link").show();
								$("#boomtown-clear-btn").hide();
							} else {
								$("#boomtown-authorize-link").hide();
								$("#boomtown-clear-btn").show();
							}
            } else {
							$("#boomtown-authorize-link").attr("href","http://leads.boomtownroi.com/app/oauth?client_id=f9910f43-4c13-4c2f-97a7-ff12c8694ab0&redirect_url=https://ws.spac.io/prod/oh/oauth2/boomtown&scope=all&state="+user["id"]);
							$("#boomtown-authorize-link").show();
							$("#boomtown-clear-btn").hide();
						}

						if("bostonlogicEnabled" in user["integrations"]) {
								if (user["integrations"]["bostonlogicEnabled"] == "YES") {
										$("#edit-bostonlogicEnabled").prop( "checked", true );
								} else {
										$("#edit-bostonlogicEnabled").prop( "checked", false );
								}
						}

            if("contactuallyID" in user["integrations"]) {
		     			if (user["integrations"]["contactuallyID"] == "N/A" || user["integrations"]["contactuallyID"] == "") {
		     				$("#contactually-authorize-link").attr("href","https://auth.contactually.com/oauth2/authorize/?client_id=ff0312d73de7cba90ac50751006bd9fa4c6e0f1717c2331b0f33f8f59292c151&scope=user:basic+contacts:manage+buckets:manage+notes:manage+interactions:manage&redirect_uri=https%3a%2f%2fws.spac.io%2fprod%2foh%2foauth2%2fcontactually&response_type=code");
		     				$("#contactually-pre").show();
		     				$("#contactually-post").hide();
		     			} else {
		     				$("#contactually-pre").hide();
		     				$("#contactually-post").show();
		     				$("input[name='contactuallyID']").val(user["integrations"]["contactuallyID"]);
		     				getContactuallyBuckets(function(){
		     					$("select[name='contactuallyBucket']").val(user["integrations"]["contactuallyBucket"]);
		     				});
		     			}
		     		} else {
		     			$("#contactually-authorize-link").attr("href","https://auth.contactually.com/oauth2/authorize/?client_id=ff0312d73de7cba90ac50751006bd9fa4c6e0f1717c2331b0f33f8f59292c151&scope=user:basic+contacts:manage+buckets:manage+notes:manage+interactions:manage&redirect_uri=https%3a%2f%2fws.spac.io%2fprod%2foh%2foauth2%2fcontactually&response_type=code");
		     			$("#contactually-pre").show();
		     			$("#contactually-post").hide();
		     		}


            if("corelogicEnabled" in user["integrations"]) {
                if (user["integrations"]["corelogicEnabled"] == "YES") {
                    $("#edit-corelogicEnabled").prop( "checked", true );
                } else {
                    $("#edit-corelogicEnabled").prop( "checked", false );
                }
            }

            if("boojEnabled" in user["integrations"]) {
                if (user["integrations"]["boojEnabled"] == "YES") {
                    $("#edit-boojEnabled").prop( "checked", true );
                } else {
                    $("#edit-boojEnabled").prop( "checked", false );
                }
            }

						if("kvcoreEnabled" in user["integrations"]) {
								if (user["integrations"]["kvcoreEnabled"] == "YES") {
										$("#edit-kvcoreEnabled").prop( "checked", true );
								} else {
										$("#edit-kvcoreEnabled").prop( "checked", false );
								}
						}

         if("docsEnabled" in user["integrations"]) {
             if (user["integrations"]["docsEnabled"] == "YES") {
                 $("#edit-docsEnabled").prop( "checked", true );
             } else {
                 $("#edit-docsEnabled").prop( "checked", false );
             }
         }

         if("docsAuto" in user["integrations"]) {
             if (user["integrations"]["docsAuto"] == "YES") {
                 $("#edit-docsAuto").prop( "checked", true );
             } else {
                 $("#edit-docsAuto").prop( "checked", false );
             }
         }

  			if("idcEnabled" in user["integrations"]) {
  					 if (user["integrations"]["idcEnabled"] == "YES") {
  							 $("#edit-idcEnabled").prop( "checked", true );
  					 } else {
  							 $("#edit-idcEnabled").prop( "checked", false );
  					 }
  			 }

         if ("followupbossID" in user["integrations"]) {
           if(user["integrations"]["followupbossID"] != "N/A") {
         			$("input[name='followupbossID']").val(user["integrations"]["followupbossID"]);
            }
     		 }

         if ("mailchimpKey" in user["integrations"]) {
            if(user["integrations"]["mailchimpKey"] != "N/A") {
              $("input[name='mailchimp_key']").val(user["integrations"]["mailchimpKey"]);
            }
          }

        if ("mailchimpUsername" in user["integrations"]) {
           if(user["integrations"]["mailchimpUsername"] != "N/A") {
             $("input[name='mailchimp_username']").val(user["integrations"]["mailchimpUsername"]);
           }
         }

				 if("realscoutEmail" in user["integrations"]) {
     		    $("input[name='realscoutEmail']").val(user["integrations"]["realscoutEmail"]);
            if("realscoutEnableOptIn" in  user["integrations"]) {
                if(user["integrations"]["realscoutEnableOptIn"] == "YES") {
                    $("#edit-realscoutEnableOptIn").prop( "checked", true );
                } else {
                    $("#edit-realscoutEnableOptIn").prop( "checked", false );
                }
             }
     	    }

         if ("realtyjugglerID" in user["integrations"]) {
            if(user["integrations"]["realtyjugglerID"] != "N/A") {
              $("input[name='realtyjugglerID']").val(user["integrations"]["realtyjugglerID"]);
            }
          }

         if("realvolveID" in user["integrations"]) {
            $("input[name='realvolveID']").val(user["integrations"]["realvolveID"]);
         }

         if ("renthopID" in user["integrations"]) {
           if(user["integrations"]["renthopID"] != "N/A") {
     			   $("input[name='renthopID']").val(user["integrations"]["renthopID"]);
           }
     		 }

         if("rsID" in user["integrations"]) {
     			  $("input[name='rsID']").val(user["integrations"]["rsID"]);
     		 }

         if ("rsAutoClose" in user["integrations"]) {
           if (user["integrations"]["rsAutoClose"] == "YES") {
         			$("#edit-rsAutoClose").prop( "checked", true );
         		} else {
         			$("#edit-rsAutoClose").prop( "checked", false );
         		}
         }

         if ("topproducerID" in user["integrations"]) {
     			 $("input[name='topproducerID']").val(user["integrations"]["topproducerID"]);
     		 }

       		if("ttID" in user["integrations"]) {
						if(user["integrations"]["ttID"] == "N/A") {
							user["integrations"]["ttID"] = "";
						}
       			$("input[name='ttID']").val(user["integrations"]["ttID"]);
       		}

         if ("ttAutoClose" in user["integrations"]) {
         		if (user["integrations"]["ttAutoClose"] == "YES") {
         			$("#edit-ttAutoClose").prop( "checked", true );
         		} else {
         			$("#edit-ttAutoClose").prop( "checked", false );
         		}
          }

         if("wiseagentID" in user["integrations"]) {
           $("input[name='wiseagentID']").val(user["integrations"]["wiseagentID"]);
         }

				 if("xpressdocsEnabled" in user["integrations"]) {
					 if (user["integrations"]["xpressdocsEnabled"] == "YES") {
							 $("#edit-xpressdocsEnabled").prop( "checked", true );
					 } else {
							 $("#edit-xpressdocsEnabled").prop( "checked", false );
					 }
			 		}


				 if("zillowreviewID" in user["integrations"]) {
            if(user["integrations"]["zillowreviewID"] == "N/A") {
                user["integrations"]["zillowreviewID"] = "";
            }
            $("input[name='zillowreviewID']").val(user["integrations"]["zillowreviewID"]);
        }

        if ("zillowreviewAutoClose" in user["integrations"]) {
           if (user["integrations"]["zillowreviewAutoClose"] == "YES") {
                $("#edit-zillowreviewAutoClose").prop( "checked", true );
            } else {
                $("#edit-zillowreviewAutoClose").prop( "checked", false );
            }
         }



         		if("cloudcmaID" in user["integrations"]) {
         			$("input[name='cloudcmaID']").val(user["integrations"]["cloudcmaID"]);
         		}

         		if("moxipresentEnabled" in user["integrations"]) {
         			if(user["integrations"]["moxipresentEnabled"] == "YES") {
         				$("#edit-moxipresentEnabled").prop( "checked", true );
         			} else if(user["integrations"]["moxipresentEnabled"] == "NO") {
         				$("#edit-moxipresentEnabled").prop( "checked", false );
         			}
         		}

             if("touchcmaEnabled" in user["integrations"]) {
         			if(user["integrations"]["touchcmaEnabled"] == "YES") {
         				$("#edit-touchcmaEnabled").prop( "checked", true );
         			} else if(user["integrations"]["touchcmaEnabled"] == "NO") {
         				$("#edit-touchcmaEnabled").prop( "checked", false );
         			}
         		}

         		if("presentationproEnabled" in user["integrations"]) {
         			 if(user["integrations"]["presentationproEnabled"] == "YES") {
         				 $("#edit-presentationproEnabled").prop( "checked", true );
         			 } else if(user["integrations"]["presentationproEnabled"] == "NO") {
         				 $("#edit-presentationproEnabled").prop( "checked", false );
         			 }
         		 }

         		 if("homekeeprEnabled" in user["integrations"]) {
         			 if(user["integrations"]["homekeeprEnabled"] == "YES") {
         				 $("#edit-homekeeprEnabled").prop( "checked", true );
         				$("#edit-homekeeprURL").val(user["integrations"]["homekeeprURL"]);
         			 } else if(user["integrations"]["homekeeprEnabled"] == "NO") {
         				 $("#edit-homekeeprEnabled").prop( "checked", false );
         			 }
         		 }


         		if("olrEnabled" in user["integrations"]) {
         			if(user["integrations"]["olrEnabled"] == "YES") {
         				$("#edit-olrEnabled").prop( "checked", true );
         			} else if(user["integrations"]["olrEnabled"] == "NO") {
         				$("#edit-olrEnabled").prop( "checked", false );
         			}
         		}

         		if("moxiEnabled" in user["integrations"]) {
         			if(user["integrations"]["moxiEnabled"] == "YES") {
         				$("#edit-moxiEnabled").prop( "checked", true );
         			} else if(user["integrations"]["moxiEnabled"] == "NO") {
         				$("#edit-moxiEnabled").prop( "checked", false );
         			}
         		}

         		if("rewEnabled" in user["integrations"]) {
         			if(user["integrations"]["rewEnabled"] == "YES") {
         				$("#edit-rewEnabled").prop( "checked", true );
         			} else if(user["integrations"]["rewEnabled"] == "NO") {
         				$("#edit-rewEnabled").prop( "checked", false );
         			}
         		}

         		if("tribusEnabled" in user["integrations"]) {
         			if(user["integrations"]["tribusEnabled"] == "YES") {
         				$("#edit-tribusEnabled").prop( "checked", true );
         			} else if(user["integrations"]["tribusEnabled"] == "NO") {
         				$("#edit-tribusEnabled").prop( "checked", false );
         			}
         		}

         		if("docusignEnabled" in user["integrations"]) {
         			if(user["integrations"]["docusignEnabled"] == "YES") {
         				$("#edit-docusignEnabled").prop( "checked", true );
         			} else if(user["integrations"]["docusignEnabled"] == "NO") {
         				$("#edit-docusignEnabled").prop( "checked", false );
         			}
         		}

         		if("docusignAutoEnabled" in user["integrations"]) {
         			if(user["integrations"]["docusignAutoEnabled"] == "YES") {
         				$("#edit-docusignAutoEnabled").prop( "checked", true );
         			} else if(user["integrations"]["docusignAutoEnabled"] == "NO") {
         				$("#edit-docusignAutoEnabled").prop( "checked", false );
         			}
         		}

         		if("relianceEnabled" in user["integrations"]) {
         			if(user["integrations"]["relianceEnabled"] == "YES") {
         				$("#edit-relianceEnabled").prop( "checked", true );
         			} else if(user["integrations"]["relianceEnabled"] == "NO") {
         				$("#edit-relianceEnabled").prop( "checked", false );
         			}
         		}
        }

		if("sendToRepresented" in user) {
			$.each(user["sendToRepresented"], function(index,value){
				if (value == "YES") {
					$("#"+index+"SendToRepresented").prop( "checked", true );
				} else {
					$("#"+index+"SendToRepresented").prop( "checked", false );
				}

			})
		}

        $("#integrations-scroll").scrollTop(0);

	} else {

		showAlert("Offline Mode","This function requires a live internet connection.");
	 }
}



function showMLSPanel() {
	if(online) {
		updateLastActive();
		console.log("show mls panel");
		$(".sub-container").hide();
		$("#mls-panel").show();
		//$(".menu-item").removeClass("selected").addClass("not-selected");
		//$("#account-menu-item").removeClass("not-selected").addClass("selected");
		curPanel = "mls";


		getMLSList(function(){

			$("select[name='mls']").html("");

			$("select[name='mls']").append($('<option>', {
				value: "N/A",
				text: "None"
			}));

			$.each(mlsList, function(i,v){
				$("select[name='mls']").append($('<option>', {
					value: i,
					text: v["mls_name"]
				}));
			});

            if("mls" in user) {
                $("select[name='mls']").val(user["mls"]);
            }

            if("mlsID" in user) {
                $("input[name='userMLSID']").val(user["mlsID"]);
            }

		});

	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}

}


function showAccountPanel() {
	if(online) {
		updateLastActive();
		console.log("show profile panel");
		$(".sub-container").hide();
		$("#account-panel").show();
		//$(".menu-item").removeClass("selected").addClass("not-selected");
		//$("#account-menu-item").removeClass("not-selected").addClass("selected");
		curPanel = "account";
		$(".account-email").html(user["email"]);
		$(".account-status").html("Active");


		if (user["type"] == "trial") {
			$(".account-plan").html("Trial (30-day unlimited-use FREE trial)");
			$(".upgrade-button").show();
			$(".downgrade-button").hide();
		}

		if (user["type"] == "freemium" || user["type"] == "lite") {
			$(".account-plan").html("Lite");
			$(".upgrade-button").show();
			$(".downgrade-button").hide();
		}

		if (user["type"] == "standard") {
			$(".account-plan").html("Premium");

			if (accountCancelled) {
				$(".upgrade-button").hide();
				$(".downgrade-button").hide();
				$(".account-status").html("Cancelled");
			} else {
				$(".upgrade-button").hide();
				$(".downgrade-button").show();
			}
		}

		if (user["account_type"] == "brokerage_individual") {
			if (user["account_status"] == "downgraded") {
				$(".upgrade-button").hide();
				$(".downgrade-button").hide();
				$(".account-status").html("Cancelled");
			} else {
				$(".upgrade-button").hide();
				$(".downgrade-button").show();
			}
		}

		if (user["type"] == "brokerage") {
			$(".account-plan").html("Brokerage");
			$(".downgrade-button").hide();
		}

		if (user["type"] == "free") {
			$(".account-plan").html("Platinum (Free with Full Features)");
			$(".upgrade-button").hide();
			$(".downgrade-button").hide();
		}


		$(".account-promo").html(user["promocode"]);

		if (user["promocode"] != "N/A" && user["promocode"] != "") {
			$("input[name='promocode']").val(user["promocode"]);
		}

		if (user["brand"] != "N/A") {
			$("input[name='corpcode']").val(user["brand"]);
		}

	 } else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	 }
}

function confirmDowngradeAccount() {
	showAlert("Downgrade Plan","Are you sure you want to downgrade your plan? You will have access to Spacio  until the last day of your current billing cycle.","Downgrade","Cancel",function(){  $(".alert-overlay").hide();downgradeAccount();},function(){ $(".alert-overlay").hide();	 });

}

function showPlanPanel() {
	if(online) {
		updateLastActive();
		console.log("show plan panel");
		$(".sub-container").hide();
		$("#plan-panel").show();
		//$(".menu-item").removeClass("selected").addClass("not-selected");
		//$("#account-menu-item").removeClass("not-selected").addClass("selected");
		curPanel = "plan";
		$(".account-email").html(user["email"]);
		$(".account-status").html("Active");

		if (user["type"] == "trial") {
			$(".account-plan").html("Trial (30-day unlimited-use FREE trial)");
			$(".upgrade-button").show();
			$(".downgrade-button").hide();
		}

		if (user["type"] == "freemium" || user["type"] == "lite") {
			$(".account-plan").html("Lite");
			$(".upgrade-button").show();
			$(".downgrade-button").hide();
		}

		if (user["type"] == "standard") {
			$(".account-plan").html("Premium");

			if (accountCancelled) {
				$(".upgrade-button").hide();
				$(".downgrade-button").hide();
				$(".account-status").html("Cancelled");
			} else {
				$(".upgrade-button").hide();
				$(".downgrade-button").show();
			}
		}

		if (user["type"] == "brokerage") {
			$(".account-plan").html("Brokerage");
			$(".downgrade-button").hide();
		}

		if (user["type"] == "free") {
			$(".account-plan").html("Platinum (Free with Full Features)");
			$(".upgrade-button").hide();
			$(".downgrade-button").hide();
		}

		$(".account-promo").html(user["promocode"]);

		if (user["promocode"] != "N/A" && user["promocode"] != "") {
			$("input[name='promocode']").val(user["promocode"]);
		}

		if (user["brand"] != "N/A") {
			$("input[name='corpcode']").val(user["brand"]);
		}

	 } else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	 }
}

function showScoringPanel() {
	if(online) {
		updateLastActive();
		console.log("show plan panel");
		$(".sub-container").hide();
		$("#scoring-panel").show();
		curPanel = "scoring";

		loadScoring();
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	 }

}



function showEmailsTab(tab) {
    curEmailTemplate = tab;
	if (curEmailsTab == tab) {

	} else {
		getEmailTemplate(tab, function(){
			$("#emails-"+curEmailsTab+"-tab").hide();
			$("#emails-"+tab+"-tab").show();
			curEmailsTab = tab;
			$(".emails-tab").removeClass("selected").addClass("unselected");
			$("#emails-"+tab+"-tab-button").removeClass("unselected").addClass("selected");
		});

	}
}

function showEmailTemplate() {
    var template = $("#template-filter").val();
    $(".template-desc").html("");
	if (curEmailTemplate == template) {

	} else {
        curEmailTemplate = template;

        if(template == "customer") {
            $(".template-desc").html("Customize the email your Visitors receive after signing in. Tags inside brackets {{ }} are HTML variables that will be replaced with corresponding fields. To see the email Visitors receive, send yourself a test email.");
        } else if(template == "agent") {
            $(".template-desc").html("Customize the email your Visitors' representatives receive if they input a contact email on the sign-in form. Tags inside brackets {{ }} are HTML variables that will be replaced with corresponding fields. To see the email Visitors' representatives receive, send yourself a test email.");
        } else if(template == "broker") {
            $(".template-desc").html("Customize the email brokers receive after signing in at your Brokers Open House. Tags inside brackets {{ }} are HTML variables that will be replaced with corresponding fields. To see the email brokers receive, send yourself a test email.");
        } else if(template == "customerinquiry") {
            $(".template-desc").html("Customize the email your Registrants receive after filling out the Inquire/Register form on your Property Page. Tags inside brackets {{ }} are HTML variables that will be replaced with corresponding fields.");
        } else if(template == "agentinquiry") {
            $(".template-desc").html("Customize the email your Registrants' representatives receive after filling out the Inquire/Register form on your Property Page. Tags inside brackets {{ }} are HTML variables that will be replaced with corresponding fields.");
        } else {
            $(".template-desc").html("Customize the email your Visitors receive when you select this template from Broadcast Message. Tags inside brackets {{ }} are HTML variables that will be replaced with corresponding fields. To see the email Visitors receive, send yourself a test email.");
        }

		getEmailTemplate(template, function(){

		});



	}
}


function toggleIntegration(integration) {

	if($("#"+integration+"-integration-content").is(":visible")) {
		$("#"+integration+"-integration-row .integration-name").css("background-image","url(img/arrow_down.png)");
	} else {
		$("#"+integration+"-integration-row .integration-name").css("background-image","url(img/arrow_up.png)");

        if(integration == "mls") {
            getMLSList(function(){
							loading(false);
                $("select[name='mls']").html("");

                $("select[name='mls']").append($('<option>', {
                    value: "N/A",
                    text: "None"
                }));

                $.each(mlsList, function(i,v){
                    $("select[name='mls']").append($('<option>', {
                        value: i,
                        text: v["mls_name"]
                    }));
                });

                if("mls" in user) {
                    $("select[name='mls']").val(user["mls"]);
                }

                if("mlsID" in user) {
                    $("input[name='userMLSID']").val(user["mlsID"]);
                }

            });
        }

		console.log($("#"+integration+"-integration-row").offset().top);
		//$(".account-tab-content").scrollTop($("#"+integration+"-integration-row").offset().top);
	}

	$("#"+integration+"-integration-desc").toggle();
	$("#"+integration+"-integration-content").toggle();
    $("#"+integration+"2-integration-desc").toggle();
	$("#"+integration+"2-integration-content").toggle();

}

function mlsChanged() {

	var t_mls = $("select[name='mls']").val();
	console.log("MLS Chamged to "+t_mls);
	if (t_mls == "N/A") {
		$(".user-mls-id").hide();
		$(".user-mls-id").val("");

		$(".mls-disclaimer").hide();
	} else {
		if(t_mls == "Northstar") {
			verifyMLSEmail();
			$(".mls-disclaimer").hide();
		} else {
            console.log("Disclaimer "+mlsList[t_mls]["disclaimer"]);
			$(".mls-disclaimer").html(mlsList[t_mls]["disclaimer"]);
			$(".mls-disclaimer").show();
		}

		if(mlsList[t_mls]["authentication"] == true) {
			$(".user-mls-id").show();
		} else {
			$(".user-mls-id").hide();
		}
	}

    /*
	if (user["mls"] != "N/A" && ('mls' in user)) {
				$("select[name='mls']").val(user["mls"]);

				if(user["mls"] != "Northstar") {
					//console.log(mlsList[user["mls"]]["authentication"]);
					$(".mls-disclaimer").html(mlsList[user["mls"]]["disclaimer"]);
					$(".mls-disclaimer").show();
					if(mlsList[user["mls"]]["authentication"] == true) {

						$(".user-mls-id").show();
						$("input[name='userMLSID']").val(user["mlsid"]);
					} else {
						$(".user-mls-id").hide();
					}
				} else {
					$(".mls-disclaimer").hide();
				}
			}
            */
}


function saveAsPDF() {
	if (user["type"] == "freemium" || user["type"] == "lite") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else {
		showAlert("Browser Only Function","Please Save as PDF from your computer or browser. Login at spac.io/dashboard");
	}
}

function showEmailTemplatesPanel() {
	if(online) {
	    curEmailTemplate = "";
		updateLastActive();
		closeAllPanels();
        $("#email-templates-menu-item").removeClass("not-selected").addClass("selected");
		$("#email-panel").show();
        prevPanel = curPanel;
		curPanel = "email";

        //$("#template-filter-dd").setCustomDDValue("customer");

		showEmailTemplate();
		//getEmailTemplate();
	} else {
		showAlert("Offline Mode","This function requires a live internet connection.");
	}
}

function showFeedbackPanel() {

	console.log("show feedback panel");
	closeAllPanels();
	$("#feedback-menu-item").removeClass("not-selected").addClass("selected");
	prevPanel = curPanel;
	curPanel = "feedback";

	$("#feedback-panel").show();

}


function addField(tar, e) {
	if (user["type"] == "freemium" || user["type"] == "lite") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else {
		var msgField = $(e).val();

		var temp_html = $('#'+tar+'').trumbowyg('html');
		temp_html = temp_html + "{{"+msgField+"}}";
		//$('#'+tar+'').prev().focus();
		$('#'+tar+'').trumbowyg('html', temp_html);

		//bodyEditor.composer.commands.exec("insertHTML", "{{"+msgField+"}}");
		$(e).val('default');
	}
}

function clearListingsSearchResults() {
	//propertiesSearchResult = $.extend({}, properties);
	$("#listings-search-terms").val("");
    $(".listings-search-results").html("");
	//loadProperties("enabled");
}



function clearRegistrantsSearchResults() {
	registrantsSearchResult = $.extend({}, registrants);
	$("#registrants-search-terms").val("");
	showSortedRegistrantsByType("buyers");
}


function searchRegistrants() {
	loading(true, "Searching...");
	var criterias = $("#registrants-search-terms").val();
	criterias = criterias.toLowerCase();

	if(criterias == "") {
		loading(false);
		clearRegistrantsSearchResults();
	} else {
		registrantsSearchResult = {};
		$.each(registrants, function(index,value){

				var p_name = value["name"].toLowerCase();
				var p_email = value["email"].toLowerCase();
				var p_phone = value["phone"].toLowerCase();
				var p_note = value["note"].toLowerCase();



				var includeReg = false;
				if(p_name.indexOf(criterias) >= 0) {
					includeReg = true;
				} else if(p_email.indexOf(criterias) >= 0) {
					includeReg = true;
				} else if(p_phone.indexOf(criterias) >= 0) {
					includeReg = true;
				} else if(p_note.indexOf(criterias) >= 0) {
					includeReg = true;
				}

				if (includeReg) {
					registrantsSearchResult[index] = value;
				}
		});
		loading(false);

		if($("#registrants-filter-back").css("left") == "0px") {
			showSortedRegistrantsByType("buyers");
		} else {
			showSortedRegistrantsByType("brokers");
		}
	}

}



function loadProfile() {
	console.log("Loading Profile");
	$.each(user, function(index,value){
		//console.log(index+":"+value);
		$("#profile-"+index).html(value);
		$(".profile-"+index).html(value);

		if (index == "title") {
			$("#edit-p"+index).val(value);
		} else if((index== "office") && value == "N/A"){
			$("#edit-"+index).val("");
			$("#profile-"+index).html("");
			$(".profile-"+index).html("");
		} else {
			$("#edit-"+index).val(value);
			//console.log(index+" "+value);
		}

	})

	$("#edit-office-addr1").val(user["addr1"]);
	$("#edit-office-addr2").val(user["addr2"]);

	$(".profile-purl").html("<u>http://www.spac.io/p/"+user["ukey"]+"</u>");

	var d = new Date();

  if (user['pphoto'].indexOf("?") >= 0) {
      $(".profile-image-container").css("background-image","url("+user['pphoto']+")");
      $(".profile-image-container2").css("background-image","url("+user['pphoto']+")");
      $("#profile-image-preview").css("background-image","url("+user['pphoto']+")");
  } else {
      $(".profile-image-container").css("background-image","url("+user['pphoto']+"?"+d.getTime()+")");
      $(".profile-image-container2").css("background-image","url("+user['pphoto']+"?"+d.getTime()+")");
      $("#profile-image-preview").css("background-image","url("+user['pphoto']+"?"+d.getTime()+")");
  }


		$(".remove-profile-image-button").show();



	if (user["type"] == "trial") {
		$(".account-type").html("Trial");
	}

	if (user["type"] == "freemium" || user["type"] == "lite") {
		$(".account-type").html("Lite");
	}

	if (user["type"] == "free") {
		$(".account-type").html("");
	}

	if (user["type"] == "standard") {
		//$(".account-type").html("Pro");
	}

	if (user["type"] == "brokerage") {
		$(".account-type").html("");
		$(".brokerage").show();
		$("#edit-email").prop( "readonly", true );
		$("#edit-office").prop( "readonly", true );
		$("#edit-office-addr1").prop( "readonly", true );
		$("#edit-office-addr2").prop( "readonly", true );
	} else {
		$(".brokerage").hide();
		$("#edit-email").prop( "readonly", false );
		$("#edit-office").prop( "readonly", false );
		$("#edit-office-addr1").prop( "readonly", false );
		$("#edit-office-addr2").prop( "readonly", false );
	}
}

function loadQuestions(questions) {
	$(".questions").html("");
  $(".num-questions").html(questions.length);

	var total_questions = questions.length;
	$.each(questions, function(index, value) {

		var removeButton = "<div class='remove-question-button' onclick='removeQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div>";
		var moveUpButton = "<div class='move-question-up-button' onclick='moveQuestionUp("+index+")'><i class='fa fa-arrow-up' aria-hidden='true'></i></div>";
		var moveDownButton = "<div class='move-question-down-button' onclick='moveQuestionDown("+index+")'><i class='fa fa-arrow-down' aria-hidden='true'></i></div>";

		if(index == 0) {
			moveUpButton = "<div class='move-question-up-button disabled'><i class='fa fa-arrow-up' aria-hidden='true'></i></div>";
		}

		if (index >= total_questions - 1) {
			moveDownButton = "<div class='move-question-down-button disabled'><i class='fa fa-arrow-down' aria-hidden='true'></i></div>";
		}

		 if(value["type"] == "agent") {
		$(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='selected-question-"+index+"-yes' type='radio' value='YES' name='selected-question-"+index+"'><label for='selected-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='selected-question-"+index+"-no' type='radio' value='NO' name='selected-question-"+index+"'><label for='selected-question-"+index+"-no'>No</label></div></div>"+removeButton+moveUpButton+moveDownButton+"</div>");
	   }
	   if(value["type"] == "yn") {
		$(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='selected-question-"+index+"-yes' type='radio' value='YES' name='selected-question-"+index+"'><label for='selected-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='selected-question-"+index+"-no' type='radio' value='NO' name='selected-question-"+index+"'><label for='selected-question-"+index+"-no'>No</label></div></div>"+removeButton+moveUpButton+moveDownButton+"</div>");
	   }
	   if(value["type"] == "text") {
		$(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><input name='selected-question-"+index+"' type='text' value='' placeholder='Text Answer' readonly>"+removeButton+moveUpButton+moveDownButton+"</div>");

	   }
	   if(value["type"] == "mc") {

		$(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='full-width-checkboxes answers-section' id='selected-answers-section-"+index+"'></div>"+removeButton+moveUpButton+moveDownButton+"</div>");


		$.each(value["choices"], function(i, v) {
			$("#edit-question-"+value["questionID"]+" .answers-section").append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='selected-question-"+index+"' id='selected-question-"+index+"-"+i+"'><label for='selected-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
		});

	   }

    if(value["mandatory"] == "YES") {
        $("#edit-question-"+value["questionID"]+" .remove-question-button").hide();
    }

		if(value["positionLocked"] == "YES") {
        $("#edit-question-"+value["questionID"]+" .move-question-up-button").hide();
    }

		if(value["positionLocked"] == "YES") {
        $("#edit-question-"+value["questionID"]+" .move-question-down-button").hide();
    }

	});
}

function loadDefaultQuestions(questions) {
    $(".questions").html("");
    $(".num-questions").html(questions.length);

    var total_questions = questions.length;
    $.each(questions, function(index, value) {

        var removeButton = "<div class='remove-question-button' onclick='removeDefaultQuestion(&#39;"+value["questionID"]+"&#39;)'><i class='fa fa-trash' aria-hidden='true'></i></div>";
        var moveUpButton = "<div class='move-question-up-button' onclick='moveDefaultQuestionUp("+index+")'><i class='fa fa-arrow-up' aria-hidden='true'></i></div>";
        var moveDownButton = "<div class='move-question-down-button' onclick='moveDefaultQuestionDown("+index+")'><i class='fa fa-arrow-down' aria-hidden='true'></i></div>";

        if(index == 0) {
            moveUpButton = "<div class='move-question-up-button disabled'><i class='fa fa-arrow-up' aria-hidden='true'></i></div>";
        }

        if (index >= total_questions - 1) {
            moveDownButton = "<div class='move-question-down-button disabled'><i class='fa fa-arrow-down' aria-hidden='true'></i></div>";
        }

         if(value["type"] == "agent") {
        $(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='selected-question-"+index+"-yes' type='radio' value='YES' name='selected-question-"+index+"'><label for='selected-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='selected-question-"+index+"-no' type='radio' value='NO' name='selected-question-"+index+"'><label for='selected-question-"+index+"-no'>No</label></div></div>"+removeButton+moveUpButton+moveDownButton+"</div>");
       }
       if(value["type"] == "yn") {
        $(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='half-width-checkboxes'><div class='checkbox'><input id='selected-question-"+index+"-yes' type='radio' value='YES' name='selected-question-"+index+"'><label for='selected-question-"+index+"-yes'>Yes</label></div><div class='checkbox'><input id='selected-question-"+index+"-no' type='radio' value='NO' name='selected-question-"+index+"'><label for='selected-question-"+index+"-no'>No</label></div></div>"+removeButton+moveUpButton+moveDownButton+"</div>");
       }
       if(value["type"] == "text") {
        $(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><input name='selected-question-"+index+"' type='text' value='' placeholder='Text Answer' readonly>"+removeButton+moveUpButton+moveDownButton+"</div>");

       }
       if(value["type"] == "mc") {

        $(".questions").append("<div class='form-question-row' id='edit-question-"+value["questionID"]+"'><div class='question-section'>"+value["question"]+"</div><div class='full-width-checkboxes answers-section' id='selected-answers-section-"+index+"'></div>"+removeButton+moveUpButton+moveDownButton+"</div>");


        $.each(value["choices"], function(i, v) {
            $("#edit-question-"+value["questionID"]+" .answers-section").append("<div class='checkbox'><input type='radio' value='"+v["answer"]+"' name='selected-question-"+index+"' id='selected-question-"+index+"-"+i+"'><label for='selected-question-"+index+"-"+i+"'>"+v["answer"]+"</label></div>");
        });

       }

    if(value["mandatory"] == "YES") {
        $("#edit-question-"+value["questionID"]+" .remove-question-button").hide();
    }

		if(value["positionLocked"] == "YES") {
        $("#edit-question-"+value["questionID"]+" .move-question-up-button").hide();
    }

    if(value["positionLocked"] == "YES") {
        $("#edit-question-"+value["questionID"]+" .move-question-down-button").hide();
    }

    });
}



function showDefaultForm() {

    if(online) {
        loading(true);
        getDefaultForm(function(){
            loading(false);
            curPanel = "defaultform";

            getStandardQuestions();
            getCustomQuestions();

            temp_default_questions = default_form["questionsObj"].slice(0);
            $(".num-questions").html(temp_default_questions.length);


            if (temp_default_questions.length == 0) {
                $(".questions").html("");
                $(".questions").html("<span style='color:#666;'>You have not added any additional questions.</span>");
            } else {
                loadDefaultQuestions(temp_default_questions);
            }

            if (default_form["mobile"] == "YES") {
                $("#default-allow-mobile").prop( "checked", true );
            } else {
                console.log("allow-mobile off");
                $("#default-allow-mobile").prop( "checked", false );
            }


            if (default_form["autoEmail"] == "YES") {
                $("#default-allow-autoEmail").prop( "checked", true );
            } else {
                $("#default-allow-autoEmail").prop( "checked", false );
            }

            if ("autoEmailOffset" in default_form) {
                $("#default-autoEmail-offset").val( default_form["autoEmailOffset"] );
            } else {
                $("#default-autoEmail-offset").val(3);
            }

            if (default_form["autoBCC"] == "YES") {
                $("#default-allow-bcc").prop( "checked", true );
            } else {
                $("#default-allow-bcc").prop( "checked", false );
            }

						if ("fontSize" in default_form) {
                $("#default-font-size").val( default_form["fontSize"] );
            } else {
                $("#default-font-size").val("regular");
            }

            if (default_form["cnEnabled"] == "YES") {
                $("#default-allow-cnEnabled").prop( "checked", true );
            } else {
                $("#default-allow-cnEnabled").prop( "checked", false );
            }

            if (default_form["emailMandatory"] == "YES") {
                $("#default-allow-MandatoryEmail").prop( "checked", true );
            } else {
                $("#default-allow-MandatoryEmail").prop( "checked", false );
            }

            if (default_form["phoneMandatory"] == "YES") {
                $("#default-allow-MandatoryPhone").prop( "checked", true );
            } else {
                $("#default-allow-MandatoryPhone").prop( "checked", false );
            }

            showPopup("default-form");
            $(".popup-box-content-right").scrollTop(0);
            $(".popup-box-content-left").scrollTop(0);
        });
    } else {
        showAlert("Offline Mode","This function requires a live internet connection.");

    }

}

function showEditForm(pid) {
	curPanel = "editform";
	getStandardQuestions();

	getCustomQuestions();

	var propDisabled = false;
	if("disabled" in properties[pid]) {
		if(properties[pid]["disabled"] == "YES") {
			propDisabled = true;
		}
	}

    var propShared = false;
	if("shared" in properties[pid]) {
		if(properties[pid]["shared"] == "YES") {
			propShared = true;
		}
	}

	if(propDisabled) {
		showAlert("Property Inactive","Please activate property in Property Details.");
	} else if(propShared && user["teamRole"] == "member"){
        showAlert("Owner Access Only","Only Owners can access this function.");
    } else {
        $("#gospaciourl").val("https://spac.io/l/"+properties[pid]["propertyID"]+"/");
				$(".print-instructions-url").attr("href","/dashboard/printout/?id="+properties[pid]["propertyID"]+"&c="+main_color+"&c2="+main_color);
        if(properties[pid]["addr1"] != "") {
            $(".settings-property-address").html(""+properties[pid]["addr1"]);
        } else {
            $(".settings-property-address").html(""+properties[pid]["title"]);
        }

        $(".settings-property-photo").css("background-image","url("+properties[pid]["image"]+")");

		updateLastActive();
		curPID = pid;

		temp_questions = properties[pid]["questionsObj"].slice(0);
		$(".num-questions").html(temp_questions.length);

		if (temp_questions.length == 0) {
			$(".questions").html("");
			$(".questions").html("<span style='color:#666;'>You have not added any additional questions.</span>");
		} else {
			loadQuestions(temp_questions);
		}

		if("mobile" in properties[pid]) {
			console.log("allow-mobile exists");
			if (properties[pid]["mobile"] == "YES") {
				console.log("allow-mobile on");
				$("#allow-mobile").prop( "checked", true );
			} else {
				console.log("allow-mobile off");
				$("#allow-mobile").prop( "checked", false );
			}
		} else {
			$("#allow-mobile").prop( "checked", true );
		}

        if (user["passcodeProtection"] == "YES") {
            $("#allow-passcode").prop( "checked", true );
        } else {
            $("#allow-passcode").prop( "checked", false );
        }

        $("#edit-pin").val(user["pin"]);

        if(!online) {
            $(".passcode-section").hide();
        } else {
            $(".passcode-section").show();
        }

				if ("fontSize" in properties[pid]) {
            $("#font-size").val( properties[pid]["fontSize"] );
        } else {
            $("#font-size").val("regular");
        }

		if (properties[pid]["autoEmail"] == "YES") {
			$("#allow-autoEmail").prop( "checked", true );
		} else {
			$("#allow-autoEmail").prop( "checked", false );
		}

        if ("autoEmailOffset" in properties[pid]) {
			$("#autoEmail-offset").val( properties[pid]["autoEmailOffset"] );
		} else {
			$("#autoEmail-offset").val(3);
		}

		if (properties[pid]["autoBCC"] == "YES") {
			$("#allow-bcc").prop( "checked", true );
		} else {
			$("#allow-bcc").prop( "checked", false );
		}

		if (properties[pid]["cnEnabled"] == "YES") {
			$("#allow-cnEnabled").prop( "checked", true );
		} else {
			$("#allow-cnEnabled").prop( "checked", false );
		}

		if("brokersEnabled" in properties[pid]) {
			if (properties[pid]["brokersEnabled"] == "YES") {
				$("#enable-brokers").prop( "checked", true );
			} else {
				$("#enable-brokers").prop( "checked", false );
			}
		} else {
			$("#enable-brokers").prop( "checked", false );
		}

		if("cloudcmaMLS" in properties[pid]) {
			$("input[name='cloudcmaMLS']").val(properties[pid]["cloudcmaMLS"]);
		} else {
			$("input[name='cloudcmaMLS']").val("");
		}

        if("moxipresentURL" in properties[pid]) {
			$("input[name='moxipresentURL']").val(properties[pid]["moxipresentURL"]);
		} else {
			$("input[name='moxipresentURL']").val("");
		}

        if("touchcmaURL" in properties[pid]) {
			$("input[name='touchcmaURL']").val(properties[pid]["touchcmaURL"]);
		} else {
			$("input[name='touchcmaURL']").val("");
		}

		if (properties[pid]["emailMandatory"] == "YES") {
			$("#allow-MandatoryEmail").prop( "checked", true );
		} else {
			$("#allow-MandatoryEmail").prop( "checked", false );
		}

		if (properties[pid]["phoneMandatory"] == "YES") {
			$("#allow-MandatoryPhone").prop( "checked", true );
		} else {
			$("#allow-MandatoryPhone").prop( "checked", false );
		}

		if("cloudcmaID" in user["integrations"]) {
			if(user["integrations"]["cloudcmaID"] == "" || user["integrations"]["cloudcmaID"] == "N/A") {
				$(".cloudcma").hide();
			} else {
				$(".cloudcma").show();
			}
		} else {
			$(".cloudcma").hide();
		}

        if("moxipresentEnabled" in user["integrations"]) {
			if(user["integrations"]["moxipresentEnabled"] == "" || user["integrations"]["moxipresentEnabled"] == "NO") {
				$(".moxipresent").hide();
			} else if(user["integrations"]["moxipresentEnabled"] == "YES") {
				$(".moxipresent").show();
			}
		} else {
			$(".moxipresent").hide();
		}

        if("touchcmaEnabled" in user["integrations"]) {
			if(user["integrations"]["touchcmaEnabled"] == "" || user["integrations"]["touchcmaEnabled"] == "NO") {
				$(".touchcma").hide();
			} else if(user["integrations"]["touchcmaEnabled"] == "YES") {
				$(".touchcma").show();
			}
		} else {
			$(".touchcma").hide();
		}

		showPopup("edit-form");
        $(".popup-box-content-right").scrollTop(0);
		$(".popup-box-content-left").scrollTop(0);

	}
}

function setFormToDefault() {
    loading(true);
    getDefaultForm(function(){
        loading(false);
        temp_questions = default_form["questionsObj"].slice(0);
        $(".num-questions").html(temp_questions.length);

        if (temp_questions.length == 0) {
            $(".questions").html("");
            $(".questions").html("<span style='color:#666;'>You have not added any additional questions.</span>");
        } else {
            loadQuestions(temp_questions);
        }

        if (default_form["autoEmail"] == "YES") {
            $("#allow-autoEmail").prop( "checked", true );
        } else {
            $("#allow-autoEmail").prop( "checked", false );
        }

        $("#autoEmail-offset").val( default_form["autoEmailOffset"] );

        if (default_form["autoBCC"] == "YES") {
            $("#allow-bcc").prop( "checked", true );
        } else {
            $("#allow-bcc").prop( "checked", false );
        }

        if (default_form["cnEnabled"] == "YES") {
            $("#allow-cnEnabled").prop( "checked", true );
        } else {
            $("#allow-cnEnabled").prop( "checked", false );
        }

        showAlert("Default Form","Your Sign In Form has been set to Default.");
    });
}

function saveCurrentForm() {
	properties[curPID]["questionsObj"] = temp_questions.slice(0);
	saveForm(curPID, function() {
		hidePopup("edit-form");
	})

}

function addQuestion(questionID, questionIndex, type) {

	var questionObj = {};

	if(type == "standard") {
		questionObj = standardQuestions[questionIndex];
	} else if(type == "custom"){
		questionObj = customQuestions[questionIndex];
	}

	var index = temp_questions.length;

	if (index == 0) {
		$(".questions").html("");
	}

	var question_exists = false;

	$.each(temp_questions, function(index,value){
		if(value["questionID"] == questionID) {
			question_exists = true;
		}
	})

	if(temp_questions.length < 6 && !question_exists) {
		temp_questions.push(questionObj);

		loadQuestions(temp_questions);
		$(".num-questions").html(temp_questions.length);
	} else {
		if(temp_questions.length >= 6) {
			showAlert("Limit Reached", "You can only add up to 6 questions.");
		}
		if(question_exists) {
			showAlert("Questions Already Exists", "You have already added this question in your form.");
		}

	}

	closeQuestionsPopup();
}

function addDefaultQuestion(questionID, questionIndex, type) {

	var questionObj = {};

	if(type == "standard") {
		questionObj = standardQuestions[questionIndex];
	} else if(type == "custom"){
		questionObj = customQuestions[questionIndex];
	}

	var index = temp_default_questions.length;

	if (index == 0) {
		$(".questions").html("");
	}

	var question_exists = false;

	$.each(temp_default_questions, function(index,value){
		if(value["questionID"] == questionID) {
			question_exists = true;
		}
	})

	if(temp_default_questions.length < 6 && !question_exists) {
		temp_default_questions.push(questionObj);

		loadDefaultQuestions(temp_default_questions);
		$(".num-questions").html(temp_default_questions.length);
	} else {
		if(temp_default_questions.length >= 6) {
			showAlert("Limit Reached", "You can only add up to 6 questions.");
		}
		if(question_exists) {
			showAlert("Questions Already Exists", "You have already added this question in your form.");
		}

	}

	closeQuestionsPopup();
}

function moveQuestionUp(i) {
	var o_index = i;
	var t_index = i - 1;

	if(t_index >= 0) {
		if(temp_questions[t_index]["positionLocked"] != "YES") {
			var temp_o = temp_questions[t_index];
			temp_questions[t_index] = temp_questions[o_index];
			temp_questions[o_index] = temp_o;
		} else {
			showAlert("Cannot Be Rearranged ", "The position of your brokerage’s default questions have been locked and cannot be rearranged.");

		}
	}

	loadQuestions(temp_questions);
}

function moveQuestionDown(i) {
	var o_index = i;
	var t_index = i + 1;
	if(t_index < temp_questions.length) {
		if(temp_questions[t_index]["positionLocked"] != "YES") {
			var temp_o = temp_questions[t_index];
			temp_questions[t_index] = temp_questions[o_index];
			temp_questions[o_index] = temp_o;
		} else {
			showAlert("Cannot Be Rearranged ", "The position of your brokerage’s default questions have been locked and cannot be rearranged.");

		}
	}
	loadQuestions(temp_questions);
}

function removeQuestion(questionID) {
    var rmvIndex = 0;
    for (var i = 0; i < temp_questions.length; i++) {
        if (temp_questions[i]["questionID"] == questionID) {
            rmvIndex = i;
        }
    }

    if(temp_questions[rmvIndex]["mandatory"] == "YES") {
        showAlert("Mandatory Question","Your brokerage has made this question mandatory.");

    } else {

        temp_questions.splice(rmvIndex,1);

        if (temp_questions.length == 0) {
            $(".questions").html("<span style='color:#666;'>You have not added any additional questions.</span>");
        }
        $(".num-questions").html(temp_questions.length);
        loadQuestions(temp_questions);

    }
}


function moveDefaultQuestionUp(i) {
    var o_index = i;
    var t_index = i - 1;

    if(t_index >= 0) {
        if(temp_default_questions[t_index]["positionLocked"] != "YES") {
            var temp_o = temp_default_questions[t_index];
            temp_default_questions[t_index] = temp_default_questions[o_index];
            temp_default_questions[o_index] = temp_o;
        } else {
            showAlert("Cannot Be Rearranged ", "The position of your brokerage’s default questions have been locked and cannot be rearranged.");
        }
    }

    loadDefaultQuestions(temp_default_questions);

}

function moveDefaultQuestionDown(i) {
    var o_index = i;
    var t_index = i + 1;
    if(t_index < temp_default_questions.length) {
        if(temp_default_questions[t_index]["positionLocked"] != "YES") {
            var temp_o = temp_default_questions[t_index];
            temp_default_questions[t_index] = temp_default_questions[o_index];
            temp_default_questions[o_index] = temp_o;
        } else {
            showAlert("Cannot Be Rearranged ", "The position of your brokerage’s default questions have been locked and cannot be rearranged.");

        }
    }
    loadDefaultQuestions(temp_default_questions);
}


function removeDefaultQuestion(questionID) {
    var rmvIndex = 0;
    for (var i = 0; i < temp_default_questions.length; i++) {
        if (temp_default_questions[i]["questionID"] == questionID) {
            rmvIndex = i;
        }
    }

    if(temp_default_questions[rmvIndex]["mandatory"] == "YES") {
        showAlert("Mandatory Question","Your brokerage has made this question mandatory.");

    } else {

        temp_default_questions.splice(rmvIndex,1);

        if (temp_default_questions.length == 0) {
            $(".questions").html("<span style='color:#666;'>You have not added any additional questions.</span>");
        }
        $(".num-questions").html(temp_default_questions.length);
        loadDefaultQuestions(temp_default_questions);

    }
}

function showQuestionsPopup(type) {

	if(type == "standard") {
		$(".questions-popup").show();
		$(".standard-questions").show();
		$(".custom-questions").hide();
		$(".new-custom-questions").hide();
		$(".question-popup-title").html("CHOOSE A STANDARD QUESTION");
	}

	if(type == "custom") {
		$(".questions-popup").show();
		$(".custom-questions").show();
		$(".standard-questions").hide();
		$(".new-custom-questions").hide();
		$(".question-popup-title").html("CHOOSE A SAVED CUSTOM QUESTION");
	}

	if(type == "new-custom") {
		if(online) {
			$(".questions-popup").show();
			$(".standard-questions").hide();
			$(".custom-questions").hide();
			$(".new-custom-questions").show();
			$(".create-custom-question-button").hide();
			$(".reset-custom-question-button").hide();
      $(".add-choices").hide();
      $("input:radio[name='custom-question-type']").each(function(i) {
         this.checked = false;
      });
      $("input:radio[name='custom-question-text']").val("");
			$(".question-popup-title").html("CREATE A CUSTOM QUESTION");
			$(".add-custom-question-step2").hide();
			$(".create-custom-question-button").hide();
			$(".reset-custom-question-button").hide();


			resetChoices();
		} else {
			showAlert("Offline Mode","This function requires a live internet connection.");
		}
	}
}

function toggleAddChoices() {
    if(curPanel == "defaultform") {
        var temp_val = $("#popup-default-form input[name='custom-question-type']:checked").val();
    }
    if(curPanel == "editform") {
        var temp_val = $("#popup-edit-form input[name='custom-question-type']:checked").val();
    }

	if(temp_val == "mc") {
		resetChoices();
		$(".add-choices").show();
		$(".reset-custom-question-button").show();
	} else {
		$(".add-choices").hide();
		$(".reset-custom-question-button").hide();
	}
	$(".add-custom-question-step2").show();
	$(".create-custom-question-button").show();
}

function addChoice() {

	if(curPanel == "defaultform") {
        var temp_val = $("#popup-default-form input[name='custom-question-choice']").val();
    } else{
        var temp_val = $("#popup-edit-form input[name='custom-question-choice']").val();
    }

	if (temp_val == "") {
		showAlert("Blank Answer","You cannot add a blank answer.");
	} else {
	var temp_choice = {};
		temp_choice["answer"] = temp_val;
		temp_choice["answerCN"] = "";
		temp_choice["type"] = "radio";
		customChoicesToAdd.push(temp_choice);
		$(".added-choices").append("<div class='mc-choice'><div class='disabled-radio'></div>"+temp_val+"</div>");
		$("input[name='custom-question-choice']").val("");
	}

}

function resetChoices() {
	customChoicesToAdd = [];
	$(".added-choices").html("");
	$("input[name='custom-question-choice']").val("");
}


function closeQuestionsPopup(type) {
	$(".questions-popup").hide();
}


function showBroadcastMessage(pid) {

	$("#broadcastMessage_Form input[name='propertyID']").val(properties[pid]["propertyID"]);
	$("#broadcast-property-title").html(properties[pid]["title"]);
	showPopup("broadcast-message");

}

function toggleBombbomb() {
  if ($("#broadcast-bombbomb").is(':checked')) {
    $('#broadcast-message').trumbowyg('html',' ');
    $(".broadcast-cc-option").hide();
    $(".broadcast-template-option").hide();
    $(".broadcast-small-caption").hide();
    $(".bombbomb-videos").show();
    getBBVideos(function(data){
        $("#broadcast-bombbomb-video").html("");
        $.each(data.list.info, function(index,value){
            $("#broadcast-bombbomb-video").append("<option value='"+value["id"]+"'>"+value["name"]+"</option>");
        });

    });
  } else {
    $('#broadcast-message').trumbowyg('html',' ');
    $(".broadcast-cc-option").show();
    $(".broadcast-small-caption").show();
    $(".bombbomb-videos").hide();
    $(".broadcast-template-option").show();

  }

}



function showChangePassword() {
	showPopup("change-password");

}


function showScheduleOpenHouse() {

        $('.openhouse-scheduler-overlay').show();

        var d = new Date();
        d = nextDay(d, 0);
        var n = d.getDay();
        var date_timestamp = d.getTime();

        //console.log($.format.date(date_timestamp, "yyyy-MM-dd"));
        $("#scheduler-form")[0].reset();
        $(".datetime-input").val($.format.date(date_timestamp, "yyyy-MM-dd"));
        $("input[name='oh-starttime-1']").val("14:00:00");
        $("input[name='oh-endtime-1']").val("16:00:00");

        schedulers_open = 1;


}

function showEditScheduleOpenHouse(index) {
    curScheduleIndex = index;
    $('.edit-scheduler-overlay').show();

    var date_timestamp = openhouses_scheduled[index]["starttime_timestamp"];
    var starttime_timestamp = openhouses_scheduled[index]["starttime_timestamp"];
    var endtime_timestamp = openhouses_scheduled[index]["endtime_timestamp"];
    var date_string = $.format.date(date_timestamp, "yyyy-MM-dd");
    var starttime_string = $.format.date(starttime_timestamp, "HH:mm:ss");
    var endtime_string = $.format.date(endtime_timestamp, "HH:mm:ss");

    $("input[name='edit-oh-date']").val(date_string);
    $("input[name='edit-oh-starttime']").val(starttime_string);
    $("input[name='edit-oh-endtime']").val(endtime_string);

}

function hideScheduleOpenHouse() {
  $('.openhouse-scheduler-overlay').hide();
  schedulers_open = 1;
}

function hideEditScheduleOpenHouse() {
  $('.edit-scheduler-overlay').hide();
}

function scheduleAdditionalOpenhouse () {
    var total_scheduled_oh = schedulers_open + openhouses_scheduled.length;
    if(total_scheduled_oh == 1) {
        $(".open-house-2").show();
        $(".open-house-3").hide();
        schedulers_open = 2;
    } else if (total_scheduled_oh == 2) {
        $(".open-house-2").show();
        $(".open-house-3").show();
        schedulers_open = 3;
        $(".additional-openhouse").hide();
    } else {
        showAlert("Cannot Schedule Open House","You can only schedule a total of 3 active open houses.");
    }
}

function saveScheduledOpenhouse() {
    var o = new Date();
    var n = o.getTimezoneOffset() * 60 * 1000;


    var date_string,date_timestamp,starttime_string,endtime_string,starttime_arr,endtime_arr,starttime_hours,endtime_hours,starttime_minutes,endtime_minutes,starttime_ms,endtime_ms,starttime_timestamp,endtime_timestamp,recurring;

    date_string = $("input[name='edit-oh-date']").val();
    date_timestamp = Date.parse(date_string);
    date_timestamp = date_timestamp + n;
    starttime_string = $("input[name='edit-oh-starttime']").val();
    endtime_string = $("input[name='edit-oh-endtime']").val();
    starttime_arr = starttime_string.split(":");
    endtime_arr = endtime_string.split(":");
    starttime_hours = parseInt(starttime_arr[0]);
    endtime_hours = parseInt(endtime_arr[0]);
    starttime_minutes = parseInt(starttime_arr[1]);
    endtime_minutes = parseInt(endtime_arr[1]);

    starttime_ms = (starttime_hours*60*60*1000) + (starttime_minutes*60*1000);
    endtime_ms = (endtime_hours*60*60*1000) + (endtime_minutes*60*1000);

    starttime_timestamp = date_timestamp + starttime_ms;
    endtime_timestamp = date_timestamp + endtime_ms;

    date_string = $.format.date(date_timestamp, "MMM d, yyyy");
    starttime_string = $.format.date(starttime_timestamp, "h:mm a");
    endtime_string = $.format.date(endtime_timestamp, "h:mm a");


    openhouses_scheduled[curScheduleIndex]["date"] = date_string;
    openhouses_scheduled[curScheduleIndex]["starttime"] = starttime_string;
    openhouses_scheduled[curScheduleIndex]["endtime"] = endtime_string;
    openhouses_scheduled[curScheduleIndex]["starttime_timestamp"] = starttime_timestamp;
    openhouses_scheduled[curScheduleIndex]["endtime_timestamp"] = endtime_timestamp;



    editScheduledOpenhouse(openhouses_scheduled[curScheduleIndex], function(){
        loadOpenhouseSchedule();
        hideEditScheduleOpenHouse();
    });


}

function scheduleOpenhouse() {
    var o = new Date();
    var n = o.getTimezoneOffset() * 60 * 1000;
    var week_ms = 7*24*60*60*1000;
    var ohs_to_schedule = [];

    var date_string,date_timestamp,starttime_string,endtime_string,starttime_arr,endtime_arr,starttime_hours,endtime_hours,starttime_minutes,endtime_minutes,starttime_ms,endtime_ms,starttime_timestamp,endtime_timestamp,recurring;

    for(i=1; i<=schedulers_open; i++) {
        date_string = $("input[name='oh-date-"+i+"']").val();
        date_timestamp = Date.parse(date_string);
        date_timestamp = date_timestamp + n;
        starttime_string = $("input[name='oh-starttime-"+i+"']").val();
        endtime_string = $("input[name='oh-endtime-"+i+"']").val();
        starttime_arr = starttime_string.split(":");
        endtime_arr = endtime_string.split(":");
        starttime_hours = parseInt(starttime_arr[0]);
        endtime_hours = parseInt(endtime_arr[0]);
        starttime_minutes = parseInt(starttime_arr[1]);
        endtime_minutes = parseInt(endtime_arr[1]);

        starttime_ms = (starttime_hours*60*60*1000) + (starttime_minutes*60*1000);
        endtime_ms = (endtime_hours*60*60*1000) + (endtime_minutes*60*1000);

        starttime_timestamp = date_timestamp + starttime_ms;
        endtime_timestamp = date_timestamp + endtime_ms;

        recurring = $("select[name='oh-recurring-"+i+"']").val();

        date_string = $.format.date(date_timestamp, "MMM d, yyyy");
        starttime_string = $.format.date(starttime_timestamp, "h:mm a");
        endtime_string = $.format.date(endtime_timestamp, "h:mm a");
        ohs_to_schedule.push({
            "date":date_string,
            "starttime":starttime_string,
            "endtime":endtime_string,
            "starttime_timestamp":starttime_timestamp,
            "endtime_timestamp":endtime_timestamp,
            "pid":curPID,
            "managerID":user["id"]
        });
        if(recurring == "weekly") {
            for(j=i; j<3; j++) {
              date_timestamp = date_timestamp + week_ms;
              starttime_timestamp = date_timestamp + starttime_ms;
              endtime_timestamp = date_timestamp + endtime_ms;
              date_string = $.format.date(date_timestamp, "MMM d, yyyy");
              starttime_string = $.format.date(starttime_timestamp, "h:mm a");
              endtime_string = $.format.date(endtime_timestamp, "h:mm a");
              ohs_to_schedule.push({
                "date":date_string,
                "starttime":starttime_string,
                "endtime":endtime_string,
                "starttime_timestamp":starttime_timestamp,
                "endtime_timestamp":endtime_timestamp,
                "pid":curPID,
                "managerID":user["id"]
              });
            }
        }
    }

    var schedule_limit = 3 - openhouses_scheduled.length;

    ohs_to_schedule = ohs_to_schedule.splice(0,schedule_limit);

    scheduleOpenhouses(ohs_to_schedule, function(){
        getScheduledOpenhouses(curPID,function(){
            loadOpenhouseSchedule();
            hideScheduleOpenHouse();
        });

    });


}

function loadOpenhouseSchedule() {
  $('.openhouses-scheduled').html(" ");

  $.each(openhouses_scheduled, function(index,value){
    if(value["id"] == "restricted") {
      $('.openhouses-scheduled').append("<div class='openhouse-restricted'>You do not have permission to access this listing's open house schedule.</div>");

    } else {
      var startdate = new Date(value["starttime_timestamp"]);
      var enddate = new Date(value["endtime_timestamp"]);
      var starttime = startdate.toLocaleTimeString();
      var endtime = enddate.toLocaleTimeString();
      var cur_date = $.format.date(value["starttime_timestamp"], "MMM dd, yyyy");
      $('.openhouses-scheduled').append("<div class='openhouse-scheduled'>"+cur_date+" - "+starttime+" to "+endtime+"<div class='delete-openhouse' onclick='confirmDeleteOpenhouse("+index+")'><i class='fa fa-trash' aria-hidden='true'></i></div><div class='edit-openhouse' onclick='showEditScheduleOpenHouse("+index+")'><i class='fa fa-edit' aria-hidden='true'></i></div></div>");
    }

  });
}




function showSortedRegistrantsByType(type, sort, order) {

	var num_buyers = 0;
	var num_brokers = 0;

	if(type == "buyers") {
		$("#registrants-filter-bar .sub-container-filter-tab").css("color","#666");
		$("#registrants-filter-back").css("left","0px");
		$("#buyers-filter-tab").css("color","white");
		$(".buyers-titles").show();
		$(".brokers-titles").hide();
	} else if(type == "brokers") {
		$("#registrants-filter-bar .sub-container-filter-tab").css("color","#666");
		$("#registrants-filter-back").css("left","100px");
		$("#brokers-filter-tab").css("color","white");
		$(".buyers-titles").hide();
		$(".brokers-titles").show();
	}

	var pid = curPID;

	if (typeof sort === "undefined" || sort === null) {
		sort = "dateCreated";
	}

	if (typeof order === "undefined" || order === null) {
		order = "desc";
	}

	var showBrokers = false;

	if (type == "buyers") {
		showBrokers = false;
	}

	if (type == "brokers") {
		showBrokers = true;
	}

	//Clear Registrants panel
	$("#popup-view-registrants .popup-box-content .registrants").html("");
	var count = 0;

	var sorted_registrants = [];

	$.each(registrantsSearchResult, function(index, value) {
		value["rid"] = index;
		value["leadscore"] = calculateLeadScore(index);

			if(value["isBroker"] == "YES") {
				if((value["pid"] == pid || pid == "all")){
					num_brokers++;
				}
			}
			if(value["isBroker"] == "NO") {
				if((value["pid"] == pid || pid == "all")){
					num_buyers++;
				}
			}

		if(type == "brokers") {
			if(value["isBroker"] == "YES") {
				sorted_registrants.push(value);
			}
		} else if(type == "buyers") {
			if(value["isBroker"] == "NO") {

				sorted_registrants.push(value);
			}
		}
	});

	if(sort == "dateCreated") {
		sorted_registrants = customSortDateTime(sorted_registrants,"dateCreated",order);
	} else if (sort == "hasClient") {
		sorted_registrants = customSortHasClient(sorted_registrants,"hasClient",order);
	} else  {
		sorted_registrants = customSort(sorted_registrants,sort,order);
	}

	$.each(sorted_registrants, function(i, value) {
		var index = value["rid"];
		var isBroker = false;

		if("isBroker" in value) {
			if (value["isBroker"] == "YES") {
				isBroker = true;
			}
		}

		if((value["pid"] == pid || pid == "all") && (isBroker == showBrokers)) {

			count++;
			var name = capitalizeFirstLetterEachWordSplitBySpace(value["name"]);
			var phone = value["phone"];
			var email = value["email"];
			var company = "";

			if("company" in value) {
				company = value["company"];
			}

			var score = 0;
			var hasName = false;
			var hasEmail = false;
			var hasPhone = false;
			var verified = '<i style="color:#ddd;font-size:20px;" class="fa fa-check-circle" aria-hidden="true"></i>';

			var reg_pphoto = "img/stock/pphoto.jpg";

			if("fullcontact" in value) {
				if(!("status" in value["fullcontact"])) {

				} else if (value["fullcontact"]["status"] == 200 && user["type"] != "freemium" && user["type"] != "lite") {
					verified = '<i style="color:#578dff;font-size:20px;" class="fa fa-check-circle" aria-hidden="true"></i>';
				}

				if(("photos" in value["fullcontact"]) && user["type"] != "freemium" && user["type"] != "lite") {
					reg_pphoto = value["fullcontact"]["photos"][0]["url"];
				}

			}

			if (!online) {
				reg_pphoto = "img/stock/pphoto.jpg";
			}

			if (user["type"] == "freemium" || isBroker || user["type"] == "lite") {
				score="--";
			} else {
				score = value["leadscore"];
			}

			var temp_hasAgent = value["hasAgent"];

			if (typeof temp_hasAgent == "undefined") {
				temp_hasAgent = "Unknown";
			}

			var datetime = new Date();
			if(typeof value["dateCreated"]["sec"] != "undefined") {
				datetime = new Date(value["dateCreated"]["sec"] * 1000);
			}
			//} else {
			//	var datetime = new Date(value["dateCreated"]);
			//}

			if(isBroker) {

				var hasClient = "--";

				if(value["answersObj"][0]["answer"] == "YES") {
					hasClient = "Yes";
				}
				if(value["answersObj"][0]["answer"] == "NO") {
					hasClient = "No";
				}
				if(index.substring(0,4) != "temp") {

					$("#popup-view-registrants .popup-box-content .registrants").append("<div id='registrant-"+index+"' class='registrant-row' onclick='showRegistrant(&#39;"+index+"&#39;)'><div class='registrant-name' style='width:240px;'><div class='registrant-pphoto' style='background-image:url("+reg_pphoto+")'></div>"+name+"</div><div class='registrant-email' style='width:300px;'>"+email+"</div><div class='registrant-company' style='width:200px;'>"+company+"</div><div class='registrant-phone' style='width:130px;'>"+hasClient+"</div><div class='registrant-phone' style='width:150px;'>"+datetime.toLocaleDateString()+"</div></div>");
				} else {
					$("#popup-view-registrants .popup-box-content .registrants").append("<div id='registrant-"+index+"' class='registrant-row' onclick='showRegistrant(&#39;"+index+"&#39;)'><div class='registrant-name unsynched' style='width:240px;'><div class='registrant-pphoto' style='background-image:url("+reg_pphoto+")'></div>"+name+"</div><div class='registrant-email' style='width:300px;'>"+email+"</div><div class='registrant-company' style='width:200px;'>"+company+"</div><div class='registrant-phone' style='width:130px;'>"+hasClient+"</div><div class='registrant-phone' style='width:150px;'>"+datetime.toLocaleDateString()+"</div></div>");
				}

			} else {

				if(index.substring(0,4) != "temp") {

					$("#popup-view-registrants .popup-box-content .registrants").append("<div id='registrant-"+index+"' class='registrant-row' onclick='showRegistrant(&#39;"+index+"&#39;)'><div class='registrant-name' style='width:240px;'><div class='registrant-pphoto' style='background-image:url("+reg_pphoto+")'></div>"+name+"</div><div class='registrant-email' style='width:300px;'>"+email+"<div class='docusign-button' onclick='launchDocusign(event,&#39;"+index+"&#39;)'></div></div><div class='registrant-phone' style='width:80px;text-align:center;'>"+verified+"</div><div class='registrant-phone' style='width:130px;'>"+temp_hasAgent+"</div><div class='registrant-phone' style='width:130px;'>"+score+"</div><div class='registrant-phone' style='width:140px;'>"+datetime.toLocaleDateString()+"</div></div>");
				} else {
					$("#popup-view-registrants .popup-box-content .registrants").append("<div id='registrant-"+index+"' class='registrant-row' onclick='showRegistrant(&#39;"+index+"&#39;)'><div class='registrant-name unsynched' style='width:240px;'><div class='registrant-pphoto' style='background-image:url("+reg_pphoto+")'></div>"+name+"</div><div class='registrant-email' style='width:300px;'>"+email+"</div><div class='registrant-phone' style='width:80px;text-align:center;'>"+verified+"</div><div class='registrant-phone' style='width:130px;'>"+temp_hasAgent+"</div><div class='registrant-phone' style='width:130px;'>"+score+"</div><div class='registrant-phone' style='width:140px;'>"+datetime.toLocaleDateString()+"</div></div>");
				}
			}

            if("log" in value) {
                if(value["log"]["docusign"] == "Signed") {
                    $("#registrant-"+index+" .docusign-button").remove();
                }
            }
		}
	});

    if(user["docusignEnabled"] == "YES") {
        $(".docusign-button").show();
    }

	if(count == 0) {
		//$("#popup-view-registrants .popup-box-content .registrants").append("<div class='registrant-row'><div style='margin:10px;'>There are currently no registrants for this property.</div></div>");
	}

	$(".num-visitors").html(num_buyers);
	$(".num-brokers").html(num_brokers);
    $("#registrants-scroll").scrollTop(0);

	//$(".reg-num").html(count);
	$(".registrant-row .registrant-detail-btn").css("color","#"+main_color);
}

function selectBroadcastRegistrants() {
	var representation = $("#broadcast-registrants-representation-filter").val();

	if(representation == "none") {
		$(".broadcast-registrant").prop('checked', false);
	} else if (representation == "all") {
		$(".broadcast-registrant").prop('checked', true);
	} else if (representation == "hasAgent") {
		$(".broadcast-registrant").prop('checked', false);
		$(".broadcast-registrant.hasAgent").prop('checked', true);
	} else if (representation == "noAgent") {
		$(".broadcast-registrant").prop('checked', false);
		$(".broadcast-registrant.noAgent").prop('checked', true);
		$(".broadcast-registrant.unknownAgent").prop('checked', true);
	}
}








function showRegistrants(pid, type) {
	console.log("Show Leads Panel");
    $(".sub-container").hide();
    $(".panel").hide();
    $("#leads-panel").show();
    $(".menu-item").removeClass("selected").addClass("not-selected");
    $(".submenu-item").removeClass("selected").addClass("not-selected");
    $("#leads-menu-item").removeClass("not-selected").addClass("selected");

    curPID = pid;



//    if (typeof type === "undefined" || type === null) {
//        type = "buyers";
//    }
//
//    curPID = pid;
//    console.log("Showing Registrants for PID: "+pid);
//
    if(pid == "all") {
        $("#leads-property").html("All Properties");
        getRegistrants( function(){
              //showPopup("view-registrants");
              showSortedRegistrantsByType("buyers", "dateCreated", "desc");

          });
      } else {
         $("#leads-property").html(properties[pid]["title"]);
        if(online) {
            getRegistrantsByPID(pid, function(){
              //showPopup("view-registrants");
              showSortedRegistrantsByType("buyers", "dateCreated", "desc");
            });
        } else {
            showAlert("Offline Mode","Viewing Visitors from Inactive Properties requires a live internet connection.");

        }
      }
}

function showOhCheckList() {
	console.log("show Open House Checklist");
	closeAllPanels();
	showPropertiesPanel();
	// $("#OhCheckList-panel").show();
}

function downloadOhCheckListPDF() {
	// console.log("show Open House Checklist");
	// closeAllPanels();
	// $("#OhCheckList-panel").show();

	alert("it worked!");

}

function showScoreCalibration() {
	showPopup("score-calibration");
}

function closeScoreCalibration() {
	$("#popup-score-calibration").css({"right":"-100%"});
}


function toggleInnerCheckbox(t,e) {
    var check_val = $(t).find("input[type='checkbox']").prop('checked');
    $(t).find("input[type='checkbox']").prop('checked', !check_val);

}

function toggleCheckbox(t,e) {
    //e.stopPropagation();

}

function toggleBroadcastEmail(e) {
    var temp_email = $(e).val();

    if(e.checked) {
        broadcastEmails.push(temp_email);
        //alert($(e).val());
    } else {
        broadcastEmails.splice( $.inArray(temp_email, broadcastEmails), 1 );
        //alert("unchecked");
    }

}





function createOHObj(pid) {

	var d = new Date();
	var full_date = $.format.date(d.getTime(), "yyyyMMdd");
	var dateHosted = d.getTime();
	var dayHosted = $.format.date(d.getTime(), "ddd");

	if(curOHID == "") {

		curOHID = pid+""+full_date;

		curOHObj = {
			"ohID":curOHID,
			"pid": pid,
			"dateHosted":dateHosted,
			"dayHosted":dayHosted,
			"leadsCount":0,
			"minsHosted":0,
			"startTime":d.getTime(),
			"endTime":0

		};


	} else if ( curOHID == pid+""+full_date) {

	} else {
		addCurOHObj(function(){
			curOHID = pid+""+full_date;

			curOHObj = {
				"ohID":curOHID,
				"pid": pid,
				"dateHosted":dateHosted,
				"dayHosted":dayHosted,
				"leadsCount":0,
				"minsHosted":0,
				"startTime":d.getTime(),
				"endTime":0
			};
		});
	}

}



function confirmAddListing(index) {
	showAlert("Add Property",
		"Add '"+listingsSearchResult[index]["addr1"]+"' to your account?",
		"Ok",
		"Cancel",
		function(){
			$(".alert-overlay").hide();
			addListing(index);
		});
}


function confirmSendTestEmail(type) {
	showAlert("Send Test Email",
		"This will send a test email using this email template to your registered email address.",
		"Send",
		"Cancel",
		function(){
			$(".alert-overlay").hide();
			sendTestEmail(type);
		});
}

function confirmDeleteHosted(index) {

	console.log(index);
	showAlert("Delete Open House Session",
		"Are you sure you want to delete this open house session? This cannot be undone.",
		"Delete",
		"Cancel",
		function(){
			deleteHosted(index, function(){generateReport();});
			$(".alert-overlay").hide();
		});
}

function confirmDeleteOpenhouse(index) {

	console.log(index);
	showAlert("Delete Scheduled Open House",
		"Are you sure you want to delete this scheduled open house? This cannot be undone.",
		"Delete",
		"Cancel",
		function(){
            deleteScheduledOpenhouse(openhouses_scheduled[index]["id"],openhouses_scheduled[index]["AAListingID"],function(){
                getScheduledOpenhouses(curPID, function(){
                    loadOpenhouseSchedule();
                    $(".alert-overlay").hide();
                });
            })

		});
}

function confirmDeleteProperty() {
	if(properties[curPID]["editable"] == false) {
		showAlert("Property Details","The details of this property was added using your company's feed and cannot be edited. Please contact your listings administrator for changes.");
	} else if(!online){

        showAlert("Offline Mode","This function requires a live internet connection.");

    } else {
		var pid = curPID;
		showAlert("Delete Property",
			"Are you sure you want to delete this property? All registrants will be deleted. This cannot be undone.",
			"Delete",
			"Cancel",
			function(){
				deleteProperty(pid);
				$(".alert-overlay").hide();
			});
	}
}

function confirmDeleteCustomQuestion(questionID) {

		showAlert("Delete Custom Question",
			"Are you sure you want to delete this question from your list of saved custom questions? This cannot be undone.",
			"Delete",
			"Cancel",
			function(){
				deleteCustomQuestion(questionID);
				$(".alert-overlay").hide();
			});
}


function confirmDeleteRegistrant() {

	var rid = curRID;

	//alert(rid);

	if(!online && rid.substring(0,4) != "temp") {
		showAlert("Delete Visitor","While in Offline mode, you can only delete registrants that were collected during your current Offline session.");
	} else {
	showAlert("Delete Visitor",
		"Are you sure you want to delete this Visitor? This cannot be undone.",
		"Delete",
		"Cancel",
		function(){
			deleteRegistrant(rid);
			$(".alert-overlay").hide();
		});
	}
}


function openURL(lk) {
	//window.location.href = lk;
   // inAppBrowserRef.removeEventListener('loadstop', browserLoadStopCallBack);

    //inAppBrowserRef = null;
	inAppBrowserRef = window.open(lk, '_blank', 'location=no');
    inAppBrowserRef.removeEventListener('loadstop', browserLoadStopCallBack);
    inAppBrowserRef.addEventListener('loadstop', browserLoadStopCallBack);
}

function browserLoadStopCallBack(params) {
    console.log("Load Stopped: "+params.url);
    var str1 = params.url;
    var str2 = "ws.spac.io/prod/oh/webhooks/docusign";
    if(str1.indexOf(str2) != -1){
        inAppBrowserRef.close();
        //showAlert("Successfully Registered","Thank you!");
        //alertTimer = setTimeout(function(){ $(".alert-overlay").hide(); }, 3000);
    }
}

function loadPublicProfile() {
	openURL("http://www.spac.io/p/"+user["ukey"]+"");
}


function loading(flag, msg, delay) {

	if (typeof delay === "undefined" || delay === null) {
		delay = 0;
	}

	msg = typeof msg !== 'undefined' ? msg : "Loading...";

	if (flag) {
		$(".loading-container .loading-msg").html("Loading...");
		$(".loading-container").show();
	} else {
		$(".loading-container").hide();
	}
}

function showAlert(title, message, button1, button2, buttonfn1, buttonfn2) {


	$(".alert-overlay").show();
	$(".alert-title").html(title);
	$(".alert-message").html(message);
	$(".alert-button-1").off();
	$(".alert-button-2").off();

	if (typeof buttonfn1 === "undefined" || buttonfn1 === null) {
		buttonfn1 = function() {
			$(".alert-overlay").hide();
			clearTimeout(alertTimer);
		}
	}

	if (typeof buttonfn2 === "undefined" || buttonfn2 === null) {
		buttonfn2 = function() {
			$(".alert-overlay").hide();
			clearTimeout(alertTimer);
		}
	}

	if (typeof button1 === "undefined" || button1 === null) {
		button1 = "Ok";
		$(".alert-button-1").html(button1);
		$(".alert-button-1").css("width","100%");
		$(".alert-button-2").hide();
		buttonfn1 = function() {
			$(".alert-overlay").hide();
			clearTimeout(alertTimer);
		}
		$(".alert-button-1").on("click", buttonfn1);

  	} else if (typeof button2 === "undefined" || button2 === null) {
		$(".alert-button-1").html(button1);
		$(".alert-button-2").hide();
		$(".alert-button-1").css("width","100%");
		$(".alert-button-1").on("click", buttonfn1);
		$(".alert-button-2").on("click", buttonfn2);

	} else {
		$(".alert-button-1").show();
		$(".alert-button-2").show();
		$(".alert-button-1").css("width","50%");
		$(".alert-button-2").css("width","50%");
		$(".alert-button-1").html(button1);
		$(".alert-button-2").html(button2);
		$(".alert-button-1").off();
		$(".alert-button-2").off();
		$(".alert-button-1").on("click", buttonfn1);
		$(".alert-button-2").on("click", buttonfn2);
	}

}

function showPopup(popup) {
    $(".popup-scroll").scrollTop(0);
	$(".popup-overlay").show(0,function(){
		$("#popup-"+popup).show();
	});
}

function hidePopup(popup) {
    if(popup == "add-visitor") {
        $("#popup-"+popup).hide();
        $("#popup-view-visitors").show();
    } else if(popup == "view-registrant") {

        if(prevPanel == "visitors") {
            $("#popup-"+popup).hide();
            $("#popup-view-visitors").show();
        } else if(prevPanel == "leads") {
            $(".popup-overlay").hide();
            $("#popup-"+popup).hide();
        } else {
						$(".popup-overlay").hide();
						$("#popup-"+popup).hide();
				}
    } else {
        $(".popup-overlay").hide();
        $("#popup-"+popup).hide();
    }
    curPanel = prevPanel;
    prevPanel = "";
}

function prepImageForUpload(fileURI, maxHeight, maxWidth, completionHandler) {
    console.log("Prep Image Loaded");

    // Convert image
    getFileContentAsBase64(fileURI,function(base64Image){
        //window.open(base64Image);
        console.log(base64Image);
        var blob = new Blob([new Uint8Array(base64Image)], { type: "image/png" });

        //var image_blob = dataURItoBlob(fileURI);
        if ($.isFunction(completionHandler)) {
            completionHandler(blob);
        }

        // Then you'll be able to handle the myimage.png file as base64
    });

//    var img = new Image();
//    //what[0]["asas"] = "something";
//    //img.crossOrigin = 'anonymous';
//
//    $(img).load(function(){
//
//        var canvas = document.createElement("canvas");
//        var ctx = canvas.getContext("2d");
//        ctx.drawImage(img, 0, 0);
//
//        console.log("Here");
//
//        var MAX_WIDTH = maxWidth;
//        var MAX_HEIGHT = maxHeight;
//        var width = img.width;
//        var height = img.height;
//
//        if (width > height) {
//          if (width > MAX_WIDTH) {
//            height *= MAX_WIDTH / width;
//            width = MAX_WIDTH;
//          }
//        } else {
//          if (height > MAX_HEIGHT) {
//            width *= MAX_HEIGHT / height;
//            height = MAX_HEIGHT;
//          }
//        }
//        canvas.width = width;
//        canvas.height = height;
//        var ctx = canvas.getContext("2d");
//        ctx.drawImage(img, 0, 0, width, height);
//
//        console.log("Here 2");
//
//        var dataurl = canvas.toDataURL("image/jpeg", 0.6);
//
//        console.log("Here 3");
//
//        var image_blob = dataURItoBlob(fileURI);
//
//        console.log("Here 4");
//
//        if ($.isFunction(completionHandler)) {
//            completionHandler(image_blob);
//        }
//    });
//
//    img.src = fileURI;



}

function resizeImage(file, maxHeight, maxWidth, completionHandler) {
	var img = document.createElement("img");
	img.crossOrigin='anonymous';

	// Create a file reader
	var reader = new FileReader();
	// Set the image once loaded into file reader
	reader.onload = function(e)
	{
		img.src = e.target.result;

		var canvas = document.createElement("canvas");
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);

		var MAX_WIDTH = maxWidth;
		var MAX_HEIGHT = maxHeight;
		var width = img.width;
		var height = img.height;

		if (width > height) {
		  if (width > MAX_WIDTH) {
			height *= MAX_WIDTH / width;
			width = MAX_WIDTH;
		  }
		} else {
		  if (height > MAX_HEIGHT) {
			width *= MAX_HEIGHT / height;
			height = MAX_HEIGHT;
		  }
		}
		canvas.width = width;
		canvas.height = height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0, width, height);

		var dataurl = canvas.toDataURL("image/jpeg", 0.6);

		console.log("Making Blob");
		var image_blob = dataURItoBlob(dataurl);

		if ($.isFunction(completionHandler)) {
			completionHandler(image_blob);
		}
	}
	// Load files into file reader
	reader.readAsDataURL(file);

}

function toggleHelp(step) {


	if($("#step-"+step).height() > 60) {
		$("#step-"+step+" .help-arrow").css({"background-image":"url(img/arrow_down.png)"});
	} else {
		$("#step-"+step+" .help-arrow").css({"background-image":"url(img/arrow_up.png)"});
	}

	$(".step-"+step).toggle(250);
}

function removeImage() {
	$("#image-preview").css({"background-image":"url(img/placeholder.png)"});
	$("#image-preview-2").css({"background-image":"url(img/placeholder.png)"});
	$("#edit-image").val("");
	$("#image").val("");

	$(".remove-image-button").hide();

}

function removeProfileImage() {
	$("#profile-image-preview").css({"background-image":"url(img/placeholder.png)"});
	$("#edit-pphoto").val("");

	$(".remove-image-button").hide();

}


function loadPreviewImage(input) {
	console.log("Loading Preview Image");

	$(".remove-image-button").show();
	var type = $(input).prop("type");
	var id = $(input).prop("id");
	if (type == "file") {

		var reader = new FileReader();
		// Set the image once loaded into file reader
		reader.onload = function(e)
		{
			console.log("image loaded");
			if (id == "file-chooser") {
				$("#image-preview").css({"background-image":"url("+e.target.result+")"});
			}
			if (id == "file-chooser-2") {
				console.log("preview loaded");
				console.log(e.target.result);
				$("#image-preview-2").css({"background-image":"url("+e.target.result+")"});
			}
			if (id == "file-chooser-3") {
				$("#profile-image-preview").css({"background-image":"url("+e.target.result+")"});
			}

			var file = $(input)[0].files[0];
			var form_data = new FormData();

			form_data.append('fn', "awsImageUpload");
			form_data.append('managerID', user["id"]);
			form_data.append('vtoken', user["vtoken"]);
			var extension = file.name.substr( (file.name.lastIndexOf('.') +1) );

			if(curPanel == "editProperty") {
				var filename = curPID+"."+extension;
			} else if(curPanel == "addProperty"){
				var filename = file.name;
				filename = filename.replace(/\W/g, '');
				filename = user["id"]+"_"+filename+"."+extension;
			} else if(curPanel == "account"){
				var filename = user["id"]+"_pphoto."+extension;
			}
      form_data.append('filename', filename);

      form_data.append('files', file);
      var fileSize = file['size'];

			console.log(form_data);

			if(fileSize > 2000000){
        showAlert("File Size Too Large",'The file size of the image you are trying to upload is too large. Please reduce the size or quality of the image and try again.');
      }  else {
        loading(true);
        console.log(JSON.stringify(form_data));
        $.ajax({
          url: "https://ws.spac.io/prod/oh/webservice/1.1.6/aws_image_upload.php",
          type: "POST",
          data: form_data,
          contentType: false,
          cache: false,
          processData:false,
          success: function(data)
          {
            if(data.status == 1) {
							if(curPanel == "editProperty") {
								$("#edit-image").val(decodeURIComponent(data.url));
							} else if(curPanel == "addProperty"){
								$("#image").val(decodeURIComponent(data.url));
							} else if(curPanel == "account"){
								$("#edit-pphoto").val(decodeURIComponent(data.url));
							}
            } else {
              showAlert("error","Unknown Error",'We could not upload this image. Please contact support.');
            }
            loading(false);
          },
  			  dataType: "json"
        });
      }

		}

		reader.readAsDataURL(input.files[0]);
	} else if (type == "url") {
		var url = input.value;
		if (id == "file-chooser") {
				$("#image-preview").css({"background-image":"url("+url+")"});
			}
			if (id == "file-chooser-2") {
				$("#image-preview-2").css({"background-image":"url("+url+")"});
			}
			if (id == "file-chooser-3") {
				$("#image-preview-3").css({"background-image":"url("+url+")"});
			}
	}
}


function hideEQ(num) {
	$(".eq"+num+"-slave").hide();
}

function showEQ(num) {
	$(".eq"+num+"-slave").show();
}


function getFileContentAsBase64(path,callback){
    window.resolveLocalFileSystemURL(path, gotFile, fail);

    function fail(e) {
          alert('Cannot found requested file');
    }

    function gotFile(fileEntry) {
           fileEntry.file(function(file) {
              var reader = new FileReader();
              reader.onloadend = function(e) {
                   var content = this.result;
                   callback(content);
              };
              // The most important point, use the readAsDatURL Method from the file plugin
              reader.readAsArrayBuffer(file);
           });
    }
}

function dataURItoBlob(dataURI) {
    console.log(dataURI);
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: 'image/jpeg' });
}

function checkURL (input) {
    var string = input.value;
    if (!~string.indexOf("http") && string != ""){
        string = "http://" + string;
    }
    input.value = string;
    return input
}

function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function syncCache() {


}


function sendEmail(toEmail) {

	// cordova.plugins.email.open({
	// 	to:      toEmail
  //
	// });

}

/*
function createCSV() {
	if (user["type"] == "freemium") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else {

		showAlert("Browser Only Function","Please export Registrants from your computer or browser. Login at www.spac.io/dashboard/");
	}
}*/

function broadcast() {
	if (user["type"] == "freemium" || user["type"] == "lite") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	} else {

		showAlert("Browser Only Function","Please broadcast messages to your Registrants from your computer or browser. Login at www.spac.io/dashboard/");
	}
}

function showTLCPanel() {
	$(".tlc-container").fadeIn();
	$(".signin-left-panel div:first").fadeOut();

	tlcCloseTimer = setTimeout(function(){ hideTLCPanel(); }, 20000);


}

function hideTLCPanel() {
	$(".tlc-container").fadeOut();
	$(".signin-left-panel div:first").fadeIn();
	clearTimeout(tlcCloseTimer);

}

function showZillowReviewPanel() {
    $(".zillowreview-container").fadeIn();
    $(".signin-left-panel div:first").fadeOut();

    if(user["integrations"]["zillowreviewAutoClose"] == "YES") {
        zillowreviewCloseTimer = setTimeout(function(){ hideZillowReviewPanel(); }, 20000);
    }
    $(".zillowreview-testimonials-scroll").off();
    $(".zillowreview-testimonials-scroll").scroll(function(){
        if(user["integrations"]["zillowreviewAutoClose"] == "YES") {
            clearTimeout(zillowreviewCloseTimer);
            zillowreviewCloseTimer = setTimeout(function(){ hideZillowReviewPanel(); }, 20000);
        }
    });
}

function showTTPanel() {
    $(".tt-container").fadeIn();
    $(".signin-left-panel div:first").fadeOut();


    if(user["integrations"]["ttAutoClose"] == "YES") {
        ttCloseTimer = setTimeout(function(){ hideTTPanel(); }, 20000);
    }
    $(".tt-testimonials-scroll").off();
    $(".tt-testimonials-scroll").scroll(function(){
        if(user["integrations"]["ttAutoClose"] == "YES") {
            clearTimeout(ttCloseTimer);
            ttCloseTimer = setTimeout(function(){ hideTTPanel(); }, 20000);
        }
    });
}

function showIDCPanel() {
	$(".idc-container").fadeIn();
	$(".signin-left-panel div:first").fadeOut();

}

function showRSPanel() {
    $(".rs-container").fadeIn();
    $(".signin-left-panel div:first").fadeOut();

    if(user["integrations"]["rsAutoClose"] == "YES") {
        rsCloseTimer = setTimeout(function(){ hideRSPanel(); }, 20000);
    }
    $(".rs-testimonials-scroll").off();
    $(".rs-testimonials-scroll").scroll(function(){
        if(user["integrations"]["rsAutoClose"] == "YES") {
            clearTimeout(rsCloseTimer);
            rsCloseTimer = setTimeout(function(){ hideRSPanel(); }, 20000);
        }
    });
}

function hideZillowReviewPanel() {
    $(".zillowreview-container").fadeOut();
    $(".signin-left-panel div:first").fadeIn();

    if(user["integrations"]["zillowreviewAutoClose"] == "YES") {
        clearTimeout(zillowreviewCloseTimer);
    }
}

function hideTTPanel() {
	$(".tt-container").fadeOut();
	$(".signin-left-panel div:first").fadeIn();

	if(user["integrations"]["ttAutoClose"] == "YES") {
		clearTimeout(ttCloseTimer);
	}

}

function hideIDCPanel() {
	$(".idc-container").fadeOut();
	$(".signin-left-panel div:first").fadeIn();

}

function hideRSPanel() {
	$(".rs-container").fadeOut();
	$(".signin-left-panel div:first").fadeIn();
	if(user["integrations"]["rsAutoClose"] == "YES") {
		clearTimeout(rsCloseTimer);
	}
}

function toggleRadio(radio) {
	$("#"+radio).prop("checked",true)

}

function disableApp() {
	showAlert("Trial Account Expired","Sorry, your trial account has expired.","Ok",null,function(){checkTrial();},null);
}



function hideKeyboard() {
	document.activeElement.blur();
    $(".right-panel").off();
    $(".left-panel").off();
    clearTimeout(keyboardTimer);
    $("input").blur();
}



function generateRandString(l)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < l; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}



function getPictureFromCamera() {
	//if(properties[curPID]["editable"] == true) {
		navigator.camera.getPicture(onPictureSuccess, onPictureFail, {
			quality: 60,
			sourceType: Camera.PictureSourceType.CAMERA,
			mediaType:Camera.MediaType.PICTURE,
			encodingType: Camera.EncodingType.JPEG,
			allowEdit : false,
			destinationType: Camera.DestinationType.FILE_URI,
			saveToPhotoAlbum: false,
			targetWidth: 800,
			targetHeight: 800
		});
	//} else {
		//showAlert("Property Details","The details of this property was added using your company's feed and cannot be edited. Please contact your listings administrator for changes.");
	//}

}

function getProfilePictureFromCamera() {
	navigator.camera.getPicture(onProfilePictureSuccess, onProfilePictureFail, {
			quality: 60,
			sourceType: Camera.PictureSourceType.CAMERA,
			mediaType:Camera.MediaType.PICTURE,
			encodingType: Camera.EncodingType.JPEG,
			allowEdit : true,
			destinationType: Camera.DestinationType.FILE_URI,
			saveToPhotoAlbum: false,
			targetWidth: 400,
			targetHeight: 400
		});
}

function getPictureFromLibrary() {
    if(curPanel == "editProperty") {
        if(properties[curPID]["editable"] == true) {

            navigator.camera.getPicture(onPictureSuccess, onPictureFail, {
                quality: 60,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                mediaType:Camera.MediaType.PICTURE,
                encodingType: Camera.EncodingType.JPEG,
                allowEdit : false,
                destinationType: Camera.DestinationType.FILE_URI,
                saveToPhotoAlbum: false,
                targetWidth: 800,
                targetHeight: 800
            });

        } else {
            showAlert("Property Details","The details of this property was added using your company's feed and cannot be edited. Please contact your listings administrator for changes.");
        }

    } else {
         navigator.camera.getPicture(onPictureSuccess, onPictureFail, {
                quality: 60,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                mediaType:Camera.MediaType.PICTURE,
                encodingType: Camera.EncodingType.JPEG,
                allowEdit : false,
                destinationType: Camera.DestinationType.FILE_URI,
                saveToPhotoAlbum: false,
                targetWidth: 800,
                targetHeight: 800
            });
    }
}

function getProfilePictureFromLibrary() {

		navigator.camera.getPicture(onProfilePictureSuccess, onProfilePictureFail, {
			quality: 60,
			sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
			mediaType:Camera.MediaType.PICTURE,
			encodingType: Camera.EncodingType.JPEG,
			allowEdit : true,
			destinationType: Camera.DestinationType.FILE_URI,
			saveToPhotoAlbum: false,
			targetWidth: 400,
			targetHeight: 400
		});


}

function onPictureSuccess(imageURI) {

	$("#image").val(imageURI);
	$("#edit-image").val(imageURI);

	$("#image-preview").css({"background-image":"url("+imageURI+")"});
	$("#image-preview-2").css({"background-image":"url("+imageURI+")"});

    //var image = document.getElementById('myImage');
    //image.src = "data:image/jpeg;base64," + imageData;
}

function onProfilePictureSuccess(imageURI) {
	$("#edit-pphoto").val(imageURI);
	$("#profile-image-preview").css({"background-image":"url("+imageURI+")"});
}

function onPictureFail(message) {
    console.log('Failed because: ' + message);
}

function onProfilePictureFail(message) {
    console.log('Failed because: ' + message);
}

function contactuallyIDFocus(tar) {
	var api_key = $("input[name='contactuallyID']").val();
	if (api_key.length > 0) {
		showAlert("Contactually Integration","Please clear your current API key before applying a new one.");
	}

}

function contactuallyBucketFocus(tar) {
	var api_key = $("input[name='contactuallyID']").val();
	if (api_key.length >= 32) {
		$("#contactually-clear-btn").show();
		$("#contactually-apply-btn").show();
		$("#contactually-get-btn").hide();
	}
}



function printInstructions() {
	showAlert("Print Instructions","Please print mobile sign-in instructions from your computer or browser. Login at www.spac.io/dashboard/");
}

function checkFreemiumEmail() {
	if (user["type"] == "freemium" || user["type"] == "lite") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	}
}

function checkFreemiumIntegration() {
	if (user["type"] == "freemium" || user["type"] == "lite") {
		showAlert("Pro Feature","This feature is available for Pro users only.");
	}
}

function calculateLeadScore(index) {
	var score = 0;
	var max_score = 4; //Max score starts at 3 for name, email, phone, social profiles
	var temp_leads_arr = {};

	if(curRID in registrants) {
    temp_leads_arr = registrants;

  } else if(curRID in registrants_inactive) {
    temp_leads_arr = registrants_inactive;

  }

	if("fullcontact" in temp_leads_arr[index]) {
		if (temp_leads_arr[index]["fullcontact"]["status"] == 200) {
			score++;
		}
	}

	if(temp_leads_arr[index]["name"] != "") {
		score++;
	}
	if(temp_leads_arr[index]["email"] != "") {
		score++;
	}
	if(temp_leads_arr[index]["phone"] != "") {
		score++;
	}

	$.each(temp_leads_arr[index]["answersObj"], function(index,value){
		var _question = value["questionObj"]["question"];
		_question = _question.toLowerCase();
		var _answer = value["answer"];

		$.each(lead_score_calibration, function(index2, val){
			var cal_question =val["question"];
			cal_question = cal_question.toLowerCase();

			if(cal_question == _question) {

				max_score = max_score + parseInt(val["max"]);

				if(_answer == "") {
					score = score + 0; //score unchanged
				} else if (val["type"] == "text") {
					score= score + parseInt(val["scoring"]["Any"]); //Scoring for any answer
				} else if(val["type"] == "mc") {

					if ("Any" in val["scoring"]) {
						score= score + parseInt(val["scoring"]["Any"]);
					} else {
						var def_score = 0.5;

						$.each(val["scoring"], function(index3, val2){
							if(_answer == index3) {
								//console.log(_answer+" matches "+index3);
								def_score = parseInt(val2);
							}
						})
						score = score + def_score;
					}

				} else {

					$.each(val["scoring"], function(index3, val2){
						if(_answer == index3) {
							//console.log(_answer+" matches "+index3);
							score = score +parseInt(val2);
						}
					})
				}
			}
		})

	});

	var standardized_score = Math.floor(score/max_score*100);
	return standardized_score;
}



function loadScoring() {
	if (localStorage["lead_score_calibration"] === null) {

	} else {
		lead_score_calibration = JSON.parse(localStorage["lead_score_calibration"]);
	}
	//lead_score_calibration = JSON.parse(localStorage["lead_score_calibration"]);
	if(lead_score_calibration[0]["scoring"]["YES"] == 1 && lead_score_calibration[0]["scoring"]["NO"] == 1) {
				$('input[type="radio"][name="q0"][value="INDIFF"]').last().prop("checked", true);
			} else if(lead_score_calibration[0]["scoring"]["YES"] == 2){
				$('input[type="radio"][name="q0"][value="YES"]').last().prop("checked", true);
			} else if(lead_score_calibration[0]["scoring"]["NO"] == 2){
				$('input[type="radio"][name="q0"][value="NO"]').last().prop("checked", true);
			}

			if(lead_score_calibration[1]["scoring"]["YES"] == 1 && lead_score_calibration[1]["scoring"]["NO"] == 1) {
				$('input[type="radio"][name="q1"][value="INDIFF"]').last().prop("checked", true);
			} else if(lead_score_calibration[1]["scoring"]["YES"] == 2){
				$('input[type="radio"][name="q1"][value="YES"]').last().prop("checked", true);
			} else if(lead_score_calibration[1]["scoring"]["NO"] == 2){
				$('input[type="radio"][name="q1"][value="NO"]').last().prop("checked", true);
			}

			if(lead_score_calibration[3]["scoring"]["Rent"] == 1 && lead_score_calibration[3]["scoring"]["Own"] == 1) {
				$('input[type="radio"][name="q3"][value="INDIFF"]').last().prop("checked", true);
			} else if(lead_score_calibration[3]["scoring"]["Rent"] == 2){
				$('input[type="radio"][name="q3"][value="rent"]').last().prop("checked", true);
			} else if(lead_score_calibration[3]["scoring"]["Own"] == 2){
				$('input[type="radio"][name="q3"][value="own"]').last().prop("checked", true);
			}

			if(lead_score_calibration[4]["scoring"]["Buy"] == 1 && lead_score_calibration[4]["scoring"]["Sell"] == 1) {
				$('input[type="radio"][name="q4"][value="INDIFF"]').last().prop("checked", true);
			} else if(lead_score_calibration[4]["scoring"]["Buy"] == 2){
				$('input[type="radio"][name="q4"][value="buy"]').last().prop("checked", true);
			} else if(lead_score_calibration[4]["scoring"]["Sell"] == 2){
				$('input[type="radio"][name="q4"][value="sell"]').last().prop("checked", true);
			}

			if(lead_score_calibration[7]["scoring"]["Live-in"] == 1 && lead_score_calibration[7]["scoring"]["Investment"] == 1) {
				$('input[type="radio"][name="q7"][value="INDIFF"]').last().prop("checked", true);
			} else if(lead_score_calibration[7]["scoring"]["Live-in"] == 2){
				$('input[type="radio"][name="q7"][value="livein"]').last().prop("checked", true);
			} else if(lead_score_calibration[7]["scoring"]["Investment"] == 2){
				$('input[type="radio"][name="q7"][value="investment"]').last().prop("checked", true);
			}

			if(lead_score_calibration[8]["scoring"]["YES"] == 1 && lead_score_calibration[8]["scoring"]["NO"] == 1) {
				$('input[type="radio"][name="q8"][value="INDIFF"]').last().prop("checked", true);
			} else if(lead_score_calibration[8]["scoring"]["YES"] == 2){
				$('input[type="radio"][name="q8"][value="YES"]').last().prop("checked", true);
			} else if(lead_score_calibration[8]["scoring"]["NO"] == 2){
				$('input[type="radio"][name="q8"][value="NO"]').last().prop("checked", true);
			}

			if(lead_score_calibration[9]["scoring"]["Cash"] == 1 && lead_score_calibration[9]["scoring"]["Financing"] == 1) {
				$('input[type="radio"][name="q9"][value="INDIFF"]').last().prop("checked", true);
			} else if(lead_score_calibration[9]["scoring"]["Cash"] == 2){
				$('input[type="radio"][name="q9"][value="cash"]').last().prop("checked", true);
			} else if(lead_score_calibration[9]["scoring"]["Financing"] == 2){
				$('input[type="radio"][name="q9"][value="financing"]').last().prop("checked", true);
			}
}
function explainQualityScore() {
	showAlert("Lead Quality Score","Lead Quality Score is a unique score generated using your personalized Ideal Visitor Profile, how visitors answer questions on your sign-in form, the number of answers they provide, and any public data we gather based on the accuracy of their contact information. You can customize your Ideal Visitor Profile in My Account.");

}

function showVerifiedStatus() {
	showAlert("Verified Contact","Verified contact with additional information available.");

}

function showUnverifiedStatus() {

	showAlert("Verified Contact","Contact not verified, no additional information available.");

}

function customSort(list, onkey, order) {

	for (var n = list.length; n > 1; --n) {
    	for (var i = 0; i < n-1; ++i) {
			var tmp_obj1 = list[i][onkey];
			var tmp_obj2 = list[i+1][onkey];
				if($.type(tmp_obj1) === "string") {
					tmp_obj1 = tmp_obj1.toUpperCase();
				}
				if($.type(tmp_obj2) === "string") {
					tmp_obj2 = tmp_obj2.toUpperCase();
				}
			if (order == "asc") {
				if (tmp_obj1  > tmp_obj2 ) {
					var tmp = list[i];
					list[i] = list[i+1];
					list[i+1] = tmp;
				 }
			}
			if (order == "desc") {
				if (tmp_obj1  < tmp_obj2) {
					var tmp = list[i];
					list[i] = list[i+1];
					list[i+1] = tmp;
				 }
			}
         }
     }

	 return list;
}

function customSortHasClient(list, onkey, order) {
	console.log(list.length);
	for (var n = list.length; n > 1; --n) {
    	for (var i = 0; i < n-1; ++i) {
			var tmp_obj1 = "";
			var tmp_obj2 = "";
			if(list[i]["answersObj"].length > 0 && list[i]["isBroker"] == "YES") {
				tmp_obj1 = list[i]["answersObj"][0]["answer"];
				if(tmp_obj1 == "") {
					tmp_obj1 = "N/A";
				}
			}

			if(list[i+1]["answersObj"].length > 0 && list[i+1]["isBroker"] == "YES") {
				tmp_obj2 = list[i+1]["answersObj"][0]["answer"];
				if(tmp_obj2 == "") {
					tmp_obj2 = "N/A";
				}
			}
			//console.log(tmp_obj1);
			//tmp_obj1 = tmp_obj1.toUpperCase();
			//tmp_obj2 = tmp_obj2.toUpperCase();

			if (order == "asc") {
				if (tmp_obj1  > tmp_obj2 ) {
					var tmp = list[i];
					list[i] = list[i+1];
					list[i+1] = tmp;
				 }
			}
			if (order == "desc") {
				if (tmp_obj1  < tmp_obj2) {
					var tmp = list[i];
					list[i] = list[i+1];
					list[i+1] = tmp;
				 }
			}
         }
     }

	 console.log(list);

	 return list;
}

function customSortDateTime(list, onkey, order) {

	for (var n = list.length; n > 1; --n) {
    	for (var i = 0; i < n-1; ++i) {
			var tmp_obj1 = list[i][onkey]["sec"];
			var tmp_obj2 = list[i+1][onkey]["sec"];

			if (order == "asc") {
				if (tmp_obj1  > tmp_obj2 ) {
					var tmp = list[i];
					list[i] = list[i+1];
					list[i+1] = tmp;
				 }
			}
			if (order == "desc") {
				if (tmp_obj1  < tmp_obj2) {
					var tmp = list[i];
					list[i] = list[i+1];
					list[i+1] = tmp;
				 }
			}
         }
     }


	 return list;
}

function sortBy(e, type, sort) {
	var order = $(e).data("order");
	if (order == "desc") {
		$(e).data("order","asc");
		$(e).css("background-image","url(img/arrow_up.png)");
		showSortedRegistrantsByType(type, sort, "asc");
	} else if(order == "asc") {
		$(e).data("order","desc");
		$(e).css("background-image","url(img/arrow_down.png)");
		showSortedRegistrantsByType(type, sort, "desc");
	}

}

function checkMandatory(field) {
	if ($("#allow-MandatoryEmail").is(':checked') && field == "email") {
		$("#allow-MandatoryPhone").prop( "checked", false );

	}

	if ($("#allow-MandatoryPhone").is(':checked') && field == "phone") {
		$("#allow-MandatoryEmail").prop( "checked", false );

	}
}

function contactSupport() {
	// cordova.plugins.email.open({
	// 	to:      'support@spac.io'
  //
	// });
}

function toggleSubmenu(submenu) {
	$("#"+submenu+"-submenu").toggle();
	$("#"+submenu+"-menu-item").toggleClass("selected");
}


function showRegistrantsPanel() {
    //$(".sub-container").hide();
    //$("#properties-panel").show();
    if(user["type"] == "freemium" || user["type"] == "lite") {
        showAlert("Pro Feature","This feature is available for Pro users only.");
    } else {
        $(".menu-item").removeClass("selected").addClass("not-selected");
        $(".submenu-item").removeClass("selected").addClass("not-selected");
        $("#leads-menu-item").removeClass("not-selected").addClass("selected");
        prevPanel = curPanel;
        curPanel = "leads";
        showRegistrants('all','buyers');
    }
}



function showSettingsPanel(panel) {

	$(".menu-item").removeClass("selected");
	$(".submenu-item").removeClass("selected");
	$("#"+panel+"-menu-item").addClass("selected");

	if(panel == "account") {
        $(".sub-container").hide();
		$("#account-panel").show();
		curPanel = "account";
        showAccountTab("profile");
	}

	if(panel == "plan") {
		showPlanPanel();
	}

	if(panel == "scoring") {
		showScoringPanel();
	}

	if(panel == "email-templates") {
		showEmailPanel();
	}

	if(panel == "integrations") {
		showIntegrationsPanel();
	}

	if(panel == "mls") {
		showMLSPanel();
	}


}

function autoEmailChanged() {

    if ($("#allow-autoEmail").is(':checked')) {
        $("#allow-bcc").prop( "checked", true );
    } else {
        $("#allow-bcc").prop( "checked", false );
    }


}

function autoBCCChanged() {

    if ($("#allow-autoEmail").is(':checked')) {
        //$("#allow-bcc").prop( "checked", true );
    } else {
        $("#allow-autoEmail").prop( "checked", true );
    }


}

function showSeeReps() {
  $(".see-rep-panel").show();
  $(".docs-panel").show();
  $(".select-docs-panel").hide();
  $(".sign-doc-panel").hide();

    seerepsTimer = setTimeout(function(){ hideSeeReps();}, 7000);

}

function hideSeeReps() {
    clearTimeout(seerepsTimer);
    $(".see-rep-panel").hide();

    showSelectDocs();

}

function showSelectDocs() {
  $(".docs-panel").show();
  $(".select-docs-panel").show();
  $(".sign-doc-panel").hide();
  $(".registrant-docs").html("");
  $(".docs").html("");
  $.each(brokerage["docs"],function(index,value){
    $(".docs").append("<div id='doc-"+index+"' class='doc-row' onclick='loadDoc("+index+")'><div class='icon'><div class='completed'><i class='fa fa-check-circle' aria-hidden='true'></i></div><i class='fa fa-file-text' aria-hidden='true'></i></div><div class='doc-name'>"+value["title"]+"</div></div>");
  });

  if(brokerage["docs"].length <= 1) {
        loadDoc(0);
  }
}


function closeSelectDocs() {

        $(".docs-panel").hide();

}

function closeDoc() {
if(curPanel == "visitor") {
        $(".select-docs-panel").hide();
        $(".sign-doc-panel").hide();
         $(".docs-panel").hide();
    } else {
     $(".docs-panel").show();
    $(".select-docs-panel").show();
        $(".sign-doc-panel").hide();
        $(".doc-close-button").hide();
        }
}

function loadDoc(index, completionHandler) {
    if(online) {

        if(curPanel == "visitor") {
            var doc_status = registrants[curRID]["docs"][index]["status"];
          if (doc_status == "signed") {
            doc_status = "Signed";
          } else if (doc_status == "declined") {
            doc_status = "Declined";
          } else if (doc_status == "with_broker") {
            doc_status = "With Broker";
          } else {
            doc_status = "Unsigned";
          }
          $("#doc-status").html("Document Status: "+doc_status);

        }

        if(curPanel == "form") {
            $("#doc-status").html("PLEASE SIGN DOCUMENT");
        }
        $(".doc-close-button").show();

        curDocIndex = index;
        curDoc = brokerage["docs"][index];
        $("#doc-image").attr("src","https://spac.io/dashboard/img/docs/"+curDoc["name"]+".jpg");

        $("#doc-inputs").html("");

        if(curPanel == "visitor") {
          registrantDocs[curDocIndex] = registrants[curRID]["docs"][curDocIndex];
        } else if(index in registrantDocs){

        } else {
          registrantDocs[index] = {};
          registrantDocs[index]["status"] = "unsigned";
        }

        $.each(curDoc["input"], function(index,value){

            //logConsole("Type: "+value["type"]);
            if (value["type"] == "text") {
                $("#doc-inputs").append("<input type='text' class='"+value["class"]+"' value='"+value["value"]+"' name='"+value["name"]+"' style='top:"+value["top"]+"px;left:"+value["left"]+"px;width:"+value["width"]+"px;height:"+value["height"]+"px;"+value["style"]+"'>");

                if(registrants[curRID]["docs"][curDocIndex]["status"] != "pending") {
                    $("#doc-inputs input[type='text'][name='"+value["name"]+"']").val(registrants[curRID]["docs"][curDocIndex]["data"][value["name"]]);
                }
            }

            if (value["type"] == "checkbox") {
                $("#doc-inputs").append("<input type='checkbox' "+value["checked"]+" value='"+value["value"]+"' name='"+value["name"]+"' style='top:"+value["top"]+"px;left:"+value["left"]+"px;width:"+value["width"]+"px;height:"+value["height"]+"px;"+value["style"]+"'>");

               if(registrants[curRID]["docs"][curDocIndex]["status"] != "pending") {
                    $("#doc-inputs input[type='checkbox'][name='"+value["name"]+"'][value='"+registrants[curRID]["docs"][curDocIndex]["data"][value["name"]]+"']").last().prop("checked", true);
                }

            }

            if (value["type"] == "sig") {
                $("#doc-inputs").append("<div id='"+value["id"]+"' class='sig' style='background-image:none;top:"+value["top"]+"px;left:"+value["left"]+"px;width:"+value["width"]+"px;height:"+value["height"]+"px;"+value["style"]+"' onclick='showSignaturePad(&#39;"+value["id"]+"&#39;)'>SIGN HERE</div>");


                if(registrants[curRID]["docs"][curDocIndex]["status"] != "pending") {
                    if (value["id"] in registrants[curRID]["docs"][curDocIndex]["data"]) {
                        if(registrants[curRID]["docs"][curDocIndex]["data"][value["id"]] != "") {
                            //logConsole("Loading Signature: "+registrants[curRID]["docs"][curDocIndex]["data"][value["id"]]);
                            $("#"+value["id"]).html("");
                            $("#"+value["id"]).css("background-image","url("+registrants[curRID]["docs"][curDocIndex]["data"][value["id"]]+")");

                            $("#"+value["id"]).css("background-size","contain");
                            $("#"+value["id"]).css("background-repeat","no-repeat");
                            $("#"+value["id"]).css("background-position","center center");
                        }
                    }
                }
            }
        });
				$(".see-rep-panel").hide();
        $(".docs-panel").show();
        $(".select-docs-panel").hide();
        $(".sign-doc-panel").show();
        //$(".disclosure-btn-bar").hide();
        //$(".disclosure-agent-bar").show();

        //$(".disclosure-form").css("top","200px");
        var d = new Date();
        $(".current-date").val(d.toLocaleDateString());
        $(".visitor-name").val(registrants[curRID]["name"]);
        $(".agent-name").val(user["fname"]+" "+user["lname"]);
        $(".sign-doc-panel").scrollTop(0);
        if ($.isFunction(completionHandler)) {
            completionHandler();
        }
    } else {

		showAlert("Offline Mode","This function requires a live internet connection.");
    }

}

function showSignaturePad(tar) {
	curSig = tar;
	$(".signature-panel").show();
	signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
			backgroundColor: 'rgba(255, 255, 255, 0)',
			penColor: 'rgb(0, 0, 0)'
			});

	if(curSig.substring(0, 4) == "edit") {
		var tempSig = curSig.substring(5, curSig.length);

    signaturePad.fromDataURL(registrants[curRID]["docs"][curDocIndex]["data"][tempSig]);

		//signaturePad.fromDataURL(disclosureForms[curRID]["data"][0][tempSig]);
		//console.log(disclosureForms[curRID]["data"][0][tempSig]);
	}
}

function saveSig() {
	var data = signaturePad.toDataURL('image/png');

  if(!("data" in registrantDocs[curDocIndex])){
      registrantDocs[curDocIndex]["data"] = {};
      registrantDocs[curDocIndex]["data"][curSig] = data;
  } else {
      registrantDocs[curDocIndex]["data"][curSig] = data;
  }

	//curDisclosure[curSig] = data;
	$("#"+curSig).html("");
	$("#"+curSig).css("background-image","url("+data+")");

	$("#"+curSig).css("background-size","contain");
	$("#"+curSig).css("background-repeat","no-repeat");
	$("#"+curSig).css("background-position","center center");
	$(".signature-panel").hide();

	if(curSig == "buyer-sig" && !signaturePad.isEmpty()) {
		registrantDocs[curDocIndex]["status"] = "signed";
	} else if(tempSig == "buyer-sig" && signaturePad.isEmpty()) {
		registrantDocs[curDocIndex]["status"] = "unsigned";
	}

}

function clearSig() {
	signaturePad.clear();
}


function saveDoc() {


  if (!("data" in registrantDocs[curDocIndex])) {
      registrantDocs[curDocIndex]["data"] = {};
  }

	$("#doc-form *").filter(':input').each(function(){
		var input = $(this);
		var name = $(this).attr("name");
		var type = $(this).attr("type");
		var value = $(this).val();

		if(type == "text") {
        registrantDocs[curDocIndex]["data"][name] = value;
		} else if (type == "checkbox") {
			if ($(this).is(':checked')) {
				registrantDocs[curDocIndex]["data"][name] = "YES";
			} else {
				registrantDocs[curDocIndex]["data"][name] = "NO";
			}
		}
	});

	var status = registrantDocs[curDocIndex]["status"];

  addDocData(curDocIndex,status,registrantDocs[curDocIndex]["data"], function(){

      $(".sign-doc-panel").hide();
      $(".select-docs-panel").show();

	});

}

function declineDoc(status) {

	$("#doc-form")[0].reset();
	$(".sig").css("background-image","none");
	$(".sig").html("SIGN HERE");
	//curDisclosure = {};
  registrantDocs[curDocIndex] = {};

  if (!("data" in registrantDocs[curDocIndex])) {
      registrantDocs[curDocIndex]["data"] = {};
  }

	$("#doc-form *").filter(':input').each(function(){
		var input = $(this);
		var name = $(this).attr("name");
		var type = $(this).attr("type");
		var value = $(this).val();

		if(type == "text") {
          registrantDocs[curDocIndex]["data"][name] = value;
		} else if (type == "checkbox") {
			if ($(this).is(':checked')) {
				registrantDocs[curDocIndex]["data"][name] = "YES";
			} else {
				registrantDocs[curDocIndex]["data"][name] = "NO";
			}
		}
	});

	addDocData(curDocIndex,status,registrantDocs[curDocIndex]["data"], function(){
    $(".sign-doc-panel").hide();
    $(".select-docs-panel").show();
	});
}

function hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);
    if (alpha) {
        return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
        return "rgb(" + r + ", " + g + ", " + b + ")";
    }
}

function htmlEscape(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}


function nextDay(d, dow){
    d.setDate(d.getDate() + (dow+(7-d.getDay())) % 7);
    return d;
}

function toggleAdvancedFilter(filter) {
    $("#"+filter+"-advanced-filter").toggle();
}

function showAdvancedFilter(filter) {
    $("#"+filter+"-advanced-filter").show();
}

function hideAdvancedFilter() {
    $(".advanced-filter-overlay").hide();
}

function showSSOPanel() {
    $(".sso-overlay").show();
}

function hideSSOPanel() {
    $(".sso-overlay").hide();
}
